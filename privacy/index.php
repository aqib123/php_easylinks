<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
		<title>Privacy Policy :: EasyLinks</title>
		<link rel="stylesheet" href="css/style.css"/>
		<link rel="shortcut icon" href="http://easylinks.ninja/bonuspage/images/favicon.png">
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){

				// hide #back-top first
				$("#back-top").hide();
				
				// fade in #back-top
				$(function () {
					$(window).scroll(function () {
						if ($(this).scrollTop() > 100) {
							$('#back-top').fadeIn();
						} else {
							$('#back-top').fadeOut();
						}
					});

					// scroll body to 0px on click
					$('#back-top a').click(function () {
						$('body,html').animate({
							scrollTop: 0
						}, 800);
						return false;
					});
				});

			});
		</script>
		
		<style>
			/*
			Back to top button 
			*/
			#back-top {
				bottom: 0;
				position: fixed;
				right: 20px;
			}
			#back-top a {
				width: 108px;
				display: block;
				text-align: center;
				font: 11px/100% Arial, Helvetica, sans-serif;
				text-transform: uppercase;
				text-decoration: none;
				color: #333;
				/* background color transition */
				-webkit-transition: 1s;
				-moz-transition: 1s;
				transition: 1s;
			}
			#back-top a:hover {
				color: #000;
			}
			/* arrow icon (span tag) */
			#back-top span {
				width: 108px;
				height: 108px;
				display: block;
				margin-bottom: 7px;
				background: #999999 url(images/up-arrow.png) no-repeat center center;
				/* rounded corners */
				-webkit-border-radius: 15px;
				-moz-border-radius: 15px;
				border-radius: 15px;
				/* background color transition */
				-webkit-transition: 1s;
				-moz-transition: 1s;
				transition: 1s;
			}
			#back-top a:hover span {
				background-color: #777;
			}
		</style>
	</head>
	<body id="top">
		<div class="com">
			<div class="wrapper">
				<div class="com">
					<div class="inner_wrapper com">
						<div class="align_center" style="padding: 10px 0;border-bottom: 2px solid #521B3F;">
							<img src="images/logo.png" alt="" style="width:330px;" />
						</div>
						<div class="com m_top2">
							<div class="terms_left_div">
					<div class="terms_head_div">
						<h1 class="terms_heading">Privacy</h1>
					</div>
					<div class="left term_wid">
						<div class="terms_intro_div">
							<div class="terms_head_div">
								<h2 class="terms_common_div">Introduction</h2>
							</div>
							<div class="terms_para_div">
								<p class="terms_para">
									We respect your privacy and are committed to protecting it through our compliance with this policy. This policy describes the types of information we may collect from you or that you may provide when you visit http://easylinks.ninja (our “Site”) and our practices for collecting, using, maintaining, protecting and disclosing that information.
								</p>
								<p class="terms_para"><b><i>This policy applies to information we collect:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. On this Site.</p>
								<p style="margin-left:15px;" class="terms_para">2. When you contact our customer service department for any reason.</p>
								<p style="margin-left:15px;" class="terms_para">3. In e-mail, text, fax, mail and other messages between you and this Site.</p>
								<p class="terms_para"><b><i>It does not apply to information collected by:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. Us through any other means, including on any other affiliated website; or</p>
								<p style="margin-left:15px;" class="terms_para">2. Any third party, including through any application or content (including advertising) that may link to or be accessible from the Site. Third parties, such as payment processing companies, may have their own privacy policies different from ours, please check with such third party.</p>
								<p style="margin-left:15px;" class="terms_para">3. If you do not agree with this Privacy and Cookie Policy or the Terms of Use located at http://easylinks.ninja , you may not use our Site. By accessing or using this Site, you agree to this Privacy and Cookie Policy and the Terms of Use. This Policy may change from time to time. Your continued use of this Site after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</p>
								<p class="terms_para"><b>Information We Collect About You and How We Collect It</b></p>
								<p class="terms_para"><b><i>We collect several types of information from and about users of our Site, including information:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. By which you may be personally identified.</p>
								<p style="margin-left:15px;" class="terms_para">2. Regarding user preferences, internet connection, operating system, browser, equipment you use to access the Site, etc.</p>
								<p style="margin-left:15px;" class="terms_para">3. Financial account information (such as your PayPal and credit card information), as needed to process payments.</p>
								<p class="terms_para"><b><i>We collect this information:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. Directly from you when you provide it to us, when you contact our customer service department, sign up for our newsletter, or otherwise.</p>
								<p style="margin-left:15px;" class="terms_para">2. Automatically as you navigate through the site. Information collected automatically may include usage details, IP addresses and information collected through cookies and other tracking technologies. We use Google Analytics.</p>
								<p style="margin-left:15px;" class="terms_para">3. From third parties, for example, our business partners.</p>
								<p class="terms_para"><b>Information You Provide to Us</b></p>
								<p class="terms_para"><b><i>The information we collect on or through our Site may include:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. Records and copies of your correspondence (including e-mail addresses), if you contact us.</p>
								<p style="margin-left:15px;" class="terms_para">2. Details of transactions you carry out through our Site.</p>
								<p style="margin-left:15px;" class="terms_para">3. Your search queries on the Site.</p>
								<p class="terms_para"><b>How We Use Your Information</b></p>
								<p class="terms_para"><b><i>We use information that we collect about you or that you provide to us, including any personal information:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. To provide you with information regarding the services that might interest you.</p>
								<p style="margin-left:15px;" class="terms_para">2. To fulfil any other purpose for which you provide it.</p>
								<p style="margin-left:15px;" class="terms_para">3. To provide you with notices about your account, including expiration and renewal notices.</p>
								<p style="margin-left:15px;" class="terms_para">4. To notify you about changes to our Site or any products or services we offer or provide though it.</p>
								<p style="margin-left:15px;" class="terms_para">5. To allow you to participate in interactive features on our Site.</p>
								<p style="margin-left:15px;" class="terms_para">6. In any other way we may describe when you provide the information.</p>
								<p style="margin-left:15px;" class="terms_para">7. For any other purpose with your consent.</p>
								<p class="terms_para"><b>Disclosure of Your Information</b></p>
								<p style="margin-left:15px;" class="terms_para">We may disclose aggregated information about our users and information that does not identify any individual without restriction.</p>
								<p class="terms_para"><b><i>We may disclose personal information that we collect or you provide as described in this privacy policy:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. To third parties we use to support our business.</p>
								<p style="margin-left:15px;" class="terms_para">2. To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of the Site’s assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal information about our Site users is among the assets transferred.</p>
								<p style="margin-left:15px;" class="terms_para">3. To fulfil the purpose for which you provide it.</p>
								<p style="margin-left:15px;" class="terms_para">4. For any other purpose disclosed by us when you provide the information.</p>
								<p style="margin-left:15px;" class="terms_para">5. With your consent.</p>
								<p class="terms_para"><b><i>We may also disclose your personal information:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. To comply with any court order, law or legal process, including to respond to any government or regulatory request.</p>
								<p style="margin-left:15px;" class="terms_para">2. To enforce or apply our Terms of Use available at http://easylinks.ninja/terms</p>
								<p style="margin-left:15px;" class="terms_para">3. If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of the Site, our customers or others.</p>
								<p class="terms_para"><b><i>This website uses Google AdWords</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. This website uses the Google AdWords remarketing service to advertise on third party websites (including Google) to previous visitors to our site. It could mean that we advertise to previous visitors who haven’t completed a task on our site, for example using the contact form to make an enquiry. This could be in the form of an advertisement on the Google search results page, or a site in the Google Display Network. Third-party vendors, including Google, use cookies to serve ads based on someone’s past visits to the website. Of course, any data collected will be used in accordance with our own privacy policy and Google’s privacy policy. You can set preferences for how Google advertises to you using the Google Ad Preferences page, and if you want to you can opt out of interest-based advertising entirely by cookie settings or permanently using a browser plugin. </p>
								<p class="terms_para"><b><i>Accessing and Correcting Your Information</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. You may send us an e-mail at <a href="mailto:easylinks@chadnicelysupport.com">easylinks@chadnicelysupport.com</a>  to request access to, correct or delete any personal information that you have provided to us. We may not accommodate a request to change information if we believe the change would violate any law or legal requirement or cause the information to be incorrect. </p>
								<p class="terms_para"><b>Cookie Policy</b></p>
								<p style="margin-left:15px;" class="terms_para">1. Cookies are small text files which a website may put on your computer or mobile device when you first visit a site or page. The cookie will help our Site, or another website, to recognise your device the next time you visit. For example, cookies can help us to remember your username and preferences, analyse how well our website is performing, or even allow us to recommend content we believe will be most relevant to you.</p>
								<p class="terms_para"><b><i>We may use cookies for the following reasons and purposes:</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. To provide the service you have asked for. Some cookies are essential so you can navigate through the website and use its features. Without these cookies, we would not be able to provide the services you’ve requested. For example, some cookies allow us to identify subscribers and ensure they can access the subscription only pages. If a subscriber opts to disable these cookies, the user will not be able to access all of the content that a subscription entitles them to. These cookies don’t gather information about you that could be used for marketing or remembering where you’ve been on the internet. Essential cookies keep you logged in during your visit, ensure that when you add something to the online shopping basket, it’s still there when you get to the checkout and make it possible to navigate through the website smoothly (session cookies).</p>
								<p style="margin-left:15px;" class="terms_para">2. To improve your browsing experience. These cookies allow the website to remember choices you make, such as your language or region and they provide improved features. These cookies will help remembering your preferences and settings, including marketing preferences, remembering if you’ve filled in certain forms, so you’re not asked to do it again, remembering if you’ve been to the site before and restricting the number of times you’re shown a particular advertisement. We might also use these cookies to highlight site services that we think will be of interest to you based on your usage of the website.</p>
								<p style="margin-left:15px;" class="terms_para">3. Analytics. To improve your experience on our Site, we like to keep track of what pages and links are popular and which ones don’t get used so much to help us keep our sites relevant and up to date. It’s also very useful to be able to identify trends of how people navigate (find their way through) our sites and if they get error messages from web pages. This group of cookies, often called “analytics cookies” are used to gather this information. These cookies don’t collect information that identifies you. The information collected is anonymous and is grouped with the information from everyone else’s cookies. We can then see the overall patterns of usage rather than any one person’s activity. Analytics cookies only record activity on the site you are on and they are only used to improve how a website works.</p>
								<p style="margin-left:15px;" class="terms_para">4. To show advertising that is relevant to your interests. We sell space on our Site to advertisers. The resulting adverts often contain cookies. These cookies are used to deliver adverts more relevant to you and your interests. They are also used to limit the number of times you see an advertisement as well as help measure the effectiveness of the advertising campaign. They are usually placed by advertising networks with our permission. They remember that you have visited a website and this information is shared with other organisations such as advertisers. Quite often targeting or advertising cookies will be linked to site functionality provided by the other organisation. Our emails may contain a single, campaign-unique “web beacon pixel” to tell us whether our emails are opened and verify any clicks through to links or advertisements within the email. We may use this information for purposes including determining which of our emails are more interesting to users, to query whether users who do not open our emails wish to continue receiving them and to inform our advertisers in aggregate how many users have clicked on their advertisements. The pixel will be deleted when you delete the email. If you do not wish the pixel to be downloaded to your device, you should select to receive emails from us in plain text rather than HTML.</p>
								<p style="margin-left:15px;" class="terms_para">5. Most browsers allow you to turn off cookies. To do this look at the “help” menu on your browser. Switching off cookies may restrict your use of the website and/or delay or affect the way in which it operates.</p>
								<p class="terms_para"><b><i>Data Security</i></b></p>
								<p style="margin-left:15px;" class="terms_para">1. Where we have given you (or where you have chosen) a password for access to certain parts of our Site, you are responsible for keeping this password confidential. We ask you not to share your password with anyone.</p>
								<p style="margin-left:15px;" class="terms_para">2. Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Site. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Site.</p>
								<p class="terms_para"><b>Changes to Our Privacy and Cookie Policy</b></p>
								<p style="margin-left:15px;" class="terms_para">1. It is our policy to post any changes we make to our Privacy and Cookie Policy on this page. You are responsible for periodically visiting our Site and this Privacy and Cookie Policy to check for any changes. </p>
								
								
								<p class="terms_para"><b>Contact Information</b></p>
								<p class="terms_para"><b><i>To ask questions or comment about this Privacy and Cookie Policy and our privacy practices, contact us at:</i></b></p>
								<p style="margin-left:15px;" class="terms_para"><a href="mailto:easylinks@chadnicelysupport.com">easylinks@chadnicelysupport.com</a></p>
								<div class="clear"></div> 

							</div>
						</div>
						

					</div>
				</div>
						</div>
						<br clear="all" />
					</div>
				</div>
				<br clear="all" />
			</div>
		</div>
	</body>
</html>