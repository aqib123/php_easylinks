<?php

require_once  './../sendgrid/sendgrid-php.php'; 

use SendGrid\Mail\From;

use SendGrid\Mail\HtmlContent;

use SendGrid\Mail\Mail;

use SendGrid\Mail\PlainTextContent;

use SendGrid\Mail\Subject;

use SendGrid\Mail\To;

if($post_act == "login_user"){

    if(!empty($_POST['username']) && !empty($_POST['password'])){

        $username = md5($_POST["username"]);

        $password = md5($_POST["password"]);

        $sql = "select ".$db->users.".* from ".$db->users." inner join ".$db->user_levels." on ".$db->users.".id=".$db->user_levels.".UserID where md5(user_login)='".$username."' and user_pass='".$password."' and LevelID=1";

        $user = $db->get_row($sql);

        if($user){

            global $current_user;



            $current_user = $user;

            $_SESSION["current_user"] = $current_user;

            $_SESSION["user_loggedin"] = true;



            if(isset($_POST['savepass'])){

                save_cookie("el_username", $username);

                save_cookie("el_password", $password);

            }else{

                remove_cookie("el_username");

                remove_cookie("el_password");

            }



            set_user_time_zone();

            create_user_constants();



            site_redirect("dashboard");

            die;

        }

    }

}else if($post_act == "register_user"){

    $registered = false;

    $jsonData = array("registered"=>false, "message"=>"");

    if(!empty($_POST['username']) && !empty($_POST['email'])){

        $username = $_POST["username"];

        $email = $_POST["email"];

        $sql = "select * from ".$db->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($email)."' ";

        $user = $db->get_row($sql);

        if(!$user){

            $password = md5($_POST["password"]);

            $email = $_POST["email"];

            $displayname = $_POST["displayname"];

            $user_activation_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));

            $data = array(

                "user_login"            =>      $username,

                "user_pass"             =>      $password,

                "user_email"            =>      $email,

                "user_registered"       =>      date("Y-m-d"),

                "user_status"           =>      "0",

                "display_name"          =>      $displayname,

                "user_activation_key"   =>      $user_activation_key

            );

            $db->show_errors();

            $db->insert($db->users, $data);

            if($db->insert_id > 0){

                $jsonData["registered"] = true;



                $to = $email;



                $subject = get_option("New User Email Subject");//, 'EasyLinks Access'

                $message = get_option("New User Email Body");//, 'EasyLinks Access'

                $message = preg_replace(array('/{username}/', '/{password}/'), array($username, $_POST["password"]), $message);



                if(send_mail($to, $subject, $message))

                    $jsonData["message"] = "Please check your email to verify your account.";

            }

        }else{

            $jsonData["registered"] = false;

            $jsonData["message"] = "This Username and/or Email is already registered.. Please try again";

        }

    }



    echo json_encode($jsonData);

    die;



} else if($post_act == "change_password"){

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");

    if(!empty($_POST['CurrentPassword']) && $current_user->user_pass == md5($_POST["CurrentPassword"]) && !empty($_POST["NewPassword"]) && $_POST["NewPassword"] == $_POST["ConfirmPassword"]){

        $db->update($db->users, array("user_pass" => md5($_POST["NewPassword"])), array("id" => $current_user->id));

        $jsonData["jscode"] = "window.location.href='".get_site_url("logout")."';";

    }

    echo json_encode($jsonData);

    die;

}else if($post_act == "update_profile"){

    $data = array(

        "user_nicename"     => $_POST["user_nicename"],

        "display_name"      => $_POST["display_name"],

        "timezoneid"        => $_POST["timezoneid"],

        "user_picture"      => $_POST["user_picture"]

    );



    if($db->update($db->users, $data, array("id" => $current_user->id))){

        $current_user->user_nicename = $_POST["user_nicename"];

        $current_user->display_name = $_POST["display_name"];

        $current_user->timezoneid = $_POST["timezoneid"];

        $current_user->user_picture = $_POST["user_picture"];

    }



    site_redirect("user-profile");

    die;

}else if($post_act == "forget_password"){

    $jsonData = array("mailsent"=>false, "message"=>"");

    if(!empty($_POST['username'])){

        $username = $_POST["username"];

        $sql = "select * from ".$db->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($username)."' ";

        $user = $db->get_row($sql);

        if($user){

            $jsonData["mailsent"] = true;

            $user_password_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));

            $db->update($db->users, array("user_password_key" => $user_password_key), array("id" => $user->id));



            $to = $user->user_email;

            $subject = get_option("Forgot Password Email Subject");//, 'EasyLinks Access'

            $message = get_option("Forgot Password Email Body");//, 'EasyLinks Access'

            $message = preg_replace(array('/{display_name}/', '/{user_login}/', '/{forget-password-url}/'), array($user->display_name, $user->user_login, get_site_url("login/forget-password/?key=".$user_password_key)), $message);



            if(send_mail($to, $subject, $message))

                $jsonData["message"] = "Please check your email to reset your password.";

        }else{

            $jsonData["mailsent"] = false;

            $jsonData["message"] = "This Username not found.. Please try again";

        }

    }



    echo json_encode($jsonData);

    die;



}else if($post_act == "reset_password"){

    $jsonData = array("reset"=>false, "message"=>"");



    if(!empty($_POST['key'])){

        $user_password_key = $_POST["key"];

        $sql = "select * from ".$db->users." where md5(user_password_key)='".md5($user_password_key)."'";

        $user = $db->get_row($sql);

        if($user){

            $newpassword = $_POST["newpassword"];

            $confirmpassword = $_POST["confirmpassword"];



            if($newpassword == $confirmpassword && $db->update($db->users, array("user_pass" => md5($newpassword), "user_password_key" => ""), array("user_password_key" => $user_password_key))){

                $jsonData["reset"] = true;

                $jsonData["message"] = "Password reset successfully... You can now login..";

            }

        }else{

            $jsonData["reset"] = false;

            $jsonData["message"] = "This Key not found.. Please try again";

        }

    }



    echo json_encode($jsonData);

    die;



}else if($post_act == "save_user"){

    $isUpdateForm = false;

    $return_back = $_POST['return_back'];



    if ($return_back == 1) {

        $username = str_replace(' ', '', $_POST['name']);

        $user_login = strtolower($username).''.rand(10,100) ;

        $user_nicename = $_POST['name'];

        $user_email = $_POST['email'];

        $display_name = $_POST['name'];

        $timezoneid = 'EST';

        $return_url = $_POST['return_url'];

    } else {

        $user_login = $_POST['user_login'];

        $user_nicename = $_POST['user_nicename'];

        $user_email = $_POST['user_email'];

        $display_name = $_POST['display_name'];

        $timezoneid = $_POST['timezoneid'];

        $return_back = $_POST['return_back'];

        $return_url = $_POST['return_url'];

    }

    if(isset($_POST['userid']) && is_numeric($_POST['userid'])){

        $UserID = intval($_POST["userid"]);

        $EditUser = $db->get_row("select * from ".$db->users." where id=".$UserID);

        if($EditUser)

            $isUpdateForm = true;

    }



    $data = array(

        "user_login"            =>      $user_login,

        "user_nicename"         =>      $user_nicename,

        "user_email"            =>      $user_email,

        "display_name"          =>      $display_name,

        "timezoneid"            =>      $timezoneid,

    );

    

    

    if ($return_back == 1) {

        // $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";

        // $password = substr( str_shuffle( $chars ), 0, $length );

        // $data["user_pass"] = $password;

        $data["user_pass"] = md5('mint5511');

        $levelId = 3;



    } else {

        if(!empty($_POST["NewPassword"]) && $_POST["NewPassword"] == $_POST["ConfirmPassword"]) {

            $data["user_pass"] = md5($_POST["NewPassword"]);

            $levelId = $_POST["LevelID"];

        }

    }

    if(!$isUpdateForm){

        if(isset($data["user_pass"])){

            $user_activation_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));

            $data["user_registered"] = date("Y-m-d");

            $data["user_status"] = "0";

            $data["user_activation_key"] = $user_activation_key;

            if($db->insert($db->users, $data)){

                $db->insert($db->user_levels, array(

                    "UserID" => $db->insert_id,

                    "LevelID" => intval($levelId),

                    "UserLevelEnabled" => 1

                ));



                $subject = get_option("New User Email Subject");//, 'EasyLinks Access'

                $message = get_option("New User Email Body");//, 'EasyLinks Access'

                $message = preg_replace(array('/{username}/', '/{password}/'), array($user_login, $data["user_pass"]), $message);

                

                if ($return_back == 1) {

                    $to = $user_email;

                    // $headers  = 'MIME-Version: 1.0' . "\r\n";

                    // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // $headers .= 'From: '.$from."\r\n".

                    //     'Reply-To: '.$from."\r\n" .

                    //     'X-Mailer: PHP/' . phpversion();

                    

                    $subject = 'Welcome to EasyLinks';

                    $message = '<html><body>';

                    $message .= '<img src="https://founder.easylinks.ninja/hosted/images/60/d497307c364293a678944fad8904c8/easylinks_logo-1-.png" style="width: 350px;">';

                    $message .= '<p style="font-size: 15px;">Hey '.$user_nicename.',</p>';

                    $message .= '<p style="font-size: 15px;">Welcome to the EasyLinks Platform!  This<br/>is going to allow you to create brandable<br/>and trackable links in a matter of minutes! </p>';

                    $message .= "<p style='font-size: 15px;'>You're even going to be able to use your own<br/>domain.</p>";

                    $message .= '<p style="font-size: 15px;">Here are your details:</p>';

                    $message .= '<p style="font-size: 15px;"><b>Login:</b> http://easylinks.io/login<br>';

                    $message .= '<b>User:</b> '.$user_login.'<br>';

                    $message .= '<b>Pass:</b> mint5511</p>';

                    $message .= "<p style='font-size: 15px;'>Now once you login you're going to want to click<br/>the profile icon in the TOP RIGHT corner and change<br/>your password, as we made it the same for everybody!</p>";

                    $message .= '<p style="font-size: 15px;">Also make sure you join this group so you can learn<br/>how to go out there and start profiting with your<br/>new EasyLinks platform</p>';

                    $message .= '<a href="https://www.facebook.com/groups/myeasylinks" style="font-size: 15px;">>> Click Here To Join The Group <<</a>';

                    $message .= '<p style="font-size: 15px;">All the best,</p>';

                    $message .= '<p style="font-size: 15px;">Chad</p>';

                    $message .= "<p style='font-size: 15px;'>PS: Make sure you join that group you'll be amazed<br/>at what we are doing there!</p>";

                    $message .= '</body></html>';

                    // send_mail($to, $subject, $message);

                    $from = new From("noreply@easylinks.online");

        			$subject = new Subject($subject);

        			$to = new To($to);

        			$htmlContent = new HtmlContent(

        			   $message

        			);

        			$email = new Mail(

        			    $from,

        			    $to,

        			    $subject,

        			    $htmlContent

        			);

        			$sendgrid = new \SendGrid('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');

        			try {

        			    $response = $sendgrid->send($email);

        			 //   print $response->statusCode() . "\n";

        			 //   print_r($response->headers());

        			 //   print $response->body() . "\n";

        			} catch (Exception $e) {

        			 //   echo 'Caught exception: '.  $e->getMessage(). "\n";

        			}

        // 			die;

                     $url ='https://mau48127.api-us1.com/api/3/contacts';

                  $headers = array("Api-Token: 8abf54a4c600ff89204e83eb0213c06bb981b45510e3ddd25b01e687c722c9df1c1c86ff");

                  $data_active = [];

                  $method = "POST";

                  $data_active['contact']['email'] = $user_email;

                  if ($user_nicename) {

                    $data_active['contact']['firstName'] = $user_nicename;

                    $data_active['contact']['lastName'] = '';

                  } else {

                    $data_active['contact']['firstName'] = "VIP";

                    $data_active['contact']['lastName'] = "Guest";

                  }

                  $data_active = json_encode($data_active);

				  $ch = curl_init();

					curl_setopt_array($ch, array(

						CURLOPT_URL => $url,

						CURLOPT_RETURNTRANSFER => true,

						CURLOPT_ENCODING => '',

						CURLOPT_MAXREDIRS => 10,

						CURLOPT_TIMEOUT => 0,

						CURLOPT_FOLLOWLOCATION => true,

						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

						CURLOPT_CUSTOMREQUEST => $method,

						CURLOPT_POSTFIELDS => $data_active,

						CURLOPT_HTTPHEADER => $headers,

						CURLOPT_FAILONERROR => true

					));



					$response = curl_exec($ch);

					$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

					if (curl_errno($ch)) {

						$error_msg = curl_error($ch);

					}

					curl_close($ch);

					if (isset($error_msg)) {

						$data = [];

						$data['status'] = 'false';

						$data['status_code'] = $http_status;

						$data['message'] = $error_msg;

						

					} else {

						$result = json_decode($response);

						$data = [];

						$data['status'] = 'true';

						$data['status_code'] = $http_status;

						$data['result'] = $result;

						if($data['status_code']==201){

							 if ($data['result']->contact->id) {

							  $url = 'https://mau48127.api-us1.com/api/3/contactLists';

							  $data_active_status = [];

							  $data_active_status['contactList']['list'] = '370';

							  $data_active_status['contactList']['contact'] = $data['result']->contact->id;

							  $data_active_status['contactList']['status'] = 1;

							  $data_active_status = json_encode($data_active_status);

							          $ch = curl_init();

										curl_setopt_array($ch, array(

											CURLOPT_URL => $url,

											CURLOPT_RETURNTRANSFER => true,

											CURLOPT_ENCODING => '',

											CURLOPT_MAXREDIRS => 10,

											CURLOPT_TIMEOUT => 0,

											CURLOPT_FOLLOWLOCATION => true,

											CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

											CURLOPT_CUSTOMREQUEST => $method,

											CURLOPT_POSTFIELDS => $data_active_status,

											CURLOPT_HTTPHEADER => $headers,

											CURLOPT_FAILONERROR => true

										));



										$response = curl_exec($ch);

										$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

										if (curl_errno($ch)) {

											$error_msg = curl_error($ch);

										}

										curl_close($ch);

										if (isset($error_msg)) {

											$data = [];

											$data['status'] = 'false';

											$data['status_code'] = $http_status;

											$data['message'] = $error_msg;

											

										} else {

											$result = json_decode($response);

											$data = [];

											$data['status'] = 'true';

											$data['status_code'] = $http_status;

											$data['result'] = $result;

											

										}

							}	

						}

           }

                    

                }

            }

        }

    } else {

        $db->update($db->users, $data, array("id" => $EditUser->id));

        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.saasonboard.com/api/user-management/change_company_user_password',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('email' => $user_email,'password' => $_POST["NewPassword"]),
        ));
        $response = curl_exec($curl);
        curl_close($curl);


        $UserLevel = $db->get_row("select * from ".$db->user_levels." where LevelID = ".$_POST["LevelID"]." and UserID = ".$EditUser->id);

        if($UserLevel) {

            $db->update($db->user_levels, array("LevelID" => intval($_POST["LevelID"])), array("UserID" => $EditUser->id));

        } else {

            $db->insert($db->user_levels, array(

                "UserID" => $EditUser->id,

                "LevelID" => intval($_POST["LevelID"]),

                "UserLevelEnabled" => 1

            ));

        }

    }

      

    if ($return_back == 1) {

        header('location: '.$return_url);

    } else {

        site_redirect("users");

    }

    die;

}else if($post_act == "save_crish"){

    $isUpdateForm = false;

    $return_back = $_POST['return_back'];



    if ($return_back == 1) {

        $username = str_replace(' ', '', $_POST['name']);

        $user_login = strtolower($username).''.rand(10,100) ;

        $user_nicename = $_POST['name'];

        $user_email = $_POST['email'];

        $display_name = $_POST['name'];

        $timezoneid = 'EST';

        $return_url = $_POST['return_url'];

    } else {

        $user_login = $_POST['user_login'];

        $user_nicename = $_POST['user_nicename'];

        $user_email = $_POST['user_email'];

        $display_name = $_POST['display_name'];

        $timezoneid = $_POST['timezoneid'];

        $return_back = $_POST['return_back'];

        $return_url = $_POST['return_url'];

    }

    if(isset($_POST['userid']) && is_numeric($_POST['userid'])){

        $UserID = intval($_POST["userid"]);

        $EditUser = $db->get_row("select * from ".$db->users." where id=".$UserID);

        if($EditUser)

            $isUpdateForm = true;

    }



    $data = array(

        "user_login"            =>      $user_login,

        "user_nicename"         =>      $user_nicename,

        "user_email"            =>      $user_email,

        "display_name"          =>      $display_name,

        "timezoneid"            =>      $timezoneid,

    );

    

    

    if ($return_back == 1) {

        // $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";

        // $password = substr( str_shuffle( $chars ), 0, $length );

        // $data["user_pass"] = $password;

        $data["user_pass"] = md5('mint5511');

        $levelId = 3;



    } else {

        if(!empty($_POST["NewPassword"]) && $_POST["NewPassword"] == $_POST["ConfirmPassword"]) {

            $data["user_pass"] = md5($_POST["NewPassword"]);

            $levelId = $_POST["LevelID"];

        }

    }

    if(!$isUpdateForm){

        if(isset($data["user_pass"])){

            $user_activation_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));

            $data["user_registered"] = date("Y-m-d");

            $data["user_status"] = "0";

            $data["user_activation_key"] = $user_activation_key;

            if($db->insert($db->users, $data)){

                $db->insert($db->user_levels, array(

                    "UserID" => $db->insert_id,

                    "LevelID" => intval($levelId),

                    "UserLevelEnabled" => 1

                ));



                $subject = get_option("New User Email Subject");//, 'EasyLinks Access'

                $message = get_option("New User Email Body");//, 'EasyLinks Access'

                $message = preg_replace(array('/{username}/', '/{password}/'), array($user_login, $data["user_pass"]), $message);

                

                if ($return_back == 1) {

                    $to = $user_email;

                    // $headers  = 'MIME-Version: 1.0' . "\r\n";

                    // $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // $headers .= 'From: '.$from."\r\n".

                    //     'Reply-To: '.$from."\r\n" .

                    //     'X-Mailer: PHP/' . phpversion();

                    

                    $subject = 'Welcome to EasyLinks';

                    $message = '<html><body>';

                    $message .= '<img src="https://founder.easylinks.ninja/hosted/images/60/d497307c364293a678944fad8904c8/easylinks_logo-1-.png" style="width: 350px;">';

                    $message .= '<p style="font-size: 15px;">Hey '.$user_nicename.',</p>';

                    $message .= '<p style="font-size: 15px;">Welcome to the EasyLinks Platform!  This<br/>is going to allow you to create brandable<br/>and trackable links in a matter of minutes! </p>';

                    $message .= "<p style='font-size: 15px;'>You're even going to be able to use your own<br/>domain.</p>";

                    $message .= '<p style="font-size: 15px;">Here are your details:</p>';

                    $message .= '<p style="font-size: 15px;"><b>Login:</b> http://easylinks.io/login<br>';

                    $message .= '<b>User:</b> '.$user_login.'<br>';

                    $message .= '<b>Pass:</b> mint5511</p>';

                    $message .= "<p style='font-size: 15px;'>Now once you login you're going to want to click<br/>the profile icon in the TOP RIGHT corner and change<br/>your password, as we made it the same for everybody!</p>";

                    $message .= '<p style="font-size: 15px;">Also make sure you join this group so you can learn<br/>how to go out there and start profiting with your<br/>new EasyLinks platform</p>';

                    $message .= '<a href="https://www.facebook.com/groups/myeasylinks" style="font-size: 15px;">>> Click Here To Join The Group <<</a>';

                    $message .= '<p style="font-size: 15px;">All the best,</p>';

                    $message .= '<p style="font-size: 15px;">Chad</p>';

                    $message .= "<p style='font-size: 15px;'>PS: Make sure you join that group you'll be amazed<br/>at what we are doing there!</p>";

                    $message .= '</body></html>';

                    // send_mail($to, $subject, $message);

                    $from = new From("noreply@easylinks.online");

        			$subject = new Subject($subject);

        			$to = new To($to);

        			$htmlContent = new HtmlContent(

        			   $message

        			);

        			$email = new Mail(

        			    $from,

        			    $to,

        			    $subject,

        			    $htmlContent

        			);

        			$sendgrid = new \SendGrid('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');

        			try {

        			    $response = $sendgrid->send($email);

        			 //   print $response->statusCode() . "\n";

        			 //   print_r($response->headers());

        			 //   print $response->body() . "\n";

        			} catch (Exception $e) {

        			 //   echo 'Caught exception: '.  $e->getMessage(). "\n";

        			}



                     $url ='https://mau48127.api-us1.com/api/3/contacts';

                  $headers = array("Api-Token: 8abf54a4c600ff89204e83eb0213c06bb981b45510e3ddd25b01e687c722c9df1c1c86ff");

                  $data_active = [];

                  $method = "POST";

                  $data_active['contact']['email'] = $user_email;

                  if ($user_nicename) {

                    $data_active['contact']['firstName'] = $user_nicename;

                    $data_active['contact']['lastName'] = '';

                  } else {

                    $data_active['contact']['firstName'] = "VIP";

                    $data_active['contact']['lastName'] = "Guest";

                  }

                  $data_active = json_encode($data_active);

				  $ch = curl_init();

					curl_setopt_array($ch, array(

						CURLOPT_URL => $url,

						CURLOPT_RETURNTRANSFER => true,

						CURLOPT_ENCODING => '',

						CURLOPT_MAXREDIRS => 10,

						CURLOPT_TIMEOUT => 0,

						CURLOPT_FOLLOWLOCATION => true,

						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

						CURLOPT_CUSTOMREQUEST => $method,

						CURLOPT_POSTFIELDS => $data_active,

						CURLOPT_HTTPHEADER => $headers,

						CURLOPT_FAILONERROR => true

					));



					$response = curl_exec($ch);

					$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

					if (curl_errno($ch)) {

						$error_msg = curl_error($ch);

					}

					curl_close($ch);

					if (isset($error_msg)) {

						$data = [];

						$data['status'] = 'false';

						$data['status_code'] = $http_status;

						$data['message'] = $error_msg;

						

					} else {

						$result = json_decode($response);

						$data = [];

						$data['status'] = 'true';

						$data['status_code'] = $http_status;

						$data['result'] = $result;

						if($data['status_code']==201){

							 if ($data['result']->contact->id) {

							  $url = 'https://mau48127.api-us1.com/api/3/contactLists';

							  $data_active_status = [];

							  $data_active_status['contactList']['list'] = '372';

							  $data_active_status['contactList']['contact'] = $data['result']->contact->id;

							  $data_active_status['contactList']['status'] = 1;

							  $data_active_status = json_encode($data_active_status);

							          $ch = curl_init();

										curl_setopt_array($ch, array(

											CURLOPT_URL => $url,

											CURLOPT_RETURNTRANSFER => true,

											CURLOPT_ENCODING => '',

											CURLOPT_MAXREDIRS => 10,

											CURLOPT_TIMEOUT => 0,

											CURLOPT_FOLLOWLOCATION => true,

											CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

											CURLOPT_CUSTOMREQUEST => $method,

											CURLOPT_POSTFIELDS => $data_active_status,

											CURLOPT_HTTPHEADER => $headers,

											CURLOPT_FAILONERROR => true

										));



										$response = curl_exec($ch);

										$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

										if (curl_errno($ch)) {

											$error_msg = curl_error($ch);

										}

										curl_close($ch);

										if (isset($error_msg)) {

											$data = [];

											$data['status'] = 'false';

											$data['status_code'] = $http_status;

											$data['message'] = $error_msg;

											

										} else {

											$result = json_decode($response);

											$data = [];

											$data['status'] = 'true';

											$data['status_code'] = $http_status;

											$data['result'] = $result;

											

										}

							}	

						}

           }

                    

                }

            }

        }

    } else {

        $db->update($db->users, $data, array("id" => $EditUser->id));

        $UserLevel = $db->get_row("select * from ".$db->user_levels." where LevelID = ".$_POST["LevelID"]." and UserID = ".$EditUser->id);

        if($UserLevel) {

            $db->update($db->user_levels, array("LevelID" => intval($_POST["LevelID"])), array("UserID" => $EditUser->id));

        } else {

            $db->insert($db->user_levels, array(

                "UserID" => $EditUser->id,

                "LevelID" => intval($_POST["LevelID"]),

                "UserLevelEnabled" => 1

            ));

        }

    }

      

    if ($return_back == 1) {

        header('location: '.$return_url);

    } else {

        site_redirect("users");

    }

    die;

}