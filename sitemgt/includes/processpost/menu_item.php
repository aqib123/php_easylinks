<?php
if($post_act == "save_menu_item"){
    $isUpdateForm = false;
    $MenuItemLabel = $_POST['MenuItemLabel'];
    $MenuItemURL = $_POST['MenuItemURL'];
    $MenuItemClass = $_POST['MenuItemClass'];
    $MenuItemPosition = $_POST['MenuItemPosition'];
    $MenuItemEnabled = isset($_POST["MenuItemEnabled"]);

    if(isset($_POST['menu_itemid']) && is_numeric($_POST['menu_itemid'])){
        $MenuItemID = intval($_POST["menu_itemid"]);
        $EditMenuItem = $db->get_row("select * from ".$db->menu_items." where id=".$MenuItemID);
        if($EditMenuItem)
            $isUpdateForm = true;
    }

    $data = array(
        "MenuItemLabel" => $MenuItemLabel,
        "MenuItemURL" => $MenuItemURL,
        "MenuItemClass" => $MenuItemClass,
        "MenuItemPosition" => $MenuItemPosition,
        "MenuItemEnabled" => $MenuItemEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->menu_items, $data);
    else
        $db->update($db->menu_items, $data, array("id" => $EditMenuItem->id));

    site_redirect("menu-items");
    die;
}