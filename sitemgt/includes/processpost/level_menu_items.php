<?php
if($post_act == "save_level_menu_items"){
    $sql = "select * from ".$db->levels.((isset($_POST["levelid"]) && is_numeric($_POST["levelid"]))?" where id=".$_POST["levelid"]." ":"");
    $levels = $db->get_results($sql);

    $sql = "select * from ".$db->menu_items." where MenuItemParentID=0 and MenuItemHasAdmin=0 ".((isset($_POST["menu_itemid"]) && is_numeric($_POST["menu_itemid"]))?" and id=".$_POST['menu_itemid']." ":"");
    $menu_items = $db->get_results($sql);
    foreach ($menu_items as $menu_item){
        foreach ($levels as $level){
            $LevelMenuItemID = $db->get_var("select id from ".$db->level_menu_items." where MenuItemID=".$menu_item->id." and LevelID=".$level->id);
            $LevelMenuItemEnabled = isset($_POST["level_".$level->id."_menu_item_".$menu_item->id]);
            if($LevelMenuItemID)
                $db->update($db->level_menu_items, array("LevelMenuItemEnabled" => $LevelMenuItemEnabled), array("id" => $LevelMenuItemID));
            else
                $db->insert($db->level_menu_items, array("LevelID" => $level->id, "MenuItemID" => $menu_item->id, "LevelMenuItemEnabled" => $LevelMenuItemEnabled));
        }
    }

    $pageurl = "level-menu-items".((isset($_POST["levelid"]) && is_numeric($_POST["levelid"]))?"?levelid=".$_POST["levelid"]." ":"").((isset($_POST["menu_itemid"]) && is_numeric($_POST["menu_itemid"]))?"?menu_itemid=".$_POST['menu_itemid']." ":"");
    site_redirect($pageurl);
    die;
}