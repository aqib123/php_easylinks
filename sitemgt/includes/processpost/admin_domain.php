<?php
if($post_act == "save_admin_domain"){
    $isUpdateForm = false;
    $DomainName = $_POST['DomainName'];
    $DomainSlug = $_POST['DomainSlug'];
    $DomainUrl = $_POST['DomainUrl'];

    if(isset($_POST['domainid']) && is_numeric($_POST['domainid'])){
        $DomainID = intval($_POST["domainid"]);
        $EditDomain = $db->get_row("select * from ".$db->domains." where DomainType='admindomain' and  id=".$DomainID);
        if($EditDomain)
            $isUpdateForm = true;
    }

    $data = array(
        "DomainName" => $DomainName,
        "DomainSlug" => $DomainSlug,
        "DomainUrl" => $DomainUrl,
        "DomainType" => 'admindomain',
        "DomainForward" => 0,
        "userid" => 0,
        "DefaultCloakURL" => 0,
        "DateUpdated" => strtotime("now")
    );

    if(!$isUpdateForm){
        $data["DateAdded"] = strtotime("now");
        $db->insert($db->domains, $data);
    } else {
        $db->update($db->domains, $data, array("id" => $EditDomain->id));
    }

    site_redirect("admin-domains");
    die;
}