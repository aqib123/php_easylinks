<?php
if($post_act == "save_level_modules"){
    $sql = "select * from ".$db->levels.((isset($_POST["levelid"]) && is_numeric($_POST["levelid"]))?" where id=".$_POST["levelid"]." ":"");
    $levels = $db->get_results($sql);

    $sql = "select * from ".$db->modules.((isset($_POST["moduleid"]) && is_numeric($_POST["moduleid"]))?" where id=".$_POST['moduleid']." ":"");
    $modules = $db->get_results($sql);
    foreach ($modules as $module){
        foreach ($levels as $level){
            $LevelModuleID = $db->get_var("select id from ".$db->level_modules." where ModuleID=".$module->id." and LevelID=".$level->id);
            $LevelModuleEnabled = isset($_POST["level_".$level->id."_module_".$module->id]);
            if($LevelModuleID)
                $db->update($db->level_modules, array("LevelModuleEnabled" => $LevelModuleEnabled), array("id" => $LevelModuleID));
            else
                $db->insert($db->level_modules, array("LevelID" => $level->id, "ModuleID" => $module->id, "LevelModuleEnabled" => $LevelModuleEnabled));
        }
    }

    $pageurl = "level-modules".((isset($_POST["levelid"]) && is_numeric($_POST["levelid"]))?"?levelid=".$_POST["levelid"]." ":"").((isset($_POST["moduleid"]) && is_numeric($_POST["moduleid"]))?"?moduleid=".$_POST['moduleid']." ":"");
    site_redirect($pageurl);
    die;
}