<?php
if($post_act == "save_role_menu_items"){
    $sql = "select * from ".$db->roles.((isset($_POST["roleid"]) && is_numeric($_POST["roleid"]))?" where id=".$_POST["roleid"]." ":"");
    $roles = $db->get_results($sql);

    $sql = "select * from ".$db->menu_items." where MenuItemParentID=0 and MenuItemHasAdmin=0 ".((isset($_POST["menu_itemid"]) && is_numeric($_POST["menu_itemid"]))?" and id=".$_POST['menu_itemid']." ":"");
    $menu_items = $db->get_results($sql);
    foreach ($menu_items as $menu_item){
        foreach ($roles as $role){
            $RoleMenuItemID = $db->get_var("select id from ".$db->role_menu_items." where MenuItemID=".$menu_item->id." and RoleID=".$role->id);
            $RoleMenuItemEnabled = isset($_POST["role_".$role->id."_menu_item_".$menu_item->id]);
            if($RoleMenuItemID)
                $db->update($db->role_menu_items, array("RoleMenuItemEnabled" => $RoleMenuItemEnabled), array("id" => $RoleMenuItemID));
            else
                $db->insert($db->role_menu_items, array("RoleID" => $role->id, "MenuItemID" => $menu_item->id, "RoleMenuItemEnabled" => $RoleMenuItemEnabled));
        }
    }

    $pageurl = "role-menu-items".((isset($_POST["roleid"]) && is_numeric($_POST["roleid"]))?"?roleid=".$_POST["roleid"]." ":"").((isset($_POST["menu_itemid"]) && is_numeric($_POST["menu_itemid"]))?"?menu_itemid=".$_POST['menu_itemid']." ":"");
    site_redirect($pageurl);
    die;
}