<?php
if($post_act == "save_role_modules"){
    $sql = "select * from ".$db->roles.((isset($_POST["roleid"]) && is_numeric($_POST["roleid"]))?" where id=".$_POST["roleid"]." ":"");
    $roles = $db->get_results($sql);

    $sql = "select * from ".$db->modules.((isset($_POST["moduleid"]) && is_numeric($_POST["moduleid"]))?" where id=".$_POST['moduleid']." ":"");
    $modules = $db->get_results($sql);
    foreach ($modules as $module){
        foreach ($roles as $role){
            $RoleModuleID = $db->get_var("select id from ".$db->role_modules." where ModuleID=".$module->id." and RoleID=".$role->id);
            $RoleModuleEnabled = isset($_POST["role_".$role->id."_module_".$module->id]);
            if($RoleModuleID)
                $db->update($db->role_modules, array("RoleModuleEnabled" => $RoleModuleEnabled), array("id" => $RoleModuleID));
            else
                $db->insert($db->role_modules, array("RoleID" => $role->id, "ModuleID" => $module->id, "RoleModuleEnabled" => $RoleModuleEnabled));
        }
    }

    $pageurl = "role-modules".((isset($_POST["roleid"]) && is_numeric($_POST["roleid"]))?"?roleid=".$_POST["roleid"]." ":"").((isset($_POST["moduleid"]) && is_numeric($_POST["moduleid"]))?"?moduleid=".$_POST['moduleid']." ":"");
    site_redirect($pageurl);
    die;
}