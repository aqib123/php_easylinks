<?php
if(is_user_loggedin()){
    $current_user = $_SESSION["current_user"];

    set_user_time_zone();
    create_user_constants();
    set_user_levels();
    set_user_menu_items();
    set_user_modules();

    $_SESSION["current_user"] = $current_user;
}else{
    if(function_exists("get_cookie") && !empty(get_cookie("el_username")) && !empty(get_cookie("el_password"))){
        $username = md5(get_cookie("el_username"));
        $password = md5(get_cookie("el_password"));

        $sql = "select * from ".$db->users." where md5(md5(user_login))='".$username."' and md5(user_pass)='".$password."'";
        $user = $db->get_row($sql);
        if($user){

            $current_user = $user;
            $_SESSION["current_user"] = $current_user;
            $_SESSION["user_loggedin"] = true;

            save_cookie("el_username", get_cookie("el_username"));
            save_cookie("el_password", get_cookie("el_password"));
            
            set_user_time_zone();
            create_user_constants();
            set_user_levels();
            set_user_menu_items();
            set_user_modules();

            $_SESSION["current_user"] = $current_user;
        }
    }
}

function create_user_constants(){
    global $current_user;

    if(!defined('USER_DIR'))
	    define('USER_DIR', MAIN_DIR.'uploads/users/'.$current_user->id."/");

    if(!defined('USER_URL'))
	    define('USER_URL', MAIN_URL.'uploads/users/'.$current_user->id."/");

    if(!file_exists(USER_DIR))
        mkdir(USER_DIR, 0777, true);
}

function set_user_time_zone(){
	global $current_user;

    if(isset($current_user->timezoneid) && !empty($current_user->timezoneid)){
        date_default_timezone_set($current_user->timezoneid);
    }
}

function set_user_levels(){
	global $current_user, $db;

    {//if(!isset($current_user->LevelIDs) || empty($current_user->LevelIDs)){
        $current_user->LevelIDs = $db->get_col("select LevelID from ".$db->user_levels." where UserID=".$current_user->id);
    }
}

function set_user_menu_items(){
	global $current_user;

    {//if(!isset($current_user->MenuItems) || empty($current_user->MenuItems)){
        $current_user->MenuItems = get_level_menu_items($current_user->LevelIDs);
    }
}

function set_user_modules(){
	global $current_user;

    {//if(!isset($current_user->Modules) || empty($current_user->Modules)){
        $current_user->Modules = get_level_modules($current_user->LevelIDs);
    }
}

function is_user_loggedin(){
    return (isset($_SESSION["user_loggedin"]) && $_SESSION["user_loggedin"] == true);
}

function get_level_menu_items($LevelIDs){
    global $db;

    $sql = "select * from ".$db->menu_items." inner join ".$db->level_menu_items." on ".$db->menu_items.".id=".$db->level_menu_items.".MenuItemID where LevelID in (".implode(",", $LevelIDs).") and MenuItemEnabled=1 and LevelMenuItemEnabled=1 and MenuItemHasAdmin=1 order by MenuItemPosition";
    $menu_items = $db->get_results($sql, ARRAY_A);

    return $menu_items;
}

function get_level_modules($LevelIDs){
    global $db;

    $sql = "select * from ".$db->modules." inner join ".$db->level_modules." on ".$db->modules.".id=".$db->level_modules.".ModuleID where LevelID in (".implode(",", $LevelIDs).") and ModuleEnabled=1 and LevelModuleEnabled=1";
    $modules = $db->get_results($sql, ARRAY_A);

    return $modules;
}

function is_menu_item_enabled($ModuleName){
    global $menu_items;
    $ModuleEnabled = false;
    foreach ($menu_items as $menu_item){
    	if($menu_item["ModuleName"] == $ModuleName && $menu_item["ModuleEnabled"] == 1){
            $ModuleEnabled = true;
            break;
        }
    }

    return $ModuleEnabled;
}

function is_module_enabled($ModuleName){
    global $modules;
    $ModuleEnabled = false;
    foreach ($modules as $module){
    	if(strtolower($module["ModuleName"]) == strtolower($ModuleName) && $module["LevelModuleEnabled"] == 1){
            $ModuleEnabled = true;
            break;
        }
    }

    return $ModuleEnabled;
}