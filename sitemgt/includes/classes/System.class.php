<?php
if(!class_exists("clsSystem")){
    class clsSystem{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        var $EncryptionType;
        var $EncryptionKey;
        function __construct(){
            $this->EncryptionType = "aes128";
            $this->EncryptionKey = ENCRYPTION_KEY;
        }

        function __destruct(){
            //
        }

        function now($ReturnTimeStamp = false){
            if($ReturnTimeStamp)
                return strtotime("now");
            else
                return date("Y-m-d h:i:s");
        }

        function site_config($keyName){
            return DB()->get_var("SELECT value FROM ".DB()->site_settings." WHERE site_key='".$keyName."'");
        }

        function ValidVideoExtensions(){
            return array("mov","mp4","f4v","flv","avi","webm","wmv","ogg");;
        }

        function EncryptData($DecryptedData){
            $EncryptedData = openssl_encrypt($this->maybe_serialize($DecryptedData), $this->EncryptionType, $this->EncryptionKey);
            return $EncryptedData;
        }

        function DecryptData($EncryptedData){
            $DecryptedData = $this->maybe_unserialize(openssl_decrypt($EncryptedData, $this->EncryptionType, $this->EncryptionKey));
            return $DecryptedData;
        }

        function maybe_serialize( $data ) {
            if ( is_array( $data ) || is_object( $data ) )
                return serialize( $data );

            if ( $this->is_serialized( $data, false ) )
                return @serialize( $data );

            return $data;
        }

        function maybe_unserialize( $original ) {
            if ( $this->is_serialized( $original ) )
                return @unserialize( $original );
            return $original;
        }

        function is_serialized( $data, $strict = true ) {
            if ( ! is_string( $data ) ) {
                return false;
            }
            $data = trim( $data );
            if ( 'N;' == $data ) {
                return true;
            }
            if ( strlen( $data ) < 4 ) {
                return false;
            }
            if ( ':' !== $data[1] ) {
                return false;
            }
            if ( $strict ) {
                $lastc = substr( $data, -1 );
                if ( ';' !== $lastc && '}' !== $lastc ) {
                    return false;
                }
            } else {
                $semicolon = strpos( $data, ';' );
                $brace     = strpos( $data, '}' );
                // Either ; or } must exist.
                if ( false === $semicolon && false === $brace )
                    return false;
                // But neither must be in the first X characters.
                if ( false !== $semicolon && $semicolon < 3 )
                    return false;
                if ( false !== $brace && $brace < 4 )
                    return false;
            }
            $token = $data[0];
            switch ( $token ) {
                case 's' :
                    if ( $strict ) {
                        if ( '"' !== substr( $data, -2, 1 ) ) {
                            return false;
                        }
                    } elseif ( false === strpos( $data, '"' ) ) {
                        return false;
                    }
                // or else fall through
                case 'a' :
                case 'O' :
                    return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
                case 'b' :
                case 'i' :
                case 'd' :
                    $end = $strict ? '$' : '';
                    return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
            }
            return false;
        }

        function get_site_url($path = "", $secure = false, $echo = false){
            $url = SITE_URL.ltrim($path, "/");

            if($secure && stripos($url,"http://") !== false){
                $url = str_ireplace("http://", "https://", $url);
            }

            if($echo){
                echo $url;
                return false;
            }else{
                return $url;
            }
        }

        function site_url($path = "", $secure = false){
            $this->get_site_url($path, $secure, true);
        }

        function get_include_uri($path = ""){
            $uri = rtrim(BASE_DIR, "/")."/".ltrim($path, "/");
            return $uri;
        }

        function parseInt($val){
            $intval = 0;
            if ($this->is_numeric($val)){
                $intval = intval($val);
            }
            return $intval;
        }

        function redirect($url, $http_response_code = 301){
            header("location: ".$url, true, $http_response_code);
            echo '<script>window.location.href="'.$url.'"</script>';
            die;
        }

        function site_redirect($path){
            $url = $this->get_site_url($path);
            $this->redirect($url);
            die;
        }

        function get_menuitem($MenuItemSearchValue, $MenuItemSearchKey = 'MenuItemURL'){
            global $LoggedInUser;

            $MenuItems = $LoggedInUser->MenuItems;
            $FilteredMenuItem = array_filter($MenuItems, function($MenuItem) use ($MenuItemSearchKey, $MenuItemSearchValue){
                return($MenuItem[$MenuItemSearchKey] == $MenuItemSearchValue);
            });
            sort($FilteredMenuItem);

            if($this->is_valid($FilteredMenuItem[0]))
                $MenuItem = $FilteredMenuItem[0];
            else
                $MenuItem = $MenuItems[0];

            return $MenuItem;
        }

        function get_random_rbg($colors = array()){
            do{
                $red = mt_rand( 0, 255);
                $green = mt_rand(0, 255);
                $blue = mt_rand(0, 255);

                $RGB = "rgba(".$red.",".$green.",".$blue.", 1.0)";

            }while(in_array($RGB, $colors));

            return $RGB;
        }

        function get_nearest_rbg($color){
            $RGB = str_replace("1.0", "0.7", $color);

            return $RGB;
        }

        function save_cookie($key, $value, $isSessionCookie = false){
            setcookie($key, $value, ($isSessionCookie ? 0 : time()+(60 * 60 * 24 * 7)), "/");
        }

        function remove_cookie($key){
            setcookie($key, "", time() - 3600, "/");
        }

        function get_cookie($key){
            $value = !$this->is_string($_COOKIE[$key]) ? "" : $_COOKIE[$key];
            return $value;
        }

        function is_field_valid($Value, $NumericField = false, $StringField = true){
            $isValid = isset($Value) && !empty($Value);// && (is_scalar($Value) && (string)$Value != "d41d8cd98f00b204e9800998ecf8427e");

            if($isValid && is_scalar($Value))
                $isValid = (string)$Value != "d41d8cd98f00b204e9800998ecf8427e";

            if($isValid && $NumericField)
                $isValid = is_numeric($Value);

            if($isValid && $StringField)
                $isValid = !is_numeric($Value) && is_string($Value) && is_scalar($Value);

            return $isValid;
        }

        function is_string($Value){
            return $this->is_field_valid($Value, false, true);
        }

        function is_numeric($Value){
            return $this->is_field_valid($Value, true, false);
        }

        function is_valid($Value){
            return $this->is_field_valid($Value, false, false);
        }

        function get_ipaddress_info($IpAddress){


            require_once "dbip.class.php";
            //$mysqlidb = mysqli_connect("groupengage.io", "groupengage_geouser", "eyBy@xcUMk)R", "groupengage_geodb");
            //mysqli_select_db($mysqlidb, "groupengage_geodb");

            //$mysqlidb = mysqli_connect(DB()->dbhost, DB()->dbuser, DB()->dbpassword, DB()->dbname);
            //mysqli_select_db($mysqlidb, DB()->dbname);

            $mysqlidb = mysqli_connect(GEO_DB_HOST, GEO_DB_USER, GEO_DB_PASSWORD, GEO_DB_NAME);
            mysqli_select_db($mysqlidb, GEO_DB_NAME);

            $dbip = new DBIP_MySQLI($mysqlidb);
            $IpAddressInfo = $dbip->Lookup(trim($IpAddress), "l2sun1el_ipaddress_info");//DB()->ipaddress_info
            return $IpAddressInfo;
        }

        function get_formated_number($number){
            //setlocale(LC_MONETARY, 'en_US');
            $number = intval($number) == floatval($number)?intval($number):$number;
            $FormatedNumber = number_format($number, is_float($number)?2:0);
            return $FormatedNumber;
        }

        function get_float_number($number){
            $number = intval($number) == floatval($number)?intval($number):$number;
            $float_number = round($number, is_float($number)?2:0);
            return $float_number;
        }

        function delete_file($file){
            if (!empty($file) && @is_file($file) && @file_exists($file)){
                @unlink($file);
            }
        }

        function get_url_status($url) {
            $agent = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; pt-pt) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_exec($ch);

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            echo $httpcode;

            if ($httpcode >= 200 && $httpcode < 300)
                return "true";
            else
                return "false";
        }

        function get_files($dir, $maindir, $include_sub_dir = true){
            $files = array();
            if ($handle = opendir($dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $filepath = $dir."/".$file;
                        if(is_file($filepath)){
                            $files[] = str_replace($maindir."/", "", $filepath);
                        }else if(is_dir($filepath) && $include_sub_dir){
                            $files = array_merge($files, $this->get_files($filepath, $maindir));
                        }
                    }
                }
                closedir($handle);
            }
            return $files;
        }

        function send_option_email($EmailTo, $SubjectOption, $BodyOption, $ReplaceValues = array()){
            $EmailSubject = $this->get_option($SubjectOption);
            $EmailMessage = $this->get_option($BodyOption);

            foreach ($ReplaceValues as $ReplaceKey => $ReplaceValue){
                $EmailSubject = preg_replace(array('/'.$ReplaceKey.'/'), array($ReplaceValue), $EmailSubject);
                $EmailMessage = preg_replace(array('/'.$ReplaceKey.'/'), array($ReplaceValue), $EmailMessage);
            }

            $this->send_mail($EmailTo, $EmailSubject, $EmailMessage);
        }

        function send_mail($EmailTo, $EmailSubject, $EmailMessage, $Emailfrom = "Team Video Geyser<no-reply@videogeyser.com>", $headers = ""){
            $headers .= "From: ".$Emailfrom."\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            return @mail($EmailTo, $EmailSubject, $EmailMessage, $headers);
        }

        function send_message($EmailTo, $EmailSubject, $EmailMessage, $Emailfrom = "Team Video Geyser<no-reply@videogeyser.com>"){
            $URL = 'https://api.mailgun.net/v2/channelauthoritybuilder.com/messages';
            $PostData = array(
                "MIME-Version"      => '1.0',
                "Content-type"      => "text/html; charset=iso-8859-1",
                'from'              => $Emailfrom,
                'to'                => $EmailTo,
                'subject'           => $EmailSubject,
                'html'              => $EmailMessage
            );
            return $this->execute_curl($URL, null, null, $PostData, CURLAUTH_BASIC, 'api:key-5wg2u60tl8-z0u6bx1qj51t6qeb26uy8');
        }

        function get_option($OptionName, $OptionDefault=null){


            $sql = "select OptionValue from ".DB()->options." where ";

            if($this->is_numeric($OptionName))
                $sql .= " id=".$OptionName;
            else
                $sql .= " OptionName='".$OptionName."'";

            $OptionValue = DB()->get_var($sql);

            if(!$OptionValue)
                $OptionValue = $OptionDefault;

            return $OptionValue;
        }

        function get_today_dates(){
            $dtNow = $this->now(true);
            $dtStart = new DateTime(date('m/d/Y 00:00:00', $dtNow));
            $dtEnd = new DateTime(date('m/d/Y 23:59:59', $dtNow));
            //$dtEnd = new DateTime(date('m/d/Y 00:00:00', $dtNow));
            //$dtEnd->modify("+1 day");
            return array(
                "start_date_time"    =>  strtotime($dtStart->format("r")),
                "end_date_time"    =>  strtotime($dtEnd->format("r")),
                "start_date"    =>  $dtStart->format("r"),
                "end_date"    =>  $dtEnd->format("r"),
                "start_date_only"    =>  $dtStart->format("m/d/Y"),
                "end_date_only"    =>  $dtEnd->format("m/d/Y")
            );
        }

        function get_last_seven_days(){
            $dtNow = $this->now(true);
            $dtStart = new DateTime(date('m/d/Y 00:00:00', $dtNow));
            $dtEnd = new DateTime(date('m/d/Y 23:59:59', $dtNow));
            //$dtEnd = new DateTime(date('m/d/Y 00:00:00', $dtNow));

            $dtStart->modify("-6 days");
            //$dtEnd->modify("+1 day");
            return array(
                "start_date_time"    =>  strtotime($dtStart->format("r")),
                "end_date_time"    =>  strtotime($dtEnd->format("r")),
                "start_date"    =>  $dtStart->format("r"),
                "end_date"    =>  $dtEnd->format("r"),
                "start_date_only"    =>  $dtStart->format("m/d/Y"),
                "end_date_only"    =>  $dtEnd->format("m/d/Y")
            );
        }

        function print_r($obj, $die = true){
            echo "<pre>".print_r($obj, true)."</pre>";
            if($die)
                die;
        }

        function save_error($ErrorData, $ErrorMessage, $from, $userid){

            $data = array(
                "errordata"                     => $ErrorData,
                "errormessage"                  => $ErrorMessage,
                "errorfrom"                     => $from,
                "userid"                        => $userid,
                "created_date_time"             => $this->now(true),
            );
            DB()->insert(DB()->user_errors_log, $data);
        }

        function get_excerpt($str, $startPos=0, $maxLength=100) {
            if(strlen($str) > $maxLength) {
                $excerpt   = substr($str, $startPos, $maxLength-3);
                $lastSpace = strrpos($excerpt, ' ');
                $excerpt   = substr($excerpt, 0, $lastSpace);
                $excerpt  .= '...';
            } else {
                $excerpt = $str;
            }

            return $excerpt;
        }

        function split_numbers_equally($NumbersLength, $TotalParts = 7){
            $BaseNumber = $NumbersLength / $TotalParts;
            $EqualNumbersList = array();
            for($index = 1; $index <= $TotalParts; $index++) {
                $EqualNumbersList[] = round($index * $BaseNumber);
            }

            return $EqualNumbersList;
        }

        function split_dates_equally($StartDate, $EndDate, $TotalParts = 7, $DateFormat = "Y-m-d") {
            $EqualDatesList = array();
            if($this->is_string($StartDate))
                $StartDate = strtotime($StartDate);

            if($this->is_string($EndDate))
                $EndDate = strtotime($EndDate);

            $DatesDifference = round(($EndDate - $StartDate) / $TotalParts);//echo '$DatesDifference'.$DatesDifference;die;
            for ($index = 1; $index <= $TotalParts; $index++) {
                if($index == 1){
                    $EqualDatesList[$StartDate] = date($DateFormat, $StartDate);
                } else if($index === $TotalParts){
                    $EqualDatesList[$EndDate] = date($DateFormat, $EndDate);
                } else {
                    $LastDate = end($EqualDatesList);
                    $DateTimeString = strtotime($LastDate) + $DatesDifference;
                    $EqualDatesList[] = date($DateFormat, $DateTimeString);
                }
            }

            return $EqualDatesList;
        }

        function split_dates_equall_days($StartDate, $EndDate, $TotalParts = 7, $DateFormat = "Y-m-d") {
            $EqualDaysList = array();
            $TotalDays = $this->days_between_dates($StartDate, $EndDate);
            $SecondsInOneDay = (60 * 60 * 24);

            if($TotalDays < $TotalParts){
                $DaysDifference = $TotalParts - $TotalDays - 1;
                $TotalDays = $TotalParts;
                $StartDate -= ($SecondsInOneDay * $DaysDifference);
            }

            if($TotalDays >= $TotalParts){
                for($index = 1; $index <= $TotalParts; $index++) {
                    if($index == 1){
                        $EqualDaysList[$StartDate] = date($DateFormat, $StartDate);
                    } else if($index === $TotalParts){
                        $EqualDaysList[$EndDate] = date($DateFormat, $EndDate);
                    } else {
                        $LastDate = end($EqualDaysList);
                        $DateTimeString = strtotime($LastDate) + $SecondsInOneDay;
                        $EqualDaysList[$DateTimeString] = date($DateFormat, $DateTimeString);
                    }
                }
            }

            return $EqualDaysList;
        }

        function days_between_dates($FirstPostUpdateDateTime, $LastPostUpdateDateTime){
            if($this->is_string($FirstPostUpdateDateTime))
                $FirstPostUpdateDateTime = strtotime($FirstPostUpdateDateTime);

            if($this->is_string($LastPostUpdateDateTime))
                $LastPostUpdateDateTime = strtotime($LastPostUpdateDateTime);

            $TimeBetweenFirstAndLastPost = abs($LastPostUpdateDateTime - $FirstPostUpdateDateTime);
            $DaysBetweenFirstAndLastPost = $TimeBetweenFirstAndLastPost / (60 * 60 * 24);  // 86400 seconds in one day
            $DaysBetweenFirstAndLastPost = intval($DaysBetweenFirstAndLastPost);
            return $DaysBetweenFirstAndLastPost;
        }

        function get_previous_date($strDateTime, $DaysInHistory = -1, $ReturnDateTime = true){
            $dtPreviousDate = new DateTime($strDateTime);
            $dtPreviousDate->setTime(0,0,0);
            $dtPreviousDate->modify("-".($DaysInHistory > -1 ? $DaysInHistory : 9999)." days");
            $tsPreviousDate = strtotime($dtPreviousDate->format("r"));

            if($ReturnDateTime)
                return $dtPreviousDate;
            else
                return $tsPreviousDate;
        }

        function parse_date_time($string, $timezone=null) {
            $date = new DateTime(
              $string,
              $timezone ? $timezone : new DateTimeZone('UTC')
                // Used only when the string is ambiguous.
                // Ignored if string has a timezone offset in it.
            );
            if ($timezone) {
                // If our timezone was ignored above, force it.
                $date->setTimezone($timezone);
            }
            return $date;
        }

        function strip_time($datetime) {
            return new DateTime($datetime->format('Y-m-d'));
        }

        function create_pagination($ElementID, $TotalRecords, $VisiblePages = 7, $PageVariable = "pageno", $RecordsPerPageVariable = "perpage", $HideSinglePage = "true", $CreateLink = "true"){
            $RecordsPerPage = $this->is_numeric($_GET[$RecordsPerPageVariable]) ? $_GET[$RecordsPerPageVariable] : 10;
            $CurrentPage = $this->is_numeric($_GET[$PageVariable]) ? $_GET[$PageVariable] : 1;

            if($RecordsPerPage == "all"){
                $TotalPages = 1;
            } else {
                $TotalPages = round($TotalRecords / $RecordsPerPage);
                if(($TotalPages * $RecordsPerPage) < $TotalRecords)
                    $TotalPages++;
            }

            if($RecordsPerPage == "all"){
                $EndIndex = $TotalRecords;
                $StartIndex = 1;
            } else {
                $EndIndex = $CurrentPage * $RecordsPerPage;
                $StartIndex = $EndIndex - $RecordsPerPage + 1;
                if($EndIndex > $TotalRecords)
                    $EndIndex = $TotalRecords;
            }

            $PaginationHTML =   '<div id="'.$ElementID.'_wrapper" class="well well-sm navigation_wrapper clearfix">'.
                                    '<span class="pagination-info">Showing '.$StartIndex.' to '.$EndIndex.' of '.$TotalRecords.' entries</span>'.
                                    '<div class="pagination-controls"><ul id="'.$ElementID.'" class="pagination pagination-info twbsPagination" data-total-pages="'.$TotalPages.'" data-page-variable="'.$PageVariable.'" data-visible-pages="'.$VisiblePages.'" data-start-page="'.$CurrentPage.'" data-hide-only-one-page="'.$HideSinglePage.'" data-href="'.$CreateLink.'"></ul></div>'.
                                    '<span class="pagination-page-no">'.
                                        '<select name="'.$RecordsPerPageVariable.'" id="'.$RecordsPerPageVariable.'" class="selectpicker pagination-records-per-page" data-page-variable="'.$PageVariable.'" data-style="btn btn-rose btn-round btn-sm" data-select="select" data-live-search="true">'.
                                            '<option value="10" '.($RecordsPerPage == "10" ? ' selected=" selected" ' : "").'>10</option>'.
                                            '<option value="20" '.($RecordsPerPage == "20" ? ' selected=" selected" ' : "").'>20</option>'.
                                            '<option value="30" '.($RecordsPerPage == "30" ? ' selected=" selected" ' : "").'>30</option>'.
                                            '<option value="40" '.($RecordsPerPage == "40" ? ' selected=" selected" ' : "").'>40</option>'.
                                            '<option value="50" '.($RecordsPerPage == "50" ? ' selected=" selected" ' : "").'>50</option>'.
                                            '<option value="all" '.($RecordsPerPage == "all" ? ' selected=" selected" ' : "").'>All</option>'.
                                        '</select>'.
                                    '</span>'.
                                '</div>';
            echo $PaginationHTML;
        }

        function find($Content, $SearchStartString, $SearchEndString, $DefaultContent = ""){
            $NewContent = $DefaultContent;
            $StartIndex = stripos($Content, $SearchStartString);
            $EndIndex = stripos($Content, $SearchEndString, $StartIndex);
            if($StartIndex !== false && $EndIndex !== false){
                $NewContent = substr($Content, $StartIndex, $EndIndex - $StartIndex + strlen($SearchEndString));
            }
            return $NewContent;
        }

        function replace($Content, $ReplacedContent, $SearchStartString, $SearchEndString){
            $NewContent = $Content;
            $StartIndex = stripos($NewContent, $SearchStartString);
            $EndIndex = stripos($NewContent, $SearchEndString, $StartIndex);
            if($StartIndex !== false && $EndIndex !== false){
                $NewContent = substr_replace($NewContent, $ReplacedContent, $StartIndex, $EndIndex - $StartIndex + strlen($SearchEndString));
            }
            return $NewContent;
        }

        function get_initials($String, $Delimiter = " ", $LettersCount = 1, $ToUpperCase = true) {
            $Initials = '';
            foreach (explode($Delimiter, $String) as $Word){
                $Initial = "";
                for ($LettersIndex = 0; $LettersIndex < $LettersCount; $LettersIndex++){
                	if($this->is_valid($Word[$LettersIndex]))
                        $Initial .= $Word[$LettersIndex];
                    else
                        break;
                }
                $Initials .= $ToUpperCase ? strtoupper($Initial) : $Initial;
            }
            return $Initials;
        }

        function execute_curl($URL, $ExtraHeaders = NULL, $Cookies = NULL, $PostData = NULL, $AuthType = "", $AuthPassword){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, $ExtraHeaders);
            curl_setopt($ch, CURLOPT_NOBODY, $ExtraHeaders);
            curl_setopt($ch, CURLOPT_URL, $URL);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_COOKIE, $Cookies);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            if($this->is_valid($AuthType) && $this->is_valid($AuthPassword)){
                curl_setopt($ch, CURLOPT_HTTPAUTH, $AuthType);
                curl_setopt($ch, CURLOPT_USERPWD, $AuthPassword);
            }

            if ($PostData) {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);
            }

            $result = curl_exec($ch);

            if (!$result) {
                $result = curl_error($ch);
            }

            curl_close($ch);

            return $result;
        }

        function get_url_cookies($URL, $PostData){
            $Cookies = "";
            $a = $this->execute_curl($URL,true,null,$PostData);
            preg_match('%Set-Cookie: ([^;]+);%',$a,$b);

            $c = $this->execute_curl($URL,true,$b[1],$PostData);
            preg_match_all('%Set-Cookie: ([^;]+);%',$c,$d);

            for($i=0;$i<count($d[0]);$i++)
                $Cookies.=$d[1][$i].";";

            return $Cookies;
        }

        function remove_shortcode($content){
            global $SystemUser;
            $ShortcodeStart = '{shortcode-';
            $ShortcodeEnd= '}';

            if(stripos($content, $ShortcodeStart) !== false){
                preg_match('/' . preg_quote($ShortcodeStart) .'.*?' .preg_quote($ShortcodeEnd) . '/', $content, $MatchedShortcodes);
                foreach ($MatchedShortcodes as $MatchedShortcode){
                    $Shortcode = str_replace($ShortcodeStart, "", $MatchedShortcode);
                    $Shortcode = str_replace($ShortcodeEnd, "", $Shortcode);
                    $ShortcodeContent = "";
                    $content = preg_replace('/' . preg_quote($ShortcodeStart) .'.*?' .preg_quote($ShortcodeEnd) . '/', $ShortcodeContent, $content);
                }
            }

            return $content;
        }

        function GenerateRandomPassword($PasswordLength = 12, $LowerCaseCharacters = 'abcdefghijklmnopqrstuvwxyz', $UpperCaseCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', $Numbers = '1234567890', $SpecialSymbols = '!?~@#-_+<>[]{}'){
            $CharactersSet = $LowerCaseCharacters.$UpperCaseCharacters.$Numbers.$SpecialSymbols;
            $CharactersSetLength = strlen($CharactersSet) - 1;

            $RandomPassword = '';
            for ($PasswordCharecterIndex = 0; $PasswordCharecterIndex < $PasswordLength; $PasswordCharecterIndex++) {
                $CharecterSetIndex = rand(0, $CharactersSetLength);
                $RandomPassword .= $CharactersSet[$CharecterSetIndex];
            }

            return $RandomPassword;
        }

        function GetSortInfo($SortColumns, $DefaultIndex = 0){
            $SortColumnIndex = $this->is_string($_GET["sortby"]) ? array_search($_GET["sortby"], $SortColumns) : $DefaultIndex;
            $SortColumn = $SortColumns[$SortColumnIndex];
            $SortDirection = $this->is_string($_GET["sortdirection"]) ? $_GET["sortdirection"] : "asc";
            return array($SortColumns, $SortColumnIndex, $SortColumn, $SortDirection);
        }

        function GetPageInfo(){
            $PerPage = $this->is_numeric($_GET["perpage"]) ? intval($_GET["perpage"]) : ($_GET["perpage"] == "all" ? -1 : 10);
            $PageNo = $this->is_numeric($_GET["pageno"]) ? intval($_GET["pageno"]) : 1;
            return array($PerPage, $PageNo);
        }

        function get_time_zones(){
            $zones = timezone_identifiers_list();
            $Timezones = array();
            foreach ($zones as $zone) {
                $zone = explode('/', $zone);
                if ($this->is_string($zone[1])){
                    $zonevalue = $zone[0]. '/' . $zone[1];
                    $zonename = str_replace('_', ' ', $zone[0])." - ".str_replace('_', ' ', $zone[1]);
                } else {
                    $zonevalue = $zone[0];
                    $zonename = str_replace('_', ' ', $zone[0]);
                }

                $Timezones[] = (object)array(
                    "id"        => $zonevalue,
                    "name"      => $zonename
                );
            }

            return $Timezones;
        }

        function read_csv_file($InputCSVFile, $FirstLineIsHeader=true, $delimiter=",", $enclosure='"', $rowlength=0) {
            $CSVData = array();
            $HeaderRow = array();

            if (file_exists($InputCSVFile)) {
                $ReadHandle = @fopen($InputCSVFile, 'r');
                if ($FirstLineIsHeader === true) {
                    $HeaderRowData = fgetcsv($ReadHandle, $rowlength, $delimiter, $enclosure);//print_r($HeaderRowData);die;
                    foreach ($HeaderRowData as $HeaderFieldIndex => $HeaderFieldName) {
                        if($this->is_string($HeaderFieldName)){
                            $HeaderRow[$HeaderFieldIndex] = str_replace(array(" "), array("_"), $HeaderFieldName);
                        }
                    }
                }

                while($CSVLine = fgetcsv($ReadHandle, $rowlength, $delimiter, $enclosure)) {
                    if ($FirstLineIsHeader === true) {
                        $CSVRow = array();
                        foreach ($HeaderRow as $HeaderFieldIndex => $HeaderFieldName) {
                            if ($this->is_valid($HeaderFieldName)) {
                                $CSVRow[$HeaderFieldName] = ($this->is_string($CSVLine[$HeaderFieldIndex])) ? $CSVLine[$HeaderFieldIndex] : '';
                            }
                        }
                        if ($this->is_valid($CSVRow)) {
                            $CSVData[] = $CSVRow;
                        }
                    } else {
                        if ($this->is_valid($CSVLine)) {
                            $CSVData[] = $CSVLine;
                        }
                    }
                }
                @fclose($ReadHandle);
            }
            return $CSVData;
        }

        function get_access_key($TableName, $FieldName, $AccessKeyToCheck){
            $NewAccessKey = $AccessKeyToCheck."-".time();
            $NextAccessKeyNo = 1;
            do{
                $AccessKey = $NewAccessKey;
                $AccessKeyFound = DB()->get_var("select count(*) from ".$TableName." where ".$FieldName."='".md5($NewAccessKey)."'");
                $NewAccessKey = $AccessKeyToCheck."-".$NextAccessKeyNo."-".time();
                $NextAccessKeyNo++;
            }while($AccessKeyFound > 0);
            return md5($AccessKey);
        }

        function display_message($MessageToDisplay, $MessageType = "warning", $MessageIcon = "warning", $CustomClasses = '', $OnlyMessage = false, $ReturnMessage = false){
            ob_start();

            if(!$OnlyMessage){
?>
<div class="card">
    <div class="card-content">
        <?php } ?>
        <div data-notify="container" class="alert <?php echo $CustomClasses;?> alert-<?php echo $MessageType;?> alert-with-icon">
            <button data-dismiss="alert" class="close" type="button">x</button>
            <i class="fa fa-<?php echo $MessageIcon;?>" data-notify="icon"></i>
            <span data-notify="message">
                <?php echo $MessageToDisplay;?>
            </span>
        </div>
        <?php if(!$OnlyMessage) { ?>
    </div>
</div>
<?php
              }

              $MessageBody = ob_get_contents();
              ob_end_clean();

              if(!$ReturnMessage)
                  echo $MessageBody;

              return $MessageBody;
        }

        function display_page_alert($errors){
            $DisplayPageAlert = "false";
            $HideAlert = "true";
            $PageAlert = "";

            if (($this->is_valid($_GET['success']) && $this->is_valid($_GET["message"])) ||
                    ($this->is_valid($errors) && $this->is_valid($errors['success']) && $this->is_valid($errors["message"]))){
                $isErrorMessage = ($this->is_valid($_GET['success']) && $_GET['success'] == "false") || ($this->is_valid($errors['success']) && $errors['success'] == false);
                $MessageToDisplay = $this->is_valid($_GET["message"])?$_GET["message"]:$errors["message"];
                $MessageType = $isErrorMessage ? "danger" : "success";
                $MessageIcon = $isErrorMessage ? "exclamation-circle" : "bell";
                $HideAlert = $this->is_valid($_GET["nohidealert"]) || $this->is_valid($errors["nohidealert"]) ? "false" : "true";
                $DisplayPageAlert = "true";
                $PageAlert = $this->display_message($MessageToDisplay, $MessageType, $MessageIcon, 'alert-message alert-dismissable', true, true);
            }

            return array($DisplayPageAlert, $HideAlert, $PageAlert);
        }

        function sanitize_values($Values, $EscapeValues = true) {
            if (is_array($Values)) {
                foreach ($Values as $Key => $Value) {
                    $Values[$Key] = str_replace("\r\n", " ", $Value);
                }
            } else {
                $Values = str_replace("\r\n", " ", $Values);
            }
            if($EscapeValues)
                $Values = DB()->_real_escape(json_encode($Values, JSON_HEX_APOS));
            return $Values;
        }

        public function download_file_c($FileURL, $FilePath) {
            $FileDownloaded= false;

            try{
                $ReadCURL = curl_init();
                curl_setopt($ReadCURL, CURLOPT_URL, str_replace(" ","%20", $FileURL));
                curl_setopt($ReadCURL, CURLOPT_RETURNTRANSFER, 1);
                //curl_setopt($ReadCURL, CURLOPT_SSLVERSION,3);
                curl_setopt($ReadCURL, CURLOPT_SSL_VERIFYPEER,false);

                $ReadData = curl_exec ($ReadCURL);
                $error = curl_error($ReadCURL);
                curl_close ($ReadCURL);$this->print_r($ReadData);die;

                if($ReadData){
                    $WriteFileStream = fopen($FilePath, "w+");
                    fputs($WriteFileStream, $ReadData);
                    fclose($WriteFileStream);
                    $FileDownloaded= true;
                }
            }
            catch(Exception $ex){
                $this->print_r($ex);
            }

            return $FileDownloaded;
        }

        public function download_file($FileURL, $FilePath) {
            $FileDownloaded= false;

            try{
                //echo 'php_ini_loaded_file: '.php_ini_loaded_file()."<br />";
                //echo 'allow_url_fopen : ', ini_get('allow_url_fopen') ? 'Enabled' : 'Disabled';die;

                $ReadFileStream = fopen (str_replace(" ","%20", $FileURL), 'rb');
                if ($ReadFileStream) {
                    $WriteFileStream = fopen ($FilePath, 'wb');
                    if ($WriteFileStream) {
                        while(!feof($ReadFileStream)) {
                            fwrite($WriteFileStream, fread($ReadFileStream, 1024 * 8), 1024 * 8);
                        }
                        $FileDownloaded= true;
                    }
                    if ($WriteFileStream)
                        fclose($WriteFileStream);
                }

                if ($ReadFileStream)
                    fclose($ReadFileStream);
            }
            catch(Exception $ex){
                //$this->print_r($ex);
            }

            return $FileDownloaded;
        }

        public function upload_file($UserID) {
            $UploadResponse = array();

            try{
                $FileName = "";
                if (isset($_POST["name"])) {
                    $FileName = $_POST["name"];
                } else if (!empty($_FILES)) {
                    $FileName = $_FILES["file"]["name"];
                }

                if($FileName != ""){
                    $FilePath = USER_DIR.$FileName;
                    $TempFileName = $_FILES["file"]["tmp_name"];
                    $CurrentChunkNo = isset($_POST["chunk"]) ? intval($_POST["chunk"]) : 0;
                    $TotalChunks = isset($_POST["chunks"]) ? intval($_POST["chunks"]) : 0;

                    // Open temp file
                    if (!$WriteFileStream = @fopen("{$FilePath}.part", $TotalChunks ? "ab" : "wb")) {
                        $UploadResponse["jsonrpc"] = "2.0";
                        $UploadResponse["id"] = "id";
                        $UploadResponse["error"] = array(
                            "code" => 102,
                            "message" => "Failed to open output stream."
                        );
                    }

                    if (!empty($_FILES)) {
                        if ($_FILES["file"]["error"] || !is_uploaded_file($TempFileName)) {
                            $UploadResponse["jsonrpc"] = "2.0";
                            $UploadResponse["id"] = "id";
                            $UploadResponse["error"] = array(
                                "code" => 103,
                                "message" => "Failed to move uploaded file"
                            );
                        }

                        // Read binary input stream and append it to temp file
                        if (!$ReadFileStream = @fopen($TempFileName, "rb")) {
                            $UploadResponse["jsonrpc"] = "2.0";
                            $UploadResponse["id"] = "id";
                            $UploadResponse["error"] = array(
                                "code" => 101,
                                "message" => "Failed to open input stream"
                            );
                        }
                    } else {
                        if (!$ReadFileStream = @fopen("php://input", "rb")) {
                            $UploadResponse["jsonrpc"] = "2.0";
                            $UploadResponse["id"] = "id";
                            $UploadResponse["error"] = array(
                                "code" => 101,
                                "message" => "Failed to open input stream"
                            );
                        }
                    }

                    while ($ReadBuffer = fread($ReadFileStream, 4096)) {
                        fwrite($WriteFileStream, $ReadBuffer);
                    }

                    @fclose($WriteFileStream);
                    @fclose($ReadFileStream);

                    if (!$TotalChunks || $CurrentChunkNo == $TotalChunks - 1) {
                        rename("{$FilePath}.part", $FilePath);
                        $UploadResponse["jsonrpc"] = "2.0";
                        $UploadResponse["id"] = "id";
                        $UploadResponse["FilePath"] = $FilePath;
                        $UploadResponse["finished"] = true;
                    }
                }
            }
            catch(Exception $ex){
                //$this->print_r($ex);
            }

            return $UploadResponse;
        }

        function create_user_upload_dir($UserID){
            if(!defined('USER_DIR'))
                define('USER_DIR', BASE_DIR.'uploads/users/'.$UserID."/");

            if(!defined('USER_URL'))
                define('USER_URL', SITE_URL.'uploads/users/'.$UserID."/");

            if(!file_exists(USER_DIR))
                mkdir(USER_DIR, 0777, true);
        }

        function is_valid_video_file($VideoFile){
            if(stripos($VideoFile, "?") !== false)
                $VideoFile = substr($VideoFile, 0, stripos($VideoFile, "?"));

            $Extension = stripos($VideoFile, ".") !== false ? substr($VideoFile, strripos($VideoFile, ".") + 1) : "";

            return !empty($Extension) && in_array(strtolower($Extension), $this->ValidVideoExtensions());
        }

        function check_keywords_exist($Keywords, $TextString){
            $KeywordExist = false;
            foreach ($Keywords as $Keyword){
                $Keyword = trim($Keyword);
                if(!empty($Keyword)){
                    $Keyword = "#".ltrim($Keyword, "#");
                    if(stripos($TextString, $Keyword) !== false){
                        $KeywordExist = true;
                        break;
                    }
                }
            }
            return $KeywordExist;
        }

        function get_bread_crumbs($FolderName, $BaseURL){
            $BreadCrumbsHTML = "";
            if(isset($FolderName)){
                $LastFolder = "";
                ob_start();
?>
<ul class="breadcrumb">
    <li>
        <a href="<?php echo $this->get_site_url($BaseURL);?>">All</a>
    </li>
    <?php
                $Folders = explode("/", $FolderName);
                for($FolderIndex = 0; $FolderIndex < count($Folders) - 1; $FolderIndex++){
                    $Folder = $Folders[$FolderIndex];
                    if(!empty(trim($Folder))){
                        $LastFolder .= "/".$Folder;
	?>
    <li>
        <a href="<?php echo $this->get_site_url($BaseURL);?>">
            <?php echo ucwords(ltrim($Folder, "/"));?>
        </a>
    </li>
    <?php
                    }
                }
	?>
    <li>
        <span>
            <?php echo ucwords(ltrim($FolderName, "/"));?>
        </span>
    </li>
</ul>
<?php
                $BreadCrumbsHTML = ob_get_contents();
                ob_end_clean();
            }

            return $BreadCrumbsHTML;
        }

        function json_encode($jsonData, $echo = true, $addHeader = true, $die= true){
            $jsonString = json_encode($jsonData);

            if($addHeader) header('Content-Type: application/json');
            if($echo) echo $jsonString;
            if($die) die;

            return $jsonString;
        }

        /**
         * Decodes a JSON string
         * Takes a JSON encoded string and converts it into a PHP variable.
         *
         * @param string $json The json  string being decoded.  This function only works with UTF-8 encoded strings.  Note : PHP implements a superset of JSON as specified in the original ��RFC 7159 .
         * @param bool $assoc When TRUE , returned object s will be converted into associative array s.
         * @param int $depth User specified recursion depth.
         * @param int $options Bitmask of JSON decode options. Currently there are two supported options. The first is JSON_BIGINT_AS_STRING that allows casting big integers to string instead of floats which is the default. The second option is JSON_OBJECT_AS_ARRAY that has the same effect as setting assoc to TRUE .
         *
         * @return mixed
         */
        function json_decode($jsonString, $assoc = false, $depth = 512, $options = 0){
            $jsonData = $assoc ? array() : null;

            if($jsonString && $this->is_string($jsonString)){
                try{
                    do{
                        $jsonData = json_decode($jsonString, $assoc, $depth, $options);
                        if($this->is_string($jsonData))
                            $jsonString = $jsonData;
                    } while(is_string($jsonData) && json_last_error == JSON_ERROR_NONE);
                }
                catch (Exception $ex){
                    //
                }
            }

            return $jsonData;
        }
    }
}

global $sys;
if(class_exists("clsSystem") && !$vgMain){
    $vgMain = new clsSystem();
}

if(!function_exists("SYS")){
    function SYS() {
        return clsSystem::instance();
    }
}
$GLOBALS['SYS'] = SYS();
