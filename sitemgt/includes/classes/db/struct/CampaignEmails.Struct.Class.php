<?php
if(!class_exists('clsStructCampaignEmails')){
	class clsStructCampaignEmails{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $CampaignID;
		
		/**
		* @var integer
		*/
		var $AutoresponderID;
		
		/**
		* @var string
		*/
		var $EmailSubject;
		
		/**
		* @var string
		*/
		var $EmailBody;
		
		/**
		* @var integer
		*/
		var $SendTime;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var integer
		*/
		var $SetWidth;
		
		/**
		* @var integer
		*/
		var $isEnabled;
		
		/**
		* @var integer
		*/
		var $EmailTypeID;
		
		/**
		* @var integer
		*/
		var $isCopied;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_campaign_emails.*';
			$this->id = 'l2sun1el_campaign_emails.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_campaign_emails.userid';
			$this->CampaignID = 'l2sun1el_campaign_emails.CampaignID';
			$this->AutoresponderID = 'l2sun1el_campaign_emails.AutoresponderID';
			$this->EmailSubject = 'l2sun1el_campaign_emails.EmailSubject';
			$this->EmailBody = 'l2sun1el_campaign_emails.EmailBody';
			$this->SendTime = 'l2sun1el_campaign_emails.SendTime';
			$this->DateAdded = 'l2sun1el_campaign_emails.DateAdded';
			$this->LinkBankID = 'l2sun1el_campaign_emails.LinkBankID';
			$this->SetWidth = 'l2sun1el_campaign_emails.SetWidth';
			$this->isEnabled = 'l2sun1el_campaign_emails.isEnabled';
			$this->EmailTypeID = 'l2sun1el_campaign_emails.EmailTypeID';
			$this->isCopied = 'l2sun1el_campaign_emails.isCopied';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_campaign_emails';
		}
	}
}