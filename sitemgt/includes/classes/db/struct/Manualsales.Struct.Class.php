<?php
if(!class_exists('clsStructManualsales')){
	class clsStructManualsales{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $ManualSaleName;
		
		/**
		* @var string
		*/
		var $ManualSaleAccessKey;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var integer
		*/
		var $isSplitPartnerSale;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsales.*';
			$this->id = 'l2sun1el_manualsales.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ManualSaleName = 'l2sun1el_manualsales.ManualSaleName';
			$this->ManualSaleAccessKey = 'l2sun1el_manualsales.ManualSaleAccessKey';
			$this->MasterCampaignID = 'l2sun1el_manualsales.MasterCampaignID';
			$this->LinkBankID = 'l2sun1el_manualsales.LinkBankID';
			$this->isSplitPartnerSale = 'l2sun1el_manualsales.isSplitPartnerSale';
			$this->DateAdded = 'l2sun1el_manualsales.DateAdded';
			$this->userid = 'l2sun1el_manualsales.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsales';
		}
	}
}