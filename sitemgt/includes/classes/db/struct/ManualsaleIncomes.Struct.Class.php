<?php
if(!class_exists('clsStructManualsaleIncomes')){
	class clsStructManualsaleIncomes{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $ManualSaleSplitPartnerID;
		
		/**
		* @var string
		*/
		var $IncomeName;
		
		/**
		* @var string
		*/
		var $IncomeNote;
		
		/**
		* @var string
		*/
		var $IncomeAmount;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsale_incomes.*';
			$this->id = 'l2sun1el_manualsale_incomes.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ManualSaleSplitPartnerID = 'l2sun1el_manualsale_incomes.ManualSaleSplitPartnerID';
			$this->IncomeName = 'l2sun1el_manualsale_incomes.IncomeName';
			$this->IncomeNote = 'l2sun1el_manualsale_incomes.IncomeNote';
			$this->IncomeAmount = 'l2sun1el_manualsale_incomes.IncomeAmount';
			$this->DateAdded = 'l2sun1el_manualsale_incomes.DateAdded';
			$this->userid = 'l2sun1el_manualsale_incomes.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsale_incomes';
		}
	}
}