<?php
if(!class_exists('clsStructPaidtrafficConversionPixels')){
	class clsStructPaidtrafficConversionPixels{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $PaidTrafficID;
		
		/**
		* @var string
		*/
		var $ConversionPixelType;
		
		/**
		* @var string
		*/
		var $ConversionPixelName;
		
		/**
		* @var string
		*/
		var $ConversionPixelURL;
		
		/**
		* @var string
		*/
		var $ConversionPixelUnitPrice;
		
		/**
		* @var string
		*/
		var $ConversionPixelTrackingCode;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_paidtraffic_conversion_pixels.*';
			$this->id = 'l2sun1el_paidtraffic_conversion_pixels.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->PaidTrafficID = 'l2sun1el_paidtraffic_conversion_pixels.PaidTrafficID';
			$this->ConversionPixelType = 'l2sun1el_paidtraffic_conversion_pixels.ConversionPixelType';
			$this->ConversionPixelName = 'l2sun1el_paidtraffic_conversion_pixels.ConversionPixelName';
			$this->ConversionPixelURL = 'l2sun1el_paidtraffic_conversion_pixels.ConversionPixelURL';
			$this->ConversionPixelUnitPrice = 'l2sun1el_paidtraffic_conversion_pixels.ConversionPixelUnitPrice';
			$this->ConversionPixelTrackingCode = 'l2sun1el_paidtraffic_conversion_pixels.ConversionPixelTrackingCode';
			$this->DateAdded = 'l2sun1el_paidtraffic_conversion_pixels.DateAdded';
			$this->userid = 'l2sun1el_paidtraffic_conversion_pixels.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_paidtraffic_conversion_pixels';
		}
	}
}