<?php
if(!class_exists('clsStructManualsaleOtherIncomes')){
	class clsStructManualsaleOtherIncomes{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $ManualSaleSplitPartnerID;
		
		/**
		* @var string
		*/
		var $OtherIncomeName;
		
		/**
		* @var string
		*/
		var $OtherIncomeNote;
		
		/**
		* @var string
		*/
		var $OtherIncomeAmount;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsale_other_incomes.*';
			$this->id = 'l2sun1el_manualsale_other_incomes.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ManualSaleSplitPartnerID = 'l2sun1el_manualsale_other_incomes.ManualSaleSplitPartnerID';
			$this->OtherIncomeName = 'l2sun1el_manualsale_other_incomes.OtherIncomeName';
			$this->OtherIncomeNote = 'l2sun1el_manualsale_other_incomes.OtherIncomeNote';
			$this->OtherIncomeAmount = 'l2sun1el_manualsale_other_incomes.OtherIncomeAmount';
			$this->DateAdded = 'l2sun1el_manualsale_other_incomes.DateAdded';
			$this->userid = 'l2sun1el_manualsale_other_incomes.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsale_other_incomes';
		}
	}
}