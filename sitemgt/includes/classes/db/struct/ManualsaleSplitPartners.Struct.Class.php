<?php
if(!class_exists('clsStructManualsaleSplitPartners')){
	class clsStructManualsaleSplitPartners{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $ManualSaleID;
		
		/**
		* @var integer
		*/
		var $SplitPartnerID;
		
		/**
		* @var integer
		*/
		var $isUserSale;
		
		/**
		* @var string
		*/
		var $IncomePercentage;
		
		/**
		* @var string
		*/
		var $OtherIncomePercentage;
		
		/**
		* @var string
		*/
		var $ExpensePercentage;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsale_split_partners.*';
			$this->id = 'l2sun1el_manualsale_split_partners.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ManualSaleID = 'l2sun1el_manualsale_split_partners.ManualSaleID';
			$this->SplitPartnerID = 'l2sun1el_manualsale_split_partners.SplitPartnerID';
			$this->isUserSale = 'l2sun1el_manualsale_split_partners.isUserSale';
			$this->IncomePercentage = 'l2sun1el_manualsale_split_partners.IncomePercentage';
			$this->OtherIncomePercentage = 'l2sun1el_manualsale_split_partners.OtherIncomePercentage';
			$this->ExpensePercentage = 'l2sun1el_manualsale_split_partners.ExpensePercentage';
			$this->DateAdded = 'l2sun1el_manualsale_split_partners.DateAdded';
			$this->userid = 'l2sun1el_manualsale_split_partners.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsale_split_partners';
		}
	}
}