<?php
if(!class_exists('clsStructRotatorClicks')){
	class clsStructRotatorClicks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $RotatorLinkID;
		
		/**
		* @var string
		*/
		var $RotatorLinkType;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ClickType;
		
		/**
		* @var string
		*/
		var $ClickIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_rotator_clicks.*';
			$this->id = 'l2sun1el_rotator_clicks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->RotatorLinkID = 'l2sun1el_rotator_clicks.RotatorLinkID';
			$this->RotatorLinkType = 'l2sun1el_rotator_clicks.RotatorLinkType';
			$this->DateAdded = 'l2sun1el_rotator_clicks.DateAdded';
			$this->ClickType = 'l2sun1el_rotator_clicks.ClickType';
			$this->ClickIp = 'l2sun1el_rotator_clicks.ClickIp';
			$this->CountryCode = 'l2sun1el_rotator_clicks.CountryCode';
			$this->BrowserName = 'l2sun1el_rotator_clicks.BrowserName';
			$this->BrowserVersion = 'l2sun1el_rotator_clicks.BrowserVersion';
			$this->Platform = 'l2sun1el_rotator_clicks.Platform';
			$this->BotName = 'l2sun1el_rotator_clicks.BotName';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_rotator_clicks';
		}
	}
}