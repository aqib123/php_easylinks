<?php
if(!class_exists('clsStructVendors')){
	class clsStructVendors{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $VendorName;
		
		/**
		* @var string
		*/
		var $VendorType;
		
		/**
		* @var string
		*/
		var $VendorAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $VendorPicture;
		
		/**
		* @var string
		*/
		var $WebsiteUrl;
		
		/**
		* @var string
		*/
		var $SkypeName;
		
		/**
		* @var string
		*/
		var $LinkedIn;
		
		/**
		* @var string
		*/
		var $FacebookID;
		
		/**
		* @var string
		*/
		var $EmailAddress;
		
		/**
		* @var string
		*/
		var $AdditionalNotes;
		
		/**
		* @var string
		*/
		var $VendorTags;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $ReferenceID;
		
		/**
		* @var string
		*/
		var $TrafficQualityAvg;
		
		/**
		* @var string
		*/
		var $LeadScoreAvg;
		
		/**
		* @var string
		*/
		var $TimeDeliveredAvg;
		
		/**
		* @var string
		*/
		var $OverallDeliverabilityAvg;
		
		/**
		* @var string
		*/
		var $VendorOverallScoreAvg;
		
		/**
		* @var string
		*/
		var $SaleQualityAvg;
		
		/**
		* @var string
		*/
		var $OverallProfitabilityAvg;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_vendors.*';
			$this->id = 'l2sun1el_vendors.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->VendorName = 'l2sun1el_vendors.VendorName';
			$this->VendorType = 'l2sun1el_vendors.VendorType';
			$this->VendorAccessKey = 'l2sun1el_vendors.VendorAccessKey';
			$this->DateAdded = 'l2sun1el_vendors.DateAdded';
			$this->VendorPicture = 'l2sun1el_vendors.VendorPicture';
			$this->WebsiteUrl = 'l2sun1el_vendors.WebsiteUrl';
			$this->SkypeName = 'l2sun1el_vendors.SkypeName';
			$this->LinkedIn = 'l2sun1el_vendors.LinkedIn';
			$this->FacebookID = 'l2sun1el_vendors.FacebookID';
			$this->EmailAddress = 'l2sun1el_vendors.EmailAddress';
			$this->AdditionalNotes = 'l2sun1el_vendors.AdditionalNotes';
			$this->VendorTags = 'l2sun1el_vendors.VendorTags';
			$this->userid = 'l2sun1el_vendors.userid';
			$this->ReferenceID = 'l2sun1el_vendors.ReferenceID';
			$this->TrafficQualityAvg = 'l2sun1el_vendors.TrafficQualityAvg';
			$this->LeadScoreAvg = 'l2sun1el_vendors.LeadScoreAvg';
			$this->TimeDeliveredAvg = 'l2sun1el_vendors.TimeDeliveredAvg';
			$this->OverallDeliverabilityAvg = 'l2sun1el_vendors.OverallDeliverabilityAvg';
			$this->VendorOverallScoreAvg = 'l2sun1el_vendors.VendorOverallScoreAvg';
			$this->SaleQualityAvg = 'l2sun1el_vendors.SaleQualityAvg';
			$this->OverallProfitabilityAvg = 'l2sun1el_vendors.OverallProfitabilityAvg';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_vendors';
		}
	}
}