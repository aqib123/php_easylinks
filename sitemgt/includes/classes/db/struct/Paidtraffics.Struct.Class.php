<?php
if(!class_exists('clsStructPaidtraffics')){
	class clsStructPaidtraffics{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $VendorID;
		
		/**
		* @var string
		*/
		var $PaidTrafficName;
		
		/**
		* @var string
		*/
		var $PaidTrafficAccessKey;
		
		/**
		* @var string
		*/
		var $DestinationURL;
		
		/**
		* @var string
		*/
		var $TrafficCost;
		
		/**
		* @var string
		*/
		var $TrafficCostType;
		
		/**
		* @var integer
		*/
		var $NoOfClicks;
		
		/**
		* @var string
		*/
		var $VisibleLink;
		
		/**
		* @var integer
		*/
		var $UseAdminDomain;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var integer
		*/
		var $GroupID;
		
		/**
		* @var string
		*/
		var $StartDate;
		
		/**
		* @var string
		*/
		var $EndDate;
		
		/**
		* @var string
		*/
		var $PaidTrafficStatus;
		
		/**
		* @var integer
		*/
		var $PaidTrafficActive;
		
		/**
		* @var string
		*/
		var $TrackingCodeForActions;
		
		/**
		* @var integer
		*/
		var $PixelID;
		
		/**
		* @var string
		*/
		var $UnitPriceSales;
		
		/**
		* @var string
		*/
		var $TrackingCodeForSalesAndConversions;
		
		/**
		* @var string
		*/
		var $AdditionalNotes;
		
		/**
		* @var string
		*/
		var $PageImage;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $ManualSalesEntry;
		
		/**
		* @var integer
		*/
		var $PendingPageID;
		
		/**
		* @var integer
		*/
		var $CompletePageID;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		
		/**
		* @var integer
		*/
		var $CloakURL;
		
		/**
		* @var integer
		*/
		var $EasyLinkID;
		
		/**
		* @var string
		*/
		var $ReferenceID;
		
		/**
		* @var integer
		*/
		var $ShowInAdmin;
		
		/**
		* @var string
		*/
		var $TrafficQuality;
		
		/**
		* @var string
		*/
		var $LeadScore;
		
		/**
		* @var string
		*/
		var $TimeDelivered;
		
		/**
		* @var string
		*/
		var $OverallDeliverability;
		
		/**
		* @var string
		*/
		var $VendorOverallScore;
		
		/**
		* @var string
		*/
		var $SaleQuality;
		
		/**
		* @var string
		*/
		var $OverallProfitability;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_paidtraffics.*';
			$this->id = 'l2sun1el_paidtraffics.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_paidtraffics.userid';
			$this->VendorID = 'l2sun1el_paidtraffics.VendorID';
			$this->PaidTrafficName = 'l2sun1el_paidtraffics.PaidTrafficName';
			$this->PaidTrafficAccessKey = 'l2sun1el_paidtraffics.PaidTrafficAccessKey';
			$this->DestinationURL = 'l2sun1el_paidtraffics.DestinationURL';
			$this->TrafficCost = 'l2sun1el_paidtraffics.TrafficCost';
			$this->TrafficCostType = 'l2sun1el_paidtraffics.TrafficCostType';
			$this->NoOfClicks = 'l2sun1el_paidtraffics.NoOfClicks';
			$this->VisibleLink = 'l2sun1el_paidtraffics.VisibleLink';
			$this->UseAdminDomain = 'l2sun1el_paidtraffics.UseAdminDomain';
			$this->DomainID = 'l2sun1el_paidtraffics.DomainID';
			$this->GroupID = 'l2sun1el_paidtraffics.GroupID';
			$this->StartDate = 'l2sun1el_paidtraffics.StartDate';
			$this->EndDate = 'l2sun1el_paidtraffics.EndDate';
			$this->PaidTrafficStatus = 'l2sun1el_paidtraffics.PaidTrafficStatus';
			$this->PaidTrafficActive = 'l2sun1el_paidtraffics.PaidTrafficActive';
			$this->TrackingCodeForActions = 'l2sun1el_paidtraffics.TrackingCodeForActions';
			$this->PixelID = 'l2sun1el_paidtraffics.PixelID';
			$this->UnitPriceSales = 'l2sun1el_paidtraffics.UnitPriceSales';
			$this->TrackingCodeForSalesAndConversions = 'l2sun1el_paidtraffics.TrackingCodeForSalesAndConversions';
			$this->AdditionalNotes = 'l2sun1el_paidtraffics.AdditionalNotes';
			$this->PageImage = 'l2sun1el_paidtraffics.PageImage';
			$this->DateAdded = 'l2sun1el_paidtraffics.DateAdded';
			$this->ManualSalesEntry = 'l2sun1el_paidtraffics.ManualSalesEntry';
			$this->PendingPageID = 'l2sun1el_paidtraffics.PendingPageID';
			$this->CompletePageID = 'l2sun1el_paidtraffics.CompletePageID';
			$this->MasterCampaignID = 'l2sun1el_paidtraffics.MasterCampaignID';
			$this->CloakURL = 'l2sun1el_paidtraffics.CloakURL';
			$this->EasyLinkID = 'l2sun1el_paidtraffics.EasyLinkID';
			$this->ReferenceID = 'l2sun1el_paidtraffics.ReferenceID';
			$this->ShowInAdmin = 'l2sun1el_paidtraffics.ShowInAdmin';
			$this->TrafficQuality = 'l2sun1el_paidtraffics.TrafficQuality';
			$this->LeadScore = 'l2sun1el_paidtraffics.LeadScore';
			$this->TimeDelivered = 'l2sun1el_paidtraffics.TimeDelivered';
			$this->OverallDeliverability = 'l2sun1el_paidtraffics.OverallDeliverability';
			$this->VendorOverallScore = 'l2sun1el_paidtraffics.VendorOverallScore';
			$this->SaleQuality = 'l2sun1el_paidtraffics.SaleQuality';
			$this->OverallProfitability = 'l2sun1el_paidtraffics.OverallProfitability';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_paidtraffics';
		}
	}
}