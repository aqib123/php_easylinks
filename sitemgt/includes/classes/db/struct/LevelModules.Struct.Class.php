<?php
if(!class_exists('clsStructLevelModules')){
	class clsStructLevelModules{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var integer
		*/
		var $ModuleID;
		
		/**
		* @var integer
		*/
		var $LevelModuleEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_level_modules.*';
			$this->id = 'l2sun1el_level_modules.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LevelID = 'l2sun1el_level_modules.LevelID';
			$this->ModuleID = 'l2sun1el_level_modules.ModuleID';
			$this->LevelModuleEnabled = 'l2sun1el_level_modules.LevelModuleEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_level_modules';
		}
	}
}