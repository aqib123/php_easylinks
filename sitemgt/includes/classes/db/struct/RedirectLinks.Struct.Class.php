<?php
if(!class_exists('clsStructRedirectLinks')){
	class clsStructRedirectLinks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $RedirectLinkName;
		
		/**
		* @var string
		*/
		var $RedirectLinkURL;
		
		/**
		* @var string
		*/
		var $RedirectLinkType;
		
		/**
		* @var integer
		*/
		var $RedirectLinkIsDefault;
		
		/**
		* @var integer
		*/
		var $RedirectLinkIsDefault404;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $DefaultCloakURL;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_redirect_links.*';
			$this->id = 'l2sun1el_redirect_links.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->RedirectLinkName = 'l2sun1el_redirect_links.RedirectLinkName';
			$this->RedirectLinkURL = 'l2sun1el_redirect_links.RedirectLinkURL';
			$this->RedirectLinkType = 'l2sun1el_redirect_links.RedirectLinkType';
			$this->RedirectLinkIsDefault = 'l2sun1el_redirect_links.RedirectLinkIsDefault';
			$this->RedirectLinkIsDefault404 = 'l2sun1el_redirect_links.RedirectLinkIsDefault404';
			$this->DateAdded = 'l2sun1el_redirect_links.DateAdded';
			$this->userid = 'l2sun1el_redirect_links.userid';
			$this->DefaultCloakURL = 'l2sun1el_redirect_links.DefaultCloakURL';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_redirect_links';
		}
	}
}