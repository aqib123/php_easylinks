<?php
if(!class_exists('clsStructMenuItems')){
	class clsStructMenuItems{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $MenuItemURL;
		
		/**
		* @var string
		*/
		var $MenuItemClass;
		
		/**
		* @var string
		*/
		var $MenuItemIcon;
		
		/**
		* @var string
		*/
		var $MenuItemImage;
		
		/**
		* @var string
		*/
		var $MenuItemColor;
		
		/**
		* @var string
		*/
		var $MenuItemLabel;
		
		/**
		* @var integer
		*/
		var $MenuItemPosition;
		
		/**
		* @var integer
		*/
		var $MenuItemEnabled;
		
		/**
		* @var integer
		*/
		var $MenuItemParentID;
		
		/**
		* @var integer
		*/
		var $MenuItemHasAdmin;
		
		/**
		* @var integer
		*/
		var $MenuItemAlwaysAdd;
		
		/**
		* @var integer
		*/
		var $MenuItemExtendable;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_menu_items.*';
			$this->id = 'l2sun1el_menu_items.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->MenuItemURL = 'l2sun1el_menu_items.MenuItemURL';
			$this->MenuItemClass = 'l2sun1el_menu_items.MenuItemClass';
			$this->MenuItemIcon = 'l2sun1el_menu_items.MenuItemIcon';
			$this->MenuItemImage = 'l2sun1el_menu_items.MenuItemImage';
			$this->MenuItemColor = 'l2sun1el_menu_items.MenuItemColor';
			$this->MenuItemLabel = 'l2sun1el_menu_items.MenuItemLabel';
			$this->MenuItemPosition = 'l2sun1el_menu_items.MenuItemPosition';
			$this->MenuItemEnabled = 'l2sun1el_menu_items.MenuItemEnabled';
			$this->MenuItemParentID = 'l2sun1el_menu_items.MenuItemParentID';
			$this->MenuItemHasAdmin = 'l2sun1el_menu_items.MenuItemHasAdmin';
			$this->MenuItemAlwaysAdd = 'l2sun1el_menu_items.MenuItemAlwaysAdd';
			$this->MenuItemExtendable = 'l2sun1el_menu_items.MenuItemExtendable';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_menu_items';
		}
	}
}