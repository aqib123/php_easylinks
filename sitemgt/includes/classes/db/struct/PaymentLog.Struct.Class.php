<?php
if(!class_exists('clsStructPaymentLog')){
	class clsStructPaymentLog{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $custname;
		
		/**
		* @var string
		*/
		var $custemail;
		
		/**
		* @var integer
		*/
		var $proditem;
		
		/**
		* @var string
		*/
		var $custcountry;
		
		/**
		* @var string
		*/
		var $prodtype;
		
		/**
		* @var string
		*/
		var $status;
		
		/**
		* @var integer
		*/
		var $affiliate;
		
		/**
		* @var string
		*/
		var $amount;
		
		/**
		* @var string
		*/
		var $up_amount;
		
		/**
		* @var string
		*/
		var $paymode;
		
		/**
		* @var integer
		*/
		var $transvendor;
		
		/**
		* @var string
		*/
		var $transid;
		
		/**
		* @var string
		*/
		var $uptranid;
		
		/**
		* @var integer
		*/
		var $affiliateid;
		
		/**
		* @var string
		*/
		var $verifykey;
		
		/**
		* @var integer
		*/
		var $transtime;
		
		/**
		* @var integer
		*/
		var $UserID;
		
		/**
		* @var string
		*/
		var $username;
		
		/**
		* @var string
		*/
		var $password;
		
		/**
		* @var integer
		*/
		var $pro_type;
		
		/**
		* @var string
		*/
		var $LevelAccessKey;
		
		/**
		* @var integer
		*/
		var $active_status;
		
		/**
		* @var integer
		*/
		var $sent_license_key;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'payment_log.*';
			$this->id = 'payment_log.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->custname = 'payment_log.custname';
			$this->custemail = 'payment_log.custemail';
			$this->proditem = 'payment_log.proditem';
			$this->custcountry = 'payment_log.custcountry';
			$this->prodtype = 'payment_log.prodtype';
			$this->status = 'payment_log.status';
			$this->affiliate = 'payment_log.affiliate';
			$this->amount = 'payment_log.amount';
			$this->up_amount = 'payment_log.up_amount';
			$this->paymode = 'payment_log.paymode';
			$this->transvendor = 'payment_log.transvendor';
			$this->transid = 'payment_log.transid';
			$this->uptranid = 'payment_log.uptranid';
			$this->affiliateid = 'payment_log.affiliateid';
			$this->verifykey = 'payment_log.verifykey';
			$this->transtime = 'payment_log.transtime';
			$this->UserID = 'payment_log.UserID';
			$this->username = 'payment_log.username';
			$this->password = 'payment_log.password';
			$this->pro_type = 'payment_log.pro_type';
			$this->LevelAccessKey = 'payment_log.LevelAccessKey';
			$this->active_status = 'payment_log.active_status';
			$this->sent_license_key = 'payment_log.sent_license_key';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'payment_log';
		}
	}
}