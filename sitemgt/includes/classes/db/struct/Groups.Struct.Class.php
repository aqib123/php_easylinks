<?php
if(!class_exists('clsStructGroups')){
	class clsStructGroups{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $GroupName;
		
		/**
		* @var string
		*/
		var $GroupType;
		
		/**
		* @var string
		*/
		var $GroupAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $ReferenceID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_groups.*';
			$this->id = 'l2sun1el_groups.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->GroupName = 'l2sun1el_groups.GroupName';
			$this->GroupType = 'l2sun1el_groups.GroupType';
			$this->GroupAccessKey = 'l2sun1el_groups.GroupAccessKey';
			$this->DateAdded = 'l2sun1el_groups.DateAdded';
			$this->userid = 'l2sun1el_groups.userid';
			$this->ReferenceID = 'l2sun1el_groups.ReferenceID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_groups';
		}
	}
}