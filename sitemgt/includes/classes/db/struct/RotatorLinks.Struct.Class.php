<?php
if(!class_exists('clsStructRotatorLinks')){
	class clsStructRotatorLinks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $RotatorID;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var string
		*/
		var $RotatorLinkType;
		
		/**
		* @var string
		*/
		var $RotatorLinkURL;
		
		/**
		* @var integer
		*/
		var $MaxClicks;
		
		/**
		* @var string
		*/
		var $StartDate;
		
		/**
		* @var string
		*/
		var $EndDate;
		
		/**
		* @var integer
		*/
		var $RotatorLinkPosition;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $RotatorLinkStatus;
		
		/**
		* @var integer
		*/
		var $RotatorLinkLive;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_rotator_links.*';
			$this->id = 'l2sun1el_rotator_links.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_rotator_links.userid';
			$this->RotatorID = 'l2sun1el_rotator_links.RotatorID';
			$this->LinkBankID = 'l2sun1el_rotator_links.LinkBankID';
			$this->RotatorLinkType = 'l2sun1el_rotator_links.RotatorLinkType';
			$this->RotatorLinkURL = 'l2sun1el_rotator_links.RotatorLinkURL';
			$this->MaxClicks = 'l2sun1el_rotator_links.MaxClicks';
			$this->StartDate = 'l2sun1el_rotator_links.StartDate';
			$this->EndDate = 'l2sun1el_rotator_links.EndDate';
			$this->RotatorLinkPosition = 'l2sun1el_rotator_links.RotatorLinkPosition';
			$this->DateAdded = 'l2sun1el_rotator_links.DateAdded';
			$this->RotatorLinkStatus = 'l2sun1el_rotator_links.RotatorLinkStatus';
			$this->RotatorLinkLive = 'l2sun1el_rotator_links.RotatorLinkLive';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_rotator_links';
		}
	}
}