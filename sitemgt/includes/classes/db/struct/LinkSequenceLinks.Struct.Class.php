<?php
if(!class_exists('clsStructLinkSequenceLinks')){
	class clsStructLinkSequenceLinks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $LinkSequenceID;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var string
		*/
		var $LinkSequenceLinkName;
		
		/**
		* @var string
		*/
		var $LinkSequenceLinkType;
		
		/**
		* @var string
		*/
		var $LinkSequenceLinkURL;
		
		/**
		* @var string
		*/
		var $EndDateType;
		
		/**
		* @var string
		*/
		var $EndDate;
		
		/**
		* @var string
		*/
		var $EndTime;
		
		/**
		* @var integer
		*/
		var $LinkSequenceLinkPosition;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $LinkSequenceLinkStatus;
		
		/**
		* @var integer
		*/
		var $LinkSequenceLinkLive;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_link_sequence_links.*';
			$this->id = 'l2sun1el_link_sequence_links.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_link_sequence_links.userid';
			$this->LinkSequenceID = 'l2sun1el_link_sequence_links.LinkSequenceID';
			$this->LinkBankID = 'l2sun1el_link_sequence_links.LinkBankID';
			$this->LinkSequenceLinkName = 'l2sun1el_link_sequence_links.LinkSequenceLinkName';
			$this->LinkSequenceLinkType = 'l2sun1el_link_sequence_links.LinkSequenceLinkType';
			$this->LinkSequenceLinkURL = 'l2sun1el_link_sequence_links.LinkSequenceLinkURL';
			$this->EndDateType = 'l2sun1el_link_sequence_links.EndDateType';
			$this->EndDate = 'l2sun1el_link_sequence_links.EndDate';
			$this->EndTime = 'l2sun1el_link_sequence_links.EndTime';
			$this->LinkSequenceLinkPosition = 'l2sun1el_link_sequence_links.LinkSequenceLinkPosition';
			$this->DateAdded = 'l2sun1el_link_sequence_links.DateAdded';
			$this->LinkSequenceLinkStatus = 'l2sun1el_link_sequence_links.LinkSequenceLinkStatus';
			$this->LinkSequenceLinkLive = 'l2sun1el_link_sequence_links.LinkSequenceLinkLive';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_link_sequence_links';
		}
	}
}