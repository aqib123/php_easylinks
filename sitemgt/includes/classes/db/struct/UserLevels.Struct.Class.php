<?php
if(!class_exists('clsStructUserLevels')){
	class clsStructUserLevels{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $UserID;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var integer
		*/
		var $UserLevelEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_user_levels.*';
			$this->id = 'l2sun1el_user_levels.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->UserID = 'l2sun1el_user_levels.UserID';
			$this->LevelID = 'l2sun1el_user_levels.LevelID';
			$this->UserLevelEnabled = 'l2sun1el_user_levels.UserLevelEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_user_levels';
		}
	}
}