<?php
if(!class_exists('clsStructModulesCopy')){
	class clsStructModulesCopy{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $ModuleName;
		
		/**
		* @var string
		*/
		var $ModuleLabel;
		
		/**
		* @var integer
		*/
		var $ModuleEnabled;
		
		/**
		* @var integer
		*/
		var $ModuleAlwaysAdd;
		
		/**
		* @var integer
		*/
		var $ModuleExtendable;
		
		/**
		* @var integer
		*/
		var $ModulePosition;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_modules_copy.*';
			$this->id = 'l2sun1el_modules_copy.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ModuleName = 'l2sun1el_modules_copy.ModuleName';
			$this->ModuleLabel = 'l2sun1el_modules_copy.ModuleLabel';
			$this->ModuleEnabled = 'l2sun1el_modules_copy.ModuleEnabled';
			$this->ModuleAlwaysAdd = 'l2sun1el_modules_copy.ModuleAlwaysAdd';
			$this->ModuleExtendable = 'l2sun1el_modules_copy.ModuleExtendable';
			$this->ModulePosition = 'l2sun1el_modules_copy.ModulePosition';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_modules_copy';
		}
	}
}