<?php
if(!class_exists('clsStructPixels')){
	class clsStructPixels{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $PixelName;
		
		/**
		* @var string
		*/
		var $PixelType;
		
		/**
		* @var string
		*/
		var $PixelCode;
		
		/**
		* @var string
		*/
		var $PixelAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_pixels.*';
			$this->id = 'l2sun1el_pixels.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->PixelName = 'l2sun1el_pixels.PixelName';
			$this->PixelType = 'l2sun1el_pixels.PixelType';
			$this->PixelCode = 'l2sun1el_pixels.PixelCode';
			$this->PixelAccessKey = 'l2sun1el_pixels.PixelAccessKey';
			$this->DateAdded = 'l2sun1el_pixels.DateAdded';
			$this->userid = 'l2sun1el_pixels.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_pixels';
		}
	}
}