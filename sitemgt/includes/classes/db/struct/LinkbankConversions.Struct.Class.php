<?php
if(!class_exists('clsStructLinkbankConversions')){
	class clsStructLinkbankConversions{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ConversionType;
		
		/**
		* @var string
		*/
		var $UnitPrice;
		
		/**
		* @var string
		*/
		var $ConversionIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		
		/**
		* @var integer
		*/
		var $LinkBankConversionPixelID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_linkbank_conversions.*';
			$this->id = 'l2sun1el_linkbank_conversions.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LinkBankID = 'l2sun1el_linkbank_conversions.LinkBankID';
			$this->DateAdded = 'l2sun1el_linkbank_conversions.DateAdded';
			$this->ConversionType = 'l2sun1el_linkbank_conversions.ConversionType';
			$this->UnitPrice = 'l2sun1el_linkbank_conversions.UnitPrice';
			$this->ConversionIp = 'l2sun1el_linkbank_conversions.ConversionIp';
			$this->CountryCode = 'l2sun1el_linkbank_conversions.CountryCode';
			$this->BrowserName = 'l2sun1el_linkbank_conversions.BrowserName';
			$this->BrowserVersion = 'l2sun1el_linkbank_conversions.BrowserVersion';
			$this->Platform = 'l2sun1el_linkbank_conversions.Platform';
			$this->BotName = 'l2sun1el_linkbank_conversions.BotName';
			$this->EmailID = 'l2sun1el_linkbank_conversions.EmailID';
			$this->LinkBankConversionPixelID = 'l2sun1el_linkbank_conversions.LinkBankConversionPixelID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_linkbank_conversions';
		}
	}
}