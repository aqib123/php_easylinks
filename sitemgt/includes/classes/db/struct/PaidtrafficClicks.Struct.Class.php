<?php
if(!class_exists('clsStructPaidtrafficClicks')){
	class clsStructPaidtrafficClicks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $PaidTrafficID;
		
		/**
		* @var string
		*/
		var $PaidTrafficType;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ClickType;
		
		/**
		* @var string
		*/
		var $ClickIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_paidtraffic_clicks.*';
			$this->id = 'l2sun1el_paidtraffic_clicks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->PaidTrafficID = 'l2sun1el_paidtraffic_clicks.PaidTrafficID';
			$this->PaidTrafficType = 'l2sun1el_paidtraffic_clicks.PaidTrafficType';
			$this->DateAdded = 'l2sun1el_paidtraffic_clicks.DateAdded';
			$this->ClickType = 'l2sun1el_paidtraffic_clicks.ClickType';
			$this->ClickIp = 'l2sun1el_paidtraffic_clicks.ClickIp';
			$this->CountryCode = 'l2sun1el_paidtraffic_clicks.CountryCode';
			$this->BrowserName = 'l2sun1el_paidtraffic_clicks.BrowserName';
			$this->BrowserVersion = 'l2sun1el_paidtraffic_clicks.BrowserVersion';
			$this->Platform = 'l2sun1el_paidtraffic_clicks.Platform';
			$this->BotName = 'l2sun1el_paidtraffic_clicks.BotName';
			$this->EmailID = 'l2sun1el_paidtraffic_clicks.EmailID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_paidtraffic_clicks';
		}
	}
}