<?php
if(!class_exists('clsStructLinkbankPixelFires')){
	class clsStructLinkbankPixelFires{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var integer
		*/
		var $PixelID;
		
		/**
		* @var string
		*/
		var $FireType;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $FireIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_linkbank_pixel_fires.*';
			$this->id = 'l2sun1el_linkbank_pixel_fires.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LinkBankID = 'l2sun1el_linkbank_pixel_fires.LinkBankID';
			$this->PixelID = 'l2sun1el_linkbank_pixel_fires.PixelID';
			$this->FireType = 'l2sun1el_linkbank_pixel_fires.FireType';
			$this->DateAdded = 'l2sun1el_linkbank_pixel_fires.DateAdded';
			$this->FireIp = 'l2sun1el_linkbank_pixel_fires.FireIp';
			$this->CountryCode = 'l2sun1el_linkbank_pixel_fires.CountryCode';
			$this->BrowserName = 'l2sun1el_linkbank_pixel_fires.BrowserName';
			$this->BrowserVersion = 'l2sun1el_linkbank_pixel_fires.BrowserVersion';
			$this->Platform = 'l2sun1el_linkbank_pixel_fires.Platform';
			$this->BotName = 'l2sun1el_linkbank_pixel_fires.BotName';
			$this->EmailID = 'l2sun1el_linkbank_pixel_fires.EmailID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_linkbank_pixel_fires';
		}
	}
}