<?php
if(!class_exists('clsStructOptions')){
	class clsStructOptions{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $OptionName;
		
		/**
		* @var string
		*/
		var $OptionValue;
		
		/**
		* @var integer
		*/
		var $OptionEnabled;
		
		/**
		* @var integer
		*/
		var $OptionDefault;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_options.*';
			$this->id = 'l2sun1el_options.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->OptionName = 'l2sun1el_options.OptionName';
			$this->OptionValue = 'l2sun1el_options.OptionValue';
			$this->OptionEnabled = 'l2sun1el_options.OptionEnabled';
			$this->OptionDefault = 'l2sun1el_options.OptionDefault';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_options';
		}
	}
}