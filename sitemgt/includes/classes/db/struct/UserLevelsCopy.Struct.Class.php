<?php
if(!class_exists('clsStructUserLevelsCopy')){
	class clsStructUserLevelsCopy{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $UserID;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var integer
		*/
		var $UserLevelEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_user_levels_copy.*';
			$this->id = 'l2sun1el_user_levels_copy.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->UserID = 'l2sun1el_user_levels_copy.UserID';
			$this->LevelID = 'l2sun1el_user_levels_copy.LevelID';
			$this->UserLevelEnabled = 'l2sun1el_user_levels_copy.UserLevelEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_user_levels_copy';
		}
	}
}