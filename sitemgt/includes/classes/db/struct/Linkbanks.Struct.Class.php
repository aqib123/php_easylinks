<?php
if(!class_exists('clsStructLinkbanks')){
	class clsStructLinkbanks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $VendorID;
		
		/**
		* @var string
		*/
		var $LinkName;
		
		/**
		* @var string
		*/
		var $LinkBankAccessKey;
		
		/**
		* @var string
		*/
		var $DestinationURL;
		
		/**
		* @var string
		*/
		var $VisibleLink;
		
		/**
		* @var integer
		*/
		var $UseAdminDomain;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var integer
		*/
		var $GroupID;
		
		/**
		* @var string
		*/
		var $StartDate;
		
		/**
		* @var string
		*/
		var $EndDate;
		
		/**
		* @var string
		*/
		var $AdditionalNotes;
		
		/**
		* @var string
		*/
		var $PageImage;
		
		/**
		* @var integer
		*/
		var $LinkActive;
		
		/**
		* @var integer
		*/
		var $SalesConversions;
		
		/**
		* @var string
		*/
		var $RedirectAfterLinkExpired;
		
		/**
		* @var integer
		*/
		var $PixelID;
		
		/**
		* @var string
		*/
		var $TrackingCodeForActions;
		
		/**
		* @var string
		*/
		var $TrackingCodeForSalesAndConversions;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $UnitPriceAction;
		
		/**
		* @var string
		*/
		var $UnitPriceSales;
		
		/**
		* @var string
		*/
		var $LinkStatus;
		
		/**
		* @var integer
		*/
		var $SplitPartnerID;
		
		/**
		* @var string
		*/
		var $PartnerSplit;
		
		/**
		* @var string
		*/
		var $Commission;
		
		/**
		* @var integer
		*/
		var $AffiliatePlatformID;
		
		/**
		* @var integer
		*/
		var $ManualSalesEntry;
		
		/**
		* @var integer
		*/
		var $SaleQuantity;
		
		/**
		* @var string
		*/
		var $GrossSale;
		
		/**
		* @var string
		*/
		var $ExtraIncome;
		
		/**
		* @var string
		*/
		var $Expenses;
		
		/**
		* @var string
		*/
		var $SaleInfoEntered;
		
		/**
		* @var string
		*/
		var $OwedToPartner;
		
		/**
		* @var integer
		*/
		var $SplitExpensesWithPartner;
		
		/**
		* @var string
		*/
		var $SplitExpensesPercentage;
		
		/**
		* @var string
		*/
		var $PartnerPaymentStatus;
		
		/**
		* @var string
		*/
		var $PartnerPaymentDate;
		
		/**
		* @var string
		*/
		var $PartnerPaymentReferenceNo;
		
		/**
		* @var integer
		*/
		var $TrackCommission;
		
		/**
		* @var integer
		*/
		var $PartnerSplitAllowed;
		
		/**
		* @var integer
		*/
		var $PendingPageID;
		
		/**
		* @var integer
		*/
		var $CompletePageID;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		
		/**
		* @var integer
		*/
		var $CloakURL;
		
		/**
		* @var integer
		*/
		var $TrackEPC;
		
		/**
		* @var integer
		*/
		var $RawClicks;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_linkbanks.*';
			$this->id = 'l2sun1el_linkbanks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_linkbanks.userid';
			$this->VendorID = 'l2sun1el_linkbanks.VendorID';
			$this->LinkName = 'l2sun1el_linkbanks.LinkName';
			$this->LinkBankAccessKey = 'l2sun1el_linkbanks.LinkBankAccessKey';
			$this->DestinationURL = 'l2sun1el_linkbanks.DestinationURL';
			$this->VisibleLink = 'l2sun1el_linkbanks.VisibleLink';
			$this->UseAdminDomain = 'l2sun1el_linkbanks.UseAdminDomain';
			$this->DomainID = 'l2sun1el_linkbanks.DomainID';
			$this->GroupID = 'l2sun1el_linkbanks.GroupID';
			$this->StartDate = 'l2sun1el_linkbanks.StartDate';
			$this->EndDate = 'l2sun1el_linkbanks.EndDate';
			$this->AdditionalNotes = 'l2sun1el_linkbanks.AdditionalNotes';
			$this->PageImage = 'l2sun1el_linkbanks.PageImage';
			$this->LinkActive = 'l2sun1el_linkbanks.LinkActive';
			$this->SalesConversions = 'l2sun1el_linkbanks.SalesConversions';
			$this->RedirectAfterLinkExpired = 'l2sun1el_linkbanks.RedirectAfterLinkExpired';
			$this->PixelID = 'l2sun1el_linkbanks.PixelID';
			$this->TrackingCodeForActions = 'l2sun1el_linkbanks.TrackingCodeForActions';
			$this->TrackingCodeForSalesAndConversions = 'l2sun1el_linkbanks.TrackingCodeForSalesAndConversions';
			$this->DateAdded = 'l2sun1el_linkbanks.DateAdded';
			$this->UnitPriceAction = 'l2sun1el_linkbanks.UnitPriceAction';
			$this->UnitPriceSales = 'l2sun1el_linkbanks.UnitPriceSales';
			$this->LinkStatus = 'l2sun1el_linkbanks.LinkStatus';
			$this->SplitPartnerID = 'l2sun1el_linkbanks.SplitPartnerID';
			$this->PartnerSplit = 'l2sun1el_linkbanks.PartnerSplit';
			$this->Commission = 'l2sun1el_linkbanks.Commission';
			$this->AffiliatePlatformID = 'l2sun1el_linkbanks.AffiliatePlatformID';
			$this->ManualSalesEntry = 'l2sun1el_linkbanks.ManualSalesEntry';
			$this->SaleQuantity = 'l2sun1el_linkbanks.SaleQuantity';
			$this->GrossSale = 'l2sun1el_linkbanks.GrossSale';
			$this->ExtraIncome = 'l2sun1el_linkbanks.ExtraIncome';
			$this->Expenses = 'l2sun1el_linkbanks.Expenses';
			$this->SaleInfoEntered = 'l2sun1el_linkbanks.SaleInfoEntered';
			$this->OwedToPartner = 'l2sun1el_linkbanks.OwedToPartner';
			$this->SplitExpensesWithPartner = 'l2sun1el_linkbanks.SplitExpensesWithPartner';
			$this->SplitExpensesPercentage = 'l2sun1el_linkbanks.SplitExpensesPercentage';
			$this->PartnerPaymentStatus = 'l2sun1el_linkbanks.PartnerPaymentStatus';
			$this->PartnerPaymentDate = 'l2sun1el_linkbanks.PartnerPaymentDate';
			$this->PartnerPaymentReferenceNo = 'l2sun1el_linkbanks.PartnerPaymentReferenceNo';
			$this->TrackCommission = 'l2sun1el_linkbanks.TrackCommission';
			$this->PartnerSplitAllowed = 'l2sun1el_linkbanks.PartnerSplitAllowed';
			$this->PendingPageID = 'l2sun1el_linkbanks.PendingPageID';
			$this->CompletePageID = 'l2sun1el_linkbanks.CompletePageID';
			$this->MasterCampaignID = 'l2sun1el_linkbanks.MasterCampaignID';
			$this->CloakURL = 'l2sun1el_linkbanks.CloakURL';
			$this->TrackEPC = 'l2sun1el_linkbanks.TrackEPC';
			$this->RawClicks = 'l2sun1el_linkbanks.RawClicks';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_linkbanks';
		}
	}
}