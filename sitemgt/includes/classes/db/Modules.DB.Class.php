<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructModules'))
	include_once 'struct/Modules.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBModules')){
	class clsDBModules extends clsStructModules {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->modules." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBModules|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->modules->Asterisk." from ".DB()->modules;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBModules|null
         */
        function get_by_key($id){
            $Condition = DB()->modules->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBModules|null
         */
        function get_row($id = null, $ModuleName = null, $ModuleLabel = null, $ModuleEnabled = null, $ModuleAlwaysAdd = null, $ModuleExtendable = null, $ModulePosition = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($ModuleName !== null) $data["ModuleName"] = $ModuleName;
			if($ModuleLabel !== null) $data["ModuleLabel"] = $ModuleLabel;
			if($ModuleEnabled !== null) $data["ModuleEnabled"] = $ModuleEnabled;
			if($ModuleAlwaysAdd !== null) $data["ModuleAlwaysAdd"] = $ModuleAlwaysAdd;
			if($ModuleExtendable !== null) $data["ModuleExtendable"] = $ModuleExtendable;
			if($ModulePosition !== null) $data["ModulePosition"] = $ModulePosition;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->modules->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBModules[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->modules->Asterisk." from ".DB()->modules;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->modules.".".$GroupBy." ORDER BY ".DB()->modules.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->modules->id.") from ".DB()->modules;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBModules[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->modules;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->modules->id.") ".$sql;
            $sql = "select ".DB()->modules->Asterisk.$sql." group by ".DB()->modules.".".$GroupBy." ORDER BY ".DB()->modules.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($ModuleName = null, $ModuleLabel = null, $ModuleEnabled = null, $ModuleAlwaysAdd = null, $ModuleExtendable = null, $ModulePosition = null){
            $data = array();
            $Inserted = false;

            if($ModuleName !== null) $data["ModuleName"] = $ModuleName;
			if($ModuleLabel !== null) $data["ModuleLabel"] = $ModuleLabel;
			if($ModuleEnabled !== null) $data["ModuleEnabled"] = $ModuleEnabled;
			if($ModuleAlwaysAdd !== null) $data["ModuleAlwaysAdd"] = $ModuleAlwaysAdd;
			if($ModuleExtendable !== null) $data["ModuleExtendable"] = $ModuleExtendable;
			if($ModulePosition !== null) $data["ModulePosition"] = $ModulePosition;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->modules, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $ModuleName = null, $ModuleLabel = null, $ModuleEnabled = null, $ModuleAlwaysAdd = null, $ModuleExtendable = null, $ModulePosition = null){
            $data = array();
            $Updated = false;

            if($ModuleName !== null) $data["ModuleName"] = $ModuleName;
			if($ModuleLabel !== null) $data["ModuleLabel"] = $ModuleLabel;
			if($ModuleEnabled !== null) $data["ModuleEnabled"] = $ModuleEnabled;
			if($ModuleAlwaysAdd !== null) $data["ModuleAlwaysAdd"] = $ModuleAlwaysAdd;
			if($ModuleExtendable !== null) $data["ModuleExtendable"] = $ModuleExtendable;
			if($ModulePosition !== null) $data["ModulePosition"] = $ModulePosition;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->modules, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $ModuleName = null, $ModuleLabel = null, $ModuleEnabled = null, $ModuleAlwaysAdd = null, $ModuleExtendable = null, $ModulePosition = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($ModuleName !== null) $data["ModuleName"] = $ModuleName;
			if($ModuleLabel !== null) $data["ModuleLabel"] = $ModuleLabel;
			if($ModuleEnabled !== null) $data["ModuleEnabled"] = $ModuleEnabled;
			if($ModuleAlwaysAdd !== null) $data["ModuleAlwaysAdd"] = $ModuleAlwaysAdd;
			if($ModuleExtendable !== null) $data["ModuleExtendable"] = $ModuleExtendable;
			if($ModulePosition !== null) $data["ModulePosition"] = $ModulePosition;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->modules, $data);
            }

            return $Deleted;
        }

        function save($Condition, $ModuleName = null, $ModuleLabel = null, $ModuleEnabled = null, $ModuleAlwaysAdd = null, $ModuleExtendable = null, $ModulePosition = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($ModuleName, $ModuleLabel, $ModuleEnabled, $ModuleAlwaysAdd, $ModuleExtendable, $ModulePosition);
            } else {
                $this->update($DBRow->id, $ModuleName, $ModuleLabel, $ModuleEnabled, $ModuleAlwaysAdd, $ModuleExtendable, $ModulePosition);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBMO')){
	function DBMO($obj = null) {
		return clsDBModules::instance($obj);
	}
}