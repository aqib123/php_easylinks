<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkSequenceLinks'))
	include_once 'struct/LinkSequenceLinks.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkSequenceLinks')){
	class clsDBLinkSequenceLinks extends clsStructLinkSequenceLinks {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->link_sequence_links." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkSequenceLinks|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->link_sequence_links->Asterisk." from ".DB()->link_sequence_links;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkSequenceLinks|null
         */
        function get_by_key($id){
            $Condition = DB()->link_sequence_links->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkSequenceLinks|null
         */
        function get_row($id = null, $userid = null, $LinkSequenceID = null, $LinkBankID = null, $LinkSequenceLinkName = null, $LinkSequenceLinkType = null, $LinkSequenceLinkURL = null, $EndDateType = null, $EndDate = null, $EndTime = null, $LinkSequenceLinkPosition = null, $DateAdded = null, $LinkSequenceLinkStatus = null, $LinkSequenceLinkLive = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($LinkSequenceID !== null) $data["LinkSequenceID"] = $LinkSequenceID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkSequenceLinkName !== null) $data["LinkSequenceLinkName"] = $LinkSequenceLinkName;
			if($LinkSequenceLinkType !== null) $data["LinkSequenceLinkType"] = $LinkSequenceLinkType;
			if($LinkSequenceLinkURL !== null) $data["LinkSequenceLinkURL"] = $LinkSequenceLinkURL;
			if($EndDateType !== null) $data["EndDateType"] = $EndDateType;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($EndTime !== null) $data["EndTime"] = $EndTime;
			if($LinkSequenceLinkPosition !== null) $data["LinkSequenceLinkPosition"] = $LinkSequenceLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkSequenceLinkStatus !== null) $data["LinkSequenceLinkStatus"] = $LinkSequenceLinkStatus;
			if($LinkSequenceLinkLive !== null) $data["LinkSequenceLinkLive"] = $LinkSequenceLinkLive;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->link_sequence_links->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkSequenceLinks[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->link_sequence_links->Asterisk." from ".DB()->link_sequence_links;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->link_sequence_links.".".$GroupBy." ORDER BY ".DB()->link_sequence_links.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->link_sequence_links->id.") from ".DB()->link_sequence_links;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkSequenceLinks[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->link_sequence_links;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->link_sequence_links->id.") ".$sql;
            $sql = "select ".DB()->link_sequence_links->Asterisk.$sql." group by ".DB()->link_sequence_links.".".$GroupBy." ORDER BY ".DB()->link_sequence_links.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $LinkSequenceID = null, $LinkBankID = null, $LinkSequenceLinkName = null, $LinkSequenceLinkType = null, $LinkSequenceLinkURL = null, $EndDateType = null, $EndDate = null, $EndTime = null, $LinkSequenceLinkPosition = null, $DateAdded = null, $LinkSequenceLinkStatus = null, $LinkSequenceLinkLive = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($LinkSequenceID !== null) $data["LinkSequenceID"] = $LinkSequenceID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkSequenceLinkName !== null) $data["LinkSequenceLinkName"] = $LinkSequenceLinkName;
			if($LinkSequenceLinkType !== null) $data["LinkSequenceLinkType"] = $LinkSequenceLinkType;
			if($LinkSequenceLinkURL !== null) $data["LinkSequenceLinkURL"] = $LinkSequenceLinkURL;
			if($EndDateType !== null) $data["EndDateType"] = $EndDateType;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($EndTime !== null) $data["EndTime"] = $EndTime;
			if($LinkSequenceLinkPosition !== null) $data["LinkSequenceLinkPosition"] = $LinkSequenceLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkSequenceLinkStatus !== null) $data["LinkSequenceLinkStatus"] = $LinkSequenceLinkStatus;
			if($LinkSequenceLinkLive !== null) $data["LinkSequenceLinkLive"] = $LinkSequenceLinkLive;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->link_sequence_links, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $LinkSequenceID = null, $LinkBankID = null, $LinkSequenceLinkName = null, $LinkSequenceLinkType = null, $LinkSequenceLinkURL = null, $EndDateType = null, $EndDate = null, $EndTime = null, $LinkSequenceLinkPosition = null, $DateAdded = null, $LinkSequenceLinkStatus = null, $LinkSequenceLinkLive = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($LinkSequenceID !== null) $data["LinkSequenceID"] = $LinkSequenceID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkSequenceLinkName !== null) $data["LinkSequenceLinkName"] = $LinkSequenceLinkName;
			if($LinkSequenceLinkType !== null) $data["LinkSequenceLinkType"] = $LinkSequenceLinkType;
			if($LinkSequenceLinkURL !== null) $data["LinkSequenceLinkURL"] = $LinkSequenceLinkURL;
			if($EndDateType !== null) $data["EndDateType"] = $EndDateType;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($EndTime !== null) $data["EndTime"] = $EndTime;
			if($LinkSequenceLinkPosition !== null) $data["LinkSequenceLinkPosition"] = $LinkSequenceLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkSequenceLinkStatus !== null) $data["LinkSequenceLinkStatus"] = $LinkSequenceLinkStatus;
			if($LinkSequenceLinkLive !== null) $data["LinkSequenceLinkLive"] = $LinkSequenceLinkLive;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->link_sequence_links, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $LinkSequenceID = null, $LinkBankID = null, $LinkSequenceLinkName = null, $LinkSequenceLinkType = null, $LinkSequenceLinkURL = null, $EndDateType = null, $EndDate = null, $EndTime = null, $LinkSequenceLinkPosition = null, $DateAdded = null, $LinkSequenceLinkStatus = null, $LinkSequenceLinkLive = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($LinkSequenceID !== null) $data["LinkSequenceID"] = $LinkSequenceID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkSequenceLinkName !== null) $data["LinkSequenceLinkName"] = $LinkSequenceLinkName;
			if($LinkSequenceLinkType !== null) $data["LinkSequenceLinkType"] = $LinkSequenceLinkType;
			if($LinkSequenceLinkURL !== null) $data["LinkSequenceLinkURL"] = $LinkSequenceLinkURL;
			if($EndDateType !== null) $data["EndDateType"] = $EndDateType;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($EndTime !== null) $data["EndTime"] = $EndTime;
			if($LinkSequenceLinkPosition !== null) $data["LinkSequenceLinkPosition"] = $LinkSequenceLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkSequenceLinkStatus !== null) $data["LinkSequenceLinkStatus"] = $LinkSequenceLinkStatus;
			if($LinkSequenceLinkLive !== null) $data["LinkSequenceLinkLive"] = $LinkSequenceLinkLive;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->link_sequence_links, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $LinkSequenceID = null, $LinkBankID = null, $LinkSequenceLinkName = null, $LinkSequenceLinkType = null, $LinkSequenceLinkURL = null, $EndDateType = null, $EndDate = null, $EndTime = null, $LinkSequenceLinkPosition = null, $DateAdded = null, $LinkSequenceLinkStatus = null, $LinkSequenceLinkLive = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $LinkSequenceID, $LinkBankID, $LinkSequenceLinkName, $LinkSequenceLinkType, $LinkSequenceLinkURL, $EndDateType, $EndDate, $EndTime, $LinkSequenceLinkPosition, $DateAdded, $LinkSequenceLinkStatus, $LinkSequenceLinkLive);
            } else {
                $this->update($DBRow->id, $userid, $LinkSequenceID, $LinkBankID, $LinkSequenceLinkName, $LinkSequenceLinkType, $LinkSequenceLinkURL, $EndDateType, $EndDate, $EndTime, $LinkSequenceLinkPosition, $DateAdded, $LinkSequenceLinkStatus, $LinkSequenceLinkLive);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLSL')){
	function DBLSL($obj = null) {
		return clsDBLinkSequenceLinks::instance($obj);
	}
}