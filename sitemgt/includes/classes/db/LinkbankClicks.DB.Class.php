<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkbankClicks'))
	include_once 'struct/LinkbankClicks.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkbankClicks')){
	class clsDBLinkbankClicks extends clsStructLinkbankClicks {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->linkbank_clicks." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkbankClicks|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_clicks->Asterisk." from ".DB()->linkbank_clicks;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkbankClicks|null
         */
        function get_by_key($id){
            $Condition = DB()->linkbank_clicks->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkbankClicks|null
         */
        function get_row($id = null, $LinkBankID = null, $LinkType = null, $DateAdded = null, $ClickType = null, $ClickIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkType !== null) $data["LinkType"] = $LinkType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ClickType !== null) $data["ClickType"] = $ClickType;
			if($ClickIp !== null) $data["ClickIp"] = $ClickIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->linkbank_clicks->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkbankClicks[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_clicks->Asterisk." from ".DB()->linkbank_clicks;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->linkbank_clicks.".".$GroupBy." ORDER BY ".DB()->linkbank_clicks.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->linkbank_clicks->id.") from ".DB()->linkbank_clicks;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkbankClicks[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->linkbank_clicks;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->linkbank_clicks->id.") ".$sql;
            $sql = "select ".DB()->linkbank_clicks->Asterisk.$sql." group by ".DB()->linkbank_clicks.".".$GroupBy." ORDER BY ".DB()->linkbank_clicks.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($LinkBankID = null, $LinkType = null, $DateAdded = null, $ClickType = null, $ClickIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Inserted = false;

            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkType !== null) $data["LinkType"] = $LinkType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ClickType !== null) $data["ClickType"] = $ClickType;
			if($ClickIp !== null) $data["ClickIp"] = $ClickIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->linkbank_clicks, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $LinkBankID = null, $LinkType = null, $DateAdded = null, $ClickType = null, $ClickIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Updated = false;

            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkType !== null) $data["LinkType"] = $LinkType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ClickType !== null) $data["ClickType"] = $ClickType;
			if($ClickIp !== null) $data["ClickIp"] = $ClickIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->linkbank_clicks, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $LinkBankID = null, $LinkType = null, $DateAdded = null, $ClickType = null, $ClickIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($LinkType !== null) $data["LinkType"] = $LinkType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ClickType !== null) $data["ClickType"] = $ClickType;
			if($ClickIp !== null) $data["ClickIp"] = $ClickIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->linkbank_clicks, $data);
            }

            return $Deleted;
        }

        function save($Condition, $LinkBankID = null, $LinkType = null, $DateAdded = null, $ClickType = null, $ClickIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($LinkBankID, $LinkType, $DateAdded, $ClickType, $ClickIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID);
            } else {
                $this->update($DBRow->id, $LinkBankID, $LinkType, $DateAdded, $ClickType, $ClickIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLICL')){
	function DBLICL($obj = null) {
		return clsDBLinkbankClicks::instance($obj);
	}
}