<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructPaidtraffics'))
	include_once 'struct/Paidtraffics.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBPaidtraffics')){
	class clsDBPaidtraffics extends clsStructPaidtraffics {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->paidtraffics." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBPaidtraffics|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->paidtraffics->Asterisk." from ".DB()->paidtraffics;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBPaidtraffics|null
         */
        function get_by_key($id){
            $Condition = DB()->paidtraffics->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBPaidtraffics|null
         */
        function get_row($id = null, $userid = null, $VendorID = null, $PaidTrafficName = null, $PaidTrafficAccessKey = null, $DestinationURL = null, $TrafficCost = null, $TrafficCostType = null, $NoOfClicks = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $PaidTrafficStatus = null, $PaidTrafficActive = null, $TrackingCodeForActions = null, $PixelID = null, $UnitPriceSales = null, $TrackingCodeForSalesAndConversions = null, $AdditionalNotes = null, $PageImage = null, $DateAdded = null, $ManualSalesEntry = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $EasyLinkID = null, $ReferenceID = null, $ShowInAdmin = null, $TrafficQuality = null, $LeadScore = null, $TimeDelivered = null, $OverallDeliverability = null, $VendorOverallScore = null, $SaleQuality = null, $OverallProfitability = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaidTrafficName !== null) $data["PaidTrafficName"] = $PaidTrafficName;
			if($PaidTrafficAccessKey !== null) $data["PaidTrafficAccessKey"] = $PaidTrafficAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($TrafficCost !== null) $data["TrafficCost"] = $TrafficCost;
			if($TrafficCostType !== null) $data["TrafficCostType"] = $TrafficCostType;
			if($NoOfClicks !== null) $data["NoOfClicks"] = $NoOfClicks;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($PaidTrafficStatus !== null) $data["PaidTrafficStatus"] = $PaidTrafficStatus;
			if($PaidTrafficActive !== null) $data["PaidTrafficActive"] = $PaidTrafficActive;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($EasyLinkID !== null) $data["EasyLinkID"] = $EasyLinkID;
			if($ReferenceID !== null) $data["ReferenceID"] = $ReferenceID;
			if($ShowInAdmin !== null) $data["ShowInAdmin"] = $ShowInAdmin;
			if($TrafficQuality !== null) $data["TrafficQuality"] = $TrafficQuality;
			if($LeadScore !== null) $data["LeadScore"] = $LeadScore;
			if($TimeDelivered !== null) $data["TimeDelivered"] = $TimeDelivered;
			if($OverallDeliverability !== null) $data["OverallDeliverability"] = $OverallDeliverability;
			if($VendorOverallScore !== null) $data["VendorOverallScore"] = $VendorOverallScore;
			if($SaleQuality !== null) $data["SaleQuality"] = $SaleQuality;
			if($OverallProfitability !== null) $data["OverallProfitability"] = $OverallProfitability;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->paidtraffics->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBPaidtraffics[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->paidtraffics->Asterisk." from ".DB()->paidtraffics;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->paidtraffics.".".$GroupBy." ORDER BY ".DB()->paidtraffics.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->paidtraffics->id.") from ".DB()->paidtraffics;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBPaidtraffics[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->paidtraffics;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->paidtraffics->id.") ".$sql;
            $sql = "select ".DB()->paidtraffics->Asterisk.$sql." group by ".DB()->paidtraffics.".".$GroupBy." ORDER BY ".DB()->paidtraffics.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $VendorID = null, $PaidTrafficName = null, $PaidTrafficAccessKey = null, $DestinationURL = null, $TrafficCost = null, $TrafficCostType = null, $NoOfClicks = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $PaidTrafficStatus = null, $PaidTrafficActive = null, $TrackingCodeForActions = null, $PixelID = null, $UnitPriceSales = null, $TrackingCodeForSalesAndConversions = null, $AdditionalNotes = null, $PageImage = null, $DateAdded = null, $ManualSalesEntry = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $EasyLinkID = null, $ReferenceID = null, $ShowInAdmin = null, $TrafficQuality = null, $LeadScore = null, $TimeDelivered = null, $OverallDeliverability = null, $VendorOverallScore = null, $SaleQuality = null, $OverallProfitability = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaidTrafficName !== null) $data["PaidTrafficName"] = $PaidTrafficName;
			if($PaidTrafficAccessKey !== null) $data["PaidTrafficAccessKey"] = $PaidTrafficAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($TrafficCost !== null) $data["TrafficCost"] = $TrafficCost;
			if($TrafficCostType !== null) $data["TrafficCostType"] = $TrafficCostType;
			if($NoOfClicks !== null) $data["NoOfClicks"] = $NoOfClicks;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($PaidTrafficStatus !== null) $data["PaidTrafficStatus"] = $PaidTrafficStatus;
			if($PaidTrafficActive !== null) $data["PaidTrafficActive"] = $PaidTrafficActive;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($EasyLinkID !== null) $data["EasyLinkID"] = $EasyLinkID;
			if($ReferenceID !== null) $data["ReferenceID"] = $ReferenceID;
			if($ShowInAdmin !== null) $data["ShowInAdmin"] = $ShowInAdmin;
			if($TrafficQuality !== null) $data["TrafficQuality"] = $TrafficQuality;
			if($LeadScore !== null) $data["LeadScore"] = $LeadScore;
			if($TimeDelivered !== null) $data["TimeDelivered"] = $TimeDelivered;
			if($OverallDeliverability !== null) $data["OverallDeliverability"] = $OverallDeliverability;
			if($VendorOverallScore !== null) $data["VendorOverallScore"] = $VendorOverallScore;
			if($SaleQuality !== null) $data["SaleQuality"] = $SaleQuality;
			if($OverallProfitability !== null) $data["OverallProfitability"] = $OverallProfitability;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->paidtraffics, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $VendorID = null, $PaidTrafficName = null, $PaidTrafficAccessKey = null, $DestinationURL = null, $TrafficCost = null, $TrafficCostType = null, $NoOfClicks = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $PaidTrafficStatus = null, $PaidTrafficActive = null, $TrackingCodeForActions = null, $PixelID = null, $UnitPriceSales = null, $TrackingCodeForSalesAndConversions = null, $AdditionalNotes = null, $PageImage = null, $DateAdded = null, $ManualSalesEntry = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $EasyLinkID = null, $ReferenceID = null, $ShowInAdmin = null, $TrafficQuality = null, $LeadScore = null, $TimeDelivered = null, $OverallDeliverability = null, $VendorOverallScore = null, $SaleQuality = null, $OverallProfitability = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaidTrafficName !== null) $data["PaidTrafficName"] = $PaidTrafficName;
			if($PaidTrafficAccessKey !== null) $data["PaidTrafficAccessKey"] = $PaidTrafficAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($TrafficCost !== null) $data["TrafficCost"] = $TrafficCost;
			if($TrafficCostType !== null) $data["TrafficCostType"] = $TrafficCostType;
			if($NoOfClicks !== null) $data["NoOfClicks"] = $NoOfClicks;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($PaidTrafficStatus !== null) $data["PaidTrafficStatus"] = $PaidTrafficStatus;
			if($PaidTrafficActive !== null) $data["PaidTrafficActive"] = $PaidTrafficActive;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($EasyLinkID !== null) $data["EasyLinkID"] = $EasyLinkID;
			if($ReferenceID !== null) $data["ReferenceID"] = $ReferenceID;
			if($ShowInAdmin !== null) $data["ShowInAdmin"] = $ShowInAdmin;
			if($TrafficQuality !== null) $data["TrafficQuality"] = $TrafficQuality;
			if($LeadScore !== null) $data["LeadScore"] = $LeadScore;
			if($TimeDelivered !== null) $data["TimeDelivered"] = $TimeDelivered;
			if($OverallDeliverability !== null) $data["OverallDeliverability"] = $OverallDeliverability;
			if($VendorOverallScore !== null) $data["VendorOverallScore"] = $VendorOverallScore;
			if($SaleQuality !== null) $data["SaleQuality"] = $SaleQuality;
			if($OverallProfitability !== null) $data["OverallProfitability"] = $OverallProfitability;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->paidtraffics, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $VendorID = null, $PaidTrafficName = null, $PaidTrafficAccessKey = null, $DestinationURL = null, $TrafficCost = null, $TrafficCostType = null, $NoOfClicks = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $PaidTrafficStatus = null, $PaidTrafficActive = null, $TrackingCodeForActions = null, $PixelID = null, $UnitPriceSales = null, $TrackingCodeForSalesAndConversions = null, $AdditionalNotes = null, $PageImage = null, $DateAdded = null, $ManualSalesEntry = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $EasyLinkID = null, $ReferenceID = null, $ShowInAdmin = null, $TrafficQuality = null, $LeadScore = null, $TimeDelivered = null, $OverallDeliverability = null, $VendorOverallScore = null, $SaleQuality = null, $OverallProfitability = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaidTrafficName !== null) $data["PaidTrafficName"] = $PaidTrafficName;
			if($PaidTrafficAccessKey !== null) $data["PaidTrafficAccessKey"] = $PaidTrafficAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($TrafficCost !== null) $data["TrafficCost"] = $TrafficCost;
			if($TrafficCostType !== null) $data["TrafficCostType"] = $TrafficCostType;
			if($NoOfClicks !== null) $data["NoOfClicks"] = $NoOfClicks;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($PaidTrafficStatus !== null) $data["PaidTrafficStatus"] = $PaidTrafficStatus;
			if($PaidTrafficActive !== null) $data["PaidTrafficActive"] = $PaidTrafficActive;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($EasyLinkID !== null) $data["EasyLinkID"] = $EasyLinkID;
			if($ReferenceID !== null) $data["ReferenceID"] = $ReferenceID;
			if($ShowInAdmin !== null) $data["ShowInAdmin"] = $ShowInAdmin;
			if($TrafficQuality !== null) $data["TrafficQuality"] = $TrafficQuality;
			if($LeadScore !== null) $data["LeadScore"] = $LeadScore;
			if($TimeDelivered !== null) $data["TimeDelivered"] = $TimeDelivered;
			if($OverallDeliverability !== null) $data["OverallDeliverability"] = $OverallDeliverability;
			if($VendorOverallScore !== null) $data["VendorOverallScore"] = $VendorOverallScore;
			if($SaleQuality !== null) $data["SaleQuality"] = $SaleQuality;
			if($OverallProfitability !== null) $data["OverallProfitability"] = $OverallProfitability;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->paidtraffics, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $VendorID = null, $PaidTrafficName = null, $PaidTrafficAccessKey = null, $DestinationURL = null, $TrafficCost = null, $TrafficCostType = null, $NoOfClicks = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $PaidTrafficStatus = null, $PaidTrafficActive = null, $TrackingCodeForActions = null, $PixelID = null, $UnitPriceSales = null, $TrackingCodeForSalesAndConversions = null, $AdditionalNotes = null, $PageImage = null, $DateAdded = null, $ManualSalesEntry = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $EasyLinkID = null, $ReferenceID = null, $ShowInAdmin = null, $TrafficQuality = null, $LeadScore = null, $TimeDelivered = null, $OverallDeliverability = null, $VendorOverallScore = null, $SaleQuality = null, $OverallProfitability = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $VendorID, $PaidTrafficName, $PaidTrafficAccessKey, $DestinationURL, $TrafficCost, $TrafficCostType, $NoOfClicks, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $PaidTrafficStatus, $PaidTrafficActive, $TrackingCodeForActions, $PixelID, $UnitPriceSales, $TrackingCodeForSalesAndConversions, $AdditionalNotes, $PageImage, $DateAdded, $ManualSalesEntry, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $EasyLinkID, $ReferenceID, $ShowInAdmin, $TrafficQuality, $LeadScore, $TimeDelivered, $OverallDeliverability, $VendorOverallScore, $SaleQuality, $OverallProfitability);
            } else {
                $this->update($DBRow->id, $userid, $VendorID, $PaidTrafficName, $PaidTrafficAccessKey, $DestinationURL, $TrafficCost, $TrafficCostType, $NoOfClicks, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $PaidTrafficStatus, $PaidTrafficActive, $TrackingCodeForActions, $PixelID, $UnitPriceSales, $TrackingCodeForSalesAndConversions, $AdditionalNotes, $PageImage, $DateAdded, $ManualSalesEntry, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $EasyLinkID, $ReferenceID, $ShowInAdmin, $TrafficQuality, $LeadScore, $TimeDelivered, $OverallDeliverability, $VendorOverallScore, $SaleQuality, $OverallProfitability);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBP')){
	function DBP($obj = null) {
		return clsDBPaidtraffics::instance($obj);
	}
}