<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkbankConversions'))
	include_once 'struct/LinkbankConversions.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkbankConversions')){
	class clsDBLinkbankConversions extends clsStructLinkbankConversions {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->linkbank_conversions." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkbankConversions|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_conversions->Asterisk." from ".DB()->linkbank_conversions;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkbankConversions|null
         */
        function get_by_key($id){
            $Condition = DB()->linkbank_conversions->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkbankConversions|null
         */
        function get_row($id = null, $LinkBankID = null, $DateAdded = null, $ConversionType = null, $UnitPrice = null, $ConversionIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null, $LinkBankConversionPixelID = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ConversionType !== null) $data["ConversionType"] = $ConversionType;
			if($UnitPrice !== null) $data["UnitPrice"] = $UnitPrice;
			if($ConversionIp !== null) $data["ConversionIp"] = $ConversionIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;
			if($LinkBankConversionPixelID !== null) $data["LinkBankConversionPixelID"] = $LinkBankConversionPixelID;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->linkbank_conversions->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkbankConversions[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_conversions->Asterisk." from ".DB()->linkbank_conversions;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->linkbank_conversions.".".$GroupBy." ORDER BY ".DB()->linkbank_conversions.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->linkbank_conversions->id.") from ".DB()->linkbank_conversions;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkbankConversions[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->linkbank_conversions;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->linkbank_conversions->id.") ".$sql;
            $sql = "select ".DB()->linkbank_conversions->Asterisk.$sql." group by ".DB()->linkbank_conversions.".".$GroupBy." ORDER BY ".DB()->linkbank_conversions.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($LinkBankID = null, $DateAdded = null, $ConversionType = null, $UnitPrice = null, $ConversionIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null, $LinkBankConversionPixelID = null){
            $data = array();
            $Inserted = false;

            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ConversionType !== null) $data["ConversionType"] = $ConversionType;
			if($UnitPrice !== null) $data["UnitPrice"] = $UnitPrice;
			if($ConversionIp !== null) $data["ConversionIp"] = $ConversionIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;
			if($LinkBankConversionPixelID !== null) $data["LinkBankConversionPixelID"] = $LinkBankConversionPixelID;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->linkbank_conversions, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $LinkBankID = null, $DateAdded = null, $ConversionType = null, $UnitPrice = null, $ConversionIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null, $LinkBankConversionPixelID = null){
            $data = array();
            $Updated = false;

            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ConversionType !== null) $data["ConversionType"] = $ConversionType;
			if($UnitPrice !== null) $data["UnitPrice"] = $UnitPrice;
			if($ConversionIp !== null) $data["ConversionIp"] = $ConversionIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;
			if($LinkBankConversionPixelID !== null) $data["LinkBankConversionPixelID"] = $LinkBankConversionPixelID;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->linkbank_conversions, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $LinkBankID = null, $DateAdded = null, $ConversionType = null, $UnitPrice = null, $ConversionIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null, $LinkBankConversionPixelID = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($ConversionType !== null) $data["ConversionType"] = $ConversionType;
			if($UnitPrice !== null) $data["UnitPrice"] = $UnitPrice;
			if($ConversionIp !== null) $data["ConversionIp"] = $ConversionIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;
			if($LinkBankConversionPixelID !== null) $data["LinkBankConversionPixelID"] = $LinkBankConversionPixelID;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->linkbank_conversions, $data);
            }

            return $Deleted;
        }

        function save($Condition, $LinkBankID = null, $DateAdded = null, $ConversionType = null, $UnitPrice = null, $ConversionIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null, $LinkBankConversionPixelID = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($LinkBankID, $DateAdded, $ConversionType, $UnitPrice, $ConversionIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID, $LinkBankConversionPixelID);
            } else {
                $this->update($DBRow->id, $LinkBankID, $DateAdded, $ConversionType, $UnitPrice, $ConversionIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID, $LinkBankConversionPixelID);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLICO')){
	function DBLICO($obj = null) {
		return clsDBLinkbankConversions::instance($obj);
	}
}