<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLevelMenuItems'))
	include_once 'struct/LevelMenuItems.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLevelMenuItems')){
	class clsDBLevelMenuItems extends clsStructLevelMenuItems {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->level_menu_items." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLevelMenuItems|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->level_menu_items->Asterisk." from ".DB()->level_menu_items;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLevelMenuItems|null
         */
        function get_by_key($id){
            $Condition = DB()->level_menu_items->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLevelMenuItems|null
         */
        function get_row($id = null, $LevelID = null, $MenuItemID = null, $LevelMenuItemEnabled = null, $LevelMenuItemLocked = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($MenuItemID !== null) $data["MenuItemID"] = $MenuItemID;
			if($LevelMenuItemEnabled !== null) $data["LevelMenuItemEnabled"] = $LevelMenuItemEnabled;
			if($LevelMenuItemLocked !== null) $data["LevelMenuItemLocked"] = $LevelMenuItemLocked;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->level_menu_items->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLevelMenuItems[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->level_menu_items->Asterisk." from ".DB()->level_menu_items;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->level_menu_items.".".$GroupBy." ORDER BY ".DB()->level_menu_items.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->level_menu_items->id.") from ".DB()->level_menu_items;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLevelMenuItems[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->level_menu_items;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->level_menu_items->id.") ".$sql;
            $sql = "select ".DB()->level_menu_items->Asterisk.$sql." group by ".DB()->level_menu_items.".".$GroupBy." ORDER BY ".DB()->level_menu_items.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($LevelID = null, $MenuItemID = null, $LevelMenuItemEnabled = null, $LevelMenuItemLocked = null){
            $data = array();
            $Inserted = false;

            if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($MenuItemID !== null) $data["MenuItemID"] = $MenuItemID;
			if($LevelMenuItemEnabled !== null) $data["LevelMenuItemEnabled"] = $LevelMenuItemEnabled;
			if($LevelMenuItemLocked !== null) $data["LevelMenuItemLocked"] = $LevelMenuItemLocked;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->level_menu_items, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $LevelID = null, $MenuItemID = null, $LevelMenuItemEnabled = null, $LevelMenuItemLocked = null){
            $data = array();
            $Updated = false;

            if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($MenuItemID !== null) $data["MenuItemID"] = $MenuItemID;
			if($LevelMenuItemEnabled !== null) $data["LevelMenuItemEnabled"] = $LevelMenuItemEnabled;
			if($LevelMenuItemLocked !== null) $data["LevelMenuItemLocked"] = $LevelMenuItemLocked;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->level_menu_items, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $LevelID = null, $MenuItemID = null, $LevelMenuItemEnabled = null, $LevelMenuItemLocked = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($MenuItemID !== null) $data["MenuItemID"] = $MenuItemID;
			if($LevelMenuItemEnabled !== null) $data["LevelMenuItemEnabled"] = $LevelMenuItemEnabled;
			if($LevelMenuItemLocked !== null) $data["LevelMenuItemLocked"] = $LevelMenuItemLocked;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->level_menu_items, $data);
            }

            return $Deleted;
        }

        function save($Condition, $LevelID = null, $MenuItemID = null, $LevelMenuItemEnabled = null, $LevelMenuItemLocked = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($LevelID, $MenuItemID, $LevelMenuItemEnabled, $LevelMenuItemLocked);
            } else {
                $this->update($DBRow->id, $LevelID, $MenuItemID, $LevelMenuItemEnabled, $LevelMenuItemLocked);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLMI')){
	function DBLMI($obj = null) {
		return clsDBLevelMenuItems::instance($obj);
	}
}