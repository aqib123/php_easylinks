<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructFeatures'))
	include_once 'struct/Features.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBFeatures')){
	class clsDBFeatures extends clsStructFeatures {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->features." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBFeatures|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->features->Asterisk." from ".DB()->features;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBFeatures|null
         */
        function get_by_key($id){
            $Condition = DB()->features->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBFeatures|null
         */
        function get_row($id = null, $FeatureType = null, $FeatureName = null, $FeatureAccessKey = null, $FeatureLabel = null, $FeatureOrder = null, $FeatureEnabled = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($FeatureType !== null) $data["FeatureType"] = $FeatureType;
			if($FeatureName !== null) $data["FeatureName"] = $FeatureName;
			if($FeatureAccessKey !== null) $data["FeatureAccessKey"] = $FeatureAccessKey;
			if($FeatureLabel !== null) $data["FeatureLabel"] = $FeatureLabel;
			if($FeatureOrder !== null) $data["FeatureOrder"] = $FeatureOrder;
			if($FeatureEnabled !== null) $data["FeatureEnabled"] = $FeatureEnabled;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->features->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBFeatures[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->features->Asterisk." from ".DB()->features;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->features.".".$GroupBy." ORDER BY ".DB()->features.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->features->id.") from ".DB()->features;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBFeatures[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->features;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->features->id.") ".$sql;
            $sql = "select ".DB()->features->Asterisk.$sql." group by ".DB()->features.".".$GroupBy." ORDER BY ".DB()->features.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($FeatureType = null, $FeatureName = null, $FeatureAccessKey = null, $FeatureLabel = null, $FeatureOrder = null, $FeatureEnabled = null){
            $data = array();
            $Inserted = false;

            if($FeatureType !== null) $data["FeatureType"] = $FeatureType;
			if($FeatureName !== null) $data["FeatureName"] = $FeatureName;
			if($FeatureAccessKey !== null) $data["FeatureAccessKey"] = $FeatureAccessKey;
			if($FeatureLabel !== null) $data["FeatureLabel"] = $FeatureLabel;
			if($FeatureOrder !== null) $data["FeatureOrder"] = $FeatureOrder;
			if($FeatureEnabled !== null) $data["FeatureEnabled"] = $FeatureEnabled;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->features, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $FeatureType = null, $FeatureName = null, $FeatureAccessKey = null, $FeatureLabel = null, $FeatureOrder = null, $FeatureEnabled = null){
            $data = array();
            $Updated = false;

            if($FeatureType !== null) $data["FeatureType"] = $FeatureType;
			if($FeatureName !== null) $data["FeatureName"] = $FeatureName;
			if($FeatureAccessKey !== null) $data["FeatureAccessKey"] = $FeatureAccessKey;
			if($FeatureLabel !== null) $data["FeatureLabel"] = $FeatureLabel;
			if($FeatureOrder !== null) $data["FeatureOrder"] = $FeatureOrder;
			if($FeatureEnabled !== null) $data["FeatureEnabled"] = $FeatureEnabled;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->features, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $FeatureType = null, $FeatureName = null, $FeatureAccessKey = null, $FeatureLabel = null, $FeatureOrder = null, $FeatureEnabled = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($FeatureType !== null) $data["FeatureType"] = $FeatureType;
			if($FeatureName !== null) $data["FeatureName"] = $FeatureName;
			if($FeatureAccessKey !== null) $data["FeatureAccessKey"] = $FeatureAccessKey;
			if($FeatureLabel !== null) $data["FeatureLabel"] = $FeatureLabel;
			if($FeatureOrder !== null) $data["FeatureOrder"] = $FeatureOrder;
			if($FeatureEnabled !== null) $data["FeatureEnabled"] = $FeatureEnabled;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->features, $data);
            }

            return $Deleted;
        }

        function save($Condition, $FeatureType = null, $FeatureName = null, $FeatureAccessKey = null, $FeatureLabel = null, $FeatureOrder = null, $FeatureEnabled = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($FeatureType, $FeatureName, $FeatureAccessKey, $FeatureLabel, $FeatureOrder, $FeatureEnabled);
            } else {
                $this->update($DBRow->id, $FeatureType, $FeatureName, $FeatureAccessKey, $FeatureLabel, $FeatureOrder, $FeatureEnabled);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBF')){
	function DBF($obj = null) {
		return clsDBFeatures::instance($obj);
	}
}