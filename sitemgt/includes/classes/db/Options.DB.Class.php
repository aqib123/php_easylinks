<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructOptions'))
	include_once 'struct/Options.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBOptions')){
	class clsDBOptions extends clsStructOptions {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->options." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBOptions|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->options->Asterisk." from ".DB()->options;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBOptions|null
         */
        function get_by_key($id){
            $Condition = DB()->options->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBOptions|null
         */
        function get_row($id = null, $OptionName = null, $OptionValue = null, $OptionEnabled = null, $OptionDefault = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($OptionName !== null) $data["OptionName"] = $OptionName;
			if($OptionValue !== null) $data["OptionValue"] = $OptionValue;
			if($OptionEnabled !== null) $data["OptionEnabled"] = $OptionEnabled;
			if($OptionDefault !== null) $data["OptionDefault"] = $OptionDefault;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->options->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBOptions[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->options->Asterisk." from ".DB()->options;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->options.".".$GroupBy." ORDER BY ".DB()->options.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->options->id.") from ".DB()->options;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBOptions[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->options;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->options->id.") ".$sql;
            $sql = "select ".DB()->options->Asterisk.$sql." group by ".DB()->options.".".$GroupBy." ORDER BY ".DB()->options.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($OptionName = null, $OptionValue = null, $OptionEnabled = null, $OptionDefault = null){
            $data = array();
            $Inserted = false;

            if($OptionName !== null) $data["OptionName"] = $OptionName;
			if($OptionValue !== null) $data["OptionValue"] = $OptionValue;
			if($OptionEnabled !== null) $data["OptionEnabled"] = $OptionEnabled;
			if($OptionDefault !== null) $data["OptionDefault"] = $OptionDefault;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->options, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $OptionName = null, $OptionValue = null, $OptionEnabled = null, $OptionDefault = null){
            $data = array();
            $Updated = false;

            if($OptionName !== null) $data["OptionName"] = $OptionName;
			if($OptionValue !== null) $data["OptionValue"] = $OptionValue;
			if($OptionEnabled !== null) $data["OptionEnabled"] = $OptionEnabled;
			if($OptionDefault !== null) $data["OptionDefault"] = $OptionDefault;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->options, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $OptionName = null, $OptionValue = null, $OptionEnabled = null, $OptionDefault = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($OptionName !== null) $data["OptionName"] = $OptionName;
			if($OptionValue !== null) $data["OptionValue"] = $OptionValue;
			if($OptionEnabled !== null) $data["OptionEnabled"] = $OptionEnabled;
			if($OptionDefault !== null) $data["OptionDefault"] = $OptionDefault;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->options, $data);
            }

            return $Deleted;
        }

        function save($Condition, $OptionName = null, $OptionValue = null, $OptionEnabled = null, $OptionDefault = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($OptionName, $OptionValue, $OptionEnabled, $OptionDefault);
            } else {
                $this->update($DBRow->id, $OptionName, $OptionValue, $OptionEnabled, $OptionDefault);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBO')){
	function DBO($obj = null) {
		return clsDBOptions::instance($obj);
	}
}