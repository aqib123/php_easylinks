<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkbankSaleinfo'))
	include_once 'struct/LinkbankSaleinfo.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkbankSaleinfo')){
	class clsDBLinkbankSaleinfo extends clsStructLinkbankSaleinfo {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->linkbank_saleinfo." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkbankSaleinfo|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_saleinfo->Asterisk." from ".DB()->linkbank_saleinfo;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkbankSaleinfo|null
         */
        function get_by_key($id){
            $Condition = DB()->linkbank_saleinfo->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkbankSaleinfo|null
         */
        function get_row($id = null, $userid = null, $LinkBankID = null, $SaleInfoType = null, $SaleInfoTitle = null, $isPartnerAmount = null, $SaleInfoAmount = null, $DateAdded = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SaleInfoType !== null) $data["SaleInfoType"] = $SaleInfoType;
			if($SaleInfoTitle !== null) $data["SaleInfoTitle"] = $SaleInfoTitle;
			if($isPartnerAmount !== null) $data["isPartnerAmount"] = $isPartnerAmount;
			if($SaleInfoAmount !== null) $data["SaleInfoAmount"] = $SaleInfoAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->linkbank_saleinfo->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkbankSaleinfo[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbank_saleinfo->Asterisk." from ".DB()->linkbank_saleinfo;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->linkbank_saleinfo.".".$GroupBy." ORDER BY ".DB()->linkbank_saleinfo.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->linkbank_saleinfo->id.") from ".DB()->linkbank_saleinfo;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkbankSaleinfo[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->linkbank_saleinfo;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->linkbank_saleinfo->id.") ".$sql;
            $sql = "select ".DB()->linkbank_saleinfo->Asterisk.$sql." group by ".DB()->linkbank_saleinfo.".".$GroupBy." ORDER BY ".DB()->linkbank_saleinfo.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $LinkBankID = null, $SaleInfoType = null, $SaleInfoTitle = null, $isPartnerAmount = null, $SaleInfoAmount = null, $DateAdded = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SaleInfoType !== null) $data["SaleInfoType"] = $SaleInfoType;
			if($SaleInfoTitle !== null) $data["SaleInfoTitle"] = $SaleInfoTitle;
			if($isPartnerAmount !== null) $data["isPartnerAmount"] = $isPartnerAmount;
			if($SaleInfoAmount !== null) $data["SaleInfoAmount"] = $SaleInfoAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->linkbank_saleinfo, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $LinkBankID = null, $SaleInfoType = null, $SaleInfoTitle = null, $isPartnerAmount = null, $SaleInfoAmount = null, $DateAdded = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SaleInfoType !== null) $data["SaleInfoType"] = $SaleInfoType;
			if($SaleInfoTitle !== null) $data["SaleInfoTitle"] = $SaleInfoTitle;
			if($isPartnerAmount !== null) $data["isPartnerAmount"] = $isPartnerAmount;
			if($SaleInfoAmount !== null) $data["SaleInfoAmount"] = $SaleInfoAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->linkbank_saleinfo, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $LinkBankID = null, $SaleInfoType = null, $SaleInfoTitle = null, $isPartnerAmount = null, $SaleInfoAmount = null, $DateAdded = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SaleInfoType !== null) $data["SaleInfoType"] = $SaleInfoType;
			if($SaleInfoTitle !== null) $data["SaleInfoTitle"] = $SaleInfoTitle;
			if($isPartnerAmount !== null) $data["isPartnerAmount"] = $isPartnerAmount;
			if($SaleInfoAmount !== null) $data["SaleInfoAmount"] = $SaleInfoAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->linkbank_saleinfo, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $LinkBankID = null, $SaleInfoType = null, $SaleInfoTitle = null, $isPartnerAmount = null, $SaleInfoAmount = null, $DateAdded = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $LinkBankID, $SaleInfoType, $SaleInfoTitle, $isPartnerAmount, $SaleInfoAmount, $DateAdded);
            } else {
                $this->update($DBRow->id, $userid, $LinkBankID, $SaleInfoType, $SaleInfoTitle, $isPartnerAmount, $SaleInfoAmount, $DateAdded);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLISA')){
	function DBLISA($obj = null) {
		return clsDBLinkbankSaleinfo::instance($obj);
	}
}