<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructRotators'))
	include_once 'struct/Rotators.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBRotators')){
	class clsDBRotators extends clsStructRotators {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->rotators." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBRotators|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->rotators->Asterisk." from ".DB()->rotators;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBRotators|null
         */
        function get_by_key($id){
            $Condition = DB()->rotators->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBRotators|null
         */
        function get_row($id = null, $userid = null, $RotatorName = null, $RoratorAccessKey = null, $GroupID = null, $RotatorType = null, $RotatorIsSticky = null, $DateAdded = null, $hasSingleLink = null, $RotatorStatus = null, $UseAdminDomain = null, $DomainID = null, $VisibleLink = null, $LastRotatorVisitID = null, $MasterCampaignID = null, $CloakURL = null, $LastSplitRotatorLinkPosition = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($RotatorName !== null) $data["RotatorName"] = $RotatorName;
			if($RoratorAccessKey !== null) $data["RoratorAccessKey"] = $RoratorAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($RotatorType !== null) $data["RotatorType"] = $RotatorType;
			if($RotatorIsSticky !== null) $data["RotatorIsSticky"] = $RotatorIsSticky;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleLink !== null) $data["hasSingleLink"] = $hasSingleLink;
			if($RotatorStatus !== null) $data["RotatorStatus"] = $RotatorStatus;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($LastRotatorVisitID !== null) $data["LastRotatorVisitID"] = $LastRotatorVisitID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($LastSplitRotatorLinkPosition !== null) $data["LastSplitRotatorLinkPosition"] = $LastSplitRotatorLinkPosition;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->rotators->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBRotators[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->rotators->Asterisk." from ".DB()->rotators;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->rotators.".".$GroupBy." ORDER BY ".DB()->rotators.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->rotators->id.") from ".DB()->rotators;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBRotators[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->rotators;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->rotators->id.") ".$sql;
            $sql = "select ".DB()->rotators->Asterisk.$sql." group by ".DB()->rotators.".".$GroupBy." ORDER BY ".DB()->rotators.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $RotatorName = null, $RoratorAccessKey = null, $GroupID = null, $RotatorType = null, $RotatorIsSticky = null, $DateAdded = null, $hasSingleLink = null, $RotatorStatus = null, $UseAdminDomain = null, $DomainID = null, $VisibleLink = null, $LastRotatorVisitID = null, $MasterCampaignID = null, $CloakURL = null, $LastSplitRotatorLinkPosition = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($RotatorName !== null) $data["RotatorName"] = $RotatorName;
			if($RoratorAccessKey !== null) $data["RoratorAccessKey"] = $RoratorAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($RotatorType !== null) $data["RotatorType"] = $RotatorType;
			if($RotatorIsSticky !== null) $data["RotatorIsSticky"] = $RotatorIsSticky;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleLink !== null) $data["hasSingleLink"] = $hasSingleLink;
			if($RotatorStatus !== null) $data["RotatorStatus"] = $RotatorStatus;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($LastRotatorVisitID !== null) $data["LastRotatorVisitID"] = $LastRotatorVisitID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($LastSplitRotatorLinkPosition !== null) $data["LastSplitRotatorLinkPosition"] = $LastSplitRotatorLinkPosition;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->rotators, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $RotatorName = null, $RoratorAccessKey = null, $GroupID = null, $RotatorType = null, $RotatorIsSticky = null, $DateAdded = null, $hasSingleLink = null, $RotatorStatus = null, $UseAdminDomain = null, $DomainID = null, $VisibleLink = null, $LastRotatorVisitID = null, $MasterCampaignID = null, $CloakURL = null, $LastSplitRotatorLinkPosition = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($RotatorName !== null) $data["RotatorName"] = $RotatorName;
			if($RoratorAccessKey !== null) $data["RoratorAccessKey"] = $RoratorAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($RotatorType !== null) $data["RotatorType"] = $RotatorType;
			if($RotatorIsSticky !== null) $data["RotatorIsSticky"] = $RotatorIsSticky;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleLink !== null) $data["hasSingleLink"] = $hasSingleLink;
			if($RotatorStatus !== null) $data["RotatorStatus"] = $RotatorStatus;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($LastRotatorVisitID !== null) $data["LastRotatorVisitID"] = $LastRotatorVisitID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($LastSplitRotatorLinkPosition !== null) $data["LastSplitRotatorLinkPosition"] = $LastSplitRotatorLinkPosition;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->rotators, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $RotatorName = null, $RoratorAccessKey = null, $GroupID = null, $RotatorType = null, $RotatorIsSticky = null, $DateAdded = null, $hasSingleLink = null, $RotatorStatus = null, $UseAdminDomain = null, $DomainID = null, $VisibleLink = null, $LastRotatorVisitID = null, $MasterCampaignID = null, $CloakURL = null, $LastSplitRotatorLinkPosition = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($RotatorName !== null) $data["RotatorName"] = $RotatorName;
			if($RoratorAccessKey !== null) $data["RoratorAccessKey"] = $RoratorAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($RotatorType !== null) $data["RotatorType"] = $RotatorType;
			if($RotatorIsSticky !== null) $data["RotatorIsSticky"] = $RotatorIsSticky;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleLink !== null) $data["hasSingleLink"] = $hasSingleLink;
			if($RotatorStatus !== null) $data["RotatorStatus"] = $RotatorStatus;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($LastRotatorVisitID !== null) $data["LastRotatorVisitID"] = $LastRotatorVisitID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($LastSplitRotatorLinkPosition !== null) $data["LastSplitRotatorLinkPosition"] = $LastSplitRotatorLinkPosition;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->rotators, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $RotatorName = null, $RoratorAccessKey = null, $GroupID = null, $RotatorType = null, $RotatorIsSticky = null, $DateAdded = null, $hasSingleLink = null, $RotatorStatus = null, $UseAdminDomain = null, $DomainID = null, $VisibleLink = null, $LastRotatorVisitID = null, $MasterCampaignID = null, $CloakURL = null, $LastSplitRotatorLinkPosition = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $RotatorName, $RoratorAccessKey, $GroupID, $RotatorType, $RotatorIsSticky, $DateAdded, $hasSingleLink, $RotatorStatus, $UseAdminDomain, $DomainID, $VisibleLink, $LastRotatorVisitID, $MasterCampaignID, $CloakURL, $LastSplitRotatorLinkPosition);
            } else {
                $this->update($DBRow->id, $userid, $RotatorName, $RoratorAccessKey, $GroupID, $RotatorType, $RotatorIsSticky, $DateAdded, $hasSingleLink, $RotatorStatus, $UseAdminDomain, $DomainID, $VisibleLink, $LastRotatorVisitID, $MasterCampaignID, $CloakURL, $LastSplitRotatorLinkPosition);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBR')){
	function DBR($obj = null) {
		return clsDBRotators::instance($obj);
	}
}