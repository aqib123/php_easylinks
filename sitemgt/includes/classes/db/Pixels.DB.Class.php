<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructPixels'))
	include_once 'struct/Pixels.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBPixels')){
	class clsDBPixels extends clsStructPixels {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->pixels." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBPixels|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->pixels->Asterisk." from ".DB()->pixels;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBPixels|null
         */
        function get_by_key($id){
            $Condition = DB()->pixels->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBPixels|null
         */
        function get_row($id = null, $userid = null, $PixelName = null, $PixelType = null, $PixelCode = null, $PixelAccessKey = null, $DateAdded = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($PixelName !== null) $data["PixelName"] = $PixelName;
			if($PixelType !== null) $data["PixelType"] = $PixelType;
			if($PixelCode !== null) $data["PixelCode"] = $PixelCode;
			if($PixelAccessKey !== null) $data["PixelAccessKey"] = $PixelAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->pixels->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBPixels[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->pixels->Asterisk." from ".DB()->pixels;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->pixels.".".$GroupBy." ORDER BY ".DB()->pixels.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->pixels->id.") from ".DB()->pixels;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBPixels[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->pixels;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->pixels->id.") ".$sql;
            $sql = "select ".DB()->pixels->Asterisk.$sql." group by ".DB()->pixels.".".$GroupBy." ORDER BY ".DB()->pixels.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $PixelName = null, $PixelType = null, $PixelCode = null, $PixelAccessKey = null, $DateAdded = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($PixelName !== null) $data["PixelName"] = $PixelName;
			if($PixelType !== null) $data["PixelType"] = $PixelType;
			if($PixelCode !== null) $data["PixelCode"] = $PixelCode;
			if($PixelAccessKey !== null) $data["PixelAccessKey"] = $PixelAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->pixels, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $PixelName = null, $PixelType = null, $PixelCode = null, $PixelAccessKey = null, $DateAdded = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($PixelName !== null) $data["PixelName"] = $PixelName;
			if($PixelType !== null) $data["PixelType"] = $PixelType;
			if($PixelCode !== null) $data["PixelCode"] = $PixelCode;
			if($PixelAccessKey !== null) $data["PixelAccessKey"] = $PixelAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->pixels, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $PixelName = null, $PixelType = null, $PixelCode = null, $PixelAccessKey = null, $DateAdded = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($PixelName !== null) $data["PixelName"] = $PixelName;
			if($PixelType !== null) $data["PixelType"] = $PixelType;
			if($PixelCode !== null) $data["PixelCode"] = $PixelCode;
			if($PixelAccessKey !== null) $data["PixelAccessKey"] = $PixelAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->pixels, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $PixelName = null, $PixelType = null, $PixelCode = null, $PixelAccessKey = null, $DateAdded = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $PixelName, $PixelType, $PixelCode, $PixelAccessKey, $DateAdded);
            } else {
                $this->update($DBRow->id, $userid, $PixelName, $PixelType, $PixelCode, $PixelAccessKey, $DateAdded);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBPI')){
	function DBPI($obj = null) {
		return clsDBPixels::instance($obj);
	}
}