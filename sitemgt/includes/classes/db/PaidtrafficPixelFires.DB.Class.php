<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructPaidtrafficPixelFires'))
	include_once 'struct/PaidtrafficPixelFires.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBPaidtrafficPixelFires')){
	class clsDBPaidtrafficPixelFires extends clsStructPaidtrafficPixelFires {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->paidtraffic_pixel_fires." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBPaidtrafficPixelFires|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->paidtraffic_pixel_fires->Asterisk." from ".DB()->paidtraffic_pixel_fires;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBPaidtrafficPixelFires|null
         */
        function get_by_key($id){
            $Condition = DB()->paidtraffic_pixel_fires->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBPaidtrafficPixelFires|null
         */
        function get_row($id = null, $PaidTrafficID = null, $PixelID = null, $FireType = null, $DateAdded = null, $FireIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($PaidTrafficID !== null) $data["PaidTrafficID"] = $PaidTrafficID;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($FireType !== null) $data["FireType"] = $FireType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($FireIp !== null) $data["FireIp"] = $FireIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->paidtraffic_pixel_fires->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBPaidtrafficPixelFires[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->paidtraffic_pixel_fires->Asterisk." from ".DB()->paidtraffic_pixel_fires;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->paidtraffic_pixel_fires.".".$GroupBy." ORDER BY ".DB()->paidtraffic_pixel_fires.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->paidtraffic_pixel_fires->id.") from ".DB()->paidtraffic_pixel_fires;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBPaidtrafficPixelFires[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->paidtraffic_pixel_fires;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->paidtraffic_pixel_fires->id.") ".$sql;
            $sql = "select ".DB()->paidtraffic_pixel_fires->Asterisk.$sql." group by ".DB()->paidtraffic_pixel_fires.".".$GroupBy." ORDER BY ".DB()->paidtraffic_pixel_fires.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($PaidTrafficID = null, $PixelID = null, $FireType = null, $DateAdded = null, $FireIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Inserted = false;

            if($PaidTrafficID !== null) $data["PaidTrafficID"] = $PaidTrafficID;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($FireType !== null) $data["FireType"] = $FireType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($FireIp !== null) $data["FireIp"] = $FireIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->paidtraffic_pixel_fires, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $PaidTrafficID = null, $PixelID = null, $FireType = null, $DateAdded = null, $FireIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Updated = false;

            if($PaidTrafficID !== null) $data["PaidTrafficID"] = $PaidTrafficID;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($FireType !== null) $data["FireType"] = $FireType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($FireIp !== null) $data["FireIp"] = $FireIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->paidtraffic_pixel_fires, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $PaidTrafficID = null, $PixelID = null, $FireType = null, $DateAdded = null, $FireIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($PaidTrafficID !== null) $data["PaidTrafficID"] = $PaidTrafficID;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($FireType !== null) $data["FireType"] = $FireType;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($FireIp !== null) $data["FireIp"] = $FireIp;
			if($CountryCode !== null) $data["CountryCode"] = $CountryCode;
			if($BrowserName !== null) $data["BrowserName"] = $BrowserName;
			if($BrowserVersion !== null) $data["BrowserVersion"] = $BrowserVersion;
			if($Platform !== null) $data["Platform"] = $Platform;
			if($BotName !== null) $data["BotName"] = $BotName;
			if($EmailID !== null) $data["EmailID"] = $EmailID;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->paidtraffic_pixel_fires, $data);
            }

            return $Deleted;
        }

        function save($Condition, $PaidTrafficID = null, $PixelID = null, $FireType = null, $DateAdded = null, $FireIp = null, $CountryCode = null, $BrowserName = null, $BrowserVersion = null, $Platform = null, $BotName = null, $EmailID = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($PaidTrafficID, $PixelID, $FireType, $DateAdded, $FireIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID);
            } else {
                $this->update($DBRow->id, $PaidTrafficID, $PixelID, $FireType, $DateAdded, $FireIp, $CountryCode, $BrowserName, $BrowserVersion, $Platform, $BotName, $EmailID);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBPPF')){
	function DBPPF($obj = null) {
		return clsDBPaidtrafficPixelFires::instance($obj);
	}
}