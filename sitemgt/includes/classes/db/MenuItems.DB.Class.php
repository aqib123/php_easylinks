<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructMenuItems'))
	include_once 'struct/MenuItems.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBMenuItems')){
	class clsDBMenuItems extends clsStructMenuItems {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->menu_items." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBMenuItems|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->menu_items->Asterisk." from ".DB()->menu_items;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBMenuItems|null
         */
        function get_by_key($id){
            $Condition = DB()->menu_items->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBMenuItems|null
         */
        function get_row($id = null, $MenuItemURL = null, $MenuItemClass = null, $MenuItemIcon = null, $MenuItemImage = null, $MenuItemColor = null, $MenuItemLabel = null, $MenuItemPosition = null, $MenuItemEnabled = null, $MenuItemParentID = null, $MenuItemHasAdmin = null, $MenuItemAlwaysAdd = null, $MenuItemExtendable = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($MenuItemURL !== null) $data["MenuItemURL"] = $MenuItemURL;
			if($MenuItemClass !== null) $data["MenuItemClass"] = $MenuItemClass;
			if($MenuItemIcon !== null) $data["MenuItemIcon"] = $MenuItemIcon;
			if($MenuItemImage !== null) $data["MenuItemImage"] = $MenuItemImage;
			if($MenuItemColor !== null) $data["MenuItemColor"] = $MenuItemColor;
			if($MenuItemLabel !== null) $data["MenuItemLabel"] = $MenuItemLabel;
			if($MenuItemPosition !== null) $data["MenuItemPosition"] = $MenuItemPosition;
			if($MenuItemEnabled !== null) $data["MenuItemEnabled"] = $MenuItemEnabled;
			if($MenuItemParentID !== null) $data["MenuItemParentID"] = $MenuItemParentID;
			if($MenuItemHasAdmin !== null) $data["MenuItemHasAdmin"] = $MenuItemHasAdmin;
			if($MenuItemAlwaysAdd !== null) $data["MenuItemAlwaysAdd"] = $MenuItemAlwaysAdd;
			if($MenuItemExtendable !== null) $data["MenuItemExtendable"] = $MenuItemExtendable;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->menu_items->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBMenuItems[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->menu_items->Asterisk." from ".DB()->menu_items;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->menu_items.".".$GroupBy." ORDER BY ".DB()->menu_items.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->menu_items->id.") from ".DB()->menu_items;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBMenuItems[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->menu_items;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->menu_items->id.") ".$sql;
            $sql = "select ".DB()->menu_items->Asterisk.$sql." group by ".DB()->menu_items.".".$GroupBy." ORDER BY ".DB()->menu_items.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($MenuItemURL = null, $MenuItemClass = null, $MenuItemIcon = null, $MenuItemImage = null, $MenuItemColor = null, $MenuItemLabel = null, $MenuItemPosition = null, $MenuItemEnabled = null, $MenuItemParentID = null, $MenuItemHasAdmin = null, $MenuItemAlwaysAdd = null, $MenuItemExtendable = null){
            $data = array();
            $Inserted = false;

            if($MenuItemURL !== null) $data["MenuItemURL"] = $MenuItemURL;
			if($MenuItemClass !== null) $data["MenuItemClass"] = $MenuItemClass;
			if($MenuItemIcon !== null) $data["MenuItemIcon"] = $MenuItemIcon;
			if($MenuItemImage !== null) $data["MenuItemImage"] = $MenuItemImage;
			if($MenuItemColor !== null) $data["MenuItemColor"] = $MenuItemColor;
			if($MenuItemLabel !== null) $data["MenuItemLabel"] = $MenuItemLabel;
			if($MenuItemPosition !== null) $data["MenuItemPosition"] = $MenuItemPosition;
			if($MenuItemEnabled !== null) $data["MenuItemEnabled"] = $MenuItemEnabled;
			if($MenuItemParentID !== null) $data["MenuItemParentID"] = $MenuItemParentID;
			if($MenuItemHasAdmin !== null) $data["MenuItemHasAdmin"] = $MenuItemHasAdmin;
			if($MenuItemAlwaysAdd !== null) $data["MenuItemAlwaysAdd"] = $MenuItemAlwaysAdd;
			if($MenuItemExtendable !== null) $data["MenuItemExtendable"] = $MenuItemExtendable;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->menu_items, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $MenuItemURL = null, $MenuItemClass = null, $MenuItemIcon = null, $MenuItemImage = null, $MenuItemColor = null, $MenuItemLabel = null, $MenuItemPosition = null, $MenuItemEnabled = null, $MenuItemParentID = null, $MenuItemHasAdmin = null, $MenuItemAlwaysAdd = null, $MenuItemExtendable = null){
            $data = array();
            $Updated = false;

            if($MenuItemURL !== null) $data["MenuItemURL"] = $MenuItemURL;
			if($MenuItemClass !== null) $data["MenuItemClass"] = $MenuItemClass;
			if($MenuItemIcon !== null) $data["MenuItemIcon"] = $MenuItemIcon;
			if($MenuItemImage !== null) $data["MenuItemImage"] = $MenuItemImage;
			if($MenuItemColor !== null) $data["MenuItemColor"] = $MenuItemColor;
			if($MenuItemLabel !== null) $data["MenuItemLabel"] = $MenuItemLabel;
			if($MenuItemPosition !== null) $data["MenuItemPosition"] = $MenuItemPosition;
			if($MenuItemEnabled !== null) $data["MenuItemEnabled"] = $MenuItemEnabled;
			if($MenuItemParentID !== null) $data["MenuItemParentID"] = $MenuItemParentID;
			if($MenuItemHasAdmin !== null) $data["MenuItemHasAdmin"] = $MenuItemHasAdmin;
			if($MenuItemAlwaysAdd !== null) $data["MenuItemAlwaysAdd"] = $MenuItemAlwaysAdd;
			if($MenuItemExtendable !== null) $data["MenuItemExtendable"] = $MenuItemExtendable;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->menu_items, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $MenuItemURL = null, $MenuItemClass = null, $MenuItemIcon = null, $MenuItemImage = null, $MenuItemColor = null, $MenuItemLabel = null, $MenuItemPosition = null, $MenuItemEnabled = null, $MenuItemParentID = null, $MenuItemHasAdmin = null, $MenuItemAlwaysAdd = null, $MenuItemExtendable = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($MenuItemURL !== null) $data["MenuItemURL"] = $MenuItemURL;
			if($MenuItemClass !== null) $data["MenuItemClass"] = $MenuItemClass;
			if($MenuItemIcon !== null) $data["MenuItemIcon"] = $MenuItemIcon;
			if($MenuItemImage !== null) $data["MenuItemImage"] = $MenuItemImage;
			if($MenuItemColor !== null) $data["MenuItemColor"] = $MenuItemColor;
			if($MenuItemLabel !== null) $data["MenuItemLabel"] = $MenuItemLabel;
			if($MenuItemPosition !== null) $data["MenuItemPosition"] = $MenuItemPosition;
			if($MenuItemEnabled !== null) $data["MenuItemEnabled"] = $MenuItemEnabled;
			if($MenuItemParentID !== null) $data["MenuItemParentID"] = $MenuItemParentID;
			if($MenuItemHasAdmin !== null) $data["MenuItemHasAdmin"] = $MenuItemHasAdmin;
			if($MenuItemAlwaysAdd !== null) $data["MenuItemAlwaysAdd"] = $MenuItemAlwaysAdd;
			if($MenuItemExtendable !== null) $data["MenuItemExtendable"] = $MenuItemExtendable;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->menu_items, $data);
            }

            return $Deleted;
        }

        function save($Condition, $MenuItemURL = null, $MenuItemClass = null, $MenuItemIcon = null, $MenuItemImage = null, $MenuItemColor = null, $MenuItemLabel = null, $MenuItemPosition = null, $MenuItemEnabled = null, $MenuItemParentID = null, $MenuItemHasAdmin = null, $MenuItemAlwaysAdd = null, $MenuItemExtendable = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($MenuItemURL, $MenuItemClass, $MenuItemIcon, $MenuItemImage, $MenuItemColor, $MenuItemLabel, $MenuItemPosition, $MenuItemEnabled, $MenuItemParentID, $MenuItemHasAdmin, $MenuItemAlwaysAdd, $MenuItemExtendable);
            } else {
                $this->update($DBRow->id, $MenuItemURL, $MenuItemClass, $MenuItemIcon, $MenuItemImage, $MenuItemColor, $MenuItemLabel, $MenuItemPosition, $MenuItemEnabled, $MenuItemParentID, $MenuItemHasAdmin, $MenuItemAlwaysAdd, $MenuItemExtendable);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBMEIT')){
	function DBMEIT($obj = null) {
		return clsDBMenuItems::instance($obj);
	}
}