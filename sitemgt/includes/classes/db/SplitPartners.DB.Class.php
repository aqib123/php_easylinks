<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructSplitPartners'))
	include_once 'struct/SplitPartners.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBSplitPartners')){
	class clsDBSplitPartners extends clsStructSplitPartners {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->split_partners." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBSplitPartners|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->split_partners->Asterisk." from ".DB()->split_partners;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBSplitPartners|null
         */
        function get_by_key($id){
            $Condition = DB()->split_partners->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBSplitPartners|null
         */
        function get_row($id = null, $userid = null, $PartnerName = null, $PartnerType = null, $PartnerAccessKey = null, $DateAdded = null, $PartnerPicture = null, $WebsiteUrl = null, $SkypeName = null, $LinkedIn = null, $FacebookID = null, $EmailAddress = null, $AdditionalNotes = null, $PartnerTags = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($PartnerName !== null) $data["PartnerName"] = $PartnerName;
			if($PartnerType !== null) $data["PartnerType"] = $PartnerType;
			if($PartnerAccessKey !== null) $data["PartnerAccessKey"] = $PartnerAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($PartnerPicture !== null) $data["PartnerPicture"] = $PartnerPicture;
			if($WebsiteUrl !== null) $data["WebsiteUrl"] = $WebsiteUrl;
			if($SkypeName !== null) $data["SkypeName"] = $SkypeName;
			if($LinkedIn !== null) $data["LinkedIn"] = $LinkedIn;
			if($FacebookID !== null) $data["FacebookID"] = $FacebookID;
			if($EmailAddress !== null) $data["EmailAddress"] = $EmailAddress;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PartnerTags !== null) $data["PartnerTags"] = $PartnerTags;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->split_partners->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBSplitPartners[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->split_partners->Asterisk." from ".DB()->split_partners;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->split_partners.".".$GroupBy." ORDER BY ".DB()->split_partners.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->split_partners->id.") from ".DB()->split_partners;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBSplitPartners[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->split_partners;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->split_partners->id.") ".$sql;
            $sql = "select ".DB()->split_partners->Asterisk.$sql." group by ".DB()->split_partners.".".$GroupBy." ORDER BY ".DB()->split_partners.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $PartnerName = null, $PartnerType = null, $PartnerAccessKey = null, $DateAdded = null, $PartnerPicture = null, $WebsiteUrl = null, $SkypeName = null, $LinkedIn = null, $FacebookID = null, $EmailAddress = null, $AdditionalNotes = null, $PartnerTags = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($PartnerName !== null) $data["PartnerName"] = $PartnerName;
			if($PartnerType !== null) $data["PartnerType"] = $PartnerType;
			if($PartnerAccessKey !== null) $data["PartnerAccessKey"] = $PartnerAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($PartnerPicture !== null) $data["PartnerPicture"] = $PartnerPicture;
			if($WebsiteUrl !== null) $data["WebsiteUrl"] = $WebsiteUrl;
			if($SkypeName !== null) $data["SkypeName"] = $SkypeName;
			if($LinkedIn !== null) $data["LinkedIn"] = $LinkedIn;
			if($FacebookID !== null) $data["FacebookID"] = $FacebookID;
			if($EmailAddress !== null) $data["EmailAddress"] = $EmailAddress;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PartnerTags !== null) $data["PartnerTags"] = $PartnerTags;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->split_partners, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $PartnerName = null, $PartnerType = null, $PartnerAccessKey = null, $DateAdded = null, $PartnerPicture = null, $WebsiteUrl = null, $SkypeName = null, $LinkedIn = null, $FacebookID = null, $EmailAddress = null, $AdditionalNotes = null, $PartnerTags = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($PartnerName !== null) $data["PartnerName"] = $PartnerName;
			if($PartnerType !== null) $data["PartnerType"] = $PartnerType;
			if($PartnerAccessKey !== null) $data["PartnerAccessKey"] = $PartnerAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($PartnerPicture !== null) $data["PartnerPicture"] = $PartnerPicture;
			if($WebsiteUrl !== null) $data["WebsiteUrl"] = $WebsiteUrl;
			if($SkypeName !== null) $data["SkypeName"] = $SkypeName;
			if($LinkedIn !== null) $data["LinkedIn"] = $LinkedIn;
			if($FacebookID !== null) $data["FacebookID"] = $FacebookID;
			if($EmailAddress !== null) $data["EmailAddress"] = $EmailAddress;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PartnerTags !== null) $data["PartnerTags"] = $PartnerTags;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->split_partners, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $PartnerName = null, $PartnerType = null, $PartnerAccessKey = null, $DateAdded = null, $PartnerPicture = null, $WebsiteUrl = null, $SkypeName = null, $LinkedIn = null, $FacebookID = null, $EmailAddress = null, $AdditionalNotes = null, $PartnerTags = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($PartnerName !== null) $data["PartnerName"] = $PartnerName;
			if($PartnerType !== null) $data["PartnerType"] = $PartnerType;
			if($PartnerAccessKey !== null) $data["PartnerAccessKey"] = $PartnerAccessKey;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($PartnerPicture !== null) $data["PartnerPicture"] = $PartnerPicture;
			if($WebsiteUrl !== null) $data["WebsiteUrl"] = $WebsiteUrl;
			if($SkypeName !== null) $data["SkypeName"] = $SkypeName;
			if($LinkedIn !== null) $data["LinkedIn"] = $LinkedIn;
			if($FacebookID !== null) $data["FacebookID"] = $FacebookID;
			if($EmailAddress !== null) $data["EmailAddress"] = $EmailAddress;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PartnerTags !== null) $data["PartnerTags"] = $PartnerTags;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->split_partners, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $PartnerName = null, $PartnerType = null, $PartnerAccessKey = null, $DateAdded = null, $PartnerPicture = null, $WebsiteUrl = null, $SkypeName = null, $LinkedIn = null, $FacebookID = null, $EmailAddress = null, $AdditionalNotes = null, $PartnerTags = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $PartnerName, $PartnerType, $PartnerAccessKey, $DateAdded, $PartnerPicture, $WebsiteUrl, $SkypeName, $LinkedIn, $FacebookID, $EmailAddress, $AdditionalNotes, $PartnerTags);
            } else {
                $this->update($DBRow->id, $userid, $PartnerName, $PartnerType, $PartnerAccessKey, $DateAdded, $PartnerPicture, $WebsiteUrl, $SkypeName, $LinkedIn, $FacebookID, $EmailAddress, $AdditionalNotes, $PartnerTags);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBSP')){
	function DBSP($obj = null) {
		return clsDBSplitPartners::instance($obj);
	}
}