<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLevelsCopy'))
	include_once 'struct/LevelsCopy.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLevelsCopy')){
	class clsDBLevelsCopy extends clsStructLevelsCopy {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->levels_copy." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLevelsCopy|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->levels_copy->Asterisk." from ".DB()->levels_copy;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLevelsCopy|null
         */
        function get_by_key($id){
            $Condition = DB()->levels_copy->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLevelsCopy|null
         */
        function get_row($id = null, $LevelName = null, $LevelEnabled = null, $LevelDefault = null, $LevelLogo = null, $LevelAccessKey = null, $created_userid = null, $created_date_time = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($LevelName !== null) $data["LevelName"] = $LevelName;
			if($LevelEnabled !== null) $data["LevelEnabled"] = $LevelEnabled;
			if($LevelDefault !== null) $data["LevelDefault"] = $LevelDefault;
			if($LevelLogo !== null) $data["LevelLogo"] = $LevelLogo;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($created_userid !== null) $data["created_userid"] = $created_userid;
			if($created_date_time !== null) $data["created_date_time"] = $created_date_time;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->levels_copy->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLevelsCopy[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->levels_copy->Asterisk." from ".DB()->levels_copy;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->levels_copy.".".$GroupBy." ORDER BY ".DB()->levels_copy.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->levels_copy->id.") from ".DB()->levels_copy;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLevelsCopy[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->levels_copy;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->levels_copy->id.") ".$sql;
            $sql = "select ".DB()->levels_copy->Asterisk.$sql." group by ".DB()->levels_copy.".".$GroupBy." ORDER BY ".DB()->levels_copy.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($LevelName = null, $LevelEnabled = null, $LevelDefault = null, $LevelLogo = null, $LevelAccessKey = null, $created_userid = null, $created_date_time = null){
            $data = array();
            $Inserted = false;

            if($LevelName !== null) $data["LevelName"] = $LevelName;
			if($LevelEnabled !== null) $data["LevelEnabled"] = $LevelEnabled;
			if($LevelDefault !== null) $data["LevelDefault"] = $LevelDefault;
			if($LevelLogo !== null) $data["LevelLogo"] = $LevelLogo;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($created_userid !== null) $data["created_userid"] = $created_userid;
			if($created_date_time !== null) $data["created_date_time"] = $created_date_time;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->levels_copy, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $LevelName = null, $LevelEnabled = null, $LevelDefault = null, $LevelLogo = null, $LevelAccessKey = null, $created_userid = null, $created_date_time = null){
            $data = array();
            $Updated = false;

            if($LevelName !== null) $data["LevelName"] = $LevelName;
			if($LevelEnabled !== null) $data["LevelEnabled"] = $LevelEnabled;
			if($LevelDefault !== null) $data["LevelDefault"] = $LevelDefault;
			if($LevelLogo !== null) $data["LevelLogo"] = $LevelLogo;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($created_userid !== null) $data["created_userid"] = $created_userid;
			if($created_date_time !== null) $data["created_date_time"] = $created_date_time;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->levels_copy, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $LevelName = null, $LevelEnabled = null, $LevelDefault = null, $LevelLogo = null, $LevelAccessKey = null, $created_userid = null, $created_date_time = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($LevelName !== null) $data["LevelName"] = $LevelName;
			if($LevelEnabled !== null) $data["LevelEnabled"] = $LevelEnabled;
			if($LevelDefault !== null) $data["LevelDefault"] = $LevelDefault;
			if($LevelLogo !== null) $data["LevelLogo"] = $LevelLogo;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($created_userid !== null) $data["created_userid"] = $created_userid;
			if($created_date_time !== null) $data["created_date_time"] = $created_date_time;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->levels_copy, $data);
            }

            return $Deleted;
        }

        function save($Condition, $LevelName = null, $LevelEnabled = null, $LevelDefault = null, $LevelLogo = null, $LevelAccessKey = null, $created_userid = null, $created_date_time = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($LevelName, $LevelEnabled, $LevelDefault, $LevelLogo, $LevelAccessKey, $created_userid, $created_date_time);
            } else {
                $this->update($DBRow->id, $LevelName, $LevelEnabled, $LevelDefault, $LevelLogo, $LevelAccessKey, $created_userid, $created_date_time);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLC')){
	function DBLC($obj = null) {
		return clsDBLevelsCopy::instance($obj);
	}
}