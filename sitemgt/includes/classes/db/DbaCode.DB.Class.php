<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructDbaCode'))
	include_once 'struct/DbaCode.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBDbaCode')){
	class clsDBDbaCode extends clsStructDbaCode {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->dba_code." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBDbaCode|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->dba_code->Asterisk." from ".DB()->dba_code;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBDbaCode|null
         */
        function get_by_key($id){
            $Condition = DB()->dba_code->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBDbaCode|null
         */
        function get_row($id = null, $email_id = null, $code = null, $status = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($email_id !== null) $data["email_id"] = $email_id;
			if($code !== null) $data["code"] = $code;
			if($status !== null) $data["status"] = $status;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->dba_code->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBDbaCode[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->dba_code->Asterisk." from ".DB()->dba_code;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->dba_code.".".$GroupBy." ORDER BY ".DB()->dba_code.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->dba_code->id.") from ".DB()->dba_code;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBDbaCode[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->dba_code;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->dba_code->id.") ".$sql;
            $sql = "select ".DB()->dba_code->Asterisk.$sql." group by ".DB()->dba_code.".".$GroupBy." ORDER BY ".DB()->dba_code.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($email_id = null, $code = null, $status = null){
            $data = array();
            $Inserted = false;

            if($email_id !== null) $data["email_id"] = $email_id;
			if($code !== null) $data["code"] = $code;
			if($status !== null) $data["status"] = $status;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->dba_code, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $email_id = null, $code = null, $status = null){
            $data = array();
            $Updated = false;

            if($email_id !== null) $data["email_id"] = $email_id;
			if($code !== null) $data["code"] = $code;
			if($status !== null) $data["status"] = $status;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->dba_code, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $email_id = null, $code = null, $status = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($email_id !== null) $data["email_id"] = $email_id;
			if($code !== null) $data["code"] = $code;
			if($status !== null) $data["status"] = $status;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->dba_code, $data);
            }

            return $Deleted;
        }

        function save($Condition, $email_id = null, $code = null, $status = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($email_id, $code, $status);
            } else {
                $this->update($DBRow->id, $email_id, $code, $status);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBDC')){
	function DBDC($obj = null) {
		return clsDBDbaCode::instance($obj);
	}
}