<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructHelpContents'))
	include_once 'struct/HelpContents.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBHelpContents')){
	class clsDBHelpContents extends clsStructHelpContents {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->help_contents." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBHelpContents|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->help_contents->Asterisk." from ".DB()->help_contents;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBHelpContents|null
         */
        function get_by_key($id){
            $Condition = DB()->help_contents->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBHelpContents|null
         */
        function get_row($id = null, $HelpContentTitle = null, $HelpContentExcerpt = null, $HelpContentText = null, $HelpContentType = null, $HelpContentPosition = null, $HelpContentEnabled = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($HelpContentTitle !== null) $data["HelpContentTitle"] = $HelpContentTitle;
			if($HelpContentExcerpt !== null) $data["HelpContentExcerpt"] = $HelpContentExcerpt;
			if($HelpContentText !== null) $data["HelpContentText"] = $HelpContentText;
			if($HelpContentType !== null) $data["HelpContentType"] = $HelpContentType;
			if($HelpContentPosition !== null) $data["HelpContentPosition"] = $HelpContentPosition;
			if($HelpContentEnabled !== null) $data["HelpContentEnabled"] = $HelpContentEnabled;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->help_contents->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBHelpContents[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->help_contents->Asterisk." from ".DB()->help_contents;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->help_contents.".".$GroupBy." ORDER BY ".DB()->help_contents.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->help_contents->id.") from ".DB()->help_contents;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBHelpContents[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->help_contents;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->help_contents->id.") ".$sql;
            $sql = "select ".DB()->help_contents->Asterisk.$sql." group by ".DB()->help_contents.".".$GroupBy." ORDER BY ".DB()->help_contents.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($HelpContentTitle = null, $HelpContentExcerpt = null, $HelpContentText = null, $HelpContentType = null, $HelpContentPosition = null, $HelpContentEnabled = null){
            $data = array();
            $Inserted = false;

            if($HelpContentTitle !== null) $data["HelpContentTitle"] = $HelpContentTitle;
			if($HelpContentExcerpt !== null) $data["HelpContentExcerpt"] = $HelpContentExcerpt;
			if($HelpContentText !== null) $data["HelpContentText"] = $HelpContentText;
			if($HelpContentType !== null) $data["HelpContentType"] = $HelpContentType;
			if($HelpContentPosition !== null) $data["HelpContentPosition"] = $HelpContentPosition;
			if($HelpContentEnabled !== null) $data["HelpContentEnabled"] = $HelpContentEnabled;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->help_contents, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $HelpContentTitle = null, $HelpContentExcerpt = null, $HelpContentText = null, $HelpContentType = null, $HelpContentPosition = null, $HelpContentEnabled = null){
            $data = array();
            $Updated = false;

            if($HelpContentTitle !== null) $data["HelpContentTitle"] = $HelpContentTitle;
			if($HelpContentExcerpt !== null) $data["HelpContentExcerpt"] = $HelpContentExcerpt;
			if($HelpContentText !== null) $data["HelpContentText"] = $HelpContentText;
			if($HelpContentType !== null) $data["HelpContentType"] = $HelpContentType;
			if($HelpContentPosition !== null) $data["HelpContentPosition"] = $HelpContentPosition;
			if($HelpContentEnabled !== null) $data["HelpContentEnabled"] = $HelpContentEnabled;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->help_contents, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $HelpContentTitle = null, $HelpContentExcerpt = null, $HelpContentText = null, $HelpContentType = null, $HelpContentPosition = null, $HelpContentEnabled = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($HelpContentTitle !== null) $data["HelpContentTitle"] = $HelpContentTitle;
			if($HelpContentExcerpt !== null) $data["HelpContentExcerpt"] = $HelpContentExcerpt;
			if($HelpContentText !== null) $data["HelpContentText"] = $HelpContentText;
			if($HelpContentType !== null) $data["HelpContentType"] = $HelpContentType;
			if($HelpContentPosition !== null) $data["HelpContentPosition"] = $HelpContentPosition;
			if($HelpContentEnabled !== null) $data["HelpContentEnabled"] = $HelpContentEnabled;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->help_contents, $data);
            }

            return $Deleted;
        }

        function save($Condition, $HelpContentTitle = null, $HelpContentExcerpt = null, $HelpContentText = null, $HelpContentType = null, $HelpContentPosition = null, $HelpContentEnabled = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($HelpContentTitle, $HelpContentExcerpt, $HelpContentText, $HelpContentType, $HelpContentPosition, $HelpContentEnabled);
            } else {
                $this->update($DBRow->id, $HelpContentTitle, $HelpContentExcerpt, $HelpContentText, $HelpContentType, $HelpContentPosition, $HelpContentEnabled);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBHC')){
	function DBHC($obj = null) {
		return clsDBHelpContents::instance($obj);
	}
}