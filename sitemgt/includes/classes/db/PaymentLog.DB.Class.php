<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructPaymentLog'))
	include_once 'struct/PaymentLog.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBPaymentLog')){
	class clsDBPaymentLog extends clsStructPaymentLog {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->payment_log." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->payment_log->Asterisk." from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get_by_key($id){
            $Condition = DB()->payment_log->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get_row($id = null, $UserID = null, $custname = null, $custemail = null, $proditem = null, $custcountry = null, $prodtype = null, $status = null, $affiliate = null, $amount = null, $up_amount = null, $paymode = null, $transvendor = null, $transid = null, $uptranid = null, $affiliateid = null, $verifykey = null, $transtime = null, $username = null, $password = null, $pro_type = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($UserID !== null) $data["UserID"] = $UserID;
			if($custname !== null) $data["custname"] = $custname;
			if($custemail !== null) $data["custemail"] = $custemail;
			if($proditem !== null) $data["proditem"] = $proditem;
			if($custcountry !== null) $data["custcountry"] = $custcountry;
			if($prodtype !== null) $data["prodtype"] = $prodtype;
			if($status !== null) $data["status"] = $status;
			if($affiliate !== null) $data["affiliate"] = $affiliate;
			if($amount !== null) $data["amount"] = $amount;
			if($up_amount !== null) $data["up_amount"] = $up_amount;
			if($paymode !== null) $data["paymode"] = $paymode;
			if($transvendor !== null) $data["transvendor"] = $transvendor;
			if($transid !== null) $data["transid"] = $transid;
			if($uptranid !== null) $data["uptranid"] = $uptranid;
			if($affiliateid !== null) $data["affiliateid"] = $affiliateid;
			if($verifykey !== null) $data["verifykey"] = $verifykey;
			if($transtime !== null) $data["transtime"] = $transtime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($pro_type !== null) $data["pro_type"] = $pro_type;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->payment_log->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBPaymentLog[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->payment_log->Asterisk." from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->payment_log.".".$GroupBy." ORDER BY ".DB()->payment_log.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->payment_log->id.") from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBPaymentLog[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->payment_log;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->payment_log->id.") ".$sql;
            $sql = "select ".DB()->payment_log->Asterisk.$sql." group by ".DB()->payment_log.".".$GroupBy." ORDER BY ".DB()->payment_log.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($UserID = null, $custname = null, $custemail = null, $proditem = null, $custcountry = null, $prodtype = null, $status = null, $affiliate = null, $amount = null, $up_amount = null, $paymode = null, $transvendor = null, $transid = null, $uptranid = null, $affiliateid = null, $verifykey = null, $transtime = null, $username = null, $password = null, $pro_type = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null){
            $data = array();
            $Inserted = false;

            if($UserID !== null) $data["UserID"] = $UserID;
			if($custname !== null) $data["custname"] = $custname;
			if($custemail !== null) $data["custemail"] = $custemail;
			if($proditem !== null) $data["proditem"] = $proditem;
			if($custcountry !== null) $data["custcountry"] = $custcountry;
			if($prodtype !== null) $data["prodtype"] = $prodtype;
			if($status !== null) $data["status"] = $status;
			if($affiliate !== null) $data["affiliate"] = $affiliate;
			if($amount !== null) $data["amount"] = $amount;
			if($up_amount !== null) $data["up_amount"] = $up_amount;
			if($paymode !== null) $data["paymode"] = $paymode;
			if($transvendor !== null) $data["transvendor"] = $transvendor;
			if($transid !== null) $data["transid"] = $transid;
			if($uptranid !== null) $data["uptranid"] = $uptranid;
			if($affiliateid !== null) $data["affiliateid"] = $affiliateid;
			if($verifykey !== null) $data["verifykey"] = $verifykey;
			if($transtime !== null) $data["transtime"] = $transtime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($pro_type !== null) $data["pro_type"] = $pro_type;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->payment_log, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $UserID = null, $custname = null, $custemail = null, $proditem = null, $custcountry = null, $prodtype = null, $status = null, $affiliate = null, $amount = null, $up_amount = null, $paymode = null, $transvendor = null, $transid = null, $uptranid = null, $affiliateid = null, $verifykey = null, $transtime = null, $username = null, $password = null, $pro_type = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null){
            $data = array();
            $Updated = false;

            if($UserID !== null) $data["UserID"] = $UserID;
			if($custname !== null) $data["custname"] = $custname;
			if($custemail !== null) $data["custemail"] = $custemail;
			if($proditem !== null) $data["proditem"] = $proditem;
			if($custcountry !== null) $data["custcountry"] = $custcountry;
			if($prodtype !== null) $data["prodtype"] = $prodtype;
			if($status !== null) $data["status"] = $status;
			if($affiliate !== null) $data["affiliate"] = $affiliate;
			if($amount !== null) $data["amount"] = $amount;
			if($up_amount !== null) $data["up_amount"] = $up_amount;
			if($paymode !== null) $data["paymode"] = $paymode;
			if($transvendor !== null) $data["transvendor"] = $transvendor;
			if($transid !== null) $data["transid"] = $transid;
			if($uptranid !== null) $data["uptranid"] = $uptranid;
			if($affiliateid !== null) $data["affiliateid"] = $affiliateid;
			if($verifykey !== null) $data["verifykey"] = $verifykey;
			if($transtime !== null) $data["transtime"] = $transtime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($pro_type !== null) $data["pro_type"] = $pro_type;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->payment_log, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $UserID = null, $custname = null, $custemail = null, $proditem = null, $custcountry = null, $prodtype = null, $status = null, $affiliate = null, $amount = null, $up_amount = null, $paymode = null, $transvendor = null, $transid = null, $uptranid = null, $affiliateid = null, $verifykey = null, $transtime = null, $username = null, $password = null, $pro_type = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($UserID !== null) $data["UserID"] = $UserID;
			if($custname !== null) $data["custname"] = $custname;
			if($custemail !== null) $data["custemail"] = $custemail;
			if($proditem !== null) $data["proditem"] = $proditem;
			if($custcountry !== null) $data["custcountry"] = $custcountry;
			if($prodtype !== null) $data["prodtype"] = $prodtype;
			if($status !== null) $data["status"] = $status;
			if($affiliate !== null) $data["affiliate"] = $affiliate;
			if($amount !== null) $data["amount"] = $amount;
			if($up_amount !== null) $data["up_amount"] = $up_amount;
			if($paymode !== null) $data["paymode"] = $paymode;
			if($transvendor !== null) $data["transvendor"] = $transvendor;
			if($transid !== null) $data["transid"] = $transid;
			if($uptranid !== null) $data["uptranid"] = $uptranid;
			if($affiliateid !== null) $data["affiliateid"] = $affiliateid;
			if($verifykey !== null) $data["verifykey"] = $verifykey;
			if($transtime !== null) $data["transtime"] = $transtime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($pro_type !== null) $data["pro_type"] = $pro_type;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->payment_log, $data);
            }

            return $Deleted;
        }

        function save($Condition, $UserID = null, $custname = null, $custemail = null, $proditem = null, $custcountry = null, $prodtype = null, $status = null, $affiliate = null, $amount = null, $up_amount = null, $paymode = null, $transvendor = null, $transid = null, $uptranid = null, $affiliateid = null, $verifykey = null, $transtime = null, $username = null, $password = null, $pro_type = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($UserID, $custname, $custemail, $proditem, $custcountry, $prodtype, $status, $affiliate, $amount, $up_amount, $paymode, $transvendor, $transid, $uptranid, $affiliateid, $verifykey, $transtime, $username, $password, $pro_type, $LevelAccessKey, $active_status, $sent_license_key);
            } else {
                $this->update($DBRow->id, $UserID, $custname, $custemail, $proditem, $custcountry, $prodtype, $status, $affiliate, $amount, $up_amount, $paymode, $transvendor, $transid, $uptranid, $affiliateid, $verifykey, $transtime, $username, $password, $pro_type, $LevelAccessKey, $active_status, $sent_license_key);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBPL')){
	function DBPL($obj = null) {
		return clsDBPaymentLog::instance($obj);
	}
}