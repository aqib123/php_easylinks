<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructDomains'))
	include_once 'struct/Domains.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBDomains')){
	class clsDBDomains extends clsStructDomains {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->domains." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBDomains|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->domains->Asterisk." from ".DB()->domains;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBDomains|null
         */
        function get_by_key($id){
            $Condition = DB()->domains->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBDomains|null
         */
        function get_row($id = null, $userid = null, $DomainName = null, $DomainSlug = null, $DomainUrl = null, $DateAdded = null, $DomainType = null, $DomainForward = null, $DomainAccessKey = null, $DefaultCloakURL = null, $DateUpdated = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($DomainName !== null) $data["DomainName"] = $DomainName;
			if($DomainSlug !== null) $data["DomainSlug"] = $DomainSlug;
			if($DomainUrl !== null) $data["DomainUrl"] = $DomainUrl;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($DomainType !== null) $data["DomainType"] = $DomainType;
			if($DomainForward !== null) $data["DomainForward"] = $DomainForward;
			if($DomainAccessKey !== null) $data["DomainAccessKey"] = $DomainAccessKey;
			if($DefaultCloakURL !== null) $data["DefaultCloakURL"] = $DefaultCloakURL;
			if($DateUpdated !== null) $data["DateUpdated"] = $DateUpdated;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->domains->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBDomains[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->domains->Asterisk." from ".DB()->domains;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->domains.".".$GroupBy." ORDER BY ".DB()->domains.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->domains->id.") from ".DB()->domains;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBDomains[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->domains;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->domains->id.") ".$sql;
            $sql = "select ".DB()->domains->Asterisk.$sql." group by ".DB()->domains.".".$GroupBy." ORDER BY ".DB()->domains.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $DomainName = null, $DomainSlug = null, $DomainUrl = null, $DateAdded = null, $DomainType = null, $DomainForward = null, $DomainAccessKey = null, $DefaultCloakURL = null, $DateUpdated = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($DomainName !== null) $data["DomainName"] = $DomainName;
			if($DomainSlug !== null) $data["DomainSlug"] = $DomainSlug;
			if($DomainUrl !== null) $data["DomainUrl"] = $DomainUrl;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($DomainType !== null) $data["DomainType"] = $DomainType;
			if($DomainForward !== null) $data["DomainForward"] = $DomainForward;
			if($DomainAccessKey !== null) $data["DomainAccessKey"] = $DomainAccessKey;
			if($DefaultCloakURL !== null) $data["DefaultCloakURL"] = $DefaultCloakURL;
			if($DateUpdated !== null) $data["DateUpdated"] = $DateUpdated;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->domains, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $DomainName = null, $DomainSlug = null, $DomainUrl = null, $DateAdded = null, $DomainType = null, $DomainForward = null, $DomainAccessKey = null, $DefaultCloakURL = null, $DateUpdated = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($DomainName !== null) $data["DomainName"] = $DomainName;
			if($DomainSlug !== null) $data["DomainSlug"] = $DomainSlug;
			if($DomainUrl !== null) $data["DomainUrl"] = $DomainUrl;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($DomainType !== null) $data["DomainType"] = $DomainType;
			if($DomainForward !== null) $data["DomainForward"] = $DomainForward;
			if($DomainAccessKey !== null) $data["DomainAccessKey"] = $DomainAccessKey;
			if($DefaultCloakURL !== null) $data["DefaultCloakURL"] = $DefaultCloakURL;
			if($DateUpdated !== null) $data["DateUpdated"] = $DateUpdated;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->domains, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $DomainName = null, $DomainSlug = null, $DomainUrl = null, $DateAdded = null, $DomainType = null, $DomainForward = null, $DomainAccessKey = null, $DefaultCloakURL = null, $DateUpdated = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($DomainName !== null) $data["DomainName"] = $DomainName;
			if($DomainSlug !== null) $data["DomainSlug"] = $DomainSlug;
			if($DomainUrl !== null) $data["DomainUrl"] = $DomainUrl;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($DomainType !== null) $data["DomainType"] = $DomainType;
			if($DomainForward !== null) $data["DomainForward"] = $DomainForward;
			if($DomainAccessKey !== null) $data["DomainAccessKey"] = $DomainAccessKey;
			if($DefaultCloakURL !== null) $data["DefaultCloakURL"] = $DefaultCloakURL;
			if($DateUpdated !== null) $data["DateUpdated"] = $DateUpdated;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->domains, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $DomainName = null, $DomainSlug = null, $DomainUrl = null, $DateAdded = null, $DomainType = null, $DomainForward = null, $DomainAccessKey = null, $DefaultCloakURL = null, $DateUpdated = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $DomainName, $DomainSlug, $DomainUrl, $DateAdded, $DomainType, $DomainForward, $DomainAccessKey, $DefaultCloakURL, $DateUpdated);
            } else {
                $this->update($DBRow->id, $userid, $DomainName, $DomainSlug, $DomainUrl, $DateAdded, $DomainType, $DomainForward, $DomainAccessKey, $DefaultCloakURL, $DateUpdated);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBD')){
	function DBD($obj = null) {
		return clsDBDomains::instance($obj);
	}
}