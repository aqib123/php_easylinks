<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkbanks'))
	include_once 'struct/Linkbanks.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkbanks')){
	class clsDBLinkbanks extends clsStructLinkbanks {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->linkbanks." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbanks->Asterisk." from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get_by_key($id){
            $Condition = DB()->linkbanks->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get_row($id = null, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->linkbanks->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkbanks[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbanks->Asterisk." from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->linkbanks.".".$GroupBy." ORDER BY ".DB()->linkbanks.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->linkbanks->id.") from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkbanks[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->linkbanks;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->linkbanks->id.") ".$sql;
            $sql = "select ".DB()->linkbanks->Asterisk.$sql." group by ".DB()->linkbanks.".".$GroupBy." ORDER BY ".DB()->linkbanks.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->linkbanks, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->linkbanks, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->linkbanks, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $VendorID, $LinkName, $LinkBankAccessKey, $DestinationURL, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $AdditionalNotes, $PageImage, $LinkActive, $SalesConversions, $RedirectAfterLinkExpired, $PixelID, $TrackingCodeForActions, $TrackingCodeForSalesAndConversions, $DateAdded, $UnitPriceAction, $UnitPriceSales, $LinkStatus, $SplitPartnerID, $PartnerSplit, $Commission, $AffiliatePlatformID, $ManualSalesEntry, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $SaleInfoEntered, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $PartnerPaymentStatus, $PartnerPaymentDate, $PartnerPaymentReferenceNo, $TrackCommission, $PartnerSplitAllowed, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $TrackEPC, $RawClicks);
            } else {
                $this->update($DBRow->id, $userid, $VendorID, $LinkName, $LinkBankAccessKey, $DestinationURL, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $AdditionalNotes, $PageImage, $LinkActive, $SalesConversions, $RedirectAfterLinkExpired, $PixelID, $TrackingCodeForActions, $TrackingCodeForSalesAndConversions, $DateAdded, $UnitPriceAction, $UnitPriceSales, $LinkStatus, $SplitPartnerID, $PartnerSplit, $Commission, $AffiliatePlatformID, $ManualSalesEntry, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $SaleInfoEntered, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $PartnerPaymentStatus, $PartnerPaymentDate, $PartnerPaymentReferenceNo, $TrackCommission, $PartnerSplitAllowed, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $TrackEPC, $RawClicks);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLI')){
	function DBLI($obj = null) {
		return clsDBLinkbanks::instance($obj);
	}
}