<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructCampaignEmails'))
	include_once 'struct/CampaignEmails.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBCampaignEmails')){
	class clsDBCampaignEmails extends clsStructCampaignEmails {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->campaign_emails." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBCampaignEmails|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->campaign_emails->Asterisk." from ".DB()->campaign_emails;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBCampaignEmails|null
         */
        function get_by_key($id){
            $Condition = DB()->campaign_emails->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBCampaignEmails|null
         */
        function get_row($id = null, $userid = null, $CampaignID = null, $AutoresponderID = null, $EmailSubject = null, $EmailBody = null, $SendTime = null, $DateAdded = null, $LinkBankID = null, $SetWidth = null, $isEnabled = null, $EmailTypeID = null, $isCopied = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($CampaignID !== null) $data["CampaignID"] = $CampaignID;
			if($AutoresponderID !== null) $data["AutoresponderID"] = $AutoresponderID;
			if($EmailSubject !== null) $data["EmailSubject"] = $EmailSubject;
			if($EmailBody !== null) $data["EmailBody"] = $EmailBody;
			if($SendTime !== null) $data["SendTime"] = $SendTime;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SetWidth !== null) $data["SetWidth"] = $SetWidth;
			if($isEnabled !== null) $data["isEnabled"] = $isEnabled;
			if($EmailTypeID !== null) $data["EmailTypeID"] = $EmailTypeID;
			if($isCopied !== null) $data["isCopied"] = $isCopied;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->campaign_emails->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBCampaignEmails[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->campaign_emails->Asterisk." from ".DB()->campaign_emails;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->campaign_emails.".".$GroupBy." ORDER BY ".DB()->campaign_emails.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->campaign_emails->id.") from ".DB()->campaign_emails;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBCampaignEmails[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->campaign_emails;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->campaign_emails->id.") ".$sql;
            $sql = "select ".DB()->campaign_emails->Asterisk.$sql." group by ".DB()->campaign_emails.".".$GroupBy." ORDER BY ".DB()->campaign_emails.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $CampaignID = null, $AutoresponderID = null, $EmailSubject = null, $EmailBody = null, $SendTime = null, $DateAdded = null, $LinkBankID = null, $SetWidth = null, $isEnabled = null, $EmailTypeID = null, $isCopied = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($CampaignID !== null) $data["CampaignID"] = $CampaignID;
			if($AutoresponderID !== null) $data["AutoresponderID"] = $AutoresponderID;
			if($EmailSubject !== null) $data["EmailSubject"] = $EmailSubject;
			if($EmailBody !== null) $data["EmailBody"] = $EmailBody;
			if($SendTime !== null) $data["SendTime"] = $SendTime;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SetWidth !== null) $data["SetWidth"] = $SetWidth;
			if($isEnabled !== null) $data["isEnabled"] = $isEnabled;
			if($EmailTypeID !== null) $data["EmailTypeID"] = $EmailTypeID;
			if($isCopied !== null) $data["isCopied"] = $isCopied;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->campaign_emails, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $CampaignID = null, $AutoresponderID = null, $EmailSubject = null, $EmailBody = null, $SendTime = null, $DateAdded = null, $LinkBankID = null, $SetWidth = null, $isEnabled = null, $EmailTypeID = null, $isCopied = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($CampaignID !== null) $data["CampaignID"] = $CampaignID;
			if($AutoresponderID !== null) $data["AutoresponderID"] = $AutoresponderID;
			if($EmailSubject !== null) $data["EmailSubject"] = $EmailSubject;
			if($EmailBody !== null) $data["EmailBody"] = $EmailBody;
			if($SendTime !== null) $data["SendTime"] = $SendTime;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SetWidth !== null) $data["SetWidth"] = $SetWidth;
			if($isEnabled !== null) $data["isEnabled"] = $isEnabled;
			if($EmailTypeID !== null) $data["EmailTypeID"] = $EmailTypeID;
			if($isCopied !== null) $data["isCopied"] = $isCopied;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->campaign_emails, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $CampaignID = null, $AutoresponderID = null, $EmailSubject = null, $EmailBody = null, $SendTime = null, $DateAdded = null, $LinkBankID = null, $SetWidth = null, $isEnabled = null, $EmailTypeID = null, $isCopied = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($CampaignID !== null) $data["CampaignID"] = $CampaignID;
			if($AutoresponderID !== null) $data["AutoresponderID"] = $AutoresponderID;
			if($EmailSubject !== null) $data["EmailSubject"] = $EmailSubject;
			if($EmailBody !== null) $data["EmailBody"] = $EmailBody;
			if($SendTime !== null) $data["SendTime"] = $SendTime;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($SetWidth !== null) $data["SetWidth"] = $SetWidth;
			if($isEnabled !== null) $data["isEnabled"] = $isEnabled;
			if($EmailTypeID !== null) $data["EmailTypeID"] = $EmailTypeID;
			if($isCopied !== null) $data["isCopied"] = $isCopied;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->campaign_emails, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $CampaignID = null, $AutoresponderID = null, $EmailSubject = null, $EmailBody = null, $SendTime = null, $DateAdded = null, $LinkBankID = null, $SetWidth = null, $isEnabled = null, $EmailTypeID = null, $isCopied = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $CampaignID, $AutoresponderID, $EmailSubject, $EmailBody, $SendTime, $DateAdded, $LinkBankID, $SetWidth, $isEnabled, $EmailTypeID, $isCopied);
            } else {
                $this->update($DBRow->id, $userid, $CampaignID, $AutoresponderID, $EmailSubject, $EmailBody, $SendTime, $DateAdded, $LinkBankID, $SetWidth, $isEnabled, $EmailTypeID, $isCopied);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBCE')){
	function DBCE($obj = null) {
		return clsDBCampaignEmails::instance($obj);
	}
}