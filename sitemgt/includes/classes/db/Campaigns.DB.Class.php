<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructCampaigns'))
	include_once 'struct/Campaigns.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBCampaigns')){
	class clsDBCampaigns extends clsStructCampaigns {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->campaigns." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBCampaigns|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->campaigns->Asterisk." from ".DB()->campaigns;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBCampaigns|null
         */
        function get_by_key($id){
            $Condition = DB()->campaigns->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBCampaigns|null
         */
        function get_row($id = null, $userid = null, $CampaignName = null, $CampaignAccessKey = null, $GroupID = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $Commission = null, $PartnerSplit = null, $AdditionalNotes = null, $DateAdded = null, $hasSingleEmail = null, $CampaignStatus = null, $SaleInfoEntered = null, $StartDate = null, $EndDate = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $MasterCampaignID = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($CampaignName !== null) $data["CampaignName"] = $CampaignName;
			if($CampaignAccessKey !== null) $data["CampaignAccessKey"] = $CampaignAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleEmail !== null) $data["hasSingleEmail"] = $hasSingleEmail;
			if($CampaignStatus !== null) $data["CampaignStatus"] = $CampaignStatus;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->campaigns->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBCampaigns[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->campaigns->Asterisk." from ".DB()->campaigns;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->campaigns.".".$GroupBy." ORDER BY ".DB()->campaigns.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->campaigns->id.") from ".DB()->campaigns;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBCampaigns[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->campaigns;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->campaigns->id.") ".$sql;
            $sql = "select ".DB()->campaigns->Asterisk.$sql." group by ".DB()->campaigns.".".$GroupBy." ORDER BY ".DB()->campaigns.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $CampaignName = null, $CampaignAccessKey = null, $GroupID = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $Commission = null, $PartnerSplit = null, $AdditionalNotes = null, $DateAdded = null, $hasSingleEmail = null, $CampaignStatus = null, $SaleInfoEntered = null, $StartDate = null, $EndDate = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $MasterCampaignID = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($CampaignName !== null) $data["CampaignName"] = $CampaignName;
			if($CampaignAccessKey !== null) $data["CampaignAccessKey"] = $CampaignAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleEmail !== null) $data["hasSingleEmail"] = $hasSingleEmail;
			if($CampaignStatus !== null) $data["CampaignStatus"] = $CampaignStatus;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->campaigns, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $CampaignName = null, $CampaignAccessKey = null, $GroupID = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $Commission = null, $PartnerSplit = null, $AdditionalNotes = null, $DateAdded = null, $hasSingleEmail = null, $CampaignStatus = null, $SaleInfoEntered = null, $StartDate = null, $EndDate = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $MasterCampaignID = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($CampaignName !== null) $data["CampaignName"] = $CampaignName;
			if($CampaignAccessKey !== null) $data["CampaignAccessKey"] = $CampaignAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleEmail !== null) $data["hasSingleEmail"] = $hasSingleEmail;
			if($CampaignStatus !== null) $data["CampaignStatus"] = $CampaignStatus;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->campaigns, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $CampaignName = null, $CampaignAccessKey = null, $GroupID = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $Commission = null, $PartnerSplit = null, $AdditionalNotes = null, $DateAdded = null, $hasSingleEmail = null, $CampaignStatus = null, $SaleInfoEntered = null, $StartDate = null, $EndDate = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $MasterCampaignID = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($CampaignName !== null) $data["CampaignName"] = $CampaignName;
			if($CampaignAccessKey !== null) $data["CampaignAccessKey"] = $CampaignAccessKey;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($hasSingleEmail !== null) $data["hasSingleEmail"] = $hasSingleEmail;
			if($CampaignStatus !== null) $data["CampaignStatus"] = $CampaignStatus;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->campaigns, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $CampaignName = null, $CampaignAccessKey = null, $GroupID = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $Commission = null, $PartnerSplit = null, $AdditionalNotes = null, $DateAdded = null, $hasSingleEmail = null, $CampaignStatus = null, $SaleInfoEntered = null, $StartDate = null, $EndDate = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $MasterCampaignID = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $CampaignName, $CampaignAccessKey, $GroupID, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $Commission, $PartnerSplit, $AdditionalNotes, $DateAdded, $hasSingleEmail, $CampaignStatus, $SaleInfoEntered, $StartDate, $EndDate, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $MasterCampaignID);
            } else {
                $this->update($DBRow->id, $userid, $CampaignName, $CampaignAccessKey, $GroupID, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $Commission, $PartnerSplit, $AdditionalNotes, $DateAdded, $hasSingleEmail, $CampaignStatus, $SaleInfoEntered, $StartDate, $EndDate, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $MasterCampaignID);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBC')){
	function DBC($obj = null) {
		return clsDBCampaigns::instance($obj);
	}
}