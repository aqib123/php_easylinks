<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructRotatorLinks'))
	include_once 'struct/RotatorLinks.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBRotatorLinks')){
	class clsDBRotatorLinks extends clsStructRotatorLinks {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_var($FieldValue, $FieldName, $ReturnField = "id"){
            $sql = "select ".$ReturnField." from ".DB()->rotator_links." where ".$FieldName." = '".$FieldValue."' ";
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBRotatorLinks|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->rotator_links->Asterisk." from ".DB()->rotator_links;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBRotatorLinks|null
         */
        function get_by_key($id){
            $Condition = DB()->rotator_links->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBRotatorLinks|null
         */
        function get_row($id = null, $userid = null, $RotatorID = null, $LinkBankID = null, $RotatorLinkType = null, $RotatorLinkURL = null, $MaxClicks = null, $StartDate = null, $EndDate = null, $RotatorLinkPosition = null, $DateAdded = null, $RotatorLinkStatus = null, $RotatorLinkLive = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($RotatorID !== null) $data["RotatorID"] = $RotatorID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($RotatorLinkType !== null) $data["RotatorLinkType"] = $RotatorLinkType;
			if($RotatorLinkURL !== null) $data["RotatorLinkURL"] = $RotatorLinkURL;
			if($MaxClicks !== null) $data["MaxClicks"] = $MaxClicks;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($RotatorLinkPosition !== null) $data["RotatorLinkPosition"] = $RotatorLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($RotatorLinkStatus !== null) $data["RotatorLinkStatus"] = $RotatorLinkStatus;
			if($RotatorLinkLive !== null) $data["RotatorLinkLive"] = $RotatorLinkLive;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->rotator_links->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBRotatorLinks[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->rotator_links->Asterisk." from ".DB()->rotator_links;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->rotator_links.".".$GroupBy." ORDER BY ".DB()->rotator_links.".".$OrderBy." ";
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->rotator_links->id.") from ".DB()->rotator_links;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBRotatorLinks[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id"){
            $sql = " from ".DB()->rotator_links;
            $FilterQuery = array();

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                }
            }

            $sqlCount = "select count(".DB()->rotator_links->id.") ".$sql;
            $sql = "select ".DB()->rotator_links->Asterisk.$sql." group by ".DB()->rotator_links.".".$GroupBy." ORDER BY ".DB()->rotator_links.".".$OrderBy." ";

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }


            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $RotatorID = null, $LinkBankID = null, $RotatorLinkType = null, $RotatorLinkURL = null, $MaxClicks = null, $StartDate = null, $EndDate = null, $RotatorLinkPosition = null, $DateAdded = null, $RotatorLinkStatus = null, $RotatorLinkLive = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($RotatorID !== null) $data["RotatorID"] = $RotatorID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($RotatorLinkType !== null) $data["RotatorLinkType"] = $RotatorLinkType;
			if($RotatorLinkURL !== null) $data["RotatorLinkURL"] = $RotatorLinkURL;
			if($MaxClicks !== null) $data["MaxClicks"] = $MaxClicks;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($RotatorLinkPosition !== null) $data["RotatorLinkPosition"] = $RotatorLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($RotatorLinkStatus !== null) $data["RotatorLinkStatus"] = $RotatorLinkStatus;
			if($RotatorLinkLive !== null) $data["RotatorLinkLive"] = $RotatorLinkLive;

            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->rotator_links, $data);
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $RotatorID = null, $LinkBankID = null, $RotatorLinkType = null, $RotatorLinkURL = null, $MaxClicks = null, $StartDate = null, $EndDate = null, $RotatorLinkPosition = null, $DateAdded = null, $RotatorLinkStatus = null, $RotatorLinkLive = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($RotatorID !== null) $data["RotatorID"] = $RotatorID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($RotatorLinkType !== null) $data["RotatorLinkType"] = $RotatorLinkType;
			if($RotatorLinkURL !== null) $data["RotatorLinkURL"] = $RotatorLinkURL;
			if($MaxClicks !== null) $data["MaxClicks"] = $MaxClicks;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($RotatorLinkPosition !== null) $data["RotatorLinkPosition"] = $RotatorLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($RotatorLinkStatus !== null) $data["RotatorLinkStatus"] = $RotatorLinkStatus;
			if($RotatorLinkLive !== null) $data["RotatorLinkLive"] = $RotatorLinkLive;

            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->rotator_links, $data, array("id" => $id));
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $RotatorID = null, $LinkBankID = null, $RotatorLinkType = null, $RotatorLinkURL = null, $MaxClicks = null, $StartDate = null, $EndDate = null, $RotatorLinkPosition = null, $DateAdded = null, $RotatorLinkStatus = null, $RotatorLinkLive = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($RotatorID !== null) $data["RotatorID"] = $RotatorID;
			if($LinkBankID !== null) $data["LinkBankID"] = $LinkBankID;
			if($RotatorLinkType !== null) $data["RotatorLinkType"] = $RotatorLinkType;
			if($RotatorLinkURL !== null) $data["RotatorLinkURL"] = $RotatorLinkURL;
			if($MaxClicks !== null) $data["MaxClicks"] = $MaxClicks;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($RotatorLinkPosition !== null) $data["RotatorLinkPosition"] = $RotatorLinkPosition;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($RotatorLinkStatus !== null) $data["RotatorLinkStatus"] = $RotatorLinkStatus;
			if($RotatorLinkLive !== null) $data["RotatorLinkLive"] = $RotatorLinkLive;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->rotator_links, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $RotatorID = null, $LinkBankID = null, $RotatorLinkType = null, $RotatorLinkURL = null, $MaxClicks = null, $StartDate = null, $EndDate = null, $RotatorLinkPosition = null, $DateAdded = null, $RotatorLinkStatus = null, $RotatorLinkLive = null){
            $DBRow = $this->get($Condition);
            if(!$DBRow){
                $this->insert($userid, $RotatorID, $LinkBankID, $RotatorLinkType, $RotatorLinkURL, $MaxClicks, $StartDate, $EndDate, $RotatorLinkPosition, $DateAdded, $RotatorLinkStatus, $RotatorLinkLive);
            } else {
                $this->update($DBRow->id, $userid, $RotatorID, $LinkBankID, $RotatorLinkType, $RotatorLinkURL, $MaxClicks, $StartDate, $EndDate, $RotatorLinkPosition, $DateAdded, $RotatorLinkStatus, $RotatorLinkLive);
            }
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBROLI')){
	function DBROLI($obj = null) {
		return clsDBRotatorLinks::instance($obj);
	}
}