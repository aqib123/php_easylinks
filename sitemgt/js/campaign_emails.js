﻿$(document).ready(function () {
    $('.copy-email-data').each(function () {
        var $this = $(this);
        CopyEmailData($this);
    });

    $(".create-new-emailbox").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $email_box_create_new = $this.closest("#email-box-create-new");
        var index = $(".emailbox").length > 0 ? (parseInt($(".emailbox:last").attr("data-box-index")) + 1) : 0;
        var newbox = emptybox.replace(/{index}/ig, index);

        $email_box_create_new.before(newbox);
    });

});

function CopyEmailData($ele) {
    var client = new ZeroClipboard($ele);
    client.on("ready", function (readyEvent) {
        client.on("beforecopy", function (event) {

        });

        client.on("copy", function (event) {
            var email = emails[$ele.attr("data-object-id")];

            if (typeof email != "undefined") {
                var copytext = $ele.attr("data-copy-type") == "subject" ? email.subject : email.body;
                copytext = $("<div/>").html(copytext).text();
            }
            event.clipboardData.clearData();
            event.clipboardData.setData("text/plain", copytext);
            event.clipboardData.setData("text/html", copytext);
        });

        client.on("aftercopy", function (event) {
            alert("Copied " + $ele.attr("data-copy-type") + " to clipboard!");
        });
    });
}

$('.email-copied').on('ifChanged', EmailCopied);
function EmailCopied(e) {
    var $this = $(this);
    if ($this.attr("data-emailid")) {
        var emailid = $this.attr("data-emailid");
        var url = $this.attr("data-url");
        var data = "act=update_email_copied&EmailID=" + emailid + "&isCopied=" + ($this.is(":checked") ? "1" : "0");

        $.ajax({
            method: "post",
            url: url,
            data: data
        }).done(function (data) {
            if ($this.attr("data-related")) {
                var $related = $($this.attr("data-related"));
                var jsonData = $.parseJSON(data);
                $related.before(jsonData.before);
                $related.html(jsonData.value);


                $related.find('.email-copied').on('ifChanged', EmailCopied);

                $related.after(jsonData.after);
                eval(jsonData.jscode);
            }
        });
    }
}