﻿/// <reference path="_references.js" />

$(document).ready(function (e) {
    $("#rotatorlinks").sortable({
        items: "div.rotatorlinkbox:not(.ui-state-disabled)",
        placeholder: "col-sm-12 rotatorlinkbox ui-state-highlight",
        tolerance: 'pointer',
        update: function (event, ui) {
            $("#EasyLinksWaitModel").modal();
            reset_rotatorlinks_position();
        }
    });
    //$("#rotatorlinks").disableSelection();
    //console.log($("#rotatorlinks").sortable("serialize"));

    $(".btn-add-rotatorlink").on("click", add_rotatorlink);

    $(".btn-advance-settings").on("click", btn_advance_settings_click);
    $(".btn-easylinks").on("click", showeasylink);
    $(".btn-customurl").on("click", showcustomurl);
    
    $(".btn-save-rotatorlink").on("click", save_rotatorlink)
    $(".btn-remove-rotatorlink").on("click", delete_rotatorlink)
    $(".btn-rotatorlink-status").on("click", change_rotatorlink_status);
});

function btn_advance_settings_click(e) {
    e.preventDefault();
    $this = $(this);
    if ($this.find(".fa").hasClass("fa-caret-right")) {
        $this.find(".fa").removeClass("fa-caret-right");
        $this.find(".fa").addClass("fa-caret-down");

        $this.closest(".rotatorlinkbox-contents").find(".advance-settings").slideDown("slow");
    } else if ($this.find(".fa").hasClass("fa-caret-down")) {
        $this.find(".fa").removeClass("fa-caret-down");
        $this.find(".fa").addClass("fa-caret-right");

        $this.closest(".rotatorlinkbox-contents").find(".advance-settings").slideUp("slow");
    }
}

function showeasylink(e) {
    e.preventDefault();
    $this = $(this);

    $this.closest(".rotatorlinkurl").find(".customurl").hide();
    $this.closest(".rotatorlinkurl").find(".easylinkurl").show();

    $this.closest(".rotatorlinkbox").find('[name="RotatorLinkType"]').val("linkbank");
}

function showcustomurl(e) {
    e.preventDefault();
    $this = $(this);

    $this.closest(".rotatorlinkurl").find(".easylinkurl").hide();
    $this.closest(".rotatorlinkurl").find(".customurl").show();
    $this.closest(".rotatorlinkbox").find('[name="RotatorLinkType"]').val("customurl");
}

function add_rotatorlink(e) {
    e.preventDefault();
    var $this = $(this);
    $.pleasewait();
    $this.off("click", add_rotatorlink);
    $.get(url, { act: "add_rotatorlink", index: ($(".rotatorlinkbox").length + 1) },
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", add_rotatorlink);
            if (jsonData.value != "") {
                $this.before(jsonData.value);
                var $rotator_linkbox = $this.prev(".rotatorlinkbox");

                $rotator_linkbox.find(".btn-advance-settings").on("click", btn_advance_settings_click);
                $rotator_linkbox.find(".btn-easylinks").on("click", showeasylink);
                $rotator_linkbox.find(".btn-customurl").on("click", showcustomurl);

                $rotator_linkbox.find(".btn-save-rotatorlink").on("click", save_rotatorlink)
                $rotator_linkbox.find(".btn-remove-rotatorlink").on("click", delete_rotatorlink)
                $rotator_linkbox.find(".btn-rotatorlink-status").on("click", change_rotatorlink_status);

                $.pleasewait('hide');
            }
        },
    "json");
}

function save_rotatorlink(e) {
    e.preventDefault();
    var $this = $(this);
    var $form = $this.closest("form");
    var data = "act=save_rotatorlink&" + $form.serialize();
    //console.log(data); return;

    $form.validator('validate');
    if ($form.find(".has-error").length > 0) {
        $form.find(".has-error").eq(0).find('.form-control').focus()
        return;
    }
    $.pleasewait();
    $this.off("click", save_rotatorlink);

    $.post(url, data,
        function (jsonData, textStatus, jqXHR) {
            if (jsonData.value == "success") {
                var boxid = $this.closest(".rotatorlinkbox").attr("id");
                $this.closest(".rotatorlinkbox").replaceWith(jsonData.box);
                var $newbox = $("#" + boxid);
                $newbox.find(".btn-advance-settings").on("click", btn_advance_settings_click)
                $newbox.find(".btn-easylinks").on("click", showeasylink)
                $newbox.find(".btn-customurl").on("click", showcustomurl)
                $newbox.find(".btn-save-rotatorlink").on("click", save_rotatorlink)
                $newbox.find(".btn-remove-rotatorlink").on("click", delete_rotatorlink)
                $newbox.find(".btn-rotatorlink-status").on("click", change_rotatorlink_status);
                console.log($newbox);
            } else {
                $this.on("click", save_rotatorlink);
            }
            $.pleasewait('hide');
        },
    "json");
}

function delete_rotatorlink(e) {
    e.preventDefault();

    if (confirm("Are you sure. You want to delete this?") == false) {
        return;
    }
    $.pleasewait();
    var $this = $(this);
    var data = "act=delete_rotatorlink&" + $this.closest("form").serialize();

    $this.off("click", delete_rotatorlink);

    $.post(url, data,
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", delete_rotatorlink);
            if (jsonData.value == "success") {
                $this.closest(".rotatorlinkbox").remove();
                reset_rotatorlinks_position();
            }
            //$.pleasewait('hide');
        },
    "json");
} 

function change_rotatorlink_status(e) {
    e.preventDefault();
    var $this = $(this);
    $.pleasewait();
    $this.off("click", change_rotatorlink_status);
    $.get(url, { act: "change_rotatorlink_status", rotatorlinkid: $this.closest(".rotatorlinkbox").attr("data-rotatorlink-id") },
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", change_rotatorlink_status);
            if (jsonData.value == "success") {
                if ($this.hasClass("btn-success")) {
                    $this.removeClass("btn-success")
                    $this.addClass("btn-danger")
                    $this.html("Paused")
                } else if ($this.hasClass("btn-danger")) {
                    $this.removeClass("btn-danger")
                    $this.addClass("btn-success")
                    $this.html("Active")
                }
            }
            $.pleasewait('hide');
        },
    "json");
}

function reset_rotatorlinks_position() {
    var index = 0;
    var data = "act=update_rotatorlink_postion&" + $("#rotatorlinks").sortable("serialize");

    $.get(url, data,
        function (data, textStatus, jqXHR) {
            $("#EasyLinksWaitModel").modal('hide');
        },
    "json");

    $(".rotatorlinkbox").each(function (e) {
        var $this = $(this);
        $this.find(".rotatorlinkposition").html(++index);
    });
}