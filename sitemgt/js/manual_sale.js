﻿$(document).ready(function () {
    $('#isSplitPartnerSale').on('ifChanged', function (e) {
        $("#SplitPartnerOptions").toggle("fast");
    });

    $(".btn-add-partner").on("click", function (e) {
        e.preventDefault();

        $('#IncomePercentage').attr("required", "required");
        $('#OtherIncomePercentage').attr("required", "required");
        $('#ExpensePercentage').attr("required", "required");

        $('#IncomePercentage').trigger('input.bs.validator');
        $('#OtherIncomePercentage').trigger('input.bs.validator');
        $('#ExpensePercentage').trigger('input.bs.validator');

        if ($("#PartnerID").val() != "" &&
            $("#IncomePercentage").val() != "" && !$("#IncomePercentage").closest(".form-group").hasClass("has-error") &&
            $("#OtherIncomePercentage").val() != "" && !$("#OtherIncomePercentage").closest(".form-group").hasClass("has-error") &&
            $("#ExpensePercentage").val() != "" && !$("#ExpensePercentage").closest(".form-group").hasClass("has-error")) {
            $.pleasewait();
            var data = "act=add_manualsalesplitpartner&SplitPartnerID=" + $("#PartnerID").val() + "&ManualSaleID=" + ManualSaleID + "&IncomePercentage=" + $("#IncomePercentage").val() + "&OtherIncomePercentage=" + $("#OtherIncomePercentage").val() + "&ExpensePercentage=" + $("#ExpensePercentage").val();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (jsonData) {
                    if (jsonData.result == "successfull") {
                        $(".manual-sale-split-partners-list").append(jsonData.row);
                        $(".manual-sale-split-partners-list").find("li:last").find(".remove-manual-sale-split-partner").on("click", RemoveManualSaleSplitPartner);
                    }
                },
                complete: function (jqXHR, textStatus) {
                    $.pleasewait('hide');
                    $('#IncomePercentage').removeAttr("required");
                    $('#OtherIncomePercentage').removeAttr("required");
                    $('#ExpensePercentage').removeAttr("required");

                    $("#IncomePercentage").val("");
                    $("#OtherIncomePercentage").val("");
                    $("#ExpensePercentage").val("");
                }
            });
        } else {
            //alert("Select Split Partner First");
            //$("#PartnerID").focus();
        }
    });

    $(".remove-manual-sale-split-partner").on("click", RemoveManualSaleSplitPartner);
});

function RemoveManualSaleSplitPartner(e) {
    e.preventDefault();
    var $this = $(this);
    var ManualSaleSplitPartnerID = $this.attr("data-splitpartner-id");

    if (!ConfirmDelete())
        return;

    $.pleasewait();

    $.ajax({
        type: "POST",
        url: url,
        data: "act=remove_manualsalesplitpartner&ManualSaleSplitPartnerID=" + ManualSaleSplitPartnerID,
        dataType: "json",
        success: function (jsonData) {
            if (jsonData.result == "successfull") {
                $this.closest("li").remove();
            }
        },
        complete: function (jqXHR, textStatus) {
            $.pleasewait('hide');
        }
    });
}