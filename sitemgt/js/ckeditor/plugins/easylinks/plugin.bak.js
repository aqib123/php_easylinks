/**
 * @license Copyright © 2013 Stuart Sillitoe <stuart@vericode.co.uk>
 * This is open source, can modify it as you wish.
 *
 * Stuart Sillitoe
 * stuartsillitoe.co.uk
 *
 */

CKEDITOR.plugins.add('easylinks',
{
    init: function (editor) {
        var config = editor.config;

        editor.ui.addButton('easylinks',
        {
            //label: 'Insert Timestamp',
            //command: 'insertTimestamp',
            //icon: this.path + 'images/timestamp.png',

            label: "Easylinks",
            title: "Insert Easylink",
            voiceLabel: "Easylinks",
            toolbar: 'insert',
            className: 'cke_format',
            //command: 'insertLink',

            beforeInit: function (editor) {
                console.log("beforeInit ");
            },

            click: function (editor) {
                var selectedText = editor.getSelection().getSelectedText();
                if (selectedText == "")
                    selectedText = "test";

                //var selectedElement = editor.getSelection().getStartElement();

                var newElement = new CKEDITOR.dom.element("a");
                newElement.setAttributes({ 'href': 'http://msn.com/', 'class': 'added' });
                newElement.setText(selectedText);
                editor.insertElement(newElement);
            }
        });

        editor.addCommand('insertLink', {
            exec: function (editor) {
                var selectedText = editor.getSelection().getSelectedText();
                //var selectedElement = editor.getSelection().getStartElement();
                if (selectedText != "") {
                    var selectedElement = editor.getSelection().getStartElement();
                    //if (selectedElement.is("a")) {
                    //    var newElement = new CKEDITOR.dom.element("a");
                    //} else {
                    //    var newElement = new CKEDITOR.dom.element("a");
                    //    newElement.setAttributes({href: 'http://msn.com/' })
                    //}

                    var newElement = new CKEDITOR.dom.element("a");
                    newElement.setAttributes({ href: 'http://msn.com/' })
                    newElement.setText(selectedText);
                    editor.insertElement(newElement);
                }
                //var curElement = editor.getSelection().getStartElement();
                //var curHtml = curElement.getHtml();
                ////console.log(editor.getData());
                ////console.log(editor.getSelection().getRanges());
                //console.log(editor);

                //editor.insertHtml('changed text');
                //var timestamp = new Date();   
                //editor.insertHtml('The current date and time is: <em>' + timestamp.toString() + '</em>');
            }
        });
    }
});
