/**
 * @license Copyright © 2013 Stuart Sillitoe <stuart@vericode.co.uk>
 * This is open source, can modify it as you wish.
 *
 * Stuart Sillitoe
 * stuartsillitoe.co.uk
 *
 */

CKEDITOR.plugins.add('easylinks',
{
    init: function (editor) {
        var config = editor.config;

        editor.ui.addButton('easylinks',
        {
            label: "<i class='fa fa-link'></i>",
            title: "Insert Easylink",
            voiceLabel: "Easylinks",
            toolbar: 'easylinks_group, 1',
            className: 'cke_easylinks',
            command: 'insertLink',
            //icon: this.path + 'images/timestamp.png',

            beforeInit: function (editor) {
                console.log("beforeInit ");
            },

            click: function (editor) {
                if (editor != null && editor.mode != "source") {
                    var selectedText = editor.getSelection() != null ? editor.getSelection().getSelectedText() : "";
                    if (selectedText == "")
                        selectedText = "Click Here";

                    //var selectedElement = editor.getSelection().getStartElement();

                    var newElement = new CKEDITOR.dom.element("a");
                    newElement.setAttributes({ 'href': '{easy_links_mail}', 'class': 'easy_links_mail' });
                    newElement.setText(selectedText);

                    editor.focus();
                    editor.fire('saveSnapshot');
                    editor.insertElement(newElement);
                    editor.fire('saveSnapshot');
                }
            }
        });

        editor.addCommand('insertLink', {
            exec: function (editor) {
                
            },

            //contextSensitive: 1,
            //startDisabled: 1,
            //requiredContent: 'a[href]'
        });
    }
});
