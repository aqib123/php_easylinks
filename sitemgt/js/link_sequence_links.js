﻿/// <reference path="easylinks.js" />

$(document).ready(function (e) {
    $("#link_sequence_links").sortable({
        items: "div.link_sequence_linkbox:not(.ui-state-disabled)",
        placeholder: "col-sm-12 link_sequence_linkbox ui-state-highlight",
        tolerance: 'pointer',
        update: function (event, ui) {
            $("#EasyLinksWaitModel").modal();
            reset_link_sequence_links_position();
        }
    });
    //$("#link_sequence_links").disableSelection();
    //console.log($("#link_sequence_links").sortable("serialize"));

    $(".btn-add-link_sequence_link").on("click", add_link_sequence_link);

    $(".btn-advance-settings").on("click", btn_advance_settings_click);
    $(".btn-easylinks").on("click", showeasylink);
    $(".btn-customurl").on("click", showcustomurl);
    
    $(".btn-save-link_sequence_link").on("click", save_link_sequence_link)
    $(".btn-remove-link_sequence_link").on("click", delete_link_sequence_link)
    $(".btn-link_sequence_link-status").on("click", change_link_sequence_link_status);
});

function btn_advance_settings_click(e) {
    e.preventDefault();
    $this = $(this);
    if ($this.find(".fa").hasClass("fa-caret-right")) {
        $this.find(".fa").removeClass("fa-caret-right");
        $this.find(".fa").addClass("fa-caret-down");

        $this.closest(".link_sequence_linkbox-contents").find(".advance-settings").slideDown("slow");
    } else if ($this.find(".fa").hasClass("fa-caret-down")) {
        $this.find(".fa").removeClass("fa-caret-down");
        $this.find(".fa").addClass("fa-caret-right");

        $this.closest(".link_sequence_linkbox-contents").find(".advance-settings").slideUp("slow");
    }
}

function showeasylink(e) {
    e.preventDefault();
    $this = $(this);

    $this.closest(".link_sequence_linkurl").find(".customurl").hide();
    $this.closest(".link_sequence_linkurl").find(".easylinkurl").show();

    $this.closest(".link_sequence_linkurl").find("[name=LinkSequenceLinkName]").removeAttr("required");


    $this.closest(".link_sequence_linkbox").find('[name="LinkSequenceLinkType"]').val("linkbank");
}

function showcustomurl(e) {
    e.preventDefault();
    $this = $(this);

    $this.closest(".link_sequence_linkurl").find(".easylinkurl").hide();
    $this.closest(".link_sequence_linkurl").find(".customurl").show();
    $this.closest(".link_sequence_linkbox").find('[name="LinkSequenceLinkType"]').val("customurl");
    $this.closest(".link_sequence_linkurl").find("[name=LinkSequenceLinkName]").attr("required", "required");
}

function add_link_sequence_link(e) {
    e.preventDefault();
    var $this = $(this);
    $.pleasewait();
    $this.off("click", add_link_sequence_link);
    $.get(url, { act: "add_link_sequence_link", index: ($(".link_sequence_linkbox").length + 1) },
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", add_link_sequence_link);
            if (jsonData.value != "") {
                $this.before(jsonData.value);
                var $link_sequence_linkbox = $this.prev(".link_sequence_linkbox");

                $link_sequence_linkbox.find(".btn-advance-settings").on("click", btn_advance_settings_click);
                $link_sequence_linkbox.find(".btn-easylinks").on("click", showeasylink);
                $link_sequence_linkbox.find(".btn-customurl").on("click", showcustomurl);

                $link_sequence_linkbox.find(".btn-save-link_sequence_link").on("click", save_link_sequence_link)
                $link_sequence_linkbox.find(".btn-remove-link_sequence_link").on("click", delete_link_sequence_link)
                $link_sequence_linkbox.find(".btn-link_sequence_link-status").on("click", change_link_sequence_link_status);
                CreateDateRangePicker($link_sequence_linkbox.find('.input-singledate-time'));
                CreateSelect2($link_sequence_linkbox.find('.select2'));
                $link_sequence_linkbox.find('input[type="checkbox"].blue, input[type="radio"].blue').each(createblueicheck);
                $.pleasewait('hide');
            }
        },
    "json");
}

function save_link_sequence_link(e) {
    e.preventDefault();
    var $this = $(this);
    var $form = $this.closest("form");
    var data = "act=save_link_sequence_link&" + $form.serialize();
    //console.log(data); return;

    $form.validator('validate');
    if ($form.find(".has-error").length > 0) {
        $form.find(".has-error").eq(0).find('.form-control').focus()
        return;
    }
    $.pleasewait();
    $this.off("click", save_link_sequence_link);

    $.post(url, data,
        function (jsonData, textStatus, jqXHR) {
            if (jsonData.value == "success") {
                var boxid = $this.closest(".link_sequence_linkbox").attr("id");
                $this.closest(".link_sequence_linkbox").replaceWith(jsonData.box);
                var $newbox = $("#" + boxid);
                $newbox.find(".btn-advance-settings").on("click", btn_advance_settings_click)
                $newbox.find(".btn-easylinks").on("click", showeasylink)
                $newbox.find(".btn-customurl").on("click", showcustomurl)
                $newbox.find(".btn-save-link_sequence_link").on("click", save_link_sequence_link)
                $newbox.find(".btn-remove-link_sequence_link").on("click", delete_link_sequence_link)
                $newbox.find(".btn-link_sequence_link-status").on("click", change_link_sequence_link_status);
                CreateDateRangePicker($newbox.find('.input-singledate-time'));
                CreateSelect2($newbox.find('.select2'));

                $newbox.find('input[type="checkbox"].blue, input[type="radio"].blue').each(createblueicheck);
            } else {
                $this.on("click", save_link_sequence_link);
            }
            $.pleasewait('hide');
        },
    "json");
}

function delete_link_sequence_link(e) {
    e.preventDefault();

    if (confirm("Are you sure. You want to delete this?") == false) {
        return;
    }
    $.pleasewait();
    var $this = $(this);
    var data = "act=delete_link_sequence_link&" + $this.closest("form").serialize();

    $this.off("click", delete_link_sequence_link);

    $.post(url, data,
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", delete_link_sequence_link);
            if (jsonData.value == "success") {
                $this.closest(".link_sequence_linkbox").remove();
                reset_link_sequence_links_position();
            }
            //$.pleasewait('hide');
        },
    "json");
} 

function change_link_sequence_link_status(e) {
    e.preventDefault();
    var $this = $(this);
    $.pleasewait();
    $this.off("click", change_link_sequence_link_status);
    $.get(url, { act: "change_link_sequence_link_status", link_sequence_linkid: $this.closest(".link_sequence_linkbox").attr("data-link_sequence_link-id") },
        function (jsonData, textStatus, jqXHR) {
            $this.on("click", change_link_sequence_link_status);
            if (jsonData.value == "success") {
                if ($this.hasClass("btn-success")) {
                    $this.removeClass("btn-success")
                    $this.addClass("btn-danger")
                    $this.html("Paused")
                } else if ($this.hasClass("btn-danger")) {
                    $this.removeClass("btn-danger")
                    $this.addClass("btn-success")
                    $this.html("Active")
                }
            }
            $.pleasewait('hide');
        },
    "json");
}

function reset_link_sequence_links_position() {
    var index = 0;
    var data = "act=update_link_sequence_link_postion&" + $("#link_sequence_links").sortable("serialize");

    $.get(url, data,
        function (data, textStatus, jqXHR) {
            $("#EasyLinksWaitModel").modal('hide');
        },
    "json");

    $(".link_sequence_linkbox").each(function (e) {
        var $this = $(this);
        $this.find(".link_sequence_linkposition").html(++index);
    });
}

function CheckEndTime($el) {
    var $link_sequence_linkurl = $el.closest(".link_sequence_linkurl");
    var EndDateType = $link_sequence_linkurl.find('[name=EndDateType]:checked').val();

    if (EndDateType == "EndTime") {
        var Days = $link_sequence_linkurl.find('[name=Days]').val();
        var Hours = $link_sequence_linkurl.find('[name=Hours]').val();
        var Minutes = $link_sequence_linkurl.find('[name=Minutes]').val();

        if (Days == 0 && Hours == 0 && Minutes == 0)
            return false;
        else
            return true;
    } else {
        return true;
    }
    //console.log(EndDateType);

    //var value = $el.val() != null ? $el.val().trim() : "";
    ////console.log("value: " + value);
    //return (value != "" && value != -1);
    //console.log("ok");
}