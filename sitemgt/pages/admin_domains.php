<?php
$pageurl = "admin_domains";
$pagetitle = "Admin Domains";
$modulename = "admin_domains";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("admin-domain")?>" role="button">New Admin Domain</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("admin-domains")?>" role="button">Admin Domains</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Domain Name</span></th>
                            <th><span>Domain Slug</span></th>
                            <th><span>Domain URL</span></th>
                            <th><span class="text-center">Date Added</span></th>
                            <th><span class="text-center">Last Updated</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->domains." where DomainType='admindomain'";
                        $domains = $db->get_results($sql);
                        foreach ($domains as $domain){
                        ?>
                        <tr>
                            <td><span><?php echo $domain->DomainName?></span></td>
                            <td><span><?php echo $domain->DomainSlug?></span></td>
                            <td><a href="<?php echo $domain->DomainUrl?>"><?php echo $domain->DomainUrl?></a></td>
                            <td><span class="text-center"><?php echo date("d/m/Y", $domain->DateAdded);?></span></td>
                            <td><span class="text-center"><?php echo date("d/m/Y", $domain->DateUpdated);?></span></td>
                            <td>
                                <a href="<?php site_url("admin-domain/?domainid=".$domain->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("admin-domains/?act=delete_admin_domain&domainid=".$domain->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>