<?php
$pagetitle = "Members Dashboard";
$pageurl = "dashboard";
$modulename = "dashboard";

include_once "member_header.php";

$current_userid = $current_user->id;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="active"><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<link href="<?php site_url("css/jquery-jvectormap-2.0.3.css");?>" rel="stylesheet" type="text/css" />
<script src="<?php site_url("js/jquery-jvectormap-2.0.3.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/jquery-jvectormap-world-merc-en.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/gdp-data.js");?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo "IpAddresses = ['".implode("','",  $IpAddresses)."'];\n";?>
        CreateMap($('#map_canvas'));
        AddMarker();

        CreateDateRange($("#dashboard_statsdaterange"));

        $(".yellow-icon").each(function () {
            var $this = $(this);
            MoveRowToTop($this);
        });

        $(".red-icon").each(function () {
            var $this = $(this);
            MoveRowToTop($this);
        });

        //CreatePieChart($('#TopChart'));
    });
    function MoveRowToTop($icon) {
        var $tr = $icon.closest("tr");
        if ($tr.prev("tr").length > 0) {
            var $tbody = $icon.closest("tbody");
            $tr.insertBefore($tbody.find("tr").eq(0));
        }
    }
</script>
<?php
    include_once "member_footer.php";
?>