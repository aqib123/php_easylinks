<?php
include_once "../includes/functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Easy Links | <?php echo $pagetitle;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php site_url("css/bootstrap.min.css");?>" />

    <!-- Font Awesome Icons -->
    <link href="<?php site_url("css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="<?php site_url("css/AdminLTE.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php site_url("css/skin-black-light.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php site_url("css/blue.css");?>" />

    <!-- Select2 -->
    <link href="<?php site_url("css/select2.min.css");?>" rel="stylesheet" />

    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/daterangepicker.css");?>" />

    <!-- custom scrollbar stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery.mCustomScrollbar.min.css");?>" />

    <!-- Datatables stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/dataTables.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/responsive/responsive.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/buttons/buttons.bootstrap.min.css");?>" />

    <link href="<?php site_url("css/sitestyle.css");?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php site_url("js/jquery-1.11.3.min.js");?>"></script>

    <!-- jQueryUI -->
    <script src="<?php site_url("js/jquery-ui.min.js");?>"></script>

    <!-- jquery.picture.cut stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery-ui-1.10.0.custom.css");?>" />

    <!-- bootstrap tokenfield plugin -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/bootstrap-tokenfield.min.css");?>" />

    <script>
        var siteurl = '<?php site_url("");?>';
    </script>
</head>
<body class="skin-black-light sidebar-mini">

    <div id="EasyLinksModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="EasyLinksModelTitle">Modal title</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EasyLinksModelSave">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="EasyLinksWaitModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksWaitModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body">
                    <div class="progress progress-sm active">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">100%</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Site wrapper -->
    <div class="wrapper">

        <?php
        if (isset($_GET) && isset($_GET['success']) && isset($_GET['message'])){
            if($_GET['success'] == false){
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
            <h4><i class="icon fa fa-ban"></i>Alert!</h4>
            <?php echo $_GET['message'];?>
        </div>
        <?php
            }else{
        ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
            <h4><i class="icon fa fa-check"></i>Alert!</h4>
            <?php echo $_GET['message'];?>   
        </div>
        <?php
            }
        }               
        ?>

        <div class="container">
            <header class="site-header">
                <div class="navbar-header">
                    <a href="<?php site_url("");?>" class="navbar-brand">
                        <img src="<?php site_url("images/easylinks_logo.png")?>" class="easylinks_logo logo" title="Easy Links" /></a>
                    <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>-->
                </div>
                <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php site_url("login")?>">Login</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>-->
                <!-- /.navbar-collapse -->
            </header>
        </div>
        <!-- =============================================== -->
