<?php
$pageurl = "option";
$pagetitle = "Options";
$modulename = "options";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['optionid']) && is_numeric($_GET['optionid'])){
    $OptionID = intval($_GET["optionid"]);
    $EditOption = $db->get_row("select * from ".$db->options." where OptionDefault=0 and id=".$OptionID);
    if($EditOption){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("option")?>" role="button">New Option</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("options")?>" role="button">Options</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Option</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("option");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="OptionName" class="col-sm-3 control-label">Option Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditOption->OptionName;?>" class="form-control " id="OptionName" name="OptionName" placeholder="Option Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="OptionValue" class="col-sm-3 control-label">Option Value</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control " id="OptionValue" name="OptionValue" placeholder="Option Value" required="required"><?php if($isUpdateForm) echo $EditOption->OptionValue;?></textarea>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="OptionEnabled" class="col-sm-3 control-label">Option Enable</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="OptionEnabled" name="OptionEnabled" <?php if($isUpdateForm && $EditOption->OptionEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("options")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="optionid" value="<?php echo $EditOption->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_option" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>