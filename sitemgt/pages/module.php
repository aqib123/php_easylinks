<?php
$pageurl = "module";
$pagetitle = "Modules";
$modulename = "modules";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['moduleid']) && is_numeric($_GET['moduleid'])){
    $ModuleID = intval($_GET["moduleid"]);
    $EditModule = $db->get_row("select * from ".$db->modules." where id=".$ModuleID);
    if($EditModule){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("module")?>" role="button">New Module</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("modules")?>" role="button">Modules</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-modules")?>" role="button">Level Modules</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Module</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("module");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="ModuleName" class="col-sm-3 control-label">Module Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditModule->ModuleName;?>" class="form-control " id="ModuleName" name="ModuleName" placeholder="Module Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="ModuleLabel" class="col-sm-3 control-label">Module Label</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditModule->ModuleLabel;?>" class="form-control " id="ModuleLabel" name="ModuleLabel" placeholder="Module Label" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="ModuleEnabled" class="col-sm-3 control-label">Module Enable</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="ModuleEnabled" name="ModuleEnabled" <?php if($isUpdateForm && $EditModule->ModuleEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("modules")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="moduleid" value="<?php echo $EditModule->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_module" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>