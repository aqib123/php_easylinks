<?php

$pageurl = "users";

$pagetitle = "Users";

$modulename = "users";



include_once "member_header.php";



$item = get_menuitem($pageurl);

?>

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper white-bg">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <ol class="breadcrumb">

            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>

            <li class="active"><?php echo $pagetitle;?></li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-xs-12 top-buttons">

                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("user")?>" role="button">New User</a>

                <a class="btn btn-flat btn btn-gray" href="<?php site_url("users")?>" role="button">Users</a>

            </div>

        </div>

        <hr />

        <div class="row">

            <div class="col-sm-12">

                <h1><?php echo $pagetitle;?></h1>

            </div>

        </div>

        <br />

        <div class="box box-gray-">

            <div class="box-header with-border">

                <h3 class="box-title"><?php echo $pagetitle;?></h3>

                <div class="box-tools pull-right">

                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>

                </div>

            </div>

            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">

                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-nosort-columns="0" data-default-sort-column="1" data-paging="true" data-paging="true" data-searching="true" data-info="true">

                    <thead>

                        <tr>

                            <th><span></span></th>

                            <th><span>Display Name</span></th>

                            <th><span>UserName</span></th>

                            <th><span>Nic Name</span></th>

                            <th><span>Email Address</span></th>

                            <th><span>Timezone</span></th>

                            <th><span class="text-center">Registered</span></th>

                            <th><span class="text-center">Level(s)</span></th>

                            <th></th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php

                        $sql = "select ".$db->users.".* from ".$db->users;

                        if(isset($_GET['levelid']) && is_numeric($_GET['levelid']))

                            $sql .= " inner join ".$db->user_levels." on ".$db->users.".id = ".$db->user_levels.".UserID where LevelID=".intval($_GET["levelid"])." group by UserID";



                        $users = $db->get_results($sql);

                        foreach ($users as $user){

                            $levels = $db->get_results("select ".$db->levels.".* from ".$db->levels." inner join ".$db->user_levels." on ".$db->levels.".id = ".$db->user_levels.".LevelID where UserID =".$user->id);

                        ?>

                        <tr>

                            <td class="text-center">

                                <!-- <img src="<?php main_url(!empty($user->user_picture)?$user->user_picture:"images/avatar5.png");?>" class="img-circle img-user-list" alt="User Image" /> -->
                                
                                <?php if(empty($user->user_picture)){
                                    ?>
                                    <img src="https://easylinks.io/images/avatar5.png" class="img-circle img-user-list" alt="User Image" />
                                <?php
                                }else{
                                ?>
                                    <img src="<?php  echo 'https://easylinks.io'.$user->user_picture ?>" class="img-circle img-user-list" alt="User Image" />
                                <?php
                                }
                                ?>

                            </td>

                            <td><span><?php echo $user->display_name?></span></td>

                            <td><span><?php echo $user->user_login?></span></td>

                            <td><span><?php echo $user->user_nicename?></span></td>

                            <td><span><?php echo $user->user_email?></span></td>

                            <td><span><?php echo $user->timezoneid?></span></td>

                            <td><span class="text-center"><?php echo $user->user_registered;?></span></td>

                            <td>

                                <?php

                            if(is_array($levels)){

                                foreach ($levels as $level){

                                ?>

                                <div class="text-center"><?php echo $level->LevelName;?></div>

                                <?php

                                }

                            }

                                ?>

                            </td>

                            <td>

                                <a href="<?php site_url("user/?userid=".$user->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>

                                <a href="<?php site_url("users/?act=delete_user&userid=".$user->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>

                                <a href="<?php main_url("login/?act=admin_user_login&admin_username=".$current_user->user_login."&admin_password=".$current_user->user_pass."&client_username=".$user->user_login."&client_password=".$user->user_pass);?>" class="btn btn-sm btn-links" title="" target="_blank"><i class="fa fa-sign-in"></i></a>

                            </td>

                        </tr>

                        <?php	

                        }

                        ?>

                    </tbody>

                </table>

            </div>

        </div>

    </section>

    <!-- /.content -->

</div>

<!-- /.content-wrapper -->

<?php

include_once "member_footer.php";

?>