<?php
$pageurl = "level-menu_items";
$pagetitle = "Level Menu Items";
$menu_itemname = "level-menu-items";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">

            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("level-menu-items");?>">
                                <?php if(isset($_GET["levelid"]) && is_numeric($_GET["levelid"])) { ?>
                                <input type="hidden" name="levelid" value="<?php echo $_GET["levelid"];?>" />
                                <?php } ?>
                                <?php if(isset($_GET["menu_itemid"]) && is_numeric($_GET["menu_itemid"])) { ?>
                                <input type="hidden" name="menu_itemid" value="<?php echo $_GET["menu_itemid"];?>" />
                                <?php } ?>
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                                            <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-nobuttons="true">
                                                <thead>
                                                    <tr>
                                                        <th><span class="text-center">Position</span></th>
                                                        <th>Menu Item</th>
                                                        <?php
                                                        $sql = "select * from ".$db->levels.((isset($_GET["levelid"]) && is_numeric($_GET["levelid"]))?" where id=".$_GET["levelid"]." ":"");
                                                        $levels = $db->get_results($sql);
                                                        foreach ($levels as $level){
                                                        ?>
                                                        <th><span class="text-center"><?php echo $level->LevelName?></span></th>
                                                        <?php
                                                        }   
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sql = "select * from ".$db->menu_items." where MenuItemParentID=0 and MenuItemHasAdmin=0 ".((isset($_GET["menu_itemid"]) && is_numeric($_GET["menu_itemid"]))?" and id=".$_GET['menu_itemid']." ":"");
                                                    $menu_items = $db->get_results($sql);
                                                    foreach ($menu_items as $menu_item){
                                                    ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <span><?php echo $menu_item->MenuItemPosition;?></span>
                                                        </td>
                                                        <td>
                                                            <h5><?php echo $menu_item->MenuItemLabel;?></h5>
                                                        </td>
                                                        <?php
                                                        foreach ($levels as $level){
                                                            $LevelMenuItemEnabled = $db->get_var("select LevelMenuItemEnabled from ".$db->level_menu_items." where MenuItemID=".$menu_item->id." and LevelID=".$level->id);
                                                        ?>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="blue" name="<?php echo "level_".$level->id."_menu_item_".$menu_item->id;?>" value="1" <?php if($LevelMenuItemEnabled && $LevelMenuItemEnabled == 1) echo ' checked="checked" ';?> />
                                                        </td>
                                                        <?php
                                                        }   
                                                        ?>
                                                    </tr>
                                                    <?php	
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("level-menu-items")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_level_menu_items" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>