<?php
$pageurl = "menu-items";
$pagetitle = "Menu Items";
$modulename = "menu_items";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("menu-item")?>" role="button">New Menu Item</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("menu-items")?>" role="button">Menu Items</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-menu-items")?>" role="button">Level Menu Items</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-default-sort-column="3">
                    <thead>
                        <tr>
                            <th><span>Label</span></th>
                            <th><span>URL</span></th>
                            <th><span>Class</span></th>
                            <th><span class="text-center">Position</span></th>
                            <th><span class="text-center">Level(s)</span></th>
                            <th><span class="text-center">Enabled</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->menu_items." where MenuItemParentID=0 and MenuItemHasAdmin=0";//." where MenuItemDefault=0";
                        $menu_items = $db->get_results($sql);
                        foreach ($menu_items as $menu_item){
                            $levels = $db->get_var("select count(*) from ".$db->level_menu_items." where LevelMenuItemEnabled=1 and MenuItemID=".$menu_item->id);
                        ?>
                        <tr>
                            <td><span><?php echo $menu_item->MenuItemLabel?></span></td>
                            <td><span><?php echo $menu_item->MenuItemURL?></span></td>
                            <td><span><?php echo $menu_item->MenuItemClass?></span></td>
                            <td><span class="text-center"><?php echo $menu_item->MenuItemPosition;?></span></td>
                            <td><span class="text-center"><?php echo $levels;?></span></td>
                            <td><span class="text-center"><?php echo $menu_item->MenuItemEnabled == 0?"NO":"YES";?></span></td>
                            <td>
                                <a href="<?php site_url("level-menu-items/?menu_itemid=".$menu_item->id);?>" class="btn btn-sm btn-links" data-toggle="qtiptooltip" title="Levels Settings"><i class="fa fa-cogs"></i></a>
                                <?php if($menu_item->MenuItemDefault == 0) { ?>
                                <a href="<?php site_url("menu-item/?menu_itemid=".$menu_item->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("menu-items/?act=delete_menu_item&menu_itemid=".$menu_item->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>