<?php
global $db, $EditUser;
$pagetitle = "User";
$pageurl = "user";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['userid']) && is_numeric($_GET['userid'])){
    $UserID = intval($_GET["userid"]);
    $EditUser = $db->get_row("select * from ".$db->users." where id=".$UserID);
    if($EditUser){
        $EditUser->LevelIDs = $db->get_col("select LevelID from ".$db->user_levels." where UserId=".$EditUser->id);
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("user")?>" role="button">New User</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("users")?>" role="button">Users</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> User</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("user")?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="user_login" class="col-sm-4 control-label">Username</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo isset($_POST['user_login'])?$_POST['user_login']:$EditUser->user_login;?>" class="form-control " id="user_login" name="user_login" placeholder="Username" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="NewPassword" class="col-sm-4 control-label">New Password</label>
                                            <div class="col-sm-8" />
                                            <input type="password" value="" class="form-control " id="NewPassword" name="NewPassword" placeholder="New Password" <?php if(!$isUpdateForm) echo ' required="required" '?> <?php echo ALLALLOWED_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="ConfirmPassword" class="col-sm-4 control-label">Confirm Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" value="" class="form-control " id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password" data-match="#NewPassword" data-match-error="Passwords do not match" <?php if(!$isUpdateForm) echo ' required="required" '?> <?php echo ALLALLOWED_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="user_nicename" class="col-sm-4 control-label">Nic Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="<?php echo isset($_POST['user_nicename'])?$_POST['user_nicename']:$EditUser->user_nicename;?>" class="form-control " id="user_nicename" name="user_nicename" placeholder="Nic Name" <?php echo NAME_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="user_email" class="col-sm-4 control-label">Email Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="<?php echo isset($_POST['user_email'])?$_POST['user_email']:$EditUser->user_email;?>" class="form-control " id="user_email" name="user_email" placeholder="Email Address" required="required" <?php echo EMAIL_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="display_name" class="col-sm-4 control-label">Display Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="<?php echo isset($_POST['display_name'])?$_POST['display_name']:$EditUser->display_name;?>" class="form-control " id="display_name" name="display_name" placeholder="Display Name" required="required" <?php echo NAME_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group select-form-group">
                                        <label for="LevelID" class="col-sm-4 col-xs-12 control-label">Choose Level</label>
                                        <div class="col-sm-8 col-xs-12 ">
                                            <select name="LevelID" id="LevelID" class="form-control select2" style="width: 100%" data-select="select" data-enable-search="true">
                                                <?php
                                                $levels = $db->get_results("select * from ".$db->levels." where LevelEnabled=1");
                                                foreach ($levels as $level) {
                                                ?>
                                                <option value="<?php echo $level->id;?>" <?php if($isUpdateForm && in_array($level->id, $EditUser->LevelIDs)) echo ' selected = "selected" ';?>><?php echo $level->LevelName;?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group select-form-group">
                                        <label for="timezoneid" class="col-sm-4 col-xs-12 control-label">Choose Timezone</label>
                                        <div class="col-sm-8 col-xs-12 ">
                                            <select name="timezoneid" id="timezoneid" class="form-control select2" style="width: 100%" data-select="select" data-enable-search="true">
                                                <option value="EST" <?php if($EditUser->timezoneid == "EST") echo ' selected = "selected" ';?>>Default(EST)</option>
                                                <?php
                                                $zones = timezone_identifiers_list();
                                                foreach ($zones as $zone) {
                                                    $zone = explode('/', $zone);
                                                    if (isset($zone[1]) != ''){
                                                        $zonevalue = $zone[0]. '/' . $zone[1];
                                                        $zonename = str_replace('_', ' ', $zone[0])." - ".str_replace('_', ' ', $zone[1]);
                                                    } else {
                                                        $zonevalue = $zone[0];
                                                        $zonename = str_replace('_', ' ', $zone[0]);
                                                    }
                                                ?>
                                                <option value="<?php echo $zonevalue;?>" <?php if($EditUser->timezoneid == $zonevalue) echo ' selected = "selected" ';?>><?php echo $zonename;?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="box box-gray- box-solid- box-controls">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                    <div class="col-sm-8">
                                        <input type="submit" class="btn btn-success" value="Save" />
                                        <a href="<?php site_url("users")?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($isUpdateForm){ ?>
                        <input type="hidden" name="userid" value="<?php echo $EditUser->id;?>" />
                        <?php } ?>
                        <input type="hidden" name="act" value="save_user" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>