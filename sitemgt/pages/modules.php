<?php
$pageurl = "modules";
$pagetitle = "Modules";
$modulename = "modules";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("module")?>" role="button">New Module</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("modules")?>" role="button">Modules</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-modules")?>" role="button">Level Modules</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Module Name</span></th>
                            <th><span>Module Label</span></th>
                            <th><span class="text-center">Level(s)</span></th>
                            <th><span class="text-center">Enabled</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->modules;//." where ModuleDefault=0";
                        $modules = $db->get_results($sql);
                        foreach ($modules as $module){
                            $levels = $db->get_var("select count(*) from ".$db->level_modules." where LevelModuleEnabled=1 and ModuleID=".$module->id);
                        ?>
                        <tr>
                            <td><span><?php echo $module->ModuleName?></span></td>
                            <td><span><?php echo $module->ModuleLabel?></span></td>
                            <td><span class="text-center"><?php echo $levels;?></span></td>
                            <td><span class="text-center"><?php echo $module->ModuleEnabled == 0?"NO":"YES";?></span></td>
                            <td>
                                <a href="<?php site_url("level-modules/?moduleid=".$module->id);?>" class="btn btn-sm btn-links" data-toggle="qtiptooltip" title="Levels Settings"><i class="fa fa-cog"></i></a>
                                <?php if($module->ModuleDefault == 0) { ?>
                                <a href="<?php site_url("module/?moduleid=".$module->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("modules/?act=delete_module&moduleid=".$module->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>