<?php
$pageurl = "help-content";
$pagetitle = "Help Contents";
$modulename = "help_contents";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['help_contentid']) && is_numeric($_GET['help_contentid'])){
    $HelpContentID = intval($_GET["help_contentid"]);
    $EditHelpContent = $db->get_row("select * from ".$db->help_contents." where id=".$HelpContentID);
    if($EditHelpContent){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("help-content")?>" role="button">New Help Content</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("help-contents")?>" role="button">Help Contents</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Help Content</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("help-content");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="HelpContentTitle" class="col-sm-3 control-label">Title</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditHelpContent->HelpContentTitle;?>" class="form-control " id="HelpContentTitle" name="HelpContentTitle" placeholder="Title" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="HelpContentExcerpt" class="col-sm-3 control-label">Excerpt</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control " id="HelpContentExcerpt" name="HelpContentExcerpt" placeholder="Excerpt"><?php if($isUpdateForm) echo $EditHelpContent->HelpContentExcerpt;?></textarea>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="HelpContentText" class="col-sm-3 control-label">Text</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control " id="HelpContentText" name="HelpContentText" placeholder="Text" required="required"><?php if($isUpdateForm) echo $EditHelpContent->HelpContentText;?></textarea>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="HelpContentType" class="col-sm-3 control-label">Type</label>
                                            <div class="col-sm-9">
                                                <select name="HelpContentType" id="HelpContentType" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $HelpContentTypes = array("s3video" => "S3 Video", "youtubevideo" => "Youtube Video", "html" => "HTML Content");
                                                    foreach ($HelpContentTypes as $HelpContentTypeID => $HelpContentTypeValue){
                                                    ?>
                                                    <option value="<?php echo $HelpContentTypeID?>" <?php if($isUpdateForm && $HelpContentTypeID == $EditHelpContent->HelpContentType) echo ' selected="selected" '?>><?php echo $HelpContentTypeValue?></option>
                                                    <?php } ?>
                                                </select>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="HelpContentPosition" class="col-sm-3 control-label">Position</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditHelpContent->HelpContentPosition;?>" class="form-control " id="HelpContentPosition" name="HelpContentPosition" placeholder="Position" required="required" <?php echo INT_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="HelpContentEnabled" class="col-sm-3 control-label">Enable</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="HelpContentEnabled" name="HelpContentEnabled" <?php if($isUpdateForm && $EditHelpContent->HelpContentEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("help-contents")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="help_contentid" value="<?php echo $EditHelpContent->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_help_content" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>