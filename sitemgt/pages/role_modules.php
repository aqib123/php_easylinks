<?php
$pageurl = "role-modules";
$pagetitle = "Role Modules";
$modulename = "role-modules";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <!--<a class="btn btn-flat btn btn-bordered" href="<?php site_url("module")?>" role="button">New Module</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("modules")?>" role="button">Modules</a>-->
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("role-modules");?>">
                                <?php if(isset($_GET["roleid"]) && is_numeric($_GET["roleid"])) { ?>
                                <input type="hidden" name="roleid" value="<?php echo $_GET["roleid"];?>" />
                                <?php } ?>
                                <?php if(isset($_GET["moduleid"]) && is_numeric($_GET["moduleid"])) { ?>
                                <input type="hidden" name="moduleid" value="<?php echo $_GET["moduleid"];?>" />
                                <?php } ?>
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                                            <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-nobuttons="true">
                                                <thead>
                                                    <tr>
                                                        <th>Module Name</th>
                                                        <?php
                                                        $sql = "select * from ".$db->roles.((isset($_GET["roleid"]) && is_numeric($_GET["roleid"]))?" where id=".$_GET["roleid"]." ":"");

                                                        $roles = $db->get_results($sql);
                                                        foreach ($roles as $role){
                                                        ?>
                                                        <th><span class="text-center"><?php echo $role->RoleName?></span></th>
                                                        <?php
                                                        }   
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sql = "select * from ".$db->modules.((isset($_GET["moduleid"]) && is_numeric($_GET["moduleid"]))?" where id=".$_GET['moduleid']." ":"");
                                                    $modules = $db->get_results($sql);
                                                    foreach ($modules as $module){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <h5><?php echo $module->ModuleLabel;?></h5>
                                                        </td>
                                                        <?php
                                                        foreach ($roles as $role){
                                                            $RoleModuleEnabled = $db->get_var("select RoleModuleEnabled from ".$db->role_modules." where ModuleID=".$module->id." and RoleID=".$role->id);
                                                        ?>
                                                        <td class="text-center">
                                                            <input type="checkbox" name="<?php echo "role_".$role->id."_module_".$module->id;?>" value="1" <?php if($RoleModuleEnabled && $RoleModuleEnabled == 1) echo ' checked="checked" ';?> />
                                                        </td>
                                                        <?php
                                                        }   
                                                        ?>
                                                    </tr>
                                                    <?php	
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("role-modules")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_role_modules" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>