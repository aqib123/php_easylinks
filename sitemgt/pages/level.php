<?php
$pageurl = "level";
$pagetitle = "Levels";
$modulename = "levels";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['levelid']) && is_numeric($_GET['levelid'])){
    $LevelID = intval($_GET["levelid"]);
    $EditLevel = $db->get_row("select * from ".$db->levels." where LevelDefault=0 and id=".$LevelID);
    if($EditLevel){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("level")?>" role="button">New Level</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("levels")?>" role="button">Levels</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-modules")?>" role="button">Level Modules</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-menu-items")?>" role="button">Level Menu Items</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Level</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("level");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="LevelName" class="col-sm-3 control-label">Level Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditLevel->LevelName;?>" class="form-control " id="LevelName" name="LevelName" placeholder="Level Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="LevelLogo" class="col-sm-3 control-label">Level Logo</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditLevel->LevelLogo;?>" class="form-control " id="LevelLogo" name="LevelLogo" placeholder="Level Logo" required="required" <?php echo URL_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="LevelEnabled" class="col-sm-3 control-label">Level Enable</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="LevelEnabled" name="LevelEnabled" <?php if($isUpdateForm && $EditLevel->LevelEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("levels")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="levelid" value="<?php echo $EditLevel->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_level" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>