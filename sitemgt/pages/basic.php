<?php
include_once "../includes/functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Easy Links | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="<?php siteurl("css/AdminLTE.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php siteurl("css/skin-black-light.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php siteurl("css/blue.css");?>" />

    <link href="<?php siteurl("css/style.css");?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-black-light sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="../../index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Admin</b>LTE</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="">
                                <img src="<?php main_url("images/avatar5.png");?>" class="user-image" alt="User Image" />
                                <span class="hidden-xs">Alexander Pierce</span>
                            </a>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar linear_gradient">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php main_url("images/avatar5.png");?>" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>Alexander Pierce</p>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-link"></i><span>Link Bank</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-circle-o-notch"></i><span>Rotator</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-envelope-o"></i><span>Solo Ads</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-users"></i><span>Manage Groups</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-flag-checkered"></i><span>Lineup</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-calendar"></i><span>Calendars</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-area-chart"></i><span>Analytics</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-hourglass-start"></i><span>Split Testing</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-globe"></i><span>Link Monitoring</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa fa-cog"></i><span>System Settings</span>
                        </a>
                    </li>
                    <li>
                        <a href="../widgets.html">
                            <i class="fa ion-power"></i><span>Logout</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <!-- <li><a href="#">Examples</a></li> -->
                    <li class="active">Link Bank</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-2 col-xs-12">
                        <a class="btn btn-default btn-block" href="#" role="button">Create</a>
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <a class="btn btn-default btn-block" href="#" role="button">Existing</a>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Link Tracking Stats</h1>
                    </div>
                </div>
                <br />

                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-striped table-link-tracking-stats">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox" name="" id="" class="blue" />
                                </th>
                                <th><span>Link Name</span></th>
                                <th><span>Unique Clicks</span></th>
                                <th><span>Raw Clicks</span></th>
                                <th><span># of Sales</span></th>
                                <th><span>$ of Sales</span></th>
                                <th><span>Group</span></th>
                                <th><span>Vendor</span></th>
                                <th><span>Quick Sub Links</span></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.2.0
            </div>
            <strong>Copyright &copy; 2014-2015 <a href="http://chadnicely.com">Chad Nicely</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- iCheck -->
    <script src="<?php siteurl("js/icheck.min.js");?>" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="<?php siteurl("js/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="<?php siteurl("js/fastclick.min.js");?>" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php siteurl("js/app.min.js");?>" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php //siteurl("js/demo.js");?>" type="text/javascript"></script>

    <!-- site js -->
    <script src="<?php siteurl("js/easylinks.js");?>" type="text/javascript"></script>
</body>
</html>
