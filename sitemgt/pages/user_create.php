<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Page Title</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
   </head>
   <body>
      <div style="width: 500px">

      	<!-- <iframe src="https://sitemgt.easylinks.site/user_create" title="Add User"></iframe> -->
            <?php 
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
             	
             		?>
         <div class="alert alert-success success-msg d-none" role="alert">
            <strong>Well done!</strong> You successfully added
         </div>
         <div class="alert alert-danger error-msg d-none" role="alert">
         	<strong>Oh snap!</strong> Please Enter Data
         </div>
         <input type ="hidden" class="form-control " id="password" name="password" required="required" value="<?php echo $passwrod; ?>">
         <div>
            <div class="form-group">
               <label for="user_email" class="col-sm-4 control-label">Name</label>
               <div class="col-sm-8">
                  <input type="text" value="" class="form-control " id="user_login" name="user_login" placeholder="Username">
               </div>
            </div>
         </div>
         <div>
            <div class="form-group ">
               <label for="user_email" class="col-sm-4 control-label">Email Address</label>
               <div class="col-sm-8">
                  <input type="text" value="" class="form-control " id="user_email" name="user_email" placeholder="Email Address">
               </div>
            </div>
         </div>
         <div>
            <div class="box box-gray- box-solid- box-controls">
               <div class="box-body">
                  <div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label"></label>
                     <div class="col-sm-8">
                        <input  type="submit" class="btn btn-success" id="submit_fun" value="Save">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
      <script type="text/javascript">
         $(document).ready(function(){
           $("#submit_fun").click(function(){
         
           	var user_login = $('#user_login').val();
           	var password = '<?php echo substr(str_shuffle($chars),0,8) ?>';
           	var user_email = $('#user_email').val();
           	if(user_login != '' && password != '' && user_email != '')
           	{
	           	var data= {
	         					user_login : user_login,
	         					NewPassword : password,
	         					ConfirmPassword : password,
	         					user_email : user_email,
	         					LevelID : 3,
	         					user_nicename : user_login,
	         					display_name : user_login,
	         					act : 'save_user',
	         					return_back : 1
	         				}
	             $.ajax({
	             	type: "POST",
	         				data: {
	         					user_login : user_login,
	         					NewPassword : password,
	         					ConfirmPassword : password,
	         					user_email : user_email,
	         					LevelID : 3,
	         					user_nicename : user_login,
	         					display_name : user_login,
	         					act : 'save_user',
	         					return_back : 1,
	         					timezoneid: 'EST'
	         				},
	             	url: "https://sitemgt.easylinks.site/user", 
	             	success: function(result){
	             		$(".success-msg").removeClass("d-none");
	             		setTimeout(function() { $(".success-msg").addClass("d-none"); }, 5000);
	             		$('#user_login').val('');
	         		  	$('#password').val('');
	         		  	$('#user_email').val('');
	             	}});
           	}
           	else{
           		$(".error-msg").removeClass("d-none");
           		setTimeout(function() { $(".error-msg").addClass("d-none"); }, 5000);
           	}
           	});
         });
      </script>
   </body>
</html>