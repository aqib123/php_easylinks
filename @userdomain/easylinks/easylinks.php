<?php
include_once __DIR__."/config.php";
$CheckURL = $ServerURL."?checklink=true";
$jsonString = "";

if(function_exists('curl_version')){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $CheckURL);
    curl_setopt($ch, CURLOPT_HEADER, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $jsonString = curl_exec($ch);
    curl_close($ch);
}else if(function_exists("file_get_contents")){
    $jsonString = file_get_contents($CheckURL);
}else if(function_exists("stream_get_contents")){
    $jsonString = stream_get_contents(fopen($CheckURL, "rb"));
}

if(!empty($jsonString)){
    $jsonData = json_decode($jsonString);
    if($jsonData->LinkNameValid == "true"){
        $LinkTitle = $jsonData->LinkTitle;
        $DestinationURL = $ServerURL;//$jsonData->DestinationURL;
        if($jsonData->CloakLink == "true"){
            echo "<html><head><title>".$LinkTitle."</title></head>\n";
            echo "<frameset rows='100%'><frame src='".$DestinationURL."'/></frameset>\n";
            echo "</html>\n";
        }else{
            header("Location: ".$DestinationURL, true, 301);
        }
    }else{
        if(!isset($Disable404))
            include_once __DIR__."/404.php";
    }
}else{
    if(!isset($Disable404))
        include_once __DIR__."/404.php";
}
if(!isset($Disable404))
    die;