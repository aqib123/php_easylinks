<?php
$UserName = "<username>";
$DomainSlug = "<DomainSlug>";
$DomainURL = "<DomainURL>";
$DefaultCloak = "<DefaultCloak>";
$DefaultTitle = "<DefaultTitle>";
$Default404 = "<DomainURL>";

//$Protocol = isset($_SERVER['HTTPS'])?"https://":"http://";
$Protocol = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== "off"?"https://":"http://";
$CurrentURL = $Protocol.str_ireplace("www.", "", $_SERVER["HTTP_HOST"]).$_SERVER["REQUEST_URI"];
$CurrentURL = trim(strtolower($CurrentURL));
$CurrentURL = rtrim($CurrentURL, '/');
$CurrentURL = strpos($CurrentURL, "?") > 0?substr($CurrentURL, 0, strpos($CurrentURL, "?")):$CurrentURL;
$LinkName = ltrim(str_ireplace(rtrim(strtolower($DomainURL), "/"), "", rtrim($CurrentURL, "/")), "/");
$LinkName = trim($LinkName);
$LinkName = strpos($LinkName, "?") > 0?substr($LinkName, 0, strpos($LinkName, "?")):$LinkName;
$ServerURL = $Protocol.$UserName."-".$DomainSlug.".easylinks.online/".$LinkName;