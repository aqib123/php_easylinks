<?php
/*
Plugin Name: Easy Links
Plugin URI: 
Description: EasyLink WP Plugin.
Version: 1.0.1
Author: Easy Links Team
Author URI: http://easylinks.io/
*/

function easylink_redirect() {
    global $wp_query;

	if(is_404()){
        status_header( 200 );
        $wp_query->is_404=false;
        $Disable404 = true;
        include_once __DIR__."/easylinks.php";
        //die();
    }
}
//add_action('wp', 'easylink_redirect', 1);
add_filter('template_redirect', 'easylink_redirect' );