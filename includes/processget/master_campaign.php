<?php
if($get_act == "delete_master_campaign"){
    DB()->delete(DB()->master_campaigns, array("id" => parseInt($_GET['MasterCampaignID'])));
    $url = get_site_url("master-campaign/?success=true&msg=Master Campaign Deleted Successfully");
    redirect($url);
    die;
} else if($get_act == "master_campaign_details"){
    $MasterCampaignID = intval($_GET["MasterCampaignID"]);
    ob_start();
    $userid = $LoggedInUser->id;

    $sql = "select * from ".DB()->linkbanks." where MasterCampaignID=".$MasterCampaignID." and userid=".$userid;
    $linkbanks = DB()->get_results($sql);
    if(is_array($linkbanks) && count($linkbanks)){
?>
<h5 class="detail_table_heading">Link Bank Campaigns</h5>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0" data-default-sort-column="1">
    <thead>
        <tr>
            <th class="text-center"><span></span></th>
            <th><span>Link Name</span></th>
            <th class="text-center"><span></span></th>
            <th><span>Vendor</span></th>
            <th><span>Group</span></th>
            <th class="text-center"><span>Raw Clicks</span></th>
            <th class="text-center"><span>Unique Clicks</span></th>
            <th class="text-center"><span>Sales</span></th>
            <th class="text-center"><span>Pixels</span></th>
            <th class="text-center"><span>Status</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach($linkbanks as $linkbank){
            $LinkBankID = $linkbank->id;
            $VendorName = DB()->get_var("select VendorName from ".DB()->vendors." where id=".$linkbank->VendorID);
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$linkbank->GroupID);
            
            $UniqueClicks = DB()->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".DB()->linkbank_clicks." where LinkBankID=".$LinkBankID." group by ClickIp) as UniqueClicks") * 1;
            $RawClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$LinkBankID) * 1;
            //$RawClicks -= $UniqueClicks;

            $Sales = DB()->get_var("select sum(UnitPrice) from ".DB()->linkbank_conversions." where LinkBankID=".$LinkBankID." and ConversionType='sales'") * 1;
            $PixelFires = DB()->get_var("select count(FireIp) as FireCount from ".DB()->linkbank_pixel_fires." where LinkBankID=".$LinkBankID) * 1;

            $linkbankurl = get_linkbankurl($linkbank);
            $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$linkbank->id);//site_url("linkbank/stats/".$LinkBankID."/details");
            $StatusIcon = get_domain_status_icon($linkbank->DomainID);

            $LinkStatus = $linkbank->LinkStatus;
            if($LinkStatus == "active"){
                $linkbankstatus = "Live";
                $linkbankstatusicon = "fa fa-rocket";
            }else if($LinkStatus == "pending"){
                $linkbankstatus = "Pending";
                $linkbankstatusicon = "fa fa-hourglass-o";
            }else if($LinkStatus == "complete"){
                $linkbankstatus = "Completed";
                $linkbankstatusicon = "fa fa-check-square-o";
            }else if($LinkStatus == "evergreen"){
                $linkbankstatus = "Evergreen";
                $linkbankstatusicon = "fa fa-tree";
            }else if($LinkStatus == "mylink"){
                $linkbankstatus = "My Links";
                $linkbankstatusicon = "fa fa-user";
            }
        ?>
        <tr>
            <td class="text-center"><?php echo $StatusIcon;?></td>
            <td>
                <a href="<?php echo $namelinkedurl;?>" class="pull-left qtip2" data-qtip-image="<?php echo PREVIEW_URL.$linkbankurl;?>"><?php echo $linkbank->LinkName?></a>
            </td>
            <td class="text-center grey-scale">
                <a href="<?php site_url("linkbank/stats/".$linkbank->id."/details");?>" class="btn btn-sm btn-links" title="Stats"><i class="fa fa-globe"></i></a>
            </td>
            <td><span><?php echo $VendorName;?></span></td>
            <td><span><?php echo $GroupName;?></span></td>
            <td class="text-center"><span><?php echo $RawClicks;?></span></td>
            <td class="text-center"><span><?php echo $UniqueClicks;?></span></td>
            <td class="text-center"><span><?php echo $Sales;?></span></td>
            <td class="text-center"><span><?php echo $PixelFires;?></span></td>
            <td class="text-center"><a href="javascript:" title="<?php echo $linkbankstatus?>" class="status_icon grey-scale"><i class="fa <?php echo $linkbankstatusicon;?>"></i></a></td>
        </tr>
        <?php        
        }
        ?>
    </tbody>
</table>
</div>
<?php
    }

    $sql = "select * from ".DB()->rotators." where MasterCampaignID=".$MasterCampaignID." and userid=".$userid;
    $rotators = DB()->get_results($sql);
    if(is_array($rotators) && count($rotators)){
?>
<h5 class="detail_table_heading">Rotator Campaigns</h5>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0,1,2" data-default-sort-column="3">
    <thead>
        <tr>
            <th class="text-center"><span></span></th>
            <th class="text-center"><span></span></th>
            <th class="text-center"><span></span></th>
            <th><span>Rotator Name</span></th>
            <th class="text-center"><span></span></th>
            <th><span>Group</span></th>
            <th><span>Type</span></th>
            <th class="text-center"><span>#Rotator Links</span></th>
            <th class="text-center"><span>Rotator Links Visited</span></th>
            <th class="text-center"><span>Status</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach($rotators as $rotator){
            $RotatorID = $rotator->id;
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
            
            $RotatorRotatorLinks = DB()->get_var("SELECT count(*) from ".DB()->rotator_links." where RotatorID=".$RotatorID." ") * 1;
            $RawRotatorVisits = DB()->get_var("select SUM(RotatorVisitCount) as TotalRotatorVisits from (select count(*) as RotatorVisitCount from ".DB()->rotator_clicks." where RotatorLinkID in (SELECT id from ".DB()->rotator_links." where RotatorID=".$RotatorID." and userid=".$LoggedInUser->id.")) as RotatorVisits") * 1;

            $namelinkedurl = get_site_url("rotator/?RotatorID=".$rotator->id);//get_site_url("rotator/?RotatorID=".$RotatorID);
            $rotator_linkurl = get_rotatorurl($rotator);
            $StatusIcon = get_domain_status_icon($rotator->DomainID);

            $RotatorStatus = $rotator->RotatorStatus;
            if($RotatorStatus == "active"){
                $rotatorstatus = "Live";
                $rotatorstatusicon = "fa fa-rocket";
            }else if($RotatorStatus == "pending"){
                $rotatorstatus = "Pending";
                $rotatorstatusicon = "fa fa-hourglass-o";
            }else if($RotatorStatus == "complete"){
                $rotatorstatus = "Completed";
                $rotatorstatusicon = "fa fa-check-square-o";
            }
        ?>
        <tr>
            <td class="text-center"><?php echo $StatusIcon;?></td>
            <td class="text-center">
                <a href="javascript:" data-url="<?php site_url("master-campaign?act=rotator_details&RotatorID=".$RotatorID);?>" data-toggle="DetailRow" id="" class="btn btn-rotator-detail"><i class="fa fa-caret-square-o-down"></i></a>
            </td>
            <td class="text-center"><a href="<?php site_url("rotator/links/".$RotatorID."/")?>"><i class="fa ion-network"></i></a></td>
            <td>
                <a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $rotator->RotatorName?></a>
            </td>
            <td class="text-center grey-scale">
                <a href="<?php site_url("rotator/stats/".$rotator->id."/details");?>" class="btn btn-sm btn-links" title="Stats"><i class="fa fa-globe"></i></a>
            </td>
            <td><span><?php echo $GroupName;?></span></td>
            <td><span><?php echo $rotator->RotatorType;?></span></td>
            <td class="text-center"><span><?php echo $RotatorRotatorLinks;?></span></td>
            <td class="text-center"><span><?php echo $RawRotatorVisits;?></span></td>
            <td class="text-center"><a href="javascript:" title="<?php echo $rotatorstatus?>" class="status_icon grey-scale"><i class="fa <?php echo $rotatorstatusicon;?>"></i></a></td>
        </tr>
        <?php        
        }
        ?>
    </tbody>
</table>
</div>
<?php
    }

    $sql = "select * from ".DB()->link_sequences." where MasterCampaignID=".$MasterCampaignID." and userid=".$userid;
    $link_sequences = DB()->get_results($sql);
    if(is_array($link_sequences) && count($link_sequences)){
?>
<h5 class="detail_table_heading">Link Sequence Campaigns</h5>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0,1,2" data-default-sort-column="3">
    <thead>
        <tr>
            <th class="text-center"><span></span></th>
            <th class="text-center"><span></span></th>
            <th class="text-center"><span></span></th>
            <th><span>Name</span></th>
            <th class="text-center"><span></span></th>
            <th><span>Group</span></th>
            <th class="text-center"><span>#LinkSequence Links</span></th>
            <th class="text-center"><span>LinkSequence Links Visited</span></th>
            <th class="text-center"><span>Status</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach($link_sequences as $link_sequence){
            $LinkSequenceID = $link_sequence->id;
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
            
            $LinkSequenceLinkSequenceLinks = DB()->get_var("SELECT count(*) from ".DB()->link_sequence_links." where LinkSequenceID=".$LinkSequenceID." ") * 1;
            $RawLinkSequenceVisits = DB()->get_var("select SUM(LinkSequenceVisitCount) as TotalLinkSequenceVisits from (select count(*) as LinkSequenceVisitCount from ".DB()->link_sequence_clicks." where LinkSequenceLinkID in (SELECT id from ".DB()->link_sequence_links." where LinkSequenceID=".$LinkSequenceID." and userid=".$LoggedInUser->id.")) as LinkSequenceVisits") * 1;

            $namelinkedurl = get_site_url("link_sequence/?LinkSequenceID=".$link_sequence->id);//get_site_url("link_sequence/?LinkSequenceID=".$LinkSequenceID);
            $link_sequence_linkurl = get_link_sequenceurl($link_sequence);
            $StatusIcon = get_domain_status_icon($link_sequence->DomainID);

            $LinkSequenceStatus = $link_sequence->LinkSequenceStatus;
            if($LinkSequenceStatus == "active"){
                $link_sequencestatus = "Live";
                $link_sequencestatusicon = "fa fa-rocket";
            }else if($LinkSequenceStatus == "pending"){
                $link_sequencestatus = "Pending";
                $link_sequencestatusicon = "fa fa-hourglass-o";
            }else if($LinkSequenceStatus == "complete"){
                $link_sequencestatus = "Completed";
                $link_sequencestatusicon = "fa fa-check-square-o";
            }
        ?>
        <tr>
            <td class="text-center"><?php echo $StatusIcon;?></td>
            <td class="text-center">
                <a href="javascript:" data-url="<?php site_url("master-campaign?act=link_sequence_details&LinkSequenceID=".$LinkSequenceID);?>" data-toggle="DetailRow" id="" class="btn btn-link_sequence-detail"><i class="fa fa-caret-square-o-down"></i></a>
            </td>
            <td class="text-center"><a href="<?php site_url("link_sequence/links/".$LinkSequenceID."/")?>"><i class="fa ion-network"></i></a></td>
            <td>
                <a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $link_sequence->LinkSequenceName?></a>
            </td>
            <td class="text-center grey-scale">
                <a href="<?php site_url("link_sequence/stats/".$link_sequence->id."/details");?>" class="btn btn-sm btn-links" title="Stats"><i class="fa fa-globe"></i></a>
            </td>
            <td><span><?php echo $GroupName;?></span></td>
            <td class="text-center"><span><?php echo $LinkSequenceLinkSequenceLinks;?></span></td>
            <td class="text-center"><span><?php echo $RawLinkSequenceVisits;?></span></td>
            <td class="text-center"><a href="javascript:" title="<?php echo $link_sequencestatus?>" class="status_icon grey-scale"><i class="fa <?php echo $link_sequencestatusicon;?>"></i></a></td>
        </tr>
        <?php        
        }
        ?>
    </tbody>
</table>
</div>
<?php
    }

    $sql = "select * from ".DB()->campaigns." where MasterCampaignID=".$MasterCampaignID." and userid=".$userid;
    $campaigns = DB()->get_results($sql);
    if(is_array($campaigns) && count($campaigns)){
?>
<h5 class="detail_table_heading">Email Campaigns</h5>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0,1" data-default-sort-column="2">
    <thead>
        <tr>
            <th class="text-center"><span></span></th>
            <th class="text-center"><span></span></th>
            <th><span>Campaign Name</span></th>
            <th><span>Group</span></th>
            <th class="text-center"><span>#Emails</span></th>
            <th class="text-center"><span>Email Clicks</span></th>
            <th class="text-center"><span>CTA</span></th>
            <th class="text-center"><span>Fires</span></th>
            <th class="text-center"><span>Status</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach($campaigns as $campaign){
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$campaign->GroupID);
            $Emails = DB()->get_var("SELECT count(*) from ".DB()->campaign_emails." where CampaignID=".$campaign->id." ") * 1;
            $RawClicks = DB()->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".DB()->linkbank_clicks." where ClickType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")) as Clicks") * 1;
            $CTAClicks = get_campaign_cta_clicks($campaign->id);
            $PixelFires = DB()->get_var("select count(FireIp) as FireCount from ".DB()->linkbank_pixel_fires." where FireType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")") * 1;
            $namelinkedurl = get_site_url("campaign/?CampaignID=".$campaign->id);

            $CampaignStatus = $campaign->CampaignStatus;
            if($CampaignStatus == "active"){
                $campaignstatus = "Live";
                $campaignstatusicon = "fa fa-rocket";
            }else if($CampaignStatus == "pending"){
                $campaignstatus = "Pending";
                $campaignstatusicon = "fa fa-hourglass-o";
            }else if($CampaignStatus == "complete"){
                $campaignstatus = "Completed";
                $campaignstatusicon = "fa fa-check-square-o";
            }
        ?>
        <tr>
            <td class="text-center"><a href="javascript:" data-url="<?php site_url("master-campaign?act=campaign_details&CampaignID=".$campaign->id);?>" data-toggle="DetailRow" id="" class="btn btn-campaign-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
            <td class="text-center"><a href="<?php site_url("campaign/emails/".$campaign->id."/")?>" class=""><i class="fa ion-network"></i></a></td>
            <td><a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $campaign->CampaignName?></a></td>
            <td><span><?php echo $GroupName;?></span></td>
            <td class="text-center"><span><?php echo $Emails;?></span></td>
            <td class="text-center"><span><?php echo $RawClicks;?></span></td>
            <td class="text-center"><span><?php echo $CTAClicks;?></span></td>
            <td class="text-center"><span><?php echo $PixelFires;?></span></td>
            <td class="text-center"><a href="javascript:" title="<?php echo $campaignstatus?>" class="status_icon grey-scale"><i class="fa <?php echo $campaignstatusicon;?>"></i></a></td>
        </tr>
        <?php        
        }
        ?>
    </tbody>
</table>
</div>
<?php
    }

    $sql = "select * from ".DB()->paidtraffics." where MasterCampaignID=".$MasterCampaignID." and userid=".$userid;
    $paidtraffics = DB()->get_results($sql);
    if(is_array($paidtraffics) && count($paidtraffics)){
?>
<h5 class="detail_table_heading">Paid Traffic Campaigns</h5>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0" data-default-sort-column="1">
    <thead>
        <tr>
            <th class="text-center"><span></span></th>
            <th><span>Paid Traffic Name</span></th>
            <th class="text-center"><span></span></th>
            <th><span>Vendor</span></th>
            <th><span>Group</span></th>
            <th class="text-center"><span>Raw Clicks</span></th>
            <th class="text-center"><span>Unique Clicks</span></th>
            <th class="text-center"><span># of Actions</span></th>
            <th class="text-center"><span>% of Optins</span></th>
            <th class="text-center"><span>Pixels</span></th>
            <th class="text-center"><span>Status</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach($paidtraffics as $paidtraffic){
            $PaidTrafficID = $paidtraffic->id;
            $VendorName = DB()->get_var("select VendorName from ".DB()->vendors." where id=".$paidtraffic->VendorID);
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
            
            $UniqueClicks = DB()->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." group by ClickIp) as UniqueClicks") * 1;
            $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
            //$RawClicks -= $UniqueClicks;

            $TotalActions = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='action'") * 1;
            $PercentOptins = ($TotalActions * $RawClicks)/100;
            $PixelFires = DB()->get_var("select count(FireIp) as FireCount from ".DB()->paidtraffic_pixel_fires." where PaidTrafficID=".$paidtraffic->id) * 1;

            $paidtrafficurl = get_paidtrafficurl($paidtraffic);
            $namelinkedurl = get_site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id);
            $StatusIcon = get_domain_status_icon($paidtraffic->DomainID);

            $PaidTrafficStatus = $paid_traffic->PaidTrafficStatus;
            if($PaidTrafficStatus == "active"){
                $paid_trafficstatus = "Live";
                $paid_trafficstatusicon = "fa fa-rocket";
            }else if($PaidTrafficStatus == "pending"){
                $paid_trafficstatus = "Pending";
                $paid_trafficstatusicon = "fa fa-hourglass-o";
            }else if($PaidTrafficStatus == "complete"){
                $paid_trafficstatus = "Completed";
                $paid_trafficstatusicon = "fa fa-check-square-o";
            }
        ?>
        <tr>
            <td class="text-center"><?php echo $StatusIcon;?></td>
            <td>
                <a href="<?php echo $namelinkedurl; ?>" class="pull-left qtip2" data-qtip-image="<?php echo PREVIEW_URL.$paidtrafficurl;?>"><?php echo $paidtraffic->PaidTrafficName?></a>
            </td>
            <td class="text-center grey-scale">
                <a href="<?php site_url("paidtraffic/stats/".$paidtraffic->id."/details");?>" class="btn btn-sm btn-links" title="Stats"><i class="fa fa-globe"></i></a>
            </td>
            <td><span><?php echo $VendorName;?></span></td>
            <td><span><?php echo $GroupName;?></span></td>
            <td class="text-center"><span><?php echo $RawClicks;?></span></td>
            <td class="text-center"><span><?php echo $UniqueClicks;?></span></td>
            <td class="text-center"><span><?php echo $TotalActions;?></span></td>
            <td class="text-center"><span><?php echo $PercentOptins;?></span></td>
            <td class="text-center"><span><?php echo $PixelFires;?></span></td>
            <td class="text-center"><a href="javascript:" title="<?php echo $paid_trafficstatus?>" class="status_icon grey-scale"><i class="fa <?php echo $paid_trafficstatusicon;?>"></i></a></td>
        </tr>
        <?php        
        }
        ?>
    </tbody>
</table>
</div>
<?php
    }

    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
}