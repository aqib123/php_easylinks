<?php
if($get_act == "delete_link"){
    $ConverPixels = DB()->get_var("select count(*) from ".DB()->linkbank_conversion_pixels." where LinkBankID=".parseInt($_GET['LinkBankID']));
    delete_linkbank_status(parseInt($_GET['LinkBankID']));
    DB()->delete(DB()->linkbanks, array("id" => parseInt($_GET['LinkBankID'])));
    DB()->delete(DB()->linkbank_saleinfo, array("LinkBankID" => $LinkBankID));
    $url = get_site_url("linkbank/stats/".($ConverPixels>0?"conv/":"")."?success=true&msg=Link Deleted Successfully");
    redirect($url);
    die;
}else if($get_act == "reset_link"){
    $ConverPixels = DB()->get_var("select count(*) from ".DB()->linkbank_conversion_pixels." where LinkBankID=".parseInt($_GET['LinkBankID']));
    delete_linkbank_status(parseInt($_GET['LinkBankID']));
    $url = get_site_url("linkbank/stats/".($ConverPixels>0?"conv/":"")."?success=true&msg=Link Stats Reset Successfully");
    redirect($url);
    die;
}else if($get_act == "clone_link"){
    $ConverPixels = DB()->get_var("select count(*) from ".DB()->linkbank_conversion_pixels." where LinkBankID=".parseInt($_GET['LinkBankID']));
    $url = "linkbank/stats/".($ConverPixels>0?"conv/":"");
    $LinkBankID = parseInt($_GET['LinkBankID']);
    $linkbank = DB()->get_row("select * from ".DB()->linkbanks." where id=".$LinkBankID, ARRAY_A);
    if ($linkbank){
        unset($linkbank["id"]);
        $linkbank["userid"] = $LoggedInUser->id;
        $linkbank["DateAdded"] = strtotime("now");
        $linkbank["LinkStatus"] = "pending";
        if(DB()->insert(DB()->linkbanks, $linkbank) && DB()->insert_id > 0){
            $id = DB()->insert_id;
            $data = array(
                "LinkName"      => $linkbank["LinkName"]."-clone-".$id,
                "VisibleLink"   => $linkbank["VisibleLink"]."-clone-".$id,
            );
            if(DB()->update(DB()->linkbanks, $data, array("id" => $id)))
                $url = "linkbank/?LinkBankID=".$id."&success=true&msg=Link Cloned Successfully";
        }
    }

    //$url = get_site_url("linkbank/stats/?success=true&msg=Link Deleted Successfully");
    site_redirect($url);
    die;
} else if($get_act == 'exportlinkdetails' && isset($_GET["LinkBankID"]) && elUsers()->is_user_loggedin()){
    $filename = "linkdetail_".$_GET["LinkBankID"]."_.csv";

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename);
    $output = fopen('php://output', 'w');
    fputcsv($output, array('Access Time', 'IP', 'Tier', 'Country', 'Browser', 'Platform', 'Type'));

    $sql = "select * from ".DB()->linkbanks." where id=".$_GET["LinkBankID"]." and userid=".$LoggedInUser->id;
    $linkbank = DB()->get_row($sql);
    if($linkbank){
        $sql = "select * from ".DB()->linkbank_clicks." where LinkBankID=".$linkbank->id;

        if(isset($_GET['startdate']) && isset($_GET['enddate']))
            $sql .= " and (DateAdded >= '".strtotime($_GET['startdate'])."' and DateAdded <= '".strtotime($_GET['enddate'])."') ";

        $linkbank_clicks = DB()->get_results($sql);
        foreach($linkbank_clicks as $linkbank_click){
            $countrycode = $linkbank_click->CountryCode;
            if($countrycode == "") {
                $countryname = "Unknown";
                $countrytier = 3;
            }else{
                $country = DB()->get_row("select * from ".DB()->countries." where countrycode='".$countrycode."'");
                $countryname = $country->countryname;
                $countrytier = $country->countrytier;
            }

            if($countrycode == "")
                $browsername = "Unknown";
            else
                $browsername = $linkbank_click->BrowserName . " " . $linkbank_click->BrowserVersion;

            $platform = $linkbank_click->Platform;

            $linkbank_clicktype = "non-unique";
            if($linkbank_click->BotName != ""){
                $linkbank_clicktype = "botc";
            }else{
                $linkbank_clickscount = (DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where ClickIp='".$linkbank_click->ClickIp."' and LinkBankID=".$linkbank->id)) * 1;
                if($linkbank_clickscount == 1)
                    $linkbank_clicktype = "unique";
            }

            fputcsv($output, array(
                date("m/d/Y h:i:s A", $linkbank_click->DateAdded),
                $linkbank_click->ClickIp,
                $countrytier,
                $countryname,
                $browsername,
                $platform,
                $linkbank_clicktype
            ));
        }
    }
    die;
} else if($get_act == "changelinkstatus"){
    $status = $_GET["status"];
    $LinkBankID = intval($_GET["LinkBankID"]);

    $data = array("LinkStatus" => $status);
    if($status == "pending"){
        $data["StartDate"] = "";
        $data["EndDate"] = "";
    } else if($status == "active") {
        $data["StartDate"] = strtotime("now");
        $data["EndDate"] = "";
    } else if($status == "complete") {
        $data["EndDate"] = strtotime("now");
    }

    DB()->update(DB()->linkbanks, $data, array("id" => $LinkBankID));
    $url = "linkbank/stats/".(!empty($_GET['linkbank_type'])?$_GET['linkbank_type']."/":"")."?success=true&msg=Link Updated Successfully&".$status."=true&LinkBankID=".$LinkBankID;
    //echo $url;
    site_redirect($url);
    die;
} else if($get_act == "check_link_name" && isset($_GET["type"]) && $_GET["type"] == "linkbank"){
    $linkbank = DB()->get_row("select * from ".DB()->linkbanks." where userid=".$LoggedInUser->id." and LinkName='".$_GET["LinkName"]."' and id <> ".intval($_GET["id"]));

    if(!$linkbank)
        http_response_code(200);
    else
        http_response_code(418);
    die;
}else if($get_act == "linkbank_details"){
    $LinkBankID = intval($_GET["LinkBankID"]);
    ob_start();
?>
<div class="table-responsive data-table-container-">
    <table class="table table-condensed table-bordered table-striped table-linkbank-details data-table responsive- nowrap" data-nobuttons-="true">
    <thead>
        <tr>
            <th class="text-center"><span>Seq#</span></th>
            <th class="text-center"><span>Type</span></th>
            <th class="text-center"><span>Upsell/Downsell Name</span></th>
            <th class="text-center"><span>URL</span></th>
            <th class="text-center"><span>Conv #</span></th>
            <th class="text-center"><span>Conv $</span></th>
            <th class="text-center"><span>% Sales</span></th>
        </tr>
    </thead>

    <tbody>
    <?php
    $conv_total = DB()->get_var("select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".$LinkBankID." and ConversionType='sales'") * 1;

    $sql = "select * from ".DB()->linkbank_conversion_pixels." where userid = ".$LoggedInUser->id." and LinkBankID=".$LinkBankID." order by id";
    $linkbank_conversion_pixels = DB()->get_results($sql);
    $seqno = 0;
    foreach($linkbank_conversion_pixels as $linkbank_conversion_pixel){
        $ConversionPixelURL = $linkbank_conversion_pixel->ConversionPixelURL;

        $conv_no = DB()->get_var("select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".$LinkBankID." and LinkBankConversionPixelID=".$linkbank_conversion_pixel->id." and ConversionType='sales'") * 1;
        $conv_amount = DB()->get_var("select sum(UnitPrice) from ".DB()->linkbank_conversions." where LinkBankID=".$LinkBankID." and LinkBankConversionPixelID=".$linkbank_conversion_pixel->id." and ConversionType='sales'") * 1;
        $conv_percent = $conv_total == 0?0:($conv_no / $conv_total * 100);
    ?>
        <tr>
            <td class="text-center"><span><?php echo (++$seqno);?></span></td>
            <td class="text-center"><span><?php echo $linkbank_conversion_pixel->ConversionPixelType;?></span></td>
            <td><a href="<?php echo $ConversionPixelURL?>" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$ConversionPixelURL;?>"><?php echo $linkbank_conversion_pixel->ConversionPixelName;?></a></td>
            <td>
                <a href="javascript: void(0);" data-clipboard-text="<?php echo $ConversionPixelURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $ConversionPixelURL;?>"><i class="fa fa-clipboard"></i></a>
                <a href="<?php echo $ConversionPixelURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $ConversionPixelURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
            </td>
            <td class="text-center"><span><?php echo get_formated_number($conv_no);?></span></td>
            <td class="text-center"><span>$<?php echo get_formated_number(round($conv_amount, 2));?></span></td>
            <td class="text-center"><span><?php echo get_formated_number(round($conv_percent, 2));?>%</span></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
}