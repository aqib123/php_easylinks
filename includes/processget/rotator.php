<?php
if($get_act == "add_rotatorlink"){
    $RotatorID = intval($_GET["RotatorID"]);
    $index = intval($_GET["index"]);
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");

    $data = array(
        "userid"                    => $LoggedInUser->id,
        "RotatorID"                 => $RotatorID,
        "RotatorLinkPosition"       => $index,
        "RotatorLinkType"           => "customurl",
        "DateAdded"                 => strtotime("now"),
    );
    
    DB()->insert(DB()->rotator_links, $data);
    if(is_numeric(DB()->insert_id)){
        $jsonData["value"] = get_rotatorlink_box(DB()->insert_id, $index);
    }
    echo json_encode($jsonData);
    die;
}else if($get_act == "update_rotatorlink_postion"){
    $RotatorID = intval($_GET["RotatorID"]);
    $RotatorLinkIDs = $_GET["RotatorLinkIDs"];
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    
    for ($index = 0; $index < count($RotatorLinkIDs); $index++)
    {
        $RotatorLinkID = $RotatorLinkIDs[$index];
        $RotatorLinkPosition = $index + 1;
        DB()->update(DB()->rotator_links, array("RotatorLinkPosition" => $RotatorLinkPosition), array("id" => $RotatorLinkID, "userid" => $LoggedInUser->id));
    }
    echo json_encode($jsonData);
    die;
}else if($get_act == "change_rotatorlink_status"){
    $RotatorLinkID = intval($_GET["rotatorlinkid"]);
    $RotatorLinkLive = DB()->get_var("select RotatorLinkLive from ".DB()->rotator_links." where userid=".$LoggedInUser->id." and id=".$RotatorLinkID) * 1;
    $RotatorLinkLive = $RotatorLinkLive == 1?0:1;
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(DB()->update(DB()->rotator_links, array("RotatorLinkLive" => $RotatorLinkLive), array("id" => $RotatorLinkID, "userid" => $LoggedInUser->id)))
        $jsonData["value"] = "success";
    echo json_encode($jsonData);
    die;
}else if($get_act == "changerotatorstatus"){
    $status = $_GET["status"];
    $RotatorID = intval($_GET["RotatorID"]);
    
    $data = array("RotatorStatus" => $status);
    DB()->update(DB()->rotators, $data, array("id" => $RotatorID));
    $url = "rotator/stats/".(!empty($_GET['rotator_type'])?$_GET['rotator_type']."/":"")."?success=true&msg=Rotator Updated Successfully&".$status."=true&RotatorID=".$RotatorID;
    site_redirect($url);
    die;
}else if($get_act == "rotator_details"){
    $RotatorID = intval($_GET["RotatorID"]);
    ob_start();
?>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-rotator-details data-table responsive- nowrap" data-nobuttons-="true">
    <thead>
        <tr>
            <th class="text-center"><span>Seq#</span></th>
            <th><span>URl</span></th>
            <th class="text-center"><span>Max Clicks</span></th>
            <th class="text-center"><span>Start Date</span></th>
            <th class="text-center"><span>End Date</span></th>
            <th class="text-center"><span>Days Left</span></th>
            <th class="text-center"><span>Status</span></th>
            <th class="text-center"><span>Visits</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
    $sql = "select * from ".DB()->rotator_links." where userid = ".$LoggedInUser->id." and RotatorID=".$RotatorID." order by id";
    $rotator_links = DB()->get_results($sql);
    $seqno = 0;
    foreach($rotator_links as $rotator_link){
        if ($rotator_link->RotatorLinkType == "customurl")
            $RotatorLinkURL = $rotator_link->RotatorLinkURL;
        else
            $RotatorLinkURL = get_linkbankurl($rotator_link->LinkBankID);

        $days = "Evergreen";
        if ($rotator_link->StartDate != "" && $rotator_link->EndDate != ""){
            $dtStartDate = new DateTime(date("r", $rotator_link->StartDate));
            $dtEndDate = new DateTime(date("r", $rotator_link->EndDate));
            $days = $dtStartDate->diff($dtEndDate)->format("%a days");
        }

        $SubLinkVisits = DB()->get_var("select count(*) as VisitCount from ".DB()->rotator_clicks." where RotatorLinkID=".$rotator_link->id) * 1;
        ?>

        <tr>
            <td class="text-center"><span><?php echo (++$seqno);?></span></td>
            <td><a href="<?php echo $RotatorLinkURL?>" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$RotatorLinkURL;?>"><?php echo $RotatorLinkURL;?></a></td>
            <td class="text-center"><span><?php echo $rotator_link->MaxClicks == 0?"Unlimited":$rotator_link->MaxClicks;?></span></td>
            <td class="text-center"><span><?php echo empty($rotator_link->StartDate)?"N/A":date("m/d/Y h:i A", $rotator_link->StartDate);?></span></td>
            <td class="text-center"><span><?php echo empty($rotator_link->EndDate)?"N/A":date("m/d/Y h:i A", $rotator_link->EndDate);?></span></td>
            <td class="text-center"><span><?php echo $days;?></span></td>
            <td class="text-center"><span><?php echo $rotator_link->RotatorLinkLive == 1?"Active":"Paused"; ?></span></td>
            <td class="text-center"><span><?php echo $SubLinkVisits;?></span></td>
        </tr>
        <?php        
    }
        ?>
    </tbody>
</table>
</div>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
} else if($get_act == "delete_rotator"){
    delete_rotator_stats(parseInt($_GET['RotatorID']));
    DB()->delete(DB()->rotator_links, array("RotatorID" => parseInt($_GET['RotatorID'])));
    DB()->delete(DB()->rotators, array("id" => parseInt($_GET['RotatorID'])));
    $url = get_site_url("rotator/stats/?success=true&msg=Rotator Deleted Successfully");
    redirect($url);
    die;
}else if($get_act == "reset_rotator"){
    delete_rotator_stats(parseInt($_GET['RotatorID']));
    $url = get_site_url("rotator/stats/?success=true&msg=Rotator Stats Reset Successfully");
    redirect($url);
    die;
}else if($get_act == "clone_rotator"){
    $url = "rotator/stats/";
    $RotatorID = parseInt($_GET['RotatorID']);
    $rotator = DB()->get_row("select * from ".DB()->rotators." where id=".$RotatorID, ARRAY_A);
    if ($rotator){
        //echo "<pre>".print_r($rotator, true)."</pre>";
        unset($rotator["id"]);
        $rotator["userid"] = $LoggedInUser->id;
        $rotator["DateAdded"] = strtotime("now");
        $rotator["RotatorStatus"] = "pending";
        if(DB()->insert(DB()->rotators, $rotator) && DB()->insert_id > 0){
            $id = DB()->insert_id;
            $data = array(
                "RotatorName"   => $rotator["RotatorName"]."-clone-".$id,
                "VisibleLink"   => $rotator["VisibleLink"]."-clone-".$id,
            );
            if(DB()->update(DB()->rotators, $data, array("id" => $id))){
                $url = "rotator/?RotatorID=".$id."&success=true&msg=Rotator Cloned Successfully";
                
                $sql = "select * from ".DB()->rotator_links." where userid=".$LoggedInUser->id." and RotatorID=".$RotatorID;
                $rotator_links = DB()->get_results($sql, ARRAY_A);
                foreach($rotator_links as $rotator_link){
                    unset($rotator_link["id"]);
                    $rotator_link["userid"] = $LoggedInUser->id;
                    $rotator_link["DateAdded"] = strtotime("now");
                    $rotator_link["RotatorID"] = $id;
                    DB()->insert(DB()->rotator_links, $rotator_link);
                }
            }
        }
    }
    site_redirect($url);
    die;
} else if($get_act == "check_link_name" && isset($_GET["type"]) && $_GET["type"] == "rotator"){
    $rotator = DB()->get_row("select * from ".DB()->rotators." where userid=".$LoggedInUser->id." and RotatorName='".$_GET["RotatorName"]."' and id <> ".intval($_GET["id"]));
    
    if(!$rotator)
        http_response_code(200);
    else
        http_response_code(418);
    die;
}