<?php
if($get_act == "delete_manualsale"){
    if(isset($_GET["ManualSaleID"]) && is_numeric($_GET["ManualSaleID"])){
        $pageurl = "manual-sale/stats/";
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where userid=".$LoggedInUser->id." and id = ".$_GET["ManualSaleID"]);//MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and 
        if($ManualSale){
            DB()->query("delete from ".DB()->manualsale_incomes." where ManualSaleSplitPartnerID in (select id from ".DB()->manualsale_split_partners." where userid = ".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id.")");
            DB()->query("delete from ".DB()->manualsale_other_incomes." where ManualSaleSplitPartnerID in (select id from ".DB()->manualsale_split_partners." where userid = ".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id.")");
            DB()->query("delete from ".DB()->manualsale_expenses." where ManualSaleSplitPartnerID in (select id from ".DB()->manualsale_split_partners." where userid = ".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id.")");
            DB()->delete(DB()->manualsale_split_partners, array("userid" => $LoggedInUser->id, "ManualSaleID" => $ManualSale->id));
            DB()->delete(DB()->manualsales, array("userid" => $LoggedInUser->id, "id" => $ManualSale->id));
            $pageurl .= "?success=true&msg=Sale Deleted Successfully";
        }
        
        site_redirect($pageurl);
        die;
    }
}