<?php
if($get_act == "mark_page_default"){
    $RedirectLinkType = $_GET["RedirectLinkType"];
    $RedirectLinkID = $_GET["RedirectLinkID"];
    
    DB()->update(DB()->redirect_links, 
        array(
            "RedirectLinkIsDefault" => 0
        ), 
        array(
            "RedirectLinkType" => $RedirectLinkType, 
            "userid" => $LoggedInUser->id
        )
    );

    DB()->update(DB()->redirect_links, 
        array(
            "RedirectLinkIsDefault" => 1
        ), 
        array(
            "RedirectLinkType" => $RedirectLinkType,
            "id" => $RedirectLinkID,
            "userid" => $LoggedInUser->id
        )
    );
    echo json_encode(array("completed" => "successfully"));
    die;
}else if($get_act == "mark_page_default_404"){
    $RedirectLinkType = $_GET["RedirectLinkType"];
    $RedirectLinkID = $_GET["RedirectLinkID"];
    
    DB()->update(DB()->redirect_links, 
        array(
            "RedirectLinkIsDefault404" => 0
        ), 
        array(
            "RedirectLinkType" => $RedirectLinkType, 
            "userid" => $LoggedInUser->id
        )
    );

    DB()->update(DB()->redirect_links, 
        array(
            "RedirectLinkIsDefault404" => 1
        ), 
        array(
            "RedirectLinkType" => $RedirectLinkType,
            "id" => $RedirectLinkID,
            "userid" => $LoggedInUser->id
        )
    );
    echo json_encode(array("completed" => "successfully"));
    die;
}