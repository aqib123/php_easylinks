<?php
if($get_act == "generate_custom_files"){
    //echo "ok123";
    //error_reporting(E_ALL);
    require_once BASE_DIR.'includes/pclzip.lib.php';

    create_user_constants();

    $sql = "select * from ".DB()->domains." where id = ".parseInt($_GET["DomainID"]);
    $domain = DB()->get_row($sql);

    if($domain){
        $zip_name = $domain->DomainSlug;//$LoggedInUser->user_login;
        $zip_name = str_replace(" ", "_", $zip_name);
        $zip_name = preg_replace('/[^A-Za-z0-9\_]/', '', $zip_name);
        $zip_name = str_replace("__", "_", $zip_name);
        $zip_name = strtolower($zip_name.".zip");
        $zipname = USER_DIR.$zip_name;

        delete_file($zipname);

        $zip = new PclZip($zipname);
        if ($zip) {
            $dir = str_replace("\\", "/", BASE_DIR)."@userdomain";
            $files = get_files($dir, $dir);
            foreach ($files as $file){
                $source_file = rtrim($dir, "/")."/".$file;
                $user_file = rtrim(USER_DIR, "/")."/".basename($file);
                delete_file($user_file);
                if(@copy($source_file, $user_file)){
                    if(strpos($file, "/") > 0){
                        $addpath = substr($file, 0, strrpos($file, "/"));

                        if(basename($user_file) == "config.php"){
                            $contents = file_get_contents($user_file);
                            $contents = preg_replace("/<username>/i", $LoggedInUser->user_login, $contents);
                            $contents = preg_replace("/<DomainSlug>/i", $domain->DomainSlug, $contents);
                            $contents = preg_replace("/<DomainURL>/i", rtrim(str_replace("www.", "", $domain->DomainUrl), "/"), $contents);
                            $contents = preg_replace("/<DefaultCloak>/i", ($domain->DefaultCloakURL==1?"true":"false"), $contents);
                            $contents = preg_replace("/<DefaultTitle>/i", $domain->DomainName, $contents);
                            file_put_contents($user_file, $contents);
                        }

                        $zip->add($user_file, PCLZIP_OPT_REMOVE_PATH, USER_DIR, PCLZIP_OPT_ADD_PATH, $addpath);
                        delete_file($user_file);
                    }else if(!isset($_GET["wp"])){
                        $zip->add($user_file, PCLZIP_OPT_REMOVE_PATH, USER_DIR);
                        delete_file($user_file);
                    }
                }
            }

            $zip_name = basename($zipname);

            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"".basename($zipname)."\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($zipname));
            @readfile($zipname);
            delete_file($zipname);
        }
    }
    die;
}  else if($get_act == "CheckVisibleLink"){
    $type = $_GET["type"];
    $id = parseInt($_GET["id"]);
    $DomainID = parseInt($_GET["DomainID"]);
    $VisibleLink = $_GET["VisibleLink"];
    $table = $type=="linkbank"?DB()->linkbanks:($type=="paidtraffics"?DB()->paidtraffics:DB()->rotators);
    $item = DB()->get_row("select * from ".$table." where userid=".$LoggedInUser->id." and id=".$id);
    $CurrentVisibleLink = $item?$item->VisibleLink:"";

    if($id == -1 || $VisibleLink == $CurrentVisibleLink || is_visible_link_valid($DomainID, $VisibleLink))
        http_response_code(200);
    else
        http_response_code(418);
    die;
}else if($get_act == "check_domain_slug"){
    $id = parseInt($_GET["id"]);
    $DomainSlug = $_GET["DomainSlug"];
    $domain = DB()->get_row("select * from ".DB()->domains." where userid=".$LoggedInUser->id." and id=".$id);
    $CurrentDomainSlug = $domain?$domain->DomainSlug:"";
    //echo "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and id <> ".$id." and DomainSlug='".$DomainSlug."'";
    $sql = "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and id <> ".$id." and DomainSlug='".$DomainSlug."'";
    if($DomainSlug == $CurrentDomainSlug)
        http_response_code(200);
    else if($DomainSlug != $CurrentDomainSlug && !DB()->get_row($sql))
        http_response_code(200);
    else
        http_response_code(418);
    die;
}