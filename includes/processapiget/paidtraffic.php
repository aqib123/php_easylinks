<?php
if($post_act == "save_paidtraffic"){
    $admindomain = get_available_domain(0, false);
    $jsonData = array("result" => "", "msg" => "",  "PaidTrafficID" => "-1");
    $data = array(
       "userid"                                => $apiUser->id,
       "VendorID"                              => $_POST['VendorID'],
       "PaidTrafficName"                       => $_POST['PaidTrafficName'],
       "DestinationURL"                        => $_POST['DestinationURL'],
       "PaidTrafficStatus"                     => "active",
       "TrafficCost"                           => $_POST['TrafficCost'],
       "TrafficCostType"                       => "CPC",
       "NoOfClicks"                            => $_POST['NoOfClicks'],
       "VisibleLink"                           => $_POST['VisibleLink'],
       "UseAdminDomain"                        => 1,
       "DomainID"                              => $admindomain->id,
       "CloakURL"                              => 0,
       "GroupID"                               => !empty($_POST['GroupID'])?$_POST['GroupID']:"390",
       "StartDate"                             => !empty($_POST['StartDate'])?strtotime($_POST['StartDate']):"",
       "EndDate"                               => !empty($_POST['EndDate'])?strtotime($_POST['EndDate']):"",
       "PaidTrafficActive"                     => 0,
       "TrackingCodeForActions"                => "",
       "PixelID"                               => 0,
       "EasyLinkID"                            => 0,
       "UnitPriceSales"                        => 9.95,
       "TrackingCodeForSalesAndConversions"    => "",
       "AdditionalNotes"                       => "",
       "PageImage"                             => "",
       "PendingPageID"                         => 0,
       "CompletePageID"                        => 0,
       "MasterCampaignID"                      => 0,
       "ReferenceID"                           => $_GET["ref"],
       "DateAdded"                             => strtotime("now")
    );

    if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'], $apiUser)){
        if(DB()->insert(DB()->paidtraffics, $data)){
            $jsonData["result"] = "successful";
            $jsonData["msg"] = "Paid Traffic insertion successful";
            $jsonData["PaidTrafficID"] = DB()->insert_id;
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Paid Traffic insertion failed";
        }
    }else{
        $jsonData["result"] = "failed";
        $jsonData["msg"] = "Visible Link for selected Domain already exist";
    }

    echo json_encode($jsonData);
    die;
}else if($post_act == "delete_paidtraffic"){
    delete_paid_traffic_stats(parseInt($_POST['PaidTrafficID']), $apiUser);
    if(DB()->delete(DB()->paidtraffics, array("id" => parseInt($_POST['PaidTrafficID'])))){
        DB()->delete(DB()->paidtraffic_saleinfo, array("PaidTrafficID" => $PaidTrafficID));
        $jsonData["result"] = "successful";
        $jsonData["msg"] = "Paid Traffic Deleted Successfully";
    }else{
        $jsonData["result"] = "failed";
        $jsonData["msg"] = "Paid Traffic Not Deleted";
    }
    echo json_encode($jsonData);
    die;
}else if($post_act == "reset_paidtraffic"){
    delete_paid_traffic_stats(parseInt($_POST['PaidTrafficID']), $apiUser);
    $jsonData["result"] = "successful";
    $jsonData["msg"] = "Paid Traffic Stats Reset Successfully";
    echo json_encode($jsonData);
    die;
}else if($post_act == "save_vendor"){
    $data = array(
        "userid"            => $apiUser->id,
        "VendorName"        => $_POST['VendorName'],
        "VendorType"        => "SoloProvider",
        "VendorPicture"     => "",
        "WebsiteUrl"        => $_POST['WebsiteUrl'],
        "SkypeName"         => $_POST['SkypeName'],
        "LinkedIn"          => $_POST['LinkedIn'],
        "FacebookID"        => $_POST['FacebookID'],
        "EmailAddress"      => $_POST['EmailAddress'],
        "AdditionalNotes"   => $_POST['AdditionalNotes'],
        "VendorTags"        => $_POST['VendorTags'],
        "ReferenceID"       => $_GET["ref"],
        "DateAdded"         => strtotime("now"),
    );

    if(DB()->insert(DB()->vendors, $data)){
        $jsonData["result"] = "successful";
        $jsonData["msg"] = "Paid Traffic Provider insertion successful";
        $jsonData["PaidTrafficProviderID"] = DB()->insert_id;
        $jsonData["PaidTrafficProviderName"] = $data["VendorName"];
    }else{
        $jsonData["result"] = "failed";
        $jsonData["msg"] = "Paid Traffic Provider insertion failed";
    }

    echo json_encode($jsonData);
    die;
}