<?php
if($get_act == "get_master_campaigns"){
    if($apiUser){
        $MasterCampaignsData = array();
        $jsonData = array("success" => false, "message" => "", "MasterCampaigns" => $MasterCampaignsData);
        $SubCampaignType = SYS()->is_valid($_GET['type']) ? $_GET["type"] : "all";

        $Condition = DB()->master_campaigns->userid." = ".$apiUser->id;
        if(SYS()->is_valid($_GET["status"])){
            $MasterCampaignStatus = $_GET["status"];
            $Condition .= " and md5(MasterCampaignStatus) = md5('".$MasterCampaignStatus."') ";
        }

        $MasterCampaigns = DBMasterCampaigns()->get_all($Condition);
        foreach ($MasterCampaigns as $MasterCampaign){
            $MasterCampaignID = $MasterCampaign->id;
            $LinkBankData = array();
            $PaidTrafficData = array();
            $RotatorData = array();
            $LinkSequenceData = array();
            $EmailCampaignData = array();

            if(stripos($SubCampaignType, "linkbank") !== false || $SubCampaignType == "all"){
                $LinkBanks = DBLinkbanks()->get_all(DB()->linkbanks->MasterCampaignID." = ".$MasterCampaignID);
                foreach ($LinkBanks as $LinkBank){
                    $LinkBankData[] = array(
                        "id"        => $LinkBank->id,
                        "name"      => $LinkBank->LinkName,
                        "link"      => get_linkbankurl($LinkBank),
                    );
                }
            }

            if(stripos($SubCampaignType, "paidtraffic") !== false || $SubCampaignType == "all"){
                $PaidTraffics = DBPaidtraffics()->get_all(DB()->paidtraffics->MasterCampaignID." = ".$MasterCampaignID);
                foreach ($PaidTraffics as $PaidTraffic){
                    $PaidTrafficData[] = array(
                        "id"        => $PaidTraffic->id,
                        "name"      => $PaidTraffic->PaidTrafficName,
                        "link"      => get_paidtrafficurl($PaidTraffic),
                    );
                }
            }

            if(stripos($SubCampaignType, "rotator") !== false || $SubCampaignType == "all"){
                $Rotators = DBRotators()->get_all(DB()->rotators->MasterCampaignID." = ".$MasterCampaignID);
                foreach ($Rotators as $Rotator){
                    $RotatorData[] = array(
                        "id"        => $Rotator->id,
                        "name"      => $Rotator->RotatorName,
                        "link"      => get_rotatorurl($Rotator),
                    );
                }
            }

            if(stripos($SubCampaignType, "link_sequence") !== false || $SubCampaignType == "all"){
                $LinkSequences = DBLinkSequences()->get_all(DB()->link_sequences->MasterCampaignID." = ".$MasterCampaignID);
                foreach ($LinkSequences as $LinkSequence){
                    $LinkSequenceData[] = array(
                        "id"        => $LinkSequence->id,
                        "name"      => $LinkSequence->LinkSequenceName,
                        "link"      => get_link_sequenceurl($LinkSequence),
                    );
                }
            }

            if(stripos($SubCampaignType, "email_campaign") !== false){
                $EmailCampaigns = DBCampaigns()->get_all(DB()->campaigns->MasterCampaignID." = ".$MasterCampaignID);
                foreach ($EmailCampaigns as $EmailCampaign){
                    $EmailCampaignData[] = array(
                        "id"        => $EmailCampaign->id,
                        "name"      => $EmailCampaign->CampaignName,
                        "link"      => "#",
                    );
                }
            }

            $MasterCampaignsData[] = array(
                "id"            => $MasterCampaign->id,
                "name"          => $MasterCampaign->MasterCampaignName,
                "status"        => $MasterCampaign->MasterCampaignStatus,
                "subcampaigns"  => array(
                    "linkbanks"         =>  $LinkBankData,
                    "paidtraffics"      =>  $PaidTrafficData,
                    "rotators"          =>  $RotatorData,
                    "link_sequences"    =>  $LinkSequenceData,
                    "email_campaigns"   =>  $EmailCampaignData
                )
            );
        }
        $jsonData["success"] = true;
        $jsonData["MasterCampaigns"] = $MasterCampaignsData;
    } else {
        $jsonData["message"] = "verification failed";
    }

    SYS()->json_encode($jsonData);
}