<?php
include_once "functions.php";
if(SYS()->is_valid($_GET["apiact"])){
    $api_get = $_GET["apiact"];
    if(SYS()->is_valid($_GET["apiusername"]) && SYS()->is_valid($_GET["apipassword"])){
        $apiusername = $_GET["apiusername"];
        $apipassword = $_GET["apipassword"];
        $jsonData = elUsers()->login(array("username" => $apiusername, "password" => $apipassword));
        if($jsonData["loggedin"] == true){
            $apiUser = LoggedInUser();
        }
    } else if($api_get == "verify_api_key" || SYS()->is_valid($_GET["api_key"])){
        $UserApiToken = $_GET["api_key"];
        $jsonData = array("verified" => false, "message" => "verification failed");
        $apiUser = elUsers()->login_by_api_key($UserApiToken);
        if($apiUser){
            $jsonData['verified'] = true;
            $jsonData['message'] = "Api-Key verified successfully";
        }

        if($api_get == "verify_api_key")
            SYS()->json_encode($jsonData);
    }

    if($apiUser){
        include_once "apiprocesspost.php";
        include_once "apiprocessget.php";
    }
}