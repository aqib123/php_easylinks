<?php
if(!class_exists("clsLevels")){
    class clsLevels{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        function __construct(){

        }

        function __destruct(){
            //
        }

        function get($UserID, $LevelAccessKey){
            $sql = "select ".DB()->levels->Asterisk." from ".DB()->levels." where 1=1 ".($UserID > 0 ? " and ".DB()->levels->created_userid."=".$UserID : "")." and ".DB()->levels->LevelAccessKey."='".$LevelAccessKey."' and ".DB()->levels->LevelEnabled." = 1 group by ".DB()->levels->id." ";
            $LevelType = DB()->get_row($sql);
            return $LevelType;
        }

        function get_all($UserID, $OrderBy = "LevelName"){
            $sql = "select ".DB()->levels->Asterisk." from ".DB()->levels." where ".DB()->levels->created_userid."=".$UserID." and ".DB()->levels->LevelEnabled." = 1 group by ".DB()->levels->id." ORDER BY ".DB()->levels.".".$OrderBy." ";
            $LevelTypes = DB()->get_results($sql);
            return $LevelTypes;
        }

        function get_filtered($UserID, $FilteringData, $PageNo = 0, $DataLimit = -1, $OrderBy = "LevelName"){
            $sql = "select ".DB()->levels->Asterisk." from ".DB()->levels." where ".DB()->levels->created_userid."=".$UserID;

            $sql .= " and ".DB()->levels->LevelEnabled." = 1 group by ".DB()->levels->id." ORDER BY ".DB()->levels.".".$OrderBy." ";
            $sqlCount = 'select count(distinct id) as TotalLevels from ('.$sql.') as LevelsTable';

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Levels = DB()->get_results($sql);
            $TotalLevels = DB()->get_var($sqlCount);

            return array('Levels' => $Levels, 'TotalLevels' => $TotalLevels);
        }
    }
}

global $elLevels;
if(class_exists("clsLevels") && !$elLevels){
    $elLevels = new clsLevels();
}

if(!function_exists("elLevels")){
    function elLevels() {
        return clsLevels::instance();
    }
}
$GLOBALS['elLevels'] = elLevels();