<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructUsers'))
	include_once 'struct/Users.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBUsers')){
	class clsDBUsers extends clsStructUsers {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
        /**
         * Summary of $LevelIDs
         * @var clsDBLevels[]
         */
        var $LevelIDs;

        /**
         * Summary of $MenuItems
         * @var clsDBMenuItems[]
         */
        var $MenuItems;

        /**
         * Summary of $Modules
         * @var clsDBModules[]
         */
        var $Modules;
        /** [User Variables End] **/

		function __construct($obj = null){
            parent::__construct();

            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_last_id(){
            return clsDBUsers::$Lastid;
        }

        function set_last_id($Lastid){
            clsDBUsers::$Lastid = $Lastid;
        }

        /**
         * Summary of get_typed_row
         * @param mixed $Row
         * @return clsDBUsers
         */
        function get_typed_row($Row){
            return $Row;
        }

        /**
         * Summary of get_typed_rows
         * @param mixed $Rows
         * @return clsDBUsers[]
         */
        function get_typed_rows($Rows){
            return $Rows;
        }

        function get_var($FieldValue, $FieldName, $ReturnField = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->users.".") == false ? DB()->users.".".$OrderBy: $OrderBy;
            $sql = "select ".$ReturnField." from ".DB()->users." where ".$FieldName." = '".$FieldValue."' ORDER BY ".$OrderBy." ".$OrderType;;
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBUsers|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->users->Asterisk." from ".DB()->users;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBUsers|null
         */
        function get_by_key($id){
            $Condition = DB()->users->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBUsers|null
         */
        function get_row($id = null, $user_login = null, $user_pass = null, $user_pass_backup = null, $user_nicename = null, $user_email = null, $user_url = null, $user_registered = null, $user_activation_key = null, $user_status = null, $display_name = null, $user_picture = null, $user_password_key = null, $timezoneid = null, $user_api_token = null, $user_type = null, $user_currency_symbol = null, $user_access_key = null, $user_parent_id = null, $force_username_change = null, $force_password_change = null, $force_email_change = null, $registration_email_sent = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($user_login !== null) $data["user_login"] = $user_login;
			if($user_pass !== null) $data["user_pass"] = $user_pass;
			if($user_pass_backup !== null) $data["user_pass_backup"] = $user_pass_backup;
			if($user_nicename !== null) $data["user_nicename"] = $user_nicename;
			if($user_email !== null) $data["user_email"] = $user_email;
			if($user_url !== null) $data["user_url"] = $user_url;
			if($user_registered !== null) $data["user_registered"] = $user_registered;
			if($user_activation_key !== null) $data["user_activation_key"] = $user_activation_key;
			if($user_status !== null) $data["user_status"] = $user_status;
			if($display_name !== null) $data["display_name"] = $display_name;
			if($user_picture !== null) $data["user_picture"] = $user_picture;
			if($user_password_key !== null) $data["user_password_key"] = $user_password_key;
			if($timezoneid !== null) $data["timezoneid"] = $timezoneid;
			if($user_api_token !== null) $data["user_api_token"] = $user_api_token;
			if($user_type !== null) $data["user_type"] = $user_type;
			if($user_currency_symbol !== null) $data["user_currency_symbol"] = $user_currency_symbol;
			if($user_access_key !== null) $data["user_access_key"] = $user_access_key;
			if($user_parent_id !== null) $data["user_parent_id"] = $user_parent_id;
			if($force_username_change !== null) $data["force_username_change"] = $force_username_change;
			if($force_password_change !== null) $data["force_password_change"] = $force_password_change;
			if($force_email_change !== null) $data["force_email_change"] = $force_email_change;
			if($registration_email_sent !== null) $data["registration_email_sent"] = $registration_email_sent;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->users->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBUsers[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->users.".") == false ? DB()->users.".".$OrderBy: $OrderBy;
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->users->Asterisk." from ".DB()->users;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->users.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->users->id.") from ".DB()->users;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBUsers[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc", $Having = ""){
            $sql = stripos($Condition, "select ") === false ? " from ".DB()->users : $Condition;
            $Condition =  stripos($Condition, "select ") === false ? $Condition : "";
            $FilterQuery = array();
            //$OrderBy = stripos($OrderBy, DB()->users.".") == false ? DB()->users.".".$OrderBy: $OrderBy;

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    if(is_array($Filter) && SYS()->is_string($Filter['condition']) && SYS()->is_valid($Filter['value'])){
                        $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                    }
                }
            }

            $Condition = $Condition.(($Condition != "" && count($FilterQuery) > 0? " and " : "").implode(" and ", $FilterQuery));

            if(stripos($Condition, "where") === false && $Condition != "")
                $sql .= " where ".$Condition;

            if(stripos($sql, "select ") === false){
                $sqlCount = "select count(".DB()->users->id.") ".$sql;
                $sql = "select ".DB()->users->Asterisk.$sql." group by ".DB()->users.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                if(stripos($sql, '{from_start}') !== false){
                    $sqlCount = "select count(".DB()->users->id.") ".substr($sql, stripos($sql, "{from_start}"));
                } else {
                    $sqlCount = "select count(".DB()->users->id.") ".substr($sql, stripos($sql, " from"));
                }
                $sql = $sql." ".$Condition." group by ".DB()->users.".".$GroupBy.($Having != "" ? " having (".$Having.") " : "");//." ORDER BY ".$OrderBy." ".$OrderType;
            }

            if(stripos($sql, '{from_start}') !== false){
                $sql = str_ireplace('{from_start}', '', $sql);
                $sqlCount = "select count(*) from (".$sql.") as tbl";
                $sql = "select * from (".$sql.") as tbl";
            }

            $sql .=  " ORDER BY ".$OrderBy." ".$OrderType;

            if(stripos($sqlCount, '{from_start}') !== false)
                $sqlCount = str_ireplace('{from_start}', '', $sqlCount);

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($user_login = null, $user_pass = null, $user_pass_backup = null, $user_nicename = null, $user_email = null, $user_url = null, $user_registered = null, $user_activation_key = null, $user_status = null, $display_name = null, $user_picture = null, $user_password_key = null, $timezoneid = null, $user_api_token = null, $user_type = null, $user_currency_symbol = null, $user_access_key = null, $user_parent_id = null, $force_username_change = null, $force_password_change = null, $force_email_change = null, $registration_email_sent = null){
            $data = array();
            $Inserted = false;

            if($user_login !== null) $data["user_login"] = $user_login;
			if($user_pass !== null) $data["user_pass"] = $user_pass;
			if($user_pass_backup !== null) $data["user_pass_backup"] = $user_pass_backup;
			if($user_nicename !== null) $data["user_nicename"] = $user_nicename;
			if($user_email !== null) $data["user_email"] = $user_email;
			if($user_url !== null) $data["user_url"] = $user_url;
			if($user_registered !== null) $data["user_registered"] = $user_registered;
			if($user_activation_key !== null) $data["user_activation_key"] = $user_activation_key;
			if($user_status !== null) $data["user_status"] = $user_status;
			if($display_name !== null) $data["display_name"] = $display_name;
			if($user_picture !== null) $data["user_picture"] = $user_picture;
			if($user_password_key !== null) $data["user_password_key"] = $user_password_key;
			if($timezoneid !== null) $data["timezoneid"] = $timezoneid;
			if($user_api_token !== null) $data["user_api_token"] = $user_api_token;
			if($user_type !== null) $data["user_type"] = $user_type;
			if($user_currency_symbol !== null) $data["user_currency_symbol"] = $user_currency_symbol;
			if($user_access_key !== null) $data["user_access_key"] = $user_access_key;
			if($user_parent_id !== null) $data["user_parent_id"] = $user_parent_id;
			if($force_username_change !== null) $data["force_username_change"] = $force_username_change;
			if($force_password_change !== null) $data["force_password_change"] = $force_password_change;
			if($force_email_change !== null) $data["force_email_change"] = $force_email_change;
			if($registration_email_sent !== null) $data["registration_email_sent"] = $registration_email_sent;

            $this->set_last_id(0);
            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->users, $data);
                if($Inserted){
                    $this->set_last_id(DB()->insert_id);
                }
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $user_login = null, $user_pass = null, $user_pass_backup = null, $user_nicename = null, $user_email = null, $user_url = null, $user_registered = null, $user_activation_key = null, $user_status = null, $display_name = null, $user_picture = null, $user_password_key = null, $timezoneid = null, $user_api_token = null, $user_type = null, $user_currency_symbol = null, $user_access_key = null, $user_parent_id = null, $force_username_change = null, $force_password_change = null, $force_email_change = null, $registration_email_sent = null){
            $data = array();
            $Updated = false;

            if($user_login !== null) $data["user_login"] = $user_login;
			if($user_pass !== null) $data["user_pass"] = $user_pass;
			if($user_pass_backup !== null) $data["user_pass_backup"] = $user_pass_backup;
			if($user_nicename !== null) $data["user_nicename"] = $user_nicename;
			if($user_email !== null) $data["user_email"] = $user_email;
			if($user_url !== null) $data["user_url"] = $user_url;
			if($user_registered !== null) $data["user_registered"] = $user_registered;
			if($user_activation_key !== null) $data["user_activation_key"] = $user_activation_key;
			if($user_status !== null) $data["user_status"] = $user_status;
			if($display_name !== null) $data["display_name"] = $display_name;
			if($user_picture !== null) $data["user_picture"] = $user_picture;
			if($user_password_key !== null) $data["user_password_key"] = $user_password_key;
			if($timezoneid !== null) $data["timezoneid"] = $timezoneid;
			if($user_api_token !== null) $data["user_api_token"] = $user_api_token;
			if($user_type !== null) $data["user_type"] = $user_type;
			if($user_currency_symbol !== null) $data["user_currency_symbol"] = $user_currency_symbol;
			if($user_access_key !== null) $data["user_access_key"] = $user_access_key;
			if($user_parent_id !== null) $data["user_parent_id"] = $user_parent_id;
			if($force_username_change !== null) $data["force_username_change"] = $force_username_change;
			if($force_password_change !== null) $data["force_password_change"] = $force_password_change;
			if($force_email_change !== null) $data["force_email_change"] = $force_email_change;
			if($registration_email_sent !== null) $data["registration_email_sent"] = $registration_email_sent;

            $this->set_last_id(0);
            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->users, $data, array("id" => $id));
                $this->set_last_id($id);
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $user_login = null, $user_pass = null, $user_pass_backup = null, $user_nicename = null, $user_email = null, $user_url = null, $user_registered = null, $user_activation_key = null, $user_status = null, $display_name = null, $user_picture = null, $user_password_key = null, $timezoneid = null, $user_api_token = null, $user_type = null, $user_currency_symbol = null, $user_access_key = null, $user_parent_id = null, $force_username_change = null, $force_password_change = null, $force_email_change = null, $registration_email_sent = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($user_login !== null) $data["user_login"] = $user_login;
			if($user_pass !== null) $data["user_pass"] = $user_pass;
			if($user_pass_backup !== null) $data["user_pass_backup"] = $user_pass_backup;
			if($user_nicename !== null) $data["user_nicename"] = $user_nicename;
			if($user_email !== null) $data["user_email"] = $user_email;
			if($user_url !== null) $data["user_url"] = $user_url;
			if($user_registered !== null) $data["user_registered"] = $user_registered;
			if($user_activation_key !== null) $data["user_activation_key"] = $user_activation_key;
			if($user_status !== null) $data["user_status"] = $user_status;
			if($display_name !== null) $data["display_name"] = $display_name;
			if($user_picture !== null) $data["user_picture"] = $user_picture;
			if($user_password_key !== null) $data["user_password_key"] = $user_password_key;
			if($timezoneid !== null) $data["timezoneid"] = $timezoneid;
			if($user_api_token !== null) $data["user_api_token"] = $user_api_token;
			if($user_type !== null) $data["user_type"] = $user_type;
			if($user_currency_symbol !== null) $data["user_currency_symbol"] = $user_currency_symbol;
			if($user_access_key !== null) $data["user_access_key"] = $user_access_key;
			if($user_parent_id !== null) $data["user_parent_id"] = $user_parent_id;
			if($force_username_change !== null) $data["force_username_change"] = $force_username_change;
			if($force_password_change !== null) $data["force_password_change"] = $force_password_change;
			if($force_email_change !== null) $data["force_email_change"] = $force_email_change;
			if($registration_email_sent !== null) $data["registration_email_sent"] = $registration_email_sent;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->users, $data);
            }

            return $Deleted;
        }

        function save($Condition, $user_login = null, $user_pass = null, $user_pass_backup = null, $user_nicename = null, $user_email = null, $user_url = null, $user_registered = null, $user_activation_key = null, $user_status = null, $display_name = null, $user_picture = null, $user_password_key = null, $timezoneid = null, $user_api_token = null, $user_type = null, $user_currency_symbol = null, $user_access_key = null, $user_parent_id = null, $force_username_change = null, $force_password_change = null, $force_email_change = null, $registration_email_sent = null){
            $DBRow = $this->get($Condition);
            $Saved = false;
            if(!$DBRow){
                $Saved = $this->insert($user_login, $user_pass, $user_pass_backup, $user_nicename, $user_email, $user_url, $user_registered, $user_activation_key, $user_status, $display_name, $user_picture, $user_password_key, $timezoneid, $user_api_token, $user_type, $user_currency_symbol, $user_access_key, $user_parent_id, $force_username_change, $force_password_change, $force_email_change, $registration_email_sent);
            } else {
                $this->update($DBRow->id, $user_login, $user_pass, $user_pass_backup, $user_nicename, $user_email, $user_url, $user_registered, $user_activation_key, $user_status, $display_name, $user_picture, $user_password_key, $timezoneid, $user_api_token, $user_type, $user_currency_symbol, $user_access_key, $user_parent_id, $force_username_change, $force_password_change, $force_email_change, $registration_email_sent);
                $Saved = true;
            }

            return $Saved;
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBUsers')){
	function DBUsers($obj = null) {
		return clsDBUsers::instance($obj);
	}
}