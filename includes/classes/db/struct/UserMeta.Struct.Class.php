<?php
if(!class_exists('clsStructUserMeta')){
	class clsStructUserMeta{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $UserMetaKey;
		
		/**
		* @var string
		*/
		var $UserMetaValue;
		
		/**
		* @var integer
		*/
		var $UserMetaIsSecure;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_user_meta.*';
			$this->id = 'l2sun1el_user_meta.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_user_meta.userid';
			$this->UserMetaKey = 'l2sun1el_user_meta.UserMetaKey';
			$this->UserMetaValue = 'l2sun1el_user_meta.UserMetaValue';
			$this->UserMetaIsSecure = 'l2sun1el_user_meta.UserMetaIsSecure';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_user_meta';
		}
	}
}