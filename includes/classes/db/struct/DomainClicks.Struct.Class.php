<?php
if(!class_exists('clsStructDomainClicks')){
	class clsStructDomainClicks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ClickIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_domain_clicks.*';
			$this->id = 'l2sun1el_domain_clicks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->DomainID = 'l2sun1el_domain_clicks.DomainID';
			$this->DateAdded = 'l2sun1el_domain_clicks.DateAdded';
			$this->ClickIp = 'l2sun1el_domain_clicks.ClickIp';
			$this->CountryCode = 'l2sun1el_domain_clicks.CountryCode';
			$this->BrowserName = 'l2sun1el_domain_clicks.BrowserName';
			$this->BrowserVersion = 'l2sun1el_domain_clicks.BrowserVersion';
			$this->Platform = 'l2sun1el_domain_clicks.Platform';
			$this->BotName = 'l2sun1el_domain_clicks.BotName';
			$this->EmailID = 'l2sun1el_domain_clicks.EmailID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_domain_clicks';
		}
	}
}