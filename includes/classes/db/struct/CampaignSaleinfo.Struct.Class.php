<?php
if(!class_exists('clsStructCampaignSaleinfo')){
	class clsStructCampaignSaleinfo{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $CampaignID;
		
		/**
		* @var string
		*/
		var $SaleInfoType;
		
		/**
		* @var string
		*/
		var $SaleInfoTitle;
		
		/**
		* @var string
		*/
		var $SaleInfoAmount;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_campaign_saleinfo.*';
			$this->id = 'l2sun1el_campaign_saleinfo.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->CampaignID = 'l2sun1el_campaign_saleinfo.CampaignID';
			$this->SaleInfoType = 'l2sun1el_campaign_saleinfo.SaleInfoType';
			$this->SaleInfoTitle = 'l2sun1el_campaign_saleinfo.SaleInfoTitle';
			$this->SaleInfoAmount = 'l2sun1el_campaign_saleinfo.SaleInfoAmount';
			$this->DateAdded = 'l2sun1el_campaign_saleinfo.DateAdded';
			$this->userid = 'l2sun1el_campaign_saleinfo.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_campaign_saleinfo';
		}
	}
}