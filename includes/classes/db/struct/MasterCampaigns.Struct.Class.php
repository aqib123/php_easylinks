<?php
if(!class_exists('clsStructMasterCampaigns')){
	class clsStructMasterCampaigns{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $MasterCampaignName;
		
		/**
		* @var string
		*/
		var $MasterCampaignStatus;
		
		/**
		* @var string
		*/
		var $MasterCampaignAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_master_campaigns.*';
			$this->id = 'l2sun1el_master_campaigns.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->MasterCampaignName = 'l2sun1el_master_campaigns.MasterCampaignName';
			$this->MasterCampaignStatus = 'l2sun1el_master_campaigns.MasterCampaignStatus';
			$this->MasterCampaignAccessKey = 'l2sun1el_master_campaigns.MasterCampaignAccessKey';
			$this->DateAdded = 'l2sun1el_master_campaigns.DateAdded';
			$this->userid = 'l2sun1el_master_campaigns.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_master_campaigns';
		}
	}
}