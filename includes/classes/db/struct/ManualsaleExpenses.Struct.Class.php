<?php
if(!class_exists('clsStructManualsaleExpenses')){
	class clsStructManualsaleExpenses{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $ManualSaleSplitPartnerID;
		
		/**
		* @var string
		*/
		var $ExpenseName;
		
		/**
		* @var string
		*/
		var $ExpenseNote;
		
		/**
		* @var string
		*/
		var $ExpenseAmount;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsale_expenses.*';
			$this->id = 'l2sun1el_manualsale_expenses.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ManualSaleSplitPartnerID = 'l2sun1el_manualsale_expenses.ManualSaleSplitPartnerID';
			$this->ExpenseName = 'l2sun1el_manualsale_expenses.ExpenseName';
			$this->ExpenseNote = 'l2sun1el_manualsale_expenses.ExpenseNote';
			$this->ExpenseAmount = 'l2sun1el_manualsale_expenses.ExpenseAmount';
			$this->DateAdded = 'l2sun1el_manualsale_expenses.DateAdded';
			$this->userid = 'l2sun1el_manualsale_expenses.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsale_expenses';
		}
	}
}