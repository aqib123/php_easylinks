<?php
if(!class_exists('clsStructDbaCode')){
	class clsStructDbaCode{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $email_id;
		
		/**
		* @var string
		*/
		var $code;
		
		/**
		* @var integer
		*/
		var $status;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_dba_code.*';
			$this->id = 'l2sun1el_dba_code.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->email_id = 'l2sun1el_dba_code.email_id';
			$this->code = 'l2sun1el_dba_code.code';
			$this->status = 'l2sun1el_dba_code.status';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_dba_code';
		}
	}
}