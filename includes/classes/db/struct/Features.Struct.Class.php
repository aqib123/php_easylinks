<?php
if(!class_exists('clsStructFeatures')){
	class clsStructFeatures{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $FeatureType;
		
		/**
		* @var string
		*/
		var $FeatureName;
		
		/**
		* @var string
		*/
		var $FeatureAccessKey;
		
		/**
		* @var string
		*/
		var $FeatureLabel;
		
		/**
		* @var integer
		*/
		var $FeatureOrder;
		
		/**
		* @var integer
		*/
		var $FeatureEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_features.*';
			$this->id = 'l2sun1el_features.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->FeatureType = 'l2sun1el_features.FeatureType';
			$this->FeatureName = 'l2sun1el_features.FeatureName';
			$this->FeatureAccessKey = 'l2sun1el_features.FeatureAccessKey';
			$this->FeatureLabel = 'l2sun1el_features.FeatureLabel';
			$this->FeatureOrder = 'l2sun1el_features.FeatureOrder';
			$this->FeatureEnabled = 'l2sun1el_features.FeatureEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_features';
		}
	}
}