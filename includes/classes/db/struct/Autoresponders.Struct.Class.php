<?php
if(!class_exists('clsStructAutoresponders')){
	class clsStructAutoresponders{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $AutoresponderName;
		
		/**
		* @var string
		*/
		var $AutoresponderType;
		
		/**
		* @var string
		*/
		var $AutoresponderAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $parentid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_autoresponders.*';
			$this->id = 'l2sun1el_autoresponders.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->AutoresponderName = 'l2sun1el_autoresponders.AutoresponderName';
			$this->AutoresponderType = 'l2sun1el_autoresponders.AutoresponderType';
			$this->AutoresponderAccessKey = 'l2sun1el_autoresponders.AutoresponderAccessKey';
			$this->DateAdded = 'l2sun1el_autoresponders.DateAdded';
			$this->userid = 'l2sun1el_autoresponders.userid';
			$this->parentid = 'l2sun1el_autoresponders.parentid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_autoresponders';
		}
	}
}