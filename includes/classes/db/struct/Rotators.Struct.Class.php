<?php
if(!class_exists('clsStructRotators')){
	class clsStructRotators{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $RotatorName;
		
		/**
		* @var string
		*/
		var $RoratorAccessKey;
		
		/**
		* @var integer
		*/
		var $GroupID;
		
		/**
		* @var string
		*/
		var $RotatorType;
		
		/**
		* @var integer
		*/
		var $RotatorIsSticky;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $hasSingleLink;
		
		/**
		* @var string
		*/
		var $RotatorStatus;
		
		/**
		* @var integer
		*/
		var $UseAdminDomain;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var string
		*/
		var $VisibleLink;
		
		/**
		* @var integer
		*/
		var $LastRotatorVisitID;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		
		/**
		* @var integer
		*/
		var $CloakURL;
		
		/**
		* @var integer
		*/
		var $LastSplitRotatorLinkPosition;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_rotators.*';
			$this->id = 'l2sun1el_rotators.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_rotators.userid';
			$this->RotatorName = 'l2sun1el_rotators.RotatorName';
			$this->RoratorAccessKey = 'l2sun1el_rotators.RoratorAccessKey';
			$this->GroupID = 'l2sun1el_rotators.GroupID';
			$this->RotatorType = 'l2sun1el_rotators.RotatorType';
			$this->RotatorIsSticky = 'l2sun1el_rotators.RotatorIsSticky';
			$this->DateAdded = 'l2sun1el_rotators.DateAdded';
			$this->hasSingleLink = 'l2sun1el_rotators.hasSingleLink';
			$this->RotatorStatus = 'l2sun1el_rotators.RotatorStatus';
			$this->UseAdminDomain = 'l2sun1el_rotators.UseAdminDomain';
			$this->DomainID = 'l2sun1el_rotators.DomainID';
			$this->VisibleLink = 'l2sun1el_rotators.VisibleLink';
			$this->LastRotatorVisitID = 'l2sun1el_rotators.LastRotatorVisitID';
			$this->MasterCampaignID = 'l2sun1el_rotators.MasterCampaignID';
			$this->CloakURL = 'l2sun1el_rotators.CloakURL';
			$this->LastSplitRotatorLinkPosition = 'l2sun1el_rotators.LastSplitRotatorLinkPosition';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_rotators';
		}
	}
}