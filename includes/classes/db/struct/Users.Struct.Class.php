<?php
if(!class_exists('clsStructUsers')){
	class clsStructUsers{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $user_login;
		
		/**
		* @var string
		*/
		var $user_pass;
		
		/**
		* @var string
		*/
		var $user_pass_backup;
		
		/**
		* @var string
		*/
		var $user_nicename;
		
		/**
		* @var string
		*/
		var $user_email;
		
		/**
		* @var string
		*/
		var $user_url;
		
		/**
		* @var string
		*/
		var $user_registered;
		
		/**
		* @var string
		*/
		var $user_activation_key;
		
		/**
		* @var integer
		*/
		var $user_status;
		
		/**
		* @var string
		*/
		var $display_name;
		
		/**
		* @var string
		*/
		var $user_picture;
		
		/**
		* @var string
		*/
		var $user_password_key;
		
		/**
		* @var string
		*/
		var $timezoneid;
		
		/**
		* @var string
		*/
		var $user_api_token;
		
		/**
		* @var string
		*/
		var $user_type;
		
		/**
		* @var string
		*/
		var $user_currency_symbol;
		
		/**
		* @var string
		*/
		var $user_access_key;
		
		/**
		* @var integer
		*/
		var $user_parent_id;
		
		/**
		* @var integer
		*/
		var $force_username_change;
		
		/**
		* @var integer
		*/
		var $force_password_change;
		
		/**
		* @var integer
		*/
		var $force_email_change;
		
		/**
		* @var integer
		*/
		var $registration_email_sent;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_users.*';
			$this->id = 'l2sun1el_users.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->user_login = 'l2sun1el_users.user_login';
			$this->user_pass = 'l2sun1el_users.user_pass';
			$this->user_pass_backup = 'l2sun1el_users.user_pass_backup';
			$this->user_nicename = 'l2sun1el_users.user_nicename';
			$this->user_email = 'l2sun1el_users.user_email';
			$this->user_url = 'l2sun1el_users.user_url';
			$this->user_registered = 'l2sun1el_users.user_registered';
			$this->user_activation_key = 'l2sun1el_users.user_activation_key';
			$this->user_status = 'l2sun1el_users.user_status';
			$this->display_name = 'l2sun1el_users.display_name';
			$this->user_picture = 'l2sun1el_users.user_picture';
			$this->user_password_key = 'l2sun1el_users.user_password_key';
			$this->timezoneid = 'l2sun1el_users.timezoneid';
			$this->user_api_token = 'l2sun1el_users.user_api_token';
			$this->user_type = 'l2sun1el_users.user_type';
			$this->user_currency_symbol = 'l2sun1el_users.user_currency_symbol';
			$this->user_access_key = 'l2sun1el_users.user_access_key';
			$this->user_parent_id = 'l2sun1el_users.user_parent_id';
			$this->force_username_change = 'l2sun1el_users.force_username_change';
			$this->force_password_change = 'l2sun1el_users.force_password_change';
			$this->force_email_change = 'l2sun1el_users.force_email_change';
			$this->registration_email_sent = 'l2sun1el_users.registration_email_sent';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_users';
		}
	}
}