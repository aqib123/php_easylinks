<?php
if(!class_exists('clsStructCampaigns')){
	class clsStructCampaigns{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $CampaignName;
		
		/**
		* @var string
		*/
		var $CampaignAccessKey;
		
		/**
		* @var integer
		*/
		var $GroupID;
		
		/**
		* @var integer
		*/
		var $SaleQuantity;
		
		/**
		* @var string
		*/
		var $GrossSale;
		
		/**
		* @var string
		*/
		var $ExtraIncome;
		
		/**
		* @var string
		*/
		var $Expenses;
		
		/**
		* @var string
		*/
		var $Commission;
		
		/**
		* @var string
		*/
		var $PartnerSplit;
		
		/**
		* @var string
		*/
		var $AdditionalNotes;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $hasSingleEmail;
		
		/**
		* @var string
		*/
		var $CampaignStatus;
		
		/**
		* @var string
		*/
		var $SaleInfoEntered;
		
		/**
		* @var string
		*/
		var $StartDate;
		
		/**
		* @var string
		*/
		var $EndDate;
		
		/**
		* @var string
		*/
		var $OwedToPartner;
		
		/**
		* @var integer
		*/
		var $SplitExpensesWithPartner;
		
		/**
		* @var string
		*/
		var $SplitExpensesPercentage;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_campaigns.*';
			$this->id = 'l2sun1el_campaigns.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_campaigns.userid';
			$this->CampaignName = 'l2sun1el_campaigns.CampaignName';
			$this->CampaignAccessKey = 'l2sun1el_campaigns.CampaignAccessKey';
			$this->GroupID = 'l2sun1el_campaigns.GroupID';
			$this->SaleQuantity = 'l2sun1el_campaigns.SaleQuantity';
			$this->GrossSale = 'l2sun1el_campaigns.GrossSale';
			$this->ExtraIncome = 'l2sun1el_campaigns.ExtraIncome';
			$this->Expenses = 'l2sun1el_campaigns.Expenses';
			$this->Commission = 'l2sun1el_campaigns.Commission';
			$this->PartnerSplit = 'l2sun1el_campaigns.PartnerSplit';
			$this->AdditionalNotes = 'l2sun1el_campaigns.AdditionalNotes';
			$this->DateAdded = 'l2sun1el_campaigns.DateAdded';
			$this->hasSingleEmail = 'l2sun1el_campaigns.hasSingleEmail';
			$this->CampaignStatus = 'l2sun1el_campaigns.CampaignStatus';
			$this->SaleInfoEntered = 'l2sun1el_campaigns.SaleInfoEntered';
			$this->StartDate = 'l2sun1el_campaigns.StartDate';
			$this->EndDate = 'l2sun1el_campaigns.EndDate';
			$this->OwedToPartner = 'l2sun1el_campaigns.OwedToPartner';
			$this->SplitExpensesWithPartner = 'l2sun1el_campaigns.SplitExpensesWithPartner';
			$this->SplitExpensesPercentage = 'l2sun1el_campaigns.SplitExpensesPercentage';
			$this->MasterCampaignID = 'l2sun1el_campaigns.MasterCampaignID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_campaigns';
		}
	}
}