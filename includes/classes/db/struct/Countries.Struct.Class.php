<?php
if(!class_exists('clsStructCountries')){
	class clsStructCountries{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $countrycode;
		
		/**
		* @var string
		*/
		var $countryname;
		
		/**
		* @var integer
		*/
		var $countrytier;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_countries.*';
			$this->id = 'l2sun1el_countries.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->countrycode = 'l2sun1el_countries.countrycode';
			$this->countryname = 'l2sun1el_countries.countryname';
			$this->countrytier = 'l2sun1el_countries.countrytier';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_countries';
		}
	}
}