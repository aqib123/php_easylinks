<?php
if(!class_exists('clsStructSplitPartners')){
	class clsStructSplitPartners{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $PartnerName;
		
		/**
		* @var string
		*/
		var $PartnerType;
		
		/**
		* @var string
		*/
		var $PartnerAccessKey;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $PartnerPicture;
		
		/**
		* @var string
		*/
		var $WebsiteUrl;
		
		/**
		* @var string
		*/
		var $SkypeName;
		
		/**
		* @var string
		*/
		var $LinkedIn;
		
		/**
		* @var string
		*/
		var $FacebookID;
		
		/**
		* @var string
		*/
		var $EmailAddress;
		
		/**
		* @var string
		*/
		var $AdditionalNotes;
		
		/**
		* @var string
		*/
		var $PartnerTags;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_split_partners.*';
			$this->id = 'l2sun1el_split_partners.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->PartnerName = 'l2sun1el_split_partners.PartnerName';
			$this->PartnerType = 'l2sun1el_split_partners.PartnerType';
			$this->PartnerAccessKey = 'l2sun1el_split_partners.PartnerAccessKey';
			$this->DateAdded = 'l2sun1el_split_partners.DateAdded';
			$this->PartnerPicture = 'l2sun1el_split_partners.PartnerPicture';
			$this->WebsiteUrl = 'l2sun1el_split_partners.WebsiteUrl';
			$this->SkypeName = 'l2sun1el_split_partners.SkypeName';
			$this->LinkedIn = 'l2sun1el_split_partners.LinkedIn';
			$this->FacebookID = 'l2sun1el_split_partners.FacebookID';
			$this->EmailAddress = 'l2sun1el_split_partners.EmailAddress';
			$this->AdditionalNotes = 'l2sun1el_split_partners.AdditionalNotes';
			$this->PartnerTags = 'l2sun1el_split_partners.PartnerTags';
			$this->userid = 'l2sun1el_split_partners.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_split_partners';
		}
	}
}