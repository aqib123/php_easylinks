<?php
if(!class_exists('clsStructIpaddressInfo')){
	class clsStructIpaddressInfo{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastip_start;
		var $PrimaryKey;
		
		/**
		* @var string
		*/
		var $addr_type;
		
		/**
		* @var string
		*/
		var $ip_start;
		
		/**
		* @var string
		*/
		var $ip_end;
		
		/**
		* @var string
		*/
		var $country;
		
		/**
		* @var string
		*/
		var $stateprov;
		
		/**
		* @var string
		*/
		var $city;
		
		/**
		* @var string
		*/
		var $latitude;
		
		/**
		* @var string
		*/
		var $longitude;
		
		/**
		* @var string
		*/
		var $timezone_offset;
		
		/**
		* @var string
		*/
		var $timezone_name;
		
		/**
		* @var string
		*/
		var $isp_name;
		
		/**
		* @var string
		*/
		var $connection_type;
		
		/**
		* @var string
		*/
		var $organization_name;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_ipaddress_info.*';
			$this->addr_type = 'l2sun1el_ipaddress_info.addr_type';
			$this->ip_start = 'l2sun1el_ipaddress_info.ip_start';
		/**
		* @var string
		*/
		
			$this->PrimaryKey = 'ip_start';
			$this->ip_end = 'l2sun1el_ipaddress_info.ip_end';
			$this->country = 'l2sun1el_ipaddress_info.country';
			$this->stateprov = 'l2sun1el_ipaddress_info.stateprov';
			$this->city = 'l2sun1el_ipaddress_info.city';
			$this->latitude = 'l2sun1el_ipaddress_info.latitude';
			$this->longitude = 'l2sun1el_ipaddress_info.longitude';
			$this->timezone_offset = 'l2sun1el_ipaddress_info.timezone_offset';
			$this->timezone_name = 'l2sun1el_ipaddress_info.timezone_name';
			$this->isp_name = 'l2sun1el_ipaddress_info.isp_name';
			$this->connection_type = 'l2sun1el_ipaddress_info.connection_type';
			$this->organization_name = 'l2sun1el_ipaddress_info.organization_name';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_ipaddress_info';
		}
	}
}