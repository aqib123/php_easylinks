<?php
if(!class_exists('clsStructHelpContents')){
	class clsStructHelpContents{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $HelpContentTitle;
		
		/**
		* @var string
		*/
		var $HelpContentExcerpt;
		
		/**
		* @var string
		*/
		var $HelpContentText;
		
		/**
		* @var string
		*/
		var $HelpContentType;
		
		/**
		* @var integer
		*/
		var $HelpContentPosition;
		
		/**
		* @var integer
		*/
		var $HelpContentEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_help_contents.*';
			$this->id = 'l2sun1el_help_contents.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->HelpContentTitle = 'l2sun1el_help_contents.HelpContentTitle';
			$this->HelpContentExcerpt = 'l2sun1el_help_contents.HelpContentExcerpt';
			$this->HelpContentText = 'l2sun1el_help_contents.HelpContentText';
			$this->HelpContentType = 'l2sun1el_help_contents.HelpContentType';
			$this->HelpContentPosition = 'l2sun1el_help_contents.HelpContentPosition';
			$this->HelpContentEnabled = 'l2sun1el_help_contents.HelpContentEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_help_contents';
		}
	}
}