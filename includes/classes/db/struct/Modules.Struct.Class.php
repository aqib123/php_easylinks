<?php
if(!class_exists('clsStructModules')){
	class clsStructModules{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $ModuleName;
		
		/**
		* @var string
		*/
		var $ModuleLabel;
		
		/**
		* @var integer
		*/
		var $ModuleEnabled;
		
		/**
		* @var integer
		*/
		var $ModuleAlwaysAdd;
		
		/**
		* @var integer
		*/
		var $ModuleExtendable;
		
		/**
		* @var integer
		*/
		var $ModulePosition;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_modules.*';
			$this->id = 'l2sun1el_modules.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->ModuleName = 'l2sun1el_modules.ModuleName';
			$this->ModuleLabel = 'l2sun1el_modules.ModuleLabel';
			$this->ModuleEnabled = 'l2sun1el_modules.ModuleEnabled';
			$this->ModuleAlwaysAdd = 'l2sun1el_modules.ModuleAlwaysAdd';
			$this->ModuleExtendable = 'l2sun1el_modules.ModuleExtendable';
			$this->ModulePosition = 'l2sun1el_modules.ModulePosition';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_modules';
		}
	}
}