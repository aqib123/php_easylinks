<?php
if(!class_exists('clsStructLinkbankSaleinfo')){
	class clsStructLinkbankSaleinfo{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var string
		*/
		var $SaleInfoType;
		
		/**
		* @var string
		*/
		var $SaleInfoTitle;
		
		/**
		* @var integer
		*/
		var $isPartnerAmount;
		
		/**
		* @var string
		*/
		var $SaleInfoAmount;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_linkbank_saleinfo.*';
			$this->id = 'l2sun1el_linkbank_saleinfo.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LinkBankID = 'l2sun1el_linkbank_saleinfo.LinkBankID';
			$this->SaleInfoType = 'l2sun1el_linkbank_saleinfo.SaleInfoType';
			$this->SaleInfoTitle = 'l2sun1el_linkbank_saleinfo.SaleInfoTitle';
			$this->isPartnerAmount = 'l2sun1el_linkbank_saleinfo.isPartnerAmount';
			$this->SaleInfoAmount = 'l2sun1el_linkbank_saleinfo.SaleInfoAmount';
			$this->DateAdded = 'l2sun1el_linkbank_saleinfo.DateAdded';
			$this->userid = 'l2sun1el_linkbank_saleinfo.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_linkbank_saleinfo';
		}
	}
}