<?php
if(!class_exists('clsStructUserUpdates')){
	class clsStructUserUpdates{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $email;
		
		/**
		* @var integer
		*/
		var $mail_status;
		
		/**
		* @var integer
		*/
		var $status;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_user_updates.*';
			$this->id = 'l2sun1el_user_updates.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->email = 'l2sun1el_user_updates.email';
			$this->mail_status = 'l2sun1el_user_updates.mail_status';
			$this->status = 'l2sun1el_user_updates.status';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_user_updates';
		}
	}
}