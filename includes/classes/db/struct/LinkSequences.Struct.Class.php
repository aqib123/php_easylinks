<?php
if(!class_exists('clsStructLinkSequences')){
	class clsStructLinkSequences{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var string
		*/
		var $LinkSequenceName;
		
		/**
		* @var string
		*/
		var $LinkSequenceAccessKey;
		
		/**
		* @var integer
		*/
		var $GroupID;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $LinkSequenceStatus;
		
		/**
		* @var integer
		*/
		var $UseAdminDomain;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var string
		*/
		var $VisibleLink;
		
		/**
		* @var integer
		*/
		var $MasterCampaignID;
		
		/**
		* @var integer
		*/
		var $CloakURL;
		
		/**
		* @var integer
		*/
		var $hasSingleLink;
		
		/**
		* @var string
		*/
		var $StartDate;
		
		/**
		* @var string
		*/
		var $EndDate;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_link_sequences.*';
			$this->id = 'l2sun1el_link_sequences.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->userid = 'l2sun1el_link_sequences.userid';
			$this->LinkSequenceName = 'l2sun1el_link_sequences.LinkSequenceName';
			$this->LinkSequenceAccessKey = 'l2sun1el_link_sequences.LinkSequenceAccessKey';
			$this->GroupID = 'l2sun1el_link_sequences.GroupID';
			$this->DateAdded = 'l2sun1el_link_sequences.DateAdded';
			$this->LinkSequenceStatus = 'l2sun1el_link_sequences.LinkSequenceStatus';
			$this->UseAdminDomain = 'l2sun1el_link_sequences.UseAdminDomain';
			$this->DomainID = 'l2sun1el_link_sequences.DomainID';
			$this->VisibleLink = 'l2sun1el_link_sequences.VisibleLink';
			$this->MasterCampaignID = 'l2sun1el_link_sequences.MasterCampaignID';
			$this->CloakURL = 'l2sun1el_link_sequences.CloakURL';
			$this->hasSingleLink = 'l2sun1el_link_sequences.hasSingleLink';
			$this->StartDate = 'l2sun1el_link_sequences.StartDate';
			$this->EndDate = 'l2sun1el_link_sequences.EndDate';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_link_sequences';
		}
	}
}