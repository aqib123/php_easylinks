<?php
if(!class_exists('clsStructPaymentLog')){
	class clsStructPaymentLog{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $CustomerName;
		
		/**
		* @var string
		*/
		var $CustomerEmailAddress;
		
		/**
		* @var string
		*/
		var $CustomerProductID;
		
		/**
		* @var string
		*/
		var $CustomCountryCode;
		
		/**
		* @var string
		*/
		var $CustomerProductType;
		
		/**
		* @var string
		*/
		var $TransactionType;
		
		/**
		* @var string
		*/
		var $isAffiliateTransaction;
		
		/**
		* @var string
		*/
		var $TotalSaleAmount;
		
		/**
		* @var string
		*/
		var $UpSaleAmount;
		
		/**
		* @var string
		*/
		var $PaymentMethod;
		
		/**
		* @var string
		*/
		var $VendorID;
		
		/**
		* @var string
		*/
		var $PaymentID;
		
		/**
		* @var string
		*/
		var $UpSellPaymentID;
		
		/**
		* @var string
		*/
		var $AffiliateID;
		
		/**
		* @var string
		*/
		var $VerificationKey;
		
		/**
		* @var string
		*/
		var $TransactionTime;
		
		/**
		* @var integer
		*/
		var $UserID;
		
		/**
		* @var string
		*/
		var $username;
		
		/**
		* @var string
		*/
		var $password;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var string
		*/
		var $LevelAccessKey;
		
		/**
		* @var integer
		*/
		var $active_status;
		
		/**
		* @var integer
		*/
		var $sent_license_key;
		
		/**
		* @var string
		*/
		var $PaymentData;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_payment_log.*';
			$this->id = 'l2sun1el_payment_log.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->CustomerName = 'l2sun1el_payment_log.CustomerName';
			$this->CustomerEmailAddress = 'l2sun1el_payment_log.CustomerEmailAddress';
			$this->CustomerProductID = 'l2sun1el_payment_log.CustomerProductID';
			$this->CustomCountryCode = 'l2sun1el_payment_log.CustomCountryCode';
			$this->CustomerProductType = 'l2sun1el_payment_log.CustomerProductType';
			$this->TransactionType = 'l2sun1el_payment_log.TransactionType';
			$this->isAffiliateTransaction = 'l2sun1el_payment_log.isAffiliateTransaction';
			$this->TotalSaleAmount = 'l2sun1el_payment_log.TotalSaleAmount';
			$this->UpSaleAmount = 'l2sun1el_payment_log.UpSaleAmount';
			$this->PaymentMethod = 'l2sun1el_payment_log.PaymentMethod';
			$this->VendorID = 'l2sun1el_payment_log.VendorID';
			$this->PaymentID = 'l2sun1el_payment_log.PaymentID';
			$this->UpSellPaymentID = 'l2sun1el_payment_log.UpSellPaymentID';
			$this->AffiliateID = 'l2sun1el_payment_log.AffiliateID';
			$this->VerificationKey = 'l2sun1el_payment_log.VerificationKey';
			$this->TransactionTime = 'l2sun1el_payment_log.TransactionTime';
			$this->UserID = 'l2sun1el_payment_log.UserID';
			$this->username = 'l2sun1el_payment_log.username';
			$this->password = 'l2sun1el_payment_log.password';
			$this->LevelID = 'l2sun1el_payment_log.LevelID';
			$this->LevelAccessKey = 'l2sun1el_payment_log.LevelAccessKey';
			$this->active_status = 'l2sun1el_payment_log.active_status';
			$this->sent_license_key = 'l2sun1el_payment_log.sent_license_key';
			$this->PaymentData = 'l2sun1el_payment_log.PaymentData';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_payment_log';
		}
	}
}