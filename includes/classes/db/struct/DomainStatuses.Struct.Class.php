<?php
if(!class_exists('clsStructDomainStatuses')){
	class clsStructDomainStatuses{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $DomainID;
		
		/**
		* @var integer
		*/
		var $DomainBlacklisted;
		
		/**
		* @var integer
		*/
		var $DomainServerError;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $LastCheckDate;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_domain_statuses.*';
			$this->id = 'l2sun1el_domain_statuses.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->DomainID = 'l2sun1el_domain_statuses.DomainID';
			$this->DomainBlacklisted = 'l2sun1el_domain_statuses.DomainBlacklisted';
			$this->DomainServerError = 'l2sun1el_domain_statuses.DomainServerError';
			$this->DateAdded = 'l2sun1el_domain_statuses.DateAdded';
			$this->LastCheckDate = 'l2sun1el_domain_statuses.LastCheckDate';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_domain_statuses';
		}
	}
}