<?php
if(!class_exists('clsStructLevelFeatures')){
	class clsStructLevelFeatures{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var integer
		*/
		var $FeatureID;
		
		/**
		* @var integer
		*/
		var $LevelFeatureEnabled;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_level_features.*';
			$this->id = 'l2sun1el_level_features.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LevelID = 'l2sun1el_level_features.LevelID';
			$this->FeatureID = 'l2sun1el_level_features.FeatureID';
			$this->LevelFeatureEnabled = 'l2sun1el_level_features.LevelFeatureEnabled';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_level_features';
		}
	}
}