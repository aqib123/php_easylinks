<?php
if(!class_exists('clsStructLevelMenuItems')){
	class clsStructLevelMenuItems{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LevelID;
		
		/**
		* @var integer
		*/
		var $MenuItemID;
		
		/**
		* @var integer
		*/
		var $LevelMenuItemEnabled;
		
		/**
		* @var integer
		*/
		var $LevelMenuItemLocked;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_level_menu_items.*';
			$this->id = 'l2sun1el_level_menu_items.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LevelID = 'l2sun1el_level_menu_items.LevelID';
			$this->MenuItemID = 'l2sun1el_level_menu_items.MenuItemID';
			$this->LevelMenuItemEnabled = 'l2sun1el_level_menu_items.LevelMenuItemEnabled';
			$this->LevelMenuItemLocked = 'l2sun1el_level_menu_items.LevelMenuItemLocked';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_level_menu_items';
		}
	}
}