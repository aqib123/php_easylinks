<?php
if(!class_exists('clsStructLinkbankClicks')){
	class clsStructLinkbankClicks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LinkBankID;
		
		/**
		* @var string
		*/
		var $LinkType;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ClickType;
		
		/**
		* @var string
		*/
		var $ClickIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_linkbank_clicks.*';
			$this->id = 'l2sun1el_linkbank_clicks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LinkBankID = 'l2sun1el_linkbank_clicks.LinkBankID';
			$this->LinkType = 'l2sun1el_linkbank_clicks.LinkType';
			$this->DateAdded = 'l2sun1el_linkbank_clicks.DateAdded';
			$this->ClickType = 'l2sun1el_linkbank_clicks.ClickType';
			$this->ClickIp = 'l2sun1el_linkbank_clicks.ClickIp';
			$this->CountryCode = 'l2sun1el_linkbank_clicks.CountryCode';
			$this->BrowserName = 'l2sun1el_linkbank_clicks.BrowserName';
			$this->BrowserVersion = 'l2sun1el_linkbank_clicks.BrowserVersion';
			$this->Platform = 'l2sun1el_linkbank_clicks.Platform';
			$this->BotName = 'l2sun1el_linkbank_clicks.BotName';
			$this->EmailID = 'l2sun1el_linkbank_clicks.EmailID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_linkbank_clicks';
		}
	}
}