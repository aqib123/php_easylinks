<?php
if(!class_exists('clsStructUserLogins')){
	class clsStructUserLogins{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $UserID;
		
		/**
		* @var string
		*/
		var $IPAddress;
		
		/**
		* @var string
		*/
		var $LoginDateTime;
		
		/**
		* @var string
		*/
		var $LoginType;
		
		/**
		* @var string
		*/
		var $LoginReference;
		
		/**
		* @var string
		*/
		var $created;
		
		/**
		* @var string
		*/
		var $modified;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_user_logins.*';
			$this->id = 'l2sun1el_user_logins.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->UserID = 'l2sun1el_user_logins.UserID';
			$this->IPAddress = 'l2sun1el_user_logins.IPAddress';
			$this->LoginDateTime = 'l2sun1el_user_logins.LoginDateTime';
			$this->LoginType = 'l2sun1el_user_logins.LoginType';
			$this->LoginReference = 'l2sun1el_user_logins.LoginReference';
			$this->created = 'l2sun1el_user_logins.created';
			$this->modified = 'l2sun1el_user_logins.modified';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_user_logins';
		}
	}
}