<?php
if(!class_exists('clsStructManualsaleSplitPartnerPayments')){
	class clsStructManualsaleSplitPartnerPayments{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $SenderManualSaleSplitPartnerID;
		
		/**
		* @var integer
		*/
		var $ReceiverManualSaleSplitPartnerID;
		
		/**
		* @var string
		*/
		var $SentAmount;
		
		/**
		* @var string
		*/
		var $ReferenceNo;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var integer
		*/
		var $userid;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_manualsale_split_partner_payments.*';
			$this->id = 'l2sun1el_manualsale_split_partner_payments.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->SenderManualSaleSplitPartnerID = 'l2sun1el_manualsale_split_partner_payments.SenderManualSaleSplitPartnerID';
			$this->ReceiverManualSaleSplitPartnerID = 'l2sun1el_manualsale_split_partner_payments.ReceiverManualSaleSplitPartnerID';
			$this->SentAmount = 'l2sun1el_manualsale_split_partner_payments.SentAmount';
			$this->ReferenceNo = 'l2sun1el_manualsale_split_partner_payments.ReferenceNo';
			$this->DateAdded = 'l2sun1el_manualsale_split_partner_payments.DateAdded';
			$this->userid = 'l2sun1el_manualsale_split_partner_payments.userid';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_manualsale_split_partner_payments';
		}
	}
}