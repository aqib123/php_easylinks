<?php
if(!class_exists('clsStructDomains')){
	class clsStructDomains{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $DomainName;
		
		/**
		* @var string
		*/
		var $DomainSlug;
		
		/**
		* @var string
		*/
		var $DomainUrl;
		
		/**
		* @var string
		*/
		var $DefaultURL;
		
		/**
		* @var string
		*/
		var $RetargetingPixel;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $DomainType;
		
		/**
		* @var integer
		*/
		var $DomainForward;
		
		/**
		* @var string
		*/
		var $DomainAccessKey;
		
		/**
		* @var integer
		*/
		var $userid;
		
		/**
		* @var integer
		*/
		var $DefaultCloakURL;
		
		/**
		* @var string
		*/
		var $DateUpdated;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_domains.*';
			$this->id = 'l2sun1el_domains.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->DomainName = 'l2sun1el_domains.DomainName';
			$this->DomainSlug = 'l2sun1el_domains.DomainSlug';
			$this->DomainUrl = 'l2sun1el_domains.DomainUrl';
			$this->DefaultURL = 'l2sun1el_domains.DefaultURL';
			$this->RetargetingPixel = 'l2sun1el_domains.RetargetingPixel';
			$this->DateAdded = 'l2sun1el_domains.DateAdded';
			$this->DomainType = 'l2sun1el_domains.DomainType';
			$this->DomainForward = 'l2sun1el_domains.DomainForward';
			$this->DomainAccessKey = 'l2sun1el_domains.DomainAccessKey';
			$this->userid = 'l2sun1el_domains.userid';
			$this->DefaultCloakURL = 'l2sun1el_domains.DefaultCloakURL';
			$this->DateUpdated = 'l2sun1el_domains.DateUpdated';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_domains';
		}
	}
}