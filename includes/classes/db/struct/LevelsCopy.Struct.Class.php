<?php
if(!class_exists('clsStructLevelsCopy')){
	class clsStructLevelsCopy{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var string
		*/
		var $LevelName;
		
		/**
		* @var integer
		*/
		var $LevelEnabled;
		
		/**
		* @var integer
		*/
		var $LevelDefault;
		
		/**
		* @var string
		*/
		var $LevelLogo;
		
		/**
		* @var string
		*/
		var $LevelAccessKey;
		
		/**
		* @var integer
		*/
		var $created_userid;
		
		/**
		* @var string
		*/
		var $created_date_time;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_levels_copy.*';
			$this->id = 'l2sun1el_levels_copy.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LevelName = 'l2sun1el_levels_copy.LevelName';
			$this->LevelEnabled = 'l2sun1el_levels_copy.LevelEnabled';
			$this->LevelDefault = 'l2sun1el_levels_copy.LevelDefault';
			$this->LevelLogo = 'l2sun1el_levels_copy.LevelLogo';
			$this->LevelAccessKey = 'l2sun1el_levels_copy.LevelAccessKey';
			$this->created_userid = 'l2sun1el_levels_copy.created_userid';
			$this->created_date_time = 'l2sun1el_levels_copy.created_date_time';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_levels_copy';
		}
	}
}