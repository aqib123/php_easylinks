<?php
if(!class_exists('clsStructLinkSequenceClicks')){
	class clsStructLinkSequenceClicks{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $LinkSequenceLinkID;
		
		/**
		* @var string
		*/
		var $LinkSequenceLinkType;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ClickType;
		
		/**
		* @var string
		*/
		var $ClickIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_link_sequence_clicks.*';
			$this->id = 'l2sun1el_link_sequence_clicks.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->LinkSequenceLinkID = 'l2sun1el_link_sequence_clicks.LinkSequenceLinkID';
			$this->LinkSequenceLinkType = 'l2sun1el_link_sequence_clicks.LinkSequenceLinkType';
			$this->DateAdded = 'l2sun1el_link_sequence_clicks.DateAdded';
			$this->ClickType = 'l2sun1el_link_sequence_clicks.ClickType';
			$this->ClickIp = 'l2sun1el_link_sequence_clicks.ClickIp';
			$this->CountryCode = 'l2sun1el_link_sequence_clicks.CountryCode';
			$this->BrowserName = 'l2sun1el_link_sequence_clicks.BrowserName';
			$this->BrowserVersion = 'l2sun1el_link_sequence_clicks.BrowserVersion';
			$this->Platform = 'l2sun1el_link_sequence_clicks.Platform';
			$this->BotName = 'l2sun1el_link_sequence_clicks.BotName';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_link_sequence_clicks';
		}
	}
}