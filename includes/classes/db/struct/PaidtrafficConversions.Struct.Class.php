<?php
if(!class_exists('clsStructPaidtrafficConversions')){
	class clsStructPaidtrafficConversions{
		/** [Fields Declaration Start] **/
		var $Asterisk;
		public static $Lastid;
		var $PrimaryKey;
		
		/**
		* @var integer
		*/
		var $id;
		
		/**
		* @var integer
		*/
		var $PaidTrafficID;
		
		/**
		* @var string
		*/
		var $DateAdded;
		
		/**
		* @var string
		*/
		var $ConversionType;
		
		/**
		* @var string
		*/
		var $UnitPrice;
		
		/**
		* @var string
		*/
		var $ConversionIp;
		
		/**
		* @var string
		*/
		var $CountryCode;
		
		/**
		* @var string
		*/
		var $BrowserName;
		
		/**
		* @var string
		*/
		var $BrowserVersion;
		
		/**
		* @var string
		*/
		var $Platform;
		
		/**
		* @var string
		*/
		var $BotName;
		
		/**
		* @var integer
		*/
		var $EmailID;
		
		/**
		* @var integer
		*/
		var $PaidTrafficConversionPixelID;
		/** [Fields Declaration End] **/

		function __construct(){
			/** [Fields Initialization Start] **/
			$this->Asterisk = 'l2sun1el_paidtraffic_conversions.*';
			$this->id = 'l2sun1el_paidtraffic_conversions.id';
		/**
		* @var integer
		*/
		
			$this->PrimaryKey = 'id';
			$this->PaidTrafficID = 'l2sun1el_paidtraffic_conversions.PaidTrafficID';
			$this->DateAdded = 'l2sun1el_paidtraffic_conversions.DateAdded';
			$this->ConversionType = 'l2sun1el_paidtraffic_conversions.ConversionType';
			$this->UnitPrice = 'l2sun1el_paidtraffic_conversions.UnitPrice';
			$this->ConversionIp = 'l2sun1el_paidtraffic_conversions.ConversionIp';
			$this->CountryCode = 'l2sun1el_paidtraffic_conversions.CountryCode';
			$this->BrowserName = 'l2sun1el_paidtraffic_conversions.BrowserName';
			$this->BrowserVersion = 'l2sun1el_paidtraffic_conversions.BrowserVersion';
			$this->Platform = 'l2sun1el_paidtraffic_conversions.Platform';
			$this->BotName = 'l2sun1el_paidtraffic_conversions.BotName';
			$this->EmailID = 'l2sun1el_paidtraffic_conversions.EmailID';
			$this->PaidTrafficConversionPixelID = 'l2sun1el_paidtraffic_conversions.PaidTrafficConversionPixelID';
			/** [Fields Initialization End] **/
		}

		function __destruct(){
		}

		function __toString(){
			return 'l2sun1el_paidtraffic_conversions';
		}
	}
}