<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructPaymentLog'))
	include_once 'struct/PaymentLog.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBPaymentLog')){
	class clsDBPaymentLog extends clsStructPaymentLog {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            parent::__construct();

            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_last_id(){
            return clsDBPaymentLog::$Lastid;
        }

        function set_last_id($Lastid){
            clsDBPaymentLog::$Lastid = $Lastid;
        }

        /**
         * Summary of get_typed_row
         * @param mixed $Row
         * @return clsDBPaymentLog
         */
        function get_typed_row($Row){
            return $Row;
        }

        /**
         * Summary of get_typed_rows
         * @param mixed $Rows
         * @return clsDBPaymentLog[]
         */
        function get_typed_rows($Rows){
            return $Rows;
        }

        function get_var($FieldValue, $FieldName, $ReturnField = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->payment_log.".") == false ? DB()->payment_log.".".$OrderBy: $OrderBy;
            $sql = "select ".$ReturnField." from ".DB()->payment_log." where ".$FieldName." = '".$FieldValue."' ORDER BY ".$OrderBy." ".$OrderType;;
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->payment_log->Asterisk." from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get_by_key($id){
            $Condition = DB()->payment_log->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBPaymentLog|null
         */
        function get_row($id = null, $UserID = null, $CustomerName = null, $CustomerEmailAddress = null, $CustomerProductID = null, $CustomCountryCode = null, $CustomerProductType = null, $TransactionType = null, $isAffiliateTransaction = null, $TotalSaleAmount = null, $UpSaleAmount = null, $PaymentMethod = null, $VendorID = null, $PaymentID = null, $UpSellPaymentID = null, $AffiliateID = null, $VerificationKey = null, $TransactionTime = null, $username = null, $password = null, $LevelID = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null, $PaymentData = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($UserID !== null) $data["UserID"] = $UserID;
			if($CustomerName !== null) $data["CustomerName"] = $CustomerName;
			if($CustomerEmailAddress !== null) $data["CustomerEmailAddress"] = $CustomerEmailAddress;
			if($CustomerProductID !== null) $data["CustomerProductID"] = $CustomerProductID;
			if($CustomCountryCode !== null) $data["CustomCountryCode"] = $CustomCountryCode;
			if($CustomerProductType !== null) $data["CustomerProductType"] = $CustomerProductType;
			if($TransactionType !== null) $data["TransactionType"] = $TransactionType;
			if($isAffiliateTransaction !== null) $data["isAffiliateTransaction"] = $isAffiliateTransaction;
			if($TotalSaleAmount !== null) $data["TotalSaleAmount"] = $TotalSaleAmount;
			if($UpSaleAmount !== null) $data["UpSaleAmount"] = $UpSaleAmount;
			if($PaymentMethod !== null) $data["PaymentMethod"] = $PaymentMethod;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaymentID !== null) $data["PaymentID"] = $PaymentID;
			if($UpSellPaymentID !== null) $data["UpSellPaymentID"] = $UpSellPaymentID;
			if($AffiliateID !== null) $data["AffiliateID"] = $AffiliateID;
			if($VerificationKey !== null) $data["VerificationKey"] = $VerificationKey;
			if($TransactionTime !== null) $data["TransactionTime"] = $TransactionTime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;
			if($PaymentData !== null) $data["PaymentData"] = $PaymentData;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->payment_log->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBPaymentLog[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->payment_log.".") == false ? DB()->payment_log.".".$OrderBy: $OrderBy;
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->payment_log->Asterisk." from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->payment_log.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->payment_log->id.") from ".DB()->payment_log;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBPaymentLog[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc", $Having = ""){
            $sql = stripos($Condition, "select ") === false ? " from ".DB()->payment_log : $Condition;
            $Condition =  stripos($Condition, "select ") === false ? $Condition : "";
            $FilterQuery = array();
            //$OrderBy = stripos($OrderBy, DB()->payment_log.".") == false ? DB()->payment_log.".".$OrderBy: $OrderBy;

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    if(is_array($Filter) && SYS()->is_string($Filter['condition']) && SYS()->is_valid($Filter['value'])){
                        $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                    }
                }
            }

            $Condition = $Condition.(($Condition != "" && count($FilterQuery) > 0? " and " : "").implode(" and ", $FilterQuery));

            if(stripos($Condition, "where") === false && $Condition != "")
                $sql .= " where ".$Condition;

            if(stripos($sql, "select ") === false){
                $sqlCount = "select count(".DB()->payment_log->id.") ".$sql;
                $sql = "select ".DB()->payment_log->Asterisk.$sql." group by ".DB()->payment_log.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                if(stripos($sql, '{from_start}') !== false){
                    $sqlCount = "select count(".DB()->payment_log->id.") ".substr($sql, stripos($sql, "{from_start}"));
                } else {
                    $sqlCount = "select count(".DB()->payment_log->id.") ".substr($sql, stripos($sql, " from"));
                }
                $sql = $sql." ".$Condition." group by ".DB()->payment_log.".".$GroupBy.($Having != "" ? " having (".$Having.") " : "");//." ORDER BY ".$OrderBy." ".$OrderType;
            }

            if(stripos($sql, '{from_start}') !== false){
                $sql = str_ireplace('{from_start}', '', $sql);
                $sqlCount = "select count(*) from (".$sql.") as tbl";
                $sql = "select * from (".$sql.") as tbl";
            }

            $sql .=  " ORDER BY ".$OrderBy." ".$OrderType;

            if(stripos($sqlCount, '{from_start}') !== false)
                $sqlCount = str_ireplace('{from_start}', '', $sqlCount);

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($UserID = null, $CustomerName = null, $CustomerEmailAddress = null, $CustomerProductID = null, $CustomCountryCode = null, $CustomerProductType = null, $TransactionType = null, $isAffiliateTransaction = null, $TotalSaleAmount = null, $UpSaleAmount = null, $PaymentMethod = null, $VendorID = null, $PaymentID = null, $UpSellPaymentID = null, $AffiliateID = null, $VerificationKey = null, $TransactionTime = null, $username = null, $password = null, $LevelID = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null, $PaymentData = null){
            $data = array();
            $Inserted = false;

            if($UserID !== null) $data["UserID"] = $UserID;
			if($CustomerName !== null) $data["CustomerName"] = $CustomerName;
			if($CustomerEmailAddress !== null) $data["CustomerEmailAddress"] = $CustomerEmailAddress;
			if($CustomerProductID !== null) $data["CustomerProductID"] = $CustomerProductID;
			if($CustomCountryCode !== null) $data["CustomCountryCode"] = $CustomCountryCode;
			if($CustomerProductType !== null) $data["CustomerProductType"] = $CustomerProductType;
			if($TransactionType !== null) $data["TransactionType"] = $TransactionType;
			if($isAffiliateTransaction !== null) $data["isAffiliateTransaction"] = $isAffiliateTransaction;
			if($TotalSaleAmount !== null) $data["TotalSaleAmount"] = $TotalSaleAmount;
			if($UpSaleAmount !== null) $data["UpSaleAmount"] = $UpSaleAmount;
			if($PaymentMethod !== null) $data["PaymentMethod"] = $PaymentMethod;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaymentID !== null) $data["PaymentID"] = $PaymentID;
			if($UpSellPaymentID !== null) $data["UpSellPaymentID"] = $UpSellPaymentID;
			if($AffiliateID !== null) $data["AffiliateID"] = $AffiliateID;
			if($VerificationKey !== null) $data["VerificationKey"] = $VerificationKey;
			if($TransactionTime !== null) $data["TransactionTime"] = $TransactionTime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;
			if($PaymentData !== null) $data["PaymentData"] = $PaymentData;

            $this->set_last_id(0);
            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->payment_log, $data);
                if($Inserted){
                    $this->set_last_id(DB()->insert_id);
                }
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $UserID = null, $CustomerName = null, $CustomerEmailAddress = null, $CustomerProductID = null, $CustomCountryCode = null, $CustomerProductType = null, $TransactionType = null, $isAffiliateTransaction = null, $TotalSaleAmount = null, $UpSaleAmount = null, $PaymentMethod = null, $VendorID = null, $PaymentID = null, $UpSellPaymentID = null, $AffiliateID = null, $VerificationKey = null, $TransactionTime = null, $username = null, $password = null, $LevelID = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null, $PaymentData = null){
            $data = array();
            $Updated = false;

            if($UserID !== null) $data["UserID"] = $UserID;
			if($CustomerName !== null) $data["CustomerName"] = $CustomerName;
			if($CustomerEmailAddress !== null) $data["CustomerEmailAddress"] = $CustomerEmailAddress;
			if($CustomerProductID !== null) $data["CustomerProductID"] = $CustomerProductID;
			if($CustomCountryCode !== null) $data["CustomCountryCode"] = $CustomCountryCode;
			if($CustomerProductType !== null) $data["CustomerProductType"] = $CustomerProductType;
			if($TransactionType !== null) $data["TransactionType"] = $TransactionType;
			if($isAffiliateTransaction !== null) $data["isAffiliateTransaction"] = $isAffiliateTransaction;
			if($TotalSaleAmount !== null) $data["TotalSaleAmount"] = $TotalSaleAmount;
			if($UpSaleAmount !== null) $data["UpSaleAmount"] = $UpSaleAmount;
			if($PaymentMethod !== null) $data["PaymentMethod"] = $PaymentMethod;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaymentID !== null) $data["PaymentID"] = $PaymentID;
			if($UpSellPaymentID !== null) $data["UpSellPaymentID"] = $UpSellPaymentID;
			if($AffiliateID !== null) $data["AffiliateID"] = $AffiliateID;
			if($VerificationKey !== null) $data["VerificationKey"] = $VerificationKey;
			if($TransactionTime !== null) $data["TransactionTime"] = $TransactionTime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;
			if($PaymentData !== null) $data["PaymentData"] = $PaymentData;

            $this->set_last_id(0);
            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->payment_log, $data, array("id" => $id));
                $this->set_last_id($id);
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $UserID = null, $CustomerName = null, $CustomerEmailAddress = null, $CustomerProductID = null, $CustomCountryCode = null, $CustomerProductType = null, $TransactionType = null, $isAffiliateTransaction = null, $TotalSaleAmount = null, $UpSaleAmount = null, $PaymentMethod = null, $VendorID = null, $PaymentID = null, $UpSellPaymentID = null, $AffiliateID = null, $VerificationKey = null, $TransactionTime = null, $username = null, $password = null, $LevelID = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null, $PaymentData = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($UserID !== null) $data["UserID"] = $UserID;
			if($CustomerName !== null) $data["CustomerName"] = $CustomerName;
			if($CustomerEmailAddress !== null) $data["CustomerEmailAddress"] = $CustomerEmailAddress;
			if($CustomerProductID !== null) $data["CustomerProductID"] = $CustomerProductID;
			if($CustomCountryCode !== null) $data["CustomCountryCode"] = $CustomCountryCode;
			if($CustomerProductType !== null) $data["CustomerProductType"] = $CustomerProductType;
			if($TransactionType !== null) $data["TransactionType"] = $TransactionType;
			if($isAffiliateTransaction !== null) $data["isAffiliateTransaction"] = $isAffiliateTransaction;
			if($TotalSaleAmount !== null) $data["TotalSaleAmount"] = $TotalSaleAmount;
			if($UpSaleAmount !== null) $data["UpSaleAmount"] = $UpSaleAmount;
			if($PaymentMethod !== null) $data["PaymentMethod"] = $PaymentMethod;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($PaymentID !== null) $data["PaymentID"] = $PaymentID;
			if($UpSellPaymentID !== null) $data["UpSellPaymentID"] = $UpSellPaymentID;
			if($AffiliateID !== null) $data["AffiliateID"] = $AffiliateID;
			if($VerificationKey !== null) $data["VerificationKey"] = $VerificationKey;
			if($TransactionTime !== null) $data["TransactionTime"] = $TransactionTime;
			if($username !== null) $data["username"] = $username;
			if($password !== null) $data["password"] = $password;
			if($LevelID !== null) $data["LevelID"] = $LevelID;
			if($LevelAccessKey !== null) $data["LevelAccessKey"] = $LevelAccessKey;
			if($active_status !== null) $data["active_status"] = $active_status;
			if($sent_license_key !== null) $data["sent_license_key"] = $sent_license_key;
			if($PaymentData !== null) $data["PaymentData"] = $PaymentData;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->payment_log, $data);
            }

            return $Deleted;
        }

        function save($Condition, $UserID = null, $CustomerName = null, $CustomerEmailAddress = null, $CustomerProductID = null, $CustomCountryCode = null, $CustomerProductType = null, $TransactionType = null, $isAffiliateTransaction = null, $TotalSaleAmount = null, $UpSaleAmount = null, $PaymentMethod = null, $VendorID = null, $PaymentID = null, $UpSellPaymentID = null, $AffiliateID = null, $VerificationKey = null, $TransactionTime = null, $username = null, $password = null, $LevelID = null, $LevelAccessKey = null, $active_status = null, $sent_license_key = null, $PaymentData = null){
            $DBRow = $this->get($Condition);
            $Saved = false;
            if(!$DBRow){
                $Saved = $this->insert($UserID, $CustomerName, $CustomerEmailAddress, $CustomerProductID, $CustomCountryCode, $CustomerProductType, $TransactionType, $isAffiliateTransaction, $TotalSaleAmount, $UpSaleAmount, $PaymentMethod, $VendorID, $PaymentID, $UpSellPaymentID, $AffiliateID, $VerificationKey, $TransactionTime, $username, $password, $LevelID, $LevelAccessKey, $active_status, $sent_license_key, $PaymentData);
            } else {
                $this->update($DBRow->id, $UserID, $CustomerName, $CustomerEmailAddress, $CustomerProductID, $CustomCountryCode, $CustomerProductType, $TransactionType, $isAffiliateTransaction, $TotalSaleAmount, $UpSaleAmount, $PaymentMethod, $VendorID, $PaymentID, $UpSellPaymentID, $AffiliateID, $VerificationKey, $TransactionTime, $username, $password, $LevelID, $LevelAccessKey, $active_status, $sent_license_key, $PaymentData);
                $Saved = true;
            }

            return $Saved;
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBPaymentLog')){
	function DBPaymentLog($obj = null) {
		return clsDBPaymentLog::instance($obj);
	}
}