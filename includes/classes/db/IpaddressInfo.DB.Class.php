<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructIpaddressInfo'))
	include_once 'struct/IpaddressInfo.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBIpaddressInfo')){
	class clsDBIpaddressInfo extends clsStructIpaddressInfo {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->ip_start != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            parent::__construct();

            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->ip_start))
                    $this->PrimaryKey = $obj->ip_start;
			}
		}

        function get_last_ip_start(){
            return clsDBIpaddressInfo::$Lastip_start;
        }

        function set_last_ip_start($Lastip_start){
            clsDBIpaddressInfo::$Lastip_start = $Lastip_start;
        }

        /**
         * Summary of get_typed_row
         * @param mixed $Row
         * @return clsDBIpaddressInfo
         */
        function get_typed_row($Row){
            return $Row;
        }

        /**
         * Summary of get_typed_rows
         * @param mixed $Rows
         * @return clsDBIpaddressInfo[]
         */
        function get_typed_rows($Rows){
            return $Rows;
        }

        function get_var($FieldValue, $FieldName, $ReturnField = "ip_start", $OrderBy = "ip_start", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->ipaddress_info.".") == false ? DB()->ipaddress_info.".".$OrderBy: $OrderBy;
            $sql = "select ".$ReturnField." from ".DB()->ipaddress_info." where ".$FieldName." = '".$FieldValue."' ORDER BY ".$OrderBy." ".$OrderType;;
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBIpaddressInfo|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->ipaddress_info->Asterisk." from ".DB()->ipaddress_info;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBIpaddressInfo|null
         */
        function get_by_key($ip_start){
            $Condition = DB()->ipaddress_info->ip_start." = ".$ip_start;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBIpaddressInfo|null
         */
        function get_row($ip_start = null, $addr_type = null, $ip_end = null, $country = null, $stateprov = null, $city = null, $latitude = null, $longitude = null, $timezone_offset = null, $timezone_name = null, $isp_name = null, $connection_type = null, $organization_name = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($ip_start) $data["ip_start"] = $ip_start;
            if($addr_type !== null) $data["addr_type"] = $addr_type;
			if($ip_end !== null) $data["ip_end"] = $ip_end;
			if($country !== null) $data["country"] = $country;
			if($stateprov !== null) $data["stateprov"] = $stateprov;
			if($city !== null) $data["city"] = $city;
			if($latitude !== null) $data["latitude"] = $latitude;
			if($longitude !== null) $data["longitude"] = $longitude;
			if($timezone_offset !== null) $data["timezone_offset"] = $timezone_offset;
			if($timezone_name !== null) $data["timezone_name"] = $timezone_name;
			if($isp_name !== null) $data["isp_name"] = $isp_name;
			if($connection_type !== null) $data["connection_type"] = $connection_type;
			if($organization_name !== null) $data["organization_name"] = $organization_name;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->ipaddress_info->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBIpaddressInfo[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "ip_start", $OrderBy = "ip_start", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->ipaddress_info.".") == false ? DB()->ipaddress_info.".".$OrderBy: $OrderBy;
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->ipaddress_info->Asterisk." from ".DB()->ipaddress_info;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->ipaddress_info.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->ipaddress_info->ip_start.") from ".DB()->ipaddress_info;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBIpaddressInfo[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "ip_start", $OrderBy = "ip_start", $OrderType = "asc", $Having = ""){
            $sql = stripos($Condition, "select ") === false ? " from ".DB()->ipaddress_info : $Condition;
            $Condition =  stripos($Condition, "select ") === false ? $Condition : "";
            $FilterQuery = array();
            //$OrderBy = stripos($OrderBy, DB()->ipaddress_info.".") == false ? DB()->ipaddress_info.".".$OrderBy: $OrderBy;

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    if(is_array($Filter) && SYS()->is_string($Filter['condition']) && SYS()->is_valid($Filter['value'])){
                        $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                    }
                }
            }

            $Condition = $Condition.(($Condition != "" && count($FilterQuery) > 0? " and " : "").implode(" and ", $FilterQuery));

            if(stripos($Condition, "where") === false && $Condition != "")
                $sql .= " where ".$Condition;

            if(stripos($sql, "select ") === false){
                $sqlCount = "select count(".DB()->ipaddress_info->ip_start.") ".$sql;
                $sql = "select ".DB()->ipaddress_info->Asterisk.$sql." group by ".DB()->ipaddress_info.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                if(stripos($sql, '{from_start}') !== false){
                    $sqlCount = "select count(".DB()->ipaddress_info->ip_start.") ".substr($sql, stripos($sql, "{from_start}"));
                } else {
                    $sqlCount = "select count(".DB()->ipaddress_info->ip_start.") ".substr($sql, stripos($sql, " from"));
                }
                $sql = $sql." ".$Condition." group by ".DB()->ipaddress_info.".".$GroupBy.($Having != "" ? " having (".$Having.") " : "");//." ORDER BY ".$OrderBy." ".$OrderType;
            }

            if(stripos($sql, '{from_start}') !== false){
                $sql = str_ireplace('{from_start}', '', $sql);
                $sqlCount = "select count(*) from (".$sql.") as tbl";
                $sql = "select * from (".$sql.") as tbl";
            }

            $sql .=  " ORDER BY ".$OrderBy." ".$OrderType;

            if(stripos($sqlCount, '{from_start}') !== false)
                $sqlCount = str_ireplace('{from_start}', '', $sqlCount);

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($addr_type = null, $ip_end = null, $country = null, $stateprov = null, $city = null, $latitude = null, $longitude = null, $timezone_offset = null, $timezone_name = null, $isp_name = null, $connection_type = null, $organization_name = null){
            $data = array();
            $Inserted = false;

            if($addr_type !== null) $data["addr_type"] = $addr_type;
			if($ip_end !== null) $data["ip_end"] = $ip_end;
			if($country !== null) $data["country"] = $country;
			if($stateprov !== null) $data["stateprov"] = $stateprov;
			if($city !== null) $data["city"] = $city;
			if($latitude !== null) $data["latitude"] = $latitude;
			if($longitude !== null) $data["longitude"] = $longitude;
			if($timezone_offset !== null) $data["timezone_offset"] = $timezone_offset;
			if($timezone_name !== null) $data["timezone_name"] = $timezone_name;
			if($isp_name !== null) $data["isp_name"] = $isp_name;
			if($connection_type !== null) $data["connection_type"] = $connection_type;
			if($organization_name !== null) $data["organization_name"] = $organization_name;

            $this->set_last_ip_start(0);
            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->ipaddress_info, $data);
                if($Inserted){
                    $this->set_last_ip_start(DB()->insert_id);
                }
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($ip_start, $addr_type = null, $ip_end = null, $country = null, $stateprov = null, $city = null, $latitude = null, $longitude = null, $timezone_offset = null, $timezone_name = null, $isp_name = null, $connection_type = null, $organization_name = null){
            $data = array();
            $Updated = false;

            if($addr_type !== null) $data["addr_type"] = $addr_type;
			if($ip_end !== null) $data["ip_end"] = $ip_end;
			if($country !== null) $data["country"] = $country;
			if($stateprov !== null) $data["stateprov"] = $stateprov;
			if($city !== null) $data["city"] = $city;
			if($latitude !== null) $data["latitude"] = $latitude;
			if($longitude !== null) $data["longitude"] = $longitude;
			if($timezone_offset !== null) $data["timezone_offset"] = $timezone_offset;
			if($timezone_name !== null) $data["timezone_name"] = $timezone_name;
			if($isp_name !== null) $data["isp_name"] = $isp_name;
			if($connection_type !== null) $data["connection_type"] = $connection_type;
			if($organization_name !== null) $data["organization_name"] = $organization_name;

            $this->set_last_ip_start(0);
            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->ipaddress_info, $data, array("ip_start" => $ip_start));
                $this->set_last_ip_start($ip_start);
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($ip_start, $addr_type = null, $ip_end = null, $country = null, $stateprov = null, $city = null, $latitude = null, $longitude = null, $timezone_offset = null, $timezone_name = null, $isp_name = null, $connection_type = null, $organization_name = null){
            $data = array();
            $Deleted = false;

            if($ip_start) $data["ip_start"] = $ip_start;
            if($addr_type !== null) $data["addr_type"] = $addr_type;
			if($ip_end !== null) $data["ip_end"] = $ip_end;
			if($country !== null) $data["country"] = $country;
			if($stateprov !== null) $data["stateprov"] = $stateprov;
			if($city !== null) $data["city"] = $city;
			if($latitude !== null) $data["latitude"] = $latitude;
			if($longitude !== null) $data["longitude"] = $longitude;
			if($timezone_offset !== null) $data["timezone_offset"] = $timezone_offset;
			if($timezone_name !== null) $data["timezone_name"] = $timezone_name;
			if($isp_name !== null) $data["isp_name"] = $isp_name;
			if($connection_type !== null) $data["connection_type"] = $connection_type;
			if($organization_name !== null) $data["organization_name"] = $organization_name;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->ipaddress_info, $data);
            }

            return $Deleted;
        }

        function save($Condition, $addr_type = null, $ip_end = null, $country = null, $stateprov = null, $city = null, $latitude = null, $longitude = null, $timezone_offset = null, $timezone_name = null, $isp_name = null, $connection_type = null, $organization_name = null){
            $DBRow = $this->get($Condition);
            $Saved = false;
            if(!$DBRow){
                $Saved = $this->insert($addr_type, $ip_end, $country, $stateprov, $city, $latitude, $longitude, $timezone_offset, $timezone_name, $isp_name, $connection_type, $organization_name);
            } else {
                $this->update($DBRow->ip_start, $addr_type, $ip_end, $country, $stateprov, $city, $latitude, $longitude, $timezone_offset, $timezone_name, $isp_name, $connection_type, $organization_name);
                $Saved = true;
            }

            return $Saved;
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->ip_start) || $this->ip_start == null || $this->ip_start == 0;
        }

        function isNotNull(){
            return isset($this->ip_start) && $this->ip_start != null && $this->ip_start != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBIpaddressInfo')){
	function DBIpaddressInfo($obj = null) {
		return clsDBIpaddressInfo::instance($obj);
	}
}