<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructLinkbanks'))
	include_once 'struct/Linkbanks.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBLinkbanks')){
	class clsDBLinkbanks extends clsStructLinkbanks {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            parent::__construct();

            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_last_id(){
            return clsDBLinkbanks::$Lastid;
        }

        function set_last_id($Lastid){
            clsDBLinkbanks::$Lastid = $Lastid;
        }

        /**
         * Summary of get_typed_row
         * @param mixed $Row
         * @return clsDBLinkbanks
         */
        function get_typed_row($Row){
            return $Row;
        }

        /**
         * Summary of get_typed_rows
         * @param mixed $Rows
         * @return clsDBLinkbanks[]
         */
        function get_typed_rows($Rows){
            return $Rows;
        }

        function get_var($FieldValue, $FieldName, $ReturnField = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->linkbanks.".") == false ? DB()->linkbanks.".".$OrderBy: $OrderBy;
            $sql = "select ".$ReturnField." from ".DB()->linkbanks." where ".$FieldName." = '".$FieldValue."' ORDER BY ".$OrderBy." ".$OrderType;;
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbanks->Asterisk." from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get_by_key($id){
            $Condition = DB()->linkbanks->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBLinkbanks|null
         */
        function get_row($id = null, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->linkbanks->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBLinkbanks[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->linkbanks.".") == false ? DB()->linkbanks.".".$OrderBy: $OrderBy;
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->linkbanks->Asterisk." from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->linkbanks.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->linkbanks->id.") from ".DB()->linkbanks;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBLinkbanks[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc", $Having = ""){
            $sql = stripos($Condition, "select ") === false ? " from ".DB()->linkbanks : $Condition;
            $Condition =  stripos($Condition, "select ") === false ? $Condition : "";
            $FilterQuery = array();
            //$OrderBy = stripos($OrderBy, DB()->linkbanks.".") == false ? DB()->linkbanks.".".$OrderBy: $OrderBy;

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    if(is_array($Filter) && SYS()->is_string($Filter['condition']) && SYS()->is_valid($Filter['value'])){
                        $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                    }
                }
            }

            $Condition = $Condition.(($Condition != "" && count($FilterQuery) > 0? " and " : "").implode(" and ", $FilterQuery));

            if(stripos($Condition, "where") === false && $Condition != "")
                $sql .= " where ".$Condition;

            if(stripos($sql, "select ") === false){
                $sqlCount = "select count(".DB()->linkbanks->id.") ".$sql;
                $sql = "select ".DB()->linkbanks->Asterisk.$sql." group by ".DB()->linkbanks.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                if(stripos($sql, '{from_start}') !== false){
                    $sqlCount = "select count(".DB()->linkbanks->id.") ".substr($sql, stripos($sql, "{from_start}"));
                } else {
                    $sqlCount = "select count(".DB()->linkbanks->id.") ".substr($sql, stripos($sql, " from"));
                }
                $sql = $sql." ".$Condition." group by ".DB()->linkbanks.".".$GroupBy.($Having != "" ? " having (".$Having.") " : "");//." ORDER BY ".$OrderBy." ".$OrderType;
            }

            if(stripos($sql, '{from_start}') !== false){
                $sql = str_ireplace('{from_start}', '', $sql);
                $sqlCount = "select count(*) from (".$sql.") as tbl";
                $sql = "select * from (".$sql.") as tbl";
            }

            $sql .=  " ORDER BY ".$OrderBy." ".$OrderType;

            if(stripos($sqlCount, '{from_start}') !== false)
                $sqlCount = str_ireplace('{from_start}', '', $sqlCount);

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            $this->set_last_id(0);
            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->linkbanks, $data);
                if($Inserted){
                    $this->set_last_id(DB()->insert_id);
                }
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            $this->set_last_id(0);
            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->linkbanks, $data, array("id" => $id));
                $this->set_last_id($id);
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($VendorID !== null) $data["VendorID"] = $VendorID;
			if($LinkName !== null) $data["LinkName"] = $LinkName;
			if($LinkBankAccessKey !== null) $data["LinkBankAccessKey"] = $LinkBankAccessKey;
			if($DestinationURL !== null) $data["DestinationURL"] = $DestinationURL;
			if($VisibleLink !== null) $data["VisibleLink"] = $VisibleLink;
			if($UseAdminDomain !== null) $data["UseAdminDomain"] = $UseAdminDomain;
			if($DomainID !== null) $data["DomainID"] = $DomainID;
			if($GroupID !== null) $data["GroupID"] = $GroupID;
			if($StartDate !== null) $data["StartDate"] = $StartDate;
			if($EndDate !== null) $data["EndDate"] = $EndDate;
			if($AdditionalNotes !== null) $data["AdditionalNotes"] = $AdditionalNotes;
			if($PageImage !== null) $data["PageImage"] = $PageImage;
			if($LinkActive !== null) $data["LinkActive"] = $LinkActive;
			if($SalesConversions !== null) $data["SalesConversions"] = $SalesConversions;
			if($RedirectAfterLinkExpired !== null) $data["RedirectAfterLinkExpired"] = $RedirectAfterLinkExpired;
			if($PixelID !== null) $data["PixelID"] = $PixelID;
			if($TrackingCodeForActions !== null) $data["TrackingCodeForActions"] = $TrackingCodeForActions;
			if($TrackingCodeForSalesAndConversions !== null) $data["TrackingCodeForSalesAndConversions"] = $TrackingCodeForSalesAndConversions;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;
			if($UnitPriceAction !== null) $data["UnitPriceAction"] = $UnitPriceAction;
			if($UnitPriceSales !== null) $data["UnitPriceSales"] = $UnitPriceSales;
			if($LinkStatus !== null) $data["LinkStatus"] = $LinkStatus;
			if($SplitPartnerID !== null) $data["SplitPartnerID"] = $SplitPartnerID;
			if($PartnerSplit !== null) $data["PartnerSplit"] = $PartnerSplit;
			if($Commission !== null) $data["Commission"] = $Commission;
			if($AffiliatePlatformID !== null) $data["AffiliatePlatformID"] = $AffiliatePlatformID;
			if($ManualSalesEntry !== null) $data["ManualSalesEntry"] = $ManualSalesEntry;
			if($SaleQuantity !== null) $data["SaleQuantity"] = $SaleQuantity;
			if($GrossSale !== null) $data["GrossSale"] = $GrossSale;
			if($ExtraIncome !== null) $data["ExtraIncome"] = $ExtraIncome;
			if($Expenses !== null) $data["Expenses"] = $Expenses;
			if($SaleInfoEntered !== null) $data["SaleInfoEntered"] = $SaleInfoEntered;
			if($OwedToPartner !== null) $data["OwedToPartner"] = $OwedToPartner;
			if($SplitExpensesWithPartner !== null) $data["SplitExpensesWithPartner"] = $SplitExpensesWithPartner;
			if($SplitExpensesPercentage !== null) $data["SplitExpensesPercentage"] = $SplitExpensesPercentage;
			if($PartnerPaymentStatus !== null) $data["PartnerPaymentStatus"] = $PartnerPaymentStatus;
			if($PartnerPaymentDate !== null) $data["PartnerPaymentDate"] = $PartnerPaymentDate;
			if($PartnerPaymentReferenceNo !== null) $data["PartnerPaymentReferenceNo"] = $PartnerPaymentReferenceNo;
			if($TrackCommission !== null) $data["TrackCommission"] = $TrackCommission;
			if($PartnerSplitAllowed !== null) $data["PartnerSplitAllowed"] = $PartnerSplitAllowed;
			if($PendingPageID !== null) $data["PendingPageID"] = $PendingPageID;
			if($CompletePageID !== null) $data["CompletePageID"] = $CompletePageID;
			if($MasterCampaignID !== null) $data["MasterCampaignID"] = $MasterCampaignID;
			if($CloakURL !== null) $data["CloakURL"] = $CloakURL;
			if($TrackEPC !== null) $data["TrackEPC"] = $TrackEPC;
			if($RawClicks !== null) $data["RawClicks"] = $RawClicks;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->linkbanks, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $VendorID = null, $LinkName = null, $LinkBankAccessKey = null, $DestinationURL = null, $VisibleLink = null, $UseAdminDomain = null, $DomainID = null, $GroupID = null, $StartDate = null, $EndDate = null, $AdditionalNotes = null, $PageImage = null, $LinkActive = null, $SalesConversions = null, $RedirectAfterLinkExpired = null, $PixelID = null, $TrackingCodeForActions = null, $TrackingCodeForSalesAndConversions = null, $DateAdded = null, $UnitPriceAction = null, $UnitPriceSales = null, $LinkStatus = null, $SplitPartnerID = null, $PartnerSplit = null, $Commission = null, $AffiliatePlatformID = null, $ManualSalesEntry = null, $SaleQuantity = null, $GrossSale = null, $ExtraIncome = null, $Expenses = null, $SaleInfoEntered = null, $OwedToPartner = null, $SplitExpensesWithPartner = null, $SplitExpensesPercentage = null, $PartnerPaymentStatus = null, $PartnerPaymentDate = null, $PartnerPaymentReferenceNo = null, $TrackCommission = null, $PartnerSplitAllowed = null, $PendingPageID = null, $CompletePageID = null, $MasterCampaignID = null, $CloakURL = null, $TrackEPC = null, $RawClicks = null){
            $DBRow = $this->get($Condition);
            $Saved = false;
            if(!$DBRow){
                $Saved = $this->insert($userid, $VendorID, $LinkName, $LinkBankAccessKey, $DestinationURL, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $AdditionalNotes, $PageImage, $LinkActive, $SalesConversions, $RedirectAfterLinkExpired, $PixelID, $TrackingCodeForActions, $TrackingCodeForSalesAndConversions, $DateAdded, $UnitPriceAction, $UnitPriceSales, $LinkStatus, $SplitPartnerID, $PartnerSplit, $Commission, $AffiliatePlatformID, $ManualSalesEntry, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $SaleInfoEntered, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $PartnerPaymentStatus, $PartnerPaymentDate, $PartnerPaymentReferenceNo, $TrackCommission, $PartnerSplitAllowed, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $TrackEPC, $RawClicks);
            } else {
                $this->update($DBRow->id, $userid, $VendorID, $LinkName, $LinkBankAccessKey, $DestinationURL, $VisibleLink, $UseAdminDomain, $DomainID, $GroupID, $StartDate, $EndDate, $AdditionalNotes, $PageImage, $LinkActive, $SalesConversions, $RedirectAfterLinkExpired, $PixelID, $TrackingCodeForActions, $TrackingCodeForSalesAndConversions, $DateAdded, $UnitPriceAction, $UnitPriceSales, $LinkStatus, $SplitPartnerID, $PartnerSplit, $Commission, $AffiliatePlatformID, $ManualSalesEntry, $SaleQuantity, $GrossSale, $ExtraIncome, $Expenses, $SaleInfoEntered, $OwedToPartner, $SplitExpensesWithPartner, $SplitExpensesPercentage, $PartnerPaymentStatus, $PartnerPaymentDate, $PartnerPaymentReferenceNo, $TrackCommission, $PartnerSplitAllowed, $PendingPageID, $CompletePageID, $MasterCampaignID, $CloakURL, $TrackEPC, $RawClicks);
                $Saved = true;
            }

            return $Saved;
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/

        function get_columns($LinkBankSaleType = ""){
            if($LinkBankSaleType == "conv")
                $columns = ["", "", "", "LinkName", "VendorName", "RawClicks", "UniqueClicks", "ConversionsCount", "ConversionsAmount", "ConversionsPercentage", "GroupName", "CreatedDate", ""];
            else
                $columns = ["", "", "LinkName", "VendorName", "RawClicks", "UniqueClicks", "", "GroupName", "CreatedDate", ""];

            return $columns;
        }

        function get_pagged($LinkBanksType, $UserID, $LinkBankSaleType = "", $QueryData = [], $PageNo = 0, $PerPage = -1){
            $columns = $this->get_columns($LinkBankSaleType);

            $sql = "select id, LinkName, LinkStatus, VisibleLink, DomainID, GroupID, VendorID, MasterCampaignID, DateAdded, FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as CreatedDate,
                                    GrossSale, ExtraIncome, Expenses, Commission, PartnerSplit, SplitExpensesWithPartner, SplitExpensesPercentage,
                                    if(LinkStatus = 'active','Live',if(LinkStatus = 'pending','Pending', if(LinkStatus = 'complete','Completed', if(LinkStatus = 'evergreen','Evergreen', 'My Links')))) as LinkBankStatus,
	                                if(LinkStatus = 'active','fa fa-rocket',if(LinkStatus = 'pending','fa fa-hourglass-o', if(LinkStatus = 'complete','fa fa-check-square-o', if(LinkStatus = 'evergreen','fa fa-tree', 'fa fa-user')))) as LinkBankStatusIcon,
	                                (select GroupName from ".DB()->groups." where id=".DB()->linkbanks.".GroupID) as GroupName,
	                                (select VendorName from ".DB()->vendors." where id=".DB()->linkbanks.".VendorID) as VendorName,
	                                LinkBankUniqueClicks(".DB()->linkbanks.".id) as UniqueClicks,
	                                (select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".DB()->linkbanks.".id) as RawClicks,
	                                (select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='action') as Actions,
	                                (select count(*) as FireCount from ".DB()->linkbank_pixel_fires." where LinkBankID=".DB()->linkbanks.".id) as PixelFires,
	                                (select MasterCampaignName from ".DB()->master_campaigns." where id=".DB()->linkbanks.".MasterCampaignID) as MasterCampaignName,
	                                (select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='sales') as ConversionsCount,
	                                (select sum(UnitPrice) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='sales') as ConversionsAmount,
                                    LinkBankConversionPercentage((select UniqueClicks), (select ConversionsCount)) as ConversionsPercentage
                                 {from_start} from ".DB()->linkbanks." where id ".($LinkBankSaleType == ""?"not ":"")." in(select LinkBankID from ".DB()->linkbank_conversion_pixels.") and userid=".$UserID;

            if($LinkBanksType == "pending")
                $sql .= " and LinkStatus = 'pending' ";
            else if($LinkBanksType == "active")
                $sql .= " and LinkStatus = 'active' ";
            else if($LinkBanksType == "complete")
                $sql .= " and LinkStatus = 'complete' ";
            else if($LinkBanksType == "evergreen")
                $sql .= " and LinkStatus = 'evergreen' ";
            else if($LinkBanksType == "mylink")
                $sql .= " and LinkStatus = 'mylink' ";

            if(isset($QueryData["act"]) && ($QueryData["act"] == "filter_linkbank_vendor" || $QueryData["act"] == "app_filters"))
                $sql .= " and ".DB()->linkbanks.".VendorID = '".intval($QueryData['VendorID'])."' ";

            if(isset($QueryData["act"]) && ($QueryData["act"] == "filter_linkbank_group" || $QueryData["act"] == "app_filters"))
                $sql .= " and ".DB()->linkbanks.".GroupID = '".intval($QueryData['GroupID'])."' ";

            $OrderBy = SYS()->is_valid($QueryData["column_no"]) && SYS()->is_valid($columns[$QueryData["column_no"]]) ? $columns[$QueryData["column_no"]] : "DateAdded";//$columns[$LinkBankSaleType == "conv" ? 3 : 2];
            $OrderType = (SYS()->is_valid($QueryData["order_type"]) && $QueryData["order_type"] == "desc") || !SYS()->is_valid($QueryData["column_no"]) ? "desc" : "asc";

            if($OrderBy == "")
                $OrderBy = "LinkName";
            else if($OrderBy == "CreatedDate")
                $OrderBy = "DateAdded";

            //$sql .= " group by ".DB()->linkbanks.".id ";
            $HavingSQL = array();
            if(isset($QueryData["act"]) && ($QueryData["act"] == "search_linkbank" || $QueryData["act"] == "app_filters")){
                $keywords = " like '%".$QueryData["keywords"]."%' ";
                foreach ($columns as $column){
                    if(!empty($column)){
                        $HavingSQL[] = $column.$keywords;
                    }
                }
                //$sql .= " having ".implode(" or ", $HavingSQL);
            }

            //list($PerPage, $PageNo) = SYS()->get_page_info();
            //$PerPage = -1;
            $FilteredData = $this->get_filtered($QueryData, $sql, $PageNo, $PerPage, "id", $OrderBy, $OrderType, implode(" or ", $HavingSQL));

            return array($FilteredData['Rows'], $FilteredData['TotalRows']);
        }

        /**
         * Summary of get_formated
         * @param clsDBLinkbanks $LinkBank
         * @param mixed $UserID
         * @param mixed $LinkBanksType 
         * @param mixed $LinkBankSaleType 
         * @return []
         */
        function get_formated($LinkBank, $UserID, $LinkBanksType = "", $LinkBankSaleType = ""){
            $LinkbankStauses = SYS()->LinkStatusesAll;
            $GroupName = $LinkBank->GroupName;
            $VendorName = $LinkBank->VendorName;
            $LinkBankID = $LinkBank->id;

            $UniqueClicks = $LinkBank->UniqueClicks;
            $RawClicks = $LinkBank->RawClicks;

            $Actions = $LinkBank->Actions;
            //$PixelFires = $LinkBank->PixelFires;

            $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where MasterCampaignID = 0 and LinkBankID=".$LinkBankID." and userid=".$UserID);
            if($ManualSale){
                $SaleAmount = get_manualsale_amount($LinkBankID);
                $ManualSaleLink = '<a href="'.get_site_url("manual-sale/split-partners/?LinkBankID=".$LinkBankID).'" class="" title="Manual Sale">$'.$SaleAmount['TotalAmount'].'</a>';
            } else {
                $ManualSaleLink = '<a href="'.get_site_url("manual-sale/?LinkBankID=".$LinkBankID).'" class="" title="Manual Sale">create</a>';
            }

            $LinkBankurl = get_linkbankurl($LinkBank);
            //$SaleInfoDetails = get_sale_info_details($LinkBank);

            $LinkStatus = $LinkBank->LinkStatus;
            if($LinkStatus == "active"){
                $LinkBankStatus = "Live";
                $LinkBankStatusIcon = "fa fa-rocket";
            }else if($LinkStatus == "pending"){
                $LinkBankStatus = "Pending";
                $LinkBankStatusIcon = "fa fa-hourglass-o";
            }else if($LinkStatus == "complete"){
                $LinkBankStatus = "Completed";
                $LinkBankStatusIcon = "fa fa-check-square-o";
            }else if($LinkStatus == "evergreen"){
                $LinkBankStatus = "Evergreen";
                $LinkBankStatusIcon = "fa fa-tree";
            }else if($LinkStatus == "mylink"){
                $LinkBankStatus = "My Links";
                $LinkBankStatusIcon = "fa fa-user";
            }

            $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$LinkBankID);

            $StatusIcon = get_domain_status_icon($LinkBank->DomainID);

            $MasterCampaignName = $LinkBank->MasterCampaignName;

            $ConversionsCount = $LinkBank->ConversionsCount;
            $ConversionsAmount = $LinkBank->ConversionsAmount;
            $ConversionsPercentage =  $LinkBank->ConversionsPercentage;//$UniqueClicks == 0 ? 0 : ($ConversionsCount / $UniqueClicks * 100);

            $actions_percentage = $UniqueClicks > 0 ? $Actions / $UniqueClicks * 100 : 0;


            $FormatedRow = [
                "LinkBankID"                => $LinkBankID,
                "StatusIcon"                => $StatusIcon,
                "NameLinkedURL"             => $namelinkedurl,
                "LinkBankURL"               => $LinkBankurl,
                "LinkName"                  => $LinkBank->LinkName,
                "VendorName"                => $VendorName,
                "RawClicks"                 => get_formated_number($RawClicks),
                "UniqueClicks"              => get_formated_number($UniqueClicks),
                "ManualSaleLink"            => $LinkBankSaleType == "" ? $ManualSaleLink : "",
                "ConversionsCount"          => $LinkBankSaleType == "conv" ? get_formated_number($ConversionsCount) : 0,
                "ConversionsAmount"         => $LinkBankSaleType == "conv" ? get_formated_number(round($ConversionsAmount, 2)) : 0,
                "ConversionsPercentage"     => $LinkBankSaleType == "conv" ? get_formated_number(round($ConversionsPercentage, 2)) : 0,
                "GroupName"                 => $GroupName,
                "CreatedDate"               => $LinkBank->CreatedDate,
                "LinkBankStatusIcon"        => $LinkBankStatusIcon,
                //"LinkbankStauses"           => $LinkbankStauses,
                "LinkStatus"                => $LinkBank->LinkStatus,
                "LinkBankStatus"            => $LinkBankStatus,
                "MasterCampaignName"        => $MasterCampaignName,
                "ActionsPercentage"         => $actions_percentage,
                "VendorID"                  => $LinkBank->VendorID,
                "GroupID"                   => $LinkBank->GroupID,
                "DomainID"                  => $LinkBank->DomainID
            ];

            return $FormatedRow;
        }

        /**
         * Summary of get_display_rows
         * @param clsDBLinkbanks[] $LinkBanks 
         * @param mixed $UserID
         * @param mixed $LinkBanksType 
         * @param mixed $LinkBankSaleType 
         * @param mixed $echo 
         * @return string
         */
        function get_display_rows($LinkBanks, $UserID, $LinkBanksType = "", $LinkBankSaleType = "", $echo = true){
            //ob_end_clean();
            ob_start();

            /*****************************Contents Start Here*****************************/

            $LinkbankStauses = SYS()->LinkStatusesAll;
            $CurrentPage = "linkbank/stats".($LinkBankSaleType == ""?"":"/".$LinkBankSaleType);
            $PageURL = "linkbank/stats";
            foreach($LinkBanks as $LinkBank){
                $FormatedRow = $this->get_formated($LinkBank, $LinkBanksType, $LinkBankSaleType, $UserID);
                $GroupName = $FormatedRow["GroupName"];
                $VendorName = $FormatedRow["VendorName"];
                $LinkBankID = $FormatedRow["LinkBankID"];
                $LinkBankURL = $FormatedRow["LinkBankURL"];
?>
<tr>
    <td class="text-center hidden-xs">
        <?php echo $FormatedRow["StatusIcon"];?>
    </td>
    <?php if($LinkBankSaleType == "conv") { ?>
    <td class="text-center grey-scale">
        <a href="javascript:" data-url="<?php site_url($CurrentPage."?act=linkbank_details&LinkBankID=".$LinkBankID);?>" data-toggle="DetailRow" id="" class="btn btn-rotator-detail">
            <i class="fa fa-caret-square-o-down"></i>
        </a>
    </td>
    <?php } ?>
    <td class="text-center">
        <a href="<?php site_url("linkbank/?LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links visible-xs-inline-block" title="Edit Link">
            <i class="fa fa-pencil-square"></i>
        </a>
        <a href="javascript: void(0);" data-clipboard-text="<?php echo $LinkBankURL;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $LinkBankURL;?>">Copy</a>
    </td>
    <td>
        <a href="<?php echo $FormatedRow["NameLinkedURL"]; ?>" class="pull-left qtip2" data-qtip-image="<?php echo PREVIEW_URL.$LinkBankURL;?>">
            <?php echo $FormatedRow["LinkName"]?>
        </a>
    </td>

    <td class="hidden-xs">
        <span>
            <?php echo $VendorName;?>
        </span>
    </td>
    <td class="text-center">
        <span>
            <?php echo $FormatedRow["RawClicks"];?>
        </span>
    </td>
    <td class="text-center">
        <span>
            <?php echo $FormatedRow["UniqueClicks"];?>
        </span>
    </td>
    <?php if($LinkBankSaleType == "") { ?>
    <td class="text-center hidden-xs">
        <span>
            <?php echo $FormatedRow["ManualSaleLink"];?>
        </span>
    </td>
    <?php } ?>
    <?php if($LinkBankSaleType == "conv") { ?>
    <td class="text-center">
        <span>
            <?php echo $FormatedRow["ConversionsCount"];?>
        </span>
    </td>
    <td class="text-center">
        <span>
            $<?php echo $FormatedRow["ConversionsAmount"];?>
        </span>
    </td>
    <td class="text-center">
        <span>
            <?php echo $FormatedRow["ConversionsPercentage"];?>%
        </span>
    </td>
    <?php } ?>
    <td class="hidden-xs">
        <span>
            <?php echo $GroupName;?>
        </span>
    </td>
    <td data-sort="<?php echo strtotime($FormatedRow["CreatedDate"]);?>" class="text-center hidden-xs">
        <span>
            <?php echo $FormatedRow["CreatedDate"];?>
        </span>
    </td>
    <td class="text-center grey-scale hidden-xs">
        <a href="javascript:" class="status_icon" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click" id="">
            <i class="fa <?php echo $FormatedRow["LinkBankStatusIcon"];?>"></i>
        </a>
        <div class="qtipsubmenu grey-scale">
            <form class="form-inline status_form" role="form" method="get" action="<?php site_url($CurrentPage.(!empty($LinkBanksType) && $LinkBanksType != "all"?"/".$LinkBanksType."/":""));?>">
                <div class="form-group select-xs">
                    <select name="status" id="LinkStatus" class="form-control select2">
                        <?php foreach($LinkbankStauses as $LinkbankStausName => $LinkbankStausText){ ?>
                        <option value="<?php echo $LinkbankStausName;?>" <?php if($FormatedRow["LinkStatus"] == $LinkbankStausName) echo ' selected = "selected" ';?>><?php echo $LinkbankStausText;?></option>
                        <?php } ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-xs btn-default">Change</button>
                <input type="hidden" name="act" value="changelinkstatus" />
                <input type="hidden" name="LinkBankID" value="<?php echo $LinkBankID;?>" />
            </form>
        </div>
    </td>
    <td class="text-center hidden-xs">
        <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-" data-qtip-showevent="click">
            <i class="fa fa-cog"></i>
        </a>
        <div class="qtipsubmenu grey-scale">
            <ul class="list-unstyled">
                <li>
                    <a href="<?php site_url("linkbank/?LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links" title="Edit Link">
                        <i class="fa fa-pencil-square"></i>Edit Link
                    </a>
                </li>
                <li>
                    <a href="<?php echo $LinkBankURL."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link">
                        <i class="fa fa-external-link-square"></i>Direct Link
                    </a>
                </li>
                <li>
                    <a href="<?php site_url($CurrentPage."/?act=delete_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links btn-delete" title="Delete Link">
                        <i class="fa fa-trash"></i>Delete Link
                    </a>
                </li>
                <li>
                    <a href="<?php site_url($CurrentPage."/?act=reset_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links btn-reset" title="Reset Statistics">
                        <i class="fa fa-ban"></i>Reset Statistics
                    </a>
                </li>
                <li>
                    <a href="<?php site_url($CurrentPage."/?act=clone_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links" title="Clone Link">
                        <i class="fa fa-clone"></i>Clone Link
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" data-clipboard-text="<?php echo $LinkBankURL;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $LinkBankURL;?>">
                        <i class="fa fa-clipboard"></i>Copy Link
                    </a>
                </li>
                <li>
                    <a href="<?php site_url($PageURL."/".$LinkBankID."/details");?>" class="btn btn-sm btn-links" title="Statistics">
                        <i class="fa fa-globe"></i>Statistics
                    </a>
                </li>
            </ul>
        </div>
    </td>
</tr>
<?php
            }
            /*****************************Contents Start Here*****************************/
            $Contents = ob_get_contents();
            ob_end_clean();

            if($echo)
                echo $Contents;

            return $Contents;
        }
		/** [User Functions End] **/
	}
}
if(!function_exists('DBLinkbanks')){
	function DBLinkbanks($obj = null) {
		return clsDBLinkbanks::instance($obj);
	}
}