<?php
/** [Database Struct Include Start] **/
if(!class_exists('clsStructManualsaleExpenses'))
	include_once 'struct/ManualsaleExpenses.Struct.Class.php';
/** [Database Struct Include End] **/

if(!class_exists('clsDBManualsaleExpenses')){
	class clsDBManualsaleExpenses extends clsStructManualsaleExpenses {
		protected static $_instance = null;
		public static function instance($obj = null) {
			if (is_null(self::$_instance) || is_null($obj) || (is_object($obj) && self::$_instance != $obj) || (is_scalar($obj) && self::$_instance->id != $obj)) {
				self::$_instance = new self($obj);
			}
			return self::$_instance;
		}

        /** [User Variables Start] **/
		/** [User Variables End] **/

		function __construct($obj = null){
            parent::__construct();

            if(!$obj || $obj == null || $obj == "0")
                return;

			if(is_numeric($obj)){
				$obj = $this->get_by_key($obj);
			} else if(is_string($obj)){
				$obj = $this->get($obj);
			}

			if($obj && !is_numeric($obj) && !is_string($obj)) {
				$Fields = get_object_vars($obj);
				foreach ($Fields as $FieldName => $FieldValue){
					$this->$FieldName = $obj->$FieldName;
				}
                if(isset($obj->id))
                    $this->PrimaryKey = $obj->id;
			}
		}

        function get_last_id(){
            return clsDBManualsaleExpenses::$Lastid;
        }

        function set_last_id($Lastid){
            clsDBManualsaleExpenses::$Lastid = $Lastid;
        }

        /**
         * Summary of get_typed_row
         * @param mixed $Row
         * @return clsDBManualsaleExpenses
         */
        function get_typed_row($Row){
            return $Row;
        }

        /**
         * Summary of get_typed_rows
         * @param mixed $Rows
         * @return clsDBManualsaleExpenses[]
         */
        function get_typed_rows($Rows){
            return $Rows;
        }

        function get_var($FieldValue, $FieldName, $ReturnField = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->manualsale_expenses.".") == false ? DB()->manualsale_expenses.".".$OrderBy: $OrderBy;
            $sql = "select ".$ReturnField." from ".DB()->manualsale_expenses." where ".$FieldName." = '".$FieldValue."' ORDER BY ".$OrderBy." ".$OrderType;;
            $ReturnedValue = DB()->get_var($sql);
            return $ReturnedValue;
        }

        /**
         * @return clsDBManualsaleExpenses|null
         */
        function get($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->manualsale_expenses->Asterisk." from ".DB()->manualsale_expenses;

                if($Condition != "")
                    $sql .= " WHERE ".$Condition;
            } else {
                $sql = $Condition;
            }

            $sql .= " limit 0,1 ";

            $Row = DB()->get_row($sql);

            return $Row;
        }

        /**
         * @return clsDBManualsaleExpenses|null
         */
        function get_by_key($id){
            $Condition = DB()->manualsale_expenses->id." = ".$id;
            $Row = $this->get_row($Condition);
            return $Row;
        }

        /**
         * @return clsDBManualsaleExpenses|null
         */
        function get_row($id = null, $userid = null, $ManualSaleSplitPartnerID = null, $ExpenseName = null, $ExpenseNote = null, $ExpenseAmount = null, $DateAdded = null){
            $data = array();
            $Fields = array();
            $Condition = "";

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($ManualSaleSplitPartnerID !== null) $data["ManualSaleSplitPartnerID"] = $ManualSaleSplitPartnerID;
			if($ExpenseName !== null) $data["ExpenseName"] = $ExpenseName;
			if($ExpenseNote !== null) $data["ExpenseNote"] = $ExpenseNote;
			if($ExpenseAmount !== null) $data["ExpenseAmount"] = $ExpenseAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                foreach ($data as $FieldName => $FieldValue){
                	$Fields[] = DB()->manualsale_expenses->$FieldName." = '".$FieldValue."' ";
                }

                $Condition = implode(" and ", $Fields);
            }

            $Row = $this->get($Condition);

            return $Row;
        }

        /**
         * @return clsDBManualsaleExpenses[]
         */
        function get_all($Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc"){
            $OrderBy = stripos($OrderBy, DB()->manualsale_expenses.".") == false ? DB()->manualsale_expenses.".".$OrderBy: $OrderBy;
            if(stripos($Condition, "select ") === false){
                $sql = "select ".DB()->manualsale_expenses->Asterisk." from ".DB()->manualsale_expenses;

                if($Condition != "")
                    $sql .= " where ".$Condition;

                $sql .= " group by ".DB()->manualsale_expenses.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                $sql = $Condition;
            }

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);

            return $Rows;
        }

        /**
         * @return integer
         */
        function get_total($Condition = ""){
            if(stripos($Condition, "select ") === false){
                $sql = "select count(".DB()->manualsale_expenses->id.") from ".DB()->manualsale_expenses;

                if($Condition != "")
                    $sql .= " where ".$Condition;
            } else {
                $sql = $Condition;
            }

            $TotalRows = DB()->get_var($sql);

            return $TotalRows;
        }

        /**
         * @return array(clsDBManualsaleExpenses[], int)
         */
        function get_filtered($FilteringData, $Condition = "", $PageNo = 0, $DataLimit = -1, $GroupBy = "id", $OrderBy = "id", $OrderType = "asc", $Having = ""){
            $sql = stripos($Condition, "select ") === false ? " from ".DB()->manualsale_expenses : $Condition;
            $Condition =  stripos($Condition, "select ") === false ? $Condition : "";
            $FilterQuery = array();
            //$OrderBy = stripos($OrderBy, DB()->manualsale_expenses.".") == false ? DB()->manualsale_expenses.".".$OrderBy: $OrderBy;

            if($FilteringData && is_array($FilteringData) && count($FilteringData) > 0){
                foreach ($FilteringData as $FilterKey => $Filter){
                    if(is_array($Filter) && SYS()->is_string($Filter['condition']) && SYS()->is_valid($Filter['value'])){
                        $FilterQuery[] = $FilterKey." ".$Filter['condition']." '".$Filter['value']."'";
                    }
                }
            }

            $Condition = $Condition.(($Condition != "" && count($FilterQuery) > 0? " and " : "").implode(" and ", $FilterQuery));

            if(stripos($Condition, "where") === false && $Condition != "")
                $sql .= " where ".$Condition;

            if(stripos($sql, "select ") === false){
                $sqlCount = "select count(".DB()->manualsale_expenses->id.") ".$sql;
                $sql = "select ".DB()->manualsale_expenses->Asterisk.$sql." group by ".DB()->manualsale_expenses.".".$GroupBy." ORDER BY ".$OrderBy." ".$OrderType;
            } else {
                if(stripos($sql, '{from_start}') !== false){
                    $sqlCount = "select count(".DB()->manualsale_expenses->id.") ".substr($sql, stripos($sql, "{from_start}"));
                } else {
                    $sqlCount = "select count(".DB()->manualsale_expenses->id.") ".substr($sql, stripos($sql, " from"));
                }
                $sql = $sql." ".$Condition." group by ".DB()->manualsale_expenses.".".$GroupBy.($Having != "" ? " having (".$Having.") " : "");//." ORDER BY ".$OrderBy." ".$OrderType;
            }

            if(stripos($sql, '{from_start}') !== false){
                $sql = str_ireplace('{from_start}', '', $sql);
                $sqlCount = "select count(*) from (".$sql.") as tbl";
                $sql = "select * from (".$sql.") as tbl";
            }

            $sql .=  " ORDER BY ".$OrderBy." ".$OrderType;

            if(stripos($sqlCount, '{from_start}') !== false)
                $sqlCount = str_ireplace('{from_start}', '', $sqlCount);

            if($DataLimit != -1){
                $Offset = ($PageNo - 1) * $DataLimit;
                $sql .= " limit ".$Offset.",".$DataLimit;
            }

            $Rows = DB()->get_results($sql);
            $TotalRows = DB()->get_var($sqlCount);

            return array('Rows' => $Rows, 'TotalRows' => $TotalRows);
        }

        /**
         * @return boolean
         */
        function insert($userid = null, $ManualSaleSplitPartnerID = null, $ExpenseName = null, $ExpenseNote = null, $ExpenseAmount = null, $DateAdded = null){
            $data = array();
            $Inserted = false;

            if($userid !== null) $data["userid"] = $userid;
			if($ManualSaleSplitPartnerID !== null) $data["ManualSaleSplitPartnerID"] = $ManualSaleSplitPartnerID;
			if($ExpenseName !== null) $data["ExpenseName"] = $ExpenseName;
			if($ExpenseNote !== null) $data["ExpenseNote"] = $ExpenseNote;
			if($ExpenseAmount !== null) $data["ExpenseAmount"] = $ExpenseAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            $this->set_last_id(0);
            if(count($data) > 0){
                $Inserted = DB()->insert(DB()->manualsale_expenses, $data);
                if($Inserted){
                    $this->set_last_id(DB()->insert_id);
                }
            }

            return $Inserted;
        }

        /**
         * @return boolean
         */
        function update($id, $userid = null, $ManualSaleSplitPartnerID = null, $ExpenseName = null, $ExpenseNote = null, $ExpenseAmount = null, $DateAdded = null){
            $data = array();
            $Updated = false;

            if($userid !== null) $data["userid"] = $userid;
			if($ManualSaleSplitPartnerID !== null) $data["ManualSaleSplitPartnerID"] = $ManualSaleSplitPartnerID;
			if($ExpenseName !== null) $data["ExpenseName"] = $ExpenseName;
			if($ExpenseNote !== null) $data["ExpenseNote"] = $ExpenseNote;
			if($ExpenseAmount !== null) $data["ExpenseAmount"] = $ExpenseAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            $this->set_last_id(0);
            if(count($data) > 0){
                if(isset($data["created"])) unset($data["created"]);
                if(isset($data["CreatedDateTime"])) unset($data["CreatedDateTime"]);

                $Updated = DB()->update(DB()->manualsale_expenses, $data, array("id" => $id));
                $this->set_last_id($id);
            }

            return $Updated;
        }

        /**
         * @return boolean
         */
        function delete($id, $userid = null, $ManualSaleSplitPartnerID = null, $ExpenseName = null, $ExpenseNote = null, $ExpenseAmount = null, $DateAdded = null){
            $data = array();
            $Deleted = false;

            if($id) $data["id"] = $id;
            if($userid !== null) $data["userid"] = $userid;
			if($ManualSaleSplitPartnerID !== null) $data["ManualSaleSplitPartnerID"] = $ManualSaleSplitPartnerID;
			if($ExpenseName !== null) $data["ExpenseName"] = $ExpenseName;
			if($ExpenseNote !== null) $data["ExpenseNote"] = $ExpenseNote;
			if($ExpenseAmount !== null) $data["ExpenseAmount"] = $ExpenseAmount;
			if($DateAdded !== null) $data["DateAdded"] = $DateAdded;

            if(count($data) > 0){
                $Deleted = DB()->delete(DB()->manualsale_expenses, $data);
            }

            return $Deleted;
        }

        function save($Condition, $userid = null, $ManualSaleSplitPartnerID = null, $ExpenseName = null, $ExpenseNote = null, $ExpenseAmount = null, $DateAdded = null){
            $DBRow = $this->get($Condition);
            $Saved = false;
            if(!$DBRow){
                $Saved = $this->insert($userid, $ManualSaleSplitPartnerID, $ExpenseName, $ExpenseNote, $ExpenseAmount, $DateAdded);
            } else {
                $this->update($DBRow->id, $userid, $ManualSaleSplitPartnerID, $ExpenseName, $ExpenseNote, $ExpenseAmount, $DateAdded);
                $Saved = true;
            }

            return $Saved;
        }

		function __destruct(){
		}

        function isNull(){
            return !isset($this->id) || $this->id == null || $this->id == 0;
        }

        function isNotNull(){
            return isset($this->id) && $this->id != null && $this->id != 0;
        }

        /** [User Functions Start] **/
		/** [User Functions End] **/
	}
}
if(!function_exists('DBManualsaleExpenses')){
	function DBManualsaleExpenses($obj = null) {
		return clsDBManualsaleExpenses::instance($obj);
	}
}