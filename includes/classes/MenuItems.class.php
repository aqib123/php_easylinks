<?php
if(!class_exists("clsMenuItems")){
    class clsMenuItems{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        function __construct(){

        }

        function __destruct(){
            //
        }

        function get($MenuItemID){
            $sql = "select ".DB()->menu_items->Asterisk." from ".DB()->menu_items." where ".DB()->menu_items->id."=".$MenuItemID." and ".DB()->menu_items->MenuItemEnabled." = 1 group by ".DB()->menu_items->id." ";
            $MenuItem = DB()->get_row($sql);
            return $MenuItem;
        }

        function get_all($OrderBy = "MenuItemLabel", $OutputType = OBJECT){
            $sql = "select ".DB()->menu_items->Asterisk." from ".DB()->menu_items." where ".DB()->menu_items->MenuItemEnabled." = 1 group by ".DB()->menu_items->id." ORDER BY ".DB()->menu_items.".".$OrderBy." ";
            $MenuItems = DB()->get_results($sql, $OutputType);
            return $MenuItems;
        }

        function get_all_by_level($LevelAccessKey, $OrderBy = "MenuItemLabel", $OutputType = OBJECT_K){
            $sql = "select ".DB()->menu_items->Asterisk.", ".DB()->level_menu_items->id." as LevelMenuItemID, ".DB()->level_menu_items->LevelMenuItemEnabled." from ".DB()->menu_items." inner join ".DB()->level_menu_items." on ".DB()->menu_items->id." = ".DB()->level_menu_items->MenuItemID." inner join ".DB()->levels." on ".DB()->levels->id." = ".DB()->level_menu_items->LevelID." where ".DB()->menu_items->MenuItemEnabled." = 1 and ".DB()->levels->LevelAccessKey." = '".$LevelAccessKey."' group by ".DB()->menu_items->id." ORDER BY ".DB()->menu_items.".".$OrderBy." ";
            $LevelMenuItems = DB()->get_results($sql, $OutputType);
            return $LevelMenuItems;
        }

        function get_all_by_user($UserID, $OrderBy = "MenuItemLabel", $OutputType = OBJECT){
            $sql = "select ".DB()->menu_items->Asterisk.", ".DB()->levels->Asterisk.", ".DB()->level_menu_items->Asterisk." from ".DB()->menu_items." inner join ".DB()->level_menu_items." on ".DB()->menu_items->id." = ".DB()->level_menu_items->MenuItemID." inner join ".DB()->levels." on ".DB()->levels->id." = ".DB()->level_menu_items->LevelID." inner join ".DB()->user_levels." on ".DB()->levels->id." = ".DB()->user_levels->LevelID." where ".DB()->menu_items->MenuItemEnabled." = 1 and ".DB()->user_levels->UserID." = ".$UserID." group by ".DB()->menu_items->id." ORDER BY ".DB()->menu_items.".".$OrderBy." ";
            $LevelMenuItems = DB()->get_results($sql, $OutputType);
            return $LevelMenuItems;
        }

        function get_by_level($LevelIDs){
            $sql = "select ".DB()->menu_items.".*, ".DB()->level_menu_items->MenuItemID.", ".DB()->level_menu_items->LevelMenuItemEnabled.", ".DB()->level_menu_items->LevelMenuItemLocked." from ".DB()->menu_items." inner join ".DB()->level_menu_items." on ".DB()->menu_items.".id=".DB()->level_menu_items.".MenuItemID where LevelID in (".implode(",", $LevelIDs).") and MenuItemEnabled=1 and LevelMenuItemEnabled=1 and MenuItemHasAdmin=0 order by MenuItemPosition";
            $MenuItems = DB()->get_results($sql, ARRAY_A);

            return $MenuItems;
        }

        function get_submenu($MenuItemParentID, $OrderMenuItems = true){
            global $LoggedInUser;

            $MenuItems = $LoggedInUser->MenuItems;
            $SubMenuItems = array_filter($MenuItems, function($MenuItem) use ($MenuItemParentID){
                return ($MenuItem["MenuItemParentID"] == $MenuItemParentID);
            });

            if($OrderMenuItems){
                usort($SubMenuItems, function($MenuItem1, $MenuItem2) {
                    return $MenuItem1['MenuItemPosition'] - $MenuItem2['MenuItemPosition'];
                });
            } else {
                sort($SubMenuItems);
            }

            return $SubMenuItems;
        }

        function is_menu_active($MenuItems, $PageURL){
            $SubMenuIndex = array_search($PageURL, array_column($MenuItems, 'MenuItemURL'));
            $isSubMenuItemActive = $SubMenuIndex !== false && GE()->is_valid($MenuItems[$SubMenuIndex]);
            if(!$isSubMenuItemActive){
                foreach ($MenuItems as $MenuItem){
                    $SubMenuItems = $this->get_submenu($MenuItem["id"]);
                    if(count($SubMenuItems) > 0){
                        $isSubMenuItemActive = $this->is_menu_active($SubMenuItems, $PageURL);
                        if($isSubMenuItemActive)
                            break;
                    }
                }
            }
            return $isSubMenuItemActive;
        }

        function find($MenuItems, $ColumnName, $SearchKey){
            $MenuItemIndex = array_search($SearchKey, array_column($MenuItems, $ColumnName));
            $MenuItem = $MenuItems[$MenuItemIndex];
            return $MenuItem;
        }

        function is_menu_enabled($ModuleName){
            global $LoggedInUser;
            $ModuleEnabled = false;
            $MenuItems = $LoggedInUser->MenuItems;
            foreach ($MenuItems as $MenuItem){
                if($MenuItem["ModuleName"] == $ModuleName && $MenuItem["ModuleEnabled"] == 1){
                    $ModuleEnabled = true;
                    break;
                }
            }

            return $ModuleEnabled;
        }
    }
}

global $elMenuItems;
if(class_exists("clsMenuItems") && !$elMenuItems){
    $elMenuItems = new clsMenuItems();
}

if(!function_exists("elMenuItems")){
    function elMenuItems() {
        return clsMenuItems::instance();
    }
}
$GLOBALS['elMenuItems'] = elMenuItems();