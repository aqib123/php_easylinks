<?php
if(!class_exists("clsForms")){
    class clsForms{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        function __construct(){

        }

        function __destruct(){
            //
        }

        function CreateSelectDropdown($SelectID, $Options, $OptionKeyField, $OptionValueField, $SelectedOptionKey = '', $DefaultOptionText = '', $DefaultOptionValue = '', $CustomClasses = "", $ExtraOptions = "", $DataWidth = "226px", $echo = true){
            $html = '';

            if($SelectID != ""){
                $html .= '<select name="'.$SelectID.'" id="'.$SelectID.'" data-width="'.$DataWidth.'" class="bootstrap-select '.$CustomClasses.'" '.$ExtraOptions.' >';
            }

            if($DefaultOptionText != ""){
                $html .= '<option value="'.$DefaultOptionValue.'" '.(!SYS()->is_valid($SelectedOptionKey) || (SYS()->is_valid($SelectedOptionKey) &&  $DefaultOptionValue == $SelectedOptionKey) ? ' selected="selected" ' : "").'>'.$DefaultOptionText.'</option>';
            }

            foreach ($Options as $Option){
                if($OptionKeyField != "" && $OptionValueField != ""){
                    if(is_array($Option))
                        $Option = (object)$Option;

                    $OptionKey = $Option->$OptionKeyField;
                    $OptionValue = $Option->$OptionValueField;
                } else {
                    $OptionKey = $Option;
                    $OptionValue = strtoupper($Option);
                }

            	$html .= '<option value="'.$OptionKey.'" '.(SYS()->is_valid($SelectedOptionKey) && ((is_array($SelectedOptionKey) && in_array($OptionKey, $SelectedOptionKey)) || (!is_array($SelectedOptionKey) && $OptionKey == $SelectedOptionKey)) ? ' selected="selected" ' : "").' '.(SYS()->is_string($Option->content) ? " data-content='".$Option->content."' " : "").'>'.$OptionValue.'</option>';
            }

            if($SelectID != ""){
                $html .= '</select>';
            }

            if($echo)
                echo $html;

            return $html;
        }

        function CreateSelectAddOn($DataHref, $DataTarget, $PageTitle, $DataMethod = 'post', $Href = 'javascript:', $Icon = 'fa fa-plus'){
            $html = '';

            $html .= '<span class="input-group-addon">';
            $html .= '  <a class="btn btn-just-icon btn-rose tool-tip-container" href="'.$Href.'" data-href="'.$DataHref.'" data-widget="ShowGroupEngageModel" data-related="'.$DataTarget.'" data-method="'.$DataMethod.'" data-page-title="'.$PageTitle.'">';
            $html .= '      <i class="'.$Icon.'"></i>';
            $html .= '  </a>';
            $html .= '</span>';

            return $html;
        }

        function CreateButton($Text, $Href, $ButtonClass = 'btn-rose'){
            $html = '';

            $html .= '<a href="'.$Href.'" class="btn btn-sm '.$ButtonClass.'">'.$Text.'</a>';

            return $html;
        }

        function CreateDataButton($Text, $DataHref, $DataTarget, $PageTitle, $AppendData = false, $ExtraData = '', $DataWidget = 'ShowGroupEngageModel', $DataAppendTarget = '', $ButtonClass = 'btn-rose', $DataMethod = 'post', $Href = 'javascript:'){
            $html = '';

            $html .= '<a href="'.$Href.'" data-href="'.$DataHref.'" '.($AppendData ? ' data-append="true" ' : '').''.($ExtraData != '' ? ' data-values="'.$ExtraData.'" ' : '').''.($DataAppendTarget != '' ? ' data-append-object="'.$DataAppendTarget.'" ' : '').' data-widget="'.$DataWidget.'" data-related="'.$DataTarget.'" data-method="'.$DataMethod.'" data-page-title="'.$PageTitle.'" class="btn btn-sm '.$ButtonClass.'">'.$Text.'</a>';

            return $html;
        }

        function CreateCheckBox($CheckboxName, $CheckboxID, $CheckboxValue, $CheckboxLabel = "", $isChecked = false, $CustomClass = "", $ExtraAttributes = ""){
            $html = '';

            $html .= '<div class="checkbox">';
            $html .= '   <label>';
            $html .= '       <input type="checkbox" name="'.$CheckboxName.'" id="'.$CheckboxID.'" class="'.$CustomClass.'" value="'.$CheckboxValue.'" '.($isChecked ? ' checked="checked" ' : "").' '.$ExtraAttributes.' />';
            if($CheckboxLabel != "")
                $html .= '       &nbsp;<span>'.$CheckboxLabel.'</span>&nbsp;&nbsp;';
            $html .= '   </label>';
            $html .= '</div>';

            return $html;
        }

        function CreateRadioButton($RadiobuttonName, $RadiobuttonID, $RadiobuttonValue, $RadiobuttonLabel = "", $isChecked = false, $CustomClass = "", $ExtraAttributes = ""){
            $html = '';

            $html .= '<div class="radio">';
            $html .= '   <label>';
            $html .= '       <input type="radio" name="'.$RadiobuttonName.'" id="'.$RadiobuttonID.'" class="'.$CustomClass.'" value="'.$RadiobuttonValue.'" '.($isChecked ? ' checked="checked" ' : "").' '.$ExtraAttributes.' />';
            if($RadiobuttonLabel != "")
                $html .= '       &nbsp;<span class="radio_title">'.$RadiobuttonLabel.'</span>&nbsp;&nbsp;';
            $html .= '   </label>';
            $html .= '</div>';

            return $html;
        }

        function CreateHidenInputs($Options, $OptionKeyField, $OptionValueField, $Prefix = "", $PostFix = "", $echo = true){
            $html = '';

            foreach ($Options as $Option){
                if(is_array($Option))
                    $Option = (object)$Option;

            	$html .= '<input type="hidden" id="'.$Prefix.$Option->$OptionKeyField.$PostFix.'" name="'.$Prefix.$Option->$OptionKeyField.$PostFix.'" value="'.$Option->$OptionValueField.'" />';
            }

            if($echo)
                echo $html;

            return $html;
        }

        function CreateToggleSwitch($SwitchName, $isChecked = false, $SwitchClass = 'btn-default',  $Value= "1", $Title="Active", $ExtraData = '', $echo = true){
            ob_start();
?>
<span class="btn btn-xs btn-switch <?php echo $SwitchClass;?>">
    <label class="switch switch-sm">
        <input type="checkbox" class="" id="<?php echo $SwitchName;?>" name="<?php echo $SwitchName;?>" title="<?php echo $Title;?>" value="<?php echo $Value;?>" <?php echo $isChecked ? ' checked="checked" ' : ""; ?> <?php echo $ExtraData; ?> />
        <span class="slider round"></span>
    </label>
</span>
<?php
            $html = ob_get_contents();
            ob_end_clean();

            if($echo)
                echo $html;

            return $html;
        }
    }
}

global $elForms;
if(class_exists("clsForms") && !$elForms){
    $elForms = new clsForms();
}

if(!function_exists("elForms")){
    function elForms() {
        return clsForms::instance();
    }
}
$GLOBALS['elForms'] = elForms();