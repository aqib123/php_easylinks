<?php
if(!class_exists("clsDataset")){
    class clsDataset{
        var $ClassesPath;
        var $TemplatePath;
        var $ClassNamePrefix;
        var $ClassFileNamePostfix;
        var $StructNamePrefix;
        var $StructFileNamePostfix;
        var $DBClassesPath;
        var $DBStructesPath;
        var $FunctionNames;

        function __construct($ClassesPath, $TemplatePath = ""){
            $ClassesPath = str_replace("\\", "/", $ClassesPath);
            $TemplatePath = str_replace("\\", "/", $TemplatePath);

            rtrim("/", $ClassesPath);
            rtrim("/", $TemplatePath);

            $this->ClassesPath = $ClassesPath;
            $this->TemplatePath = $TemplatePath;
            $this->DBClassesPath = $ClassesPath."/db";
            $this->DBStructesPath = $this->DBClassesPath."/struct";

            if(!file_exists($this->DBClassesPath))
                @mkdir($this->DBClassesPath);

            if(!file_exists($this->DBStructesPath))
                @mkdir($this->DBStructesPath);

            $this->ClassNamePrefix = "clsDB";
            $this->ClassFileNamePostfix = ".DB.Class.php";

            $this->StructNamePrefix = "clsStruct";
            $this->StructFileNamePostfix = ".Struct.Class.php";

            $this->FunctionNames = array();
        }

        function __destruct(){
            //
        }

        function create_database_tables_declaration(){
            $DBTableNames = DB()->get_tables();
            $DataBaseClassPHP = file_get_contents($this->ClassesPath."/DataBase.Class.php");
            $NewDataBaseClassPHP = $DataBaseClassPHP;

            $IncludeSearchStartString = '/** [Tables Include Start] **/';
            $IncludeSearchEndString = '/** [Tables Include End] **/';
            $IncludeReplaceString = $IncludeSearchStartString."\r\n";

            $DeclarationSearchStartString = '/** [Tables Declaration Start] **/';
            $DeclarationSearchEndString = '/** [Tables Declaration End] **/';
            $DeclarationReplaceString = $DeclarationSearchStartString."\r\n";

            $InitializationSearchStartString = '/** [Tables Initialization Start] **/';
            $InitializationSearchEndString = '/** [Tables Initialization End] **/';
            $InitializationReplaceString = $InitializationSearchStartString."\r\n";

            foreach($DBTableNames as $DBTableID => $DBTableName){
                $FormatedDBTableID = str_replace(" ", "", ucwords(str_replace("_", " ", $DBTableID)));
                $StructName = $this->StructNamePrefix.$FormatedDBTableID;
                $StructFileName = $FormatedDBTableID.$this->StructFileNamePostfix;

                $IncludeReplaceString .= 'include_once "db/'.$StructFileName.'";'."\r\n";
                $DeclarationReplaceString .= "\r\n\t\t/**\r\n\t\t* @var ".$StructName."\r\n\t\t*/\r\n";
                $DeclarationReplaceString .= "\t\tvar $".$DBTableID.";"."\r\n";
                $InitializationReplaceString .= "\t\t\t\$this->".$DBTableID." = class_exists('".$StructName."') ? new ".$StructName."() : null;"."\r\n";

                $this->create_database_table_struct($DBTableID, $DBTableName);
                $this->create_database_table_class($DBTableID, $DBTableName);
            }

            $IncludeReplaceString .= $IncludeSearchEndString;
            $DeclarationReplaceString .= "\t\t".$DeclarationSearchEndString;
            $InitializationReplaceString .= "\t\t\t".$InitializationSearchEndString;

            //$NewDataBaseClassPHP = SYS()->replace($NewDataBaseClassPHP, $IncludeReplaceString, $IncludeSearchStartString, $IncludeSearchEndString);
            $NewDataBaseClassPHP = SYS()->replace($NewDataBaseClassPHP, $DeclarationReplaceString, $DeclarationSearchStartString, $DeclarationSearchEndString);
            $NewDataBaseClassPHP = SYS()->replace($NewDataBaseClassPHP, $InitializationReplaceString, $InitializationSearchStartString, $InitializationSearchEndString);

            file_put_contents($this->ClassesPath."/DataBase.Class.php", $NewDataBaseClassPHP);
            //echo $NewDataBaseClassPHP;
        }

        function create_database_table_struct($DBTableID, $DBTableName){
            $FormatedDBTableID = str_replace(" ", "", ucwords(str_replace("_", " ", $DBTableID)));
            $StructName = $this->StructNamePrefix.$FormatedDBTableID;
            $StructFileName = $FormatedDBTableID.$this->StructFileNamePostfix;

            $DBTableColumns = DB()->get_columns($DBTableID);//SYS()->print_r($DBTableColumns);
            $FieldsDeclarationCode = "var \$Asterisk;\r\n\t\t";
            $FieldsDeclarationCode .= "public static \${LastPrimaryKey};\r\n\t\t";
            $FieldsDeclarationCode .= "var \$PrimaryKey;\r\n\t\t";
            $FieldsInitializationCode = "\r\n\t\t\t\$this->Asterisk = '".$DBTableName.".*';";

            foreach($DBTableColumns as $DBTableColumn){
                $PhpDoc = "";
                if(stripos($DBTableColumn->Type, "int(") !== false)
                    $PhpDoc = "integer";
                else if(stripos($DBTableColumn->Type, "varchar(") !== false)
                    $PhpDoc = "string";
                else
                    $PhpDoc = "string";

                if($PhpDoc != "")
                    $PhpDoc = "\r\n\t\t/**\r\n\t\t* @var ".$PhpDoc."\r\n\t\t*/\r\n\t\t";

                $FieldsDeclarationCode .= $PhpDoc;
                $FieldsDeclarationCode .= "var $".$DBTableColumn->Field.";\r\n\t\t";
                $FieldsInitializationCode .= "\r\n\t\t\t\$this->".$DBTableColumn->Field." = '".$DBTableName.".".$DBTableColumn->Field."';";
                if(strtolower($DBTableColumn->Key) == "pri"){
                    $FieldsInitializationCode .= $PhpDoc;
                    $FieldsInitializationCode .= "\r\n\t\t\t\$this->PrimaryKey = '".$DBTableColumn->Field."';";
                    $FieldsDeclarationCode = str_replace(array("{LastPrimaryKey}"), array("Last".$DBTableColumn->Field), $FieldsDeclarationCode);
                }
            }

            if(!empty($this->TemplatePath) && file_exists($this->TemplatePath."/Struct.Class.Template.php")){
                $StructFileContent = file_get_contents($this->TemplatePath."/Struct.Class.Template.php");
                $DeclarationSearchStartString = '/** [Fields Declaration Start] **/';
                $DeclarationSearchEndString = '/** [Fields Declaration End] **/';
                $DeclarationReplaceString = $DeclarationSearchStartString."\r\n";

                $InitializationSearchStartString = '/** [Fields Initialization Start] **/';
                $InitializationSearchEndString = '/** [Fields Initialization End] **/';
                $InitializationReplaceString = $InitializationSearchStartString;

                $DeclarationReplaceString .= "\t\t".$FieldsDeclarationCode.$DeclarationSearchEndString;
                $InitializationReplaceString .= $FieldsInitializationCode."\r\n\t\t\t".$InitializationSearchEndString;

                $StructFileContent = SYS()->replace($StructFileContent, $DeclarationReplaceString, $DeclarationSearchStartString, $DeclarationSearchEndString);
                $StructFileContent = SYS()->replace($StructFileContent, $InitializationReplaceString, $InitializationSearchStartString, $InitializationSearchEndString);
                $StructFileContent = str_replace(array("{StructName}"), array($StructName), $StructFileContent);
                $StructFileContent = str_replace(array("{TableName}"), array($DBTableName), $StructFileContent);
            } else {
                $StructFileContent = "<?php\r\n";
                $StructFileContent .= "if(!class_exists('".$StructName."')){\r\n\t";
                $StructFileContent .= "class ".$StructName."{\r\n\t\t";
                $StructFileContent .= $FieldsDeclarationCode;
                $StructFileContent .= "function __construct(){";
                $StructFileContent .= $FieldsInitializationCode;
                $StructFileContent .= "\r\n\t\t}\r\n\r\n\t\t";
                $StructFileContent .= "function __destruct(){\r\n\t\t";
                $StructFileContent .= "}\r\n\r\n\t\t";
                $StructFileContent .= "function __toString(){\r\n\t\t\t";
                $StructFileContent .= "return '".$DBTableName."';\r\n\t\t";
                $StructFileContent .= "}\r\n\t";
                $StructFileContent .= "}\r\n";
                $StructFileContent .= "}";
            }
            file_put_contents($this->DBStructesPath."/".$StructFileName, $StructFileContent);
            //echo $StructFileContent;die;
        }

        function create_database_table_class($DBTableID, $DBTableName){
            $FormatedDBTableID = str_replace(" ", "", ucwords(str_replace("_", " ", $DBTableID)));
            $StructName = $this->StructNamePrefix.$FormatedDBTableID;
            $StructFileName = $FormatedDBTableID.$this->StructFileNamePostfix;

            $LettersCount = 0;
            do{
                $FunctionName = "DB".SYS()->get_initials($DBTableID, "_", $LettersCount, false, true);
                $LettersCount++;
            } while(isset($this->FunctionNames[$FunctionName]));
            $this->FunctionNames[$FunctionName] = $FunctionName;

            $ClassName = $this->ClassNamePrefix.$FormatedDBTableID;
            $ClassFileName = $FormatedDBTableID.$this->ClassFileNamePostfix;

            $ClassFilePath = $this->DBClassesPath."/".$ClassFileName;
            $DBTableColumns = DB()->get_columns($DBTableID);

            $FieldsDeclarationCode = "";
            $FieldsInitializationCode = "";

            $FieldsEqualNull = array();
            $IfFieldData = array();
            $FieldsOnly = array();

            //foreach($DBTableColumns as $DBTableColumn){
                //$FieldsDeclarationCode .= "var $".$DBTableColumn->Field.";\r\n\t\t";
                //$FieldsInitializationCode .= "\r\n\t\t\t\t\$this->".$DBTableColumn->Field." = \$obj->".$DBTableColumn->Field.";";
            //}

            $PrimaryKey = "";
            foreach($DBTableColumns as $DBTableColumn){
                $Field = strtolower($DBTableColumn->Field);
                $isUserIDField = $Field == "uid" || $Field == "userid" || $Field == "user_id" || $Field == "fk_user_id" || $Field == "UserID";
                if(strtolower($DBTableColumn->Key) == "pri"){
                    $PrimaryKey = $DBTableColumn->Field;
                    //break;
                } else {
                    $FieldName = "$".$DBTableColumn->Field;
                    $FieldEqualNull = $FieldName." = null";
                    $IfField = 'if('.$FieldName.' !== null) $data["'.$DBTableColumn->Field.'"] = '.$FieldName.';';
                    $FieldOnly = $FieldName;
                    if(!$isUserIDField){
                        $FieldsEqualNull[] = $FieldEqualNull;
                        $IfFieldData[] = $IfField;
                        $FieldsOnly[] = $FieldOnly;
                    } else {
                        array_unshift($FieldsEqualNull, $FieldEqualNull);
                        array_unshift($IfFieldData, $IfField);
                        array_unshift($FieldsOnly, $FieldOnly);
                    }


                }
            }

            //print_r($DBTableColumns);
            $FieldsEqualNull = implode(", ", $FieldsEqualNull);
            $IfFieldData = implode("\r\n\t\t\t", $IfFieldData);
            $FieldsOnly = implode(", ", $FieldsOnly);

            if($FieldsEqualNull == "") $FieldsEqualNull = '$NoFields = null';
            if($FieldsOnly == "") $FieldsOnly = '$NoFields';

            $DeclarationSearchStartString = '/** [Database Fields Start] **/';
            $DeclarationSearchEndString = '/** [Database Fields End] **/';
            $DeclarationString = $DeclarationSearchStartString."\r\n\t\t".$FieldsDeclarationCode.$DeclarationSearchEndString;

            $StructIncludeSearchStartString = '/** [Database Struct Include Start] **/';
            $StructIncludeSearchEndString = '/** [Database Struct Include End] **/';
            $StructIncludeString = $StructIncludeSearchStartString."\r\nif(!class_exists('".$StructName."'))\r\n\tinclude_once 'struct/".$StructFileName."';\r\n".$StructIncludeSearchEndString;

            $UserVariablesStartString = '/** [User Variables Start] **/';
            $UserVariablesEndString = '/** [User Variables End] **/';

            $UserFunctionsStartString = '/** [User Functions Start] **/';
            $UserFunctionsEndString = '/** [User Functions End] **/';

            //echo $ClassFilePath;die;
            if(file_exists($ClassFilePath)){
                if(!empty($this->TemplatePath) && file_exists($this->TemplatePath."/DB.Class.Template.php")){
                    $ClassFileContent = file_get_contents($this->TemplatePath."/DB.Class.Template.php");
                    $CurrentClassFileContent = file_get_contents($ClassFilePath);
                    $UserVariablesString = SYS()->find($CurrentClassFileContent, $UserVariablesStartString, $UserVariablesEndString, $UserVariablesStartString."\r\n\t\t".$UserVariablesEndString);
                    $UserFunctionsString = SYS()->find($CurrentClassFileContent, $UserFunctionsStartString, $UserFunctionsEndString, $UserFunctionsStartString."\r\n\t\t".$UserFunctionsEndString);

                    $ClassFileContent = str_replace(array("{ClassName}"), array($ClassName), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{StructName}"), array($StructName), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{TableName}"), array($DBTableName), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{FunctionName}"), array($FunctionName), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{PrimaryKey}"), array($PrimaryKey), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{DBTableID}"), array($DBTableID), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{FieldsEqualNull}"), array($FieldsEqualNull), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{IfFieldData}"), array($IfFieldData), $ClassFileContent);
                    $ClassFileContent = str_replace(array("{FieldsOnly}"), array($FieldsOnly), $ClassFileContent);

                    $ClassFileContent = SYS()->replace($ClassFileContent, $StructIncludeString, $StructIncludeSearchStartString, $StructIncludeSearchEndString);
                    $ClassFileContent = SYS()->replace($ClassFileContent, $UserVariablesString, $UserVariablesStartString, $UserVariablesEndString);
                    $ClassFileContent = SYS()->replace($ClassFileContent, $UserFunctionsString, $UserFunctionsStartString, $UserFunctionsEndString);
                } else {
                    $FieldsInitializationCode .= "\r\n\t\t\t\t\$Fields = get_object_vars(\$obj);";
                    $FieldsInitializationCode .= "\r\n\t\t\t\tforeach (\$Fields as \$FieldName => \$FieldValue){";
                    $FieldsInitializationCode .= "\r\n\t\t\t\t\t\$this->\$FieldName = \$obj->\$FieldName;";
                    $FieldsInitializationCode .= "\r\n\t\t\t\t}";

                    $ClassFileContent = "<?php\r\n";
                    $ClassFileContent .= $StructIncludeString;
                    $ClassFileContent .= "\r\n\r\nif(!class_exists('".$ClassName."')){\r\n\t";
                    $ClassFileContent .= "class ".$ClassName." extends ".$StructName." {";
                    //$ClassFileContent .= $DeclarationString;
                    $ClassFileContent .= "\r\n\t\tprotected static \$_instance = null;";
                    $ClassFileContent .= "\r\n\t\tpublic static function instance(\$obj) {";
                    $ClassFileContent .= "\r\n\t\t\tif ( is_null( self::\$_instance ) ) {";
                    $ClassFileContent .= "\r\n\t\t\t\tself::\$_instance = new self(\$obj);";
                    $ClassFileContent .= "\r\n\t\t\t}";
                    $ClassFileContent .= "\r\n\t\t\treturn self::\$_instance;";
                    $ClassFileContent .= "\r\n\t\t}";

                    $ClassFileContent .= "\r\n\t\tfunction __construct(\$obj){";
                    $ClassFileContent .= "\r\n\t\t\t\$this->PrimaryKey = '{PrimaryKey}';";
                    $ClassFileContent .= "\r\n\t\t\tif(is_numeric(\$obj)){";
                    $ClassFileContent .= "\r\n\t\t\t\t\$obj = DB()->get_row('select * from '.DB()->".$DBTableID.".' where id='.\$obj);";
                    $ClassFileContent .= "\r\n\t\t\t} else if(is_string(\$obj)){";
                    $ClassFileContent .= "\r\n\t\t\t\t\$obj = DB()->get_row(\$obj);";
                    $ClassFileContent .= "\r\n\t\t\t}";
                    $ClassFileContent .= "\r\n\t\t\tif(\$obj){";
                    $ClassFileContent .= $FieldsInitializationCode;
                    $ClassFileContent .= "\r\n\t\t\t}";
                    $ClassFileContent .= "\r\n\t\t}\r\n\r\n\t\t";
                    $ClassFileContent .= "function __destruct(){\r\n\t\t";
                    $ClassFileContent .= "}\r\n\t";
                    //$ClassFileContent .= "function __toString(){\r\n\t\t\t";
                    //$ClassFileContent .= "return '".$DBTableName."';\r\n\t\t";
                    //$ClassFileContent .= "}\r\n\t";
                    $ClassFileContent .= "}\r\n";
                    $ClassFileContent .= "}\r\n";
                    $ClassFileContent .= "if(!function_exists('".$FunctionName."')){\r\n\t";
                    $ClassFileContent .= "function ".$FunctionName."(\$obj) {\r\n\t\t";
                    $ClassFileContent .= "return ".$ClassName."::instance(\$obj);\r\n\t";
                    $ClassFileContent .= "}\r\n";
                    $ClassFileContent .= "}";
                }
            } else {
                $ClassFileContent = file_get_contents($ClassFilePath);
                $ClassFileContent = SYS()->replace($ClassFileContent, $StructIncludeString, $StructIncludeSearchStartString, $StructIncludeSearchEndString);
                $ClassFileContent = SYS()->replace($ClassFileContent, $DeclarationString, $DeclarationSearchStartString, $DeclarationSearchEndString);
            }
            file_put_contents($ClassFilePath, $ClassFileContent);
            //echo $ClassFileContent;die;
        }
    }
}
