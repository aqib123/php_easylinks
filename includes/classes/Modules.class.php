<?php
if(!class_exists("clsModules")){
    class clsModules{
        protected static $_instance = null;
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        function __construct(){

        }

        function __destruct(){
            //
        }

        function get($ModuleID){
            $sql = "select ".DB()->modules->Asterisk." from ".DB()->modules." where ".DB()->modules->id."=".$ModuleID." and ".DB()->modules->ModuleEnabled." = 1 group by ".DB()->modules->id." ";
            $Module = DB()->get_row($sql);
            return $Module;
        }

        function get_all($OrderBy = "ModulePosition", $OutputType = OBJECT){
            $sql = "select ".DB()->modules->Asterisk." from ".DB()->modules." where ".DB()->modules->ModuleEnabled." = 1 group by ".DB()->modules->id." ORDER BY ".DB()->modules.".".$OrderBy." ";
            $Modules = DB()->get_results($sql, $OutputType);
            return $Modules;
        }

        function get_all_by_level($LevelAccessKey, $OrderBy = "ModulePosition", $OutputType = OBJECT_K){
            $sql = "select ".DB()->modules->Asterisk.", ".DB()->level_modules->id." as LevelModuleID, ".DB()->level_modules->LevelModuleEnabled." from ".DB()->modules." inner join ".DB()->level_modules." on ".DB()->modules->id." = ".DB()->level_modules->ModuleID." inner join ".DB()->levels." on ".DB()->levels->id." = ".DB()->level_modules->LevelID." where ".DB()->modules->ModuleEnabled." = 1 and ".DB()->levels->LevelAccessKey." = '".$LevelAccessKey."' group by ".DB()->modules->id." ORDER BY ".DB()->modules.".".$OrderBy." ";
            $LevelModules = DB()->get_results($sql, $OutputType);
            return $LevelModules;
        }

        function get_all_by_user($UserID, $OrderBy = "ModulePosition", $OutputType = OBJECT){
            $sql = "select ".DB()->modules->Asterisk.", ".DB()->levels->Asterisk.", ".DB()->level_modules->Asterisk." from ".DB()->modules." inner join ".DB()->level_modules." on ".DB()->modules->id." = ".DB()->level_modules->ModuleID." inner join ".DB()->levels." on ".DB()->levels->id." = ".DB()->level_modules->LevelID." inner join ".DB()->user_levels." on ".DB()->levels->id." = ".DB()->user_levels->LevelID." where ".DB()->modules->ModuleEnabled." = 1 and ".DB()->user_levels->UserID." = ".$UserID." group by ".DB()->modules->id." ORDER BY ".DB()->modules.".".$OrderBy." ";
            $LevelModules = DB()->get_results($sql, $OutputType);
            return $LevelModules;
        }

        function get_by_level($LevelIDs, $OrderBy = "ModulePosition", $OutputType = ARRAY_A){
            $sql = "select * from ".DB()->modules." inner join ".DB()->level_modules." on ".DB()->modules.".id=".DB()->level_modules.".ModuleID where LevelID in (".implode(",", $LevelIDs).") and ModuleEnabled=1 and LevelModuleEnabled=1"." ORDER BY ".DB()->modules.".".$OrderBy." ";
            $modules = DB()->get_results($sql, $OutputType);

            return $modules;
        }

        function is_module_enabled($ModuleName){
            global $LoggedInUser;
            $ModuleEnabled = false;
            $Modules = $LoggedInUser->Modules;
            foreach ($Modules as $Module){
                if(strtolower($Module["ModuleName"]) == strtolower($ModuleName) && $Module["LevelModuleEnabled"] == 1){
                    $ModuleEnabled = true;
                    break;
                }
            }

            return $ModuleEnabled;
        }
    }
}

global $elModules;
if(class_exists("clsModules") && !$elModules){
    $elModules = new clsModules();
}

if(!function_exists("elModules")){
    function elModules() {
        return clsModules::instance();
    }
}
$GLOBALS['elModules'] = elModules();