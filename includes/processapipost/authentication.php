<?php
if($act == "login" && SYS()->is_valid($_POST["username"]) && SYS()->is_valid($_POST["password"])){
    $username = $_POST["username"];
    $password = $_POST["password"];
    $jsonData = ["success"=> false, "message" => "Unable to login. Please try again"];
    $LoginData = elUsers()->login([
        "username" => $username,
        "password" => $password
    ], "api");
    if($LoginData["loggedin"] == true){
        $apiUser = LoggedInUser();
        $jsonData['success'] = true;
        $jsonData['message'] = "User loggedin successfully";
        $jsonData["user"] = [
            "id"            => $apiUser->id,
            "email_address" => $apiUser->user_email,
            "display_name"  => $apiUser->display_name,
            "user_nicename" => $apiUser->user_nicename,
            "profile_image" => SYS()->get_site_url(SYS()->is_valid($apiUser->user_picture) ? $apiUser->user_picture : "images/user.png"),
            "api_key"       => $apiUser->user_api_token

        ];
    }
    SYS()->print_json_encode($jsonData);
} else if($act == "get_api_key" && SYS()->is_valid($_POST["email"])){
    $EmailAddress = $_POST["email"];
    $jsonData = ["success"=> false, "api_key" => "", "message" => "Unable to get api key"];
    $User = elUsers()->get_api_key($EmailAddress);

    if($User){
        $jsonData['success'] = true;
        $jsonData['api_key'] = $User->UserApiToken;
        $jsonData['message'] = "Api Key retrieved successfully";
    }
    SYS()->print_json_encode($jsonData);
} else if($act == "verify"){
    $jsonData = ["success" => false, "message" => "Api-Key verification failed"];
    $UserApiToken = $_GET["api_key"];
    $apiUser = elUsers()->login_by_api_key($UserApiToken, $act);
    if($apiUser){
        $jsonData['success'] = true;
        $jsonData['message'] = "Api-Key verified successfully";

        elUsers()->create_constants($apiUser->id);
    }

    SYS()->print_json_encode($jsonData);
}
//if(SYS()->is_valid($_POST["apiusername"]) && SYS()->is_valid($_POST["apipassword"])){
//    $apiusername = $_POST["apiusername"];
//    $apipassword = $_POST["apipassword"];
//    $jsonData = elUsers()->login(array("username" => $apiusername, "password" => $apipassword));
//    if($jsonData["loggedin"] == true){
//        $apiUser = LoggedInUser();
//    }
//} else if($post_act == "verify_api_key" || SYS()->is_valid($_POST["api_key"])){
//    $UserApiToken = $_POST["api_key"];
//    $jsonData = array("verified" => false, "message" => "verification failed");
//    $apiUser = elUsers()->login_by_api_key($UserApiToken);
//    if($apiUser){
//        $jsonData['verified'] = true;
//        $jsonData['message'] = "Api-Key verified successfully";
//    }

//    if($post_act == "verify_api_key")
//        SYS()->json_encode($jsonData);
//}