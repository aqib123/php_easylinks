<?php
if($get_act == "get_traffic_statistics"){
    $jsonData = array("success" => false, "result" => "", "content" => "", "paidtrafficids" => array());
    if($apiUser){
        $ref = isset($_GET["ref"])?$_GET["ref"]:"";
        $paidtrafficids = isset($_GET["paidtrafficids"]) && !empty($_GET["paidtrafficids"])?" and id in(".$_GET["paidtrafficids"].") ":"";
        $paidtraffic_sale_type = isset($_GET["paidtraffic_sale_type"])?$_GET["paidtraffic_sale_type"]:"";
        ob_start();
?>
<div class="traffic_statistics">
    {error}
    <div class="clearfix">
        <a class="btn btn-primary btn-create-new-paid-traffic pull-right" data-toggle="customcollapse" data-target="#create-new-paid-traffic">Create New Campaign</a>
    </div>
    <br />
    <div id="create-new-paid-traffic" class="collapse">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <form id="create-new-paid-traffic-form" class="form-horizontal easylink-form" method="post" role="form" action="">
                    <div class="box box-gray- box-solid- box-default" id="Retargeting">
                        <div class="box-header with-border">
                            <h3 class="box-title">New Campaign</h3>
                            <div class="box-tools pull-right">
                                <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                <button class="btn btn-box-tool" data-toggle="customcollapse" data-target="#create-new-paid-traffic"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body box-top-padded">
                            <div class="form-group select-form-group has-feedback">
                                <label for="VendorID" class="col-sm-3 col-xs-12 control-label">Traffic Provider</label>
                                <div class="col-sm-9 col-xs-12 ">
                                    <div class="<?php echo ($apiUser->user_login != "ignite" || isset($_GET['provider-add-allowed']))?"input-group right-addon":"";?>">
                                        <select name="VendorID" id="VendorID" class="form-control select2" style="width: <?php echo ($apiUser->user_login != "ignite" || isset($_GET['provider-add-allowed']))?99:100;?>%" data-select="select">
                                            <option value="">Select Traffic Provider</option>
                                            <?php
        if($apiUser->user_login == "ignite"){
            //$sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$apiUser->id." and md5(ReferenceID) = '".md5($ref)."' order by VendorName";
            $sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$apiUser->id." or (userid = ".$apiUser->id." and md5(ReferenceID) = '".md5($ref)."') order by VendorName";
            if(isset($_GET['provider-add-allowed']))
                $sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$apiUser->id." or (userid = ".$apiUser->id." and md5(ReferenceID) = '".md5($ref)."') order by VendorName";
            else
                $sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$apiUser->id." order by VendorName";
        }else {
            $sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$apiUser->id." order by VendorName";
        }

        $vendors = DB()->get_results($sql);
        foreach($vendors as $vendor){
											?>
                                            <option value="<?php echo $vendor->id;?>"><?php echo $vendor->VendorName;?></option>
                                            <?php
        }
											?>
                                        </select>
                                        <?php if($apiUser->user_login != "ignite" || isset($_GET['provider-add-allowed'])){ ?>
                                        <a href="javascript:" class="input-group-addon" id="create-new-vendor-add-" data-toggle="modal" data-target="#create-new-vendor"><i class="fa fa-plus"></i></a>
                                        <?php } ?>
                                    </div>
                                    <small class="help-block with-errors"></small>
                                </div>
                            </div>

                            <div class="form-group select-form-group has-feedback">
                                <label for="GroupID" class="col-sm-3 col-xs-12 control-label">Choose Group</label>
                                <div class="col-sm-9 col-xs-12 ">
                                    <div class="<?php echo ($apiUser->user_login != "ignite" || isset($_GET['group-add-allowed']))?"input-group right-addon":"";?>">
                                        <select name="GroupID" id="GroupID" class="form-control select2" style="width: <?php echo ($apiUser->user_login != "ignite" || isset($_GET['group-add-allowed']))?99:100;?>%" data-select="select">
                                            <option value="">Select Group</option>
                                            <?php
        if($apiUser->user_login == "ignite"){
            if(isset($_GET['group-add-allowed']))
                $sql = "select * from ".DB()->groups." where GroupType='PaidTraffic' and userid = ".$apiUser->id." or (userid = ".$apiUser->id." and md5(ReferenceID) = '".md5($ref)."') order by GroupName";
            else
                $sql = "select * from ".DB()->groups." where GroupType='PaidTraffic' and userid = ".$apiUser->id." order by GroupName";
        } else{
            $sql = "select * from ".DB()->groups." where GroupType='PaidTraffic' and userid = ".$apiUser->id." order by GroupName";
        }

        $groups = DB()->get_results($sql);
        foreach($groups as $group){
											?>
                                            <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                            <?php } ?>
                                        </select>
                                        <?php if($apiUser->user_login != "ignite" || isset($_GET['group-add-allowed'])){ ?>
                                        <a href="javascript:" class="input-group-addon" id="create-new-group-add-" data-toggle="modal" data-target="#create-new-group"><i class="fa fa-plus"></i></a>
                                        <?php } ?>
                                    </div>
                                    <small class="help-block with-errors"></small>
                                </div>
                            </div>

                            <div class="form-group select-form-group has-feedback">
                                <label for="LeadCapturePageID" class="col-sm-3 col-xs-12 control-label">Lead Capture Page</label>
                                <div class="col-sm-9 col-xs-12 ">
                                    <div class="input-group-">
                                        <select name="LeadCapturePageID" id="LeadCapturePageID" class="form-control select2" style="width: 100%" data-select="select">
                                            {LeadCapturePages}
                                        </select>
                                        <small class="help-block with-errors"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="VisibleLink" class="col-sm-3 control-label">Visible Link</label>
                                <div class="col-sm-9">
                                    <input type="text" value="" class="form-control " id="VisibleLink" name="VisibleLink" placeholder="Visible Link" required="required" <?php echo NAME_PATTERN;?> />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <small class="help-block with-errors"></small>
                                    <small class="help-block text-left text-light-blue no-margin" id="paidtrafficurl"></small>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="TrafficCostType" class="col-sm-3 control-label">Traffic Cost</label>
                                <div class="col-sm-9 smallradio- smallcheck-">
                                    <input type="text" value="" class="form-control " id="TrafficCost" name="TrafficCost" placeholder="Traffic Cost" required="required" <?php echo FLOAT_PATTERN;?> />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <small class="help-block with-errors"></small>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="NoOfClicks" class="col-sm-3 control-label"># Of Clicks</label>
                                <div class="col-sm-9">
                                    <input type="text" value="" class="form-control " id="NoOfClicks" name="NoOfClicks" placeholder="# Of Clicks" required="required" <?php echo INT_PATTERN ;?> />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <small class="help-block with-errors"></small>
                                </div>
                            </div>

                            <div class="form-group left-addon">
                                <label for="StartDate" class="col-sm-3 control-label">Start Date</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" data-linked="#StartDate" value="" class="form-control  input-singledate-time with-left-addon" id="StartDateInput" name="StartDateInput" />
                                        <input type="hidden" value="" id="StartDate" name="StartDate" />
                                        <small class="help-block with-errors"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="EndDate" class="col-sm-3 control-label">End Date</label>
                                <div class="col-sm-9">
                                    <div class="input-group left-addon">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" data-linked="#EndDate" value="" class="form-control  input-singledate-time with-left-addon" id="EndDateInput" name="EndDateInput" />
                                        <input type="hidden" value="" id="EndDate" name="EndDate" />
                                        <small class="help-block with-errors"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <input type="button" class="btn btn-success create-new-paid-traffic-save" value="Save" />
                                    <button type="button" class="btn btn-danger create-new-paid-traffic-cancel">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="act" value="save_paidtraffic" />
                </form>
            </div>
        </div>
    </div>
    <br />
    <div id="linkslist" class="table-responsive collapse in" aria-expanded="true">
        <table class="table table-condensed table-bordered table-striped table-paidtraffic-tracking-stats data-table responsive- nowrap" data-nobuttons="true">
            <thead>
                <tr>
                    <th><span>Vendor</span></th>
                    <th><span>Link Name</span></th>
                    <th><span>URL</span></th>
                    <th class="text-center"><span>Date</span></th>
                    <th class="text-center stats-column"><span>Raw</span></th>
                    <th class="text-center stats-column"><span>Unique</span></th>
                    <th class="text-center action-column"><span>Leads</span></th>
                    <th class="text-center action-column"><span>Optin %</span></th>
                    <?php if($paidtraffic_sale_type == "conv") { ?>
                    <th class="text-center sale-column"><span>F/E #</span></th>
                    <th class="text-center sale-column"><span>F/E $</span></th>
                    <th class="text-center sale-column"><span>F/E %</span></th>
                    <?php } ?>
                    <th class="text-center cost-column"><span>CPC</span></th>
                    <th class="text-center cost-column"><span>CPL</span></th>
                    <?php if($paidtraffic_sale_type == "conv") { ?>
                    <th class="text-center cost-column"><span>CPS</span></th>
                    <th class="text-center cost-column"><span>EPC</span></th>
                    <th class="text-center cost-column"><span>Net Profit $</span></th>
                    <th class="text-center non-score-column"><span class="text-center">Non Scoring</span></th>
                    <?php } ?>
                    <th class="text-center score-column"><span class="text-center">Scoring Blocks</span></th>
                    <th class="text-center score-column-" data-toggle="qtiptooltip" title="Overall Score"><span class="text-center">Score</span></th>
                </tr>
            </thead>
            <tbody>
                <?php
        $sql = "select * from ".DB()->paidtraffics." where userid=".$apiUser->id." and md5(ReferenceID) = '".md5($ref)."'";

        $paidtraffics = DB()->get_results($sql);
        foreach($paidtraffics as $paidtraffic){
            $userid = $LoggedInUser->id;
            $TrafficCost = $paidtraffic->TrafficCost;
            $jsonData["paidtrafficids"][] = $paidtraffic->id;

            $VendorName = DB()->get_var("select VendorName from ".DB()->vendors." where id=".$paidtraffic->VendorID);
            $DomainType = DB()->get_var("select DomainType from ".DB()->domains." where id=".$paidtraffic->DomainID);
            $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
            $UniqueClicks = DB()->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." group by ClickIp) as UniqueClicks") * 1;
            //$RawClicks -= $UniqueClicks;
            $TotalActions = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='action'") * 1;
            $PixelFires = DB()->get_var("select count(FireIp) as FireCount from ".DB()->paidtraffic_pixel_fires." where PaidTrafficID=".$paidtraffic->id) * 1;
            $BadClicks = DB()->get_var("select count(ClickIp) as ClickCount from ".DB()->paidtraffic_clicks." where BotName <> '' and BotName is not NULL and PaidTrafficID=".$paidtraffic->id) * 1;
            $TotalSales = DB()->get_var("select sum(UnitPrice) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
            $TotalSalesCount = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
            $EasyLinkClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$paidtraffic->EasyLinkID." and ClickType='sa'") * 1;
            $NetProfit = $TotalSales - $TrafficCost;
            $paidtrafficurl = get_paidtrafficurl($paidtraffic, '', $apiUser);

            //$PercentOptins = $RawClicks>0?(($TotalActions/$RawClicks) * 100):0;
            $PercentOptins = $UniqueClicks>0?(($TotalActions/$UniqueClicks) * 100):0;
            $CPC = $RawClicks==0?0:$TrafficCost/$RawClicks;
            $CPA = $TotalActions==0?0:$TrafficCost/$TotalActions;
            $CPS = $TotalSales==0?0:$TrafficCost/$TotalSales;
            $EPC = $RawClicks==0?0:$TotalSales/$RawClicks;
            $PercentSales = $EasyLinkClicks>0?(($TotalSales/$EasyLinkClicks) * 100):0;


            $TrafficQuality = get_traffic_quallity($paidtraffic, $apiUser);
            $LeadScore = get_lead_score($paidtraffic, $apiUser);
            $TimeDelivered = get_time_delivered($paidtraffic, $apiUser);
            $SaleQuality = get_sale_quality($paidtraffic, $apiUser);
            $OverallProfitability = get_overall_profitability($paidtraffic, $apiUser);
            $OverallDeliverability = get_overall_deliverability($paidtraffic, $apiUser);

            $SaleQuality = $SaleQuality > 100? 100:$SaleQuality;
            $OverallProfitability = $OverallProfitability > 100? 100:$OverallProfitability;
            
            $VendorOverallScore = get_vendor_overall_score($paidtraffic, $apiUser, $TrafficQuality, $TimeDelivered, $LeadScore, $OverallDeliverability);
				?>
                <tr>
                    <td><span><?php echo $VendorName;?></span></td>
                    <td><a href="<?php echo "{LinkURL_".$paidtraffic->id."}";?>" class="easylinks_qtip" data-image="<?php echo "{data-image_".$paidtraffic->id."}";?>" data-title="<?php echo "{data-title_".$paidtraffic->id."}";?>" target="_blank"><?php echo "{LinkName_".$paidtraffic->id."}";?></a></td>
                    <td>
                        <div class="grey-scale">
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtrafficurl;?>" class="btn btn-sm btn-links copy" data-toggle="qtiptooltip" title="Copy <?php echo $paidtrafficurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo "{page_url}&act=delete_paidtraffic&PaidTrafficID=".$paidtraffic->id;?>" class="btn btn-sm btn-links btn-delete" data-toggle="qtiptooltip" title="Delete PaidTraffic"><i class="fa fa-trash"></i></a>
                            <a href="<?php echo "{page_url}&act=reset_paidtraffic&PaidTrafficID=".$paidtraffic->id;?>" class="btn btn-sm btn-links btn-reset" data-toggle="qtiptooltip" title="Reset PaidTraffic"><i class="fa fa-ban"></i></a>
                            <input type="hidden" id="ptlink-<?php echo $paidtraffic->id;?>" value="<?php echo $paidtrafficurl;?>" />
                        </div>
                    </td>
                    <td class="text-center"><span><?php echo date("d/m/Y", $paidtraffic->DateAdded);?></span></td>
                    <td class="text-center stats-column"><span><?php echo get_formated_number($RawClicks);?></span></td>
                    <td class="text-center stats-column"><span><?php echo get_formated_number($UniqueClicks);?></span></td>
                    <td class="text-center action-column"><span><?php echo get_formated_number($TotalActions);?></span></td>
                    <td class="text-center action-column"><span><?php echo get_formated_number(round($PercentOptins, 2));?>%</span></td>
                    <?php if($paidtraffic_sale_type == "conv") { ?>
                    <td class="text-center sale-column"><span><?php echo get_formated_number($TotalSalesCount);?></span></td>
                    <td class="text-center sale-column"><span>$<?php echo get_formated_number($TotalSales);?></span></td>
                    <td class="text-center sale-column"><span><?php echo get_formated_number(round($PercentSales, 2));?>%</span></td>
                    <?php } ?>
                    <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPC, 2));?></span></td>
                    <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPA, 2));?></span></td>
                    <?php if($paidtraffic_sale_type == "conv") { ?>
                    <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPS, 2));?></span></td>
                    <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($EPC, 2));?></span></td>
                    <td class="text-center cost-column"><span>$<?php echo get_formated_number($NetProfit);?></span></td>
                    <td class="text-center non-score-column">
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($SaleQuality));?>" data-toggle="qtiptooltip" title="Sale Quality"><span><?php echo get_formated_number(round($SaleQuality));?></span></div>
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallProfitability));?>" data-toggle="qtiptooltip" title="Overall Profitability"><span><?php echo get_formated_number(round($OverallProfitability));?></span></div>
                    </td>
                    <?php } ?>
                    <td class="text-center score-column">
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TrafficQuality));?>" data-toggle="qtiptooltip" title="Traffic Quality"><span><?php echo get_formated_number(round($TrafficQuality));?></span></div>
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TimeDelivered));?>" data-toggle="qtiptooltip" title="Time Delivered"><span><?php echo get_formated_number(round($TimeDelivered));?></span></div>
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($LeadScore));?>" data-toggle="qtiptooltip" title="Lead Score"><span><?php echo get_formated_number(round($LeadScore));?></span></div>
                        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallDeliverability));?>" data-toggle="qtiptooltip" title="Overall Deliverability"><span><?php echo get_formated_number(round($OverallDeliverability));?></span></div>
                    </td>
                    <td class="text-center score-column-">
                        <div class="vendor-stats-box-sm" data-toggle="qtiptooltip" title="Overall Score"><?php echo get_formated_number(round($VendorOverallScore));?>%</div>
                    </td>
                </tr>
                <?php
        }
				?>
            </tbody>
        </table>
    </div>
    <br />
    <div class="clearfix">
        <a class="btn btn-primary btn-create-new-paid-traffic pull-right" data-toggle="customcollapse" data-target="#create-new-paid-traffic">Create New Campaign</a>
    </div>
    <br />
    <div class="modal fade" id="create-new-vendor" tabindex="-1" role="dialog" aria-labelledby="create-new-vendor-label">
        <form id="create-new-vendor-form" class="form-horizontal easylink-form" method="post" role="form" action="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="create-new-vendor-label">Create Traffic Provider</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-body box-top-padded">
                                    <div class="form-group has-feedback">
                                        <label for="VendorName" class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="VendorName" name="VendorName" placeholder="Name" required="required" <?php NAME_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="WebsiteUrl" class="col-sm-4 control-label">Website Url</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="WebsiteUrl" name="WebsiteUrl" placeholder="Website Url" <?php echo URL_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="SkypeName" class="col-sm-4 control-label">Skype Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="SkypeName" name="SkypeName" placeholder="Skype Name" <?php echo NAME_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="LinkedIn" class="col-sm-4 control-label">Linked In</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="LinkedIn" name="LinkedIn" placeholder="Linked In" <?php echo URL_PATTERN?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="FacebookID" class="col-sm-4 control-label">Facebook ID</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="FacebookID" name="FacebookID" placeholder="Facebook ID" <?php echo URL_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="EmailAddress" class="col-sm-4 control-label">Email Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="EmailAddress" name="EmailAddress" placeholder="Email Address" <?php echo EMAIL_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="VendorTags" class="col-sm-4 control-label">Tags</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control tokenfieldinput ignore" id="VendorTags" name="VendorTags" placeholder="Vendor Tags" />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="AdditionalNotes" class="col-sm-4 control-label">Additional Notes</label>
                                        <div class="col-sm-8">
                                            <textarea rows="3" class="form-control " id="AdditionalNotes" name="AdditionalNotes" placeholder="Additional Notes"></textarea>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default create-new-vendor-cancel" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary create-new-vendor-save">Save changes</button>
                        <input type="hidden" name="act" value="save_vendor" />
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="create-new-group" tabindex="-1" role="dialog" aria-labelledby="create-new-vendor-label">
        <form id="create-new-group-form" class="form-horizontal easylink-form" method="post" role="form" action="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="create-new-group-label">Create Group</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-body box-top-padded">
                                    <div class="form-group has-feedback">
                                        <label for="GroupName" class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" value="" class="form-control " id="GroupName" name="GroupName" placeholder="Group Name" required="required" <?php echo NAME_PATTERN;?> />
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <small class="help-block with-errors"></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default create-new-vendor-cancel" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary create-new-vendor-save">Save changes</button>
                        <input type="hidden" name="act" value="save_group" />
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
<?php
        $contents = ob_get_contents();
        ob_end_clean();

        $jsonData["result"] = "successful";
        $jsonData["content"] = $contents;
    } else {
        $jsonData["message"] = "verification failed";
    }
    echo json_encode($jsonData);
    die;
}