<?php
$apiUserID = $apiUser->id;

if($act == "linkbank-statuses"){
    $jsonData = ["success" => true, "statuses" => []];
    $LinkBankSaleType = SYS()->is_valid($_POST["LinkBankSaleType"]) ? $_POST["LinkBankSaleType"] : "";

    $Statuses = SYS()->LinkStatusesAll;

    foreach ($Statuses as $Status) {
        if(SYS()->is_valid($Status))
            $jsonData["statuses"][] = $Status;
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-columns"){
    $jsonData = ["success" => true, "columns" => []];
    $LinkBankSaleType = SYS()->is_valid($_POST["LinkBankSaleType"]) ? $_POST["LinkBankSaleType"] : "";

    $Columns = DBLinkbanks()->get_columns($LinkBankSaleType);

    foreach ($Columns as $Column) {
        if(SYS()->is_valid($Column))
            $jsonData["columns"][] = $Column;
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-vendors"){
    $jsonData = ["success" => false, "vendors" => []];

    $Vendors = DBVendors()->get_all(DBVendors()->VendorType." = 'Affiliate' and ".DBVendors()->userid." = ".$apiUserID, 0, -1, "VendorName");
    if($Vendors){
        $jsonData["success"] = true;
        foreach ($Vendors as $Vendor) {
            $jsonData["vendors"][] = [
                "VendorID"      => $Vendor->id,
                "VendorName"    => $Vendor->VendorName
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-groups"){
    $jsonData = ["success" => false, "groups" => []];

    $Groups = DBGroups()->get_all(DBGroups()->GroupType." = 'LinkBank' and ".DBGroups()->userid." = ".$apiUserID, 0, -1, "GroupName");
    if($Groups){
        $jsonData["success"] = true;
        foreach ($Groups as $Group) {
            $jsonData["groups"][] = [
                "GroupID"           => $Group->id,
                "GroupAccessKey"    => $Group->GroupAccessKey,
                "GroupName"         => $Group->GroupName
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-domains"){
    $jsonData = ["success" => false, "domains" => []];

    $Domains = DBDomains()->get_all(DBDomains()->DomainType." = 'userdomain' and ".DBDomains()->userid." = ".$apiUserID, 0, -1, "DomainName");
    if($Domains){
        $jsonData["success"] = true;
        foreach ($Domains as $Domain) {
            $jsonData["domains"][] = [
                "DomainID"           => $Domain->id,
                "DomainAccessKey"    => $Domain->DomainAccessKey,
                "DomainName"         => $Domain->DomainName,
                "DomainSlug"         => $Domain->DomainSlug,
                "DomainUrl"         => $Domain->DomainUrl
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-pixels"){
    $jsonData = ["success" => false, "pixels" => []];

    $Pixels = DBPixels()->get_all(DBPixels()->userid." = ".$apiUserID, 0, -1, "PixelName");
    if($Pixels){
        $jsonData["success"] = true;
        foreach ($Pixels as $Pixel) {
            $jsonData["pixels"][] = [
                "PixelID"           => $Pixel->id,
                "PixelAccessKey"    => $Pixel->PixelAccessKey,
                "PixelName"         => $Pixel->PixelName,
                "PixelType"         => $Pixel->PixelType
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-pending-pages"){
    $jsonData = ["success" => false, "pages" => []];

    $Pages = DBRedirectLinks()->get_all(DBRedirectLinks()->RedirectLinkType." = 'PendingPage' and ".DBRedirectLinks()->userid." = ".$apiUserID, 0, -1, "RedirectLinkName");
    if($Pages){
        $jsonData["success"] = true;
        foreach ($Pages as $Page) {
            $jsonData["pages"][] = [
                "PageID"            => $Page->id,
                "PageName"          => $Page->RedirectLinkName,
                "PageURL"           => $Page->RedirectLinkURL
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-complete-pages"){
    $jsonData = ["success" => false, "pages" => []];

    $Pages = DBRedirectLinks()->get_all(DBRedirectLinks()->RedirectLinkType." = 'CompletePage' and ".DBRedirectLinks()->userid." = ".$apiUserID, 0, -1, "RedirectLinkName");
    if($Pages){
        $jsonData["success"] = true;
        foreach ($Pages as $Page) {
            $jsonData["pages"][] = [
                "PageID"            => $Page->id,
                "PageName"          => $Page->RedirectLinkName,
                "PageURL"           => $Page->RedirectLinkURL
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-platforms"){
    $jsonData = ["success" => false, "platforms" => []];

    $Platforms = DBGroups()->get_all(DBGroups()->GroupType." = 'AffiliatePlatform' and ".DBGroups()->userid." = ".$apiUserID, 0, -1, "GroupName");
    if($Platforms){
        $jsonData["success"] = true;
        foreach ($Platforms as $Platform) {
            $jsonData["platforms"][] = [
                "PlatformID"           => $Platform->id,
                "PlatformAccessKey"    => $Platform->GroupAccessKey,
                "PlatformName"         => $Platform->GroupName
            ];
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-master-campaigns"){
    $jsonData = ["success" => false, "mastercampaigns" => []];

    $MasterCampaignss = DBMasterCampaigns()->get_all(DBMasterCampaigns()->MasterCampaignStatus." = 'active' and ". DBMasterCampaigns()->userid." = ".$apiUserID, 0, -1, "MasterCampaignName");
    if($MasterCampaignss){
        $jsonData["success"] = true;
        foreach ($MasterCampaignss as $MasterCampaigns) {
            $jsonData["mastercampaigns"][] = [
                "MasterCampaignID"           => $MasterCampaigns->id,
                "MasterCampaignAccessKey"    => $MasterCampaigns->MasterCampaignAccessKey,
                "MasterCampaignName"         => $MasterCampaigns->MasterCampaignName,
            ];
        }
    }

    SYS()->print_api_json($jsonData);
}