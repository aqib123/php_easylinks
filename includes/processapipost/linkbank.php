<?php
$apiUserID = $apiUser->id;

if($act == "linkbank-list"){
    $jsonData = ["success" => false, "linkbanks" => []];
    $PageNo = SYS()->is_numeric($_POST['page']) ? $_POST['page'] : 1;
    $PerPage = SYS()->is_numeric($_POST['limit']) ? $_POST['limit'] : -1;
    $LinkBankSaleType = SYS()->is_valid($_POST["LinkBankSaleType"]) ? $_POST["LinkBankSaleType"] : "";
    $LinkBanksType = SYS()->is_valid($_POST["LinkBanksType"]) ? $_POST["LinkBanksType"] : "";

    $Columns = DBLinkbanks()->get_columns($LinkBankSaleType);
    $QueryData = [];
    $LinkBankStauses = SYS()->LinkStatusesAll;

    if(SYS()->is_string($_POST["OrderBy"]))
        $QueryData["column_no"] = array_search($_POST["OrderBy"], $Columns);

    if(SYS()->is_string($_POST["OrderType"]))
        $QueryData["order_type"] = $_POST["OrderType"];

    if((SYS()->is_numeric($_POST["VendorID"]) && $_POST["VendorID"] > 0) || (SYS()->is_numeric($_POST["GroupID"]) && $_POST["GroupID"] > 0) || SYS()->is_numeric($_POST["Keywords"]))
        $QueryData["act"] = "app_filters";

    if(SYS()->is_valid($_POST["VendorID"]) && $_POST["VendorID"] > 0)
        $QueryData["VendorID"] = $_POST["VendorID"];

    if(SYS()->is_valid($_POST["GroupID"]) && $_POST["GroupID"] > 0)
        $QueryData["GroupID"] = $_POST["GroupID"];

    if(SYS()->is_valid($_POST["Keywords"]))
        $QueryData["keywords"] = $_POST["Keywords"];


    list($Rows, $TotalRows) = DBLinkbanks()->get_pagged($LinkBanksType, $apiUserID, $LinkBankSaleType, $QueryData, $PageNo, $PerPage);
    if($Rows){
        $jsonData["success"] = true;
        foreach ($Rows as $LinkBank) {
            $FormatedLinkBank = DBLinkbanks()->get_formated($LinkBank, $apiUserID, $LinkBanksType, $LinkBankSaleType);
            $jsonData["linkbanks"][] = $FormatedLinkBank;
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-get"){
    $jsonData = ["success" => false, "linkbank" => []];
    $LinkBankID = SYS()->is_numeric($_POST['id']) ? $_POST['id'] : 0;
    $LinkBankStauses = SYS()->LinkStatusesAll;

    $LinkBank = DBLinkbanks()->get_row($LinkBankID, $apiUserID);
    if($LinkBank){
        $jsonData["success"] = true;
        $FormatedLinkBank = DBLinkbanks()->get_formated($LinkBank, $apiUserID, "", "");

        $jsonData["linkbank"] = $FormatedLinkBank;
    }
    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-save"){
    $jsonData = ["success" => false, "message" => "Linkbank saving failed"];

    $data = array(
        "userid"                                => $apiUserID,
        "VendorID"                              => SYS()->is_valid($_POST['VendorID']) ? $_POST['VendorID'] : 0,
        "LinkName"                              => SYS()->is_valid($_POST['LinkName']) ? $_POST['LinkName'] : "",
        "DestinationURL"                        => SYS()->is_valid($_POST['DestinationURL']) ? $_POST['DestinationURL'] : "",
        "VisibleLink"                           => SYS()->is_valid($_POST['VisibleLink']) ? $_POST['VisibleLink'] : "",
        "UseAdminDomain"                        => SYS()->is_valid($_POST["UseAdminDomain"]) ? $_POST["UseAdminDomain"] : 0,
        "DomainID"                              => SYS()->is_valid($_POST["AdminDomainID"]) ? $_POST["AdminDomainID"] : $_POST['DomainID'],
        "CloakURL"                              => SYS()->is_valid($_POST["CloakURL"]) ? $_POST["CloakURL"] : 0,
        "TrackEPC"                              => SYS()->is_valid($_POST["TrackEPC"]) ? $_POST["TrackEPC"] : 0,
        "GroupID"                               => SYS()->is_valid($_POST['GroupID']) ? $_POST['GroupID'] : 0,
        "StartDate"                             => SYS()->is_valid($_POST['StartDate']) ? strtotime($_POST['StartDate']) : "",
        "EndDate"                               => SYS()->is_valid($_POST['EndDate']) ? strtotime($_POST['EndDate']) : "",
        "PendingPageID"                         => SYS()->is_valid($_POST['PendingPageID']) ? $_POST['PendingPageID'] : 0,
        "CompletePageID"                        => SYS()->is_valid($_POST['CompletePageID']) ? $_POST['CompletePageID'] : 0,
        "AdditionalNotes"                       => SYS()->is_valid($_POST['AdditionalNotes']) ? $_POST['AdditionalNotes'] : "",
        "PageImage"                             => SYS()->is_valid($_POST['PageImage']) ? $_POST['PageImage'] : "",
        "LinkStatus"                            => SYS()->is_valid($_POST["LinkStatus"]) ? $_POST["LinkStatus"] : "pending",
        "LinkActive"                            => 0,
        "SalesConversions"                      => 0,
        "UnitPriceSales"                        => SYS()->is_valid($_POST['EndDate']) ? $_POST['UnitPriceSales'] : 0,
        "RedirectAfterLinkExpired"              => "",
        "PixelID"                               => SYS()->is_valid($_POST['EndDate']) ? $_POST['PixelID'] : 0,
        //"TrackingCodeForActions"                => $_POST['TrackingCodeForActions'],
        "TrackingCodeForSalesAndConversions"    => SYS()->is_valid($_POST['EndDate']) ? $_POST['TrackingCodeForSalesAndConversions'] : "",
        "AffiliatePlatformID"                   => SYS()->is_valid($_POST['EndDate']) ? $_POST['AffiliatePlatformID'] : 0,
        "MasterCampaignID"                      => SYS()->is_valid($_POST['EndDate']) ? $_POST['MasterCampaignID'] : 0,
        "DateAdded"                             => strtotime("now")
    );

    if(SYS()->is_numeric($_POST['LinkBankID'])){
        $LinkBankID = $_POST['LinkBankID'];
        $LinkBank = DBLinkbanks()->get_row($LinkBankID, $apiUserID);
        $UpdateLinkBank = true;
        if($LinkBank->VisibleLink != $data['VisibleLink'] || $LinkBank->DomainID != $data['DomainID']){
            if(!is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                $jsonData["success"] = false;
                $jsonData["message"] = "Visible Link for selected Domain already exist";
                $UpdateLinkBank = false;
            }
        }

        if($UpdateLinkBank){
            unset($data['DateAdded']);
            DB()->update(DBLinkbanks(), $data, ["id" => $LinkBankID]);
            update_linkbank_conversion_pixels($LinkBankID);

            $jsonData["success"] = true;
            $jsonData["message"] = "Link Updated Successfully";
        }
    }else{
        if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
            if(DB()->insert(DBLinkbanks(), $data)){
                update_linkbank_conversion_pixels(DBLinkbanks()->get_last_id());

                $jsonData["success"] = true;
                $jsonData["message"] = "Link Inserted Successfully";
            } else{
                $jsonData["success"] = false;
                $jsonData["message"] = "Link insertion failed";
                $redirect = false;
            }
        }else{
            $jsonData["success"] = false;
            $jsonData["message"] = "Visible Link for selected Domain already exist";
            $redirect = false;
        }
    }

    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-remove"){
    $jsonData = ["success" => false, "message" => "Linkbank removing failed"];
    if(SYS()->is_numeric($_POST['id'])){
        $LinkBankID = SYS()->is_numeric($_POST['id']) ? $_POST['id'] : 0;
        $LinkBank = DBLinkbanks()->get_row($LinkBankID, $apiUserID);

        if(!$LinkBank){
            $jsonData["message"] = "Invalid linkbank id";
        } else {
            delete_linkbank_status($LinkBankID);
            DB()->delete(DB()->linkbanks, ["id" => $LinkBankID]);
            DB()->delete(DB()->linkbank_saleinfo, ["LinkBankID" => $LinkBankID]);

            $jsonData["success"] = true;
            $jsonData["message"] = "Link Deleted Successfully";
        }
    } else {
        $jsonData["message"] = "Invalid linkbank id";
    }
    SYS()->print_api_json($jsonData);
} else if($act == "linkbank-remove"){
    $jsonData = ["success" => false, "message" => "Linkbank removing failed"];
    if(SYS()->is_numeric($_POST['id'])){
        $LinkBankID = SYS()->is_numeric($_POST['id']) ? $_POST['id'] : 0;
        $LinkBank = DBLinkbanks()->get_row($LinkBankID, $apiUserID);

        if(!$LinkBank){
            $jsonData["message"] = "Invalid linkbank id";
        } else {
            delete_linkbank_status($LinkBankID);
            DB()->delete(DB()->linkbanks, ["id" => $LinkBankID]);
            DB()->delete(DB()->linkbank_saleinfo, ["LinkBankID" => $LinkBankID]);

            $jsonData["success"] = true;
            $jsonData["message"] = "Link Deleted Successfully";
        }
    } else {
        $jsonData["message"] = "Invalid linkbank id";
    }
    SYS()->print_api_json($jsonData);
}