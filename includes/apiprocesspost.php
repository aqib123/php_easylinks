<?php
$errors = array();
if($_POST && isset($_POST['apiact'])){
    $act =  $_POST['apiact'];
    
    if($act == "get_apitoken"){
        if(empty($apiUser->apitoken)){
            $apitoken = md5(uniqid(microtime() . mt_rand(), true));
            while(DB()->get_row("select * from ".DB()->users." where apitoken='".$apitoken."'"))
                $apitoken = md5(uniqid(microtime() . mt_rand(), true));

            $apiUser->apitoken = $apitoken;
            DB()->update(DB()->users, array("apitoken" => $apitoken), array("id" => $apiUser->id));
        }
        $jsonData = array("result" => "successful", "apitoken" => $apiUser->apitoken);
        echo json_encode($jsonData);
        die;
    } else if($act == "save_group"){
        $data = array(
            "userid"            => $apiUser->id,
            "GroupName"         => $_POST['GroupName'],
            "GroupType"         => "PaidTraffic",
            "ReferenceID"       => $_GET["ref"],
            "DateAdded"         => strtotime("now"),
        );

        if(DB()->insert(DB()->groups, $data)){
            $jsonData["result"] = "successful";
            $jsonData["msg"] = "Group insertion successful";
            $jsonData["GroupID"] = DB()->insert_id;
            $jsonData["GroupName"] = $data["GroupName"];
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Group insertion failed";
        }

        echo json_encode($jsonData);
        die;
    } else if($act == "register_action_pixel_old") {
        $jsonData = array("PaidTrafficID" => 0, "PaidTrafficClickID" => 0, "PaidTrafficConversionID" => 0);
        $ip_address = $_POST["ip_address"];
        $ip_address2 = $_POST["ip_address2"];
        //$sql = "select ".DB()->paidtraffic_clicks.".* from ".DB()->paidtraffics." inner join ".DB()->paidtraffic_clicks." on ".DB()->paidtraffics.".id = ".DB()->paidtraffic_clicks.".PaidTrafficID where ".DB()->paidtraffics.".userid = ".$apiUser->id." and ".DB()->paidtraffic_clicks.".ClickIp = '".$ip_address."' and ".DB()->paidtraffics.".id not in (select PaidTrafficID from ".DB()->paidtraffic_conversions." where ConversionIp = '".$ip_address."' and ConversionType = 'action') order by ".DB()->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $sql = "select ".DB()->paidtraffic_clicks.".* from ".DB()->paidtraffics." inner join ".DB()->paidtraffic_clicks." on ".DB()->paidtraffics.".id = ".DB()->paidtraffic_clicks.".PaidTrafficID where ".DB()->paidtraffics.".userid = ".$apiUser->id." and ".DB()->paidtraffic_clicks.".ClickIp in ('".$ip_address."', '".$ip_address2."') and ".DB()->paidtraffics.".id not in (select PaidTrafficID from ".DB()->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action') order by ".DB()->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $paidtraffic_click = DB()->get_row($sql);
        if($paidtraffic_click){
            $data = array(
                    "PaidTrafficID"     =>  $paidtraffic_click->PaidTrafficID,
                    "DateAdded"         =>  strtotime("now"),
                    "ConversionType"    =>  "action",
                    "UnitPrice"         =>  0,
                    "ConversionIp"      =>  $paidtraffic_click->ClickIp,
                    "CountryCode"       =>  $paidtraffic_click->CountryCode,
                    "BotName"           =>  $paidtraffic_click->BotName,
                    "BrowserName"       =>  $paidtraffic_click->BrowserName,
                    "BrowserVersion"    =>  $paidtraffic_click->BrowserVersion,
                    "Platform"          =>  $paidtraffic_click->Platform,
                    "PaidTrafficConversionPixelID"     =>  0,
                );
            DB()->insert(DB()->paidtraffic_conversions, $data);

            $jsonData = array(
                "PaidTrafficID" => $paidtraffic_click->PaidTrafficID, 
                "PaidTrafficClickID" => $paidtraffic_click->id, 
                "PaidTrafficConversionID" => DB()->insert_id
            );
        }
        echo json_encode($jsonData);
        die;
    } else if($act == "register_action_pixel") {
        $jsonData = array("PaidTrafficID" => 0, "PaidTrafficClickID" => 0, "PaidTrafficConversionID" => 0, "PaidTrafficURL" => "");
        $ip_address = $_POST["ip_address"];
        $ip_address2 = $_POST["ip_address2"];
        //$sql = "select ".DB()->paidtraffic_clicks.".* from ".DB()->paidtraffics." inner join ".DB()->paidtraffic_clicks." on ".DB()->paidtraffics.".id = ".DB()->paidtraffic_clicks.".PaidTrafficID where ".DB()->paidtraffics.".userid = ".$apiUser->id." and ".DB()->paidtraffic_clicks.".ClickIp = '".$ip_address."' and ".DB()->paidtraffics.".id not in (select PaidTrafficID from ".DB()->paidtraffic_conversions." where ConversionIp = '".$ip_address."' and ConversionType = 'action') order by ".DB()->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $sql = "select ".DB()->paidtraffic_clicks.".* from ".DB()->paidtraffics." inner join ".DB()->paidtraffic_clicks." on ".DB()->paidtraffics.".id = ".DB()->paidtraffic_clicks.".PaidTrafficID where ".DB()->paidtraffics.".userid = ".$apiUser->id." and ".DB()->paidtraffic_clicks.".ClickIp in ('".$ip_address."', '".$ip_address2."') and ".DB()->paidtraffics.".id not in (select PaidTrafficID from ".DB()->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action' and ".DB()->paidtraffic_clicks.".DateAdded < (select DateAdded from ".DB()->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action' and PaidTrafficID = ".DB()->paidtraffic_clicks.".PaidTrafficID ORDER BY DateAdded desc LIMIT 0,1)) order by ".DB()->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $jsonData["sql"] = $sql;
        $paidtraffic_click = DB()->get_row($sql);
        if($paidtraffic_click){
            $data = array(
                    "PaidTrafficID"     =>  $paidtraffic_click->PaidTrafficID,
                    "DateAdded"         =>  strtotime("now"),
                    "ConversionType"    =>  "action",
                    "UnitPrice"         =>  0,
                    "ConversionIp"      =>  $paidtraffic_click->ClickIp,
                    "CountryCode"       =>  $paidtraffic_click->CountryCode,
                    "BotName"           =>  $paidtraffic_click->BotName,
                    "BrowserName"       =>  $paidtraffic_click->BrowserName,
                    "BrowserVersion"    =>  $paidtraffic_click->BrowserVersion,
                    "Platform"          =>  $paidtraffic_click->Platform,
                    "PaidTrafficConversionPixelID"     =>  0,
                );
            DB()->insert(DB()->paidtraffic_conversions, $data);

            $jsonData = array(
                "PaidTrafficID" => $paidtraffic_click->PaidTrafficID, 
                "PaidTrafficClickID" => $paidtraffic_click->id, 
                "PaidTrafficConversionID" => DB()->insert_id,
                "PaidTrafficURL" => get_paidtrafficurl($paidtraffic_click->PaidTrafficID, "", $apiUser),
            );
        }
        echo json_encode($jsonData);
        die;
    }
}