<?php
function get_link_sequenceurl($link_sequence, $sublink = ""){
    $LoggedInUser = LoggedInUser();
    $username = $LoggedInUser->user_login;
    if(is_numeric($link_sequence)){
        $sql = "select * from ".DB()->link_sequences." where userid=".$LoggedInUser->id." and id=".$link_sequence;
        $link_sequence = DB()->get_row($sql);
    }
    $link_sequenceurl = "#";
    if($link_sequence){
        $domain = DB()->get_row("select * from ".DB()->domains." where id=".$link_sequence->DomainID);
        if($domain){
            if ($domain->DomainType == "admindomain")
                $link_sequenceurl = str_replace("://", "://".$username.".", rtrim($domain->DomainUrl, "/")) . "/";
            else
                $link_sequenceurl = rtrim($domain->DomainUrl, "/") . "/";

            if(!empty($sublink))
                $link_sequenceurl .= rtrim($sublink, "/")."/";

            $link_sequenceurl .= rtrim($link_sequence->VisibleLink, "/");
        }
    }
    return $link_sequenceurl;
}

function get_link_sequence_link_box($LinkSequenceLink, $index, $LinkSequenceLinkType = "all"){
    $LoggedInUser = LoggedInUser();
    $contents = "";
    if(is_numeric($LinkSequenceLink)){
        $sql = "select * from ".DB()->link_sequence_links." where userid=".$LoggedInUser->id." and id=".$LinkSequenceLink;
        $LinkSequenceLink = DB()->get_row($sql);
    }
    if($LinkSequenceLink){
        $LinkSequenceLinkID = $LinkSequenceLink->id;
        //$TotalVisits = DB()->get_var("select count(*) from ".DB()->link_sequence_clicks." where LinkSequenceLinkID = ".$LinkSequenceLinkID) * 1;
        $TotalLinkSequenceVisits = DB()->get_var("select count(*) from ".DB()->link_sequence_clicks." where LinkSequenceLinkID = ".$LinkSequenceLink->id) * 1;
        $Percentage = 0;

        $sql = "select * from ".DB()->link_sequences." where id = ".$LinkSequenceLink->LinkSequenceID." and userid = ".$LoggedInUser->id;
        $LinkSequence = DB()->get_row($sql);
        $LinkActive = false;
        $LinkComplete = false;

        if($LinkSequence->LinkSequenceStatus == "pending"){
            $TimeLeft = "Pending";
        } else{
            if($LinkSequence->LinkSequenceStatus == "complete")
                $TimeNow = $LinkSequence->EndDate;
            else
                $TimeNow = strtotime("now");

            $dtStartDate = get_link_sequence_link_start_date($LinkSequenceLink);
            $dtEndDate = get_link_sequence_link_end_date($LinkSequenceLink, $dtStartDate);
        
            $StartDate = strtotime($dtStartDate->format("r"));
            $EndDate = strtotime($dtEndDate->format("r"));

            $dtNowDate = new DateTime(date("r", $TimeNow));
            $days = $dtNowDate->diff($dtEndDate)->format("%a");
            $hours = $dtNowDate->diff($dtEndDate)->format("%h");
            $minutes = $dtNowDate->diff($dtEndDate)->format("%i");

            if($StartDate <= $TimeNow && $EndDate >= $TimeNow){
                $TimeLeft = $dtNowDate->diff($dtEndDate)->format("Time Left: %a days, %h hours, %i min");//, %s seconds
                $LinkActive = true;
            } else if($StartDate > $TimeNow){
                $TimeLeft = "Queued";
            }else {// if($TimeNow > $EndDate){// || ($days == 0 && $hours == 0 && $minutes == 0)
                $TimeLeft = "Completed - 100%";
                $LinkComplete = true;
            }

            $TotalTime = $EndDate - $StartDate;
            $TotalSpentTime = $TimeNow < $StartDate?0:$TimeNow - $StartDate;
            $Percentage = $TimeLeft == "Completed - 100%"? 100:round($TotalSpentTime * 100 / $TotalTime, 1);
        }
        ob_start();
?>
<div class="col-sm-12 link_sequence_linkbox" id="LinkSequenceLinkIDs-<?php echo $LinkSequenceLinkID;?>" data-link_sequence_link-id="<?php echo $LinkSequenceLinkID;?>">
    <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form">
        <div class="link_sequence_linkbox-contents clearfix">
            <div class="col-md-7 no-gutter">
                <div class="col-md-2">
                    <div class="link_sequence_linkposition"><?php echo $index;?></div>
                </div>

                <div class="col-md-10">
                    <div class="link_sequence_linkurl">
                        <ul class="list-inline link_sequence_linkurloptions">
                            <li><a href="#" class="btn-easylinks">Choose EasyLink</a></li>
                            <li><span class=""><span>|</span></span></li>
                            <li><a href="#" class="btn-customurl">Custom URL</a></li>
                        </ul>

                        <div class="row easylinkurl form-group has-feedback" style="<?php if($LinkSequenceLink->LinkSequenceLinkType != "linkbank") echo " display: none; "?>">
                            <div class="col-md-8">
                                <select name="LinkBankID" id="LinkBankID-<?php echo $LinkSequenceLinkID;?>" class="form-control select2" style="width: 100%">
                                    <?php
                                    $userid = $LoggedInUser->id;
                                    $sql = "select * from ".DB()->linkbanks." where (LinkStatus = 'active' or LinkStatus = 'evergreen' or LinkStatus = 'mylink') and userid=".$userid;
                                    $linkbanks = DB()->get_results($sql);
                                    foreach($linkbanks as $linkbank){
                                    ?>
                                    <option value="<?php echo $linkbank->id?>" <?php if($LinkSequenceLink->LinkSequenceLinkType == "linkbank" && $LinkSequenceLink->LinkBankID == $linkbank->id) echo ' selected="selected" ';?>><?php echo $linkbank->LinkName?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>

                        <div class="row customurl form-group has-feedback" style="<?php if($LinkSequenceLink->LinkSequenceLinkType != "customurl") echo " display: none; "?>">
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $LinkSequenceLink->LinkSequenceLinkName; ?>" class="form-control " id="LinkSequenceLinkName-<?php echo $LinkSequenceLinkID;?>" name="LinkSequenceLinkName" placeholder="Link Name" <?php if($LinkSequenceLink->LinkSequenceLinkType == "customurl") echo ' required="required" '?> <?php echo NAME_PATTERN;?> />
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>

                        <div class="row customurl form-group has-feedback" style="<?php if($LinkSequenceLink->LinkSequenceLinkType != "customurl") echo " display: none; "?>">
                            <div class="col-md-8">
                                <input type="text" value="<?php if($LinkSequenceLink->LinkSequenceLinkType == "customurl") echo $LinkSequenceLink->LinkSequenceLinkURL; ?>" class="form-control " id="LinkSequenceLinkURL-<?php echo $LinkSequenceLinkID;?>" name="LinkSequenceLinkURL" placeholder="Custom URL" required="required" <?php echo URL_PATTERN;?> />
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>

                        <div class="form-group select-form-group">
                            <div class="col-sm-8">
                                <input type="radio" name="EndDateType" class="blue" value="EndDate" <?php if($LinkSequenceLink->EndDateType == "EndDate") echo ' checked = "checked" ';?> data-toggle="ToggleTarget" data-target="#EndDateGroup-<?php echo $LinkSequenceLinkID;?>" />&nbsp;<span class="small">Specific Date</span>&nbsp;
                                <input type="radio" name="EndDateType" class="blue" value="EndTime" <?php if($LinkSequenceLink->EndDateType == "EndTime") echo ' checked = "checked" ';?> data-toggle="ToggleTarget" data-target="#EndTimeGroup-<?php echo $LinkSequenceLinkID;?>" />&nbsp;<span class="small">Time Interval</span>&nbsp;
                            </div>
                        </div>

                        <div class="row enddate form-group has-feedback">
                            <div id="EndDateGroup-<?php echo $LinkSequenceLinkID;?>" class="collapse <?php if($LinkSequenceLink->EndDateType == "EndDate") echo 'in';?>">
                                <div class="col-md-8">
                                    <div class="input-group left-addon">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" data-linked="#EndDate-<?php echo $LinkSequenceLinkID;?>" value="<?php if(!empty($LinkSequenceLink->EndDate)) echo date("m/d/Y h:i A", $LinkSequenceLink->EndDate); ?>" class="form-control  input-singledate-time with-left-addon" id="EndDateInput-<?php echo $LinkSequenceLinkID;?>" name="EndDateInput" required="required" />
                                        <input type="hidden" value="<?php if(!empty($LinkSequenceLink->EndDate)) echo date("m/d/Y h:i A", $LinkSequenceLink->EndDate); ?>" class="" id="EndDate-<?php echo $LinkSequenceLinkID;?>" name="EndDate" />
                                        <small class="help-block with-errors"></small>
                                    </div>
                                </div>
                            </div>

                            <div id="EndTimeGroup-<?php echo $LinkSequenceLinkID;?>" class="collapse <?php if($LinkSequenceLink->EndDateType == "EndTime") echo 'in';?>">
                                <div class="col-md-8">
                                <?php
                                $EndTime  = !empty($LinkSequenceLink->EndTime)?explode("/", $LinkSequenceLink->EndTime):array(0,0,0);
                                ?>
                                    <div class="form-group select-form-group">
                                        <div class="col-md-4">
                                            <label for="Days-<?php echo $LinkSequenceLinkID;?>" class="col-xs-12 control-label" style="text-align: left;">Days</label>
                                            <select name="Days" id="Days-<?php echo $LinkSequenceLinkID;?>" class="form-control select2" style="width: 100%" data-custom_endtime='{"func": "CheckEndTime", "error":"No Time Interval Selected"}'>
                                            <?php for($EndTimeIndex = 0; $EndTimeIndex <= 31; $EndTimeIndex++) { ?>
                                                <option value="<?php echo $EndTimeIndex;?>" <?php if(isset($EndTime[0]) && intval($EndTime[0]) == $EndTimeIndex) echo " selected='selected' ";?>><?php echo str_pad($EndTimeIndex, 2, "0", STR_PAD_LEFT);?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="EndDateInput-<?php echo $LinkSequenceLinkID;?>" class="col-xs-12 control-label" style="text-align: left;">Hours</label>
                                            <select name="Hours" id="Hours-<?php echo $LinkSequenceLinkID;?>" class="form-control select2" style="width: 100%" data-custom_endtime='{"func": "CheckEndTime", "error":"No Time Interval Selected"}'>
                                            <?php for($EndTimeIndex = 0; $EndTimeIndex <= 24; $EndTimeIndex++) { ?>
                                                <option value="<?php echo $EndTimeIndex;?>" <?php if(isset($EndTime[1]) && intval($EndTime[1]) == $EndTimeIndex) echo " selected='selected' ";?>><?php echo str_pad($EndTimeIndex, 2, "0", STR_PAD_LEFT);?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="EndDateInput-<?php echo $LinkSequenceLinkID;?>" class="col-xs-12 control-label" style="text-align: left;">Minutes</label>
                                            <select name="Minutes" id="Minutes-<?php echo $LinkSequenceLinkID;?>" class="form-control select2" style="width: 100%" data-custom_endtime='{"func": "CheckEndTime", "error":"No Time Interval Selected"}'>
                                            <?php for($EndTimeIndex = 0; $EndTimeIndex <= 60; $EndTimeIndex++) { ?>
                                                <option value="<?php echo $EndTimeIndex;?>" <?php if(isset($EndTime[2]) && intval($EndTime[2]) == $EndTimeIndex) echo " selected='selected' ";?>><?php echo str_pad($EndTimeIndex, 2, "0", STR_PAD_LEFT);?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12"><small class="help-block with-errors"></small></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-md-offset-1 clearfix">
                <div class="link_sequence_linkinfo">
                    <div class="clearfix">
                        <ul class="list-inline link_sequence_linkdetails">
                            <?php if($LinkActive || $LinkComplete) { ?>
                            <li>
                                <span><?php echo $TotalLinkSequenceVisits.($TotalLinkSequenceVisits <= 1?" Click":" Clicks");?></span>
                            </li>
                            <?php } ?>
                            <?php if($LinkActive) { ?>
                            <li>
                                <a class="btn btn-<?php echo $LinkSequenceLink->LinkSequenceLinkLive == 1?"success":"danger"; ?> btn-sm btn-link_sequence_link-status-"><?php echo $LinkSequenceLink->LinkSequenceLinkLive == 1?"Active":"Paused"; ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="link_sequence_linkprogress clearfix">
                        <div class="link_sequence_linkprogressdetails"><?php echo $TimeLeft;?></div>
                        <?php if($LinkActive) { ?>
                        <div class="progress progress-sm- active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $Percentage;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $Percentage;?>%">
                                <span class="sr-only"><?php echo $TimeLeft;?></span>
                            </div>
                        </div>
                        <div class="link_sequence_linkprogresspercentage"><?php echo $Percentage;?>&nbsp;%</div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix">
                <div class="link_sequence_link-actions ">
                    <a href="#" class="btn btn-success btn-xs- btn-save-link_sequence_link">Save Link</a>
                    <a href="#" class="btn btn-danger btn-xs- btn-remove-link_sequence_link">Remove Link</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="link_sequence_linkposition" value="<?php echo $index;?>" />
        <input type="hidden" name="LinkSequenceLinkType" class="LinkSequenceLinkType" id="LinkSequenceLinkType-<?php echo $LinkSequenceLinkID;?>" value="<?php echo $LinkSequenceLink->LinkSequenceLinkType;?>" />
        <input type="hidden" name="LinkSequenceLinkID" class="LinkSequenceLinkID" id="LinkSequenceLinkID-<?php echo $LinkSequenceLinkID;?>" value="<?php echo $LinkSequenceLinkID;?>" />
    </form>
</div>

<?php
        $contents = ob_get_contents();
        ob_end_clean();
    }
    return $contents;
}

function get_link_sequence_link_start_date($LinkSequenceLink){
    $LoggedInUser = LoggedInUser();

    if($LinkSequenceLink->LinkSequenceLinkPosition == 1){
        $sql = "select * from ".DB()->link_sequences." where id = ".$LinkSequenceLink->LinkSequenceID." and userid = ".$LinkSequenceLink->userid;
        $LinkSequence = DB()->get_row($sql);
        $dtStartDate = new DateTime(date("r", $LinkSequence->StartDate));
        //$dtStartDate = GetLinkSequenceLinkStartDate($LinkSequenceLink, $dtStartDate);

    }else{
        $sql = "select * from ".DB()->link_sequence_links." where LinkSequenceID = ".$LinkSequenceLink->LinkSequenceID." and LinkSequenceLinkPosition < ".$LinkSequenceLink->LinkSequenceLinkPosition." ORDER BY LinkSequenceLinkPosition DESC LIMIT 0,1";
        $PreviousLinkSequenceLink = DB()->get_row($sql);
        $dtStartDate = get_link_sequence_link_start_date($PreviousLinkSequenceLink);
        $dtStartDate = get_link_sequence_link_end_date($PreviousLinkSequenceLink, $dtStartDate);
    }

    return $dtStartDate;
}

function get_link_sequence_link_end_date($LinkSequenceLink, $dtStartDate){
    $LoggedInUser = LoggedInUser();

    if($LinkSequenceLink->EndDateType == "EndDate"){
        $dtEndDate = new DateTime(date("r", $LinkSequenceLink->EndDate));
    } else {
        $EndTime  = !empty($LinkSequenceLink->EndTime)?explode("/", $LinkSequenceLink->EndTime):array(0,0,0);
        $dtEndDate = new DateTime($dtStartDate->format("r"));
        
        if(isset($EndTime[0]) && is_numeric($EndTime[0]) && $EndTime[0] > 0)
            $dtEndDate->modify("+".$EndTime[0]." days");

        if(isset($EndTime[1]) && is_numeric($EndTime[1]) && $EndTime[1] > 0)
            $dtEndDate->modify("+".$EndTime[1]." hours");

        if(isset($EndTime[2]) && is_numeric($EndTime[2]) && $EndTime[2] > 0)
            $dtEndDate->modify("+".$EndTime[2]." minutes");
    }

    return $dtEndDate;
}

function get_link_sequences_stats($link_sequenceid, $startdate = "", $enddate = ""){
    $LoggedInUser = LoggedInUser();

    $stats = array(
        "total" => 0,
        "unique" => 0,
        "nonunique" => 0,
        "bots" => 0,
        "dailyaverage" => 0,
    );

    $sql = "select * from ".DB()->link_sequences." where id=".$link_sequenceid." and userid=".$LoggedInUser->id;
    $link_sequence = DB()->get_row($sql);
    if($link_sequence){

        $datesql = !empty($startdate) && !empty($enddate)?" and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($startdate)."' and ".DB()->link_sequence_clicks.".DateAdded <= '".strtotime($enddate)."') ":"";
        $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
        $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

        $sql = "select count(*) from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql;
        $TotalLinkSequenceVisits = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select 1 AS count from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ClickIp) as LinkSequenceVisitsCount";
        $UniqueLinkSequenceVisits = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select count(ClickIp) AS count from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ClickIp) as LinkSequenceVisitsCount";
        $NonUniqueLinkSequenceVisits = DB()->get_var($sql) * 1;
        //$NonUniqueLinkSequenceVisits -= $UniqueLinkSequenceVisits;

        $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
        $BotsLinkSequenceVisits = DB()->get_var($sql) * 1;
        
        //$sql = "select count(*) from ".$table." inner join ".DB()->countries." on ".DB()->link_sequence_clicks.".CountryCode = ".DB()->countries.".countrycode where countrytier = 1 and (BotName = '' or BotName is NULL) and LinkSequenceID=".$link_sequence->id.$datesql;
        //$TotalTopLinkSequenceVisits = DB()->get_var($sql) * 1;

        //$TopLinkSequenceVisitsPercentage = $TotalLinkSequenceVisits > 0?round(($TotalTopLinkSequenceVisits * 100) / $TotalLinkSequenceVisits):0;

        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".DB()->link_sequence_clicks.".DateAdded from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql." ORDER BY ".DB()->link_sequence_clicks.".DateAdded ASC limit 0,1";
            $date1 = DB()->get_var($sql);

            $sql = "select ".DB()->link_sequence_clicks.".DateAdded from ".$table." where LinkSequenceID=".$link_sequenceid.$LinkQuery.$datesql." ORDER BY ".DB()->link_sequence_clicks.".DateAdded DESC limit 0,1";
            $date2 = DB()->get_var($sql);
            
            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $DailyAverage = $TotalDays > 0?round($TotalLinkSequenceVisits / $TotalDays, 2):0;

        $stats = array(
            "total"         => $TotalLinkSequenceVisits,
            "unique"        => $UniqueLinkSequenceVisits,
            "non-unique"     => $NonUniqueLinkSequenceVisits,
            "bots"          => $BotsLinkSequenceVisits,
            "dailyaverage"  => $DailyAverage,
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $stats;
}

function delete_link_sequence_status($LinkSequenceID){
    $LoggedInUser = LoggedInUser();
    if(DB()->get_row("select * from ".DB()->link_sequences." where id=".$LinkSequenceID." and userid=".$LoggedInUser->id)){
        $LinkSequenceLinks = DB()->get_results("SELECT * from ".DB()->link_sequence_links." where LinkSequenceID=".$LinkSequenceID);
        foreach ($LinkSequenceLinks as $LinkSequenceLink){
        	DB()->delete(DB()->link_sequence_clicks, array("LinkSequenceLinkID" => $LinkSequenceLink->id));
        }
        //DB()->delete(DB()->link_sequence_links, array("LinkSequenceID" => $LinkSequenceID));
    }
}

function update_link_sequence_status($link_sequence){
    
    
    $LinkSequenceStatus = "";

    if(is_numeric($link_sequence))
        $link_sequence = DB()->get_row("select * from ".DB()->link_sequences." where id=".$link_sequence);
    
    if($link_sequence)
        $LinkSequenceStatus = $link_sequence->LinkSequenceStatus;

    if($link_sequence && $link_sequence->LinkSequenceStatus == "active"){
        $dtStartDate = null;
        $dtEndDate = null;
        $updateLinkSequence = true;

        $sql = "select * from ".DB()->link_sequence_links." where LinkSequenceID = ".$link_sequence->id." order by LinkSequenceLinkPosition";
        $LinkSequenceLinks = DB()->get_results($sql);
        foreach ($LinkSequenceLinks as $LinkSequenceLink){
            $dtStartDate = $dtStartDate == null?get_link_sequence_link_start_date($LinkSequenceLink):$dtEndDate;
            $dtEndDate = get_link_sequence_link_end_date($LinkSequenceLink, $dtStartDate);

            $EndDate = strtotime($dtEndDate->format("r"));
            $TimeNow = strtotime("now");

            if($EndDate >= $TimeNow){
                $updateLinkSequence = false;
                break;
            }
        }

        if ($updateLinkSequence){
        	DB()->update(
                DB()->link_sequences, 
                array(
                    "LinkSequenceStatus"    => "complete",
                    "EndDate"               => strtotime("now")
                ), 
                array("id" => $link_sequence->id));
            $LinkSequenceStatus = "complete";
        }
    }

    return $LinkSequenceStatus;
}