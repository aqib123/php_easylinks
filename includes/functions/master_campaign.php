<?php
function get_master_campaign_details($MasterCampaignID){
    $LoggedInUser = LoggedInUser();
	$MasterCampaignDetails = array();

    $LinkBankClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." inner join ".DB()->linkbanks." on ".DB()->linkbank_clicks.".LinkBankID = ".DB()->linkbanks.".id where MasterCampaignID=".$MasterCampaignID) * 1;
    $EPCLinkBankClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." inner join ".DB()->linkbanks." on ".DB()->linkbank_clicks.".LinkBankID = ".DB()->linkbanks.".id where TrackEPC = 1 and MasterCampaignID=".$MasterCampaignID) * 1;
    $RotatorClicks = DB()->get_var("select SUM(RotatorClickCount) as TotalRotatorClicks from (select count(*) as RotatorClickCount from ".DB()->rotator_clicks." where RotatorLinkID in (SELECT id from ".DB()->rotator_links." where MasterCampaignID=".$MasterCampaignID." and userid=".$LoggedInUser->id.")) as RotatorClicks") * 1;
    $LinkSequenceClicks = DB()->get_var("select SUM(LinkSequenceClickCount) as TotalLinkSequenceClicks from (select count(*) as LinkSequenceClickCount from ".DB()->link_sequence_clicks." where LinkSequenceLinkID in (SELECT id from ".DB()->link_sequence_links." where MasterCampaignID=".$MasterCampaignID." and userid=".$LoggedInUser->id.")) as LinkSequenceClicks") * 1;
    $PaidTrafficClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->paidtraffics." on ".DB()->paidtraffic_clicks.".PaidTrafficID = ".DB()->paidtraffics.".id where MasterCampaignID=".$MasterCampaignID) * 1;
    $TotalClicks = $LinkBankClicks + $RotatorClicks + $PaidTrafficClicks;

    $MasterCampaignDetails["LinkBankClicks"] = $LinkBankClicks;
    $MasterCampaignDetails["EPCLinkBankClicks"] = $EPCLinkBankClicks;
    $MasterCampaignDetails["RotatorClicks"] = $RotatorClicks;
    $MasterCampaignDetails["LinkSequenceClicks"] = $LinkSequenceClicks;
    $MasterCampaignDetails["PaidTrafficClicks"] = $PaidTrafficClicks;
    $MasterCampaignDetails["TotalClicks"] = $TotalClicks;

    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "pending"));
    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "active"));
    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "complete"));

    return $MasterCampaignDetails;
}

function get_master_campaign_sub_details($MasterCampaignID, $status){
	$LoggedInUser = LoggedInUser();
	$MasterCampaignSubDetails = array();

    $LinkBanks = DB()->get_var("select count(*) from ".DB()->linkbanks." where MasterCampaignID=".$MasterCampaignID." and LinkStatus='".$status."'") * 1;
    $Rotators = DB()->get_var("SELECT count(*) from ".DB()->rotators." where MasterCampaignID=".$MasterCampaignID." and RotatorStatus='".$status."'") * 1;
    $LinkSequences = DB()->get_var("SELECT count(*) from ".DB()->link_sequences." where MasterCampaignID=".$MasterCampaignID." and LinkSequenceStatus='".$status."'") * 1;
    $PaidTraffics = DB()->get_var("select count(*) from ".DB()->paidtraffics." where MasterCampaignID=".$MasterCampaignID." and PaidTrafficStatus='".$status."'") * 1;
    $Campaigns = DB()->get_var("select count(*) from ".DB()->campaigns." where MasterCampaignID=".$MasterCampaignID." and CampaignStatus='".$status."'") * 1;
    $Total = $LinkBanks + $Rotators + $PaidTraffics + $Campaigns;

    $MasterCampaignSubDetails["LinkBanks".ucwords($status)] = $LinkBanks;
    $MasterCampaignSubDetails["Rotators".ucwords($status)] = $Rotators;
    $MasterCampaignSubDetails["LinkSequences".ucwords($status)] = $LinkSequences;
    $MasterCampaignSubDetails["PaidTraffics".ucwords($status)] = $PaidTraffics;
    $MasterCampaignSubDetails["Campaigns".ucwords($status)] = $Campaigns;
    $MasterCampaignSubDetails["Total".ucwords($status)] = $Total;

    return $MasterCampaignSubDetails;
}

function delete_campaign_stats($CampaignID){
    $LoggedInUser = LoggedInUser();
    if(DB()->get_row("select * from ".DB()->campaigns." where id=".$CampaignID." and userid=".$LoggedInUser->id)){
        DB()->query("delete from ".DB()->linkbank_clicks." where EmailID in (select id from ".DB()->campaign_emails." where userid = ".$LoggedInUser->id." and CampaignID = ".$CampaignID.")");
    }
}