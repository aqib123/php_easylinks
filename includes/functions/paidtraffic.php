<?php
function get_paidtrafficurl($paidtraffic, $sublink = "", $user = null){
    $LoggedInUser = LoggedInUser();

    if($user == null)
        $user = $LoggedInUser;
    
    $username = $user->user_login;

    if(is_numeric($paidtraffic)){
        $sql = "select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic;
        $paidtraffic = DB()->get_row($sql);
    }
    $paidtrafficurl = "#";
    if($paidtraffic){
        $domain = DB()->get_row("select * from ".DB()->domains." where id=".$paidtraffic->DomainID);
        if($domain){
            if ($domain->DomainType == "admindomain")
                $paidtrafficurl = str_replace("://", "://".$username.".", rtrim($domain->DomainUrl, "/"))."/";
            else
                $paidtrafficurl = rtrim($domain->DomainUrl, "/")."/";

            if(!empty($sublink))
                $paidtrafficurl .= rtrim($sublink, "/")."/";

            $paidtrafficurl .= rtrim($paidtraffic->VisibleLink, "/");
        }
    }
    return $paidtrafficurl;
}

function get_paidtraffic_stats($paidtrafficid, $startdate = "", $enddate = ""){
    $LoggedInUser = LoggedInUser();

    $stats = array(
        "total" => 0,
        "unique" => 0,
        "nonunique" => 0,
        "bots" => 0,
        "dailyaverage" => 0,
    );

    $sql = "select * from ".DB()->paidtraffics." where id=".$paidtrafficid." and userid=".$LoggedInUser->id;
    $paidtraffic = DB()->get_row($sql);
    if($paidtraffic){

        $datesql = !empty($startdate) && !empty($enddate)?" and (".DB()->paidtraffic_clicks.".DateAdded >= '".strtotime($startdate)."' and ".DB()->paidtraffic_clicks.".DateAdded <= '".strtotime($enddate)."') ":"";

        $sql = "select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql;
        $TotalClicks = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select 1 AS count from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." and (".DB()->paidtraffic_clicks.".BotName = '' or ".DB()->paidtraffic_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount";
        $UniqueClicks = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select count(ClickIp) AS count from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." and (".DB()->paidtraffic_clicks.".BotName = '' or ".DB()->paidtraffic_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount";
        $NonUniqueClicks = DB()->get_var($sql) * 1;
        //$NonUniqueClicks -= $UniqueClicks;

        $sql = "select count(ClickIp) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." and (".DB()->paidtraffic_clicks.".BotName <> '' and ".DB()->paidtraffic_clicks.".BotName is not NULL)";
        $BotsClicks = DB()->get_var($sql) * 1;
        
        //$sql = "select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->countries." on ".DB()->paidtraffic_clicks.".CountryCode = ".DB()->countries.".countrycode where countrytier = 1 and (BotName = '' or BotName is NULL) and PaidTrafficID=".$paidtraffic->id.$datesql;
        //$TotalTopClicks = DB()->get_var($sql) * 1;

        //$TopClicksPercentage = $TotalClicks > 0?round(($TotalTopClicks * 100) / $TotalClicks):0;

        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".DB()->paidtraffic_clicks.".DateAdded from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." ORDER BY ".DB()->paidtraffic_clicks.".DateAdded ASC limit 0,1";
            $date1 = DB()->get_var($sql);

            $sql = "select ".DB()->paidtraffic_clicks.".DateAdded from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." ORDER BY ".DB()->paidtraffic_clicks.".DateAdded DESC limit 0,1";
            $date2 = DB()->get_var($sql);

            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $DailyAverage = $TotalDays > 0?round($TotalClicks / $TotalDays, 2):0;

        $stats = array(
            "total"         => $TotalClicks,
            "unique"        => $UniqueClicks,
            "non-unique"     => $NonUniqueClicks,
            "bots"          => $BotsClicks,
            "dailyaverage"  => $DailyAverage,
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $stats;
}

function get_paidtraffic_dates($paidtrafficid, $startdate = "", $enddate = ""){
    $LoggedInUser = LoggedInUser();

    $dates = array(
        "startdate"     => 0,
        "enddate"       => 0,
        "totaldays"     => 0,
    );

    $sql = "select * from ".DB()->paidtraffics." where id=".$paidtrafficid." and userid=".$LoggedInUser->id;
    $paidtraffic = DB()->get_row($sql);
    if($paidtraffic){
        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".DB()->paidtraffic_clicks.".DateAdded from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." ORDER BY ".DB()->paidtraffic_clicks.".DateAdded ASC limit 0,1";
            $date1 = DB()->get_var($sql);

            $sql = "select ".DB()->paidtraffic_clicks.".DateAdded from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtrafficid.$datesql." ORDER BY ".DB()->paidtraffic_clicks.".DateAdded DESC limit 0,1";
            $date2 = DB()->get_var($sql);

            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $dates = array(
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $dates;
}

function delete_paid_traffic_stats($PaidTrafficID, $user = null){
    $LoggedInUser = LoggedInUser();
    if($user == null)
        $user = $LoggedInUser;
    if(DB()->get_row("select * from ".DB()->paidtraffics." where id=".$PaidTrafficID." and userid=".$user->id)){
        DB()->delete(DB()->paidtraffic_clicks, array("PaidTrafficID" => $PaidTrafficID));
        DB()->delete(DB()->paidtraffic_conversions, array("PaidTrafficID" => $PaidTrafficID));
        DB()->delete(DB()->paidtraffic_pixel_fires, array("PaidTrafficID" => $PaidTrafficID));
    }
}

function get_traffic_quallity($paidtraffic, $user, $CalculateScore = true){
	
    $TrafficQuality = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            //$NoOfClicks = $paidtraffic->NoOfClicks;
            $UniqueClicks = DB()->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".DB()->paidtraffic_clicks." where (BotName = '' or BotName is NULL) and PaidTrafficID=".$paidtraffic->id." group by ClickIp) as UniqueClicks") * 1;
            $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
            //$RawClicks -= $UniqueClicks;

            $Tier1aEvaluation = 1.1;
            $Tier1Evaluation = 1;
            $Tier2Evaluation = 0.5;
            $Tier3Evaluation = -0.1;

            $Tier1aClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where CountryCode = 'US' and PaidTrafficID=".$paidtraffic->id) * 1;
            $Tier1Clicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->countries." on ".DB()->paidtraffic_clicks.".CountryCode = ".DB()->countries.".countrycode where ".DB()->paidtraffic_clicks.".CountryCode <> 'US' and countrytier = 1 and PaidTrafficID=".$paidtraffic->id) * 1;
            $Tier2Clicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->countries." on ".DB()->paidtraffic_clicks.".CountryCode = ".DB()->countries.".countrycode where countrytier = 2 and PaidTrafficID=".$paidtraffic->id) * 1;
            $Tier3Clicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where (CountryCode = '' or CountryCode = 'unknown') and PaidTrafficID=".$paidtraffic->id) * 1;
            $Tier3Clicks += DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->countries." on ".DB()->paidtraffic_clicks.".CountryCode = ".DB()->countries.".countrycode where ".DB()->paidtraffic_clicks.".CountryCode <> '' and ".DB()->paidtraffic_clicks.".CountryCode <> 'unknown' and countrytier > 2 and PaidTrafficID=".$paidtraffic->id) * 1;

            $ClickQuality = $UniqueClicks/$RawClicks;
            $Tier1aValue = $Tier1aClicks / $RawClicks * $Tier1aEvaluation;
            $Tier1Value = $Tier1Clicks / $RawClicks * $Tier1Evaluation;
            $Tier2Value = $Tier2Clicks / $RawClicks * $Tier2Evaluation;
            $Tier3Value = $Tier3Clicks / $RawClicks * $Tier3Evaluation;
            $WeightedTierValue = $Tier1aValue + $Tier1Value + $Tier2Value + $Tier3Value;
            $TierValuePercentage = $WeightedTierValue *100;
            $TrafficQuality = $TierValuePercentage * $ClickQuality;
        } else {
            $TrafficQuality = $paidtraffic->TrafficQuality;
        }
    }

    return $TrafficQuality;
}

function get_lead_score($paidtraffic, $user, $CalculateScore = true){
	
    $LeadScore = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
            $TotalActions = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='action'") * 1;
            $OptinRatio = $RawClicks > 0?$TotalActions / $RawClicks:0;

            $StandardOptin  = 0.4;
            $ScaleFactor = 2.5;
            $LeadScore = ($OptinRatio >= $StandardOptin?1:$OptinRatio * $ScaleFactor) * 100;
        } else {
            $LeadScore = $paidtraffic->LeadScore;
        }
    }

    return $LeadScore;
}

function get_time_delivered($paidtraffic, $user, $CalculateScore = true){
	
    $TimeDelivered = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $dtStartDate = new DateTime(date("r", $paidtraffic->StartDate));
            //$dtClickDate = DB()->get_row("select * from ".DB()->paidtraffic_clicks." where PaidTrafficID = ".$paidtraffic->id." LIMIT 4, 1");
            //$dtClickDate = DB()->get_row("select * from (select * from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." group by ClickIp aving count(ClickIp) = 1) as UniqueClicks ORDER BY DateAdded LIMIT 4, 1");
            $dtClickDate = DB()->get_row("select * from (select * from (select * from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." and ClickIp = ClickIp ORDER BY id asc) as CountTable group by ClickIp) as UniqueClicks ORDER BY DateAdded LIMIT 4, 1");

            if($dtClickDate){
                $dtClickDate = new DateTime(date("r", $dtClickDate->DateAdded));
                $days = intval($dtStartDate->diff($dtClickDate)->format("%a"));
                $TimeDelivered = 100 - ($days>4?100:$days*25);
            }
        } else {
            $TimeDelivered = $paidtraffic->TimeDelivered;
        }
    }

    return $TimeDelivered;
}

function get_sale_quality($paidtraffic, $user, $CalculateScore = true){
	
    $SaleQuality = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $EasyLinkClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$paidtraffic->EasyLinkID." and ClickType='sa'") * 1;
            $TotalSales = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
            $LeadScore = get_lead_score($paidtraffic, $user);

            $SaleQuality = $EasyLinkClicks == 0?0:$TotalSales / $EasyLinkClicks;
            $SaleQuality = $SaleQuality * $LeadScore;
            $SaleQuality = $SaleQuality * 20;
        } else {
            $SaleQuality = $paidtraffic->SaleQuality;
        }
    }

    return $SaleQuality;
}

function get_overall_profitability($paidtraffic, $user, $CalculateScore = true){
	
    $OverallProfitability = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $TrafficCost = $paidtraffic->TrafficCost;
            $TotalSales = DB()->get_var("select sum(UnitPrice) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;

            $OverallProfitability = ($TrafficCost >= $TotalSales || $TrafficCost == 0)?0:(($TotalSales/$TrafficCost)/2)*100;
        } else {
            $OverallProfitability = $paidtraffic->OverallProfitability;
        }
    }

    return $OverallProfitability;
}

function get_overall_deliverability($paidtraffic, $user, $CalculateScore = true){
	
    $OverallDeliverability = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $EndDate = $paidtraffic->EndDate;
            $Now = strtotime("now");
            $dtEndDate = new DateTime(date("r", $EndDate));
            $NoOfClicks = $paidtraffic->NoOfClicks;
            $LastClickDate = DB()->get_row("select * from ".DB()->paidtraffic_clicks." where PaidTrafficID = ".$paidtraffic->id." LIMIT 0, 1 order by DateAdded desc");
            //$LastValidClickDate = DB()->get_row("select * from ".DB()->paidtraffic_clicks." where PaidTrafficID = ".$paidtraffic->id." and DateAdded <= ".$EndDate." LIMIT 0, 1 order by DateAdded desc");
            $TotalClicks = DB()->get_row("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID = ".$paidtraffic->id);
            $TotalValidClicks = DB()->get_row("select count(*) from ".DB()->paidtraffic_clicks." where DateAdded <= ".$EndDate." and PaidTrafficID = ".$paidtraffic->id);

            if($Now < $EndDate || $TotalValidClicks >= $NoOfClicks){
                $OverallDeliverability = 100;
            } else{
                if($TotalClicks >= $NoOfClicks){
                    $LastClickDate = DB()->get_row("select * from ".DB()->paidtraffic_clicks." where PaidTrafficID = ".$paidtraffic->id." LIMIT ".($NoOfClicks - 1).", 1 order by DateAdded asc");
                }else{
                    $LastClickDate = strtotime("now");
                }

                $dtLastClickDate = new DateTime(date("r", $LastClickDate));
                $DateDifference = $dtEndDate->diff($dtLastClickDate);
                $days = $DateDifference->format("%a");
                $hours = $DateDifference->format("%h");
                $months = $DateDifference->format("%m");

                $days += ($months * 30);
                $hours += ($days * 24);

                $OverallDeliverability = (100 - ($hours>40?100:$hours*2.5));
            }
        } else {
            $OverallDeliverability = $paidtraffic->OverallDeliverability;
        }
    }

    return $OverallDeliverability;
}

function get_vendor_overall_score($paidtraffic, $user, $CalculateScore = true, $TrafficQuallity = null, $TimeDelivered = null, $LeadScore = null, $OverallDeliverability = null){
	
    $VendorOverallScore = 0;

    if(is_numeric($paidtraffic))
        $paidtraffic = DB()->get_row("select * from ".DB()->paidtraffics." where userid=".$user->id." and id=".$paidtraffic);

    if($paidtraffic){
        if($CalculateScore){
            $TrafficQuallity = get_traffic_quallity($paidtraffic, $user, $CalculateScore);
            $TimeDelivered = get_time_delivered($paidtraffic, $user, $CalculateScore);
            $LeadScore = get_lead_score($paidtraffic, $user, $CalculateScore);
            $OverallDeliverability = get_overall_deliverability($paidtraffic, $user, $CalculateScore);


            $TrafficQuallityInfluence = 0.5;
            $TimeDeliveredInfluence = 0.05;
            $LeadScoreInfluence = 0.4;
            $OverallDeliverabilityInfluence = 0.05;

            $VendorOverallScore += ($TrafficQuallity * $TrafficQuallityInfluence);
            $VendorOverallScore += ($TimeDelivered * $TimeDeliveredInfluence);
            $VendorOverallScore += ($LeadScore * $LeadScoreInfluence);
            $VendorOverallScore += ($OverallDeliverability * $OverallDeliverabilityInfluence);
        } else {
            $VendorOverallScore = $paidtraffic->VendorOverallScore;
        }
    }

    return $VendorOverallScore;
}

function get_paidtraffic_conversion_pixel($conversion_pixel, $pixel_key = ""){
	$LoggedInUser = LoggedInUser();
    if($conversion_pixel != null && is_numeric($conversion_pixel))
        $conversion_pixel = DB()->get_row("select * from ".DB()->paidtraffic_conversion_pixels." where userid=".$LoggedInUser->id." and id=".$conversion_pixel);

    $conversion_pixel_id = $conversion_pixel == null?$pixel_key:$conversion_pixel->id;
	ob_start();
?>
<div class="paidtraffic_conversion_pixel_container">
    <a href="javascript:" class="btn btn-sm btn-remove-pixel" title="Delete Pixel"><i class="fa fa-trash"></i></a>
    <input class="paidtraffic_conversion_pixel_id" type="hidden" name="paidtraffic_conversion_pixels[]" value="<?php echo $conversion_pixel_id;?>" />
    <div class="form-group select-form-group">
        <label for="ConversionPixelType-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion Type</label>
        <div class="col-sm-8 ">
            <select name="ConversionPixelType-<?php echo $conversion_pixel_id;?>" id="ConversionPixelType-<?php echo $conversion_pixel_id;?>" class="form-control select2" style="width: 100%">
                <option value="NewFunnel">New Funnel</option>
                <option value="Frontend">Frontend</option>
                <option value="Upsell">Upsell</option>
                <option value="Downsell">Downsell</option>
            </select>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelName-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion Name</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelName?>" class="form-control " id="ConversionPixelName-<?php echo $conversion_pixel_id;?>" name="ConversionPixelName-<?php echo $conversion_pixel_id;?>" placeholder="Conversion Name" <?php echo NAME_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion URL</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelURL?>" class="form-control " id="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" name="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" placeholder="Conversion URL" <?php echo URL_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Unit Price</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelUnitPrice?>" class="form-control conversion_pixel_unit_price" id="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" name="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" placeholder="Unit Price For Sales & Conversions" <?php echo FLOAT_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group">
        <label for="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Tracking Code</label>
        <div class="col-sm-8">
            <textarea readonly="readonly" class="form-control ConversionPixelTrackingCode" id="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" name="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" placeholder="Tracking Code For Sales & Conversions" cols="3"><?php echo $conversion_pixel?$conversion_pixel->ConversionPixelTrackingCode:"Save PaidTraffic to get pixel code";?></textarea>
            <small class="help-block text-green text-xs">Place this tracking code just below the opening &lt;body&gt; on the page that follows your Conv Url</small>
            <small class="help-block with-errors"></small>
        </div>
    </div>
    <hr />
</div>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function get_paidtraffic_goal($goal, $goal_key = ""){
	$LoggedInUser = LoggedInUser();
    if($goal != null && is_numeric($goal))
        $goal = DB()->get_row("select * from ".DB()->paidtraffic_goals." where userid=".$LoggedInUser->id." and id=".$goal);

    $goal_id = $goal == null?$goal_key:$goal->id;
	ob_start();
?>
<div class="paidtraffic_goal_container">
    <a href="javascript:" class="btn btn-sm btn-remove-goal" title="Delete Goal"><i class="fa fa-trash"></i></a>
    <input class="paidtraffic_goal_id" type="hidden" name="paidtraffic_goals[]" value="<?php echo $goal_id;?>" />

    <div class="form-group has-feedback">
        <label for="GoalName-<?php echo $goal_id;?>" class="col-sm-4 control-label">Goal Name</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($goal) echo $goal->GoalName?>" class="form-control " id="GoalName-<?php echo $goal_id;?>" name="GoalName-<?php echo $goal_id;?>" placeholder="Goal Name" <?php echo NAME_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="GoalURL-<?php echo $goal_id;?>" class="col-sm-4 control-label"></label>
        <div class="col-sm-8">
            <input type="checkbox" name="GoalEnabled" id="GoalEnabled" class="blue " value="1" <?php if(($goal->GoalEnabled == 1) || (isset($_POST['GoalEnabled']))) echo "checked='checked'";?> />&nbsp;
            <span>Goal Enabled</span>&nbsp;&nbsp;
        </div>
    </div>
    <hr />
</div>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function update_paidtraffic_conversion_pixels($PaidTrafficID){
	$LoggedInUser = LoggedInUser();
    if(isset($_POST["paidtraffic_conversion_pixels"]) && is_array($_POST["paidtraffic_conversion_pixels"])){
        $paidtraffic_conversion_pixels = $_POST["paidtraffic_conversion_pixels"];
        foreach ($paidtraffic_conversion_pixels as $paidtraffic_conversion_pixel_id){
            $data = array(
                "PaidTrafficID"                    => $PaidTrafficID,
                "ConversionPixelType"           => $_POST["ConversionPixelType-".$paidtraffic_conversion_pixel_id],
                "ConversionPixelName"           => $_POST["ConversionPixelName-".$paidtraffic_conversion_pixel_id],
                "ConversionPixelURL"            => $_POST["ConversionPixelURL-".$paidtraffic_conversion_pixel_id],
                "ConversionPixelUnitPrice"      => $_POST["ConversionPixelUnitPrice-".$paidtraffic_conversion_pixel_id],
                "ConversionPixelTrackingCode"   => $_POST["ConversionPixelTrackingCode-".$paidtraffic_conversion_pixel_id],
            );
        	if(strpos($paidtraffic_conversion_pixel_id, "newpixel-") > -1){
                $data["DateAdded"] = strtotime("now");
                unset($data["ConversionPixelTrackingCode"]);
                $data["userid"] = $LoggedInUser->id;
                if(DB()->insert(DB()->paidtraffic_conversion_pixels, $data))
                    DB()->update(DB()->paidtraffic_conversion_pixels, array("ConversionPixelTrackingCode" => get_pixel_image("sp", $data["ConversionPixelUnitPrice"]), "ptpid=".DB()->insert_id), array("id" => DB()->insert_id));
            } else {
                DB()->update(DB()->paidtraffic_conversion_pixels, $data, array("id" => $paidtraffic_conversion_pixel_id));
            }
        }
    }

    if(isset($_POST["paidtraffic_conversion_pixel_delete"]) && is_array($_POST["paidtraffic_conversion_pixel_delete"])){
        $paidtraffic_conversion_pixel_delete = $_POST["paidtraffic_conversion_pixel_delete"];
        foreach ($paidtraffic_conversion_pixel_delete as $paidtraffic_conversion_pixel_id){
        	if(strpos($paidtraffic_conversion_pixel_id, "newpixel-") <= -1){
                DB()->delete(
                    DB()->paidtraffic_conversion_pixels, 
                    array(
                        "id" => $paidtraffic_conversion_pixel_id, 
                        "PaidTrafficID" => $PaidTrafficID
                    ));
            }
        }
    }
}

function get_vendor_score($Vendor, $user, $ReturnHTML = true, $CalculateScore = true){
	

    $contents = "";
    $campaigns = "";

    $TotalPaidTraffics = 0;
    $TrafficQualityAvg = 0;
    $LeadScoreAvg = 0;
    $TimeDeliveredAvg = 0;
    $SaleQualityAvg = 0;
    $OverallProfitabilityAvg = 0;
    $OverallDeliverabilityAvg = 0;
    $VendorOverallScoreAvg = 0;
    $SaleQualityAvg = 0;
    $OverallProfitabilityAvg = 0;
    $PaidTrafficStats = array();

    if(is_numeric($Vendor))
        $Vendor = DB()->get_row("select * from ".DB()->vendors." where userid = ".$user->id." and id = ".$Vendor);

    if($Vendor){
        $paidtraffics = DB()->get_results("select * from ".DB()->paidtraffics." where userid = ".$user->id." and VendorID = ".$Vendor->id);
        foreach ($paidtraffics as $paidtraffic){
        	$TrafficQuality =  get_traffic_quallity($paidtraffic, $user, $CalculateScore);
            $LeadScore = get_lead_score($paidtraffic, $user, $CalculateScore);
            $TimeDelivered = get_time_delivered($paidtraffic, $user, $CalculateScore);
            $SaleQuality = get_sale_quality($paidtraffic, $user, $CalculateScore);
            $OverallProfitability = get_overall_profitability($paidtraffic, $user, $CalculateScore);
            $OverallDeliverability = get_overall_deliverability($paidtraffic, $user, $CalculateScore);
            $VendorOverallScore = get_vendor_overall_score($paidtraffic, $user, $CalculateScore, $TrafficQuality, $TimeDelivered, $LeadScore, $OverallDeliverability);

            $TrafficQualityAvg += $TrafficQuality;
            $LeadScoreAvg += $LeadScore;
            $TimeDeliveredAvg += $TimeDelivered;
            $SaleQualityAvg += $SaleQuality;
            $OverallProfitabilityAvg += $OverallProfitability;
            $OverallDeliverabilityAvg += $OverallDeliverability;
            $VendorOverallScoreAvg += $VendorOverallScore;

            $namelinkedurl = get_site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id);
            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
            $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
            $TotalActions = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='action'") * 1;
            //$TotalSalesCount = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
            $TotalSales = DB()->get_var("select sum(UnitPrice) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
            $EasyLinkClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$paidtraffic->EasyLinkID." and ClickType='sa'") * 1;
            $PercentSales = $EasyLinkClicks>0?(($TotalSales/$EasyLinkClicks) * 100):0;

            $SaleQuality = $SaleQuality > 100? 100:$SaleQuality;
            $OverallProfitability = $OverallProfitability > 100? 100:$OverallProfitability;

            if(!$ReturnHTML){
                $PaidTrafficStats["paidtraffic_".$paidtraffic->id] = array(
                    "PaidTrafficID" => $paidtraffic->id,
                    "TrafficQuality" => $TrafficQuality,
                    "LeadScore" => $LeadScore,
                    "TimeDelivered" => $TimeDelivered,
                    "OverallDeliverability" => $OverallDeliverability,
                    "VendorOverallScore" => $VendorOverallScore,
                    "namelinkedurl" => $namelinkedurl,
                    "GroupName" => $GroupName,
                    "RawClicks" => $RawClicks,
                    "TotalActions" => $TotalActions,
                    "TotalSales" => $TotalSales,
                    "EasyLinkClicks" => $EasyLinkClicks,
                    "PercentSales" => $PercentSales,
                    "SaleQuality" => $SaleQuality,
                    "OverallProfitability" => $OverallProfitability
                );
            }

            if($ReturnHTML){
                ob_start();
?>
<tr>
    <td><a href="<?php echo $namelinkedurl; ?>" class="pull-left"><?php echo $paidtraffic->PaidTrafficName?></a></td>
    <td><span><?php echo $GroupName;?></span></td>
    <td class="text-center stats-column"><span><?php echo get_formated_number($RawClicks);?></span></td>
    <td class="text-center action-column"><span><?php echo get_formated_number($TotalActions);?></span></td>
    <td class="text-center sale-column"><span><?php echo get_formated_number(round($PercentSales, 2));?>%</span></td>
    <td class="text-center sale-column"><span>$<?php echo get_formated_number($TotalSales);?></span></td>
    <td class="text-center non-score-column">
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($SaleQuality));?>" data-toggle="qtiptooltip" title="Sale Quality"><span><?php echo get_formated_number(round($SaleQuality));?></span></div>
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallProfitability));?>" data-toggle="qtiptooltip" title="Overall Profitability"><span><?php echo get_formated_number(round($OverallProfitability));?></span></div>
    </td>
    <td class="text-center score-column">
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TrafficQuality));?>" data-toggle="qtiptooltip" title="Traffic Quality"><span><?php echo get_formated_number(round($TrafficQuality));?></span></div>
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TimeDelivered));?>" data-toggle="qtiptooltip" title="Time Delivered"><span><?php echo get_formated_number(round($TimeDelivered));?></span></div>
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($LeadScore));?>" data-toggle="qtiptooltip" title="Lead Score"><span><?php echo get_formated_number(round($LeadScore));?></span></div>
        <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallDeliverability));?>" data-toggle="qtiptooltip" title="Overall Deliverability"><span><?php echo get_formated_number(round($OverallDeliverability));?></span></div>
    </td>
    <td class="text-center score-column-">
        <div class="vendor-stats-box-sm " data-toggle="qtiptooltip" title="Overall Score"><?php echo get_formated_number(round($VendorOverallScore));?>%</div>
    </td>
</tr>
<?php
                $campaigns .= ob_get_contents();
                ob_end_clean();
            }
        }

        if(count($paidtraffics) > 0){
            $TotalPaidTraffics = count($paidtraffics);
            $TrafficQualityAvg = $TrafficQualityAvg / $TotalPaidTraffics;
            $LeadScoreAvg = $LeadScoreAvg / $TotalPaidTraffics;
            $TimeDeliveredAvg = $TimeDeliveredAvg / $TotalPaidTraffics;
            $SaleQualityAvg = $SaleQualityAvg / $TotalPaidTraffics;
            $OverallProfitabilityAvg = $OverallProfitabilityAvg / $TotalPaidTraffics;
            $OverallDeliverabilityAvg = $OverallDeliverabilityAvg / $TotalPaidTraffics;
            $VendorOverallScoreAvg = $VendorOverallScoreAvg / $TotalPaidTraffics;
        }

        $SaleQualityAvg = $SaleQualityAvg > 100? 100:$SaleQualityAvg;
        $OverallProfitabilityAvg = $OverallProfitabilityAvg > 100? 100:$OverallProfitabilityAvg;

        if($ReturnHTML){
            ob_start();
?>
<div class="box box-gray- box-solid- box-border-doted">
    <div class="box-body">
        <div>
            <table class="table table-vendor-stats-boxes">
                <tbody>
                    <tr>
                        <td>
                            <h2 class="stats-vendor-name">Vendor Name: <?php echo $Vendor->VendorName;?></h2>
                            <table class="table table-vendor-stats-boxes">
                                <thead>
                                    <tr>
                                        <th style="text-align:left; font-weight: normal;">Non Scoring</th>
                                        <th>Scoring</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <ul class="list-inline vendor-stats-boxes">
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($SaleQualityAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($SaleQualityAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Sales<br />
                                                            Quality
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($OverallProfitabilityAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($OverallProfitabilityAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Overall<br />
                                                            Profitability
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <ul class="list-inline vendor-stats-boxes">
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($TrafficQualityAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($TrafficQualityAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Traffic<br />
                                                            Quality
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($TimeDeliveredAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($TimeDeliveredAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Time<br />
                                                            Delivered
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($LeadScoreAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($LeadScoreAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Lead<br />
                                                            Score
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vendor-stats-box <?php echo get_score_class_name($OverallDeliverabilityAvg);?>">
                                                        <h2 class="stats-value"><?php echo get_formated_number(round($OverallDeliverabilityAvg));?></h2>
                                                        <div class="stats-desc">
                                                            Overall<br />
                                                            Deliverability
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <ul class="list-inline vendor-stats-boxes">
                                <li>
                                    <div class="vendor-stats-box vendor-stats-chart">
                                        <input type="text" id="vendor-stats" class="knob" value="<?php echo round($VendorOverallScoreAvg);?>" data-width="70" data-height="70" data-fgcolor="<?php echo get_score_color_code($VendorOverallScoreAvg);?>" data-readonly="true" data-inputcolor="#606a73" data-bgcolor="#eeeeee" />
                                        <a class="knob-label">See Vendor Profile</a>
                                    </div>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
            if(count($paidtraffics) > 0){
            ?>
            <div class="table-responsive collapse data-table-container  collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-paidtraffic-tracking-stats data-table nowrap" data-nobuttons="true">
                    <thead>
                        <tr>
                            <th><span>Name</span></th>
                            <th><span>Group</span></th>
                            <th class="text-center stats-column"><span>Clicks</span></th>
                            <th class="text-center action-column"><span>Actions</span></th>
                            <th class="text-center sale-column"><span>Sales %</span></th>
                            <th class="text-center sale-column"><span>Sales $</span></th>
                            <th class="text-center non-score-column"><span class="text-center">Non Scoring</span></th>
                            <th class="text-center score-column"><span class="text-center">Scoring Blocks</span></th>
                            <th class="text-center score-column-" data-toggle="qtiptooltip" title="Overall Score"><span class="text-center">Score</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $campaigns;?>
                    </tbody>
                </table>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<?php        
            $contents = ob_get_contents();
            ob_end_clean();
        }
    }

    if($ReturnHTML){
        return $contents;
    } else {
        return array(
            "TotalPaidTraffics" => $TotalPaidTraffics,
            "TrafficQualityAvg" => $TrafficQualityAvg,
            "LeadScoreAvg" => $LeadScoreAvg,
            "TimeDeliveredAvg" => $TimeDeliveredAvg,
            "SaleQualityAvg" => $SaleQualityAvg,
            "OverallProfitabilityAvg" => $OverallProfitabilityAvg,
            "OverallDeliverabilityAvg" => $OverallDeliverabilityAvg,
            "VendorOverallScoreAvg" => $VendorOverallScoreAvg,
            "SaleQualityAvg" => $SaleQualityAvg,
            "OverallProfitabilityAvg" => $OverallProfitabilityAvg,
            "PaidTrafficStats" => $PaidTrafficStats
        );
    }
    
}

function UpdateVendorPaidTrafficStats($VendorID = 0, $PaidTrafficID = 0){
    
	$sql = "select ".DB()->vendors.".* from ".DB()->vendors." INNER JOIN ".DB()->paidtraffics." on ".DB()->vendors.".id = ".DB()->paidtraffics.".VendorID ".($VendorID != 0?" where ".DB()->vendors.".id = ".$VendorID:"")." GROUP BY ".DB()->vendors.".id";
    $vendors = DB()->get_results($sql);
    $user = null;
    foreach ($vendors as $vendor){
        if(!$user || ($user && $user->id != $vendor->userid))
            $user = DB()->get_row("select * from ".DB()->users." where id = ".$vendor->userid);

        $VendorScore = get_vendor_score($vendor, $user, false);
        $data = array(
            "TrafficQualityAvg"     => $VendorScore["TrafficQualityAvg"],
            "LeadScoreAvg"     => $VendorScore["LeadScoreAvg"],
            "TimeDeliveredAvg"     => $VendorScore["TimeDeliveredAvg"],
            "OverallDeliverabilityAvg"     => $VendorScore["OverallDeliverabilityAvg"],
            "VendorOverallScoreAvg"     => $VendorScore["VendorOverallScoreAvg"],
            "SaleQualityAvg"     => $VendorScore["SaleQualityAvg"],
            "OverallProfitabilityAvg"     => $VendorScore["OverallProfitabilityAvg"],
        );
        DB()->update(DB()->vendors, $data, array("id" => $vendor->id));

        if(isset($VendorScore["PaidTrafficStats"]) && is_array($VendorScore["PaidTrafficStats"])){
            $PaidTrafficStats = $VendorScore["PaidTrafficStats"];
            if($PaidTrafficID == 0){
                foreach ($PaidTrafficStats as $PaidTrafficStat){
                    $data = array(
                        "TrafficQuality"     => $PaidTrafficStat["TrafficQuality"],
                        "LeadScore"     => $PaidTrafficStat["LeadScore"],
                        "TimeDelivered"     => $PaidTrafficStat["TimeDelivered"],
                        "OverallDeliverability"     => $PaidTrafficStat["OverallDeliverability"],
                        "VendorOverallScore"     => $PaidTrafficStat["VendorOverallScore"],
                        "SaleQuality"     => $PaidTrafficStat["SaleQuality"],
                        "OverallProfitability"     => $PaidTrafficStat["OverallProfitability"],
                    );
                    DB()->update(DB()->paidtraffics, $data, array("id" => $PaidTrafficStat["PaidTrafficID"]));
                }
            } else{
                $PaidTrafficStat = $PaidTrafficStats["paidtraffic_".$PaidTrafficID];
                $data = array(
                        "TrafficQuality"     => $PaidTrafficStat["TrafficQuality"],
                        "LeadScore"     => $PaidTrafficStat["LeadScore"],
                        "TimeDelivered"     => $PaidTrafficStat["TimeDelivered"],
                        "OverallDeliverability"     => $PaidTrafficStat["OverallDeliverability"],
                        "VendorOverallScore"     => $PaidTrafficStat["VendorOverallScore"],
                        "SaleQuality"     => $PaidTrafficStat["SaleQuality"],
                        "OverallProfitability"     => $PaidTrafficStat["OverallProfitability"],
                    );
                DB()->update(DB()->paidtraffics, $data, array("id" => $PaidTrafficStat["PaidTrafficID"]));
            }
            
        }
    }
}
