<?php
if(!function_exists("LogUser")){
    /**
     * @return clsDBUsers
     */
    function LoggedInUser() {
        $LoggedInUser = SYS()->maybe_unserialize($_SESSION[SITE_PREFIX."-LoggedInUser"]);
        return $LoggedInUser;
    }
}
if(!function_exists("SystemUser")){
    /**
     * @return clsDBUsers
     */
    function SystemUser() {
        $SystemUser = LoggedInUser();
        return $SystemUser;
    }
}

function get_site_url($path = "", $echo = false){
    $url = SITE_URL.ltrim($path, "/");
    if($echo){
        echo $url;
        return false;
    }else{
        return $url;
    }
}

function site_url($path = ""){
    get_site_url($path, true);
}

function parseInt($val){
    $intval = 0;
    if (is_numeric($val)){
    	$intval = intval($val);
    }
    return $intval;
}

function redirect($url, $http_response_code = 301){
    header("location: ".$url, true, $http_response_code);
    echo '<script>window.location.href="'.$url.'"</script>';
}

function site_redirect($path){
    $url = get_site_url($path);
    redirect($url);
}

/**
 * Summary of get_menuitem
 * @param string $value
 * @param string $key
 * @return clsDBMenuItems
 */
function get_menuitem($value, $key = 'MenuItemURL'){
    global $MenuItemKey, $MenuItemSearchKey;
    $MenuItemKey = $key;
    $MenuItemSearchKey = $value;
    $MenuItems = LoggedInUser()->MenuItems;
    $MenuItemsFilered = array_filter($MenuItems, function($MenuItem){
        global $MenuItemKey, $MenuItemSearchKey;
        return($MenuItem->$MenuItemKey == $MenuItemSearchKey);
    });
    sort($MenuItemsFilered);

    if(isset($MenuItemsFilered[0]))
        $MenuItemFilered = $MenuItemsFilered[0];
    else
        $MenuItemFilered = $MenuItemsFilered[0];

    return $MenuItemFilered;
}

function get_available_domain($AvailableDomainID = 0, $isjson = true){

    $admindomain = $isjson?'{id:"", name:"", forward:"", url:"", type:""}':'';
    if($AvailableDomainID == 0){
        //$sql = "select DomainID from ".DB()->linkbanks." INNER JOIN ".DB()->domains." on ".DB()->linkbanks.".DomainID = ".DB()->domains.".id where DomainType = 'admindomain' group by DomainID ORDER BY count(DomainID) ASC LIMIT 0,1";
        //$sql = "select id from ".DB()->domains." where DomainType='admindomain' ORDER BY (select count(*) from ".DB()->linkbanks." where ".DB()->linkbanks.".DomainID = ".DB()->domains.".id) ASC LIMIT 0,1";
        $sql = "select id from ".DB()->domains." where DomainType='admindomain' HAVING ((select DomainBlacklisted from ".DB()->domain_statuses." where DomainID=".DB()->domains.".id ORDER BY ".DB()->domain_statuses.".id DESC LIMIT 0, 1)=0 and (select DomainServerError from ".DB()->domain_statuses." where DomainID=".DB()->domains.".id ORDER BY ".DB()->domain_statuses.".id DESC LIMIT 0, 1)=0) ORDER BY (select count(*) from ".DB()->linkbanks." where ".DB()->linkbanks.".DomainID = ".DB()->domains.".id) ASC LIMIT 0,1";

        $AvailableDomainID = DB()->get_var($sql);
    }
    $sql = "select * from ".DB()->domains." where id=".$AvailableDomainID;
    $AvailableDomain = DB()->get_row($sql);
    if($AvailableDomain){
        if($isjson)
            $admindomain = '{id:"'.$AvailableDomain->id.'", name:"'.$AvailableDomain->DomainName.'", forward:"'.$AvailableDomain->DomainForward.'", url:"'.$AvailableDomain->DomainUrl.'", type:"'.$AvailableDomain->DomainType.'"}';
        else
            $admindomain = $AvailableDomain;
    }
    return $admindomain;
}

function get_random_rbg($colors = array()){
    do{
        //$red = str_pad(dechex(mt_rand( 0, 255 )), 2, '0', STR_PAD_LEFT);
        //$green = str_pad(dechex(mt_rand( 0, 255 )), 2, '0', STR_PAD_LEFT);
        //$blue = str_pad(dechex(mt_rand( 0, 255 )), 2, '0', STR_PAD_LEFT);
        //$RGB = "#".$red.$green.$blue;

        $red = mt_rand( 0, 255);
        $green = mt_rand(0, 255);
        $blue = mt_rand(0, 255);

        $RGB = "rgba(".$red.",".$green.",".$blue.", 1.0)";

    }while(in_array($RGB, $colors));

    return $RGB;
}

function get_nearest_rbg($color){
    //$color = str_replace("#", "", $color);
    //$red = hexdec(substr($color, 0, 2));
    //$green = hexdec(substr($color, 2, 2));
    //$blue = hexdec(substr($color, 4, 2)) + 100;

    //$red = str_pad(dechex($red), 2, '0', STR_PAD_LEFT);
    //$green = str_pad(dechex($green), 2, '0', STR_PAD_LEFT);
    //$blue = str_pad(dechex($blue), 2, '0', STR_PAD_LEFT);

    //$RGB = "#".$red.$green.$blue;

    $RGB = str_replace("1.0", "0.7", $color);

    //$colordec = hexdec($color);
    //$colordec += 40;
    //$RGB = "#".(dechex($colordec));

    return $RGB;
}

function get_saleinfo($row, $type, $table){
	$LoggedInUser = LoggedInUser();

    if(is_numeric($row)){
        $maintable = $table."s";
        $sql = "select * from ".DB()->$maintable." where userid=".$LoggedInUser->id." and id=".$row;
        $row = DB()->get_row($sql);
    }

    $saleinfotable = $table."_saleinfo";
    $linkbankedid = $table == "links" ? "LinkBankID" : "CampaignID";
    $sql = "select IFNULL(SUM(SaleInfoAmount), 0) as SaleInfoAmount, IFNULL(COUNT(*), 0) as SaleInfoCount from ".DB()->$saleinfotable." where userid=".$LoggedInUser->id." and ".$linkbankedid."=".$row->id." and SaleInfoType='".$type."'";
    $SaleInfo = DB()->get_row($sql);

    if($SaleInfo && $SaleInfo->SaleInfoCount > 0)
        $SaleInfoAmount = $SaleInfo->SaleInfoAmount;
    else
        $SaleInfoAmount = $row->$type * 1;

    return $SaleInfoAmount;
}

function save_cookie($key, $value){
	setcookie($key, $value, time()+(60 * 60 * 24 * 7), "/");
}

function remove_cookie($key){
    setcookie($key, "", time() - 3600, "/");
}

function get_cookie($key){
    $value = !isset($_COOKIE[$key]) ? "" : $_COOKIE[$key];
    return $value;
}

function get_ipaddress_info($IpAddress){


    require_once "dbip.class.php";
    //$mysqlidb = mysqli_connect("easylinks.io", "easylink_geouser", "eyBy@xcUMk)R", "easylink_geodb");
    //mysqli_select_db($mysqlidb, "easylink_geodb");

    //$mysqlidb = mysqli_connect(DB()->dbhost, DB()->dbuser, DB()->dbpassword, DB()->dbname);
    //mysqli_select_db($mysqlidb, DB()->dbname);

    $mysqlidb = mysqli_connect(GEO_DB_HOST, GEO_DB_USER, GEO_DB_PASSWORD, GEO_DB_NAME);
    mysqli_select_db($mysqlidb, GEO_DB_NAME);

	$dbip = new DBIP_MySQLI($mysqlidb);
    $IpAddressInfo = $dbip->Lookup(trim($IpAddress), "l2sun1el_ipaddress_info");//DB()->ipaddress_info
    return $IpAddressInfo;
}

function get_formated_number($number){
	//setlocale(LC_MONETARY, 'en_US');
    $number = intval($number) == floatval($number)?intval($number):$number;
    $FormatedNumber = number_format($number, is_float($number)?2:0);
    return $FormatedNumber;
}

function get_float_number($number){
    $number = intval($number) == floatval($number)?intval($number):$number;
    $get_float_number = round($number, is_float($number)?2:0);
    return $get_float_number;
}

function delete_file($file){
	if (@file_exists($file)){
        @unlink($file);
    }
}

function get_domain_server_error($DomainHost, $DomainPort = 80)
{
	$socket = @fsockopen($DomainHost, $DomainPort, $errorno, $errorstr, 2);
	if ($socket)
        return false;
	else
        return true;
}

function get_url_status($url) {
    $agent = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; pt-pt) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_exec($ch);

    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    echo $httpcode;

    if ($httpcode >= 200 && $httpcode < 300)
        return "true";
    else
        return "false";
}

function get_domain_black_list_status($domain){
    $apikey = "ea8c33a2c45b92b132474d94bb490241";
	$jsonString = @file_get_contents("http://api.whoapi.com/?apikey=".$apikey."&r=blacklist&domain=".$domain."&ip=&rr=json");
    $jsonData = @json_decode($jsonString);

    return $jsonData;
}

function get_domain_status_icon($DomainID = 0, $DomainType = 'userdomain'){
	$LoggedInUser = LoggedInUser();

    $status_icon = '<i class="fa fa-check-circle green-icon"></i>';
    $status_text = "Link is working";
    $domain_status = DB()->get_row("select * from ".DB()->domain_statuses." inner join ".DB()->domains." on ".DB()->domain_statuses.".DomainID = ".DB()->domains.".id where ".DB()->domains.".userid=".$LoggedInUser->id." and ".DB()->domains.".DomainType='".$DomainType."' and ".DB()->domains.".id=".$DomainID);
    if($domain_status){
        if($domain_status->DomainBlacklisted == 1 && $domain_status->DomainServerError == 0){
            $status_icon = '<i class="fa fa-exclamation-circle yellow-icon"></i>';
            $status_text = "Link is blacklisted";
        }else if($domain_status->DomainServerError == 1){
            $status_icon = '<i class="fa fa-times-circle red-icon"></i>';
            $status_text = "Link is offline";
        }
    }
    $StatusIcon = '<span title="'.$status_text.'" data-toggle="qtiptooltip" data-placement="auto right" >'.$status_icon.'</span>';
    return $StatusIcon;
}

function get_files($dir, $maindir){
    $files = array();
	if ($handle = opendir($dir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $filepath = $dir."/".$file;
                if(is_file($filepath)){
                    $files[] = str_replace($maindir."/", "", $filepath);
                }else if(is_dir($filepath)){
                    $files = array_merge($files, get_files($filepath, $maindir));
                }
            }
        }
        closedir($handle);
    }
    return $files;
}

function is_visible_link_valid($DomainID, $VisibleLink, $user = null){
	$LoggedInUser = LoggedInUser();
    $isValidLink = true;

    if($user == null)
        $user = $LoggedInUser;

    $sql = "select * from ".DB()->linkbanks." where userid=".$user->id." and DomainID = ".$DomainID." and VisibleLink = '".$VisibleLink."'";
    $linkbank = DB()->get_row($sql);

    $sql = "select * from ".DB()->rotators." where userid=".$user->id." and DomainID = ".$DomainID." and VisibleLink = '".$VisibleLink."'";
    $rotator = DB()->get_row($sql);

    $sql = "select * from ".DB()->link_sequences." where userid=".$user->id." and DomainID = ".$DomainID." and VisibleLink = '".$VisibleLink."'";
    $link_sequence = DB()->get_row($sql);

    $sql = "select * from ".DB()->paidtraffics." where userid=".$user->id." and DomainID = ".$DomainID." and VisibleLink = '".$VisibleLink."'";
    $paidtraffic = DB()->get_row($sql);

    if($linkbank || $paidtraffic || $rotator || $link_sequence)
        $isValidLink = false;

    return $isValidLink;
}

function get_income_row($income){
    $LoggedInUser = LoggedInUser();
    if(is_numeric($income))
        $income = DB()->get_row("select * from ".DB()->manualsale_incomes." where userid=".$LoggedInUser->id." and id=".$income);

	ob_start();
?>
<tr>
    <td class="name-col">
        <div><?php echo $income->IncomeName;?></div>
        <div class="sale-note"><?php echo $income->IncomeNote;?></div>
    </td>
    <td class="amount-col">
        <span>$<?php echo $income->IncomeAmount;?></span>
    </td>
    <td class="edit-col">
        <a href="<?php site_url("income-expense/income/".$income->id."/?ManualSaleSplitPartnerID=".$income->ManualSaleSplitPartnerID)?>" class="btn-sm btn-link btn-edit-expense" data-widget="ShowLinkModel" data-method="post" data-page-title="Income edit"><i class="fa fa-pencil"></i></a>
    </td>
    <td class="remove-col">
        <a href="javascript:" class="btn-sm btn-link btn-remove-income" data-income-id="<?php echo $income->id?>" data-splitpartner-id="<?php echo $income->ManualSaleSplitPartnerID?>"><i class="fa fa-trash"></i></a>
    </td>
</tr>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function get_other_income_row($other_income){
    $LoggedInUser = LoggedInUser();
    if(is_numeric($other_income))
        $other_income = DB()->get_row("select * from ".DB()->manualsale_other_incomes." where userid=".$LoggedInUser->id." and id=".$other_income);

	ob_start();
?>
<tr>
    <td class="name-col">
        <div><?php echo $other_income->OtherIncomeName;?></div>
        <div class="sale-note"><?php echo $other_income->OtherIncomeNote;?></div>
    </td>
    <td class="amount-col">
        <span>$<?php echo $other_income->OtherIncomeAmount;?></span>
    </td>
    <td class="edit-col">
        <a href="<?php site_url("income-expense/otherIncome/".$other_income->id."/?ManualSaleSplitPartnerID=".$other_income->ManualSaleSplitPartnerID)?>" class="btn-sm btn-link btn-edit-expense" data-widget="ShowLinkModel" data-method="post" data-page-title="OtherIncome edit"><i class="fa fa-pencil"></i></a>
    </td>
    <td class="remove-col">
        <a href="javascript:" class="btn-sm btn-link btn-remove-other_income" data-other_income-id="<?php echo $other_income->id?>" data-splitpartner-id="<?php echo $other_income->ManualSaleSplitPartnerID?>"><i class="fa fa-trash"></i></a>
    </td>
</tr>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function get_expense_row($expense){
    $LoggedInUser = LoggedInUser();
    if(is_numeric($expense))
        $expense = DB()->get_row("select * from ".DB()->manualsale_expenses." where userid=".$LoggedInUser->id." and id=".$expense);

	ob_start();
?>
<tr>
    <td class="name-col">
        <div><?php echo $expense->ExpenseName;?></div>
        <div class="sale-note"><?php echo $expense->ExpenseNote;?></div>
    </td>
    <td class="amount-col">
        <span>$<?php echo $expense->ExpenseAmount;?></span>
    </td>
    <td class="edit-col">
        <a href="<?php site_url("income-expense/expense/".$expense->id."/?ManualSaleSplitPartnerID=".$expense->ManualSaleSplitPartnerID)?>" class="btn-sm btn-link btn-edit-expense" data-widget="ShowLinkModel" data-method="post" data-page-title="Expense edit"><i class="fa fa-pencil"></i></a>
    </td>
    <td class="remove-col">
        <a href="javascript:" class="btn-sm btn-link btn-remove-expense" data-expense-id="<?php echo $expense->id?>" data-splitpartner-id="<?php echo $expense->ManualSaleSplitPartnerID?>"><i class="fa fa-trash"></i></a>
    </td>
</tr>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function send_mail($to, $subject, $message, $from = "no-reply@easylinks.io", $headers = ""){
	$headers .= "From: ".$from."\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    return @mail($to, $subject, $message, $headers);
}

function get_score_class_name($Score){
    $ClassName = "";
	if($Score <= 25)//$Score <= 0 &&
        $ClassName = "purple-vendor-box";
    else if($Score >= 26 && $Score <= 50)
        $ClassName = "red-vendor-box";
    else if($Score >= 51 && $Score <= 75)
        $ClassName = "orange-vendor-box";//$ClassName = "yellow-vendor-box";
    else if($Score >= 76)// && $Score <= 100
        $ClassName = "green-vendor-box";

    return $ClassName;
}

function get_score_color_code($Score){
    $ColorCode = "";
	if($Score <= 25)//$Score >= 0 &&
        $ColorCode = "#694f6c";
    else if($Score >= 26 && $Score <= 50)
        $ColorCode = "#e74c36";
    else if($Score >= 51 && $Score <= 75)
        $ColorCode = "#ff9738";//$ColorCode = "#ffd800";
    else if($Score >= 76)// && $Score <= 100
        $ColorCode = "#aad106";

    return $ColorCode;
}

function print_formated($obj){
	echo "<pre>".print_r($obj, true)."</pre>";
}

function get_pixel_image($PixelType, $UnitPrice = "", $QueryString = "") {
    $LoggedInUser = LoggedInUser();

    $PixelURL = "http://easylinks.online/api/" . $PixelType . "/pixel_" . $LoggedInUser->id . ".png?uid=" . $LoggedInUser->id;

    if ($UnitPrice != "")
        $PixelURL .= "&p=" . $UnitPrice;

    if ($QueryString != "")
        $PixelURL .= "&" . $QueryString;

    $PixelImage = '<img height="1" width="1" alt="" style="display:none" src="' . $PixelURL . '" />';

    return $PixelImage;
}

function get_option($OptionName, $OptionDefault=null){


    $sql = "select OptionValue from ".DB()->options." where ";

    if(is_numeric($OptionName))
        $sql .= " id=".$OptionName;
    else
        $sql .= " OptionName='".$OptionName."'";

    $OptionValue = DB()->get_var($sql);

    if(!$OptionValue)
        $OptionValue = $OptionDefault;

    return $OptionValue;
}

function get_today_dates(){
	$dtNow = strtotime("now");
    $dtStart = new DateTime(date('m/d/Y 00:00:00', $dtNow));
    $dtEnd = new DateTime(date('m/d/Y 23:59:59', $dtNow));
    //$dtEnd = new DateTime(date('m/d/Y 00:00:00', $dtNow));
    //$dtEnd->modify("+1 day");
    return array(
        "start_date_time"    =>  strtotime($dtStart->format("r")),
        "end_date_time"    =>  strtotime($dtEnd->format("r")),
        "start_date"    =>  $dtStart->format("r"),
        "end_date"    =>  $dtEnd->format("r"),
        "start_date_only"    =>  $dtStart->format("m/d/Y"),
        "end_date_only"    =>  $dtEnd->format("m/d/Y")
    );
}

function get_last_seven_days(){
	$dtNow = strtotime("now");
    $dtStart = new DateTime(date('m/d/Y 00:00:00', $dtNow));
    $dtEnd = new DateTime(date('m/d/Y 23:59:59', $dtNow));
    //$dtEnd = new DateTime(date('m/d/Y 00:00:00', $dtNow));

    $dtStart->modify("-6 days");
    //$dtEnd->modify("+1 day");
    return array(
        "start_date_time"    =>  strtotime($dtStart->format("r")),
        "end_date_time"    =>  strtotime($dtEnd->format("r")),
        "start_date"    =>  $dtStart->format("r"),
        "end_date"    =>  $dtEnd->format("r"),
        "start_date_only"    =>  $dtStart->format("m/d/Y"),
        "end_date_only"    =>  $dtEnd->format("m/d/Y")
    );
}