<?php
function LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats = array(), $colors = array()){
    $legends = array("Unique", "Non-Unique", "Bots");
    //$colors = array();
    $legendsvalues = array("Total" => 0);

    $piedata = array();
    foreach($legends as $legend){
        if(!array_key_exists($legend, $colors)){
            $color = get_random_rbg($colors);//
            $colors[$legend] = $color;
        }else{
            $color = $colors[$legend];
        }
        $nearstcolor = get_nearest_rbg($color);

        $piedata[] = '{"value": "'.$link_sequence_stats[strtolower($legend)].'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $link_sequence_stats[strtolower($legend)];
        $legendsvalues["Total"] += $link_sequence_stats[strtolower($legend)];
    }

    $labels = array();
    $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
    for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }

    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);

        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd->modify("+1 day");

        for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->link_sequence_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
            $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

            if($legend == "Unique"){
                $sql = "SELECT SUM(count) from (select 1 AS count from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ClickIp) as LinkSequenceVisitsCount";
                $value = DB()->get_var($sql) * 1;
            }else if($legend == "Non-Unique"){
                //$sql = "SELECT SUM(count) from (select 1 AS count from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ClickIp) as LinkSequenceVisitsCount";
                //$unique = DB()->get_var($sql) * 1;
                $sql = "SELECT SUM(count) from (select count(ClickIp) AS count from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ClickIp) as LinkSequenceVisitsCount";
                $value = DB()->get_var($sql) * 1;
                //$value -= $unique;
            }else if($legend == "Bots"){
                $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
                $value = DB()->get_var($sql) * 1;
            }
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );

    return $data;
}

function LinkSequence_TopBrowser($LinkSequenceID, $link_sequence_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($link_sequence_stats['startdate']) && !empty($link_sequence_stats['enddate'])?" and (".DB()->link_sequence_clicks.".DateAdded >= '".$link_sequence_stats['startdate']."' and ".DB()->link_sequence_clicks.".DateAdded <= '".$link_sequence_stats['enddate']."') ":"";
    $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
    $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

    $sql = "select count(*) as BrowserLinkSequenceVisits, CONCAT(BrowserName , ' ' , BrowserVersion) as Browser from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY Browser";
    $Browsers = DB()->get_results($sql);
    foreach($Browsers as $Browser){
        $legend = $Browser->Browser;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $BrowserLinkSequenceVisits = $Browser->BrowserLinkSequenceVisits;

        $piedata[] = '{"value": "'.$BrowserLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $BrowserLinkSequenceVisits;
        $legendsvalues["Total"] += $BrowserLinkSequenceVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
    $BotsLinkSequenceVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsLinkSequenceVisits;
    $legendsvalues["Total"] += $BotsLinkSequenceVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
    for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->link_sequence_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
            $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as BrowserLinkSequenceVisits from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and CONCAT(BrowserName , ' ' , BrowserVersion) = '".$legend."' and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY CONCAT(BrowserName , ' ' , BrowserVersion)";
            
            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function LinkSequence_TopPlatform($LinkSequenceID, $link_sequence_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($link_sequence_stats['startdate']) && !empty($link_sequence_stats['enddate'])?" and (".DB()->link_sequence_clicks.".DateAdded >= '".$link_sequence_stats['startdate']."' and ".DB()->link_sequence_clicks.".DateAdded <= '".$link_sequence_stats['enddate']."') ":"";
    $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
    $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

    $sql = "select count(*) as PlatformLinkSequenceVisits, Platform from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY Platform";
    $Platforms = DB()->get_results($sql);
    foreach($Platforms as $Platform){
        $legend = $Platform->Platform;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $PlatformLinkSequenceVisits = $Platform->PlatformLinkSequenceVisits;

        $piedata[] = '{"value": "'.$PlatformLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $PlatformLinkSequenceVisits;
        $legendsvalues["Total"] += $PlatformLinkSequenceVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
    $BotsLinkSequenceVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsLinkSequenceVisits;
    $legendsvalues["Total"] += $BotsLinkSequenceVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
    for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->link_sequence_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
            $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as PlatformLinkSequenceVisits from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and Platform = '".$legend."' and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY Platform";

            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function LinkSequence_TopCountries($LinkSequenceID, $link_sequence_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($link_sequence_stats['startdate']) && !empty($link_sequence_stats['enddate'])?" and (".DB()->link_sequence_clicks.".DateAdded >= '".$link_sequence_stats['startdate']."' and ".DB()->link_sequence_clicks.".DateAdded <= '".$link_sequence_stats['enddate']."') ":"";
    $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
    $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

    $sql = "select count(*) as CountryLinkSequenceVisits, countryname from ".$table." INNER JOIN ".DB()->countries." on ".DB()->link_sequence_clicks.".CountryCode = ".DB()->countries.".countrycode where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ".DB()->link_sequence_clicks.".CountryCode";
    $Countries = DB()->get_results($sql);
    foreach($Countries as $Country){
        $legend = $Country->countryname;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $CountryLinkSequenceVisits = $Country->CountryLinkSequenceVisits;

        $piedata[] = '{"value": "'.$CountryLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $CountryLinkSequenceVisits;
        $legendsvalues["Total"] += $CountryLinkSequenceVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
    $BotsLinkSequenceVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsLinkSequenceVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsLinkSequenceVisits;
    $legendsvalues["Total"] += $BotsLinkSequenceVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
    for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd = new DateTime(date('r', $link_sequence_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $link_sequence_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->link_sequence_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
            $LinkQuery = isset($_GET["LinkSequenceLinkID"])?" and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where LinkSequenceID=".$LinkSequenceID.$LinkQuery.$datesql." and (".DB()->link_sequence_clicks.".BotName <> '' and ".DB()->link_sequence_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as CountryLinkSequenceVisits from ".$table." INNER JOIN ".DB()->countries." on ".DB()->link_sequence_clicks.".CountryCode = ".DB()->countries.".countrycode where LinkSequenceID=".
                $datesql." and countryname='".$legend."' and (".DB()->link_sequence_clicks.".BotName = '' or ".DB()->link_sequence_clicks.".BotName is NULL) GROUP BY ".DB()->link_sequence_clicks.".CountryCode";

            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}