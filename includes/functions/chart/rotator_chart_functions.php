<?php
function Rotator_SnapShot($RotatorID, $rotator_stats = array(), $colors = array()){
    $legends = array("Unique", "Non-Unique", "Bots");
    //$colors = array();
    $legendsvalues = array("Total" => 0);

    $piedata = array();
    foreach($legends as $legend){
        if(!array_key_exists($legend, $colors)){
            $color = get_random_rbg($colors);//
            $colors[$legend] = $color;
        }else{
            $color = $colors[$legend];
        }
        $nearstcolor = get_nearest_rbg($color);

        $piedata[] = '{"value": "'.$rotator_stats[strtolower($legend)].'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $rotator_stats[strtolower($legend)];
        $legendsvalues["Total"] += $rotator_stats[strtolower($legend)];
    }

    $labels = array();
    $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
    for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }

    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);

        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd->modify("+1 day");

        for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->rotator_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
            $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

            if($legend == "Unique"){
                $sql = "SELECT SUM(count) from (select 1 AS count from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ClickIp) as RotatorVisitsCount";
                $value = DB()->get_var($sql) * 1;
            }else if($legend == "Non-Unique"){
                //$sql = "SELECT SUM(count) from (select 1 AS count from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ClickIp) as RotatorVisitsCount";
                //$unique = DB()->get_var($sql) * 1;
                $sql = "SELECT SUM(count) from (select count(ClickIp) AS count from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ClickIp) as RotatorVisitsCount";
                $value = DB()->get_var($sql) * 1;
                //$value -= $unique;
            }else if($legend == "Bots"){
                $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
                $value = DB()->get_var($sql) * 1;
            }
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );

    return $data;
}

function Rotator_TopBrowser($RotatorID, $rotator_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($rotator_stats['startdate']) && !empty($rotator_stats['enddate'])?" and (".DB()->rotator_clicks.".DateAdded >= '".$rotator_stats['startdate']."' and ".DB()->rotator_clicks.".DateAdded <= '".$rotator_stats['enddate']."') ":"";
    $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
    $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

    $sql = "select count(*) as BrowserRotatorVisits, CONCAT(BrowserName , ' ' , BrowserVersion) as Browser from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY Browser";
    $Browsers = DB()->get_results($sql);
    foreach($Browsers as $Browser){
        $legend = $Browser->Browser;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $BrowserRotatorVisits = $Browser->BrowserRotatorVisits;

        $piedata[] = '{"value": "'.$BrowserRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $BrowserRotatorVisits;
        $legendsvalues["Total"] += $BrowserRotatorVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
    $BotsRotatorVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsRotatorVisits;
    $legendsvalues["Total"] += $BotsRotatorVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
    for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->rotator_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
            $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as BrowserRotatorVisits from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and CONCAT(BrowserName , ' ' , BrowserVersion) = '".$legend."' and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY CONCAT(BrowserName , ' ' , BrowserVersion)";
            
            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function Rotator_TopPlatform($RotatorID, $rotator_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($rotator_stats['startdate']) && !empty($rotator_stats['enddate'])?" and (".DB()->rotator_clicks.".DateAdded >= '".$rotator_stats['startdate']."' and ".DB()->rotator_clicks.".DateAdded <= '".$rotator_stats['enddate']."') ":"";
    $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
    $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

    $sql = "select count(*) as PlatformRotatorVisits, Platform from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY Platform";
    $Platforms = DB()->get_results($sql);
    foreach($Platforms as $Platform){
        $legend = $Platform->Platform;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $PlatformRotatorVisits = $Platform->PlatformRotatorVisits;

        $piedata[] = '{"value": "'.$PlatformRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $PlatformRotatorVisits;
        $legendsvalues["Total"] += $PlatformRotatorVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
    $BotsRotatorVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsRotatorVisits;
    $legendsvalues["Total"] += $BotsRotatorVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
    for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->rotator_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
            $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as PlatformRotatorVisits from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and Platform = '".$legend."' and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY Platform";

            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function Rotator_TopCountries($RotatorID, $rotator_stats = array()){
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($rotator_stats['startdate']) && !empty($rotator_stats['enddate'])?" and (".DB()->rotator_clicks.".DateAdded >= '".$rotator_stats['startdate']."' and ".DB()->rotator_clicks.".DateAdded <= '".$rotator_stats['enddate']."') ":"";
    $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
    $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

    $sql = "select count(*) as CountryRotatorVisits, countryname from ".$table." INNER JOIN ".DB()->countries." on ".DB()->rotator_clicks.".CountryCode = ".DB()->countries.".countrycode where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ".DB()->rotator_clicks.".CountryCode";
    $Countries = DB()->get_results($sql);
    foreach($Countries as $Country){
        $legend = $Country->countryname;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $CountryRotatorVisits = $Country->CountryRotatorVisits;

        $piedata[] = '{"value": "'.$CountryRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

        $legendsvalues[$legend] = $CountryRotatorVisits;
        $legendsvalues["Total"] += $CountryRotatorVisits;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
    $BotsRotatorVisits = DB()->get_var($sql) * 1;

    $piedata[] = '{"value": "'.$BotsRotatorVisits.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';

    $legendsvalues[$legend] = $BotsRotatorVisits;
    $legendsvalues["Total"] += $BotsRotatorVisits;


    $labels = array();
    $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
    for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
        $labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        $dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $datavalues = array();

        $dtStart = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd = new DateTime(date('r', $rotator_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $rotator_stats['totaldays']; $dayindex++){
            $value = 0;
            $datesql = " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".DB()->rotator_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";
            $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
            $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

            if($legend == 'Bots')
                $sql = "select count(ClickIp) from ".$table." where RotatorID=".$RotatorID.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
            else
                $sql = "select count(*) as CountryRotatorVisits from ".$table." INNER JOIN ".DB()->countries." on ".DB()->rotator_clicks.".CountryCode = ".DB()->countries.".countrycode where RotatorID=".
                $datesql." and countryname='".$legend."' and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ".DB()->rotator_clicks.".CountryCode";

            $value = DB()->get_var($sql) * 1;
            $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset .= implode(',', $datavalues).']}';

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}