<?php
function get_manualsale_amount($LinkBankID = 0, $MasterCampaignID = 0){
	$LoggedInUser = LoggedInUser();
    $SaleAmount = array(
        "TotalIncome"      => 0,
        "TotalOtherIncome" => 0,
        "TotalExpense"      => 0,
        "TotalAmount"      => 0
    );

    $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where userid = ".$LoggedInUser->id." and MasterCampaignID = ".$MasterCampaignID." and LinkBankID = ".$LinkBankID);
    if ($ManualSale){
        if(is_percentage_valid($ManualSale)){
            $TotalIncome = DB()->get_var("select sum(".DB()->manualsale_incomes.".IncomeAmount) from ".DB()->manualsale_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
            $TotalOtherIncome = DB()->get_var("select sum(".DB()->manualsale_other_incomes.".OtherIncomeAmount) from ".DB()->manualsale_other_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_other_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
            $TotalExpense = DB()->get_var("select sum(".DB()->manualsale_expenses.".ExpenseAmount) from ".DB()->manualsale_expenses." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_expenses.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;

            $TotalAmount = ($TotalIncome + $TotalOtherIncome) - $TotalExpense;

            $SaleAmount = array(
                "TotalIncome"           => $TotalIncome,
                "TotalOtherIncome"      => $TotalOtherIncome,
                "TotalExpense"          => $TotalExpense,
                "TotalAmount"           => $TotalAmount < 0?0:$TotalAmount
            );
        }
    }

    return $SaleAmount;
}

function get_manualsales($StartDate = "", $EndDate = ""){
	$LoggedInUser = LoggedInUser();
    $SaleAmount = array(
        "TotalIncome"      => 0,
        "TotalOtherIncome" => 0,
        "TotalExpense"      => 0,
        "TotalAmount"      => 0
    );

    $TotalIncome = DB()->get_var("select sum(".DB()->manualsale_incomes.".IncomeAmount) from ".DB()->manualsale_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ".DB()->manualsale_incomes.".userid =".$LoggedInUser->id.($StartDate!="" && $EndDate!=""?" and (".DB()->manualsale_incomes.".DateAdded >= '".$StartDate."' and ".DB()->manualsale_incomes.".DateAdded <= '".$EndDate."') ":"")) * 1;
    $TotalOtherIncome = DB()->get_var("select sum(".DB()->manualsale_other_incomes.".OtherIncomeAmount) from ".DB()->manualsale_other_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_other_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ".DB()->manualsale_incomes.".userid =".$LoggedInUser->id.($StartDate!="" && $EndDate!=""?" and (".DB()->manualsale_incomes.".DateAdded >= '".$StartDate."' and ".DB()->manualsale_incomes.".DateAdded <= '".$EndDate."') ":"")) * 1;
    $TotalExpense = DB()->get_var("select sum(".DB()->manualsale_expenses.".ExpenseAmount) from ".DB()->manualsale_expenses." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_expenses.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ".DB()->manualsale_incomes.".userid =".$LoggedInUser->id.($StartDate!="" && $EndDate!=""?" and (".DB()->manualsale_incomes.".DateAdded >= '".$StartDate."' and ".DB()->manualsale_incomes.".DateAdded <= '".$EndDate."') ":"")) * 1;

    $TotalAmount = ($TotalIncome + $TotalOtherIncome) - $TotalExpense;

    $SaleAmount = array(
        "TotalIncome"           => $TotalIncome,
        "TotalOtherIncome"      => $TotalOtherIncome,
        "TotalExpense"          => $TotalExpense,
        "TotalAmount"           => $TotalAmount < 0?0:$TotalAmount
    );

    return $SaleAmount;
}

function get_manual_sale($isSplitPartnerSale = 0, $MasterCampaignID = 0, $LinkBankID = 0){
	$LoggedInUser = LoggedInUser();

    $data = array(
        "MasterCampaignID"      => $MasterCampaignID,
        "LinkBankID"            => $LinkBankID,
        "isSplitPartnerSale"    => $isSplitPartnerSale,
        "DateAdded"             => strtotime("now"),
        "userid"                => $LoggedInUser->id
    );

    $sql = "select * from ".DB()->manualsales." where MasterCampaignID=".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$LoggedInUser->id;
    $ManualSale = DB()->get_row($sql);

    if($ManualSale){
        $ManualSaleID = $ManualSale->id;
        $SplitPartners = DB()->get_var("select count(*) from ".DB()->manualsale_split_partners." where ManualSaleID=".$ManualSaleID." and userid=".$LoggedInUser->id);
        if($SplitPartners <= 0){
            $data = array(
                "ManualSaleID"          => $ManualSaleID,
                "SplitPartnerID"        => $LoggedInUser->id,
                "isUserSale"            => 1,
                "IncomePercentage"      => 100,
                "OtherIncomePercentage" => 100,
                "ExpensePercentage"     => 100,
                "DateAdded"             => strtotime("now"),
                "userid"                => $LoggedInUser->id
            );
            DB()->insert(DB()->manualsale_split_partners, $data);
        }
    }else{
        if(DB()->insert(DB()->manualsales, $data) && is_numeric(DB()->insert_id)){
            $ManualSaleID = DB()->insert_id;

            $data = array(
                "ManualSaleID"          => $ManualSaleID,
                "SplitPartnerID"        => $LoggedInUser->id,
                "isUserSale"            => 1,
                "IncomePercentage"      => 100,
                "OtherIncomePercentage" => 100,
                "ExpensePercentage"     => 100,
                "DateAdded"             => strtotime("now"),
                "userid"                => $LoggedInUser->id
            );
            DB()->insert(DB()->manualsale_split_partners, $data);

            $sql = "select * from ".DB()->manualsales." where id=".$ManualSaleID." and userid=".$LoggedInUser->id;
            $ManualSale = DB()->get_row($sql);
        }
    }

    return $ManualSale;
}

function get_manual_sale_result($ManualSale){
    $LoggedInUser = LoggedInUser();

    $UserResultValues = get_manual_sale_result_values($ManualSale);
    $Results = array();

    foreach ($UserResultValues as $UserResultKey => $UserResultValue){
        ob_start();
?>
<tr>
    <td class="label-col">
        <span>Income</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['Income']);?></span>
        <input type="hidden" value="$<?php echo $UserResultValue['TotalIncome'];?>" class="TotalIncome" />
        <input type="hidden" value="$<?php echo $UserResultValue['TotalExpense'];?>" class="TotalExpense" />
    </td>
</tr>
<tr>
    <td class="label-col">
        <span>Other Income</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['OtherIncome']);?></span>
    </td>
</tr>
<tr>
    <td class="label-col">
        <span>Expense</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['Expense']);?></span>
    </td>
</tr>
<tr>
    <td class="label-col">
        <strong>Gross Profit</strong>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['Gross_Profit']);?></span>
    </td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td class="label-col">
        <span>Income Split (<?php echo $UserResultValue['Income_Split_Percentage'];?>%)</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['Income_Split']);?></span>
    </td>
</tr>
<tr>
    <td class="label-col">
        <span>Other Income Split (<?php echo $UserResultValue['OtherIncome_Split_Percentage'];?>%)</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['OtherIncome_Split']);?></span>
    </td>
</tr>
<tr>
    <td class="label-col">
        <span>Expense Split (<?php echo $UserResultValue['Expense_Split_Percentage'];?>%)</span>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['Expense_Split']);?></span>
    </td>
</tr>
<tr>
    <td class="label-col">
        <strong>Owed Partner</strong>
    </td>
    <td class="amount-col">
        <span class="<?php echo $UserResultValue['UserBalance']>0?"text-green":"text-red";?>">$<?php echo $UserResultValue['UserBalance']>0?get_formated_number($UserResultValue['UserBalance']):get_formated_number(($UserResultValue['UserBalance'] * -1));?></span>
    </td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<tr>
    <td class="label-col">
        <strong>This Partner's Profit</strong>
    </td>
    <td class="amount-col">
        <span>$<?php echo get_formated_number($UserResultValue['UserProfit']);?></span>
    </td>
</tr>
<?php
        if($UserResultValue['isSplitPartnerSale'] == 1 && count($UserResultValues) > 1 && $UserResultValue['UserBalance'] != 0){
            if($UserResultValue['UserBalance'] < 0){
                foreach ($UserResultValue['SendTo'] as $SendToUser){
                    $ReceiverResultValue = $UserResultValues["manualsale_split_partner_".$SendToUser["ReceiverID"]];
                    if($SendToUser["Amount"] != 0){
                        $PartnerPayment = DB()->get_row("select * from ".DB()->manualsale_split_partner_payments." where ReferenceNo is not null and ReferenceNo <> '' and userid=".$LoggedInUser->id." and SenderManualSaleSplitPartnerID=".$UserResultValue["SplitPartnerID"]." and ReceiverManualSaleSplitPartnerID=".$SendToUser["ReceiverID"]);
                        if(!$PartnerPayment) {
?>
<tr class="no-border">
    <td colspan="2" style="text-align: center;">
        <a href="<?php site_url("manual-sale/split-partners/pay-partner/".$UserResultValue["ManualSaleID"]."/".$UserResultValue["SplitPartnerID"]."/".$SendToUser["ReceiverID"]."/?SentAmount=".$SendToUser["Amount"])?>" class="btn btn-success btn-send-partner-payment btn-xs-" data-widget="ShowLinkModel" data-method="post" data-page-title="Send Partner Payment">Pay $<?php echo get_formated_number($SendToUser["Amount"]);?> to <?php echo $ReceiverResultValue["DisplayName"];?>
        </a>
    </td>
</tr>
<?php
                        } else {
?>
<tr class="no-border">
    <td colspan="2" style="">
        <div class="well well-sm payment-status-well">
            <span class="text-green">Status</span><br />
            <?php 
                            echo "Sent $".get_formated_number($SendToUser["Amount"])." to ".$ReceiverResultValue["DisplayName"]." - (CONF# ".$PartnerPayment->ReferenceNo.")";
            ?>
        </div>
    </td>
</tr>
<?php
                        }
                    }
                }
            }else {
                foreach ($UserResultValue['ReceiveFrom'] as $ReceiveFromUser){
                    $SenderResultValue = $UserResultValues["manualsale_split_partner_".$ReceiveFromUser["SenderID"]];
                    if($ReceiveFromUser["Amount"] != 0){
                        $PartnerPayment = DB()->get_row("select * from ".DB()->manualsale_split_partner_payments." where ReferenceNo is not null and ReferenceNo <> '' and userid=".$LoggedInUser->id." and SenderManualSaleSplitPartnerID=".$ReceiveFromUser["SenderID"]." and ReceiverManualSaleSplitPartnerID=".$UserResultValue["SplitPartnerID"]);
                        
?>
<tr class="no-border">
    <td colspan="2" style="">
        <div class="well well-sm payment-status-well">
            <span class="<?php echo $PartnerPayment?"text-green":"text-red";?>">Status</span><br />
            <?php 
                        if(!$PartnerPayment)
                            echo "Waiting to Receive $".get_formated_number($ReceiveFromUser["Amount"])." from ".$SenderResultValue["DisplayName"];
                        else
                            echo "Received $".get_formated_number($ReceiveFromUser["Amount"])." from ".$SenderResultValue["DisplayName"]." - (CONF# ".$PartnerPayment->ReferenceNo.")";
            ?>
        </div>
    </td>
</tr>
<?php
                    }
                }
            }
        }
        $contents = ob_get_contents();
        ob_clean();

        $Results[$UserResultKey] = $contents;//str_replace(array("\r", "\n"), "", $contents);
    }

    return $Results;
}

function get_manual_sale_split_partner_item($split_partner){
	$LoggedInUser = LoggedInUser();
    if(is_numeric($split_partner))
        $split_partner = DB()->get_row("select * from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and id=".$split_partner);

    $partner = DB()->get_row("select * from ".DB()->split_partners." where userid=".$LoggedInUser->id." and id=".$split_partner->SplitPartnerID);
	ob_start();
?>
<li class="">
    <a href="javascript:" class="btn-sm btn-link remove-manual-sale-split-partner" data-splitpartner-id="<?php echo $split_partner->id;?>"><i class="fa fa-trash"></i></a>
    <div class="image partner-image">
        <img src="<?php site_url(!empty($partner->PartnerPicture)?$partner->PartnerPicture:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
    </div>
    <div class="partener-name"><?php echo $partner->PartnerName; ?></div>
</li>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function get_manual_sale_result_values($ManualSale){
    $LoggedInUser = LoggedInUser();

    $UserResultValues = array();
    $ToBeSendPayments = array();
    $ToBeReceivedPayments = array();

    if(is_numeric($ManualSale))
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where userid=".$LoggedInUser->id." and id=".$ManualSale);

    $PercentagesValid = is_percentage_valid($ManualSale);

    if($PercentagesValid){
        $TotalIncome = DB()->get_var("select sum(".DB()->manualsale_incomes.".IncomeAmount) from ".DB()->manualsale_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
        $TotalOtherIncome = DB()->get_var("select sum(".DB()->manualsale_other_incomes.".OtherIncomeAmount) from ".DB()->manualsale_other_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_other_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
        $TotalExpense = DB()->get_var("select sum(".DB()->manualsale_expenses.".ExpenseAmount) from ".DB()->manualsale_expenses." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_expenses.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
    }else{
        $TotalIncome = 0;
        $TotalOtherIncome = 0;
        $TotalExpense = 0;
    }

    $SplitPartners = DB()->get_results("select * from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and ManualSaleID=".$ManualSale->id);

    foreach ($SplitPartners as $SplitPartner){
        if($PercentagesValid){
            $TotalUserIncome = DB()->get_var("select sum(IncomeAmount) from ".DB()->manualsale_incomes." where userid=".$LoggedInUser->id." and ManualSaleSplitPartnerID=".$SplitPartner->id) * 1;
            $TotalUserOtherIncome = DB()->get_var("select sum(OtherIncomeAmount) from ".DB()->manualsale_other_incomes." where userid=".$LoggedInUser->id." and ManualSaleSplitPartnerID=".$SplitPartner->id) * 1;
            $TotalUserExpense = DB()->get_var("select sum(ExpenseAmount) from ".DB()->manualsale_expenses." where userid=".$LoggedInUser->id." and ManualSaleSplitPartnerID=".$SplitPartner->id) * 1;

            $UserGrossProfit = ($TotalUserIncome + $TotalUserOtherIncome) - $TotalUserExpense;

            $UserIncomeSplit = $SplitPartner->IncomePercentage == 0?0:($TotalIncome)*($SplitPartner->IncomePercentage/100);
            $UserOtherIncomeSplit = $SplitPartner->OtherIncomePercentage == 0?0:($TotalOtherIncome)*($SplitPartner->OtherIncomePercentage/100);
            $UserExpenseSplit = $SplitPartner->ExpensePercentage == 0?0:($TotalExpense)*($SplitPartner->ExpensePercentage/100);

            $UserNetProfit = count($SplitPartners) < 2 ? $UserGrossProfit : ($UserIncomeSplit + $UserOtherIncomeSplit) - $UserExpenseSplit + $TotalUserExpense;
            $UserProfit = $UserIncomeSplit + $UserOtherIncomeSplit - $UserExpenseSplit;

            $UserBalance = $UserNetProfit - ($TotalUserIncome + $TotalUserOtherIncome);
        } else {
            $TotalUserIncome = 0;
            $TotalUserOtherIncome = 0;
            $TotalUserExpense = 0;

            $UserGrossProfit = 0;

            $UserIncomeSplit = 0;
            $UserOtherIncomeSplit = 0;
            $UserExpenseSplit = 0;

            $UserNetProfit = 0;
            $UserProfit = 0;

            $UserBalance = 0;
        }

        $UserResultValues["manualsale_split_partner_".$SplitPartner->id] = array(
            "ManualSaleID"                      => $ManualSale->id,
            "SplitPartnerID"                    => $SplitPartner->id,
            "isUserSale"                        => $SplitPartner->isUserSale,
            "isSplitPartnerSale"                => $ManualSale->isSplitPartnerSale,
            "DisplayName"                       => $SplitPartner->isUserSale == 1?$LoggedInUser->display_name:DB()->get_var("select PartnerName from ".DB()->split_partners." where userid=".$LoggedInUser->id." and id=".$SplitPartner->SplitPartnerID),
            "TotalIncome"                       => round($TotalIncome, 2),
            "TotalOtherIncome"                  => round($TotalOtherIncome, 2),
            "TotalExpense"                      => round($TotalExpense, 2),
            "Income"                            => round($TotalUserIncome, 2),
            "OtherIncome"                       => round($TotalUserOtherIncome, 2),
            "Expense"                           => round($TotalUserExpense, 2),
            "Gross_Profit"                      => round($UserGrossProfit, 2),
            "Income_Split_Percentage"           => round($SplitPartner->IncomePercentage, 2),
            "OtherIncome_Split_Percentage"      => round($SplitPartner->OtherIncomePercentage, 2),
            "Expense_Split_Percentage"          => round($SplitPartner->ExpensePercentage, 2),
            "Income_Split"                      => round($UserIncomeSplit, 2),
            "OtherIncome_Split"                 => round($UserOtherIncomeSplit, 2),
            "Expense_Split"                     => round($UserExpenseSplit, 2),
            "Net_Profit"                        => round($UserNetProfit, 2),
            "UserProfit"                        => round($UserProfit, 2),
            "UserBalance"                       => round($UserBalance, 2),
            "OwedPartner"                       => 0,
            "ReceiveFrom"                       => array(),
            "SendTo"                            => array(),
        );

        if($UserBalance < 0)
            $ToBeSendPayments["manualsale_split_partner_balance_".$SplitPartner->id] = $UserBalance * -1;
        else
            $ToBeReceivedPayments["manualsale_split_partner_balance_".$SplitPartner->id] = $UserBalance;
    }

    asort($ToBeSendPayments);
    arsort($ToBeReceivedPayments);

    foreach ($ToBeReceivedPayments as $ReceiverKey => $ToBeReceivedPayment){
        $Balance = $ToBeReceivedPayment;
        $OwedPartner = 0;
        $ReceiverID = str_replace("manualsale_split_partner_balance_", "", $ReceiverKey);
        if($Balance > 0){
            foreach ($ToBeSendPayments as $SenderKey => $ToBeSendPayment){
                $SenderID = str_replace("manualsale_split_partner_balance_", "", $SenderKey);

                if($Balance >= $ToBeSendPayment)
                    $SendingAmount = $ToBeSendPayment;
                else
                    $SendingAmount = $ToBeSendPayment - $Balance;

                if($SendingAmount > 0){
                    $Balance -= $SendingAmount;
                    $OwedPartner -= $SendingAmount;
                    $ToBeSendPayments["manualsale_split_partner_balance_".$SenderID] -= $SendingAmount;
                    $UserResultValues["manualsale_split_partner_".$ReceiverID]["ReceiveFrom"][] = array("SenderID" => $SenderID, "Amount" => round($SendingAmount, 2));
                    $UserResultValues["manualsale_split_partner_".$SenderID]["SendTo"][] = array("ReceiverID" => $ReceiverID, "Amount" => round($SendingAmount, 2));

                    $UserResultValues["manualsale_split_partner_".$ReceiverID]["OwedPartner"] += $SendingAmount;
                    $UserResultValues["manualsale_split_partner_".$SenderID]["OwedPartner"] += $SendingAmount;
                }

                if ($Balance == 0)
                    break;
            }
        }
    }


    return $UserResultValues;
}

function is_percentage_valid($ManualSale){
    $LoggedInUser = LoggedInUser();

    $PercentageValid = false;

    if(is_numeric($ManualSale))
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where userid=".$LoggedInUser->id." and id=".$ManualSale);

	$TotalIncomePercentage = DB()->get_var("select sum(IncomePercentage) from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id);
    $TotalOtherIncomePercentage = DB()->get_var("select sum(OtherIncomePercentage) from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id);
    $TotalExpensePercentage = DB()->get_var("select sum(ExpensePercentage) from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and ManualSaleID = ".$ManualSale->id);
    if($TotalIncomePercentage == 100 && $TotalOtherIncomePercentage == 100 && $TotalExpensePercentage == 100){
        $PercentageValid = true;
    }

    return $PercentageValid;
}

