<?php
function get_rotatorurl($rotator, $sublink = ""){
    $LoggedInUser = LoggedInUser();
    $username = $LoggedInUser->user_login;
    if(is_numeric($rotator)){
        $sql = "select * from ".DB()->rotators." where userid=".$LoggedInUser->id." and id=".$rotator;
        $rotator = DB()->get_row($sql);
    }
    $rotatorurl = "#";
    if($rotator){
        $domain = DB()->get_row("select * from ".DB()->domains." where id=".$rotator->DomainID);
        if($domain){
            if ($domain->DomainType == "admindomain")
                $rotatorurl = str_replace("://", "://".$username.".", rtrim($domain->DomainUrl, "/")) . "/";
            else
                $rotatorurl = rtrim($domain->DomainUrl, "/") . "/";

            if(!empty($sublink))
                $rotatorurl .= rtrim($sublink, "/")."/";

            $rotatorurl .= rtrim($rotator->VisibleLink, "/");
        }
    }
    return $rotatorurl;
}

function get_rotatorlink_box($RotatorLink, $index, $RotatorLinkType = "all"){
    $LoggedInUser = LoggedInUser();
    $contents = "";
    if(is_numeric($RotatorLink)){
        $sql = "select * from ".DB()->rotator_links." where userid=".$LoggedInUser->id." and id=".$RotatorLink;
        $RotatorLink = DB()->get_row($sql);
    }
    if($RotatorLink){
        $RotatorLinkID = $RotatorLink->id;
        $TotalVisits = DB()->get_var("select count(*) from ".DB()->rotator_clicks." where RotatorLinkID = ".$RotatorLinkID) * 1;
        $TotalRotatorVisits = DB()->get_var("select count(*) from ".DB()->rotator_clicks." where RotatorLinkID  in (select id from ".DB()->rotator_links." where RotatorID = ".$RotatorLink->RotatorID." GROUP BY id)") * 1;
        $Percentage = 0;
        $MaxClicks = 0;
        if ($RotatorLink->MaxClicks > 0 )
            $MaxClicks = $RotatorLink->MaxClicks;
        else if($TotalRotatorVisits > 0)
            $MaxClicks = $TotalRotatorVisits;

        if($MaxClicks > 0)
            $Percentage = round($TotalVisits * 100 / $MaxClicks, 1);
        
        $days = "Evergreen";
        if ($RotatorLink->StartDate != "" && $RotatorLink->EndDate != ""){
        	$dtStartDate = new DateTime(date("r", $RotatorLink->StartDate));
            $dtEndDate = new DateTime(date("r", $RotatorLink->EndDate));
            $days = $dtStartDate->diff($dtEndDate)->format("%a days");
        }
        
        ob_start();
?>
<div class="col-sm-12 rotatorlinkbox" id="RotatorLinkIDs-<?php echo $RotatorLinkID;?>" data-rotatorlink-id="<?php echo $RotatorLinkID;?>">
    <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form">
        <div class="rotatorlinkbox-contents clearfix">
            <div class="col-md-7 no-gutter">
                <div class="col-md-2">
                    <div class="rotatorlinkposition"><?php echo $index;?></div>
                </div>

                <div class="col-md-10">
                    <div class="rotatorlinkurl">
                        <ul class="list-inline rotatorlinkurloptions">
                            <li><a href="#" class="btn-easylinks">Choose EasyLink</a></li>
                            <li><span class=""><span>|</span></span></li>
                            <li><a href="#" class="btn-customurl">Custom URL</a></li>
                        </ul>

                        <div class="row easylinkurl form-group has-feedback" style="<?php if($RotatorLink->RotatorLinkType != "linkbank") echo " display: none; "?>">
                            <div class="col-md-8">
                                <select name="LinkBankID" id="LinkBankID-<?php echo $RotatorLinkID;?>" class="form-control select2" style="width: 100%">
                                    <?php
                            $userid = $LoggedInUser->id;
                            $sql = "select * from ".DB()->linkbanks." where (LinkStatus = 'active' or LinkStatus = 'evergreen' or LinkStatus = 'mylink') and userid=".$userid;
                            $linkbanks = DB()->get_results($sql);
                            foreach($linkbanks as $linkbank){
                            ?>
                                    <option value="<?php echo $linkbank->id?>" <?php if($RotatorLink->RotatorLinkType == "linkbank" && $RotatorLink->LinkBankID == $linkbank->id) echo ' selected="selected" ';?>><?php echo $linkbank->LinkName?></option>
                                    <?php
                            }
                            ?>
                                </select>
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>

                        <div class="row customurl form-group has-feedback" style="<?php if($RotatorLink->RotatorLinkType != "customurl") echo " display: none; "?>">
                            <div class="col-md-8">
                                <input type="text" value="<?php if($RotatorLink->RotatorLinkType == "customurl") echo $RotatorLink->RotatorLinkURL; ?>" class="form-control " id="RotatorLinkURL-<?php echo $RotatorLinkID;?>" name="RotatorLinkURL" placeholder="Custom URL" required="required" <?php echo URL_PATTERN;?> />
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" class="btn btn-link btn-advance-settings"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Advanced Settings</a>

                <div class="col-md-10 col-md-offset-2 advance-settings">
                    <div class="row maxclicks form-group has-feedback">
                        <div class="col-md-8">
                            <input type="text" value="<?php echo $RotatorLink->MaxClicks; ?>" class="form-control " id="MaxClicks-<?php echo $RotatorLinkID;?>" name="MaxClicks" placeholder="Max Clicks" required="required" <?php echo INT_PATTERN;?> />
                            <small class="help-block with-errors"></small>
                        </div>
                    </div>


                    <div class="row startdate form-group has-feedback">
                        <div class="col-md-8">
                            <div class="input-group left-addon">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" data-linked="#StartDate-<?php echo $RotatorLinkID;?>" value="<?php if(!empty($RotatorLink->StartDate)) echo date("m/d/Y h:i A", $RotatorLink->StartDate); ?>" class="form-control  input-singledate-time with-left-addon" id="StartDateInput-<?php echo $RotatorLinkID;?>" name="StartDateInput" />
                                <input type="hidden" value="<?php if(!empty($RotatorLink->StartDate)) echo date("m/d/Y h:i A", $RotatorLink->StartDate); ?>" class="" id="StartDate-<?php echo $RotatorLinkID;?>" name="StartDate" />
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row enddate form-group has-feedback">
                        <div class="col-md-8">
                            <div class="input-group left-addon">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" data-linked="#EndDate-<?php echo $RotatorLinkID;?>" value="<?php if(!empty($RotatorLink->EndDate)) echo date("m/d/Y h:i A", $RotatorLink->EndDate); ?>" class="form-control  input-singledate-time with-left-addon" id="EndDateInput-<?php echo $RotatorLinkID;?>e" name="EndDateInput" />
                                <input type="hidden" value="<?php if(!empty($RotatorLink->EndDate)) echo date("m/d/Y h:i A", $RotatorLink->EndDate); ?>" class="" id="EndDate-<?php echo $RotatorLinkID;?>" name="EndDate" />
                                <small class="help-block with-errors"></small>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4 col-md-offset-1 clearfix">
                <div class="rotatorlinkinfo">
                    <div class="clearfix">
                        <ul class="list-inline rotatorlinkdetails">
                            <li>
                                <span><?php echo $days;?></span>
                            </li>
                            <li>
                                <a class="btn btn-<?php echo $RotatorLink->RotatorLinkLive == 1?"success":"danger"; ?> btn-sm btn-rotatorlink-status"><?php echo $RotatorLink->RotatorLinkLive == 1?"Active":"Paused"; ?></a>
                            </li>
                        </ul>
                    </div>
                    <div class="rotatorlinkprogress clearfix">
                        <div class="rotatorlinkprogressdetails"><?php echo $TotalVisits;?>&nbsp;/&nbsp;<?php echo $RotatorLink->MaxClicks == 0? "unlimited":$RotatorLink->MaxClicks;?></div>
                        <div class="progress progress-sm- active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $Percentage;?>" aria-valuemin="0" aria-valuemax="<?php echo $MaxClicks;?>" style="width: <?php echo $Percentage;?>%">
                                <span class="sr-only"><?php echo $TotalVisits;?>&nbsp;/&nbsp;<?php echo $RotatorLink->MaxClicks == 0? "unlimited":$RotatorLink->MaxClicks;?></span>
                            </div>
                        </div>
                        <div class="rotatorlinkprogresspercentage"><?php echo $Percentage;?>&nbsp;%</div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix">
                <div class="rotatorlink-actions ">
                    <a href="#" class="btn btn-success btn-xs- btn-save-rotatorlink">Save Link</a>
                    <a href="#" class="btn btn-danger btn-xs- btn-remove-rotatorlink">Remove Link</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="rotatorlinkposition" value="<?php echo $index;?>" />
        <input type="hidden" name="RotatorLinkType" class="RotatorLinkType" id="RotatorLinkType-<?php echo $RotatorLinkID;?>" value="<?php echo $RotatorLink->RotatorLinkType;?>" />
        <input type="hidden" name="RotatorLinkID" class="RotatorLinkID" id="RotatorLinkID-<?php echo $RotatorLinkID;?>" value="<?php echo $RotatorLinkID;?>" />
    </form>
</div>

<?php
        $contents = ob_get_contents();
        ob_end_clean();
    }
    return $contents;
}


//function get_rotatorurl($rotator){
//    $LoggedInUser = LoggedInUser();
//    if(is_numeric($rotator)){
//        $sql = "select * from ".DB()->rotators." where userid=".$LoggedInUser->id." and id=".$rotator;
//        $rotator = DB()->get_row($sql);
//    }
//    $rotatorurl = "#";
//    if($rotator){
//        $domain = DB()->get_row("select * from ".DB()->domains." where id=".$rotator->DomainID);
//        if($domain){
//            if ($domain->DomainType == "admindomain" || $domain->DomainForward == 1)
//                $rotatorurl = rtrim($domain->DomainUrl, "/") . "/" . $LoggedInUser->id . "/";
//            else
//                $rotatorurl = rtrim($domain->DomainUrl, "/") . "/";
//            $rotatorurl .= rtrim($rotator->VisibleLink, "/");
//        }
//    }
//    return $rotatorurl;
//}

function get_rotators_stats($rotatorid, $startdate = "", $enddate = ""){
    $LoggedInUser = LoggedInUser();

    $stats = array(
        "total" => 0,
        "unique" => 0,
        "nonunique" => 0,
        "bots" => 0,
        "dailyaverage" => 0,
    );

    $sql = "select * from ".DB()->rotators." where id=".$rotatorid." and userid=".$LoggedInUser->id;
    $rotator = DB()->get_row($sql);
    if($rotator){

        $datesql = !empty($startdate) && !empty($enddate)?" and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($startdate)."' and ".DB()->rotator_clicks.".DateAdded <= '".strtotime($enddate)."') ":"";
        $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
        $LinkQuery = isset($_GET["RotatorLinkID"])?" and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ":"";

        $sql = "select count(*) from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql;
        $TotalRotatorVisits = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select 1 AS count from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ClickIp) as RotatorVisitsCount";
        $UniqueRotatorVisits = DB()->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select count(ClickIp) AS count from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName = '' or ".DB()->rotator_clicks.".BotName is NULL) GROUP BY ClickIp) as RotatorVisitsCount";
        $NonUniqueRotatorVisits = DB()->get_var($sql) * 1;
        //$NonUniqueRotatorVisits -= $UniqueRotatorVisits;

        $sql = "select count(ClickIp) from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql." and (".DB()->rotator_clicks.".BotName <> '' and ".DB()->rotator_clicks.".BotName is not NULL)";
        $BotsRotatorVisits = DB()->get_var($sql) * 1;
        
        //$sql = "select count(*) from ".$table." inner join ".DB()->countries." on ".DB()->rotator_clicks.".CountryCode = ".DB()->countries.".countrycode where countrytier = 1 and (BotName = '' or BotName is NULL) and RotatorID=".$rotator->id.$datesql;
        //$TotalTopRotatorVisits = DB()->get_var($sql) * 1;

        //$TopRotatorVisitsPercentage = $TotalRotatorVisits > 0?round(($TotalTopRotatorVisits * 100) / $TotalRotatorVisits):0;

        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".DB()->rotator_clicks.".DateAdded from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql." ORDER BY ".DB()->rotator_clicks.".DateAdded ASC limit 0,1";
            $date1 = DB()->get_var($sql);

            $sql = "select ".DB()->rotator_clicks.".DateAdded from ".$table." where RotatorID=".$rotatorid.$LinkQuery.$datesql." ORDER BY ".DB()->rotator_clicks.".DateAdded DESC limit 0,1";
            $date2 = DB()->get_var($sql);
            
            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $DailyAverage = $TotalDays > 0?round($TotalRotatorVisits / $TotalDays, 2):0;

        $stats = array(
            "total"         => $TotalRotatorVisits,
            "unique"        => $UniqueRotatorVisits,
            "non-unique"     => $NonUniqueRotatorVisits,
            "bots"          => $BotsRotatorVisits,
            "dailyaverage"  => $DailyAverage,
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $stats;
}

function delete_rotator_stats($RotatorID){
    $LoggedInUser = LoggedInUser();
    if(DB()->get_row("select * from ".DB()->rotators." where id=".$RotatorID." and userid=".$LoggedInUser->id)){
        $RotatorLinks = DB()->get_results("SELECT * from ".DB()->rotator_links." where RotatorID=".$RotatorID);
        foreach ($RotatorLinks as $RotatorLink){
        	DB()->delete(DB()->rotator_clicks, array("RotatorLinkID" => $RotatorLink->id));
        }
        //DB()->delete(DB()->rotator_links, array("RotatorID" => $RotatorID));
    }
}

