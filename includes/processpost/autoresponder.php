<?php
if($post_act == "save_autoresponder" || $post_act == "delete_autoresponder"){
    $parentid = isset($_POST["parentid"])?$_POST["parentid"]:0;
    $AutoresponderID = 0;
    if($post_act == "save_autoresponder"){
        $data = array(
            "userid"  => $LoggedInUser->id,
            "AutoresponderName"  => $_POST['AutoresponderName'],
            "AutoresponderType"  => "Email",
            "DateAdded"  => strtotime("now"),
            "parentid"  => $parentid,
        );

        if(isset($_GET['AutoresponderID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->autoresponders, $data, array("id" => parseInt($_GET['AutoresponderID'])));
            $AutoresponderID = parseInt($_GET['AutoresponderID']);
        }else{
            DB()->insert(DB()->autoresponders, $data);
            $AutoresponderID = DB()->insert_id;
        }
    }

    if($post_act == "delete_autoresponder"){
        DB()->delete(DB()->autoresponders, array("id" => parseInt($_POST['AutoresponderID'])));
    }

    $sql = "select * from ".DB()->autoresponders." where AutoresponderType='Email' and userid = ".$LoggedInUser->id." and parentid=".$parentid." order by AutoresponderName";
    $autoresponders = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Autoresponder</option>';
    foreach($autoresponders as $autoresponder)
        $jsonData["value"] .= '<option value="'.$autoresponder->id.'" '.($autoresponder->id == $AutoresponderID?' selected="selected" ':'').' >'.$autoresponder->AutoresponderName.'</option>';
    echo json_encode($jsonData);

    die;
}