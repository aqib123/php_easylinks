<?php
if($post_act == "save_redirect_link" || $post_act == "delete_redirect_link"){
    $RedirectLinkID = 0;
    if($post_act == "save_redirect_link"){
        $data = array(
            "userid"  => $LoggedInUser->id,
            "RedirectLinkName"  => $_POST['RedirectLinkName'],
            "RedirectLinkURL"   => $_POST['RedirectLinkURL'],
            "RedirectLinkType"  => $_GET['RedirectLinkType'],
            "DateAdded"         => strtotime("now")
        );
        
        if(isset($_GET['RedirectLinkID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->redirect_links, $data, array("id" => parseInt($_GET['RedirectLinkID'])));
            $RedirectLinkID = parseInt($_GET['RedirectLinkID']);
        }else{
            DB()->insert(DB()->redirect_links, $data);
            $RedirectLinkID = DB()->insert_id;
        }
    }
    
    if($post_act == "delete_redirect_link"){
        DB()->delete(DB()->redirect_links, array("id" => parseInt($_POST['RedirectLinkID'])));
    }

    $sql = "select * from ".DB()->redirect_links." where RedirectLinkType='".$_GET['RedirectLinkType']."' and userid = ".$LoggedInUser->id." order by RedirectLinkName";
    $redirect_links = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Holding Page</option>';
    foreach($redirect_links as $redirect_link)
        $jsonData["value"] .= '<option value="'.$redirect_link->id.'" '.($redirect_link->id==$RedirectLinkID?' selected="selected" ':'').' >'.$redirect_link->RedirectLinkName.'</option>';
    echo json_encode($jsonData);
    
    die;
}