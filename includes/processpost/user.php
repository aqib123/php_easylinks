<?php
if($post_act == "login_user"){
    $jsonData = elUsers()->login($_POST);
    echo json_encode($jsonData);
    die;
}else if($post_act == "register_user"){
    $registered = false;
    $jsonData = array("registered"=>false, "message"=>"");
    if(!empty($_POST['username']) && !empty($_POST['email'])){
        $username = $_POST["username"];
        $email = $_POST["email"];
        $sql = "select * from ".DB()->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($email)."' ";
        $user = DB()->get_row($sql);
        if(!$user){
            $password = md5($_POST["password"]);
            $email = $_POST["email"];
            $displayname = $_POST["displayname"];
            $user_activation_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));
            $data = array(
                "user_login"            =>      $username,
                "user_pass"             =>      $password,
                "user_pass_backup"      =>      SYS()->EncryptData($password),
                "user_email"            =>      $email,
                "user_registered"       =>      date("Y-m-d"),
                "user_status"           =>      "0",
                "display_name"          =>      $displayname,
                "user_activation_key"   =>      $user_activation_key
            );
            DB()->show_errors();
            DB()->insert(DB()->users, $data);
            if(DB()->insert_id > 0){
                $UserID = DB()->insert_id;
                $jsonData["registered"] = true;
                $LevelID = DB()->get_var("select id from ".DB()->levels." where LevelName='Basic'");
                $ELLevelID = !isset($_GET['ELLevelID']) || !is_numeric($_GET['ELLevelID'])?$LevelID:intval($_GET['ELLevelID']);
                elUsers()->update_api_key(DBUsers()->get_row($UserID));
                DB()->insert(DB()->user_levels, array(
                    "UserID" => $UserID,
                    "LevelID" => $ELLevelID,
                    "UserLevelEnabled" => 1
                ));
                $to = $email;
                $subject = get_option("New User Email Subject");//, 'EasyLinks Access'
                $message = get_option("New User Email Body");//, 'EasyLinks Access'
                $message = preg_replace(array('/{username}/', '/{password}/'), array($username, $_POST["password"]), $message);
                if(SYS()->send_message($to, $subject, $message))
                    $jsonData["message"] = "Please check your email to verify your account.";
            }
        }else{
            $jsonData["registered"] = false;
            $jsonData["message"] = "This Username and/or Email is already registered.. Please try again";
        }
    }
    echo json_encode($jsonData);
    die;
} else if($post_act == "change_password"){
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!empty($_POST['CurrentPassword']) && $LoggedInUser->user_pass == md5($_POST["CurrentPassword"]) && !empty($_POST["NewPassword"]) && $_POST["NewPassword"] == $_POST["ConfirmPassword"]){
        DB()->update(
            DB()->users, [
                "user_pass"             => md5($_POST["NewPassword"]),
                "user_pass_backup"      => SYS()->EncryptData($_POST["NewPassword"])
            ], [
                "id" => $LoggedInUser->id
            ]
        );
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.saasonboard.com/api/user-management/change_company_user_password',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('email' => $_POST["Email"],'password' => $_POST["NewPassword"]),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $jsonData["jscode"] = "window.location.href='".get_site_url("logout")."';";
    }
    echo json_encode($jsonData);
    die;
}else if($post_act == "update_profile"){
    $data = array(
        "user_nicename"     => $_POST["user_nicename"],
        "display_name"      => $_POST["display_name"],
        "timezoneid"        => $_POST["timezoneid"],
        "user_picture"      => $_POST["user_picture"]
    );
    if(DB()->update(DB()->users, $data, array("id" => $LoggedInUser->id))){
        $LoggedInUser->user_nicename = $_POST["user_nicename"];
        $LoggedInUser->display_name = $_POST["display_name"];
        $LoggedInUser->timezoneid = $_POST["timezoneid"];
        $LoggedInUser->user_picture = $_POST["user_picture"];
    }
    site_redirect("user-profile");
    die;
}else if($post_act == "forget_password"){
    $jsonData = array("mailsent"=>false, "message"=>"");
    if(!empty($_POST['username'])){
        $username = $_POST["username"];
        $sql = "select * from ".DB()->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($username)."' ";
        $user = DB()->get_row($sql);
        if($user){
            $jsonData["mailsent"] = true;
            $user_password_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));
            DB()->update(DB()->users, array("user_password_key" => $user_password_key), array("id" => $user->id));
            $to = $user->user_email;
            $subject = get_option("Forgot Password Email Subject");//, 'EasyLinks Access'
            $message = get_option("Forgot Password Email Body");//, 'EasyLinks Access'
            $message = preg_replace(array('/{display_name}/', '/{user_login}/', '/{forget-password-url}/'), array($user->display_name, $user->user_login, get_site_url("login/forget-password/?key=".$user_password_key)), $message);
            if(SYS()->send_message($to, $subject, $message))
                $jsonData["message"] = "Please check your email to reset your password.";
        }else{
            $jsonData["mailsent"] = false;
            $jsonData["message"] = "This Username not found.. Please try again";
        }
    }
    echo json_encode($jsonData);
    die;
}else if($post_act == "reset_password"){
    $jsonData = array("reset"=>false, "message"=>"");
    if(!empty($_POST['key'])){
        $user_password_key = $_POST["key"];
        $sql = "select * from ".DB()->users." where md5(user_password_key)='".md5($user_password_key)."'";
        $user = DB()->get_row($sql);
        if($user){
            $newpassword = $_POST["newpassword"];
            $confirmpassword = $_POST["confirmpassword"];
            if($newpassword == $confirmpassword && DB()->update(DB()->users, array("user_pass" => md5($newpassword), "user_pass_backup" => SYS()->EncryptData($newpassword), "user_password_key" => ""), array("user_password_key" => $user_password_key))){
                $jsonData["reset"] = true;
                $jsonData["message"] = "Password reset successfully... You can now login..";
            }
        }else{
            $jsonData["reset"] = false;
            $jsonData["message"] = "This Key not found.. Please try again";
        }
    }
    echo json_encode($jsonData);
    die;
} else if($post_act == "update_forced_changes"){
    if($LoggedInUser->force_username_change == 1 || $LoggedInUser->force_email_change == 1 || $LoggedInUser->force_password_change == 1){
        $data = array();
        if($LoggedInUser->force_username_change == 1 && SYS()->is_string($_POST["username"])){
            $data["force_username_change"] = 0;
            $data["user_login"] = $_POST["username"];
        }
        if($LoggedInUser->force_email_change == 1 && SYS()->is_string($_POST["email"])){
            $data["force_email_change"] = 0;
            $data["user_email"] = $_POST["email"];
        }
        if($LoggedInUser->force_password_change == 1 && SYS()->is_string($_POST["password"])){
            $PlainUserPassword = $_POST["password"];
            $data["force_password_change"] = 0;
            $data["user_pass"] = md5($PlainUserPassword);
            $data["user_pass_backup"] = SYS()->EncryptData($PlainUserPassword);;
        }
        if(DB()->update(
            DB()->users,
            $data,
            array(
                "id" => $LoggedInUser->id
            )
        )){
            SYS()->site_redirect("logout");
            die;
        }
    }
}