<?php
if($post_act == "save_domain" || $post_act == "delete_domain"){
    //$jsonData = array("result" => "failed", "error" => "testing", "before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    $DomainType = $_GET["DomainType"];
    $DomainID = 0;

    if($post_act == "save_domain"){
        $data = array(
            "userid"            => $LoggedInUser->id,
            "DomainName"        => $_POST['DomainName'],
            "DomainUrl"         => $_POST['DomainUrl'],
            "DefaultURL"        => $_POST['DefaultURL'],
            "RetargetingPixel"  => $_POST['RetargetingPixel'],
            "DomainSlug"        => $_POST['DomainSlug'],
            "DefaultCloakURL"   => isset($_POST['DefaultCloakURL']),
            "DomainForward"     => isset($_POST['DomainForward']),
            "DomainType"        => $DomainType,//"userdomain",
            "DateAdded"         => strtotime("now")
        );

        if(isset($_GET['DomainID'])){
            unset($data['DateAdded']);
            $sql = "select * from ".DB()->domains." where id=".parseInt($_GET['DomainID']);
            $current_domain = DB()->get_row($sql);
            $update_domain = true;
            if($current_domain->DomainSlug != $_POST['DomainSlug']){
                $sql = "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and DomainSlug = '".$_POST['DomainSlug']."'";
                $domain = DB()->get_row($sql);
                if($domain)
                    $update_domain = false;
            }

            if ($update_domain){
                DB()->update(DB()->domains, $data, array("id" => parseInt($_GET['DomainID'])));
                $DomainID = parseInt($_GET['DomainID']);
            }
        }else{
            if($DomainType != 'customdomain')
                $sql = "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and DomainSlug = '".$_POST['DomainSlug']."'";
            else
                $sql = "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and DomainName = '".$_POST['DomainName']."' and DomainUrl = '".$_POST['DomainUrl']."'";
            $domain = DB()->get_row($sql);
            if(!$domain){
                DB()->insert(DB()->domains, $data);
                $DomainID = DB()->insert_id;
            }
        }
    }

    if($post_act == "delete_domain"){
        DB()->delete(DB()->domains, array("id" => parseInt($_POST['DomainID'])));
    }

    $sql = "select * from ".DB()->domains." where userid = ".$LoggedInUser->id." and DomainType='".stripslashes($DomainType)."'  order by DomainName";
    $domains = DB()->get_results($sql);
    $domainurls = array();

    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Domain</option>';
    foreach($domains as $domain){
        $domainurls[] = '"'.$domain->id.'": {name:"'.$domain->DomainName.'", forward:"'.$domain->DomainForward.'", url:"'.$domain->DomainUrl.'", type:"'.$domain->DomainType.'"}';
        $jsonData["value"] .= '<option value="'.$domain->id.'" '.($domain->id == $DomainID?' selected="selected" ':'').' >'.$domain->DomainName.'</option>';
    }
    $jsonData["jscode"] = 'domains = {'.implode(',', $domainurls).'};';
    echo json_encode($jsonData);

    die;
}