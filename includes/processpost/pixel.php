<?php
if($post_act == "save_pixel" || $post_act == "delete_pixel"){
    $PixelID = 0;
    if($post_act == "save_pixel"){
        $data = array(
            "userid"  => $LoggedInUser->id,
            "PixelName"  => $_POST['PixelName'],
            "PixelCode"  => $_POST['PixelCode'],
            "PixelType"  => "",//$_GET['PixelType'],
            "DateAdded"  => strtotime("now")
        );
        
        if(isset($_GET['PixelID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->pixels, $data, array("id" => parseInt($_GET['PixelID'])));
            $PixelID = parseInt($_GET['PixelID']);
        }else{
            DB()->insert(DB()->pixels, $data);
            $PixelID = DB()->insert_id;
        }
    }
    
    if($post_act == "delete_pixel"){
        DB()->delete(DB()->pixels, array("id" => parseInt($_POST['PixelID'])));
    }

    $sql = "select * from ".DB()->pixels." where userid = ".$LoggedInUser->id." order by PixelName";
    $pixels = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Pixel</option>';
    foreach($pixels as $pixel)
        $jsonData["value"] .= '<option value="'.$pixel->id.'" '.($pixel->id==$PixelID?' selected="selected" ':'').' >'.$pixel->PixelName.'</option>';
    echo json_encode($jsonData);
    
    die;
}