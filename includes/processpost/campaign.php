<?php
if($post_act == "save_campaign" || $post_act == "delete_campaign"){
    if($post_act == "save_campaign"){
        $data = array(
            "userid"            => $LoggedInUser->id,
            "CampaignName"      => $_POST['CampaignName'],
            "GroupID"           => $_POST['GroupID'],
            "AdditionalNotes"   => $_POST['AdditionalNotes'],
            "MasterCampaignID"  => $_POST['MasterCampaignID'],
            "DateAdded"         => strtotime("now")
        );

        if(!empty($_GET['CampaignID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->campaigns, $data, array("id" => parseInt($_GET['CampaignID'])));
            site_redirect("campaign/emails/".$_GET['CampaignID']."/");
        }else{
            $data['hasSingleEmail'] = isset($_GET["SingleEmail"])?1:0;
            $data['CampaignStatus'] = isset($_POST["CampaignStatus"])?$_POST["CampaignStatus"]:"pending";
            DB()->insert(DB()->campaigns, $data);
            if(is_numeric(DB()->insert_id))
                site_redirect("campaign/emails/".DB()->insert_id."/");
        }
    }

    if($post_act == "delete_campaign"){
        DB()->delete(DB()->campaigns, array("id" => parseInt($_POST['CampaignID'])));
    }
    die;
} else if($post_act == "save_email" || $post_act == "update_email_copied" || $post_act == "delete_email"){
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    $CampaignID = intval($_GET['CampaignID']);

    if($post_act == "save_email" || $post_act == "update_email_copied"){
        if($post_act == "save_email"){
            $data = array(
                "userid"            => $LoggedInUser->id,
                "CampaignID"        => $_GET['CampaignID'],
                "AutoresponderID"   => $_POST['AutoresponderID'],
                "EmailSubject"      => $_POST['EmailSubject'],
                "EmailBody"         => $_POST['EmailBody'],
                "SendTime"          => $_POST['SendTime'],
                "LinkBankID"        => $_POST['LinkBankID'],
                "SetWidth"          => isset($_POST['SetWidth']),
                "EmailTypeID"       => $_POST['EmailTypeID'],
                "DateAdded"         => strtotime("now"),
                "isEnabled"         => 1
            );

            if(isset($_GET['EmailID'])){
                $EmailID = parseInt($_GET['EmailID']);
                unset($data['DateAdded']);
                DB()->update(DB()->campaign_emails, $data, array("id" => $EmailID));
            }else{
                DB()->insert(DB()->campaign_emails, $data);
                $EmailID = DB()->insert_id;
            }
        } else if($post_act == "update_email_copied"){
            $data = array(
                "userid"            => $LoggedInUser->id,
                "isCopied"          => $_POST['isCopied'],
            );
            $EmailID = parseInt($_POST['EmailID']);
            DB()->update(DB()->campaign_emails, $data, array("id" => $EmailID));
        }

        if(is_numeric($EmailID)){

            $sql = "select * from ".DB()->campaign_emails." where userid=".$LoggedInUser->id." and id=".$EmailID;
            $email = DB()->get_row($sql);
            $jsonData["value"] = get_email_box($CampaignID, $email, $_GET["index"]);

            $jsonData["jscode"] = '
                    $related.find(".copy-email-data").each(function () {
                        var $this = $(this);
                        CopyEmailData($this);
                    });
                    $related.find(\'input[type="checkbox"].blue, input[type="radio"].blue\').each(createblueicheck);
                    $related.find(\'.email-copied\').on(\'ifChanged\', EmailCopied);
                    ';
        }
    }

    if($post_act == "delete_email"){
        DB()->delete(DB()->campaign_emails, array("id" => parseInt($_POST['EmailID'])));
        $jsonData["value"] = get_email_box($CampaignID, null, $_GET["index"]);
    }

    echo json_encode($jsonData);
    die;
}