<?php
if($post_act == "save_group" || $post_act == "delete_group"){
    $GroupID = 0;
    if($post_act == "save_group"){
        $data = array(
            "userid"  => $LoggedInUser->id,
            "GroupName"  => $_POST['GroupName'],
            "GroupType"  => $_GET['GroupType'],
            "DateAdded"  => strtotime("now")
        );

        if(isset($_GET['GroupID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->groups, $data, array("id" => parseInt($_GET['GroupID'])));
            $GroupID = parseInt($_GET['GroupID']);
        }else{
            DB()->insert(DB()->groups, $data);
            $GroupID = DB()->insert_id;
        }
    }

    if($post_act == "delete_group"){
        DB()->delete(DB()->groups, array("id" => parseInt($_POST['GroupID'])));
    }

    $sql = "select * from ".DB()->groups." where GroupType='".$_GET['GroupType']."' and userid = ".$LoggedInUser->id." order by GroupName";
    $groups = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Group</option>';
    foreach($groups as $group)
        $jsonData["value"] .= '<option value="'.$group->id.'" '.($group->id == $GroupID?' selected="selected" ':'').' >'.$group->GroupName.'</option>';
    echo json_encode($jsonData);

    die;
}