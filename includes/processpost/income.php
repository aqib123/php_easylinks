<?php
if($post_act == "add_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])){
        $MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
        $LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$LoggedInUser->id);

        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $IncomeName = $_POST["IncomeName"];
        $IncomeNote = $_POST["IncomeNote"];
        $IncomeAmount = $_POST["IncomeAmount"];

        $data = array(
            "ManualSaleSplitPartnerID"          => $ManualSaleSplitPartnerID,
            "IncomeName"                        => $IncomeName,
            "IncomeNote"                        => $IncomeNote,
            "IncomeAmount"                      => $IncomeAmount,
            "DateAdded"                         => strtotime("now"),
            "userid"                            => $LoggedInUser->id
        );
        if(DB()->insert(DB()->manualsale_incomes, $data) && is_numeric(DB()->insert_id)){
            $IncomeID = DB()->insert_id;
            $jsonData["row"] = get_income_row($IncomeID);
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
            $jsonData["result"] = "successfull";
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "update_income"){
    $jsonData = array("result" => "", "row" => "", "jscode" => "");

    $type = ucwords($_GET["type"]);
    $table = $type == "Income"?DB()->manualsale_incomes:($type == "OtherIncome"?DB()->manualsale_other_incomes:DB()->manualsale_expenses);
    $id = $_GET["id"];
    $ManualSaleSplitPartnerID = $_GET["ManualSaleSplitPartnerID"];
    $ManualSale = DB()->get_var("select ManualSaleID from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and id=".$ManualSaleSplitPartnerID);

    $NameField = $type.Name;
    $AmountField = $type.Amount;
    $NoteField = $type.Note;

    $data = array(
        $NameField => $_POST[$NameField],
        $AmountField => $_POST[$AmountField],
        $NoteField => $_POST[$NoteField]
    );

    DB()->update($table, $data, array("id" => $id, "userid" => $LoggedInUser->id));
    $jsonData["result"] = "successfull";
    $jsonData["row"] = $type == "Income"?get_income_row($id):get_expense_row($id);
    $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
    $jsonData["jscode"] = 'UpdateRow($ModelCaller, jsonData.row, jsonData.saleresult)';

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"]) && is_numeric($_POST["IncomeID"])){
        $IncomeID = $_POST["IncomeID"];
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ManualSale = DB()->get_var("select ManualSaleID from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and id=".$ManualSaleSplitPartnerID);

        if(DB()->delete(DB()->manualsale_incomes, array("id" => $IncomeID, "userid" => $LoggedInUser->id))){
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "add_other_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])){
        $MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
        $LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$LoggedInUser->id);

        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $OtherIncomeName = $_POST["OtherIncomeName"];
        $OtherIncomeNote = $_POST["OtherIncomeNote"];
        $OtherIncomeAmount = $_POST["OtherIncomeAmount"];

        $data = array(
            "ManualSaleSplitPartnerID"          => $ManualSaleSplitPartnerID,
            "OtherIncomeName"                        => $OtherIncomeName,
            "OtherIncomeNote"                        => $OtherIncomeNote,
            "OtherIncomeAmount"                      => $OtherIncomeAmount,
            "DateAdded"                         => strtotime("now"),
            "userid"                            => $LoggedInUser->id
        );
        if(DB()->insert(DB()->manualsale_other_incomes, $data) && is_numeric(DB()->insert_id)){
            $OtherIncomeID = DB()->insert_id;
            $jsonData["row"] = get_other_income_row($OtherIncomeID);
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
            $jsonData["result"] = "successfull";
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_other_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"]) && is_numeric($_POST["OtherIncomeID"])){
        $OtherIncomeID = $_POST["OtherIncomeID"];
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ManualSale = DB()->get_var("select ManualSaleID from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and id=".$ManualSaleSplitPartnerID);

        if(DB()->delete(DB()->manualsale_other_incomes, array("id" => $OtherIncomeID, "userid" => $LoggedInUser->id))){
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
        }
    }

    echo json_encode($jsonData);
    die;
}