<?php
if($post_act == "save_vendor" || $post_act == "delete_vendor"){
    if($post_act == "save_vendor"){
        $VendorID = 0;
        $data = array(
            "userid"            => $LoggedInUser->id,
            "VendorName"        => $_POST['VendorName'],
            "VendorType"        => $_GET['VendorType'],
            "DateAdded"         => strtotime("now"),
            "VendorPicture"     => $_POST['VendorPicture'],
            "WebsiteUrl"        => $_POST['WebsiteUrl'],
            "SkypeName"         => $_POST['SkypeName'],
            "LinkedIn"          => $_POST['LinkedIn'],
            "FacebookID"        => $_POST['FacebookID'],
            "EmailAddress"      => $_POST['EmailAddress'],
            "AdditionalNotes"   => $_POST['AdditionalNotes'],
            "VendorTags"        => $_POST['VendorTags']
        );
        
        if(isset($_GET['VendorID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->vendors, $data, array("id" => parseInt($_GET['VendorID'])));
            $VendorID = parseInt($_GET['VendorID']);
        }else{
            DB()->insert(DB()->vendors, $data);
            $VendorID = DB()->insert_id;
        }
    }
    
    if($post_act == "delete_vendor"){
        DB()->delete(DB()->vendors, array("id" => parseInt($_POST['VendorID'])));
    }

    $sql = "select * from ".DB()->vendors." where VendorType='".$_GET['VendorType']."' and userid = ".$LoggedInUser->id." order by VendorName";
    $vendors = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Vendor</option>';
    foreach($vendors as $vendor)
        $jsonData["value"] .= '<option value="'.$vendor->id.'" '.($vendor->id == $VendorID?' selected="selected" ':'').' >'.$vendor->VendorName.'</option>';
    echo json_encode($jsonData);

    die;
}