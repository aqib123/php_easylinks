<?php
if($post_act == "save_master_campaign" || $post_act == "delete_master_campaign"){
    $MasterCampaignID = 0;
    if($post_act == "save_master_campaign"){
        $data = array(
            "userid"  => $LoggedInUser->id,
            "MasterCampaignName"  => $_POST['MasterCampaignName'],
            "MasterCampaignStatus"  => $_POST['MasterCampaignStatus'],
            "DateAdded"  => strtotime("now")
        );
        
        if(isset($_GET['MasterCampaignID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->master_campaigns, $data, array("id" => parseInt($_GET['MasterCampaignID'])));
            $MasterCampaignID = parseInt($_GET['MasterCampaignID']);
        }else{
            DB()->insert(DB()->master_campaigns, $data);
            $MasterCampaignID = DB()->insert_id;
        }
    }
    
    if($post_act == "delete_master_campaign"){
        DB()->delete(DB()->master_campaigns, array("id" => parseInt($_POST['MasterCampaignID'])));
    }

    $sql = "select * from ".DB()->master_campaigns." where MasterCampaignStatus='active' and userid = ".$LoggedInUser->id." order by MasterCampaignName";
    $master_campaigns = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET["nodata"])){
        if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="0">Select Master Campaign</option>';
        foreach($master_campaigns as $master_campaign)
            $jsonData["value"] .= '<option value="'.$master_campaign->id.'" '.($master_campaign->id == $MasterCampaignID?' selected="selected" ':'').' >'.$master_campaign->MasterCampaignName.'</option>';
    }else{
        $jsonData["jscode"] = "window.location.href='".get_site_url("master-campaign")."'";
    }

    echo json_encode($jsonData);
    
    die;
}