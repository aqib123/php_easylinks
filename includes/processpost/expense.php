<?php
if($post_act == "add_expense"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])){
        $MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
        $LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;
        $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$LoggedInUser->id);

        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ExpenseName = $_POST["ExpenseName"];
        $ExpenseNote = $_POST["ExpenseNote"];
        $ExpenseAmount = $_POST["ExpenseAmount"];

        $data = array(
            "ManualSaleSplitPartnerID"          => $ManualSaleSplitPartnerID,
            "ExpenseName"                        => $ExpenseName,
            "ExpenseNote"                        => $ExpenseNote,
            "ExpenseAmount"                      => $ExpenseAmount,
            "DateAdded"                         => strtotime("now"),
            "userid"                            => $LoggedInUser->id
        );
        if(DB()->insert(DB()->manualsale_expenses, $data) && is_numeric(DB()->insert_id)){
            $ExpenseID = DB()->insert_id;
            $jsonData["row"] = get_expense_row($ExpenseID);
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
            $jsonData["result"] = "successfull";
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_expense"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"]) && is_numeric($_POST["ExpenseID"])){
        $ExpenseID = $_POST["ExpenseID"];
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ManualSale = DB()->get_var("select ManualSaleID from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and id=".$ManualSaleSplitPartnerID);

        if(DB()->delete(DB()->manualsale_expenses, array("id" => $ExpenseID, "userid" => $LoggedInUser->id))){
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
        }

    }

    echo json_encode($jsonData);
    die;
}