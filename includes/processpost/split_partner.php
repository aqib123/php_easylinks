<?php
if($post_act == "save_split_partner" || $post_act == "delete_split_partner"){
    $PartnerID = 0;
    if($post_act == "save_split_partner"){
        $data = array(
            "userid"            => $LoggedInUser->id,
            "PartnerName"        => $_POST['PartnerName'],
            "PartnerType"        => $_GET['PartnerType'],
            "DateAdded"         => strtotime("now"),
            "PartnerPicture"     => $_POST['PartnerPicture'],
            "WebsiteUrl"        => $_POST['WebsiteUrl'],
            "SkypeName"         => $_POST['SkypeName'],
            "LinkedIn"          => $_POST['LinkedIn'],
            "FacebookID"        => $_POST['FacebookID'],
            "EmailAddress"      => $_POST['EmailAddress'],
            "AdditionalNotes"   => $_POST['AdditionalNotes'],
            "PartnerTags"        => $_POST['PartnerTags']
        );
        
        if(isset($_GET['PartnerID'])){
            unset($data['DateAdded']);
            DB()->update(DB()->split_partners, $data, array("id" => parseInt($_GET['PartnerID'])));
            $PartnerID = parseInt($_GET['PartnerID']);
        }else{
            DB()->insert(DB()->split_partners, $data);
            $PartnerID = DB()->insert_id;
        }
    }
    
    if($post_act == "delete_split_partner"){
        DB()->delete(DB()->split_partners, array("id" => parseInt($_POST['PartnerID'])));
    }

    $sql = "select * from ".DB()->split_partners." where PartnerType='".$_GET['PartnerType']."' and userid = ".$LoggedInUser->id." order by PartnerName";
    $split_partners = DB()->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Partner</option>';
    foreach($split_partners as $split_partner)
        $jsonData["value"] .= '<option value="'.$split_partner->id.'" '.($split_partner->id==$PartnerID?' selected="selected" ':'').' >'.$split_partner->PartnerName.'</option>';
    echo json_encode($jsonData);

    die;
}