<?php
include_once "includes/functions.php";

function InsertIpAddress($sql){
    
	$ipaddresses = DB()->get_results($sql);
    if($ipaddresses)
        echo $sql.";\n";
    foreach ($ipaddresses as $ipaddress){
        $IpAddressInfo = get_ipaddress_info($ipaddress->ipaddress);
        echo "before: ".$ipaddress->ipaddress."\n";
        //echo "<pre>".print_r($IpAddressInfo, true)."</pre>";
        if(!empty($IpAddressInfo["geobytesipaddress"])){
            echo "after: ".$ipaddress->ipaddress."\n";
            DB()->insert(DB()->ipaddress_info, $IpAddressInfo);
        }
    }
}

function UpdateIpAddress($sql, $table, $field){
    global $progress;
	$ipaddresses = DB()->get_results($sql);

    foreach ($ipaddresses as $ipaddress){
        $ip = trim($ipaddress->ipaddress);
        $IpAddressInfo = get_ipaddress_info($ip);
        DB()->update($table,
            array("CountryCode" => $IpAddressInfo->country),
            array($field => $ip)
        );
        $progress++;
        echo "\r{$progress} ...";
    }

    echo "\rfinished updating {$progress} records\n";
}
global $progress;
$progress = 0;

//UpdateIpAddress("select id, ClickIp as ipaddress from ".DB()->linkbank_clicks." group by ClickIp", DB()->linkbank_clicks, "ClickIp");
UpdateIpAddress("select id, FireIp as ipaddress from ".DB()->linkbank_pixel_fires." group by FireIp", DB()->linkbank_pixel_fires, "FireIp");
UpdateIpAddress("select id, ConversionIp as ipaddress from ".DB()->linkbank_conversions." group by ConversionIp", DB()->linkbank_conversions, "ConversionIp");

//UpdateIpAddress("select id, ClickIp as ipaddress from ".DB()->paidtraffic_clicks." group by ClickIp", DB()->paidtraffic_clicks, "ClickIp");
//UpdateIpAddress("select id, FireIp as ipaddress from ".DB()->paidtraffic_pixel_fires." group by FireIp", DB()->paidtraffic_pixel_fires, "FireIp");
//UpdateIpAddress("select id, ConversionIp as ipaddress from ".DB()->paidtraffic_conversions." group by ConversionIp", DB()->paidtraffic_conversions, "ConversionIp");

//UpdateIpAddress("select id, ClickIp as ipaddress from ".DB()->rotator_clicks." group by ClickIp", DB()->rotator_clicks, "ClickIp");

//UpdateIpAddress("select id, ClickIp as ipaddress from ".DB()->link_sequence_clicks." group by ClickIp", DB()->link_sequence_clicks, "ClickIp");
