﻿$(document).ready(function () {
    $('.copy-email-data').each(function () {
        var $this = $(this);
        CopyEmailData($this);
    });

    $(".create-new-emailbox").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $email_box_create_new = $this.closest("#email-box-create-new");
        var index = $(".emailbox").length > 0 ? (parseInt($(".emailbox:last").attr("data-box-index")) + 1) : 0;
        var newbox = emptybox.replace(/{index}/ig, index);

        $email_box_create_new.before(newbox);
    });

});

function CopyEmailData($ele) {
    var client = new ClipboardJS($ele[0], {
        text: function (trigger) {
            var EmainElement = emails[$ele.attr("data-object-id")];
            var copytext = "";

            if (typeof EmainElement != "undefined") {
                var copytext = $ele.attr("data-copy-type") == "subject" ? EmainElement.subject : EmainElement.body;
                copytext = $("<div/>").html(copytext).text();
            }

            if (copytext == "") {
                alert($ele.attr("data-copy-type") + " is empty. Nothing to copy.");
            }
            
            return copytext;
        }
    });

    client.on('success', function (e) {
        alert("Copied " + $ele.attr("data-copy-type") + " to clipboard!");
    });
}

$('.email-copied').on('ifChanged', EmailCopied);
function EmailCopied(e) {
    var $this = $(this);
    if ($this.attr("data-emailid")) {
        var emailid = $this.attr("data-emailid");
        var url = $this.attr("data-url");
        var data = "act=update_email_copied&EmailID=" + emailid + "&isCopied=" + ($this.is(":checked") ? "1" : "0");

        $.ajax({
            method: "post",
            url: url,
            data: data
        }).done(function (data) {
            if ($this.attr("data-related")) {
                var $related = $($this.attr("data-related"));
                var jsonData = $.parseJSON(data);
                $related.before(jsonData.before);
                $related.html(jsonData.value);


                $related.find('.email-copied').on('ifChanged', EmailCopied);

                $related.after(jsonData.after);
                eval(jsonData.jscode);
            }
        });
    }
}