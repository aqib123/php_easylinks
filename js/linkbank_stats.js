﻿var isLoading = false;
function LoadMoreLinkbank(PageNo, PerPage) {
    var href = window.location.href;
    var method = "POST";
    var data = {
        "act": "load_more_linkbank",
        "page_no": PageNo,
        "per_page": PerPage
    };

    isLoading = true;
    $(".loadmorewait").show();
    $.ajax({
        method: method,
        url: href,
        data: data
    }).done(function (jsonData) {
        isLoading = false;
        $(".loadmorewait").hide();
        if (jsonData.success === true) {
            var LinkbanksList = DataTables["LinkbanksList"];
            var $LinkbanksList = $("#LinkbanksList");
            var $Rows = $(jsonData.LinkbankRows);
            var OrderBy = $LinkbanksList.data("default-sort-column");
            var OrderDirection = $LinkbanksList.data("default-sort-type");

            HasMoreLinks = jsonData.HasMoreLinks;
            NextPageNo = jsonData.NextPageNo;
            PerPage = jsonData.PerPage;

            LinkbanksList.rows.add($Rows).draw();//.order([OrderBy, OrderDirection]).draw();
            CreateQtipSubMenu($LinkbanksList.find('[data-toggle="ShowQtipSubMenu"]'));
            CreateCopy($LinkbanksList.find('.copy'));
            //if (jsonData.HasMoreLinks) {
            //    LoadMoreLinkbank(jsonData.NextPageNo, jsonData.PerPage);
            //}
        }
        //$(".loadmorewait").hide();
    });
}
$(document).ready(function () {
    //if (HasMoreLinks)
    //	LoadMoreLinkbank(NextPageNo, PerPage);
})

$(window).scroll(function () {
    var ScrollPosition = Math.round($(window).scrollTop() - 0);
    var CurrentPosition = Math.round($(document).height() - $(window).height() - 20);
    var BottomPosition = Math.round($(document).height() - $(window).height());

    //console.log("ScrollPosition: " + ScrollPosition);
    //console.log("CurrentPosition: " + CurrentPosition);
    //console.log("BottomPosition: " + BottomPosition);

    if (ScrollPosition == CurrentPosition || ScrollPosition == BottomPosition) {
        if (HasMoreLinks && !isLoading)
            LoadMoreLinkbank(NextPageNo, PerPage);
    }
});