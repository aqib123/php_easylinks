jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-us-pre": function (a) {
        if (a == null || a == "") {
            return 0;
        }
        var usDatea = $(a).text().split('/'); //a.split('/');
        console.log(usDatea);
        return (usDatea[2] + usDatea[0] + usDatea[1]) * 1;
    },
 
    "date-us-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "date-us-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
} );