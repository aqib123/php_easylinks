﻿/// <reference path="_references.js" />

$(document).on("change", "#DomainID", createsoloadurl);
$(document).on("keyup", "#VisibleLink", createsoloadurl);
$(document).on("keyup", "#UnitPriceSales", createsoloadtrackingpixel);
$(document).on("change", "#EasyLinkID", GetEasyLinkURL);
$(document).on("change", "#VendorID", function () {
    //$(".stats-vendor-name").text("Vendor Name: " + $("#VendorID option:selected").text());
});

$(document).ready(function () {
    createsoloadurl();
    createsoloadtrackingpixel();
    createsoloadactionpixel();
    CreateImageUploader($("#PageImageButton"));
    GetEasyLinkURL();

    $("#vendor-stats").knob({
        'format': function (value) {
            return value + '%';
        }
    });

    $(".stats-vendor-name").text("Vendor Name: " + $("#VendorID option:selected").text());
});

$('#UseAdminDomain').on('ifChanged', function (e) {
    $("#DomainID").prop("disabled", $(this).is(":checked"));
    if ($(this).is(":checked")) {
        $("#AdminDomainID").val(admindomain["id"]);
        $("#DomainID").removeAttr('data-select');
    } else {
        $("#DomainID").attr('data-select', 'select');
    }

    createsoloadurl(e);
});

$('#PartnerSplitAllowed, #TrackCommission').on('ifChanged', function (e) {
    var $this = $(this);
    if ($("#PartnerSplitAllowed").is(":checked") || $("#TrackCommission").is(":checked"))
        $("#ManualSalesEntry").val("1");
    else
        $("#ManualSalesEntry").val("0");
});

//$('#SplitExpensesWithPartner').on('ifChanged', function (e) {
//    var $this = $(this);
//    if ($this.is(":checked")) {
//        $("#div_SplitExpensesPercentage").show();
//    } else {
//        $("#div_SplitExpensesPercentage").hide();
//    }
//});

//$('#ManualSalesEntry').on('ifChanged', function (e) {
//    var $this = $(this);
//    if ($this.is(":checked")) {
//        $("#ManualSalesEntryOptions").show();
//    } else {
//        $("#ManualSalesEntryOptions").hide();
//    }
//});

function createsoloadurl(e) {
    var $soloadurl = $("#soloadurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")

    var Domain;
    var DomainURL;

    $soloadurl.html("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0) {
        $soloadurl.html(DomainURL);
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0) {
        $soloadurl.html(DomainURL + VisibleLink);
    }

    createsoloadtrackingpixel(e);
    createsoloadactionpixel(e);
}

function createsoloadtrackingpixel(e) {
    var $soloadurl = $("#soloadurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")
    var $UnitPriceSales = $("#UnitPriceSales");
    var $TrackingCodeForSalesAndConversions = $("#TrackingCodeForSalesAndConversions");

    var Domain;
    var DomainURL;

    $TrackingCodeForSalesAndConversions.val("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/tp/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/tp/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/tp/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/tp/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0 && $UnitPriceSales.val() != "") {
        DomainURL += $UnitPriceSales.val() + "/" + VisibleLink + ".png";
        $TrackingCodeForSalesAndConversions.val('<img height="1" width="1" alt="" style="display:none" src="' + DomainURL + '" />');
    }
}

function createsoloadactionpixel(e) {
    var $soloadurl = $("#soloadurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")
    var $UnitPriceSales = $("#UnitPriceSales");
    var $TrackingCodeForActions = $("#TrackingCodeForActions");

    var Domain;
    var DomainURL;

    $TrackingCodeForActions.val("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/ap/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/ap/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/ap/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/ap/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0) {
        DomainURL += VisibleLink + ".png";
        $TrackingCodeForActions.val('<img height="1" width="1" alt="" style="display:none" src="' + DomainURL + '" />');
    }
}

function GetEasyLinkURL() {
    if ($("#EasyLinkID").val() != "") {
        var EasyLinkID = "linkbank_" + $("#EasyLinkID").val();
        if (typeof EasyLinkURLs[EasyLinkID] != "undefined") {
            var EasyLinkURL = EasyLinkURLs[EasyLinkID];
            $("#EasyLinkURL").val(EasyLinkURL);
        }
    }
}

$(document).ready(function () {
    $('[data-toggle="custom-validator"]').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            if (!$('#UseAdminDomain').is(":checked") && $("#DomainID").val() == "") {
                $('#DomainID').trigger('input.bs.validator');
                e.preventDefault();
            }
        }
    })
});