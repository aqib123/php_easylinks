﻿/// <reference path="_references.js" />

$(document).on("change", "#DomainID", createrotatorurl); 
$(document).on("keyup", "#VisibleLink", createrotatorurl);
$(document).ready(function () { createrotatorurl() });

$('#UseAdminDomain').on('ifChanged', function (e) {
    $("#DomainID").prop("disabled", $(this).is(":checked"));
    if ($(this).is(":checked")) {
        $("#AdminDomainID").val(admindomain["id"]);
        $("#DomainID").removeAttr('data-select');
    } else {
        $("#DomainID").attr('data-select', 'select');
    }

    createrotatorurl(e);
});

function createrotatorurl(e) {
    var $linkbankurl = $("#linkurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")

    var Domain;
    var DomainURL;

    $linkbankurl.html("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0) {
        $linkbankurl.html(DomainURL);
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0) {
        $linkbankurl.html(DomainURL + VisibleLink);
    }
}

$(document).ready(function () {
    $('[data-toggle="custom-validator"]').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            if (!$('#UseAdminDomain').is(":checked") && $("#DomainID").val() == "") {
                $('#DomainID').trigger('input.bs.validator');
                e.preventDefault();
            }
        }
    })
});