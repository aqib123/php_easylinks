<?php
global $db, $current_user;
$pagetitle = "Rotator Series";
$pageurl = "rotator";
$modulename = "rotator";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);

$isUpdateForm = false;
$AvailableDomainID = 0;
if($_GET && !empty($_GET['RotatorID']) && !isset($errors["msg"])){
    $RotatorID = parseInt($_GET['RotatorID']);
    $sql = "select * from ".$db->rotators." where id=".$RotatorID." and userid=".$current_user->id;
    $rotator = $db->get_row($sql);
    if($rotator){
        $isUpdateForm = true;

        if($rotator->UseAdminDomain == 1)
            $AvailableDomainID = $rotator->DomainID;
    }
}
$admindomain = get_available_domain($AvailableDomainID);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a href=""><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-<?php echo !isset($_GET["SingleLink"])?"gray":"bordered"?>" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2>Link Rotator</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-7 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("rotator/".(isset($_GET['RotatorID'])?"?RotatorID=".$_GET['RotatorID']."":"").(isset($_GET["SingleLink"])?"?SingleLink=true":""))?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="RotatorName" class="col-sm-3 control-label">Rotator Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $rotator->RotatorName; elseif(isset($_POST['RotatorName'])) echo $_POST['RotatorName'];?>" class="form-control " id="RotatorName" name="RotatorName" placeholder="Rotator Name" data-remote="<?php site_url("rotator/?act=check_link_name&type=rotator&id=".(isset($_GET['RotatorID']) && !empty($_GET['RotatorID'])?$_GET['RotatorID']:"0"))?>" data-remote-error="Rotator Name already exist" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="RotatorStatus" class="col-sm-3 control-label">Choose Status</label>
                                            <div class="col-sm-9 ">
                                                <div class="input-group" style="width: 100%">
                                                    <select name="RotatorStatus" id="RotatorStatus" class="form-control select2" style="width: 100%" data-select="select">
                                                        <?php
                                                        $link_statuses = array("pending" => "Pending", "active" => "Active", "complete" => "Completed");
                                                        foreach($link_statuses as $key => $value){
                                                        ?>
                                                        <option value="<?php echo $key;?>" <?php if(($isUpdateForm && $rotator->RotatorStatus == $key) || (isset($_POST['RotatorStatus']) && $_POST['RotatorStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-3 control-label">Choose Group</label>
                                            <div class="col-sm-9 ">
                                                <div class="input-group right-addon">
                                                    <select name="GroupID" id="GroupID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Group</option>
                                                        <?php
                                                        $sql = "select * from ".$db->groups." where GroupType='Rotator' and userid = ".$current_user->id." order by GroupName";
                                                        $groups = $db->get_results($sql);
                                                        foreach($groups as $group){
                                                        ?>
                                                        <option value="<?php echo $group->id;?>" <?php if(($isUpdateForm && $rotator->GroupID == $group->id) || (isset($_POST['GroupID']) && $_POST['GroupID'] == $group->id)) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=Rotator")?>" data-widget="ShowLinkModel" data-related="#GroupID" data-method="post" data-page-title="Rotator Group Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="DomainID" class="col-sm-3 col-xs-12 control-label">Choose Domain</label>
                                            <div class="col-sm-9 col-xs-12 ">
                                                <?php
                                                $checked = "";
                                                $AdminDomainID = "";
                                                $dropdowndisable = "";
                                                if(($isUpdateForm && $rotator->UseAdminDomain == 1) || (isset($_POST['UseAdminDomain']))){
                                                    $checked = ' checked = "checked" ';
                                                    $AdminDomainID = $rotator->DomainID;
                                                    $dropdowndisable = ' disabled = "disabled" ';
                                                }else{
                                                    $dropdowndisable = ' data-select="select" ';
                                                }

                                                ?>
                                                <div class="input-group right-addon">
                                                    <select name="DomainID" id="DomainID" class="form-control select2" style="width: 99%" <?php echo $dropdowndisable;?>>
                                                        <option value="">Select Domain</option>
                                                        <?php
                                                        $sql = "select * from ".$db->domains." where userid = ".$current_user->id." and DomainType='userdomain' order by DomainName";
                                                        $domains = $db->get_results($sql);
                                                        $domainurls = array();
                                                        foreach($domains as $domain){
                                                            $domainurls[] = '"'.$domain->id.'": {name:"'.$domain->DomainName.'", forward:"'.$domain->DomainForward.'", url:"'.$domain->DomainUrl.'", type:"'.$domain->DomainType.'"}';
                                                        ?>
                                                        <option value="<?php echo $domain->id;?>" <?php if(($isUpdateForm && $rotator->DomainID == $domain->id) || (isset($_POST['DomainID']) && $_POST['DomainID'] == $domain->id)) echo ' selected = "selected" ';?>><?php echo $domain->DomainName;?></option>
                                                        <?php
                                                        }
                                                        $str_domains = 'var domains = {'.implode(',', $domainurls).'};';
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("domain/?js=true&DomainType=userdomain")?>" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="Domains Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="hidden" name="AdminDomainID" id="AdminDomainID" value="<?php echo $AdminDomainID;?>" />
                                                    <input type="checkbox" name="UseAdminDomain" id="UseAdminDomain" class="blue " value="1" <?php echo $checked;?> />&nbsp;
                                                    <span>Use Admin Domain</span>&nbsp;&nbsp;
                                                </div>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="checkbox" name="CloakURL" id="CloakURL" class="blue " value="1" <?php if(($isUpdateForm && $rotator->CloakURL == 1) || (isset($_POST['CloakURL']))) echo "checked='checked'";?> />&nbsp;
                                                    <span>Cloak URL</span>&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="VisibleLink" class="col-sm-3 control-label">Visible Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $rotator->VisibleLink; else if(isset($_POST['VisibleLink'])) echo $_POST['VisibleLink'];?>" class="form-control " id="VisibleLink" name="VisibleLink" placeholder="Visible Link" data-remote="<?php site_url("linkbank/")?>" data-remote-values="getVisibleLinkValues()" data-remote-error="Visible Link for selected Domain already exist" required="required" <?php echo VISIBLELINK_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id="linkurl"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-3 control-label">Choose Master Campaign</label>
                                            <div class="col-sm-9 ">
                                                <div class="input-group right-addon">
                                                    <select name="MasterCampaignID" id="MasterCampaignID" class="form-control select2" style="width: 99%">
                                                        <option value="0">Select Master Campaign</option>
                                                        <?php
                                                        $sql = "select * from ".$db->master_campaigns." where MasterCampaignStatus='active' and userid = ".$current_user->id." order by MasterCampaignName";
                                                        $MasterCampaigns = $db->get_results($sql);
                                                        foreach($MasterCampaigns as $MasterCampaign){
                                                        ?>
                                                        <option value="<?php echo $MasterCampaign->id;?>" <?php if(($isUpdateForm && $rotator->MasterCampaignID == $MasterCampaign->id) || (isset($_POST['MasterCampaignID']) && $_POST['MasterCampaignID'] == $MasterCampaign->id)) echo ' selected = "selected" ';?>><?php echo $MasterCampaign->MasterCampaignName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("master-campaign-add/?js=true")?>" data-widget="ShowLinkModel" data-related="#MasterCampaignID" data-method="post" data-page-title="Master Campaign" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-3 control-label">Choose Type</label>
                                            <div class="col-sm-9 smallradio- ">
                                                <?php
                                                $options = array(
                                                    "Original"  => "Normal - Each user moves through the rotation separately, in order",
                                                    "Standard"  => "Random - A random site is chosen from the rotation each time",
                                                    //"Sticky"  => "Sticky - A random site is chosen from the rotation per user, and it persists",
                                                    "Scarcity"  => "Scarcity - All go to the first site that has not exceeded its maximum hits",
                                                    "Split"     => "Split Rotation - Each user sees the next site in rotation"
                                                );
                                                $checked = "";
                                                $index = 0;
                                                foreach($options as $option=>$label){
                                                    if(empty($_GET['RotatorID']) && $index++ == 0)
                                                        $checked = ' checked = "checked" ';
                                                    else if(($isUpdateForm && $rotator->RotatorType == $option) || (isset($_POST['RotatorType']) && $_POST['RotatorType'] == $option)) 
                                                        $checked = ' checked = "checked" ';
                                                    else
                                                        $checked = ' ';
                                                ?>
                                                <div style="margin-bottom:5px;"><input type="radio" name="RotatorType" class="blue" value="<?php echo $option;?>" <?php echo $checked;?> />&nbsp;<span class="small"><?php echo $label;?></span>&nbsp;</div>
                                                <?php } ?>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="DomainID" class="col-sm-3 col-xs-12 control-label"></label>
                                            <div class="col-sm-9 col-xs-12 ">
                                                <input type="checkbox" name="RotatorIsSticky" id="RotatorIsSticky" class="blue " value="1" <?php if($isUpdateForm && $rotator->RotatorIsSticky == 1) echo ' checked="checked" ';?> />&nbsp;
                                                <small>Returning Visitor - Show same link to returing visitor</small>&nbsp;&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputLink3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("rotator/stats")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_rotator" />
                            </form>
                        </div>

                        <div class="col-md-5 col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking link? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">How to create a new tracking link</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking link articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var CurrentVisibleLink = "<?php echo $isUpdateForm?$rotator->VisibleLink:"";?>";
    var CurrentID = "<?php echo isset($_GET["RotatorID"])?$_GET["RotatorID"]:"0";?>";
</script>

<script src="<?php site_url("js/rotator.js");?>" type="text/javascript"></script>
<?php
echo '<script type="text/javascript">'.
$str_domains.
' var userid = "'.$current_user->id.'";'.
' var admindomain = '.$admindomain.';'.
'</script>';
include_once "member_footer.php";
?>