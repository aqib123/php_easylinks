<?php
if($_GET['linkmonitoring_type'] == "") 
    $linkmonitoringtype = "all";
else
    $linkmonitoringtype = $_GET['linkmonitoring_type'];

$pageurl = "link-monitoring";
$pagetitle = "Link Monitoring";
$modulename = "link-monitoring";

include_once "member_header.php";

$item = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url($pageurl)?>" role="button">Link Monitoring</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url($pageurl."/domains/")?>" role="button">Custom Domains</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($pageurl);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Links"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($pageurl."/easylinks");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="EasyLinks Domains"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($pageurl."/custom");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Custom Domains"><i class="fa fa-rocket"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Domain Name</span></th>
                            <th><span>Domain URL</span></th>
                            <th><span class="text-center">Blacklisted</span></th>
                            <th><span class="text-center">Server Error</span></th>
                            <th><span>Last Check</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->domains." where userid=".$current_user->id;
                        if($linkmonitoringtype == "easylinks")
                            $sql .= " and DomainType='userdomain' ";
                        else if($linkmonitoringtype == "custom")
                            $sql .= " and DomainType='customdomain' ";
                        
                        $domains = $db->get_results($sql);
                        foreach ($domains as $domain){
                            $sql = "select * from ".$db->domain_statuses." where DomainID = ".$domain->id." ORDER BY DateAdded DESC LIMIT 0,1";
                            $domain_status = $db->get_row($sql);
                            
                            $blacklisted = "N/A";
                            $servererror = "N/A";
                            $lastcheck = "N/A";
                            if($domain_status){
                                $blacklisted = $domain_status->DomainBlacklisted == 0?"N/A":"YES";
                                $servererror = $domain_status->DomainServerError == 0?"N/A":"YES";
                                $lastcheck = date("m/d/Y H:i:s A", $domain_status->DateAdded);
                            }
                        ?>
                        <tr>
                            <td><span><?php echo $domain->DomainName?></span></td>
                            <td><a href="javascript:" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$domain->DomainUrl;?>"><?php echo $domain->DomainUrl?></a></td>
                            <td><span class="text-center"><?php echo $blacklisted;?></span></td>
                            <td><span class="text-center"><?php echo $servererror;?></span></td>
                            <td><span><?php echo $lastcheck;?></span></td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>