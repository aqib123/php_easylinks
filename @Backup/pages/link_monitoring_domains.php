<?php
global $db, $current_user;

$pageurl = "link-monitoring";
$pagetitle = "Link Monitoring";
$modulename = "link-monitoring";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url($pageurl)?>" role="button">Link Monitoring</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url($pageurl."/domains/")?>" role="button">Custom Domains</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Custom Domains</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Custom Domain</label>
                                            <div class="col-xs-12 ">
                                                <select name="CustomDomainID" id="CustomDomainID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->domains." where userid = ".$current_user->id." and DomainType='customdomain' order by DomainName";
                                                    $domains = $db->get_results($sql);
                                                    foreach($domains as $domain){
                                                    ?>
                                                    <option value="<?php echo $domain->id;?>"><?php echo $domain->DomainName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("domain/?js=true&DomainType=customdomain&nodefault=true")?>" data-widget="ShowLinkModel" data-related="#CustomDomainID" data-method="post" data-page-title="Custom Domain" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("domain/?js=true&DomainType=customdomain&nodefault=true&DomainID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#CustomDomainID" data-method="post" data-page-title="Custom Domain" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("domain/?js=true&DomainType=customdomain&nodefault=true")?>" data-values="act=delete_domain&DomainID=" data-append="true" data-widget="RemoveOption" data-related="#CustomDomainID" data-method="post" data-page-title="Custom Domain" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>