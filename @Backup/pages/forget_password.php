<?php
$bodyclasses = ' login-page ';
$pagetitle = ' Forget Password ';
include_once "site_header.php";
?>
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg page-header">
            Reset Your Password
        </p>
<?php
if(!isset($_GET["key"])){
?>
        <form class="form-horizontal easylink-form forgetpassword-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("login/forget-password/")?>">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="User Name or Email Address" required="required"  />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-block btn-flat btn-primary"><span class="text-uppercase">Reset Password <i class="ion-log-in"></i></span></button>
                    <input type="hidden" name="act" value="forget_password" />
                </div>
            </div>
        </form>
<?php
} else {
?>
        <form class="form-horizontal easylink-form resetpassword-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("login/forget-password/")?>">
            <div class="form-group has-feedback">
                <input type="password" name="newpassword" id="NewPassword" class="form-control" placeholder="New Password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>

            <div class="form-group has-feedback">
                <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password" data-match="#NewPassword" data-match-error="Passwords do not match" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-block btn-flat btn-primary"><span class="text-uppercase">Reset Password <i class="ion-log-in"></i></span></button>
                    <input type="hidden" name="key" value="<?php echo $_GET["key"];?>" />
                    <input type="hidden" name="act" value="reset_password" />
                </div>
            </div>
        </form>
<?php
}
?>
    </div>
    <!-- /.login-box-body -->
</div>
<script src="<?php site_url("js/forget_password.js");?>" type="text/javascript"></script>
<?php
include_once "site_footer.php";
?>