<?php
$pageurl = "help-content";
$pagetitle = "Help Contents";
$modulename = "help-contents";

include_once "member_popup_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['id']) && is_numeric($_GET['id'])){
    $HelpContentID = intval($_GET["id"]);
    $HelpContent = $db->get_row("select * from ".$db->help_contents." where HelpContentEnabled=true and id=".$HelpContentID);
    if($HelpContent){
        $isUpdateForm = true;
?>
<!-- Content Wrapper. Contains page content -->
<div class="<?php //if($HelpContent->HelpContentType == 'html') echo 'container-fluid'?>" style="<?php //if($HelpContent->HelpContentType == 'youtubevideo') echo 'padding: 0;'?>">
    <?php
    if($HelpContent->HelpContentType == 'youtubevideo'){
    ?>
    <div class="video_wrap">
        <div class="video_container">
            <iframe width="100%" height="450px" src="https://www.youtube.com/embed/<?php echo $HelpContent->HelpContentText; ?>?rel=0&vq=hd720" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="clear"></div>
    </div>
    <?php
    }
    ?>
</div>
<!-- /.content-wrapper -->
<?php
    }
}
include_once "member_popup_footer.php";
?>