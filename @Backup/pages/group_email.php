<?php
global $db, $current_user;
$pagetitle = "Email Campaigns";
$pageurl = "group/email";
$curpage = "group/email/";
$modulename = "email-campaigns";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-button top-buttons">
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/linkbank/")?>" role="button">Link Bank</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/paidtraffics/")?>" role="button">Paid Traffic</a>
                <a class="btn btn-flat btn-gray" href="<?php site_url("group/email/")?>" role="button">Email Campaigns</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/rotator/")?>" role="button">Rotator Groups</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/link-sequence/")?>" role="button">Link Sequence</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/pixels/")?>" role="button">Retargeting Pixels</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/holding-pages/")?>" role="button">Holding Pages</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Email Groups</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="AutoresponderID" class="col-xs-12 control-label">Autoresponder</label>
                                            <div class="col-xs-12 ">
                                                <select name="AutoresponderID" id="AutoresponderID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->autoresponders." where AutoresponderType='Email' and userid = ".$current_user->id." and parentid=0 order by AutoresponderName";
                                                    $autoresponders = $db->get_results($sql);
                                                    foreach($autoresponders as $autoresponder){
                                                    ?>
                                                    <option value="<?php echo $autoresponder->id;?>"><?php echo $autoresponder->AutoresponderName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-widget="ShowLinkModel" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&AutoresponderID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-values="act=delete_autoresponder&AutoresponderID=" data-append="true" data-widget="RemoveOption" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="AutoresponderID" class="col-xs-12 control-label">Autoresponder List</label>
                                            <div class="col-xs-12 ">
                                                <select name="AutoresponderID" id="AutoresponderListID" class="form-control select2" style="width: 100%" data-select="select">
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&ParentID=")?>" data-append="true" data-append-object="#AutoresponderID" data-widget="ShowLinkModel" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder List" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&AutoresponderID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder List" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-values="act=delete_autoresponder&AutoresponderID=" data-append="true" data-widget="RemoveOption" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder List" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Email Type</label>
                                            <div class="col-xs-12 ">
                                                <select name="GroupID" id="EmailTypeID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->groups." where GroupType='EmailType' and userid = ".$current_user->id." order by GroupName";
                                                    $groups = $db->get_results($sql);
                                                    foreach($groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=EmailType")?>" data-widget="ShowLinkModel" data-related="#EmailTypeID" data-method="post" data-page-title="Email Type" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=EmailType&GroupID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#EmailTypeID" data-method="post" data-page-title="Email Type" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=EmailType")?>" data-values="act=delete_group&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#EmailTypeID" data-method="post" data-page-title="Email Type" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Email Group</label>
                                            <div class="col-xs-12 ">
                                                <select name="GroupID" id="EmailGroupID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->groups." where GroupType='Email' and userid = ".$current_user->id." order by GroupName";
                                                    $groups = $db->get_results($sql);
                                                    foreach($groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=Email")?>" data-widget="ShowLinkModel" data-related="#EmailGroupID" data-method="post" data-page-title="Email Group" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=Email&GroupID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#EmailGroupID" data-method="post" data-page-title="Email Group" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=Email")?>" data-values="act=delete_group&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#EmailGroupID" data-method="post" data-page-title="Email Group" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    /// <reference path="_references.js" />
    $(document).ready(function () {
        $("#AutoresponderID").on("change", function (e) {
            var url = "<?php site_url("group/email")?>";
            $this = $(this);
            if ($this.val() != null && $this.val() != "") {
                var AutoresponderID = $this.val();
                url += "?act=get_autoresponder&AutoresponderID=" + AutoresponderID;
                $.get(url, "", function (data) {
                    var $AutoresponderListID = $("#AutoresponderListID");
                    var jsonData = $.parseJSON(data);

                    $AutoresponderListID.before(jsonData.before);
                    $AutoresponderListID.html(jsonData.value);
                    $AutoresponderListID.select2({
                        minimumResultsForSearch: Infinity
                    });

                    $AutoresponderListID.after(jsonData.after);
                    eval(jsonData.jscode);
                });
            }
        });
        $("#AutoresponderID").trigger("change");
    })
</script>
<?php
include_once "member_footer.php";
?>