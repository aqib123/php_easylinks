<?php
global $db, $current_user;
$pagetitle = "RedirectLinks - Paid Traffic";
$pageurl = "group/holding-pages";
$modulename = "dashboard";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/linkbank/")?>" role="button">Link Bank</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/paidtraffics/")?>" role="button">Paid Traffic</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/email/")?>" role="button">Email Campaigns</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/rotator/")?>" role="button">Rotator Groups</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/link-sequence/")?>" role="button">Link Sequence</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/pixels/")?>" role="button">Retargeting Pixels</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-gray" href="<?php site_url("group/holding-pages/")?>" role="button">Holding Pages</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Holding Pages</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Pending Page</label>
                                            <div class="col-xs-12 ">
                                                <select name="PendingPageID" id="PendingPageID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='PendingPage' and userid = ".$current_user->id." order by RedirectLinkName";
                                                    $redirect_links = $db->get_results($sql);
                                                    foreach($redirect_links as $redirect_link){
                                                    ?>
                                                    <option value="<?php echo $redirect_link->id;?>"><?php echo $redirect_link->RedirectLinkName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=PendingPage")?>" data-widget="ShowLinkModel" data-related="#PendingPageID" data-method="post" data-page-title="Pending Page" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=PendingPage&RedirectLinkID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#PendingPageID" data-method="post" data-page-title="Pending Page" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=PendingPage")?>" data-values="act=delete_redirect_link&RedirectLinkID=" data-append="true" data-widget="RemoveOption" data-related="#PendingPageID" data-method="post" data-page-title="Pending Page" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Complete Page</label>
                                            <div class="col-xs-12 ">
                                                <select name="CompletePageID" id="CompletePageID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='CompletePage' and userid = ".$current_user->id." order by RedirectLinkName";
                                                    $redirect_links = $db->get_results($sql);
                                                    foreach($redirect_links as $redirect_link){
                                                    ?>
                                                    <option value="<?php echo $redirect_link->id;?>"><?php echo $redirect_link->RedirectLinkName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=CompletePage")?>" data-widget="ShowLinkModel" data-related="#CompletePageID" data-method="post" data-page-title="Complete Page" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=CompletePage&RedirectLinkID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#CompletePageID" data-method="post" data-page-title="Complete Page" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("holding-page/?js=true&nodefault=true&RedirectLinkType=CompletePage")?>" data-values="act=delete_redirect_link&RedirectLinkID=" data-append="true" data-widget="RemoveOption" data-related="#CompletePageID" data-method="post" data-page-title="Complete Page" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Default Pending Page</label>
                                            <div class="col-xs-12 ">
                                                <select name="DefaultPendingPageID" id="DefaultPendingPageID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='PendingPage' and userid = ".$current_user->id." order by RedirectLinkName";
                                                    $redirect_links = $db->get_results($sql);
                                                    foreach($redirect_links as $redirect_link){
                                                    ?>
                                                    <option value="<?php echo $redirect_link->id;?>" <?php if($redirect_link->RedirectLinkIsDefault == 1) echo " selected='selected' ";?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/holding-pages/?RedirectLinkType=PendingPage")?>" data-values="act=mark_page_default&RedirectLinkID=" data-append="true" data-widget="MarkPageDefault" data-related="#DefaultPendingPageID" data-method="get" data-page-title="Default Pending Page" class="btn btn-link btn-bordered btn-sm">Mark as Default</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Default Complete Page</label>
                                            <div class="col-xs-12 ">
                                                <select name="DefaultCompletePageID" id="DefaultCompletePageID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='CompletePage' and userid = ".$current_user->id." order by RedirectLinkName";
                                                    $redirect_links = $db->get_results($sql);
                                                    foreach($redirect_links as $redirect_link){
                                                    ?>
                                                    <option value="<?php echo $redirect_link->id;?>" <?php if($redirect_link->RedirectLinkIsDefault == 1) echo " selected='selected' ";?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/holding-pages/?RedirectLinkType=CompletePage")?>" data-values="act=mark_page_default&RedirectLinkID=" data-append="true" data-widget="MarkPageDefault" data-related="#DefaultCompletePageID" data-method="get" data-page-title="Default Complete Page" class="btn btn-link btn-bordered btn-sm">Mark as Default</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Default 404 Page</label>
                                            <div class="col-xs-12 ">
                                                <select name="Default404PageID" id="Default404PageID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='PendingPage' and userid = ".$current_user->id." order by RedirectLinkName";
                                                    $redirect_links = $db->get_results($sql);
                                                    foreach($redirect_links as $redirect_link){
                                                    ?>
                                                    <option value="<?php echo $redirect_link->id;?>" <?php if($redirect_link->RedirectLinkIsDefault404 == 1) echo " selected='selected' ";?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/holding-pages/?RedirectLinkType=PendingPage")?>" data-values="act=mark_page_default_404&RedirectLinkID=" data-append="true" data-widget="MarkPageDefault" data-related="#Default404PageID" data-method="get" data-page-title="Default 404 Page" class="btn btn-link btn-bordered btn-sm">Mark as Default</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    $(document).ready(function () {
        $(document).on('click', '[data-widget="MarkPageDefault"]', function (e) {
            e.preventDefault();
            var $this = $(this);
            var href = $this.attr('href');
            var data = $this.attr("data-values");
            var method = $this.attr("data-method");
            var $related = $($this.attr("data-related"));
            var selected = $related.is("select") ? $related.val() : "-1";

            if (typeof selected == "undefined" || selected == null || selected == "")
                return;

            if ($this.attr("data-related") && $this.attr("data-append")) {
                if (typeof selected != "undefined" && selected != "")
                    data += selected;
            }

            $.pleasewait();

            $.ajax({
                method: method,
                url: href,
                data: data
            }).done(function (data) {
                $.pleasewait('hide');
                console.log(data);
            });
        });
    });
</script>
<?php
include_once "member_footer.php";
?>