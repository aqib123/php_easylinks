<footer class="main-footer full-width">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.2.0
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://chadnicely.com">Chad Nicely</a>.</strong> All rights reserved.
</footer>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php site_url("js/bootstrap.min.js");?>"></script>

<!-- iCheck -->
<script src="<?php site_url("js/icheck.min.js");?>" type="text/javascript"></script>

<!-- Select2 -->
<script src="<?php site_url("js/select2.min.js");?>"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?php site_url("js/moment.min.js");?>"></script>
<script type="text/javascript" src="<?php site_url("js/daterangepicker.js");?>"></script>

<!-- SlimScroll -->
<script src="<?php site_url("js/jquery.slimscroll.min.js");?>" type="text/javascript"></script>

<!-- FastClick -->
<script src="<?php site_url("js/fastclick.min.js");?>" type="text/javascript"></script>

<!-- Validator -->
<script src="<?php site_url("js/validator.min.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/easylinks.js");?>" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?php site_url("js/app.min.js");?>" type="text/javascript"></script>
</body>
</html>