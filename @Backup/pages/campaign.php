<?php
global $db, $current_user;
$pagetitle = "Campaign Series";
$pageurl = "campaign".(isset($_GET["SingleEmail"])?"/single":"");
$modulename = "email-campaigns";
    
include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);

$isUpdateForm = false;
if($_GET && !empty($_GET['CampaignID'])){
    $CampaignID = parseInt($_GET['CampaignID']);
    $sql = "select * from ".$db->campaigns." where id=".$CampaignID." and userid=".$current_user->id;
    $campaign = $db->get_row($sql);
    if($campaign){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-<?php echo isset($_GET["SingleEmail"])?"gray":"bordered"?>" href="<?php site_url("campaign/single")?>" role="button">Create Single Email</a>
                <a class="btn btn-flat btn btn-<?php echo !isset($_GET["SingleEmail"])?"gray":"bordered"?>" href="<?php site_url("campaign")?>" role="button">Create Email Series</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign/stats")?>" role="button">Campaign Statistics</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2>Email Campaign</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("campaign/".(isset($_GET["SingleEmail"])?"single/":"").($isUpdateForm?$CampaignID."/":""))?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="CampaignName" class="col-sm-4 control-label">Campaign Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $campaign->CampaignName; elseif(isset($_POST['CampaignName'])) echo $_POST['CampaignName'];?>" class="form-control " id="CampaignName" name="CampaignName" placeholder="Campaign Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-4 control-label">Choose Email Group</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="GroupID" id="GroupID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Group</option>
                                                        <?php
                                                        $sql = "select * from ".$db->groups." where GroupType='Email' and userid = ".$current_user->id." order by GroupName";
                                                        $groups = $db->get_results($sql);
                                                        foreach($groups as $group){
                                                        ?>
                                                        <option value="<?php echo $group->id;?>" <?php if(($isUpdateForm && $campaign->GroupID == $group->id) || (isset($_POST['GroupID']) && $_POST['GroupID'] == $group->id)) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=Email")?>" data-widget="ShowLinkModel" data-related="#GroupID" data-method="post" data-page-title="Email Group Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Master Campaign</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="MasterCampaignID" id="MasterCampaignID" class="form-control select2" style="width: 99%">
                                                        <option value="0">Select Master Campaign</option>
                                                        <?php
                                                        $sql = "select * from ".$db->master_campaigns." where MasterCampaignStatus='active' and userid = ".$current_user->id." order by MasterCampaignName";
                                                        $MasterCampaigns = $db->get_results($sql);
                                                        foreach($MasterCampaigns as $MasterCampaign){
                                                        ?>
                                                        <option value="<?php echo $MasterCampaign->id;?>" <?php if(($isUpdateForm && $campaign->MasterCampaignID == $MasterCampaign->id) || (isset($_POST['MasterCampaignID']) && $_POST['MasterCampaignID'] == $MasterCampaign->id)) echo ' selected = "selected" ';?>><?php echo $MasterCampaign->MasterCampaignName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("master-campaign-add/?js=true")?>" data-widget="ShowLinkModel" data-related="#MasterCampaignID" data-method="post" data-page-title="Master Campaign" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("campaign/stats")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_campaign" />
                            </form>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking link? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">How to create a new tracking link</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking link articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>