<?php
global $db, $current_user;

if(!isset($_GET["MasterCampaignID"]) && !isset($_GET["LinkBankID"]))
    return;

$pagetitle = "Manual Sale";
$pageurl = "manual-sale/?".(isset($_GET["MasterCampaignID"])?"MasterCampaignID=".$_GET["MasterCampaignID"]:"LinkBankID=".$_GET["LinkBankID"]);
$prevpageurl = isset($_GET["MasterCampaignID"])?"master-campaign/":"linkbank/stats/";
$nextpageurl = "manual-sale/split-partners/?".(isset($_GET["MasterCampaignID"])?"MasterCampaignID=".$_GET["MasterCampaignID"]:"LinkBankID=".$_GET["LinkBankID"]);
$modulename = "manual-sale";

include_once "member_header.php";

$MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
$LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;

$mastercampaign = $db->get_row("select * from ".$db->master_campaigns." where userid=".$current_user->id." and id=".$MasterCampaignID);
$linkbank = $db->get_row("select * from ".$db->linkbanks." where userid=".$current_user->id." and id=".$LinkBankID);

$ManualSale = get_manual_sale(0, $MasterCampaignID, $LinkBankID);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2>Manual Sale</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url($pageurl)?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="ManualSaleName" class="col-sm-5 control-label">Manual Sale Name</label>
                                            <div class="col-sm-7">
                                                <input type="text" value="<?php echo isset($_POST['ManualSaleName'])?$_POST['ManualSaleName']:$ManualSale->ManualSaleName;?>" class="form-control " id="ManualSaleName" name="ManualSaleName" placeholder="Manual Sale Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-5 col-sm-7">
                                                <label>
                                                    <input type="checkbox" class="blue" name="isSplitPartnerSale" id="isSplitPartnerSale" <?php if($ManualSale->isSplitPartnerSale == 1) echo ' checked="checked" '?>>
                                                    Activate Partner Split
                                                </label>
                                            </div>
                                        </div>

                                        <div id="SplitPartnerOptions"  <?php if($ManualSale->isSplitPartnerSale == 0) echo ' style="display:none;" '?>>
                                            <div class="form-group select-form-group">
                                                <label for="PartnerID" class="col-sm-5 col-xs-12 control-label">Choose Partner</label>
                                                <div class="col-sm-7 col-xs-12 ">
                                                    <div class="input-group right-addon">
                                                        <select name="PartnerID" id="PartnerID" class="form-control select2" style="width: 99%" <?php if($ManualSale->isSplitPartnerSale == 0) echo ' data-select-="select" '?>>
                                                            <option value="" selected="selected">Select Partner</option>
                                                            <?php
                                                            $sql = "select * from ".$db->split_partners." where userid = ".$current_user->id." order by PartnerName";
                                                            $split_partners = $db->get_results($sql);
                                                            foreach($split_partners as $split_partner){
                                                            ?>
                                                            <option value="<?php echo $split_partner->id;?>"><?php echo $split_partner->PartnerName;?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <a href="<?php site_url("split-partner/?js=true")?>" data-widget="ShowLinkModel" data-related="#PartnerID" data-method="post" data-page-title="Partner" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback">
                                                <label for="IncomePercentage" class="col-sm-5 control-label">Partner Sale Income %</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="IncomePercentage" name="IncomePercentage" placeholder="Income %" value="<?php echo $split_partner->IncomePercentage?>" <?php echo FLOAT_PATTERN;?>>
                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback">
                                                <label for="OtherIncomePercentage" class="col-sm-5 control-label">Partner Other Income %</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="OtherIncomePercentage" name="OtherIncomePercentage" placeholder="OtherIncome %" value="<?php echo $split_partner->OtherIncomePercentage?>" <?php echo FLOAT_PATTERN;?>>
                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback">
                                                <label for="ExpensePercentage" class="col-sm-5 control-label">Partner Expense %</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="ExpensePercentage" name="ExpensePercentage" placeholder="Expense %" value="<?php echo $split_partner->ExpensePercentage?>" <?php echo FLOAT_PATTERN;?>>
                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-5 col-sm-7">
                                                    <button type="button" class="btn btn-success pull-left btn-sm btn-add-partner">Add Partner</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <ul class="list-inline manual-sale-split-partners-list">
                                            <?php
                                            $sql = "select ".$db->manualsale_split_partners.".* from ".$db->manualsale_split_partners." inner join ".$db->split_partners." on ".$db->manualsale_split_partners.".SplitPartnerID = ".$db->split_partners.".id  where isUserSale = 0 and ".$db->manualsale_split_partners.".userid=".$current_user->id." and ManualSaleID=".$ManualSale->id." order by id asc";
                                            $split_partners = $db->get_results($sql);
                                            foreach ($split_partners as $index => $split_partner){
                                                echo get_manual_sale_split_partner_item($split_partner);
                                            } 
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-5 control-label"></label>
                                            <div class="col-sm-7">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url($prevpageurl);?>" class="btn btn-danger">Cancel</a>
                                                <a href="<?php site_url("manual-sale/stats/?act=delete_manualsale&ManualSaleID=".$ManualSale->id);?>" class="btn btn-danger btn-delete bg-black">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="update_manual_sale" />
                                <input type="hidden" name="ManualSaleID" value="<?php echo $ManualSale->id;?>" />
                            </form>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking link? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">How to create a new tracking link</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking link articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var url = "<?php site_url($pageurl);?>";
    var ManualSaleID = "<?php echo $ManualSale->id;?>";
</script>
<script src="<?php site_url("js/manual_sale.js");?>" type="text/javascript"></script>
<?php
include_once "member_footer.php";
?>