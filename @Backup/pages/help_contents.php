<?php
$pageurl = "help-contents";
$pagetitle = "Help Contents";
$modulename = "help-contents";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th></th>
                            <th><span>Title</span></th>
                            <th><span>Description</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->help_contents." where HelpContentEnabled=true order by HelpContentPosition";
                        $help_contents = $db->get_results($sql);
                        foreach ($help_contents as $help_content){
                            $HelpContentExcerpt = $help_content->HelpContentExcerpt;
                            $HelpContentType = $help_content->HelpContentType;
                            if($HelpContentType == 'youtubevideo'){
                                $HelpContentExcerpt = '<a href="javascript:" data-widget="ShowLinkModel" data-href="'.get_site_url("help-content/?id=".$help_content->id).'" data-no-footer="true" data-model-width="900" data-page-title="'.$help_content->HelpContentTitle.'"><img src="http://img.youtube.com/vi/'.$help_content->HelpContentText.'/maxresdefault.jpg" class="youtube_thumbnail" /></a><br/>'.$HelpContentExcerpt;
                            }
                        ?>
                        <tr>
                            <td class="text-center"><span><?php echo $help_content->HelpContentPosition;?></span></td>
                            <td>
                                <a href="javascript:" data-href="<?php site_url("help-content/?id=".$help_content->id)?>" data-widget="ShowLinkModel" data-no-footer="true" data-model-width="900" data-page-title="<?php echo $help_content->HelpContentTitle?>"><?php echo $help_content->HelpContentTitle?></a>
                            </td>
                            <td><span><?php echo $HelpContentExcerpt;?></span></td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>