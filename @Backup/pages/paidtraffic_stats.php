<?php
if($_GET['paidtraffic_type'] == "") 
    $paidtrafficstype = "all";
else
    $paidtrafficstype = $_GET['paidtraffic_type'];

$paidtraffic_sale_type = $_GET['paidtraffic_sale_type'];

$pagetitle = "Paid Traffic Stats";
$curpage = "paidtraffic/stats".($paidtraffic_sale_type == ""?"":"/".$paidtraffic_sale_type);
$pageurl = "paidtraffic/stats";
$modulename = "paid-traffic".($paidtraffic_sale_type == ""?"":"-".$paidtraffic_sale_type);

//$curpage .= !empty($_GET['paidtraffic_type'])?"/".$_GET['paidtraffic_type']:"";

include_once "member_header.php";

$item = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic")?>" role="button">Create Paid Campaign</a>
                <a class="btn btn-flat btn btn-<?php echo $paidtraffic_sale_type == ""?"gray":"bordered"?>" href="<?php site_url($pageurl)?>" role="button">Paid Traffic Statistics</a>
                <a class="btn btn-flat btn-paidtraffic-conv btn btn-<?php echo $paidtraffic_sale_type == "conv"?"gray":"bordered"?>" href="<?php site_url($pageurl."/conv")?>" role="button">Paid Traffic Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Paid Traffic"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Paid Traffic"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Paid Traffic"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed Paid Traffic"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Paid Traffic Campaigns</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-paidtraffic-tracking-stats data-table responsive- nowrap" data-nosort-columns="0" data-default-sort-column="1">
                    <thead>
                        <tr>
                            <th><span></span></th>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <th><span></span></th>
                            <?php } ?>
                            <th><span>Name</span></th>
                            <th><span></span></th>
                            <th><span>Vendor</span></th>
                            <th><span>Group</span></th>
                            <th><span>Master</span></th>
                            <th class="text-center stats-column"><span>Raw</span></th>
                            <th class="text-center stats-column"><span>Unique</span></th>
                            <th class="text-center stats-column"><span>Bad</span></th>
                            <th class="text-center stats-column"><span>Leads #</span></th>
                            <th class="text-center stats-column"><span>Optin %</span></th>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <th class="text-center sale-column"><span>Sales #</span></th>
                            <th class="text-center sale-column"><span>Sales $</span></th>
                            <th class="text-center sale-column"><span>Sales %</span></th>
                            <?php } ?>
                            <th class="text-center cost-column"><span>CPC</span></th>
                            <th class="text-center cost-column"><span>CPL</span></th>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <th class="text-center cost-column"><span>CPS</span></th>
                            <th class="text-center cost-column"><span>EPC</span></th>
                            <th class="text-center cost-column"><span>Net Profit $</span></th>
                            <th class="text-center non-score-column"><span class="text-center">Non Scoring</span></th>
                            <?php } ?>
                            <th class="text-center score-column"><span class="text-center">Scoring Blocks</span></th>
                            <th class="text-center score-column-" data-toggle="qtiptooltip" title="Overall Score"><span class="text-center">Score</span></th>
                            <th class="text-center"><span>Status</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->paidtraffics." where id ".($paidtraffic_sale_type == ""?"not ":"")."in(select PaidTrafficID from ".$db->paidtraffic_conversion_pixels.") and userid=".$current_user->id;

                        if($paidtrafficstype == "pending")
                            $sql .= " and PaidTrafficStatus = 'pending' ";//" and ('".strtotime("now")."' < StartDate) ";
                        else if($paidtrafficstype == "active")
                            $sql .= " and PaidTrafficStatus = 'active' ";//" and ('".strtotime("now")."' > StartDate and '".strtotime("now")."' < EndDate) ";
                        else if($paidtrafficstype == "complete")
                            $sql .= " and PaidTrafficStatus = 'complete' ";//" and ('".strtotime("now")."' > EndDate) ";

                        $paidtraffics = $db->get_results($sql);
                        foreach($paidtraffics as $paidtraffic){
                            $userid = $current_user->id;
                            $TrafficCost = $paidtraffic->TrafficCost;

                            $GroupName = $db->get_var("select GroupName from ".$db->groups." where id=".$paidtraffic->GroupID);
                            $VendorName = $db->get_var("select VendorName from ".$db->vendors." where id=".$paidtraffic->VendorID);
                            $domain = $db->get_row("select * from ".$db->domains." where id=".$paidtraffic->DomainID);
                            $DomainType = $db->get_var("select DomainType from ".$db->domains." where id=".$paidtraffic->DomainID);
                            
                            $UniqueClicks = $db->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".$db->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." group by ClickIp) as UniqueClicks") * 1;
                            $RawClicks = $db->get_var("select count(*) from ".$db->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id) * 1;
                            //$RawClicks -= $UniqueClicks;

                            //$RawClicks = $db->get_var("select sum(ClickCount) from (select count(ClickIp) as ClickCount from ".$db->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id." group by ClickIp) as UniqueClicks") * 1;
                            $TotalActions = $db->get_var("select count(*) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='action'") * 1;
                            $PixelFires = $db->get_var("select count(FireIp) as FireCount from ".$db->paidtraffic_pixel_fires." where PaidTrafficID=".$paidtraffic->id) * 1;
                            $BadClicks = $db->get_var("select count(ClickIp) as ClickCount from ".$db->paidtraffic_clicks." where BotName <> '' and BotName is not NULL and PaidTrafficID=".$paidtraffic->id) * 1;
                            $TotalSales = $db->get_var("select sum(UnitPrice) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
                            $TotalSalesCount = $db->get_var("select count(*) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$paidtraffic->id." and ConversionType='sales'") * 1;
                            $EasyLinkClicks = $db->get_var("select count(*) from ".$db->linkbank_clicks." where LinkBankID=".$paidtraffic->EasyLinkID." and ClickType='sa'") * 1;
                            $NetProfit = $TotalSales - $TrafficCost;

                            //$PercentOptins = $RawClicks>0?(($TotalActions/$RawClicks) * 100):0;
                            $PercentOptins = $UniqueClicks>0?(($TotalActions/$UniqueClicks) * 100):0;
                            $CPC = $RawClicks==0?0:$TrafficCost/$RawClicks;
                            $CPA = $TotalActions==0?0:$TrafficCost/$TotalActions;
                            $CPS = $TotalSales==0?0:$TrafficCost/$TotalSales;
                            $EPC = $RawClicks==0?0:$TotalSales/$RawClicks;
                            $PercentSales = $EasyLinkClicks>0?(($TotalSales/$EasyLinkClicks) * 100):0;

                            $paidtrafficurl = get_paidtrafficurl($paidtraffic);

                            $PaidTrafficStatus = $paidtraffic->PaidTrafficStatus;
                            if($PaidTrafficStatus == "active"){
                                $paidtrafficstatustext = "Live";
                                $paidtrafficstatusicon = "fa fa-rocket";
                            }else if($PaidTrafficStatus == "pending"){
                                $paidtrafficstatustext = "Pending";
                                $paidtrafficstatusicon = "fa fa-hourglass-o";
                            }else if($PaidTrafficStatus == "complete"){
                                $paidtrafficstatus = "Completed";
                                $paidtrafficstatusicon = "fa fa-check-square-o";
                            }

                            $namelinkedurl = get_site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id);
                            $StatusIcon = get_domain_status_icon($paidtraffic->DomainID);

                            $MasterCampaignName = "";
                            if($paidtraffic->MasterCampaignID != "0")
                                $MasterCampaignName = $db->get_var("select MasterCampaignName from ".$db->master_campaigns." where id=".$paidtraffic->MasterCampaignID);

                            $TrafficQuality = get_traffic_quallity($paidtraffic, $current_user);
                            $LeadScore = get_lead_score($paidtraffic, $user);
                            $TimeDelivered = get_time_delivered($paidtraffic, $current_user);
                            $SaleQuality = get_sale_quality($paidtraffic, $current_user);
                            $OverallProfitability = get_overall_profitability($paidtraffic, $current_user);
                            $OverallDeliverability = get_overall_deliverability($paidtraffic, $current_user);

                            //$TrafficQuality = $TrafficQuality > 100? 100:$TrafficQuality;
                            //$LeadScore = $LeadScore > 100? 100:$LeadScore;
                            //$TimeDelivered = $TimeDelivered > 100? 100:$TimeDelivered;
                            $SaleQuality = $SaleQuality > 100? 100:$SaleQuality;
                            $OverallProfitability = $OverallProfitability > 100? 100:$OverallProfitability;
                            //$OverallDeliverability = $OverallDeliverability > 100? 100:$OverallDeliverability;
                            
                            $VendorOverallScore = get_vendor_overall_score($paidtraffic, $current_user, $TrafficQuality, $TimeDelivered, $LeadScore, $OverallDeliverability);
                            //$VendorOverallScore = $VendorOverallScore > 100? 100:$VendorOverallScore;
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $StatusIcon;?></td>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <td class="text-center grey-scale"><a href="javascript:" data-url="<?php site_url($curpage."?act=paidtraffic_details&PaidTrafficID=".$paidtraffic->id);?>" data-toggle="DetailRow" data-colspan-="8" id="" class="btn btn-rotator-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <?php } ?>
                            <td>
                                <a href="<?php echo $namelinkedurl; ?>" class="pull-left qtip2" data-qtip-image="<?php echo PREVIEW_URL.$paidtrafficurl;?>"><?php echo $paidtraffic->PaidTrafficName?></a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id);?>" class="btn btn-sm btn-links" title="Edit PaidTraffic"><i class="fa fa-pencil-square"></i>Edit Paid Traffic</a></li>
                                        <li><a href="<?php echo $paidtrafficurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i>Direct Link</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=delete_paidtraffic&PaidTrafficID=".$paidtraffic->id);?>" class="btn btn-sm btn-links btn-delete" title="Delete Paid Traffic"><i class="fa fa-trash"></i>Delete Paid Traffic</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=reset_paidtraffic&PaidTrafficID=".$paidtraffic->id);?>" class="btn btn-sm btn-links btn-reset" title="Reset Paid Traffic"><i class="fa fa-ban"></i>Reset Paid Traffic</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=clone_paidtraffic&PaidTrafficID=".$paidtraffic->id);?>" class="btn btn-sm btn-links" title="Clone Paid Traffic"><i class="fa fa-clone"></i>Clone Paid Traffic</a></li>
                                        <li><a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtrafficurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $paidtrafficurl;?>"><i class="fa fa-clipboard"></i>Copy Link</a></li>
                                        <li><a href="<?php site_url($pageurl."/".$paidtraffic->id."/details");?>" class="btn btn-sm btn-links" title="Statistics"><i class="fa fa-globe"></i>Statistics</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo $VendorName;?></span></td>
                            <td><span><?php echo $GroupName;?></span></td>
                            <td><span><?php echo $MasterCampaignName;?></span></td>
                            <td class="text-center stats-column"><span><?php echo get_formated_number($RawClicks);?></span></td>
                            <td class="text-center stats-column"><span><?php echo get_formated_number($UniqueClicks);?></span></td>
                            <td class="text-center stats-column"><span><?php echo get_formated_number($BadClicks);?></span></td>
                            <td class="text-center action-column"><span><?php echo get_formated_number($TotalActions);?></span></td>
                            <td class="text-center action-column"><span><?php echo get_formated_number(round($PercentOptins, 2));?>%</span></td>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <td class="text-center sale-column"><span><?php echo get_formated_number($TotalSalesCount);?></span></td>
                            <td class="text-center sale-column"><span>$<?php echo get_formated_number($TotalSales);?></span></td>
                            <td class="text-center sale-column"><span><?php echo get_formated_number(round($PercentSales, 2));?>%</span></td>
                            <?php } ?>
                            <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPC, 2));?></span></td>
                            <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPA, 2));?></span></td>
                            <?php if($paidtraffic_sale_type == "conv") { ?>
                            <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($CPS, 2));?></span></td>
                            <td class="text-center cost-column"><span>$<?php echo get_formated_number(round($EPC, 2));?></span></td>
                            <td class="text-center cost-column"><span>$<?php echo get_formated_number($NetProfit);?></span></td>
                            <td class="text-center non-score-column">
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($SaleQuality));?>" data-toggle="qtiptooltip" title="Sale Quality"><span><?php echo get_formated_number(round($SaleQuality));?></span></div>
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallProfitability));?>" data-toggle="qtiptooltip" title="Overall Profitability"><span><?php echo get_formated_number(round($OverallProfitability));?></span></div>
                            </td>
                            <?php } ?>
                            <td class="text-center score-column">
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TrafficQuality));?>" data-toggle="qtiptooltip" title="Traffic Quality"><span><?php echo get_formated_number(round($TrafficQuality));?></span></div>
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($TimeDelivered));?>" data-toggle="qtiptooltip" title="Time Delivered"><span><?php echo get_formated_number(round($TimeDelivered));?></span></div>
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($LeadScore));?>" data-toggle="qtiptooltip" title="Lead Score"><span><?php echo get_formated_number(round($LeadScore));?></span></div>
                                <div class="vendor-stats-box-sm <?php echo get_score_class_name(round($OverallDeliverability));?>" data-toggle="qtiptooltip" title="Overall Deliverability"><span><?php echo get_formated_number(round($OverallDeliverability));?></span></div>
                            </td>
                            <td class="text-center score-column-">
                                <div class="vendor-stats-box-sm" data-toggle="qtiptooltip" title="Overall Score"><?php echo get_formated_number(round($VendorOverallScore));?>%</div>
                            </td>
                            <td class="text-center grey-scale">
                                <a href="javascript:" class="status_icon" id="" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click"><i class="fa <?php echo $paidtrafficstatusicon;?>"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <form class="form-inline status_form" role="form" method="get" action="<?php site_url($curpage.(!empty($_GET['paidtraffic_type'])?"/".$_GET['paidtraffic_type']."/":""));?>">
                                        <div class="form-group select-xs">
                                            <select name="status" id="PaidTrafficStatus" class="form-control select2">
                                                <?php
                                                $link_statuses = array("pending" => "Pending", "active" => "Active", "complete" => "Completed");
                                                foreach($link_statuses as $key => $value){
                                                ?>
                                                <option value="<?php echo $key;?>" <?php if($paidtraffic->PaidTrafficStatus == $key || (isset($_POST['PaidTrafficStatus']) && $_POST['PaidTrafficStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-xs btn-default">Change</button>
                                        <input type="hidden" name="act" value="changepaidtrafficstatus" />
                                        <input type="hidden" name="PaidTrafficID" value="<?php echo $paidtraffic->id;?>" />
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include_once "member_footer.php";
?>