<?php
$pagetitle = "Link Sequence Stats";
$pageurl = "link-sequence/stats";

if(!isset($_GET["LinkSequenceID"]))
    site_redirect("link-sequence/stats");
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

$sql = "select * from ".$db->link_sequences." where id=".$_GET["LinkSequenceID"]." and userid=".$current_user->id;
$link_sequence = $db->get_row($sql);
if(!$link_sequence)
    site_redirect("link-sequence/stats");

$GroupName = $db->get_var("select GroupName from ".$db->groups." where id=".$link_sequence->GroupID);
$domain = $db->get_row("select * from ".$db->domains." where id=".$link_sequence->DomainID);
$link_sequenceurl = get_link_sequenceurl($link_sequence);

$LinkSequenceID = $_GET["LinkSequenceID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$link_sequence_stats = get_link_sequences_stats($LinkSequenceID, $startdate, $enddate);

$topchartdata = LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$item = get_menuitem($pageurl);

$GroupName = $db->get_var("select GroupName from ".$db->groups." where id=".$link_sequence->GroupID);
$link_sequence_link = get_link_sequenceurl($link_sequence);

$sql = "select ".$db->link_sequence_clicks.".ClickIp from ".$db->link_sequence_clicks." inner join ".$db->link_sequence_links." on ".$db->link_sequence_clicks.".LinkSequenceLinkID = ".$db->link_sequence_links.".id where ".$db->link_sequence_links.".LinkSequenceID=".$link_sequence->id." ";

if(isset($_GET['startdate']) && isset($_GET['enddate']))
    $sql .= " and (".$db->link_sequence_clicks.".DateAdded >= '".strtotime($_GET['startdate']." 00:00:00")."' and ".$db->link_sequence_clicks.".DateAdded <= '".strtotime($_GET['enddate']." 24:00:00")."') ";

if(isset($_GET['LinkSequenceLinkID']) && !empty($_GET['LinkSequenceLinkID']))
    $sql .= " and ".$db->link_sequence_clicks.".LinkSequenceLinkID = ".$_GET['LinkSequenceLinkID']." ";

$IpAddresses = $db->get_col($sql);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li><a href="<?php site_url("link-sequence/".$link_sequence->id."/")?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $link_sequence->LinkSequenceName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Sequence Tracking Stats Details</h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Link Sequence Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("link-sequence/".$link_sequence->id)?>"><?php echo $link_sequence->LinkSequenceName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $link_sequence->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $link_sequence->LinkSequenceType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $link_sequence_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $link_sequence_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $link_sequence_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="link_sequence_statslinks pull-left">
                        <select name="LinkSequenceLinkID" id="LinkSequenceLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".$db->link_sequence_links." where LinkSequenceID=".$LinkSequenceID." and userid = ".$current_user->id." order by LinkSequenceLinkPosition";
                            $link_sequence_links = $db->get_results($sql);
                            foreach($link_sequence_links as $link_sequence_link){
                                if ($link_sequence_link->LinkSequenceLinkType == "customurl")
                                	$LinkSequenceLinkURL = $link_sequence_link->LinkSequenceLinkURL;
                                else
                                    $LinkSequenceLinkURL = get_linkbankurl($link_sequence_link->LinkBankID);
                            ?>
                            <option value="<?php echo $link_sequence_link->id;?>" <?php if(isset($_GET['LinkSequenceLinkID']) && $_GET['LinkSequenceLinkID'] == $link_sequence_link->id) echo ' selected = "selected" ';?>><?php echo $LinkSequenceLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="link_sequence_statsdaterange" class="link_sequence_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/details/?".(isset($_GET['LinkSequenceLinkID'])?"LinkSequenceLinkID=".$_GET['LinkSequenceLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Sequence Tracking Stats Map</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body map-box-body-">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="map_canvas" class="embed-responsive-item" style="border: 1px solid #f4f4f4" data-map-image="<?php site_url('images/map_marker_inside_chartreuse.png');?>" data-map-simple="true"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<link href="<?php site_url("css/jquery-jvectormap-2.0.3.css");?>" rel="stylesheet" type="text/css" />
<script src="<?php site_url("js/jquery-jvectormap-2.0.3.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/jquery-jvectormap-world-merc-en.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/gdp-data.js");?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo "IpAddresses = ['".implode("','",  $IpAddresses)."'];\n";?>
        CreateMap($('#map_canvas'));
        AddMarker();
        CreateDateRange($("#link_sequence_statsdaterange"));

        $("#LinkSequenceLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/map/?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "LinkSequenceLinkID=" + $this.val();
            }
            window.location.href = url;
        });
    });
</script>
<?php
include_once "member_footer.php";
?>