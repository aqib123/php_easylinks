<?php
$pagetitle = "Linkbank Stats";
$pageurl = "linkbank/stats";
$modulename = "linkbank";

if(!isset($_GET["LinkBankID"]))
    site_redirect("linkbank/stats");

include_once "member_header.php";

$sql = "select * from ".$db->linkbanks." where id=".$_GET["LinkBankID"]." and userid=".$current_user->id;
$linkbank = $db->get_row($sql);
if(!$linkbank)
    site_redirect("linkbank/stats");

$GroupName = $db->get_var("select GroupName from ".$db->groups." where id=".$linkbank->GroupID);
$domain = $db->get_row("select * from ".$db->domains." where id=".$linkbank->DomainID);
$linkbankurl = get_linkbankurl($linkbank);

$LinkBankID = $_GET["LinkBankID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$linkbank_stats = get_linkbank_stats($LinkBankID, $startdate, $enddate);

//$topchartdata = LinkBank_SnapShot($LinkBankID, $linkbank_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li><a href="<?php site_url("linkbank/?LinkBankID=".$linkbank->id)?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $linkbank->LinkName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank/stats")?>" role="button">Linkbank Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-linkbank-conv" href="<?php site_url("linkbank/stats/conv")?>" role="button">Linkbank Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Tracking Stats Details</h1>
            </div>
        </div>
        <br />
        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("linkbank/stats/".$linkbank->id."/details")?>"><?php echo $linkbank->LinkName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $linkbank->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $linkbankurl;?>">Tracking Link: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $linkbankurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $linkbankurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                        <li>
                            <span class="small" title="<?php echo $linkbank->DestinationURL;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $linkbank->DestinationURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $linkbank->DestinationURL;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $linkbank->DestinationURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $linkbank->DestinationURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div id="linkbank_statsdaterange" class="linkbank_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("linkbank/stats/".$_GET["LinkBankID"]."/details/");?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left linkbank_statscharts">
                        <li>
                            <a href="<?php site_url("linkbank/stats/".$_GET["LinkBankID"]."/details/?act=exportlinkdetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->
                    <!--<div class="pull-left linkbank_statschart" style="width:60px; height:60px">
                        <canvas id="TopChart" class="chart_canvas" data-chart-type="" height="60" width="60" data-chart-values='<?php //echo implode("!", $topchartdata['piedata']);?>' data-chart-labels='<?php //echo implode('!', $topchartdata['labels'])?>' data-chart-datasets='<?php //echo implode('!', $topchartdata['datasets'])?>'></canvas>
                    </div>-->
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Tracking Stats Details</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-paging="true">
                    <thead>
                        <tr>
                            <th><span>Access Time</span></th>
                            <th><span>IP</span></th>
                            <th class="text-center tier-column"><span>Tier</span></th>
                            <th class="text-center country-column"><span>Country</span></th>
                            <th class="text-center browser-column"><span>Browser</span></th>
                            <th class="text-center platform-column"><span>Platform</span></th>
                            <th class="text-center type-column"><span>Type</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->linkbank_clicks." where LinkBankID=".$linkbank->id;

                        if(isset($_GET['startdate']) && isset($_GET['enddate']))
                            $sql .= " and (DateAdded >= '".strtotime($_GET['startdate'])."' and DateAdded <= '".strtotime($_GET['enddate'])."') ";
                        
                        $linkbank_clicks = $db->get_results($sql);
                        foreach($linkbank_clicks as $linkbank_click){
                            $GroupName = $db->get_var("select GroupName from ".$db->groups." where id=".$linkbank->GroupID);
                            $VendorName = $db->get_var("select VendorName from ".$db->vendors." where id=".$linkbank->VendorID);

                            $countrycode = $linkbank_click->CountryCode;
                            if($countrycode == "") {
                                $countrycode = "unknown";
                                $countryname = "Unknown";
                                $countrytier = 3;
                            }else{
                                $country = $db->get_row("select * from ".$db->countries." where countrycode='".$countrycode."'");
                                $countryname = $country->countryname;
                                $countrytier = $country->countrytier;
                                $countrycode = strtolower($countrycode);
                            }

                            switch($linkbank_click->BrowserName){
                                case 'Android Safari':
                                    $ub = "mobilesafari";
                                    break;
                                case 'Apple Safari':
                                    $ub = "safari";
                                    break;
                                case 'Microsoft Edge';
                                    $ub = "edge";
                                    break;
                                case 'Internet Explorer';
                                    $ub = "msie";
                                    break;
                                case 'Mozilla Firefox';
                                    $ub = "firefox";
                                    break;
                                case 'Google Chrome';
                                    $ub = "chrome";
                                    break;
                                case 'Opera';
                                    $ub = "opera";
                                    break;
                                case 'Netscape';
                                    $ub = "netscape";
                                    break;
                                default:
                                    $ub = "unknown";
                            }

                            if($ub == "unknown")
                                $browsername = "Unknown";
                            else
                                $browsername = $linkbank_click->BrowserName . " " . $linkbank_click->BrowserVersion;

                            $platform = $linkbank_click->Platform;
                            if(strpos($platform, "indows") > 0)
                                $os = "windows";
                            else if($platform == ""){
                                $platform = "Unknown";
                                $os = "unknown";
                            }else
                                $os = strtolower($linkbank_click->Platform);

                            $linkbank_clicktype = "nonuniqueclick";
                            $clicktype = "Non-Unique";
                            $typeclass = "fa fa-users";
                            if($linkbank_click->BotName != ""){
                                $linkbank_clicktype = "botclick";
                                $typeclass = "fa fa-bug";
                                $clicktype = "Bot";
                            }else{
                                $linkbank_clickscount = ($db->get_var("select count(*) from ".$db->linkbank_clicks." where ClickIp='".$linkbank_click->ClickIp."' and LinkBankID=".$linkbank->id)) * 1;
                                if($linkbank_clickscount == 1){
                                    $linkbank_clicktype = "uniqueclick";
                                    $typeclass = "fa fa-user";
                                    $clicktype = "Unique";
                                }
                            }
                            $flag = file_exists(BASE_DIR."/images/flags/".strtolower($countrycode).".png")?$countrycode:"noflag";
                        ?>
                        <tr>
                            <td><span><?php echo date("m/d/Y h:i:s A", $linkbank_click->DateAdded)?></span></td>
                            <td><span><?php echo $linkbank_click->ClickIp;?></span></td>
                            <td class="text-center"><span><?php echo $countrytier;?></span></td>
                            <td class="text-center"><a class="" target="_blank" href="https://maps.google.com/?q=<?php echo $countryname;?>" data-toggle="qtiptooltip" title="<?php echo $countryname;?>">
                                <img class="icon16xwidth" src="<?php site_url("images/flags/".$flag.".png")?>" title="<?php echo $countryname?>" /></a>
                                <span class="hidden"><?php echo $countryname;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$ub.".png")?>" data-toggle="qtiptooltip" title="<?php echo $browsername?>" />
                                <span class="hidden"><?php echo $browsername;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$os.".png")?>" data-toggle="qtiptooltip" title="<?php echo $platform;?>" />
                                <span class="hidden"><?php echo $platform;?></span>
                            </td>
                            <td class="text-center">
                                <span class="<?php echo $linkbank_clicktype;?>" data-toggle="qtiptooltip" title="<?php echo $clicktype;?>"><i class="<?php echo $typeclass;?>"></i></span>
                                <span class="hidden"><?php echo $clicktype;?></span>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#linkbank_statsdaterange"));

        //CreatePieChart($('#TopChart'));
    });
</script>
<?php
include_once "member_footer.php";
?>