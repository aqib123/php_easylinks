<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['DomainID'])){
    $DomainID = parseInt($_GET['DomainID']);
    $sql = "select * from ".$db->domains." where userid = ".$current_user->id." and id=".$_GET['DomainID'];
    $domain = $db->get_row($sql);
    if($domain)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form domain-form">
        <div class="form-group has-feedback">
            <label for="DomainName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $domain->DomainName;?>" class="form-control " id="DomainName" name="DomainName" placeholder="Domain Name" required="required" <?php echo NAME_PATTERN;?>  />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback" <?php if($_GET["DomainType"] == "customdomain") echo ' style="display:none;" ';?>>
            <label for="DomainSlug" class="col-sm-4 control-label">Slug</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $domain->DomainSlug;?>" class="form-control cname_update" id="DomainSlug" name="DomainSlug" placeholder="Domain Name" data-remote="<?php site_url("domain/?act=check_domain_slug&id=".(isset($_GET['DomainID'])?$_GET['DomainID']:"0"))?>" data-remote-error="Domain Slug already exist" required="required" <?php echo ALPHANUMERIC_PATTERN;?>  />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>
        
        <div class="form-group has-feedback">
            <label for="DomainUrl" class="col-sm-4 control-label">URL</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $domain->DomainUrl;?>" class="form-control cname_update" id="DomainUrl" name="DomainUrl" placeholder="Domain URL" required="required" <?php echo URL_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback cname_panel" <?php if($_GET["DomainType"] == "customdomain") echo ' style="display:none;" ';?>>
            <label for="CNameURL" class="col-sm-4 control-label">CName URL</label>
            <div class="col-sm-8">
                <input type="text" value="" class="form-control cname_update" readonly="readonly" id="CNameURL" name="CNameURL" placeholder="CName URL" />
            </div>
        </div>

        <div class="form-group has-feedback" style="display: none;">
            <label for="DomainUrl" class="col-sm-4 control-label">Is Default Cloak</label>
            <div class="col-sm-8">
                <input type="checkbox" name="DefaultCloakURL" <?php if($isUpdateForm && $domain->DefaultCloakURL == 1) echo " checked='checked' "?> class="blue" />
            </div>
        </div>

        <?php if($_GET["DomainType"] == "userdomain"){?>
        <div class="form-group has-feedback">
            <label for="DomainUrl" class="col-sm-4 control-label">Is Forward Domain</label>
            <div class="col-sm-8">
                <input type="checkbox" name="DomainForward" id="DomainForward" <?php if($isUpdateForm && $domain->DomainForward == 1) echo " checked='checked' "?> class="blue" data-ifChanged="true" />
            </div>
        </div>
        <?php } ?>

        <input type="hidden" name="act" value="save_domain" />
    </form>
</div>
<script>
    function CreateCNameURL() {
        var IsForwardDomain = true;//$('#DomainForward').is(":checked");
        $("#CNameURL").val("");
        //console.log("CreateCNameURL");
        if (IsForwardDomain) {
            //$(".cname_panel").show();
            if ($("#DomainSlug").val() != "" && $("#DomainUrl").val() != "" && $(".domain-form").find(".has-error").length <= 0) {
                var DomainSlug = $("#DomainSlug").val();
                var DomainUrl = $("#DomainUrl").val();
                var CNameURL = username.toLowerCase() + "-" + DomainSlug.toLowerCase() + ".easylinks.online";
                $("#CNameURL").val(CNameURL);
            }
        } else {
            //$(".cname_panel").hide();
        }
    }

    $(document).on("change", ".cname_update", CreateCNameURL);
    $(document).on("keyup", ".cname_update", CreateCNameURL);
    $('#DomainForward').on('ifChanged', CreateCNameURL);
    CreateCNameURL();
</script>
<?php include_once "member_popup_footer.php"; ?>