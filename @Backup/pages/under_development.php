<?php
global $db, $current_user;
$pagetitle = "Under Development";
$pageurl = "";
$modulename = "dashboard";

include_once "member_header.php";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a><i class=""></i><?php echo $pagetitle;?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <br />
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h2>UNDER DEVELOPMENT, COMING SOON</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>