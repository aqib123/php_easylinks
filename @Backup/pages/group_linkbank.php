<?php
global $db, $current_user;
$pagetitle = "Vendors";
$pageurl = "group/linkbank";
$modulename = "linkbank";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn-gray" href="<?php site_url("group/linkbank/")?>" role="button">Link Bank</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/paidtraffics/")?>" role="button">Paid Traffic</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/email/")?>" role="button">Email Campaigns</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/rotator/")?>" role="button">Rotator Groups</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/link-sequence/")?>" role="button">Link Sequence</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/pixels/")?>" role="button">Retargeting Pixels</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/holding-pages/")?>" role="button">Holding Pages</a>
            </div>
        </div><hr />

        <div class="row">
            <div class="col-sm-12">
                <h2>Link Bank Groups</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Vendor</label>
                                            <div class="col-xs-12 ">
                                                <select name="VendorID" id="LinkID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->vendors." where VendorType='Affiliate' and userid = ".$current_user->id." order by VendorName";
                                                    $vendors = $db->get_results($sql);
                                                    foreach($vendors as $vendor){
                                                    ?>
                                                    <option value="<?php echo $vendor->id;?>"><?php echo $vendor->VendorName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 btn-group" style="margin-top: 12px" role="group" aria-label="...">
                                                <a href="<?php site_url("vendor/?js=true&nodefault=true&VendorType=Affiliate")?>" data-widget="ShowLinkModel" data-related="#LinkID" data-method="post" data-page-title="Vendor" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("vendor/?js=true&nodefault=true&VendorType=Affiliate&VendorID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#LinkID" data-method="post" data-page-title="Vendor" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("vendor/?js=true&nodefault=true&VendorType=Affiliate")?>" data-values="act=delete_vendor&VendorID=" data-append="true" data-widget="RemoveOption" data-related="#LinkID" data-method="post" data-page-title="Vendor" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Group Type</label>
                                            <div class="col-xs-12 ">
                                                <select name="GroupID" id="LinkBankGroupID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->groups." where GroupType='LinkBank' and userid = ".$current_user->id." order by GroupName";
                                                    $groups = $db->get_results($sql);
                                                    foreach($groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkBank")?>" data-widget="ShowLinkModel" data-related="#LinkBankGroupID" data-method="post" data-page-title="Group Type" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkBank&GroupID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#LinkBankGroupID" data-method="post" data-page-title="Group Type" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkBank")?>" data-values="act=delete_group&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#LinkBankGroupID" data-method="post" data-page-title="Group Type" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Split Partner</label>
                                            <div class="col-xs-12 ">
                                                <select name="SplitPartnerID" id="SplitPartnerID" class="form-control select2" style="width: 99%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->split_partners." where userid = ".$current_user->id." order by PartnerName";
                                                    $split_partners = $db->get_results($sql);
                                                    foreach($split_partners as $split_partner){
                                                    ?>
                                                    <option value="<?php echo $split_partner->id;?>"><?php echo $split_partner->PartnerName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("split-partner/?js=true&nodefault=true")?>" data-widget="ShowLinkModel" data-related="#SplitPartnerID" data-method="post" data-page-title="Split Partner" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("split-partner/?js=true&nodefault=true&PartnerID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#SplitPartnerID" data-method="post" data-page-title="Split Partner" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("split-partner/?js=true&nodefault=true")?>" data-values="act=delete_split_partner&PartnerID=" data-append="true" data-widget="RemoveOption" data-related="#SplitPartnerID" data-method="post" data-page-title="Split Partner" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Link Platform</label>
                                            <div class="col-xs-12 ">
                                                <select name="GroupID" id="AffiliatePlatformID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".$db->groups." where GroupType='AffiliatePlatform' and userid = ".$current_user->id." order by GroupName";
                                                    $groups = $db->get_results($sql);
                                                    foreach($groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=AffiliatePlatform")?>" data-widget="ShowLinkModel" data-related="#AffiliatePlatformID" data-method="post" data-page-title="Platform" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=AffiliatePlatform&GroupID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#AffiliatePlatformID" data-method="post" data-page-title="Platform" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=AffiliatePlatform")?>" data-values="act=delete_group&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#AffiliatePlatformID" data-method="post" data-page-title="Platform" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>