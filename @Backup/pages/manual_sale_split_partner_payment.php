<?php
$modulename = "manual-sale";
include_once "member_popup_header.php";

$isUpdateForm = false;

if(empty($_GET["ManualSaleID"]) || empty($_GET["SenderID"]) || empty($_GET["ReceiverID"]) || !is_numeric($_GET["SentAmount"])) die;

$ManualSale = $db->get_row("select * from ".$db->manualsales." where userid=".$current_user->id." and id=".$_GET["ManualSaleID"]);
if(!$ManualSale) die;

$PartnerPayment = $db->get_row("select * from ".$db->manualsale_split_partner_payments." where userid=".$current_user->id." and SenderManualSaleSplitPartnerID=".$_GET["SenderID"]." and ReceiverManualSaleSplitPartnerID=".$_GET["ReceiverID"]);
if(!$PartnerPayment)
    $isUpdateForm = true;;

?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form" role="form" id="saleinfoform">
        <div class="form-group has-feedback">
            <label for="ReferenceNo" class="col-sm-3 control-label">Reference No</label>
            <div class="col-sm-8">
                <div class="input-group left-addon colored">
                    <div class="input-group-addon">#</div>
                    <input type="text" value="<?php if($isUpdateForm) echo $PartnerPayment->ReferenceNo;?>" class="form-control " id="ReferenceNo" name="ReferenceNo" placeholder="Reference No" required="required" <?php echo NAME_PATTERN;?> />
                </div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>
        <input type="hidden" name="act" value="save_split_partner_payment" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>