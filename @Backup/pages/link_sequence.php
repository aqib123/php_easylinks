<?php
global $db, $current_user;
$pagetitle = "Link Sequence Series";
$pageurl = "link-sequence";
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

//$item = array_filter($menu_items, function($item){return($item['MenuItemURL'] == "linkbank");})[0];
$item = get_menuitem($pageurl);

$isUpdateForm = false;
$AvailableDomainID = 0;
if($_GET && !empty($_GET['LinkSequenceID']) && !isset($errors["msg"])){
    $LinkSequenceID = parseInt($_GET['LinkSequenceID']);
    $sql = "select * from ".$db->link_sequences." where id=".$LinkSequenceID." and userid=".$current_user->id;
    $link_sequence = $db->get_row($sql);
    if($link_sequence){
        $isUpdateForm = true;

        if($link_sequence->UseAdminDomain == 1)
            $AvailableDomainID = $link_sequence->DomainID;
    }
}
$admindomain = get_available_domain($AvailableDomainID);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a href=""><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-<?php echo !isset($_GET["SingleLink"])?"gray":"bordered"?>" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2>Link Link Sequence</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-7 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("link-sequence/".(isset($_GET['LinkSequenceID'])?"?LinkSequenceID=".$_GET['LinkSequenceID']."":"").(isset($_GET["SingleLink"])?"?SingleLink=true":""))?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="LinkSequenceName" class="col-sm-4 control-label">Link Sequence Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $link_sequence->LinkSequenceName; elseif(isset($_POST['LinkSequenceName'])) echo $_POST['LinkSequenceName'];?>" class="form-control " id="LinkSequenceName" name="LinkSequenceName" placeholder="Link Sequence Name" data-remote="<?php site_url("link-sequence/?act=check_link_name&type=link_sequence&id=".(isset($_GET['LinkSequenceID']) && !empty($_GET['LinkSequenceID'])?$_GET['LinkSequenceID']:"0"))?>" data-remote-error="Link Sequence Name already exist" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="LinkSequenceStatus" class="col-sm-4 control-label">Choose Status</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group" style="width: 100%">
                                                    <select name="LinkSequenceStatus" id="LinkSequenceStatus" class="form-control select2" style="width: 100%" data-select="select">
                                                        <?php
                                                        $link_statuses = array("active" => "Active", "complete" => "Completed");
                                                        foreach($link_statuses as $key => $value){
                                                        ?>
                                                        <option value="<?php echo $key;?>" <?php if(($isUpdateForm && $link_sequence->LinkSequenceStatus == $key) || (isset($_POST['LinkSequenceStatus']) && $_POST['LinkSequenceStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-4 control-label">Choose Group</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="GroupID" id="GroupID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Group</option>
                                                        <?php
                                                        $sql = "select * from ".$db->groups." where GroupType='LinkSequence' and userid = ".$current_user->id." order by GroupName";
                                                        $groups = $db->get_results($sql);
                                                        foreach($groups as $group){
                                                        ?>
                                                        <option value="<?php echo $group->id;?>" <?php if(($isUpdateForm && $link_sequence->GroupID == $group->id) || (isset($_POST['GroupID']) && $_POST['GroupID'] == $group->id)) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=LinkSequence")?>" data-widget="ShowLinkModel" data-related="#GroupID" data-method="post" data-page-title="Link Sequence Group Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="DomainID" class="col-sm-4 col-xs-12 control-label">Choose Domain</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <?php
                                                $checked = "";
                                                $AdminDomainID = "";
                                                $dropdowndisable = "";
                                                if(($isUpdateForm && $link_sequence->UseAdminDomain == 1) || (isset($_POST['UseAdminDomain']))){
                                                    $checked = ' checked = "checked" ';
                                                    $AdminDomainID = $link_sequence->DomainID;
                                                    $dropdowndisable = ' disabled = "disabled" ';
                                                }else{
                                                    $dropdowndisable = ' data-select="select" ';
                                                }

                                                ?>
                                                <div class="input-group right-addon">
                                                    <select name="DomainID" id="DomainID" class="form-control select2" style="width: 99%" <?php echo $dropdowndisable;?>>
                                                        <option value="">Select Domain</option>
                                                        <?php
                                                        $sql = "select * from ".$db->domains." where userid = ".$current_user->id." and DomainType='userdomain' order by DomainName";
                                                        $domains = $db->get_results($sql);
                                                        $domainurls = array();
                                                        foreach($domains as $domain){
                                                            $domainurls[] = '"'.$domain->id.'": {name:"'.$domain->DomainName.'", forward:"'.$domain->DomainForward.'", url:"'.$domain->DomainUrl.'", type:"'.$domain->DomainType.'"}';
                                                        ?>
                                                        <option value="<?php echo $domain->id;?>" <?php if(($isUpdateForm && $link_sequence->DomainID == $domain->id) || (isset($_POST['DomainID']) && $_POST['DomainID'] == $domain->id)) echo ' selected = "selected" ';?>><?php echo $domain->DomainName;?></option>
                                                        <?php
                                                        }
                                                        $str_domains = 'var domains = {'.implode(',', $domainurls).'};';
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("domain/?js=true&DomainType=userdomain")?>" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="Domains Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="hidden" name="AdminDomainID" id="AdminDomainID" value="<?php echo $AdminDomainID;?>" />
                                                    <input type="checkbox" name="UseAdminDomain" id="UseAdminDomain" class="blue " value="1" <?php echo $checked;?> />&nbsp;
                                                    <span>Use Admin Domain</span>&nbsp;&nbsp;
                                                </div>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="checkbox" name="CloakURL" id="CloakURL" class="blue " value="1" <?php if(($isUpdateForm && $link_sequence->CloakURL == 1) || (isset($_POST['CloakURL']))) echo "checked='checked'";?> />&nbsp;
                                                    <span>Cloak URL</span>&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="VisibleLink" class="col-sm-4 control-label">Visible Link</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $link_sequence->VisibleLink; else if(isset($_POST['VisibleLink'])) echo $_POST['VisibleLink'];?>" class="form-control " id="VisibleLink" name="VisibleLink" placeholder="Visible Link" data-remote="<?php site_url("linkbank/")?>" data-remote-values="getVisibleLinkValues()" data-remote-error="Visible Link for selected Domain already exist" required="required" <?php echo VISIBLELINK_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id="linkurl"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Master Campaign</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="MasterCampaignID" id="MasterCampaignID" class="form-control select2" style="width: 99%">
                                                        <option value="0">Select Master Campaign</option>
                                                        <?php
                                                        $sql = "select * from ".$db->master_campaigns." where MasterCampaignStatus='active' and userid = ".$current_user->id." order by MasterCampaignName";
                                                        $MasterCampaigns = $db->get_results($sql);
                                                        foreach($MasterCampaigns as $MasterCampaign){
                                                        ?>
                                                        <option value="<?php echo $MasterCampaign->id;?>" <?php if(($isUpdateForm && $link_sequence->MasterCampaignID == $MasterCampaign->id) || (isset($_POST['MasterCampaignID']) && $_POST['MasterCampaignID'] == $MasterCampaign->id)) echo ' selected = "selected" ';?>><?php echo $MasterCampaign->MasterCampaignName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("master-campaign-add/?js=true")?>" data-widget="ShowLinkModel" data-related="#MasterCampaignID" data-method="post" data-page-title="Master Campaign" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputLink3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("link-sequence/stats")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_link_sequence" />
                            </form>
                        </div>

                        <div class="col-md-5 col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking link? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">How to create a new tracking link</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking link articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var CurrentVisibleLink = "<?php echo $isUpdateForm?$link_sequence->VisibleLink:"";?>";
    var CurrentID = "<?php echo isset($_GET["LinkSequenceID"])?$_GET["LinkSequenceID"]:"0";?>";
</script>

<script src="<?php site_url("js/link_sequence.js");?>" type="text/javascript"></script>
<?php
echo '<script type="text/javascript">'.
$str_domains.
' var userid = "'.$current_user->id.'";'.
' var admindomain = '.$admindomain.';'.
'</script>';
include_once "member_footer.php";
?>