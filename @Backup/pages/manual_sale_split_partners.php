<?php
global $db, $current_user;

if(!isset($_GET["MasterCampaignID"]) && !isset($_GET["LinkBankID"]))
    return;

$pagetitle = "Manual Sale";
$pageurl = "manual-sale/split-partners/?".(isset($_GET["MasterCampaignID"])?"MasterCampaignID=".$_GET["MasterCampaignID"]:"LinkBankID=".$_GET["LinkBankID"]);
$prevpageurl = "manual-sale/?".(isset($_GET["MasterCampaignID"])?"MasterCampaignID=".$_GET["MasterCampaignID"]:"LinkBankID=".$_GET["LinkBankID"]);
$modulename = "manual-sale";

include_once "member_header.php";

$MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
$LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;

$mastercampaign = $db->get_row("select * from ".$db->master_campaigns." where userid=".$current_user->id." and id=".$MasterCampaignID);
$linkbank = $db->get_row("select * from ".$db->linkbanks." where userid=".$current_user->id." and id=".$LinkBankID);

$ManualSale = get_manual_sale(0, $MasterCampaignID, $LinkBankID);

$ManualSaleResults = get_manual_sale_result($ManualSale);

if(!is_percentage_valid($ManualSale)){
    $errors = array(
        'success' => false,
        "nohidealert" => true,
        "msg" => "Both Income and Expense Split Ratios Must Add Up To 100(%) Each. Before You Can View Your Reports!"
    );

    $TotalIncome = 0;
    $TotalOtherIncome = 0;
    $TotalExpense = 0;
}else{
    $TotalIncome = $db->get_var("select sum(".$db->manualsale_incomes.".IncomeAmount) from ".$db->manualsale_incomes." inner join ".$db->manualsale_split_partners." on ".$db->manualsale_incomes.".ManualSaleSplitPartnerID = ".$db->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
    $TotalOtherIncome = $db->get_var("select sum(".$db->manualsale_other_incomes.".OtherIncomeAmount) from ".$db->manualsale_other_incomes." inner join ".$db->manualsale_split_partners." on ".$db->manualsale_other_incomes.".ManualSaleSplitPartnerID = ".$db->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
    $TotalExpense = $db->get_var("select sum(".$db->manualsale_expenses.".ExpenseAmount) from ".$db->manualsale_expenses." inner join ".$db->manualsale_split_partners." on ".$db->manualsale_expenses.".ManualSaleSplitPartnerID = ".$db->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
}

if(isset($_GET["MasterCampaignID"])){
    $MasterCampaignDetails = get_master_campaign_details($MasterCampaignID);
    $TotalClicks = $MasterCampaignDetails["EPCLinkBankClicks"];
    $TotalCampaignClicks = $MasterCampaignDetails["TotalClicks"];
} else {
    $TotalClicks = $db->get_var("select count(*) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID) * 1;
    $TotalCampaignClicks = $TotalClicks;
}

$EPC = $TotalIncome == 0?0:$TotalClicks/$TotalIncome;
$TotalNetProfit = ($TotalIncome + $TotalOtherIncome) - $TotalExpense;
?>
<script>var emails = {};</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <label class="small"><?php echo $MasterCampaignID == 0?"Link Bank Name: &nbsp;":"Master Campaign Name: &nbsp;";?></label>
                        <a class="" href="<?php site_url($MasterCampaignID == 0?"linkbank/stats/".$linkbank->id."/details":"master-campaign/")?>"><?php echo $MasterCampaignID == 0?$linkbank->LinkName:$mastercampaign->MasterCampaignName?></a><br />

                        <label class="small">Manual Sale Name: &nbsp;</label>
                        <a class="" href="<?php site_url($prevpageurl)?>"><?php echo empty($ManualSale->ManualSaleName)?"manual sale":$ManualSale->ManualSaleName;?></a><br />

                        <span class="small">Date Created: &nbsp;</span>
                        <a href="#" class=""><?php echo date("m/d/Y", $ManualSale->DateAdded)?></a><br />
                        <br />

                        <ul class="list-inline">
                            <li>
                                <strong class="small">Total Income: </strong>
                                <span class="small TotalIncomeTop">$<?php echo get_formated_number($TotalIncome)?></span><br />
                            </li>
                            <li>
                                <strong class="small">Total Other Income: </strong>
                                <span class="small TotalOtherIncomeTop">$<?php echo get_formated_number($TotalOtherIncome)?></span><br />
                            </li>
                            <li>
                                <strong class="small">Total Expense: </strong>
                                <span class="small TotalExpenseTop">$<?php echo get_formated_number($TotalExpense)?></span><br />
                            </li>
                            <li>
                                <strong class="small">Total Campaign Clicks: </strong>
                                <span class="small"><?php echo get_formated_number($TotalCampaignClicks)?></span><br />
                            </li>
                            <?php if(isset($_GET["MasterCampaignID"])){ ?>
                            <li>
                                <strong class="small">Total Link Clicks: </strong>
                                <span class="small"><?php echo get_formated_number($TotalClicks)?></span><br />
                            </li>
                            <?php } ?>
                            <li>
                                <strong class="small">EPC: </strong>
                                <span class="small">$<?php echo get_formated_number(round($EPC, 2));?></span><br />
                            </li>
                            <li class="pull-right">
                                <strong class="">Total Net Profit: </strong>
                                <span class="">$<?php echo get_formated_number(round($TotalNetProfit, 2));?></span><br />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid box-gray">
            <div class="box-header with-border">
                <h3 class="box-title">Split Partners</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body white-body">
                <?php
                $sql = "select * from ".$db->manualsale_split_partners." where userid=".$current_user->id." and ManualSaleID=".$ManualSale->id." order by id asc";
                $split_partners = $db->get_results($sql);
                foreach ($split_partners as $index => $split_partner){
                    $split_partner_id = $split_partner->id;
                    if($split_partner->isUserSale == 0){
                        $partner = $db->get_row("select * from ".$db->split_partners." where userid=".$current_user->id." and id=".$split_partner->SplitPartnerID);
                        $image = $partner->PartnerPicture;
                    }else{
                        $image = $current_user->user_picture;
                    }
                    if(($index % 2) == 0)
                        echo '<div class="row">'."\n";
                ?>

                <div class="col-md-6 col-lg-6">
                    <div class="well manual-sale-split-partner-well">
                        <?php if($split_partner->isUserSale != 1) { ?>
                        <a href="javascript:" class="btn-sm btn-link remove-manual-sale-split-partner" data-splitpartner-id="<?php echo $split_partner->id;?>"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                        <div class="box box-solid">
                            <div class="box-body clearfix">
                                <div class="col-md-2 image partner-image">
                                    <img src="<?php site_url(!empty($image)?$image:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
                                </div>
                                <div class="col-md-10">
                                    <form class="form-horizontal manual-sale-partner" role="form" data-toggle="custom-validator">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Name:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="" placeholder="Your Name" readonly="readonly" value="<?php echo $split_partner->isUserSale == 1?$current_user->display_name:$partner->PartnerName;?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Sales Income Split:</label>
                                            <div class="col-sm-7">
                                                <div class="input-group right-addon colored">
                                                    <input type="text" class="form-control" id="IncomePercentage" name="IncomePercentage" placeholder="Income %" value="<?php echo $split_partner->IncomePercentage?>" required="required" <?php echo FLOAT_PATTERN;?>>
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Other Income Split:</label>
                                            <div class="col-sm-7">
                                                <div class="input-group right-addon colored">
                                                    <input type="text" class="form-control" id="OtherIncomePercentage" name="OtherIncomePercentage" placeholder="OtherIncome %" value="<?php echo $split_partner->OtherIncomePercentage?>" required="required" <?php echo FLOAT_PATTERN;?>>
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Expense Split:</label>
                                            <div class="col-sm-7">
                                                <div class="input-group right-addon colored">
                                                    <input type="text" class="form-control" id="ExpensePercentage" name="ExpensePercentage" placeholder="Expense %" value="<?php echo $split_partner->ExpensePercentage?>" required="required" <?php echo FLOAT_PATTERN;?>>
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <button type="button" class="btn btn-sm pull-right btn-success btn-save-partner-info">Save Info</button>
                                                <input type="hidden" name="act" value="update_manualsalesplitpartner" />
                                                <input type="hidden" name="ManualSaleSplitPartnerID" value="<?php echo $split_partner->id;?>" />
                                                <input type="hidden" name="ManualSaleID" value="<?php echo $ManualSale->id;?>" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid">
                            <div class="box-body clearfix">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#income_tab_<?php echo $split_partner_id;?>" data-toggle="tab">Sales Income</a></li>
                                        <li class=""><a href="#other_income_tab_<?php echo $split_partner_id;?>" data-toggle="tab">Other Income</a></li>
                                        <li class=""><a href="#expense_tab_<?php echo $split_partner_id;?>" data-toggle="tab">Expense</a></li>
                                        <li class=""><a href="#result_tab_<?php echo $split_partner_id;?>" data-toggle="tab">Report</a></li>
                                    </ul>
                                    <div class="tab-content" data-splitpartnerid="<?php echo $split_partner_id;?>">

                                        <div class="tab-pane active" id="income_tab_<?php echo $split_partner_id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form class="form-inline manual-sale-details" role="form" data-toggle="custom-validator">
                                                        <div class="form-group has-feedback name-form-group">
                                                            <input type="text" class="form-control" id="IncomeName" name="IncomeName" placeholder="Name" required="required" <?php echo NAME_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <div class="form-group note-form-group">
                                                            <textarea class="" id="IncomeNote" name="IncomeNote" placeholder="Enter a short note for reference" rows="3"></textarea>
                                                        </div>
                                                        <div class="form-group has-feedback amount-form-group">
                                                            <input type="text" class="form-control" id="IncomeAmount" name="IncomeAmount" placeholder="Amount" required="required"  <?php echo FLOAT_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <button type="button" class="btn btn-sm btn-income pull-right">Add Income</button>
                                                        <input type="hidden" name="act" value="add_income" />
                                                        <input type="hidden" name="ManualSaleSplitPartnerID" value="<?php echo $split_partner->id;?>" />
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scrollable-div manual-sale-container">
                                                        <table class="table table-condensed- table-bordered manual-sale-list">
                                                            <thead>
                                                                <tr>
                                                                    <th>Description</th>
                                                                    <th>Amount</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                    $sql = "select * from ".$db->manualsale_incomes." where userid=".$current_user->id." and ManualSaleSplitPartnerID=".$split_partner->id;
                    $incomes = $db->get_results($sql);
                    foreach ($incomes as $income){                                                            
                        echo get_income_row($income);
                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="other_income_tab_<?php echo $split_partner_id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form class="form-inline manual-sale-details" role="form" data-toggle="custom-validator">
                                                        <div class="form-group has-feedback name-form-group">
                                                            <input type="text" class="form-control" id="OtherIncomeName" name="OtherIncomeName" placeholder="Name" required="required" <?php echo NAME_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <div class="form-group note-form-group">
                                                            <textarea class="" id="OtherIncomeNote" name="OtherIncomeNote" placeholder="Enter a short note for reference" rows="3"></textarea>
                                                        </div>
                                                        <div class="form-group has-feedback amount-form-group">
                                                            <input type="text" class="form-control" id="OtherIncomeAmount" name="OtherIncomeAmount" placeholder="Amount" required="required"  <?php echo FLOAT_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <button type="button" class="btn btn-sm btn-other_income pull-right">Add Income</button>
                                                        <input type="hidden" name="act" value="add_other_income" />
                                                        <input type="hidden" name="ManualSaleSplitPartnerID" value="<?php echo $split_partner->id;?>" />
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scrollable-div manual-sale-container">
                                                        <table class="table table-condensed- table-bordered manual-sale-list">
                                                            <thead>
                                                                <tr>
                                                                    <th>Description</th>
                                                                    <th>Amount</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                    $sql = "select * from ".$db->manualsale_other_incomes." where userid=".$current_user->id." and ManualSaleSplitPartnerID=".$split_partner->id;
                    $other_incomes = $db->get_results($sql);
                    foreach ($other_incomes as $other_income){                                                            
                        echo get_other_income_row($other_income);
                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="expense_tab_<?php echo $split_partner_id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form class="form-inline manual-sale-details" role="form" data-toggle="custom-validator">
                                                        <div class="form-group has-feedback name-form-group">
                                                            <input type="text" class="form-control" id="ExpenseName" name="ExpenseName" placeholder="Name" required="required" <?php echo NAME_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <div class="form-group note-form-group">
                                                            <textarea class="" id="ExpenseNote" name="ExpenseNote" placeholder="Enter a short note for reference" rows="3"></textarea>
                                                        </div>
                                                        <div class="form-group has-feedback amount-form-group">
                                                            <input type="text" class="form-control" id="ExpenseAmount" name="ExpenseAmount" placeholder="Amount" required="required"  <?php echo FLOAT_PATTERN;?>>
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            <small class="help-block with-errors"></small>
                                                        </div>
                                                        <button type="button" class="btn btn-sm btn-expense pull-right">Add Expense</button>
                                                        <input type="hidden" name="act" value="add_expense" />
                                                        <input type="hidden" name="ManualSaleSplitPartnerID" value="<?php echo $split_partner->id;?>" />
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scrollable-div manual-sale-container">
                                                        <table class="table table-condensed- table-bordered manual-sale-list">
                                                            <thead>
                                                                <tr>
                                                                    <th>Description</th>
                                                                    <th>Amount</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                    $sql = "select * from ".$db->manualsale_expenses." where userid=".$current_user->id." and ManualSaleSplitPartnerID=".$split_partner->id;
                    $expenses = $db->get_results($sql);
                    foreach ($expenses as $expense){                                                            
                        echo get_expense_row($expense);
                    }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="result_tab_<?php echo $split_partner_id;?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scrollable-div manual-sale-container manual-sale-result-container">
                                                        <table class="table manual-sale-result" id="manualsale_split_partner_<?php echo $split_partner_id;?>">
                                                            <tbody>
                                                                <?php echo $ManualSaleResults["manualsale_split_partner_".$split_partner_id];?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if(($index % 2) == 1)
                    echo '</div>';
                }

                if(($index % 2) == 0)
                    echo '</div>';
                ?>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    var url = "<?php site_url($pageurl);?>";
    var ManualSaleID = "<?php echo $ManualSale->id;?>";
</script>
<script src="<?php site_url("js/manual_sale_split_partners.js");?>" type="text/javascript"></script>
<?php
include_once "member_footer.php";
?>