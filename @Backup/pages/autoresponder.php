<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['AutoresponderID'])){
    $AutoresponderID = parseInt($_GET['AutoresponderID']);
    $sql = "select * from ".$db->autoresponders." where userid = ".$current_user->id." and id=".$_GET['AutoresponderID'];
    $autoresponder = $db->get_row($sql);
    if($autoresponder)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form" data-toggle="validator" role="form">
        <div class="form-group has-feedback">
            <label for="AutoresponderName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $autoresponder->AutoresponderName;?>" class="form-control " id="AutoresponderName" name="AutoresponderName" placeholder="Autoresponder Name" required="required" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_autoresponder" />
        <input type="hidden" name="parentid" value="<?php echo $isUpdateForm?$autoresponder->parentid:(isset($_GET["ParentID"])?$_GET["ParentID"]:"0");?>"; />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>