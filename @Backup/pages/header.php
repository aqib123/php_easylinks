<?php
include_once "../includes/functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Easy Links | <?php echo $pagetitle;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link rel="apple-touch-icon" sizes="57x57" href="<?php site_url("images/favicons/apple-icon-57x57.png");?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php site_url("images/favicons/apple-icon-60x60.png");?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php site_url("images/favicons/apple-icon-72x72.png");?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php site_url("images/favicons/apple-icon-76x76.png");?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php site_url("images/favicons/apple-icon-114x114.png");?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php site_url("images/favicons/apple-icon-120x120.png");?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php site_url("images/favicons/apple-icon-144x144.png");?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php site_url("images/favicons/apple-icon-152x152.png");?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php site_url("images/favicons/apple-icon-180x180.png");?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php site_url("images/favicons/android-icon-192x192.png");?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php site_url("images/favicons/favicon-32x32.png");?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php site_url("images/favicons/favicon-96x96.png");?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php site_url("images/favicons/favicon-16x16.png");?>">
    <link rel="manifest" href="<?php site_url("images/favicons/manifest.json");?>">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php site_url("css/bootstrap.min.css");?>" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php site_url("css/bootstrap-theme.min.css");?>" />

    <!-- Font Awesome Icons -->
    <link href="<?php site_url("css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="<?php site_url("css/AdminLTE.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php site_url("css/skin-black-light.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php site_url("css/blue.css");?>" />

    <!-- Select2 -->
    <link href="<?php site_url("css/select2.min.css");?>" rel="stylesheet" />

    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/daterangepicker.css");?>" />

    <link href="<?php site_url("css/style.css");?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php site_url("js/jquery-1.11.3.min.js");?>"></script>
</head>
<body class="skin-black-light <?php if(isset($bodyclasses)) echo $bodyclasses; ?> ">

    <div id="EasyLinksModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="EasyLinksModelTitle">Modal title</h4>
                </div>
                <div class="modal-body">
                    asdf
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EasyLinksModelSave">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="wrapper">
