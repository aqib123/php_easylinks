<?php
$modulename = "paid-traffic";
include_once "member_popup_header.php";

//$isUpdateForm = false;
//if($_GET && isset($_GET['AutoresponderID'])){
//    $AutoresponderID = parseInt($_GET['AutoresponderID']);
//    $sql = "select * from ".$db->autoresponders." where userid = ".$current_user->id." and id=".$_GET['AutoresponderID'];
//    $autoresponder = $db->get_row($sql);
//    if($autoresponder)
//        $isUpdateForm = true;
//}

function get_criteria_row($goal_criteria, $goal_criteria_key = ""){
	global $db, $current_user;
    if($goal_criteria != null && is_numeric($goal_criteria))
        $goal_criteria = $db->get_row("select * from ".$db->paidtraffic_goal_criterias." where userid=".$current_user->id." and id=".$goal_criteria);

    $goal_criteria_id = $goal_criteria == null?$goal_criteria_key:$goal_criteria->id;
    ob_start();
?>
<tr class="criteria_row">
    <td class="goal_criteria_score_column">
        <span class="goal_criteria_comparison_type">(and)</span>
        <label for="criteria_score-<?php echo $goal_criteria_id;?>">Score/Block</label>
    </td>
    <td>
        <div class="form-group select-form-group">
            <select id="criteria_score-<?php echo $goal_criteria_id;?>" name="criteria_score-<?php echo $goal_criteria_id;?>" class="form-control select2 score_dropdown">
                <option value="TrafficQuality">Traffic Quality</option>
                <option value="LeadScore">Lead Score</option>
                <option value="TimeDelivered">Time Delivered</option>
                <option value="SaleQuality">Sale Quality</option>
                <option value="OverallProfitability">Overall Profitability</option>
                <option value="OverallDeliverability">Overall Deliverability</option>
                <option value="VendorOverallScore">Vendor Overall Score</option>
            </select>
        </div>
    </td>
    <td>
        <label for="criteria_score-<?php echo $goal_criteria_id;?>">Criteria</label>
    </td>
    <td>
        <div class="form-group select-form-group">
            <select id="criteria_type-<?php echo $goal_criteria_id;?>" name="criteria_type-<?php echo $goal_criteria_id;?>" class="form-control select2 compare_dropdown">
                <option>==</option>
                <option>>=</option>
                <option><=</option>
                <option>></option>
                <option><</option>
            </select>
        </div>
    </td>
    <td>
        <label for="criteria_reaches-<?php echo $goal_criteria_id;?>">Reaches</label>
    </td>
    <td>
        <div class="form-group">
            <input type="text" class="form-control reaches_input input-sm" id="criteria_reaches-<?php echo $goal_criteria_id;?>" name="criteria_reaches-<?php echo $goal_criteria_id;?>" placeholder="" />
            <input class="paidtraffic_goal_criteria_id" type="hidden" name="paidtraffic_goal_criteria[]" value="<?php echo $goal_criteria_id;?>" />
        </div>
    </td>
    <td>
        <a href="javascript:" class="remove_goal_criteria_row"><i class="fa fa-minus-circle"></i></a>
    </td>
</tr>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
}

function get_action_row($goal_action, $goal_action_key = ""){
	global $db, $current_user;
    if($goal_action != null && is_numeric($goal_action))
        $goal_action = $db->get_row("select * from ".$db->paidtraffic_goal_actions." where userid=".$current_user->id." and id=".$goal_action);

    $goal_action_id = $goal_action == null?$goal_action_key:$goal_action->id;
    ob_start();
?>
<tr class="action_row">
    <td>
        <label for="action_name-<?php echo $goal_action_id;?>">Action Name</label>
    </td>
    <td>
        <div class="form-group select-form-group">
            <select id="action_name-<?php echo $goal_action_id;?>" name="action_name-<?php echo $goal_action_id;?>" class="form-control select2 action_name_dropdown">
                <option value="AddToAutoSchedule">Add To Auto Schedule</option>
                <option value="SendEmail">Send Email</option>
            </select>
        </div>
    </td>
    <td>
        <label for="action_frequency-<?php echo $goal_action_id;?>">Frequency</label>
    </td>
    <td>
        <div class="form-group">
            <input type="text" class="form-control action_frequency_input input-sm" id="action_frequency-<?php echo $goal_action_id;?>" name="action_frequency-<?php echo $goal_action_id;?>" placeholder="" />
            <label for="action_frequency">days</label>
            <input class="paidtraffic_goal_action_id" type="hidden" name="paidtraffic_goal_action[]" value="<?php echo $goal_action_id;?>" />
        </div>
    </td>
    <td>
        <a href="javascript:" class="remove_goal_action_row"><i class="fa fa-minus-circle"></i></a>
    </td>
</tr>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 paidtraffic_goal_header">
            <h5>Step #1 - Set Your Goal</h5>
            <form class="form-inline paidtraffic_goal_form easylink-form" data-toggle="validator" role="form">
                <table class="table table-condensed">
                    <tr>
                        <td>
                            <label for="GoalName" class="control-label">Goal Name</label></td>
                        <td>
                            <div class="form-group has-feedback">
                                <input type="text" value="<?php if($goal) echo $goal->GoalName?>" class="form-control input-sm" id="GoalName" name="GoalName" placeholder="Goal Name" <?php echo NAME_PATTERN;?> />
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <small class="help-block with-errors"></small>
                            </div>
                        </td>
                        <td>
                            <div class="form-group has-feedback">
                                <input type="checkbox" name="GoalEnabled" id="GoalEnabled" class="blue " value="1" />&nbsp;
                                <span class="control-label">Goal Enabled</span>&nbsp;&nbsp;
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 paidtraffic_goal_criteria_header">
            <h5>Step #2 - Set Your Criteria</h5>
            <ul class="list-inline">
                <li><a href="#">Total Scores</a></li>
                <li><a href="#">Campaign Scores</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-inline paidtraffic_goal_criteria_form easylink-form" data-toggle="validator" role="form">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-condensed">
                            <tr class="add_criteria_row">
                                <td></td>
                                <td class="add_criteria">
                                    <a href="javascript:" class="add_and_criteria"><i class="fa fa-plus-circle"></i><span>and</span></a>
                                    <a href="javascript:" class="add_or_criteria"><i class="fa fa-plus-circle"></i><span>or</span></a>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 paidtraffic_goal_action_header">
            <h5>Step #3 - Create Your Action</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-inline paidtraffic_goal_action_form easylink-form" data-toggle="validator" role="form">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-condensed">
                            <tr class="add_action_row">
                                <td></td>
                                <td class="add_action">
                                    <a href="javascript:" class="add_and_action"><i class="fa fa-plus-circle"></i><span>and</span></a>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var CriteriaRow = '<?php echo str_replace(array("\r", "\n", "\\", "'"), array("", "", "\\\\", "\\'"), get_criteria_row(null, 'newcriteria-{new_goal_criteria_index}'));?>';
    var ActionRow = '<?php echo str_replace(array("\r", "\n", "\\", "'"), array("", "", "\\\\", "\\'"), get_action_row(null, 'newaction-{new_goal_action_index}'));?>';
    var CriteriaIndex = 0;
    var ActionIndex = 0;
    $(".add_action .add_and_action").on("click", function (e) {
        e.preventDefault();

        ActionIndex += 1;
        var $Row = $(ActionRow.replace(/{new_goal_action_index}/ig, ActionIndex));//.clone();
        $(".add_action_row").before($Row);
        CreateSelect2($Row.find('.select2'));
        $Row.find(".remove_goal_action_row").on("click", btn_remove_goal_action_row_click);
    });

    $(".add_criteria .add_and_criteria").on("click", function (e) {
        e.preventDefault();
        CriteriaIndex += 1;
        var $Row = $(CriteriaRow.replace(/{new_goal_criteria_index}/ig, CriteriaIndex));//.clone();
        $Row.find(".goal_criteria_comparison_type").html("(and)");

        console.log($Row);
        $(".add_criteria_row").before($Row);
        CreateSelect2($Row.find('.select2'));
        $Row.find(".remove_goal_criteria_row").on("click", btn_remove_goal_criteria_row_click);
    });

    $(".add_criteria .add_or_criteria").on("click", function (e) {
        e.preventDefault();
        CriteriaIndex += 1;
        var $Row = $(CriteriaRow.replace(/{new_goal_criteria_index}/ig, CriteriaIndex));//.clone();
        $Row.find(".goal_criteria_comparison_type").html("(and)");
        $(".add_criteria_row").before($Row);
        CreateSelect2($Row.find('.select2'));
        $Row.find(".remove_goal_criteria_row").on("click", btn_remove_goal_criteria_row_click);
    });

    function btn_remove_goal_criteria_row_click(e) {
        e.preventDefault();
        if (!ConfirmDelete())
            return;

        var $this = $(this);
        var $criteria_row = $this.closest("tr.criteria_row")
        var paidtraffic_goal_criteria_id = $criteria_row.find(".paidtraffic_goal_criteria_id").val();

        if (paidtraffic_goal_criteria_id.indexOf("newcriteria-") <= -1)
            $(".paidtraffic_goal_criteria_form").append('<input type="hidden" name="paidtraffic_goal_criteria_delete[]" value="' + paidtraffic_goal_criteria_id + '">');

        $criteria_row.remove();
    }

    function btn_remove_goal_action_row_click(e) {
        e.preventDefault();
        if (!ConfirmDelete())
            return;

        var $this = $(this);
        var $action_row = $this.closest("tr.action_row")
        var paidtraffic_goal_action_id = $action_row.find(".paidtraffic_goal_action_id").val();

        if (paidtraffic_goal_action_id.indexOf("newaction-") <= -1)
            $(".paidtraffic_goal_action_form").append('<input type="hidden" name="paidtraffic_goal_action_delete[]" value="' + paidtraffic_goal_action_id + '">');

        $action_row.remove();
    }
</script>

<?php include_once "member_popup_footer.php"; ?>