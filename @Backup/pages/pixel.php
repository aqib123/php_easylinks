<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['PixelID'])){
    $PixelID = parseInt($_GET['PixelID']);
    $sql = "select * from ".$db->pixels." where id=".$_GET['PixelID'];
    $pixel = $db->get_row($sql);
    if($pixel)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form">
        <div class="form-group has-feedback">
            <label for="PixelName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $pixel->PixelName;?>" class="form-control " id="PixelName" name="PixelName" placeholder="Pixel Name" required="required" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>
        
        <div class="form-group has-feedback">
            <label for="PixelCode" class="col-sm-4 control-label">Code</label>
            <div class="col-sm-8">
                <textarea rows="3" class="form-control " id="PixelCode" name="PixelCode" placeholder="Pixel Code" required="required"><?php if($isUpdateForm) echo $pixel->PixelCode;?></textarea>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_pixel" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>