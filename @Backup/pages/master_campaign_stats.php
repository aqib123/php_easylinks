<?php
if($_GET['master_campaign_status'] == "") 
    $master_campaign_status = "all";
else
    $master_campaign_status = $_GET['master_campaign_status'];

$pagetitle = "Master Campaign";
$curpage = "master-campaign";
$pageurl = "master-campaign";
$modulename = "master-campaign";

include_once "member_header.php";
$item = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($item['MenuItemURL'])?>"><i class="<?php echo $item["MenuItemClass"]?>"></i><?php echo $item['MenuItemLabel'];?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("master-campaign-add/?js=true&nodata=true")?>" data-widget="ShowLinkModel" data-method="post" data-page-title="Master Campaign">Add Master Campaign</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Campaigns"><i class="fa fa-file"></i></a>
                <!--<a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Campaigns"><i class="fa fa-hourglass-o"></i></a>-->
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Campaigns"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed Campaigns"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Master Campaigns</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#campaignslist" aria-expanded="true" aria-controls="campaignslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="campaignslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-campaign-tracking-stats data-table responsive- nowrap" data-nobuttons="true">
                    <thead>
                        <tr>
                            <th class="text-center"><span></span></th>
                            <th><span>Master Campaign Name</span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Date Created</span></th>
                            <th class="text-center"><span>Pending</span></th>
                            <th class="text-center"><span>Active</span></th>
                            <th class="text-center"><span>Completed</span></th>
                            <th class="text-center"><span>Total Clicks</span></th>
                            <th class="text-center stats-column"><span>Manual</span></th>
                            <th class="text-center"><span>Status</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->master_campaigns." where userid=".$current_user->id;

                        if($master_campaign_status == "pending")
                            $sql .= " and MasterCampaignStatus = 'pending' ";
                        else if($master_campaign_status == "active")
                            $sql .= " and MasterCampaignStatus = 'active' ";
                        else if($master_campaign_status == "complete")
                            $sql .= " and MasterCampaignStatus = 'complete' ";

                        $master_campaigns = $db->get_results($sql);
                        foreach($master_campaigns as $master_campaign){
                            $MasterCampaignID = $master_campaign->id;
                            $MasterCampaignDetails = get_master_campaign_details($MasterCampaignID);

                            $ManualSale = $db->get_row("select * from ".$db->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=0 and userid=".$current_user->id);
                            if($ManualSale){
                                $SaleAmount = get_manualsale_amount(0, $MasterCampaignID);
                                $manualsalelink = '<a href="'.get_site_url("manual-sale/split-partners/?MasterCampaignID=".$MasterCampaignID).'" class="" title="Manual Sale">$'.$SaleAmount['TotalAmount'].'</a>';
                            } else {
                                $manualsalelink = '<a href="'.get_site_url("manual-sale/?MasterCampaignID=".$MasterCampaignID).'" class="" title="Manual Sale">create</a>';
                            }
                            //echo "<pre>".print_r($MasterCampaignDetails, true)."</pre>";

                            $MasterCampaignStatus = $master_campaign->MasterCampaignStatus;
                            if($MasterCampaignStatus == "active"){
                                $master_campaignstatus = "Live";
                                $master_campaignstatusicon = "fa fa-rocket";
                            }else if($MasterCampaignStatus == "pending"){
                                $master_campaignstatus = "Pending";
                                $master_campaignstatusicon = "fa fa-hourglass-o";
                            }else if($MasterCampaignStatus == "complete"){
                                $master_campaignstatus = "Completed";
                                $master_campaignstatusicon = "fa fa-check-square-o";
                            }
                        ?>
                        <tr>
                            <td class="text-center"><a href="javascript:" data-url="<?php site_url($curpage."?act=master_campaign_details&MasterCampaignID=".$MasterCampaignID);?>" data-toggle="DetailRow" id="" class="btn btn-campaign-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <td><span><?php echo $master_campaign->MasterCampaignName;?></span></td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("master-campaign-add/?js=true&nodata=true&MasterCampaignID=".$MasterCampaignID)?>" data-widget="ShowLinkModel" data-method="post" data-page-title="Master Campaign"" class="btn btn-sm btn-links" title="Edit Campaign"><i class="fa fa-pencil-square"></i>Edit Campaign</a></li>
                                        <li><a href="<?php site_url("master-campaign/?act=delete_master_campaign&MasterCampaignID=".$MasterCampaignID);?>" class="btn btn-sm btn-links btn-delete" title="Delete Campaign"><i class="fa fa-trash"></i>Delete Campaign</a></li>
                                        <!--<a href="<?php site_url("master-campaign/?act=clone_link&MasterCampaignID=".$MasterCampaignID);?>" class="btn btn-sm btn-links" title="Clone Link"><i class="fa fa-clone"></i></a>-->
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo date("m-d-Y", $master_campaign->DateAdded);?></span></td>
                            <td class="text-center"><span><?php echo $MasterCampaignDetails["TotalPending"];?></span></td>
                            <td class="text-center"><span><?php echo $MasterCampaignDetails["TotalActive"];?></span></td>
                            <td class="text-center"><span><?php echo $MasterCampaignDetails["TotalComplete"];?></span></td>
                            <td class="text-center"><span><?php echo $MasterCampaignDetails["TotalClicks"];?></span></td>
                            <td class="text-center"><span><?php echo $manualsalelink;?></span></td>
                            <td class="text-center"><a href="javascript:" title="<?php echo $master_campaignstatus?>" class="status_icon grey-scale"><i class="fa <?php echo $master_campaignstatusicon;?>"></i></a></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include_once "member_footer.php";
?>