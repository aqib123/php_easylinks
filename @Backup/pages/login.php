<?php
$bodyclasses = ' login-page ';
$pagetitle = ' Login ';
include_once "site_header.php";

if(is_user_loggedin())
    site_redirect("dashboard");
?>
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg page-header">
            Welcome to the Dashboard
                <small>Sign in to start your session</small>
        </p>
        <form class="form-horizontal easylink-form login-form" method="post" data-toggle="custom-validator" role="form" action="<?php //echo get_site_url("login")?>">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="User Name" required="required" <?php echo ALPHANUMERIC_PATTERN;?> />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>

            <div class="form-group has-feedback">
                <div class="checkbox-">
                    <input type="checkbox" name="savepass" id="savepass" class="blue " value="1" />&nbsp;
                    <span>Remember Password</span>&nbsp;
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-block btn-flat btn-primary"><span class="text-uppercase">Sign In <i class="ion-log-in"></i></span></button>
                    <input type="hidden" name="act" value="login_user" />
                </div>
            </div>
        </form>
        <br />

        <div class="row">
            <div class="col-sm-12 text-left">
                <a class="text-left" href="<?php site_url("login/forget-password");?>">Forget Password?</a>
            </div>
            <div class="col-sm-12 text-left">
                
            </div>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
<script src="<?php site_url("js/login.js");?>" type="text/javascript"></script>
<?php
include_once "site_footer.php";
?>