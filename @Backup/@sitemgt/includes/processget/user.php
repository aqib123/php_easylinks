<?php
if($get_act == "verify_email"){
    if(!empty($_GET['key'])){
        $user_activation_key = md5($_GET['key']);
        $id = $_GET["userid"];
        $sql = "select * from ".$db->users." where id=".$id." and md5(user_activation_key) = '".$user_activation_key."'";
    }
} else if($get_act == "logout"){
    unset($_SESSION["current_user"]);
    unset($_SESSION["user_loggedin"]);
    session_destroy();

    remove_cookie("el_username");
    remove_cookie("el_password");

    site_redirect("login/");
    die;
} else if($get_act == "delete_user"){
    if(isset($_GET['userid']) && is_numeric($_GET['userid'])){
        $UserID = intval($_GET["userid"]);
        $EditUser = $db->get_row("select * from ".$db->users." where id=".$UserID);
        if($EditUser){
            $db->delete($db->users, array("id" => $EditUser->id));
            $db->delete($db->user_levels, array("UserID" => $EditUser->id));
            site_redirect("users");
            die;
        }
    }
}