<?php
if($post_act == "save_role"){
    $isUpdateForm = false;
    $RoleName = $_POST['RoleName'];
    $RoleEnabled = isset($_POST["RoleEnabled"]);

    if(isset($_POST['roleid']) && is_numeric($_POST['roleid'])){
        $RoleID = intval($_POST["roleid"]);
        $EditRole = $db->get_row("select * from ".$db->roles." where RoleDefault=0 and id=".$RoleID);
        if($EditRole)
            $isUpdateForm = true;
    }

    $data = array(
        "RoleName" => $RoleName,
        "RoleEnabled" => $RoleEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->roles, $data);
    else
        $db->update($db->roles, $data, array("id" => $EditRole->id));

    site_redirect("roles");
    die;
}