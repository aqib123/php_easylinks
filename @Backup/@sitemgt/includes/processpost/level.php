<?php
if($post_act == "save_level"){
    $isUpdateForm = false;
    $LevelName = $_POST['LevelName'];
    $LevelLogo = $_POST['LevelLogo'];
    $LevelEnabled = isset($_POST["LevelEnabled"]);

    if(isset($_POST['levelid']) && is_numeric($_POST['levelid'])){
        $LevelID = intval($_POST["levelid"]);
        $EditLevel = $db->get_row("select * from ".$db->levels." where LevelDefault=0 and id=".$LevelID);
        if($EditLevel)
            $isUpdateForm = true;
    }

    $data = array(
        "LevelName" => $LevelName,
        "LevelLogo" => $LevelLogo,
        "LevelEnabled" => $LevelEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->levels, $data);
    else
        $db->update($db->levels, $data, array("id" => $EditLevel->id));

    site_redirect("levels");
    die;
}