<?php
if($post_act == "save_option"){
    $isUpdateForm = false;
    $OptionName = $_POST['OptionName'];
    $OptionValue = $_POST['OptionValue'];
    $OptionEnabled = isset($_POST["OptionEnabled"]);

    if(isset($_POST['optionid']) && is_numeric($_POST['optionid'])){
        $OptionID = intval($_POST["optionid"]);
        $EditOption = $db->get_row("select * from ".$db->options." where OptionDefault=0 and id=".$OptionID);
        if($EditOption)
            $isUpdateForm = true;
    }

    $data = array(
        "OptionName" => $OptionName,
        "OptionValue" => $OptionValue,
        "OptionEnabled" => $OptionEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->options, $data);
    else
        $db->update($db->options, $data, array("id" => $EditOption->id));

    site_redirect("options");
    die;
}