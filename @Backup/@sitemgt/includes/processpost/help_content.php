<?php
if($post_act == "save_help_content"){
    $isUpdateForm = false;
    $HelpContentTitle = $_POST['HelpContentTitle'];
    $HelpContentExcerpt = $_POST['HelpContentExcerpt'];
    $HelpContentText = $_POST['HelpContentText'];
    $HelpContentType = $_POST['HelpContentType'];
    $HelpContentPosition = $_POST['HelpContentPosition'];
    $HelpContentEnabled = isset($_POST["HelpContentEnabled"]);

    if(isset($_POST['help_contentid']) && is_numeric($_POST['help_contentid'])){
        $HelpContentID = intval($_POST["help_contentid"]);
        $EditHelpContent = $db->get_row("select * from ".$db->help_contents." where id=".$HelpContentID);
        if($EditHelpContent)
            $isUpdateForm = true;
    }

    $data = array(
        "HelpContentTitle" => $HelpContentTitle,
        "HelpContentExcerpt" => $HelpContentExcerpt,
        "HelpContentText" => $HelpContentText,
        "HelpContentType" => $HelpContentType,
        "HelpContentPosition" => $HelpContentPosition,
        "HelpContentEnabled" => $HelpContentEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->help_contents, $data);
    else
        $db->update($db->help_contents, $data, array("id" => $EditHelpContent->id));

    site_redirect("help-contents");
    die;
}