<?php
if($post_act == "save_module"){
    $isUpdateForm = false;
    $ModuleName = $_POST['ModuleName'];
    $ModuleLabel = $_POST['ModuleLabel'];
    $ModuleEnabled = isset($_POST["ModuleEnabled"]);

    if(isset($_POST['moduleid']) && is_numeric($_POST['moduleid'])){
        $ModuleID = intval($_POST["moduleid"]);
        $EditModule = $db->get_row("select * from ".$db->modules." where id=".$ModuleID);
        if($EditModule)
            $isUpdateForm = true;
    }

    $data = array(
        "ModuleName" => $ModuleName,
        "ModuleLabel" => $ModuleLabel,
        "ModuleEnabled" => $ModuleEnabled
    );

    if(!$isUpdateForm)
        $db->insert($db->modules, $data);
    else
        $db->update($db->modules, $data, array("id" => $EditModule->id));

    site_redirect("modules");
    die;
}