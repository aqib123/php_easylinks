<?php
$pageurl = "admin-domain";
$pagetitle = "Admin Domain";
$admin_domainname = "admin-domains";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['domainid']) && is_numeric($_GET['domainid'])){
    $DomainID = intval($_GET["domainid"]);
    $EditDomain = $db->get_row("select * from ".$db->domains." where DomainType='admindomain' and id=".$DomainID);
    if($EditDomain){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("admin-domain")?>" role="button">New Admin Domain</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("admin-domains")?>" role="button">Admin Domains</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Admin Domain</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("admin-domain");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="DomainName" class="col-sm-3 control-label">Domain Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditDomain->DomainName;?>" class="form-control " id="DomainName" name="DomainName" placeholder="Domain Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="DomainSlug" class="col-sm-3 control-label">Domain Slug</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditDomain->DomainSlug;?>" class="form-control " id="DomainSlug" name="DomainSlug" placeholder="Domain Slug" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="DomainUrl" class="col-sm-3 control-label">Domain URL</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditDomain->DomainUrl;?>" class="form-control " id="DomainUrl" name="DomainUrl" placeholder="Domain URL" required="required" <?php echo URL_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("admin-domains")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="domainid" value="<?php echo $EditDomain->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_admin_domain" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>