<?php
include_once "../includes/functions.php";
if(!is_user_loggedin())
    redirect(get_site_url("login"));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Easy Links | <?php echo $pagetitle;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php site_url("css/bootstrap.min.css");?>" />

    <!-- Font Awesome Icons -->
    <link href="<?php site_url("css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="<?php site_url("css/AdminLTE.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php site_url("css/skin-black-light.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php site_url("css/blue.css");?>" />

    <!-- Select2 -->
    <link href="<?php site_url("css/select2.min.css");?>" rel="stylesheet" />

    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/daterangepicker.css");?>" />

    <!-- custom scrollbar stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery.mCustomScrollbar.min.css");?>" />

    <!-- qtip2 stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery.qtip.min.css");?>" />

    <!-- Datatables stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/dataTables.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/responsive/responsive.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/buttons/buttons.bootstrap.min.css");?>" />

    <link href="<?php site_url("css/style.css");?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php site_url("js/jquery-1.11.3.min.js");?>"></script>

    <!-- jQueryUI -->
    <script src="<?php site_url("js/jquery-ui.min.js");?>"></script>

    <!-- jQueryUI stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery-ui-1.10.0.custom.css");?>" />

    <!-- bootstrap tokenfield plugin -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/bootstrap-tokenfield.min.css");?>" />

    <script>
        var siteurl = '<?php site_url("");?>';
        var userid = '<?php echo $current_user->id;?>';
        var username = '<?php echo $current_user->user_login;?>';
    </script>
</head>
<body class="skin-black-light sidebar-mini <?php echo isset($bodyclasses)?$bodyclasses:"";?> ">

    <div id="EasyLinksModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="EasyLinksModelTitle">Modal title</h4>
                </div>
                <div class="modal-body">
                    asdf
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EasyLinksModelSave">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="EasyLinksWaitModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksWaitModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body">
                    <div class="progress progress-sm active">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">100%</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?php site_url("");?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                    <img src="<?php site_url("images/easylinks_logo.png")?>" class="easylinks_logo" title="Easy Links" /></span>

            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php main_url(!empty($current_user->user_picture)?$current_user->user_picture:"images/avatar5.png");?>" class="user-image" alt="User Image" />
                                <span class="hidden-xs"><?php echo $current_user->display_name;?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php main_url(!empty($current_user->user_picture)?$current_user->user_picture:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $current_user->display_name;?>
                                        <small>Member since <?php echo date("M, Y", strtotime($current_user->user_registered));?></small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php site_url("user-profile")?>" class="btn btn-default btn-flat">Update Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php site_url("change-password/?js=true")?>" data-widget="ShowLinkModel" data-method="post" data-page-title="Password Manager" class="btn btn-default btn-flat">Change Password</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar linear_gradient-">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php main_url(!empty($current_user->user_picture)?$current_user->user_picture:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>
                            <small class="center-block">Welcome</small>
                            <span><?php echo $current_user->display_name;?></span>
                        </p>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <?php
                    $colors = array();
                    foreach($menu_items as $menu_item){
                        if($menu_item["MenuItemParentID"] == "0"){
                            $color = get_random_rbg($colors);//
                            $colors[] = $color;
                            $menu_item["color"] = $color;
                            $submenu_items = $db->get_results("select * from ".$db->menu_items." where MenuItemEnabled = 1 and MenuItemParentID=".$menu_item["id"], ARRAY_A);
                            $has_active_submenu_items = $db->get_var("select count(*) from ".$db->menu_items." where MenuItemParentID=".$menu_item["id"]." and MenuItemURL='".$pageurl."'") * 1;
                    ?>
                    <li class="<?php if($menu_item['MenuItemURL'] == $pageurl || $has_active_submenu_items > 0) echo "active";?>  <?php if(count($submenu_items) > 0) echo "treeview";?>">
                        <a href="<?php site_url($menu_item['MenuItemURL'])?>">
                            <i class="<?php echo $menu_item['MenuItemClass']?>" style="color: <?php echo $menu_item["color"];?>;"></i>
                            <span><?php echo $menu_item['MenuItemLabel']?></span>
                        </a>
                        <?php
                        if (count($submenu_items) > 0){
                        ?>
                        <ul class="treeview-menu">
                            <?php
                            foreach ($submenu_items as $submenu_item){
                                $color = get_random_rbg($colors);//
                                $colors[] = $color;
                                $submenu_item["color"] = $color;
                            ?>
                            <li class="<?php if($submenu_item['MenuItemURL'] == $pageurl) echo "active";?>">
                                <a href="<?php site_url($submenu_item['MenuItemURL'])?>">
                                    <i class="<?php echo $submenu_item['MenuItemClass']?>" style="color: <?php echo $submenu_item["color"];?>;"></i>
                                    <span><?php echo $submenu_item['MenuItemLabel']?></span>
                                </a>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                        <?php
                        }
                        ?>
                    </li>
                    <?php    
                        }
                    }
                    ?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->