<?php
$pageurl = "options";
$pagetitle = "Options";
$modulename = "options";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("option")?>" role="button">New Option</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("options")?>" role="button">Options</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Name</span></th>
                            <th><span>Value</span></th>
                            <th><span class="text-center">Enabled</span></th>
                            <th><span class="text-center">Default Option</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->options;//." where OptionDefault=0";
                        $options = $db->get_results($sql);
                        foreach ($options as $option){
                            $users = $db->get_var("select count(*) from ".$db->user_options." where OptionID=".$option->id);

                            $OptionValue = $option->OptionValue;
                            if(strlen($OptionValue) > 200)
                                $OptionValue = substr($OptionValue, 0, 200)." ...";
                        ?>
                        <tr>
                            <td><span><?php echo $option->OptionName?></span></td>
                            <td><span><?php echo htmlentities($OptionValue);?></span></td>
                            <td><span class="text-center"><?php echo $option->OptionEnabled == 0?"NO":"YES";?></span></td>
                            <td><span class="text-center"><?php echo $option->OptionDefault == 0?"NO":"YES";?></span></td>
                            <td>
                                <?php if($option->OptionDefault == 0) { ?>
                                <a href="<?php site_url("option/?optionid=".$option->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("options/?act=delete_option&optionid=".$option->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>