<?php
$pageurl = "role";
$pagetitle = "Roles";
$modulename = "roles";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['roleid']) && is_numeric($_GET['roleid'])){
    $RoleID = intval($_GET["roleid"]);
    $EditRole = $db->get_row("select * from ".$db->roles." where RoleDefault=0 and id=".$RoleID);
    if($EditRole){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("role")?>" role="button">New Role</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("roles")?>" role="button">Roles</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("role-modules")?>" role="button">Role Modules</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("role-menu-items")?>" role="button">Role Menu Items</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Role</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("role");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="RoleName" class="col-sm-3 control-label">Role Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditRole->RoleName;?>" class="form-control " id="RoleName" name="RoleName" placeholder="Role Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id="paidtrafficurl"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="RoleEnabled" class="col-sm-3 control-label">Role Enable</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="RoleEnabled" name="RoleEnabled" <?php if($isUpdateForm && $EditRole->RoleEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("roles")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="roleid" value="<?php echo $EditRole->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_role" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>