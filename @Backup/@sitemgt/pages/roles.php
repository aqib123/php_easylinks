<?php
$pageurl = "roles";
$pagetitle = "Roles";
$modulename = "roles";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("role")?>" role="button">New Role</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("roles")?>" role="button">Roles</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("role-modules")?>" role="button">Role Modules</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("role-menu-items")?>" role="button">Role Menu Items</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Name</span></th>
                            <th><span class="text-center">User(s)</span></th>
                            <th><span class="text-center">Enabled</span></th>
                            <th><span class="text-center">Default Role</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->roles;//." where RoleDefault=0";
                        $roles = $db->get_results($sql);
                        foreach ($roles as $role){
                            $users = $db->get_var("select count(*) from ".$db->user_roles." where RoleID=".$role->id);
                        ?>
                        <tr>
                            <td><span><?php echo $role->RoleName?></span></td>
                            <td><span class="text-center"><?php echo $users;?></span></td>
                            <td><span class="text-center"><?php echo $role->RoleEnabled == 0?"NO":"YES";?></span></td>
                            <td><span class="text-center"><?php echo $role->RoleDefault == 0?"NO":"YES";?></span></td>
                            <td>
                                <a href="<?php site_url("users/?roleid=".$role->id);?>" class="btn btn-sm btn-links" data-toggle="qtiptooltip" title="Users"><i class="fa fa-users"></i></a>
                                <a href="<?php site_url("role-modules/?roleid=".$role->id);?>" class="btn btn-sm btn-links" data-toggle="qtiptooltip" title="Modules Settings"><i class="fa fa-building"></i></a>
                                <a href="<?php site_url("role-menu-items/?roleid=".$role->id);?>" class="btn btn-sm btn-links" data-toggle="qtiptooltip" title="Menu Items Settings"><i class="fa fa-bars"></i></a>
                                <?php if($role->RoleDefault == 0) { ?>
                                <a href="<?php site_url("role/?roleid=".$role->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("roles/?act=delete_role&roleid=".$role->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>