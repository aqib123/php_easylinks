<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.2.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://chadnicely.com">Chad Nicely</a>.</strong> All rights reserved.
</footer>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php site_url("js/bootstrap.min.js");?>"></script>

<!-- iCheck -->
<script src="<?php site_url("js/icheck.min.js");?>" type="text/javascript"></script>

<!-- Select2 -->
<script src="<?php site_url("js/select2.min.js");?>"></script>

<!-- qtip2 -->
<script src="<?php site_url("js/imagesloaded.pkg.min.js");?>"></script>
<script src="<?php site_url("js/jquery.qtip.min.js");?>"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?php site_url("js/moment.min.js");?>"></script>
<script type="text/javascript" src="<?php site_url("js/moment-timezone-with-data.min.js");?>"></script>
<script type="text/javascript" src="<?php site_url("js/daterangepicker.js");?>"></script>

<!-- SlimScroll -->
<script src="<?php site_url("js/jquery.slimscroll.min.js");?>" type="text/javascript"></script>

<!-- FastClick -->
<script src="<?php site_url("js/fastclick.min.js");?>" type="text/javascript"></script>

<!-- Validator -->
<script src="<?php site_url("js/validator.js");?>" type="text/javascript"></script>

<!-- ZeroClipboard -->
<script src="<?php site_url("js/ZeroClipboard.min.js");?>" type="text/javascript"></script>

<!-- jQuery Knob -->
<script src="<?php site_url("js/jquery.knob.js");?>" type="text/javascript"></script>

<!-- ChartJs Knob -->
<script src="<?php site_url("js/Chart.min.js");?>" type="text/javascript"></script>

<!-- js cookie -->
<script src="<?php site_url("js/js.cookie.js");?>" type="text/javascript"></script>

<!-- custom scrollbar plugin -->
<script src="<?php site_url("js/jquery.mCustomScrollbar.concat.min.js");?>" type="text/javascript"></script>

<!-- jquery picture cut plugin -->
<script id="picture_element_css_to_bootstrap" src="<?php site_url("js/jquery.picture.cut.js");?>" type="text/javascript"></script>

<!-- bootstrap tokenfield plugin -->
<script src="<?php site_url("js/bootstrap-tokenfield.min.js");?>" type="text/javascript"></script>

<!-- jquery sparkline plugin -->
<script src="<?php site_url("js/jquery.sparkline.min.js");?>" type="text/javascript"></script>

<!-- Datatable plugin -->
<script src="<?php site_url("js/dataTables/jquery.dataTables.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/dataTables.bootstrap.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/responsive/dataTables.responsive.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/dataTables.buttons.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.bootstrap.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.print.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.html5.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.colVis.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/pdfmake.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/vfs_fonts.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/jszip.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/dataTable.pipeline.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/easylinks.js");?>" type="text/javascript"></script>

<script src="<?php site_url("js/bootstrap.modelfix.js");?>"></script>

<!-- Gradient Text -->
<script src="<?php site_url("js/pxgradient-1.0.3.min.js");?>"></script>

<!-- AdminLTE App -->
<script src="<?php site_url("js/app.min.js");?>" type="text/javascript"></script>

<script>
    var SaleInfoItems = [];
    var PageAlert = "";
    var HideAlert = true;
<?php
if ((isset($_GET) && isset($_GET['success']) && isset($_GET["msg"])) || 
    (isset($errors) && isset($errors['success']) && isset($errors["msg"]))){
    echo 'PageAlert = \'\';';
    echo 'PageAlert += \'<div class="alert alert-message '.((isset($_GET['success']) && $_GET['success'] == false) || (isset($errors['success']) && $errors['success'] == false)?" alert-danger ":" alert-success ").' alert-dismissable">\';';
    echo 'PageAlert += \'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>\';';
    echo 'PageAlert += \'<h4><i class="icon fa fa-ban"></i>Alert!</h4>\';';
    echo 'PageAlert += \''.(isset($_GET['msg'])?$_GET['msg']:$errors['msg']).'\';';
    echo 'PageAlert += \'</div>\';';

    if(isset($_GET["nohidealert"]) || isset($errors["nohidealert"]))
        echo "HideAlert = false;";
}
?>
    $(document).ready(function () {
        if (PageAlert != "") {
            var $alert = $(PageAlert).insertBefore($(".content-header"));
            $alert.delay(5000);

            if (HideAlert) {
                $alert.fadeOut(1000);
            } else {
                $alert.find(".close").hide();
            }
        }
    });
    <?php
    //}
    ?>
</script>
</body>
</html>