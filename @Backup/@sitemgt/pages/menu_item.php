<?php
$pageurl = "menu-item";
$pagetitle = "Menu Items";
$menu_itemname = "menu_items";

include_once "member_header.php";

$item = get_menuitem($pageurl);

$isUpdateForm = false;
if(isset($_GET['menu_itemid']) && is_numeric($_GET['menu_itemid'])){
    $MenuItemID = intval($_GET["menu_itemid"]);
    $EditMenuItem = $db->get_row("select * from ".$db->menu_items." where id=".$MenuItemID);
    if($EditMenuItem){
        $isUpdateForm = true;
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("menu-item")?>" role="button">New Menu Item</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("menu-items")?>" role="button">Menu Items</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("level-menu-items")?>" role="button">Level Menu Items</a>
            </div>
        </div>
        <hr />

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Menu Item</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php site_url("menu-item");?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="MenuItemLabel" class="col-sm-3 control-label">Label</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditMenuItem->MenuItemLabel;?>" class="form-control " id="MenuItemLabel" name="MenuItemLabel" placeholder="Label" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="MenuItemURL" class="col-sm-3 control-label">URL</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditMenuItem->MenuItemURL;?>" class="form-control " id="MenuItemURL" name="MenuItemURL" placeholder="URL" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="MenuItemClass" class="col-sm-3 control-label">Class</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditMenuItem->MenuItemClass;?>" class="form-control " id="MenuItemClass" name="MenuItemClass" placeholder="Class" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="MenuItemPosition" class="col-sm-3 control-label">Position</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php if($isUpdateForm) echo $EditMenuItem->MenuItemPosition;?>" class="form-control " id="MenuItemPosition" name="MenuItemPosition" placeholder="Position" required="required" <?php echo INT_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id=""></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="MenuItemEnabled" class="col-sm-3 control-label">Enabled</label>
                                            <div class="col-sm-9 smallradio- smallcheck-">
                                                <input type="checkbox" value="1" class="form-control- blue" id="MenuItemEnabled" name="MenuItemEnabled" <?php if($isUpdateForm && $EditMenuItem->MenuItemEnabled == 1) echo ' checked="checked" ';?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("menu_items")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($isUpdateForm){ ?>
                                <input type="hidden" name="menu_itemid" value="<?php echo $EditMenuItem->id;?>" />
                                <?php } ?>
                                <input type="hidden" name="act" value="save_menu_item" />
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>