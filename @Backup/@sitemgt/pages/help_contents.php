<?php
$pageurl = "help-contents";
$pagetitle = "Help Contents";
$modulename = "help_contents";

include_once "member_header.php";

$item = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("help-content")?>" role="button">New Help Content</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("help-contents")?>" role="button">Help Contents</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $pagetitle;?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span class="text-center">Position</span></th>
                            <th><span>Title</span></th>
                            <th><span>Excerpt</span></th>
                            <th><span>Text</span></th>
                            <th><span class="text-center">Type</span></th>
                            <th><span class="text-center">Enabled</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".$db->help_contents." order by HelpContentPosition";
                        $help_contents = $db->get_results($sql);
                        foreach ($help_contents as $help_content){
                            $HelpContentExcerpt = $help_content->HelpContentExcerpt;
                            $HelpContentText = $help_content->HelpContentText;

                            $HelpContentTypes = array("s3video" => "S3 Video", "youtubevideo" => "Youtube Video", "html" => "HTML Content");
                            $HelpContentType = $help_content->HelpContentType;
                            if($HelpContentType == 'youtubevideo'){
                                $HelpContentText = '<img src="http://img.youtube.com/vi/'.$HelpContentText.'/mqdefault.jpg" class="youtube_thumbnail" />';
                            } else{
                                if(strlen($HelpContentText) > 200)
                                    $HelpContentText = substr($HelpContentText, 0, 200)." ...";
                                $HelpContentText = htmlentities($HelpContentText);
                            }
                        ?>
                        <tr>
                            <td class="text-center"><span class="text-center"><?php echo $help_content->HelpContentPosition;?></span></td>
                            <td><span><?php echo $help_content->HelpContentTitle?></span></td>
                            <td><span><?php echo $HelpContentExcerpt;?></span></td>
                            <td><span><?php echo $HelpContentText;?></span></td>
                            <td class="text-center"><span class="text-center"><?php echo $HelpContentTypes[$help_content->HelpContentType];?></span></td>
                            <td class="text-center"><span class="text-center"><?php echo $help_content->HelpContentEnabled == 0?"NO":"YES";?></span></td>
                            <td>
                                <a href="<?php site_url("help-content/?help_contentid=".$help_content->id);?>" class="btn btn-sm btn-links" title=""><i class="fa fa-pencil-square"></i></a>
                                <a href="<?php site_url("help-contents/?act=delete_help_content&help_contentid=".$help_content->id);?>" class="btn btn-sm btn-links btn-delete" title=""><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php	
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>