﻿/// <reference path="_references.js" />

var $ModelCaller;
var ModelFormValid = true;
var Charts = {};
var $EasyLinksModel = $("#EasyLinksModel");
var markers = [];
var IpAddresses = [];
var map;
var markerIndex = 0;

$.pleasewait = function (option) {
    var action = typeof option == "undefined" ? "show" : option;
    var $EasyLinksWaitModel = $("#EasyLinksWaitModel");
    if (action == "show") {
        $EasyLinksWaitModel.modal({
            backdrop: 'static',
            keyboard: false
        });
    } else if (action == "hide") {
        $EasyLinksWaitModel.modal("hide");
    }
};

var ValidatorOptions = {
    custom: {
        select: function ($el) {
            var value = $el.val() != null ? $el.val().trim() : "";
            //console.log("value: " + value);
            return (value != "" && value != -1);
        }
    },
    errors: {
        select: "Please select valid value",
        //pattern: "testing"
    }
}

var AdminLTEOptions = {
    boxWidgetOptions: {
        boxWidgetIcons: {
            collapse: 'fa-chevron-down',
            open: 'fa-chevron-up'
        }
    }
};

var pieChartOptions = {
    showScale: true,
    scaleShowLabels: true,
    scaleLabel: "<%=value%>",
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    percentageInnerCutout: 0,
    animationSteps: 100,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    responsive: true,
    maintainAspectRatio: false,
    multiTooltipTemplate: "<%=datasetLabel%> (<%= value %>)",
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend list-inline \"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
};

var lineChartOptions = {
    showScale: true,
    scaleShowGridLines: false,
    scaleGridLineColor: "#000000",
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: true,
    bezierCurve: true,
    bezierCurveTension: 0.3,
    pointDot: true,
    pointDotRadius: 4,
    pointDotStrokeWidth: 1,
    pointHitDetectionRadius: 20,
    datasetStroke: true,
    datasetStrokeWidth: 2,
    datasetFill: false,
    maintainAspectRatio: false,
    responsive: true,
    datasetFill: false,
    multiTooltipTemplate: "<%=datasetLabel%> (<%= value %>)",
    legendTemplate: "<ul class=\" <%=name.toLowerCase()%>-legend list-inline\"><%for (var i=0; i<datasets.length; i++){%><%for (var x in datasets[i]){%> <%=x%> -- <%}%><li><input type=\"checkbox\" name=\"<%if(datasets[i].label){%><%=datasets[i].label%><%}%>\" id=\"<%if(datasets[i].label){%><%=datasets[i].label%><%}%>\" class=\"blue checkbox\" value=\"1\" checked=\"checked\" />&nbsp;<span class=\"legend_color\" style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;</span>&nbsp;<span class=\"legend_label small\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%>&nbsp;(<?php echo $legendsvalues[$legend]?>&nbsp;/<?php echo $legendsvalues[\"Total\"] > 0?round(($legendsvalues[$legend] * 100 / $legendsvalues[\"Total\"]), 1):0;?>%)</span>&nbsp;</li><%}%></ul>"
};

var barChartOptions = {
    scaleBeginAtZero: true,
    scaleShowGridLines: false,
    scaleGridLineColor: "#c1c1c1",
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: true,
    barShowStroke: true,
    barStrokeWidth: 2,
    barValueSpacing: 5,
    barDatasetSpacing: 1,
    responsive: true,
    maintainAspectRatio: false,
    datasetFill: false,
    multiTooltipTemplate: "<%=datasetLabel%> (<%= value %>)",
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend list-inline\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};

function CreateDateRangePicker($ele) {
    $ele.each(function () {
        var $this = $(this);
        var DateRangePickerOptions = {
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoApply": false,
            "timePicker": true,
            "timePickerIncrement": 1,
            "locale": {
                format: 'MM/DD/YYYY h:mm A'
            }
        };

        $this.daterangepicker(DateRangePickerOptions);
        $this.on('apply.daterangepicker', function (ev, picker) {
            var start = picker.startDate;
            var $element = $(picker.element);
            if ($element.attr("data-linked")) {
                var $linkbanked = $($element.attr("data-linked"));
                $linkbanked.val(start.format('MM/DD/YYYY h:mm A'));
            }
        });
    });
}

function CreateSelect2($ele) {
    $ele.each(function (index, element) {
        var $this = $(this);
        var Select2Options = ValidatorOptions;// {};

        if (typeof $this.attr("data-placeholder") != "undefined")
            Select2Options["placeholder"] = $this.attr("data-placeholder");

        if (typeof $this.attr("data-enable-search") == "undefined")
            Select2Options["minimumResultsForSearch"] = Infinity;

        $.each($this.data(), function (key, value) {
            if (key.indexOf("custom_") > -1) {
                key = key.replace("custom_", "");
                if (typeof Select2Options["custom"][key] == "undefined") {
                    Select2Options["custom"][key] = eval(value.func);
                    Select2Options["errors"][key] = value.error;
                }

                if (typeof ValidatorOptions["custom"][key] == "undefined") {
                    ValidatorOptions["custom"][key] = eval(value.func);
                    ValidatorOptions["errors"][key] = value.error;
                }

                if (typeof $this.data(key) == "undefined")
                    $this.data(key, "true");
            }
        })

        $this.select2(Select2Options);
    });
}

function CreateImageUploader($ele) {
    __IMAGE_LOADING = siteurl + "images/ajaxloader.gif"
    var ImageButtonCSS = {
        border: "none",
        width: 150,
        height: 225,//30,
        "padding-top": 2,
        "text-align": "center",
        "margin": "0 auto",
    };

    if ($ele.attr("data-nocenter"))
        delete ImageButtonCSS["margin"];

    $ele.PictureCut({
        InputOfImageDirectory: $ele.attr("data-input-name"),
        PluginFolderOnServer: siteurl,
        FolderOnServer: "/uploads/users/" + userid + "/",
        ActionToSubmitUpload: siteurl + "uploadimage",
        ActionToSubmitCrop: siteurl + "cropimage",
        EnableCrop: true,
        ImageButtonCSS: ImageButtonCSS,
        EnableButton: false,
        CropWindowStyle: "Bootstrap",
    });
}

function CreateTokenField($ele) {
    $ele
        .on('tokenfield:createdtoken', function (e) {
            // Über-simplistic e-mail validation
            var re = /^[a-zA-Z0-9]{1,}$/
            var valid = re.test(e.attrs.value)
            if (!valid)
                $(e.relatedTarget).addClass('invalid');

            var $formgroup = $(e.relatedTarget).closest(".form-group");
            if ($formgroup.find(".invalid").length > 0)
                $formgroup.addClass("has-error");
            else
                $formgroup.removeClass("has-error");
        })
        .on('tokenfield:removedtoken', function (e) {
            var $formgroup = $(e.relatedTarget).closest(".form-group");
            if ($formgroup.find(".invalid").length > 0)
                $formgroup.addClass("has-error");
            else
                $formgroup.removeClass("has-error");
        })
        .tokenfield();
}

function rtrim(str, ch) {
    if (str.length > 0 && str.length > ch.length && str.substr(str.length - ch.length, ch.length) == ch) {
        return str.substring(0, str.length - ch.length);
    } else {
        return str;
    }
}

function CheckUncheck(chkall, chks) {
    //alert(j('input:checkbox' + chks).length);
    $('input:checkbox' + chks).each(function () {
        this.checked = chkall.checked;
    });
}

function CreatePieChart($ele, excludes) {
    if (typeof Charts[$ele.attr("id")] != "undefined") {
        Charts[$ele.attr("id")].destroy();
    }

    var pieChartCanvas = $ele.get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [];

    var strPieValues = $ele.attr('data-chart-values');
    var PieValues = strPieValues.split('!');
    for (var index = 0; index < PieValues.length; index++) {
        var strValue = PieValues[index];
        var jsonData = JSON.parse(PieValues[index]);

        if (strValue != '') {
            var addvalue = true;
            if (typeof excludes == "object" && $.inArray(jsonData.label, excludes) > -1)
                addvalue = false

            if (addvalue)
                PieData.push(jsonData);
        }

    }


    var chart = pieChart.Pie(PieData, pieChartOptions);
    Charts[$ele.attr("id")] = chart;
    var chartlegend = GenerateLegend(PieData);
    return chartlegend;
}

function CreateBarChart($ele, excludes) {
    if (typeof Charts[$ele.attr("id")] != "undefined") {
        Charts[$ele.attr("id")].destroy();
    }

    var barChartCanvas = $ele.get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var ChartData = { labels: [], datasets: [] };

    var strLineLabels = $ele.attr('data-chart-labels');
    var BarLabels = strLineLabels.split('!');
    for (var index = 0; index < BarLabels.length; index++) {
        if (BarLabels[index] != '')
            ChartData['labels'].push(BarLabels[index]);
    }

    var strLineDatasets = $ele.attr('data-chart-datasets');
    var BarDatasets = strLineDatasets.split('!');
    for (var index = 0; index < BarDatasets.length; index++) {
        var strValue = BarDatasets[index];
        var jsonData = JSON.parse(strValue);
        if (strValue != '') {
            var addvalue = true;
            if (typeof excludes == "object" && $.inArray(jsonData.label, excludes) > -1)
                addvalue = false;

            if (addvalue)
                ChartData['datasets'].push(jsonData);
        }
    }

    var chart = barChart.Bar(ChartData, barChartOptions);
    Charts[$ele.attr("id")] = chart;
    var chartlegend = GenerateLegend(ChartData['datasets']);
    return chartlegend;
}

function CreateLineChart($ele, excludes, areachart) {
    if (typeof Charts[$ele.attr("id")] != "undefined") {
        Charts[$ele.attr("id")].destroy();
    }

    var lineChartCanvas = $ele.get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var ChartData = { labels: [], datasets: [] };


    var strLineLabels = $ele.attr('data-chart-labels');
    var LineLabels = strLineLabels.split('!');
    for (var index = 0; index < LineLabels.length; index++) {
        if (LineLabels[index] != '')
            ChartData['labels'].push(LineLabels[index]);
    }

    var strLineDatasets = $ele.attr('data-chart-datasets');
    var LineDatasets = strLineDatasets.split('!');
    for (var index = 0; index < LineDatasets.length; index++) {
        var strValue = LineDatasets[index];
        var jsonData = JSON.parse(strValue);

        if (strValue != '') {
            var addvalue = true;
            if (typeof excludes == "object" && $.inArray(jsonData.label, excludes) > -1)
                addvalue = false;

            if (addvalue)
                ChartData['datasets'].push(jsonData);
        }
    }

    if (typeof areachart == "undefined")
        lineChartOptions.datasetFill = false;
    else
        lineChartOptions.datasetFill = true;

    var chart = lineChart.Line(ChartData, lineChartOptions);
    Charts[$ele.attr("id")] = chart;
    var chartlegend = GenerateLegend(ChartData['datasets']);//chart.generateLegend();
    return chartlegend;
}

function CreateAreaChart($ele, excludes) {
    return CreateLineChart($ele, excludes, true);
}

function GenerateLegend(datasets) {
    var datasettotal = 0;
    for (var index in datasets) {
        if (typeof datasets[index].data == "undefined") {
            datasettotal += (datasets[index].value * 1);
        } else {
            for (var dataindex in datasets[index].data)
                datasettotal += (datasets[index].data[dataindex] * 1);
        }
    }
    var ul = '<ul class="list-inline">';
    for (var index in datasets) {
        var dataset = datasets[index];
        var li = '';
        var legendtotal = 0;

        li += '<li>'
        li += '<input type="checkbox" name="' + dataset.label + '" id="' + dataset.label + '" class="blue checkbox" value="' + dataset.label + '" checked="checked" />&nbsp;';
        li += '<span class="legend_color" style="background-color: ' + (typeof dataset.data == "undefined" ? dataset.color : dataset.strokeColor) + '">&nbsp;</span>&nbsp;';

        if (typeof dataset.data == "undefined") {
            legendtotal = dataset.value;
        } else {
            for (var dataindex in dataset.data)
                legendtotal += dataset.data[dataindex];
        }

        li += '<span class="legend_label small">' + dataset.label + '&nbsp;(' + legendtotal + '&nbsp;/' + (datasettotal > 0 ? Math.round(legendtotal * 100 / datasettotal) : 0) + '%)</span>&nbsp;'
        li += '</li>';

        ul += li;
    }
    ul += '</ul>';

    return ul;
}

function ConfirmDelete() {
    var c = confirm('Are you sure you want to delete?');
    return c ? true : false;
}

function ConfirmReset() {
    var c = confirm('Are you sure you want to reset stats?');
    return c ? true : false;
}

function ConfirmContinue($ele) {
    var message = 'Are you sure you want to continue?';

    if ($ele.attr("data-confirm-message") && $ele.attr("data-confirm-message") != "")
        message = $ele.attr("data-confirm-message")

    var c = confirm(message);
    return c ? true : false;
}

function createblueicheck(e) {
    var $this = $(this);
    if (!$this.parent().hasClass("icheckbox_square-blue") && !$this.parent().hasClass("iradio_square-blue")) {
        $this.iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });

        if ($this.attr("data-toggle") && $this.attr("data-toggle") == "ToggleTarget") {
            $this.on("ifChanged", function () {
                var $this = $(this);
                if ($this.attr("data-target")) {
                    var $datatarget = $($this.attr("data-target"));
                    if ($this.is(":checked")) {
                        $datatarget.show();
                        $datatarget.addClass("in");
                    } else {
                        $datatarget.hide();
                        $datatarget.removeClass("in");
                    }
                }
            });
        }
    }
}

function ShowEasyLinksModel($ele) {
    var href = $ele.attr('href');
    //var $EasyLinksModel = $("#EasyLinksModel");

    $EasyLinksModel.find(".modal-title").html($ele.attr("data-page-title"));
    $EasyLinksModel.find(".modal-body").html("");


    if ($ele.attr("data-append") && ($ele.attr("data-append-object") || $ele.attr("data-related"))) {
        var $related = $($ele.attr("data-append-object") ? $ele.attr("data-append-object") : $ele.attr("data-related"));
        var selected = $related.val();
        if (typeof selected != typeof undefined && selected != null && selected != "") {
            href += selected;
        } else {
            return;
        }
    }

    if ($ele.attr("data-model-width") && $(window).width() > $ele.attr("data-model-width")) {
        $EasyLinksModel.find(".modal-dialog").width($ele.attr("data-model-width"));
    } else {
        $EasyLinksModel.find(".modal-dialog").width("");
    }

    $ModelCaller = $ele;
    $EasyLinksModel.modal({
        backdrop: $ele.attr("data-static-model") ? "static" : true
    });

    $.get(href, "", function (data) {
        $EasyLinksModel.find(".modal-body").html(data);
        var $form = $($EasyLinksModel.find(".modal-body").find("form"));
        $form.on("submit", function (e) { e.preventDefault(); });

        ModelFormValid = true;

        $form.validator(ValidatorOptions);

        //$form.find('select.select2').select2({
        //    minimumResultsForSearch: Infinity
        //});

        CreateSelect2($form.find('select.select2'));

        $form.find('input[type="checkbox"].blue, input[type="radio"].blue').each(createblueicheck);

        if ($form.find(".ckeditor").length > 0) {
            $form.find(".ckeditor").ckeditor({
                on: {
                    instanceReady: function (evt) {
                        //var $editor = evt.editor;
                        //console.log($editor);
                    }
                }
            });
        }

        if ($form.find(".ImageUploader").length > 0) {
            $form.find(".ImageUploader").each(function (index, element) {
                var $this = $(this);
                CreateImageUploader($this);
            });
        }


        if ($form.find(".tokenfieldinput").length > 0) {
            $form.find(".tokenfieldinput").each(function (index, element) {
                var $this = $(this);
                CreateTokenField($this);
            });
        }

        $EasyLinksModel.find(".modal-dialog").mCustomScrollbar({
            //setHeight: 340,
            //autoHideScrollbar: false,
            //alwaysShowScrollbar: 2,
            theme: "dark"
            //theme: "minimal-dark"
        });
        //$EasyLinksModel.modal();
    });
}

function AddSaleinfo($ele) {
    if ($("body").hasClass("modal-open")) {
        $EasyLinksModel.on("hidden.bs.modal", AddSaleinfo);
    } else {
        $EasyLinksModel.off("hidden.bs.modal", AddSaleinfo);

        if (typeof $ele == "undefined")
            $ele = SaleInfoItems.shift();

        if (typeof $ele != "undefined") {
            ShowEasyLinksModel($("<a/>").attr("href", $ele.link).attr("data-page-title", "Manual Sales").attr("data-method", "post"));//$ele.type + " Sales Info manager"
        }
    }
}

function ucwords(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
    return str;
}

function DataTable($ele) {
    $ele.css("width", "100%");
    var isResponsive = $ele.hasClass("responsive");

    var options = {
        "paging": false,
        "lengthChange": true,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": true,
        "pageLength": 10,
        "responsive": isResponsive, //true,
        //"buttons": true,
        "stateSave": true,
        "buttons": {
            //buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
            buttons: [
                {
                    extend: 'copy',
                    text: '<i class="fa fa-clipboard" data-toggle="qtiptooltip" title="Copy To Clipboard"></i>',
                    className: '',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o" data-toggle="qtiptooltip" title="Export&nbsp;To&nbsp;Excel"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text-o" data-toggle="qtiptooltip" title="Export&nbsp;To&nbsp;CSV"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o" data-toggle="qtiptooltip" title="Export&nbsp;To&nbsp;PDF"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print" data-toggle="qtiptooltip" title="Print&nbsp;Table"></i>',
                    exportOptions: {
                        columns: ':visible'
                    },
                    autoPrint: false
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash" data-toggle="qtiptooltip" title="Show/Hide&nbsp;Column"></i>',
                    className: ' hidden-sm hidden-xs '
                },
            ],
            dom: {
                button: {
                    className: 'btn btn-link btn-xs'
                },
                buttonLiner: {
                    tag: '',
                    className: ''
                }
            }
        },
        "dom":
            "<'row'<'col-sm-12'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'data-table-footer'" +
            "<'row'<'col-sm-3'i><'col-md-6 col-sm-5'p><'col-md-1 col-sm-2'l><'col-sm-2'f>>" +
            ">",
        "language": {
            "lengthMenu": '<select class="select2" style="">' +
                '<option value="10">10</option>' +
                '<option value="20">20</option>' +
                '<option value="30">30</option>' +
                '<option value="40">40</option>' +
                '<option value="50">50</option>' +
                '<option value="-1">All</option>' +
                '</select>',
            "search": "<form class='easylink-form' role='form'><div class='form-group'>_INPUT_</div></form>"
        },
        "classes": {
            "sFilterInput": "form-control datatable-filterbox input-sm",
            "sInfo": "dataTables_info small",
            "sPageButton": "paginate_button",
        },
        "stateSaveCallback": function (settings, data) {
            if ($(this).attr("data-table-loaded") == "false") {
                $ele.attr("data-table-loaded", "true");
            } else {
                Cookies.set('dtstate_' + $(this).attr("id"), JSON.stringify(data), { expires: 7, path: '' });
            }
        }
    };

    if ($ele.attr("data-nobuttons")) {
        options["buttons"] = false;
        options["dom"] =
        "<'row'<'col-sm-12'tr>>" +
        "<'data-table-footer'" +
        "<'row'<'col-sm-4'i><'col-md-5 col-sm-4'p><'col-md-1 col-sm-2'l><'col-sm-2'f>>" +
        ">";
    }

    if ($ele.attr("data-paging")) {
        options["paging"] = true;
    }

    if ($ele.attr("data-nosearching")) {
        options["searching"] = false;
    }

    if ($ele.attr("data-searching")) {
        options["searching"] = true;
    }

    if ($ele.attr("data-noinfo")) {
        options["info"] = false
    }

    if ($ele.attr("data-info")) {
        options["info"] = true;
    }

    if ($ele.attr("data-nosort-columns") && $ele.attr("data-nosort-columns") != "") {
        var columns = $ele.attr("data-nosort-columns").split(",");
        if (columns.length > 0) {
            if (typeof options["columnDefs"] == "undefined") {
                options["columnDefs"] = [{ "targets": [], "orderable": false }];
                colindex = 0;
            } else {
                options["columnDefs"].push({ "targets": [], "orderable": false });
                colindex = 1;
            }

            for (var index = 0; index < columns.length; index++) {
                options["columnDefs"][colindex]["targets"].push(parseInt(columns[index]));
            }
        }
    }

    if ($ele.attr("data-default-sort-column") && $ele.attr("data-default-sort-column") != "") {
        options["order"] = [[$ele.attr("data-default-sort-column"), "asc"]];
    }
    
    $ele.attr("data-table-loaded", "false");
    var $table = $ele.DataTable(options);
    
    $table.on('responsive-display.dt', function (e, datatable, row, showHide, update) {
        if (showHide) {
            var $tr = $(this).find("tr.child").eq(0);
            $tr.find('[data-toggle="DetailRow"]').each(function (index, element) {
                var $this = $(this);
                $this.on("click", ShowDetailRow);
            });

            $tr.find('[data-toggle="StatusForm"]').each(function (Index, element) {
                var $this = $(this);
                $this.on("click", function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var $form = $this.next("form");

                    $form.toggle();
                })
            });

            //$tr.find(".select2").select2({ minimumResultsForSearch: Infinity });
            CreateSelect2($tr.find(".select2"));
        }
    });
    
    var data = Cookies.get('dtstate_' + $ele.attr("id"));
    if (data != undefined) {
        data = JSON.parse(data);
        var columns = data["columns"];

        for (var index = 0; index < columns.length; index++) {
            if (columns[index]["visible"] == false) {
                var column = $table.column(index);
                column.visible(false);
            }
        }
    }

    var $dataTables_wrapper = $ele.closest(".dataTables_wrapper");
    if (!isResponsive) {
        $dataTables_wrapper.css({
            "width": "97%",
            "margin-left": "1%",
        });
    }
}

function CreateMap($container) {
    var mapoptions = {
        container: $container,
        map: 'world_merc_en',
        normalizeFunction: 'polynomial',
        hoverOpacity: 0.7,
        hoverColor: false,
        backgroundColor: 'transparent',
        regionStyle: {
            initial: {
                fill: '#d2d6de',//'#174f68',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
            },
            hover: {
                "fill-opacity": 0.7,
                cursor: 'pointer'
            },
            selected: {
                fill: 'yellow'
            },
            selectedHover: {
            }
        },
        markerStyle: {
            initial: {
                //image: '<?php siteurl('images/map_marker_inside_chartreuse.png');?>',
                fill: '#00a65a',
                stroke: '#111'
            },
        },
        markers: markers.map(function (marker) {
            return {
                name: marker.title,
                latLng: marker.latLng
            }
        }),
        //series: {
        //    regions: [{
        //        values: gdpData,
        //        scale: ['#d2d6de', '#d2d6de'],
        //        normalizeFunction: 'polynomial'
        //    }],
        //},
        onMarkerClick: function (e, code) {
            //console.log(e);
            //console.log(code);
        },
        onMarkerTipShow: function (e, tip, code) {
            tip.html(tip.text());
            //console.log(e);
            //console.log(tip);
            //console.log(code);
        }
    };

    if ($container.attr("data-map-image")) {
        mapoptions.markerStyle.initial.image = $container.attr("data-map-image");
    }

    if (!$container.attr("data-map-simple")) {
        mapoptions.regionStyle.initial.fill = '#174f68';
        mapoptions.series = {
            regions: [{
                values: gdpData,
                scale: ['#C8EEFF', '#0071A4'],
                normalizeFunction: 'polynomial'
            }],
        }
    }
    map = new jvm.Map(mapoptions);
}

function AddMarker() {
    if (IpAddresses.length > 0) {
        var ipaddresses = [];
        for (var index = 0; index < 10; index++) {
            if (IpAddresses.length > 0)
                ipaddresses.push(IpAddresses.shift());
            else
                break;
        }
        //var IpAddress = IpAddresses.shift();
        $.get(siteurl, {
            "act": "get_ipaddresses_info",
            "ipaddresses": JSON.stringify(ipaddresses)
        },
            function (data, textStatus, jqXHR) {
                for (var index in data) {
                    var IpAddressInfo = data[index];
                    var marker = {
                        latLng: [IpAddressInfo.latitude, IpAddressInfo.longitude],
                        name: IpAddressInfo.IpAddress,
                        title: IpAddressInfo.title
                    };
                    map.addMarker(markerIndex, marker);
                    markerIndex += 1;
                }
                AddMarker();
            },
            "json"
        );
    }
}

function CreateDateRange($ele) {
    var url = $ele.attr("data-url");
    var startdate = $ele.attr("data-startdate");
    var enddate = $ele.attr("data-enddate");;

    if (startdate != "" && enddate != "")
        $ele.find('span').html(startdate + ' - ' + enddate);
    else
        $ele.find('span').html((moment().tz("est").subtract(6, 'days')).format('MM/DD/YYYY') + ' - ' + (moment().tz("est")).format('MM/DD/YYYY'));

    var daterangeoptions = {
        timeZone: "est",
        ranges: {
            'All': [moment().tz("est").subtract(100, 'years'), moment().tz("est")],
            'Today': [moment().tz("est").subtract(1, 'days'), moment().tz("est").subtract(1, 'days')],
            'Yesterday': [moment().tz("est").subtract(2, 'days'), moment().tz("est").subtract(2, 'days')],
            'Last 7 Days': [moment().tz("est").subtract(7, 'days'), moment().tz("est").subtract(1, 'days')],
            'Last 30 Days': [moment().tz("est").subtract(30, 'days'), moment().tz("est").subtract(1, 'days')],
            'This Month': [moment().tz("est").startOf('month'), moment().tz("est").endOf('month')],
            'Last Month': [moment().tz("est").subtract(1, 'month').startOf('month'), moment().tz("est").subtract(1, 'month').endOf('month')]
        }
    };

    if (startdate != "" && enddate != "") {
        daterangeoptions["startDate"] = startdate;
        daterangeoptions["endDate"] = enddate;
        //console.log(daterangeoptions);
    } else {
        daterangeoptions["startDate"] = moment().tz("est").subtract(100, 'years');
        daterangeoptions["endDate"] = moment().tz("est");
    }

    $ele.daterangepicker(
            daterangeoptions,
            function (start, end) {
                var url = $ele.attr("data-url");
                if (start.format('MM/DD/YYYY') != moment().tz("est").subtract(100, 'years').format('MM/DD/YYYY')) {
                    $ele.find('span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                    url += "?startdate=" + start.add(1, 'days').format('MM/DD/YYYY');
                    url += "&enddate=" + end.add(1, 'days').format('MM/DD/YYYY');
                }
                window.location.href = url;
            });
}

function ShowDetailRow(e) {
    e.preventDefault();

    var $this = $(this);
    var $tr = $this.closest("tr");
    var $table = $this.closest("table");
    var columns = $this.attr("data-colspan") != "undefined"?$this.attr("data-colspan"):$table.find("thead tr th").length;
    if (!$this.hasClass("detailopen")) {
        if (!$tr.next("tr").hasClass("detail_row") || $tr.next("tr").find(".fa-spinner").length > 0) {
            var url = $this.attr("data-url");
            var $trDetails = $('<tr class="detail_row" style="display:none;">' +
                '<td colspan="' + columns + '">' +
                    '<span class="small">' +
                        '<i class="fa fa-spinner"></i>&nbsp;' +
                        'Loading details....' +
                    '</span>' +
                '</td>' +
            '</tr>');

            $trDetails.insertAfter($tr);
            //$tr.after(trHtml);
            //var $trDetails = $tr.next("tr.detail_row");
            $trDetails.show("slow");

            $.get(url, "", function (data) {
                var jsonData = $.parseJSON(data);
                var $tdDetails = $tr.next("tr.detail_row").find("td");
                $tdDetails.html(jsonData["value"]);

                $tdDetails.find('.copy').each(function () {
                    var client = new ZeroClipboard($(this));
                    var $this = $(this);
                    client.on("ready", function (readyEvent) {
                        client.on("aftercopy", function (event) {
                            alert("Copied " + $this.attr("data-clipboard-text") + " to clipboard!");
                        });
                    });
                });

                $tdDetails.find(".data-table").each(function (index, element) {
                    var $this = $(this);
                    //console.log($this);
                    DataTable($this);
                });

                $tdDetails.find('[data-toggle="DetailRow"]').each(function (index, element) {
                    var $this = $(this);
                    $this.on("click", ShowDetailRow);
                });

                //$tdDetails.find(".select2").select2({ minimumResultsForSearch: Infinity });
                CreateSelect2($tdDetails.find(".select2"));

                CreateQTip2($tdDetails.find(".qtip2"));
            });
        } else {
            var $trDetails = $tr.next("tr.detail_row");
            $trDetails.show("slow");
        }
        $this.addClass("detailopen");
        $this.find("i.fa").removeClass("fa-caret-square-o-down").addClass("fa-caret-square-o-up");
    } else {
        var $trDetails = $tr.next("tr.detail_row");
        $trDetails.hide("slow");
        $this.removeClass("detailopen");
        $this.find("i.fa").removeClass("fa-caret-square-o-up").addClass("fa-caret-square-o-down");
        $trDetails.remove();
    }
}

function getVisibleLinkValues() {
    var data = {};
    var VisibleLink = $("#VisibleLink").val();
    var DomainID = $('#UseAdminDomain').is(":checked") ? $("#AdminDomainID").val() : $("#DomainID").val();

    data["act"] = "CheckVisibleLink";
    data["id"] = -1;
    if (CurrentVisibleLink != VisibleLink) {
        data["type"] = "linkbank";
        data["id"] = CurrentID;
        data["DomainID"] = DomainID;
        data["VisibleLink"] = VisibleLink;
    }

    return data;
}

function ShowAlertMessage(jsonData, PopupError) {
    var PageAlert;
    var $alert;
    var msg = typeof jsonData.msg == "undefined" ? jsonData.error : jsonData.msg;
    
    PageAlert = '';
    PageAlert += '<div class="alert alert-message ' + (jsonData.result == "failed"?" alert-danger ":" alert-success ") + ' alert-dismissable">';
    PageAlert += '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>';
    PageAlert += '<h4><i class="icon fa fa-ban"></i>Alert!</h4>';
    PageAlert += msg;
    PageAlert += '</div>';

    if (PopupError) {
        $EasyLinksModel.find(".alert").remove();
        $alert = $(PageAlert).prependTo($EasyLinksModel.find(".modal-body"));//.delay(5000).fadeOut(1000);
    } else {
        $alert = $(PageAlert).insertBefore($(".content-header"));
    }

    $alert.delay(5000);

    if (typeof jsonData.HideAlert == "undefined") {
        $alert.fadeOut(1000);
    } else {
        $alert.find(".close").hide();
    }
}

function GetPixelImage(PixelType, UnitPrice, QueryString) {
    var PixelURL = "http://easylinks.online/api/" + PixelType + "/pixel_" + userid + ".png?uid=" + userid;

    if (typeof UnitPrice != "undefined" && UnitPrice != "")
        PixelURL += "&p=" + UnitPrice;

    if (typeof QueryString != "undefined" && QueryString != "")
        PixelURL += "&" + QueryString;

    var PixelImage = '<img height="1" width="1" alt="" style="display:none" src="' + PixelURL + '" />';

    return PixelImage;
}

function CreateQTip2($ele) {
    $ele.qtip({
        content: {
            text: function (event, api) {
                var qtip_content = '<div class="qtip-content" id="qtip-content" aria-atomic="true"><img width="" height="" style="width: 100%;" class="photo" alt="" src=""></div>';
                var $content = $("<div/>").html(qtip_content);
                $content.find("img.photo").attr("src", api.elements.target.attr("data-qtip-image"));
                return $content.html();
            }
        },
        position: {
            viewport: $(window)
        },
        hide: {
            fixed: true,
            delay: 300
        },
        style: 'qtip-bootstrap'
    });
}

function CreateQtipSubMenu($ele) {
    var target = typeof $ele.attr("data-qtip-target") != "undefined" ? $ele.attr("data-qtip-target") : '.qtipsubmenu';
    var showevent = typeof $ele.attr("data-qtip-showevent") != "undefined" ? $ele.attr("data-qtip-showevent") : 'mouseenter';

    $ele.qtip({
        content: $ele.next(target),
        position: {
            viewport: $(window)
        },
        show: {
            //event: 'click',
            event: showevent,//'mouseenter',
            solo: true
        },
        hide: {
            event: 'unfocus',
            fixed: true,
            delay: 300
        },
        style: {
            classes: 'qtip-bootstrap qtip-submenu'
        }
    });

    $(".qtipsubmenu ul li a").on("click", function () {
        $(this).closest(".qtip").hide();
    });
}

function CreateQTipToolTip($ele) {
    $ele.qtip({
        position: {
            viewport: $(window)
        },
        hide: {
            fixed: false,
            delay: 300
        },
        style: 'qtip-youtube'//
    });
}

function CreateCopy($ele) {
    $ele.each(function (index, element) {
        var client = new ZeroClipboard($(this));
        var $this = $(this);
        client.on("ready", function (readyEvent) {
            client.on("aftercopy", function (event) {
                alert("Copied " + $this.attr("data-clipboard-text") + " to clipboard!");
            });
        });
    });
}
$(function () {
    $("[data-toggle=ShowQtipSubMenu]").each(function () {
        var $this = $(this);
        CreateQtipSubMenu($this);
    });

    $('.qtip2').each(function () {
        var $this = $(this);
        CreateQTip2($this);
    });

    $('[data-toggle="StatusForm"]').each(function (Index, element) {
        var $this = $(this);
        $this.on("click", function (e) {
            e.preventDefault();
            var $this = $(this);
            var $form = $this.next("form");

            $form.toggle();
        })
    });

    $('[data-toggle="DetailRow"]').each(function (index, element) {
        var $this = $(this);
        $this.on("click", ShowDetailRow);
    });

    $(".pxgradient").each(function () {
        var $this = $(this);
        var colors = $this.attr("data-gradient-colors").split(",");
        var step = $this.attr("data-gradient-step") ? $this.attr("data-gradient-step") : 5;
        var dir = $this.attr("data-gradient-dir") ? $this.attr("data-gradient-dir") : "x";

        $this.pxgradient({
            step: step,
            colors: colors,
            dir: dir
        });
    });

    $('.sparkbar').each(function () {
        var $this = $(this);
        $this.sparkline('html', {
            type: 'bar',
            height: $this.data('height') ? $this.data('height') : '25',
            barColor: $this.data('color')
        });
    });

    $('[data-toggle="qtiptooltip"]').each(function (index, element) {
        var $this = $(this);
        CreateQTipToolTip($this);
    });

    $(".btn-confirm").each(function (index, element) {
        var $this = $(this);
        $this.on("click", function (e) {
            if (!ConfirmContinue($this))
                e.preventDefault();
        });
    });

    $(".btn-delete").each(function (index, element) {
        var $this = $(this);
        $this.on("click", function (e) {
            if (!ConfirmDelete())
                e.preventDefault();
        });
    });

    $(".btn-reset").each(function (index, element) {
        var $this = $(this);
        $this.on("click", function (e) {
            if (!ConfirmReset())
                e.preventDefault();
        });
    });

    $(".ImageUploader").each(function (index, element) {
        var $this = $(this);
        CreateImageUploader($this);
    });

    $(".data-table").each(function (index, element) {
        var $this = $(this);
        DataTable($this);
    });

    $(".scrollable-div").each(function () {
        var $this = $(this);
        $this.mCustomScrollbar({
            theme: "dark"
        });
    });

    $('[data-toggle="customcollapse"]').on("click", function () {
        var $this = $(this);
        if ($this.attr("data-target")) {
            var $datatarget = $($this.attr("data-target"));
            if (!$datatarget.hasClass("in")) {
                $datatarget.collapse('show');
                $('html, body').animate({
                    scrollTop: $datatarget.offset().top
                }, 2000);
            } else {
                $datatarget.collapse('hide');
            }
        }
    });

    $('input[type="checkbox"].blue, input[type="radio"].blue').each(createblueicheck);

    CreateSelect2($('select.select2'));

    $('.btn-single-daterange').each(function () {
        var startdate = $(this).attr("data-value");

        $(this).daterangepicker({
            "startDate": startdate,
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoApply": true
        }, function (start, end) {
            $(this.element).attr("data-value", start.format('MM/DD/YYYY'));
            $($(this.element).attr("data-control")).val(start.format('MM/DD/YYYY'));
        });
    });

    $('.input-daterange').each(function () {
        var startdate = $(this).attr("data-start-date");
        var enddate = $(this).attr("data-end-date");

        $(this).daterangepicker({
            "startDate": startdate,
            "endDate": enddate,
            "showDropdowns": true,
        }, function (start, end) {
            $(this.element).attr("data-start-date", start.format('MM/DD/YYYY'))
            $(this.element).attr("data-end-date", end.format('MM/DD/YYYY'))
            //$($(this.element).attr("data-control")).val(start.format('MM/DD/YYYY'));
        });
    });

    $('.input-singledate').each(function () {
        //var startdate = $(this).attr("data-start-date");
        //var enddate = $(this).attr("data-end-date");

        $(this).daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoApply": true
        }, function (start, end) {

        });
    });

    CreateDateRangePicker($('.input-singledate-time'));
    $('.input-singledate-time--').each(function () {
        //var startdate = $(this).attr("data-start-date");
        //var enddate = $(this).attr("data-end-date");

        //$(this).daterangepicker({
        //    "singleDatePicker": true,
        //    "showDropdowns": true,
        //    "autoApply": false,
        //    "timePicker": true,
        //    "timePickerIncrement": 1,
        //    //"template": datetimetemplate,
        //    "locale": {
        //        format: 'MM/DD/YYYY h:mm A'
        //    }
        //}, function (start, end) {
        //    //var $this = $(this.element);
        //    //if ($this.attr("data-linked")) {
        //    //    var $linkbanked = $($this.attr("data-linked"));
        //    //    $linkbanked.val(start.format('MM/DD/YYYY h:mm A'));
        //    //}
        //});

        //$(this).on('apply.daterangepicker', function (ev, picker) {
        //    var start = picker.startDate;
        //    var $element = $(picker.element);
        //    if ($element.attr("data-linked")) {
        //        var $linkbanked = $($element.attr("data-linked"));
        //        $linkbanked.val(start.format('MM/DD/YYYY h:mm A'));
        //    }
        //});
    });

    $(document).on('click', '[data-toggle="collapse"]', function (e) {
        if ($(this).attr("aria-expanded") == "true") {
            if ($(this).find("i.fa-chevron-up"))
                $(this).find("i.fa-chevron-up").attr("class", "fa fa-chevron-down");
        } else {
            if ($(this).find("i.fa-chevron-down"))
                $(this).find("i.fa-chevron-down").attr("class", "fa fa-chevron-up");
        }
    });

    $(document).on('click', '[data-widget="hide"]', function (e) {
        $(this).closest(".box").collapse('hide');
        e.preventDefault();
    });

    $(document).on('click', '[data-widget="ShowLinkModel"]', function (e) {
        e.preventDefault();
        var $this = $(this);
        ShowEasyLinksModel($this);
    });

    $(document).on('click', '#EasyLinksModelSave', function (e) {
        //var $EasyLinksModel = $("#EasyLinksModel");
        var href = $ModelCaller.attr('href');
        //var $EasyLinksModel = $("#EasyLinksModel");
        var $form = $($EasyLinksModel.find(".modal-body").find("form"));
        var data = $form.serialize();
        var method = $ModelCaller.attr("data-method");

        //console.log(data); return;

        $form.validator('validate');
        if ($form.find(".has-error").length > 0) {
            $form.find(".has-error").eq(0).find('.form-control').focus()
            return;
        }

        if ($form.find(".token.invalid").length > 0) {
            $form.find(".tokenfield").each(function (index, element) {
                var $this = $(this);
                var $formgroup = $this.closest(".form-group");
                if ($formgroup.find(".token.invalid").length > 0) {
                    $formgroup.removeClass("has-success");
                    $formgroup.addClass("has-error");

                    $formgroup.find(".form-control-feedback").removeClass("glyphicon-ok");
                    $formgroup.find(".form-control-feedback").addClass("glyphicon-remove");
                }
            });
            $form.find(".tokenfield").eq(0).find('.form-control').focus()
            return;
        }

        if ($ModelCaller.attr("data-append") && ($ModelCaller.attr("data-append-object") || $ModelCaller.attr("data-related"))) {
            var $related = $($ModelCaller.attr("data-append-object") ? $ModelCaller.attr("data-append-object") : $ModelCaller.attr("data-related"));
            var selected = $related.val();
            if (typeof selected != typeof undefined && selected != "") {
                href += selected;
            } else {
                return;
            }
        }

        $.ajax({
            method: method,
            url: href,
            data: data
        }).done(function (data) {
            //$EasyLinksModel.modal('hide');
            var jsonData = $.parseJSON(data);
            var error = false;
            if (typeof jsonData.result != "undefined" && jsonData.result == "failed"){
                ShowAlertMessage(jsonData, true);
                error = true;
            }

            if(!error){
                $EasyLinksModel.modal('hide');

                if ($ModelCaller.attr("data-related")) {
                    var $related = $($ModelCaller.attr("data-related"));
                    var selected = $related.val();

                    $related.before(jsonData.before);
                    $related.html(jsonData.value);

                    if ($related.is("select")) {
                        $related.val(selected);
                        //$related.select2({
                        //    minimumResultsForSearch: Infinity
                        //});
                        CreateSelect2($related);
                    }
                    $related.after(jsonData.after);
                }
                eval(jsonData.jscode);
            }
        });
    });

    $(document).on('click', '[data-widget="RemoveOption"]', function (e) {
        e.preventDefault();
        var $this = $(this);
        var href = $this.attr('href');
        var data = $this.attr("data-values");
        var method = $this.attr("data-method");
        var $related = $($this.attr("data-related"));
        var selected = $related.is("select") ? $related.val() : "-1";

        if (confirm("Are you sure. You want to delete this?") == false) {
            return;
        }

        if (typeof selected == "undefined" || selected == null || selected == "")
            return;

        if ($this.attr("data-related") && $this.attr("data-append")) {
            if (typeof selected != "undefined" && selected != "")
                data += selected;
        }

        $.ajax({
            method: method,
            url: href,
            data: data
        }).done(function (data) {
            if ($this.attr("data-related")) {
                var $related = $($this.attr("data-related"));
                var jsonData = $.parseJSON(data);
                $related.before(jsonData.before);
                $related.html(jsonData.value);

                if ($related.is("select")) {
                    //$related.select2({
                    //    minimumResultsForSearch: Infinity
                    //});
                    CreateSelect2($related);
                }

                $related.after(jsonData.after);
                eval(jsonData.jscode);
            }
        });
    });

    $('.copy').each(function () {
        var client = new ZeroClipboard($(this));
        var $this = $(this);
        client.on("ready", function (readyEvent) {
            client.on("aftercopy", function (event) {
                alert("Copied " + $this.attr("data-clipboard-text") + " to clipboard!");
            });
        });
    });

    $('.pieChart').each(function (e) {
        $this = $(this);
        CreatePieChart($this);
    });

    $('.lineChart').each(function (e) {
        $this = $(this);
        CreateLineChart($this);
    });

    $('.barChart').each(function (e) {
        $this = $(this);
        CreateBarChart($this);
    });
});

$(window).on('load', function () {
    $('form[data-toggle="custom-validator"]').each(function () {
        var $form = $(this)
        $form.validator(ValidatorOptions);
    })
})