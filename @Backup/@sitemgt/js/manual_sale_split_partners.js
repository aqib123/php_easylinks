﻿/// <reference path="easylinks.js" />
$(document).ready(function () {
    $(".btn-save-partner-info").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $form = $this.closest("form");
        var data = $form.serialize();

        $form.validator('validate');
        if ($form.find(".has-error").length > 0) {
            $form.find(".has-error").eq(0).find('.form-control').focus()
            return;
        }

        $.pleasewait();

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            success: function (jsonData) {
                if (jsonData.result == "successfull") {
                    UpdateResults(null, jsonData.saleresult);
                }

                $(".alert-message").remove();
                if (typeof jsonData.errors != "undefined")
                    ShowAlertMessage(jsonData.errors, false);
            },
            complete: function (jqXHR, textStatus) {
                $.pleasewait('hide');
            }
        });
    });

    $('[data-toggle="ShowNote"]').each(function (index, element) {
        var $this = $(this);
        $this.on("click", function () {
            var $this = $(this);
            var $form = $this.closest("form");
            var $Note = $form.find(".note-form-group");

            $Note.toggle("fast");
        });
    });

    $(".btn-income, .btn-other_income, .btn-expense").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var $form = $this.closest("form");
        var data = $form.serialize();

        $form.validator('validate');
        if ($form.find(".has-error").length > 0 || $form.find(".name-form-group input").val() == "" || $form.find(".amount-form-group input").val() == "") {
            $form.find(".has-error").eq(0).find('.form-control').focus()
            return;
        }

        $.pleasewait();

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            success: function (jsonData) {
                if (jsonData.result == "successfull") {
                    $form.find("input[type=text], textarea").val("");
                    $form.find(".glyphicon-remove").removeClass("glyphicon-remove");
                    $form.find(".glyphicon-ok").removeClass("glyphicon-ok");

                    var $tabpane = $this.closest(".tab-pane");
                    var $manualsalelist = $tabpane.find("table.manual-sale-list tbody");
                    $manualsalelist.append(jsonData.row);
                    
                    var $trLast = $manualsalelist.find("tr").eq($manualsalelist.find("tr").length - 1);
                    $trLast.find(".btn-remove-income").on("click", RemoveIncome);
                    $trLast.find(".btn-remove-expense").on("click", RemoveExpense);
                    $trLast.find('[data-widget="ShowLinkModel"]').on("click", function (e) {
                        e.preventDefault();
                        var $this = $(this);
                        ShowEasyLinksModel($this);
                    });

                    var $tabcontent = $trLast.closest(".tab-content");
                    UpdateResults($tabcontent, jsonData.saleresult);
                }
            },
            complete: function (jqXHR, textStatus) {
                $.pleasewait('hide');
            }
        });
    });

    $('#isSplitPartnerSale').on('ifChanged', function (e) {
        var isSplitPartnerSale = $(this).is(":checked") ? "1" : "0";

        $.pleasewait();

        $.ajax({
            type: "POST",
            url: url,
            data: "act=update_issplitpartnersale&isSplitPartnerSale=" + isSplitPartnerSale + "&ManualSaleID=" + ManualSaleID,
            dataType: "json",
            success: function (jsonData) {
                $("#SplitPartnerOptions").toggle("fast");
                $("#PartnerID").val("");
            },
            complete: function (jqXHR, textStatus) {
                $.pleasewait('hide');
            }
        });
    });

    $(".btn-add-partner").on("click", function (e) {
        e.preventDefault();

        if ($("#PartnerID").val() != "") {
            $.pleasewait();

            $.ajax({
                type: "POST",
                url: url,
                data: "act=add_manualsalesplitpartner&SplitPartnerID=" + $("#PartnerID").val() + "&ManualSaleID=" + ManualSaleID,
                dataType: "json",
                success: function (jsonData) {
                    if (jsonData.result == "successfull") {
                        window.location.href = window.location.href;
                    }
                },
                complete: function (jqXHR, textStatus) {
                    $.pleasewait('hide');
                }
            });
        } else {
            alert("Select Split Partner First");
            $("#PartnerID").focus();
        }
    });

    //$(".btn-save-partner-info").on("click", function (e) {
    //    e.preventDefault();

    //    var $this = $(this);
    //    var $form = $this.closest("form");
    //    var data = $form.serialize();

    //    $form.validator('validate');
    //    if ($form.find(".has-error").length > 0) {
    //        $form.find(".has-error").eq(0).find('.form-control').focus()
    //        return;
    //    }

    //    $.pleasewait();

    //    $.ajax({
    //        type: "POST",
    //        url: url,
    //        data: data,
    //        dataType: "json",
    //        success: function (jsonData) {
    //            if (jsonData.result == "successfull") {
    //                window.location.href = window.location.href;
    //            }
    //        },
    //        complete: function (jqXHR, textStatus) {
    //            $.pleasewait('hide');
    //        }
    //    });
    //});

    $(".btn-remove-income").on("click", RemoveIncome);
    $(".btn-remove-expense").on("click", RemoveExpense);

    $(".remove-manual-sale-split-partner").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var ManualSaleSplitPartnerID = $this.attr("data-splitpartner-id");

        if (!ConfirmDelete())
            return;

        $.pleasewait();

        $.ajax({
            type: "POST",
            url: url,
            data: "act=remove_manualsalesplitpartner&ManualSaleSplitPartnerID=" + ManualSaleSplitPartnerID,
            dataType: "json",
            success: function (jsonData) {
                if (jsonData.result == "successfull") {
                    window.location.href = window.location.href;
                }
            },
            complete: function (jqXHR, textStatus) {
                $.pleasewait('hide');
            }
        });
    });

    //var TotalIncome = $("[name=TotalIncome]").val();
    //var TotalExpense = $("[name=TotalExpense]").val()

    //$(".TotalIncome").text(TotalIncome);
    //$(".TotalExpense").text(TotalExpense);
});

function RemoveIncome(e) {
    e.preventDefault();
    var $this = $(this);
    var IncomeID = $this.attr("data-income-id");
    var ManualSaleSplitPartnerID = $this.attr("data-splitpartner-id");

    if (!ConfirmDelete())
        return;

    $.pleasewait();

    $.ajax({
        type: "POST",
        url: url,
        data: "act=remove_income&IncomeID=" + IncomeID + "&ManualSaleSplitPartnerID=" + ManualSaleSplitPartnerID,
        dataType: "json",
        success: function (jsonData) {
            if (jsonData.result == "successfull") {
                var $tr = $this.closest("tr");
                var $tabcontent = $tr.closest(".tab-content");

                $tr.remove();
                UpdateResults($tabcontent, jsonData.saleresult);
            }
        },
        complete: function (jqXHR, textStatus) {
            $.pleasewait('hide');
        }
    });
}

function RemoveExpense(e) {
    e.preventDefault();
    var $this = $(this);
    var ExpenseID = $this.attr("data-expense-id");
    var ManualSaleSplitPartnerID = $this.attr("data-splitpartner-id");

    if (!ConfirmDelete())
        return;

    $.pleasewait();

    $.ajax({
        type: "POST",
        url: url,
        data: "act=remove_expense&ExpenseID=" + ExpenseID + "&ManualSaleSplitPartnerID=" + ManualSaleSplitPartnerID,
        dataType: "json",
        success: function (jsonData) {
            if (jsonData.result == "successfull") {
                var $tr = $this.closest("tr");
                var $tabcontent = $tr.closest(".tab-content");

                $tr.remove();
                UpdateResults($tabcontent, jsonData.saleresult);
            }
        },
        complete: function (jqXHR, textStatus) {
            $.pleasewait('hide');
        }
    });
}


function UpdateRow($ele, row, saleresult) {
    var $tr = $ele.closest("tr");
    var $tabcontent = $ele.closest(".tab-content");

    $tr.replaceWith(row);


    $tr.find(".btn-remove-income").on("click", RemoveIncome);
    $tr.find(".btn-remove-expense").on("click", RemoveExpense);
    $tr.find('[data-widget="ShowLinkModel"]').on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        ShowEasyLinksModel($this);
    });

    UpdateResults($tabcontent, saleresult);
}

function UpdateResults($tabcontent, saleresult) {
    for (var key in saleresult) {
        $("#" + key + " tbody").html(saleresult[key]);
    }
    //$tabcontent.find(".manual-sale-result tbody").html(saleresult);
    //var TotalIncome = $tabcontent.find("[name=TotalIncome]").val();
    //var TotalExpense = $tabcontent.find("[name=TotalExpense]").val();

    $(".TotalIncomeTop").text($(".TotalIncome:last").val());
    $(".TotalExpenseTop").text($(".TotalExpense:last").val());


    //$(".manual-sale-result").find('[data-widget="ShowLinkModel"]').on('click', function (e) {
    //    e.preventDefault();
    //    var $this = $(this);
    //    ShowEasyLinksModel($this);
    //});
}