﻿/// <reference path="_references.js" />
/// <reference path="easylinks.js" />

$(document).on("change", "#DomainID", createpaidtrafficurl);
$(document).on("keyup", "#VisibleLink", createpaidtrafficurl);
//$(document).on("keyup", "#UnitPriceSales", createpaidtraffictrackingpixel);
//$(document).on("change", "#EasyLinkID", GetEasyLinkURL);
$(document).on("change", "#VendorID", GetVendorStatsScore);
$(document).on("keyup", ".conversion_pixel_unit_price", createpaidtraffictrackingpixel);

$(document).ready(function () {
    createpaidtrafficurl();
    //createpaidtraffictrackingpixel();
    createpaidtrafficactionpixel();
    CreateImageUploader($("#PageImageButton"));
    //GetEasyLinkURL();

    $("#vendor-stats").knob({
        'format': function (value) {
            return value + '%';
        }
    });

    $('[data-toggle="custom-validator"]').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            if (!$('#UseAdminDomain').is(":checked") && $("#DomainID").val() == "") {
                $('#DomainID').trigger('input.bs.validator');
                e.preventDefault();
            }
        }
    });

    $(".paidtraffic_conversion_pixel_container").each(function (index, element) {
        var $this = $(this);
        var paidtraffic_conversion_pixel_id = $this.find(".paidtraffic_conversion_pixel_id").val();

        if (paidtraffic_conversion_pixel_id.indexOf("newpixel-") <= -1) {
            $this.find(".conversion_pixel_unit_price").trigger("keyup");
        }
    });

    $(".btn-remove-pixel").on("click", btn_remove_pixel_click);

    $(".btn-add-conversion-pixel").on("click", function (e) {
        e.preventDefault();
        PixelIndex += 1;

        var $this = $(this);
        var $boxbody = $this.closest(".box-body");
        var $new_conversion_pixel_box = $(PixelBox.replace(/{new_conversion_pixel_index}/ig, PixelIndex));

        $new_conversion_pixel_box.insertBefore($this);
        CreateSelect2($new_conversion_pixel_box.find('.select2'));
        $new_conversion_pixel_box.find(".btn-remove-pixel").on("click", btn_remove_pixel_click);
    });
});

$('#UseAdminDomain').on('ifChanged', function (e) {
    $("#DomainID").prop("disabled", $(this).is(":checked"));
    if ($(this).is(":checked")) {
        $("#AdminDomainID").val(admindomain["id"]);
        $("#DomainID").removeAttr('data-select');
    } else {
        $("#DomainID").attr('data-select', 'select');
    }

    createpaidtrafficurl(e);
});

$('#PartnerSplitAllowed, #TrackCommission').on('ifChanged', function (e) {
    var $this = $(this);
    if ($("#PartnerSplitAllowed").is(":checked") || $("#TrackCommission").is(":checked"))
        $("#ManualSalesEntry").val("1");
    else
        $("#ManualSalesEntry").val("0");
});

function createpaidtrafficurl(e) {
    var $paidtrafficurl = $("#paidtrafficurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")

    var Domain;
    var DomainURL;

    $paidtrafficurl.html("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0) {
        $paidtrafficurl.html(DomainURL);
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0) {
        $paidtrafficurl.html(DomainURL + VisibleLink);
    }

    //createpaidtraffictrackingpixel(e);
    createpaidtrafficactionpixel(e);
}

function createpaidtraffictrackingpixel(e) {
    var $this = $(this);
    var $paidtraffic_conversion_pixel_container = $this.closest(".paidtraffic_conversion_pixel_container");
    var paidtraffic_conversion_pixel_id = $paidtraffic_conversion_pixel_container.find(".paidtraffic_conversion_pixel_id").val();

    if (paidtraffic_conversion_pixel_id.indexOf("newpixel-") <= -1) {
        var $ConversionPixelTrackingCode = $paidtraffic_conversion_pixel_container.find(".ConversionPixelTrackingCode");
        var conversion_pixel_unit_price = $this.val();
        $ConversionPixelTrackingCode.val(GetPixelImage("sp", conversion_pixel_unit_price, "ptpid=" + paidtraffic_conversion_pixel_id));
    }
}

function createpaidtrafficactionpixel(e) {
    var $TrackingCodeForActions = $("#TrackingCodeForActions");

    $TrackingCodeForActions.val("");
    $TrackingCodeForActions.val(GetPixelImage("ap", ""));
}

function btn_remove_pixel_click(e) {
    e.preventDefault();
    if (!ConfirmDelete())
        return;

    var $this = $(this);
    var $paidtraffic_conversion_pixel_container = $this.closest(".paidtraffic_conversion_pixel_container");
    var $box_body = $paidtraffic_conversion_pixel_container.closest(".box-body");
    var paidtraffic_conversion_pixel_id = $paidtraffic_conversion_pixel_container.find(".paidtraffic_conversion_pixel_id").val();

    if (paidtraffic_conversion_pixel_id.indexOf("newpixel-") <= -1)
        $box_body.append('<input type="hidden" name="paidtraffic_conversion_pixel_delete[]" value="' + paidtraffic_conversion_pixel_id + '">');
    $paidtraffic_conversion_pixel_container.remove();
}

function createpaidtrafficactionpixel(e) {
    var $TrackingCodeForActions = $("#TrackingCodeForActions");

    $TrackingCodeForActions.val("");
    $TrackingCodeForActions.val(GetPixelImage("ap", ""));
}

function GetEasyLinkURL() {
    if ($("#EasyLinkID").val() != "") {
        var EasyLinkID = "linkbank_" + $("#EasyLinkID").val();
        if (typeof EasyLinkURLs[EasyLinkID] != "undefined") {
            var EasyLinkURL = EasyLinkURLs[EasyLinkID];
            $("#EasyLinkURL").val(EasyLinkURL);
        }
    }
}

function GetVendorStatsScore() {
    if ($("#VendorID").val() != "") {
        var VendorID = $("#VendorID").val();

        $.pleasewait();

        $.ajax({
            type: "get",
            url: siteurl,
            data: "act=get_vendor_stats_score&VendorID=" + VendorID,
            dataType: "json",
            success: function (jsonData) {
                if (jsonData.result == "successfull") {
                    $(".vendor-stats-scores").html(jsonData.vendorscore);
                    $(".vendor-stats-scores").find("#vendor-stats").knob({
                        'format': function (value) {
                            return value + '%';
                        }
                    });

                    $(".vendor-stats-scores").find(".data-table").each(function (index, element) {
                        var $this = $(this);
                        DataTable($this);
                    });
                }
            },
            complete: function (jqXHR, textStatus) {
                $.pleasewait('hide');
            }
        });
    } else {
        $(".vendor-stats-scores").html("");
    }
}