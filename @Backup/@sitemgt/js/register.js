﻿/// <reference path="_references.js" />

$(".register-form").on("submit", function (e) {
    e.preventDefault();
    
    $.pleasewait();

    var $form = $(this);
    var data = $form.serialize();

    $form.validator('validate');
    if ($form.find(".has-error").length > 0) {
        $form.find(".has-error").eq(0).find('.form-control').focus()
        return;
    }

    $form.parent().find(".alert").remove();

    $.ajax({
        type: "POST",
        url: siteurl,
        data: data,
        dataType: "json",
        success: function (response) {
            var $div = $("<div/>");
            var $h4 = $("<h4/>");

            if (!response.registered) {
                $div.attr("class", "alert alert-danger alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-ban"></i> Alert!</h4>');
            }else{
                $div.attr("class", "alert alert-success alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-check"></i> Alert!</h4>');
                window.location.href = siteurl + "login";
            }

            $.pleasewait('hide');
            $div.html(response.message).prepend($h4).insertBefore($form).delay(2000).fadeOut(5000);
        }
    });
});