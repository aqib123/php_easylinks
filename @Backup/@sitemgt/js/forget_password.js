﻿/// <reference path="_references.js" />

$(".forgetpassword-form").on("submit", function (e) {
    e.preventDefault();

    $.pleasewait();

    var $form = $(this);
    var data = $form.serialize();

    $form.parent().find(".alert").remove();

    $.ajax({
        type: "POST",
        url: siteurl,
        data: data,
        dataType: "json",
        success: function (response) {
            var $div = $("<div/>");
            var $h4 = $("<h4/>");

            if (!response.mailsent) {
                $div.attr("class", "alert alert-danger alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-ban"></i> Alert!</h4>');
            } else {
                $div.attr("class", "alert alert-success alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-check"></i> Alert!</h4>');
                //window.location.href = siteurl + "login";
            }

            $.pleasewait('hide');
            $div.html(response.message).prepend($h4).insertBefore($form).delay(2000).fadeOut(5000);
        }
    });
});

/// <reference path="_references.js" />

$(".resetpassword-form").on("submit", function (e) {
    e.preventDefault();

    $.pleasewait();

    var $form = $(this);
    var data = $form.serialize();

    $form.parent().find(".alert").remove();

    $.ajax({
        type: "POST",
        url: siteurl,
        data: data,
        dataType: "json",
        success: function (response) {
            var $div = $("<div/>");
            var $h4 = $("<h4/>");

            if (!response.reset) {
                $div.attr("class", "alert alert-danger alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-ban"></i> Alert!</h4>');
            } else {
                $div.attr("class", "alert alert-success alert-dismissable");
                $h4.html('<h4><i class="icon fa fa-check"></i> Alert!</h4>');
                window.location.href = siteurl + "login";
            }

            $.pleasewait('hide');
            $div.html(response.message).prepend($h4).insertBefore($form).delay(2000).fadeOut(5000);
        }
    });
});