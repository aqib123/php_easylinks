﻿/// <reference path="_references.js" />
/// <reference path="easylinks.js" />

$(document).on("change", "#DomainID", createlinkbankurl); 
$(document).on("keyup", "#VisibleLink", createlinkbankurl);
//$(document).on("keyup", "#UnitPriceSales", createlinktrackingpixel);
$(document).on("keyup", ".conversion_pixel_unit_price", createlinktrackingpixel);

$(document).ready(function () {
    createlinkbankurl();

    $(".linkbank_conversion_pixel_container").each(function (index, element) {
        var $this = $(this);
        var linkbank_conversion_pixel_id = $this.find(".linkbank_conversion_pixel_id").val();

        if (linkbank_conversion_pixel_id.indexOf("newpixel-") <= -1) {
            $this.find(".conversion_pixel_unit_price").trigger("keyup");
        }
    });

    //createlinktrackingpixel();
    createlinkactionpixel();
    CreateImageUploader($("#PageImageButton"));

    $('[data-toggle="custom-validator"]').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            if (!$('#UseAdminDomain').is(":checked") && $("#DomainID").val() == "") {
                $('#DomainID').trigger('input.bs.validator');
                e.preventDefault();
            }
        }
    });

    $(".btn-remove-pixel").on("click", btn_remove_pixel_click);

    $(".btn-add-conversion-pixel").on("click", function (e) {
        e.preventDefault();
        PixelIndex += 1;

        var $this = $(this);
        var $boxbody = $this.closest(".box-body");
        var $new_conversion_pixel_box = $(PixelBox.replace(/{new_conversion_pixel_index}/ig, PixelIndex));

        //$new_conversion_pixel_box.appendTo($boxbody);
        $new_conversion_pixel_box.insertBefore($this);
        CreateSelect2($new_conversion_pixel_box.find('.select2'));
        $new_conversion_pixel_box.find(".btn-remove-pixel").on("click", btn_remove_pixel_click);
    });
});

$('#UseAdminDomain').on('ifChanged', function (e) {
    $("#DomainID").prop("disabled", $(this).is(":checked"));
    if ($(this).is(":checked")) {
        $("#AdminDomainID").val(admindomain["id"]);
        $("#DomainID").removeAttr('data-select');
    } else {
        $("#DomainID").attr('data-select', 'select');
    }

    createlinkbankurl(e);
});

function createlinkbankurl(e) {
    var $linkbankurl = $("#linkurl");
    var VisibleLink = rtrim($("#VisibleLink").val(), "/")

    var Domain;
    var DomainURL;

    $('#VisibleLink').trigger('input.bs.validator');

    $linkbankurl.html("");

    if ($('#UseAdminDomain').is(":checked")) {
        if (admindomain["type"] == "admindomain")
            DomainURL = rtrim(admindomain["url"], "/").replace("://", "://" + username + ".") + "/";
        else
            DomainURL = rtrim(admindomain["url"], "/") + "/";
    } else {
        if ($("#DomainID").val() != "") {
            Domain = domains[$("#DomainID").val()];
            if (Domain["type"] == "admindomain")
                DomainURL = rtrim(Domain["url"], "/").replace("://", "://" + username + ".") + "/";
            else
                DomainURL = rtrim(Domain["url"], "/") + "/";
        } else {
            return;
        }
    }

    if (DomainURL.length > 0) {
        $linkbankurl.html(DomainURL);
    }

    if (DomainURL.length > 0 && VisibleLink.length > 0) {
        $linkbankurl.html(DomainURL + VisibleLink);
    }
}

function createlinktrackingpixel(e) {
    var $this = $(this);
    var $linkbank_conversion_pixel_container = $this.closest(".linkbank_conversion_pixel_container");
    var linkbank_conversion_pixel_id = $linkbank_conversion_pixel_container.find(".linkbank_conversion_pixel_id").val();

    if (linkbank_conversion_pixel_id.indexOf("newpixel-") <= -1) {
        var $ConversionPixelTrackingCode = $linkbank_conversion_pixel_container.find(".ConversionPixelTrackingCode");
        var conversion_pixel_unit_price = $this.val();
        $ConversionPixelTrackingCode.val(GetPixelImage("sp", conversion_pixel_unit_price, "lpid=" + linkbank_conversion_pixel_id));
    }
}

function createlinkactionpixel(e) {
    var $TrackingCodeForActions = $("#TrackingCodeForActions");

    $TrackingCodeForActions.val("");
    $TrackingCodeForActions.val(GetPixelImage("ap", ""));
}

function btn_remove_pixel_click(e) {
    e.preventDefault();
    if (!ConfirmDelete())
        return;

    var $this = $(this);
    var $linkbank_conversion_pixel_container = $this.closest(".linkbank_conversion_pixel_container");
    var $box_body = $linkbank_conversion_pixel_container.closest(".box-body");
    var linkbank_conversion_pixel_id = $linkbank_conversion_pixel_container.find(".linkbank_conversion_pixel_id").val();

    if (linkbank_conversion_pixel_id.indexOf("newpixel-") <= -1)
        $box_body.append('<input type="hidden" name="linkbank_conversion_pixel_delete[]" value="' + linkbank_conversion_pixel_id + '">');
    $linkbank_conversion_pixel_container.remove();
}
