<?php
error_reporting(0);
date_default_timezone_set('EST');
@session_start();


define('EMAIL_PATTERN', ' pattern=\'^(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$\' data-pattern-message="Email address not valid"');
define('URL_PATTERN', 'pattern=\'(http|https):\\/\\/[\\w-]+(\\.[\\w-]+)+([\\w.,@?^=%&amp;:#!\\(\\)\\/~+#-]*[\\w@?^=%&amp;\\/~+#-])$\' data-pattern-message="Please enter url including http:// or https://"');
//define('NAME_PATTERN', ' pattern="^[a-zA-Z0-9\\ \\-\\_\\\'\\.]{2,}$" data-pattern-message="Only alpha numeric character, space, -, _, \', . are allowed" data-minlength="2" data-minlength-error="Minimum 2 characters"');
define('NAME_PATTERN', ' pattern="^[a-zA-Z0-9-_\'.\s]{2,}$" data-pattern-message="Only alpha numeric character, space, -, _, \', . are allowed" data-minlength="2" data-minlength-error="Minimum 2 characters"');
define('VISIBLELINK_PATTERN', ' pattern="^[a-zA-Z0-9-]{2,}$" data-pattern-message="Only alpha numeric character, - are allowed" data-minlength="2" data-minlength-error="Minimum 2 characters"');
define('ALPHANUMERIC_PATTERN', 'pattern="^[a-zA-Z0-9]{4,}$" data-pattern-message="Only alpha numeric character are allowed" data-minlength="4" data-minlength-error="Minimum 4 characters"');
define('ALLALLOWED_PATTERN', ' pattern="^[a-zA-Z0-9\\s!@#$%^&*()_+\\-=\\[\\]{};\\\\\':\\\\|,.<>\\/?]{2,}$" data-pattern-message="Only alpha numeric and special character, space, -, _, \', . are allowed" data-minlength="2" data-minlength-error="Minimum 2 characters"');
define('DATE_PATTERN', 'pattern="^[0-9\\/]{10,10}$"');
define('FLOAT_PATTERN', ' pattern="^[0-9]*\.?[0-9]+$" data-pattern-message="Only float numbers"');
define('INT_PATTERN', 'pattern="^[0-9]{1,}$" data-pattern-message="Only numbers"');
define('MAXMINDFILE', 0);
define('MAXMINDWEB', 1);
define("PREVIEW_URL", "http://images.shrinktheweb.com/xino.php?stwembed=1&stwaccesskeyid=7ce9b34d59547ed&stwsize=lg&stwurl=");
define('DEFAULTPAGELENGTH', 10);
define('DEFAULTPAGES', 5);

global $db, $menu_items, $current_user;

include_once __DIR__."/config.php";
include_once BASE_DIR."includes/simple_html_dom.php";
include_once BASE_DIR."includes/functions/general.php";

include_once BASE_DIR."includes/db.php";
$db = new db(DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_CHARSET, DB_PREFIX);

$subdir = "functions/";
$include_files = get_files(BASE_DIR."/includes/".$subdir, BASE_DIR."/includes/".$subdir);
foreach ($include_files as $include_file){
    //if($include_file != )
	include_once $subdir.$include_file;
}

login_user();
$menu_items = is_user_loggedin()?$current_user->MenuItems:array();
$modules = is_user_loggedin()?$current_user->Modules:array();

if($_GET && isset($_GET['act'])){
    $get_act =  $_GET['act'];
    $subdir = "processget/";
    $include_files = get_files(BASE_DIR."/includes/".$subdir, BASE_DIR."/includes/".$subdir);
    foreach ($include_files as $include_file)
        include_once $subdir.$include_file;
}

$errors = array();
if($_POST && isset($_POST['act'])){
    $post_act =  $_POST['act'];
    $subdir = "processpost/";
    $include_files = get_files(BASE_DIR."/includes/".$subdir, BASE_DIR."/includes/".$subdir);
    foreach ($include_files as $include_file)
        include_once $subdir.$include_file;
}