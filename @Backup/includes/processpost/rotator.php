<?php
if($post_act == "save_rotator" || $post_act == "delete_rotator"){
    if($post_act == "save_rotator"){
        $data = array(
            "userid"            => $current_user->id,
            "RotatorName"       => $_POST['RotatorName'],
            "RotatorStatus"     => isset($_POST["RotatorStatus"])?$_POST["RotatorStatus"]:"pending",
            "GroupID"           => $_POST['GroupID'],
            "RotatorType"       => $_POST['RotatorType'],
            "RotatorIsSticky"   => isset($_POST['RotatorIsSticky'])?"1":"0",
            "VisibleLink"       => $_POST['VisibleLink'],
            "UseAdminDomain"    => isset($_POST["UseAdminDomain"]),
            "DomainID"          => isset($_POST["UseAdminDomain"])?$_POST["AdminDomainID"]:$_POST['DomainID'],
            "CloakURL"          => isset($_POST["CloakURL"]),
            "MasterCampaignID"  => $_POST['MasterCampaignID'],
            "DateAdded"         => strtotime("now")
        );

        if(isset($_GET['RotatorID']) && !empty($_GET['RotatorID'])){
            $sql = "select * from ".$db->rotators." where id=".parseInt($_GET['RotatorID']);
            $current_rotator = $db->get_row($sql);
            $update_rotator = true;
            if($current_rotator->VisibleLink != $data['VisibleLink'] || $current_rotator->DomainID != $data['DomainID']){
                if(!is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                    $errors["success"] = false;
                    $errors["msg"] = "Visible Link for selected Domain already exist";
                    $redirect = false;
                    $update_rotator = false;
                }
            }

            if($update_rotator){
                unset($data['DateAdded']);
                $db->update($db->rotators, $data, array("id" => parseInt($_GET['RotatorID'])));
                site_redirect("rotator/links/".$_GET['RotatorID']."/?success=true&msg=Rotator Updated Successfully");
                die;
            }
        }else{
            if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                if($db->insert($db->rotators, $data) && is_numeric($db->insert_id)){
                    site_redirect("rotator/links/".$db->insert_id."/?success=true&msg=Rotator Added Successfully");
                    die;
                }else{
                    $errors["success"] = false;
                    $errors["msg"] = "Rotator insertion failed";
                    $redirect = false;
                }
            }else{
                $errors["success"] = false;
                $errors["msg"] = "Visible Link for selected Domain already exist";
                $redirect = false;
            }
        }

    }
    
    if($post_act == "delete_rotator"){
        $db->delete($db->rotators, array("id" => parseInt($_POST['RotatorID'])));
    }        
    //die;
}else if($post_act == "save_rotatorlink" || $post_act == "delete_rotatorlink"){
    $jsonData = array("before"=>"", "value"=>"", "box" => "", "after"=>"", "jscode" => "");
    if($post_act == "save_rotatorlink"){
        $rotatorlinkposition = intval($_POST["rotatorlinkposition"]);
        if($_GET['rotatorlink_type'] == "") 
            $rotatorlink_type = "all";
        else
            $rotatorlink_type = $_GET['rotatorlink_type'];

        $data = array(
            "LinkBankID"            => $_POST['LinkBankID'],
            "RotatorLinkURL"        => $_POST['RotatorLinkURL'],
            "MaxClicks"             => $_POST['MaxClicks'],
            "StartDate"             => !empty($_POST['StartDate'])?strtotime($_POST['StartDate']):"",
            "EndDate"               => !empty($_POST['EndDate'])?strtotime($_POST['EndDate']):"",
            "RotatorLinkType"       => $_POST['RotatorLinkType']
        );
        if($db->update($db->rotator_links, $data, array("id" => parseInt($_POST['RotatorLinkID']))) > 0){
            $jsonData["value"] = "success";
            $jsonData["box"] = get_rotatorlink_box(parseInt($_POST['RotatorLinkID']), $rotatorlinkposition, $rotatorlink_type);
        }
    }
    
    if($post_act == "delete_rotatorlink"){
        if($db->delete($db->rotator_links, array("id" => parseInt($_POST['RotatorLinkID']))))
            $jsonData["value"] = "success";
    }   
    echo json_encode($jsonData);
    die;
}