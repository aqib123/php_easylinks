<?php
if($post_act == "add_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])){
        $MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
        $LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;
        $ManualSale = $db->get_row("select * from ".$db->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$current_user->id);

        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $IncomeName = $_POST["IncomeName"];
        $IncomeNote = $_POST["IncomeNote"];
        $IncomeAmount = $_POST["IncomeAmount"];

        $data = array(
            "ManualSaleSplitPartnerID"          => $ManualSaleSplitPartnerID,
            "IncomeName"                        => $IncomeName,
            "IncomeNote"                        => $IncomeNote,
            "IncomeAmount"                      => $IncomeAmount,
            "DateAdded"                         => strtotime("now"),
            "userid"                            => $current_user->id
        );
        if($db->insert($db->manualsale_incomes, $data) && is_numeric($db->insert_id)){
            $IncomeID = $db->insert_id;
            $jsonData["row"] = get_income_row($IncomeID);
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
            $jsonData["result"] = "successfull";
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "update_income"){
    $jsonData = array("result" => "", "row" => "", "jscode" => "");

    $type = ucwords($_GET["type"]);
    $table = $type == "Income"?$db->manualsale_incomes:($type == "OtherIncome"?$db->manualsale_other_incomes:$db->manualsale_expenses);
    $id = $_GET["id"];
    $ManualSaleSplitPartnerID = $_GET["ManualSaleSplitPartnerID"];
    $ManualSale = $db->get_var("select ManualSaleID from ".$db->manualsale_split_partners." where userid=".$current_user->id." and id=".$ManualSaleSplitPartnerID);

    $NameField = $type.Name;
    $AmountField = $type.Amount;
    $NoteField = $type.Note;

    $data = array(
        $NameField => $_POST[$NameField],
        $AmountField => $_POST[$AmountField],
        $NoteField => $_POST[$NoteField]
    );

    $db->update($table, $data, array("id" => $id, "userid" => $current_user->id));
    $jsonData["result"] = "successfull";
    $jsonData["row"] = $type == "Income"?get_income_row($id):get_expense_row($id);
    $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
    $jsonData["jscode"] = 'UpdateRow($ModelCaller, jsonData.row, jsonData.saleresult)';

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"]) && is_numeric($_POST["IncomeID"])){
        $IncomeID = $_POST["IncomeID"];
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ManualSale = $db->get_var("select ManualSaleID from ".$db->manualsale_split_partners." where userid=".$current_user->id." and id=".$ManualSaleSplitPartnerID);

        if($db->delete($db->manualsale_incomes, array("id" => $IncomeID, "userid" => $current_user->id))){
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "add_other_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])){
        $MasterCampaignID = isset($_GET["MasterCampaignID"])?$_GET["MasterCampaignID"]:0;
        $LinkBankID = isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:0;
        $ManualSale = $db->get_row("select * from ".$db->manualsales." where MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and userid=".$current_user->id);

        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $OtherIncomeName = $_POST["OtherIncomeName"];
        $OtherIncomeNote = $_POST["OtherIncomeNote"];
        $OtherIncomeAmount = $_POST["OtherIncomeAmount"];

        $data = array(
            "ManualSaleSplitPartnerID"          => $ManualSaleSplitPartnerID,
            "OtherIncomeName"                        => $OtherIncomeName,
            "OtherIncomeNote"                        => $OtherIncomeNote,
            "OtherIncomeAmount"                      => $OtherIncomeAmount,
            "DateAdded"                         => strtotime("now"),
            "userid"                            => $current_user->id
        );
        if($db->insert($db->manualsale_other_incomes, $data) && is_numeric($db->insert_id)){
            $OtherIncomeID = $db->insert_id;
            $jsonData["row"] = get_other_income_row($OtherIncomeID);
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
            $jsonData["result"] = "successfull";
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_other_income"){
    $jsonData = array("result" => "", "row" => "");

    if(isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"]) && is_numeric($_POST["OtherIncomeID"])){
        $OtherIncomeID = $_POST["OtherIncomeID"];
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $ManualSale = $db->get_var("select ManualSaleID from ".$db->manualsale_split_partners." where userid=".$current_user->id." and id=".$ManualSaleSplitPartnerID);

        if($db->delete($db->manualsale_other_incomes, array("id" => $OtherIncomeID, "userid" => $current_user->id))){
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
        }
    }

    echo json_encode($jsonData);
    die;
}