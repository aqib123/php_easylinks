<?php
if($post_act == "save_paidtraffic" || $post_act == "delete_paidtraffic"){
    $url = "paidtraffic/stats/";//get_site_url("paidtraffic/stats/");
    $redirect = true;
    if($post_act == "save_paidtraffic"){
        //print_r($_POST);die;
        $data = array(
            "userid"                                => $current_user->id,
            "VendorID"                              => $_POST['VendorID'],
            "PaidTrafficName"                            => $_POST['PaidTrafficName'],
            "DestinationURL"                        => $_POST['DestinationURL'],
            "PaidTrafficStatus"                          => isset($_POST["PaidTrafficStatus"])?$_POST["PaidTrafficStatus"]:"pending",
            "TrafficCost"                           => $_POST['TrafficCost'],
            "TrafficCostType"                       => $_POST['TrafficCostType'],
            "NoOfClicks"                            => $_POST['NoOfClicks'],
            "VisibleLink"                           => $_POST['VisibleLink'],
            "UseAdminDomain"                        => isset($_POST["UseAdminDomain"]),
            "DomainID"                              => isset($_POST["UseAdminDomain"])?$_POST["AdminDomainID"]:$_POST['DomainID'],
            "CloakURL"                              => isset($_POST["CloakURL"]),
            "GroupID"                               => $_POST['GroupID'],
            "StartDate"                             => !empty($_POST['StartDate'])?strtotime($_POST['StartDate']):"",
            "EndDate"                               => !empty($_POST['EndDate'])?strtotime($_POST['EndDate']):"",
            "PaidTrafficActive"                          => 0,
            "TrackingCodeForActions"                => $_POST['TrackingCodeForActions'],
            "PixelID"                               => $_POST['PixelID'],
            "EasyLinkID"                            => $_POST['EasyLinkID'],
            "AdditionalNotes"                       => $_POST['AdditionalNotes'],
            "PageImage"                             => $_POST['PageImage'],
            "PendingPageID"                         => $_POST['PendingPageID'],
            "CompletePageID"                        => $_POST['CompletePageID'],
            "MasterCampaignID"                      => $_POST['MasterCampaignID'],
            "DateAdded"                             => strtotime("now")
        );
        
        if(isset($_GET['PaidTrafficID'])){
            $sql = "select * from ".$db->paidtraffics." where id=".parseInt($_GET['PaidTrafficID']);
            $current_paidtraffic = $db->get_row($sql);
            $update_paidtraffic = true;
            if($current_paidtraffic->VisibleLink != $data['VisibleLink'] || $current_paidtraffic->DomainID != $data['DomainID']){
                if(!is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                    $errors["success"] = false;
                    $errors["msg"] = "Visible Link for selected Domain already exist";
                    $redirect = false;
                    $update_paidtraffic = false;
                }
            }

            if($update_paidtraffic){
                unset($data['DateAdded']);
                //$db->update($db->paidtraffics, $data, array("id" => parseInt($_GET['PaidTrafficID'])));
                //$url .= "?success=true&msg=Paid Traffic Updated Successfully";

                $db->update($db->paidtraffics, $data, array("id" => parseInt($_GET['PaidTrafficID'])));
                update_paidtraffic_conversion_pixels(parseInt($_GET['PaidTrafficID']));
                
                if($db->get_var("select count(*) from ".$db->paidtraffic_conversion_pixels." where userid=".$current_user->id." and PaidTrafficID=".parseInt($_GET['PaidTrafficID'])) > 0)
                    $url = "paidtraffic/?PaidTrafficID=".parseInt($_GET['PaidTrafficID']);
                else
                    $url .= "?success=true&msg=Paid Traffic Updated Successfully";
            }
        }else{
            if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                if($db->insert($db->paidtraffics, $data)){
                    //$url .= "?success=true&msg=Paid Traffic Added Successfully";

                    update_paidtraffic_conversion_pixels($db->insert_id);

                    if($db->get_var("select count(*) from ".$db->paidtraffic_conversion_pixels." where userid=".$current_user->id." and PaidTrafficID=".$db->insert_id) > 0)
                        $url = "paidtraffic/?PaidTrafficID=".$db->insert_id;
                    else
                        $url .= "?success=true&msg=Paid Traffic Added Successfully";
                }else{
                    $errors["success"] = false;
                    $errors["msg"] = "Paid Traffic insertion failed";
                    $redirect = false;
                }
            }else{
                $errors["success"] = false;
                $errors["msg"] = "Visible Link for selected Domain already exist";
                $redirect = false;
            }
        }
    }
    
    if($post_act == "delete_paidtraffic"){
        $db->delete($db->paidtraffics, array("id" => parseInt($_GET['PaidTrafficID'])));
        $url .= "?success=true&msg=Paid Traffic Deleted Successfully";
    }
    
    if($redirect){
        site_redirect($url);
        die;
    }
}