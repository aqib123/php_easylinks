<?php
if($post_act == "save_pixel" || $post_act == "delete_pixel"){
    $PixelID = 0;
    if($post_act == "save_pixel"){
        $data = array(
            "userid"  => $current_user->id,
            "PixelName"  => $_POST['PixelName'],
            "PixelCode"  => $_POST['PixelCode'],
            "PixelType"  => "",//$_GET['PixelType'],
            "DateAdded"  => strtotime("now")
        );
        
        if(isset($_GET['PixelID'])){
            unset($data['DateAdded']);
            $db->update($db->pixels, $data, array("id" => parseInt($_GET['PixelID'])));
            $PixelID = parseInt($_GET['PixelID']);
        }else{
            $db->insert($db->pixels, $data);
            $PixelID = $db->insert_id;
        }
    }
    
    if($post_act == "delete_pixel"){
        $db->delete($db->pixels, array("id" => parseInt($_POST['PixelID'])));
    }

    $sql = "select * from ".$db->pixels." where userid = ".$current_user->id." order by PixelName";
    $pixels = $db->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Pixel</option>';
    foreach($pixels as $pixel)
        $jsonData["value"] .= '<option value="'.$pixel->id.'" '.($pixel->id==$PixelID?' selected="selected" ':'').' >'.$pixel->PixelName.'</option>';
    echo json_encode($jsonData);
    
    die;
}