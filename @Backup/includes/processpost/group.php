<?php
if($post_act == "save_group" || $post_act == "delete_group"){
    $GroupID = 0;
    if($post_act == "save_group"){
        $data = array(
            "userid"  => $current_user->id,
            "GroupName"  => $_POST['GroupName'],
            "GroupType"  => $_GET['GroupType'],
            "DateAdded"  => strtotime("now")
        );
        
        if(isset($_GET['GroupID'])){
            unset($data['DateAdded']);
            $db->update($db->groups, $data, array("id" => parseInt($_GET['GroupID'])));
            $GroupID = parseInt($_GET['GroupID']);
        }else{
            $db->insert($db->groups, $data);
            $GroupID = $db->insert_id;
        }
    }
    
    if($post_act == "delete_group"){
        $db->delete($db->groups, array("id" => parseInt($_POST['GroupID'])));
    }

    $sql = "select * from ".$db->groups." where GroupType='".$_GET['GroupType']."' and userid = ".$current_user->id." order by GroupName";
    $groups = $db->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Group</option>';
    foreach($groups as $group)
        $jsonData["value"] .= '<option value="'.$group->id.'" '.($group->id == $GroupID?' selected="selected" ':'').' >'.$group->GroupName.'</option>';
    echo json_encode($jsonData);
    
    die;
}