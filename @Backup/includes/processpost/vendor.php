<?php
if($post_act == "save_vendor" || $post_act == "delete_vendor"){
    if($post_act == "save_vendor"){
        $VendorID = 0;
        $data = array(
            "userid"            => $current_user->id,
            "VendorName"        => $_POST['VendorName'],
            "VendorType"        => $_GET['VendorType'],
            "DateAdded"         => strtotime("now"),
            "VendorPicture"     => $_POST['VendorPicture'],
            "WebsiteUrl"        => $_POST['WebsiteUrl'],
            "SkypeName"         => $_POST['SkypeName'],
            "LinkedIn"          => $_POST['LinkedIn'],
            "FacebookID"        => $_POST['FacebookID'],
            "EmailAddress"      => $_POST['EmailAddress'],
            "AdditionalNotes"   => $_POST['AdditionalNotes'],
            "VendorTags"        => $_POST['VendorTags']
        );
        
        if(isset($_GET['VendorID'])){
            unset($data['DateAdded']);
            $db->update($db->vendors, $data, array("id" => parseInt($_GET['VendorID'])));
            $VendorID = parseInt($_GET['VendorID']);
        }else{
            $db->insert($db->vendors, $data);
            $VendorID = $db->insert_id;
        }
    }
    
    if($post_act == "delete_vendor"){
        $db->delete($db->vendors, array("id" => parseInt($_POST['VendorID'])));
    }

    $sql = "select * from ".$db->vendors." where VendorType='".$_GET['VendorType']."' and userid = ".$current_user->id." order by VendorName";
    $vendors = $db->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Vendor</option>';
    foreach($vendors as $vendor)
        $jsonData["value"] .= '<option value="'.$vendor->id.'" '.($vendor->id == $VendorID?' selected="selected" ':'').' >'.$vendor->VendorName.'</option>';
    echo json_encode($jsonData);

    die;
}