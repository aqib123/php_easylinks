<?php
if($post_act == "save_link_sequence" || $post_act == "delete_link_sequence"){
    if($post_act == "save_link_sequence"){
        $data = array(
            "userid"                        => $current_user->id,
            "LinkSequenceName"              => $_POST['LinkSequenceName'],
            "LinkSequenceStatus"            => isset($_POST["LinkSequenceStatus"])?$_POST["LinkSequenceStatus"]:"pending",
            "GroupID"                       => $_POST['GroupID'],
            "VisibleLink"                   => $_POST['VisibleLink'],
            "UseAdminDomain"                => isset($_POST["UseAdminDomain"]),
            "DomainID"                      => isset($_POST["UseAdminDomain"])?$_POST["AdminDomainID"]:$_POST['DomainID'],
            "CloakURL"                      => isset($_POST["CloakURL"]),
            "MasterCampaignID"              => $_POST['MasterCampaignID'],
            "DateAdded"                     => strtotime("now")
        );

        if($data["LinkSequenceStatus"] == "pending"){
            $data["StartDate"] = "";
            $data["EndDate"] = "";
        } else if($data["LinkSequenceStatus"] == "active") {
            $data["StartDate"] = strtotime("now");
            $data["EndDate"] = "";
        } else if($data["LinkSequenceStatus"] == "complete") {
            $data["EndDate"] = strtotime("now");
        }

        if(isset($_GET['LinkSequenceID']) && !empty($_GET['LinkSequenceID'])){
            $sql = "select * from ".$db->link_sequences." where id=".parseInt($_GET['LinkSequenceID']);
            $current_link_sequence = $db->get_row($sql);
            $update_link_sequence = true;
            if($current_link_sequence->VisibleLink != $data['VisibleLink'] || $current_link_sequence->DomainID != $data['DomainID']){
                if(!is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                    $errors["success"] = false;
                    $errors["msg"] = "Visible Link for selected Domain already exist";
                    $redirect = false;
                    $update_link_sequence = false;
                }
            }

            if($update_link_sequence){
                unset($data['DateAdded']);
                if($current_link_sequence->LinkSequenceStatus == $data["LinkSequenceStatus"]){
                    unset($data['StartDate']);
                    unset($data['EndDate']);
                }

                $db->update($db->link_sequences, $data, array("id" => parseInt($_GET['LinkSequenceID'])));
                site_redirect("link-sequence/links/".$_GET['LinkSequenceID']."/?success=true&msg=Link Sequence Updated Successfully");
                die;
            }
        }else{
            if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                if($db->insert($db->link_sequences, $data) && is_numeric($db->insert_id)){
                    site_redirect("link-sequence/links/".$db->insert_id."/?success=true&msg=Link Sequence Added Successfully");
                    die;
                }else{
                    $errors["success"] = false;
                    $errors["msg"] = "Link Sequence insertion failed";
                    $redirect = false;
                }
            }else{
                $errors["success"] = false;
                $errors["msg"] = "Visible Link for selected Domain already exist";
                $redirect = false;
            }
        }

    }
    
    if($post_act == "delete_link_sequence"){
        $db->delete($db->link_sequences, array("id" => parseInt($_POST['LinkSequenceID'])));
    }        
    //die;
}else if($post_act == "save_link_sequence_link" || $post_act == "delete_link_sequence_link"){
    $jsonData = array("before"=>"", "value"=>"", "box" => "", "after"=>"", "jscode" => "");
    if($post_act == "save_link_sequence_link"){
        $link_sequence_linkposition = intval($_POST["link_sequence_linkposition"]);
        if($_GET['link_sequence_link_type'] == "") 
            $link_sequence_link_type = "all";
        else
            $link_sequence_link_type = $_GET['link_sequence_link_type'];

        $data = array(
            "LinkBankID"                        => $_POST['LinkBankID'],
            "LinkSequenceLinkName"              => $_POST['LinkSequenceLinkName'],
            "LinkSequenceLinkURL"               => $_POST['LinkSequenceLinkURL'],
            "EndDateType"                       => $_POST['EndDateType'],
            "EndDate"                           => !empty($_POST['EndDateInput'])?strtotime($_POST['EndDateInput']):"",
            "EndTime"                           => $_POST['Days']."/".$_POST['Hours']."/".$_POST['Minutes'],
            "LinkSequenceLinkType"              => $_POST['LinkSequenceLinkType']
        );
        if($db->update($db->link_sequence_links, $data, array("id" => parseInt($_POST['LinkSequenceLinkID']))) > 0){
            $jsonData["value"] = "success";
            $jsonData["box"] = get_link_sequence_link_box(parseInt($_POST['LinkSequenceLinkID']), $link_sequence_linkposition, $link_sequence_link_type);
        }
    }
    
    if($post_act == "delete_link_sequence_link"){
        if($db->delete($db->link_sequence_links, array("id" => parseInt($_POST['LinkSequenceLinkID']))))
            $jsonData["value"] = "success";
    }   
    echo json_encode($jsonData);
    die;
}