<?php
if($post_act == "update_manual_sale"){

    if((isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])) && is_numeric($_POST["ManualSaleID"])){
        $ManualSaleID = $_POST["ManualSaleID"];
        $isSplitPartnerSale = isset($_POST["isSplitPartnerSale"]);

        $db->update(
            $db->manualsales, 
            array(
                "ManualSaleName" => $_POST["ManualSaleName"],
                "isSplitPartnerSale" => $isSplitPartnerSale
            ), 
            array("id" => $ManualSaleID, "userid" => $current_user->id)
        );
        
        site_redirect("manual-sale/split-partners/?".(isset($_GET["MasterCampaignID"])?"MasterCampaignID=".$_GET["MasterCampaignID"]:"LinkBankID=".$_GET["LinkBankID"]));
        die;
    }
} else if($post_act == "add_manualsalesplitpartner"){
    $jsonData = array("result" => "", "row" => "");

    if((isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])) && is_numeric($_POST["SplitPartnerID"]) && is_numeric($_POST["ManualSaleID"])){
        $ManualSaleID = $_POST["ManualSaleID"];
        $SplitPartnerID = $_POST["SplitPartnerID"];
        $IncomePercentage = intval($_POST["IncomePercentage"]);
        $OtherIncomePercentage = intval($_POST["OtherIncomePercentage"]);
        $ExpensePercentage = intval($_POST["ExpensePercentage"]);
        $SplitPartner = $db->get_row("select * from ".$db->manualsale_split_partners." where isUserSale = 0  and  userid=".$current_user->id." and ManualSaleID=".$ManualSaleID." and SplitPartnerID=".$SplitPartnerID);
        if(!$SplitPartner){
            $data = array(
                "ManualSaleID"          => $ManualSaleID,
                "SplitPartnerID"        => $SplitPartnerID,
                "isUserSale"            => 0,
                "IncomePercentage"      => $IncomePercentage,
                "OtherIncomePercentage" => $OtherIncomePercentage,
                "ExpensePercentage"     => $ExpensePercentage,
                "DateAdded"             => strtotime("now"),
                "userid"                => $current_user->id
            );
            if($db->insert($db->manualsale_split_partners, $data) && is_numeric($db->insert_id)){
                $SplitPartners = $db->get_var("select count(*) from ".$db->manualsale_split_partners." where userid=".$current_user->id." and ManualSaleID=".$ManualSaleID);
                if($SplitPartners == 2){
                    $db->query("update ".$db->manualsale_split_partners." set IncomePercentage = (100 - ".$IncomePercentage."), OtherIncomePercentage = (100 - ".$OtherIncomePercentage."), ExpensePercentage = (100 - ".$ExpensePercentage.") where isUserSale = 1  and  userid=".$current_user->id." and ManualSaleID=".$ManualSaleID);
                }
                $ManualSaleSplitPartnerID = $db->insert_id;
                $jsonData["result"] = "successfull";
                $jsonData["row"] = get_manual_sale_split_partner_item($ManualSaleSplitPartnerID);
            }
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "update_manualsalesplitpartner"){
    $jsonData = array("result" => "", "row" => "");

    if((isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])) && is_numeric($_POST["ManualSaleSplitPartnerID"]) && is_numeric($_POST["IncomePercentage"]) && is_numeric($_POST["OtherIncomePercentage"]) && is_numeric($_POST["ExpensePercentage"])){
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];
        $IncomePercentage = $_POST["IncomePercentage"];
        $OtherIncomePercentage = $_POST["OtherIncomePercentage"];
        $ExpensePercentage = $_POST["ExpensePercentage"];

        $ManualSaleSplitPartner = $db->get_row("select * from ".$db->manualsale_split_partners." where userid=".$current_user->id." and id=".$ManualSaleSplitPartnerID);//isUserSale = 0  and  
        if($ManualSaleSplitPartner){
            $data = array(
                "IncomePercentage"      => $IncomePercentage,
                "OtherIncomePercentage" => $OtherIncomePercentage,
                "ExpensePercentage"     => $ExpensePercentage,
            );
            $db->update(
                $db->manualsale_split_partners, 
                $data,
                array(
                    //"isUserSale" => 0,
                    "userid" => $current_user->id,
                    "id" => $ManualSaleSplitPartnerID
                ));
            $jsonData["result"] = "successfull";
            $jsonData["saleresult"] = get_manual_sale_result($ManualSaleSplitPartner->ManualSaleID);

            if(!is_percentage_valid($ManualSaleSplitPartner->ManualSaleID)){
                $jsonData["errors"] = array(
                    'result' => "failed",
                    "HideAlert" => false,
                    "msg" => "Both Income and Expense Split Ratios Must Add Up To 100(%) Each. Before You Can View Your Reports!"
                );
            }
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "remove_manualsalesplitpartner"){
    $jsonData = array("result" => "", "row" => "");

    if((isset($_GET["MasterCampaignID"]) || isset($_GET["LinkBankID"])) && is_numeric($_POST["ManualSaleSplitPartnerID"])){
        $ManualSaleSplitPartnerID = $_POST["ManualSaleSplitPartnerID"];

        $ManualSaleSplitPartner = $db->get_row("select * from ".$db->manualsale_split_partners." where isUserSale = 0  and  userid=".$current_user->id." and id=".$ManualSaleSplitPartnerID);
        if($ManualSaleSplitPartner){
            if($db->delete($db->manualsale_split_partners, array("isUserSale" => 0,"userid" => $current_user->id,"id" => $ManualSaleSplitPartnerID))){
                $db->query("update ".$db->manualsale_split_partners." set IncomePercentage = (IncomePercentage + ".$ManualSaleSplitPartner->IncomePercentage."), OtherIncomePercentage = (OtherIncomePercentage + ".$ManualSaleSplitPartner->OtherIncomePercentage."), ExpensePercentage = (ExpensePercentage + ".$ManualSaleSplitPartner->ExpensePercentage.") where isUserSale = 1  and  userid=".$current_user->id." and ManualSaleID=".$ManualSaleSplitPartner->ManualSaleID);

                $db->delete($db->manualsale_incomes, array("userid" => $current_user->id ,"ManualSaleSplitPartnerID" => $ManualSaleSplitPartnerID));
                $db->delete($db->manualsale_other_incomes, array("userid" => $current_user->id ,"ManualSaleSplitPartnerID" => $ManualSaleSplitPartnerID));
                $db->delete($db->manualsale_expenses, array("userid" => $current_user->id ,"ManualSaleSplitPartnerID" => $ManualSaleSplitPartnerID));
                $jsonData["result"] = "successfull";
                $jsonData["saleresult"] = get_manual_sale_result($ManualSaleSplitPartner->ManualSaleID);

                if(!is_percentage_valid($ManualSaleSplitPartner->ManualSaleID)){
                    $jsonData["errors"] = array(
                        'result' => "failed",
                        "HideAlert" => false,
                        "msg" => "Both Income and Expense Split Ratios Must Add Up To 100(%) Each. Before You Can View Your Reports!"
                    );
                }
            }
        }
    }

    echo json_encode($jsonData);
    die;
} else if($post_act == "save_split_partner_payment"){
    $jsonData = array("result" => "", "saleresult" => "");
    if(!empty($_GET["ManualSaleID"]) && !empty($_GET["SenderID"]) && !empty($_GET["ReceiverID"]) && is_numeric($_GET["SentAmount"]) && !empty($_POST["ReferenceNo"])){
        $ManualSale = $db->get_row("select * from ".$db->manualsales." where userid=".$current_user->id." and id=".$_GET["ManualSaleID"]);
        if($ManualSale){
            $PartnerPayment = $db->get_row("select * from ".$db->manualsale_split_partner_payments." where userid=".$current_user->id." and SenderManualSaleSplitPartnerID=".$_GET["SenderID"]." and ReceiverManualSaleSplitPartnerID=".$_GET["ReceiverID"]);
            if(!$PartnerPayment){
                $db->insert($db->manualsale_split_partner_payments, array(
                    "SenderManualSaleSplitPartnerID"            => $_GET["SenderID"],
                    "ReceiverManualSaleSplitPartnerID"          => $_GET["ReceiverID"],
                    "SentAmount"                                => $_GET["SentAmount"],
                    "ReferenceNo"                               => $_POST["ReferenceNo"],
                    "DateAdded"                                 => strtotime("now"),
                    "userid"                                    => $current_user->id
                ));
                if(is_numeric($db->insert_id)){
                    $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
                    $jsonData["result"] = "successfull";
                    $jsonData["jscode"] = 'UpdateResults(null, jsonData.saleresult)';
                }
            }else {
                $db->update($db->manualsale_split_partner_payments, array("ReferenceNo" => $_POST["ReferenceNo"]), array(
                    "SenderManualSaleSplitPartnerID"            => $_GET["SenderID"],
                    "ReceiverManualSaleSplitPartnerID"          => $_GET["ReceiverID"],
                    "id"                                        => $PartnerPayment->id
                ));
                $jsonData["saleresult"] = get_manual_sale_result($ManualSale);
                $jsonData["result"] = "successfull";
                $jsonData["jscode"] = 'UpdateResults(null, jsonData.saleresult)';
            }
        }
    }
    echo json_encode($jsonData);
    die;
} else if($post_act == "save_split_partner_payments"){
    $jsonData = array("result" => "", "saleresult" => "");
    if(!empty($_GET["ManualSaleID"])){
        $ManualSale = $db->get_row("select * from ".$db->manualsales." where userid=".$current_user->id." and id=".$_GET["ManualSaleID"]);
        if($ManualSale){
            $UserResultValues = get_manual_sale_result_values($ManualSale);
            if($UserResultValues){
                foreach ($UserResultValues as $UserResultKey => $UserResultValue){
                    if($UserResultValue['isSplitPartnerSale'] == 1 && count($UserResultValues) > 1 && $UserResultValue['UserBalance'] != 0 && $UserResultValue['UserBalance'] < 0){
                        foreach ($UserResultValue['SendTo'] as $SendToUser){
                            $ReceiverResultValue = $UserResultValues["manualsale_split_partner_".$SendToUser["ReceiverID"]];
                            if($SendToUser["Amount"] != 0){
                                $ReferenceFieldID = "ReferenceNo_".$UserResultValue["SplitPartnerID"]."_".$SendToUser["ReceiverID"];
                                if(isset($_POST[$ReferenceFieldID]) &&  !empty($_POST[$ReferenceFieldID])){
                                    $PartnerPayment = $db->get_row("select * from ".$db->manualsale_split_partner_payments." where userid=".$current_user->id." and SenderManualSaleSplitPartnerID=".$UserResultValue["SplitPartnerID"]." and ReceiverManualSaleSplitPartnerID=".$SendToUser["ReceiverID"]);
                                    if(!$PartnerPayment){
                                        $db->insert($db->manualsale_split_partner_payments, array(
                                            "SenderManualSaleSplitPartnerID"            => $UserResultValue["SplitPartnerID"],
                                            "ReceiverManualSaleSplitPartnerID"          => $SendToUser["ReceiverID"],
                                            "SentAmount"                                => floatval($SendToUser["Amount"]),
                                            "ReferenceNo"                               => $_POST[$ReferenceFieldID],
                                            "DateAdded"                                 => strtotime("now"),
                                            "userid"                                    => $current_user->id
                                        ));
                                    }else {
                                        $db->update($db->manualsale_split_partner_payments, array("ReferenceNo" => $_POST[$ReferenceFieldID]), array(
                                            "SenderManualSaleSplitPartnerID"            => $UserResultValue["SplitPartnerID"],
                                            "ReceiverManualSaleSplitPartnerID"          => $SendToUser["ReceiverID"],
                                            "id"                                        => $PartnerPayment->id
                                        ));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    echo json_encode($jsonData);
    die;
}