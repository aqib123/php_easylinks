<?php
if($post_act == "save_link" || $post_act == "delete_link"){
    $url = "linkbank/stats/";
    $redirect = true;
    if($post_act == "save_link"){
        $data = array(
            "userid"                                => $current_user->id,
            "VendorID"                              => $_POST['VendorID'],
            "LinkName"                              => $_POST['LinkName'],
            "DestinationURL"                        => $_POST['DestinationURL'],
            "VisibleLink"                           => $_POST['VisibleLink'],
            "UseAdminDomain"                        => isset($_POST["UseAdminDomain"]),
            "DomainID"                              => isset($_POST["UseAdminDomain"])?$_POST["AdminDomainID"]:$_POST['DomainID'],
            "CloakURL"                              => isset($_POST["CloakURL"]),
            "TrackEPC"                              => isset($_POST["TrackEPC"]),
            "GroupID"                               => $_POST['GroupID'],
            "StartDate"                             => !empty($_POST['StartDate'])?strtotime($_POST['StartDate']):"",
            "EndDate"                               => !empty($_POST['EndDate'])?strtotime($_POST['EndDate']):"",
            "PendingPageID"                         => $_POST['PendingPageID'],
            "CompletePageID"                        => $_POST['CompletePageID'],
            "AdditionalNotes"                       => $_POST['AdditionalNotes'],
            "PageImage"                             => $_POST['PageImage'],
            "LinkStatus"                            => isset($_POST["LinkStatus"])?$_POST["LinkStatus"]:"pending",
            "LinkActive"                            => 0,
            "SalesConversions"                      => 0,
            "UnitPriceSales"                        => $_POST['UnitPriceSales'],
            "RedirectAfterLinkExpired"              => "",
            "PixelID"                               => $_POST['PixelID'],
            //"TrackingCodeForActions"                => $_POST['TrackingCodeForActions'],
            "TrackingCodeForSalesAndConversions"    => $_POST['TrackingCodeForSalesAndConversions'],
            "AffiliatePlatformID"                   => $_POST['AffiliatePlatformID'],
            "MasterCampaignID"                      => $_POST['MasterCampaignID'],
            "DateAdded"                             => strtotime("now")
        );

        if(isset($_GET['LinkBankID'])){
            $sql = "select * from ".$db->linkbanks." where id=".parseInt($_GET['LinkBankID']);
            $current_linkbank = $db->get_row($sql);
            $update_linkbank = true;
            if($current_linkbank->VisibleLink != $data['VisibleLink'] || $current_linkbank->DomainID != $data['DomainID']){
                if(!is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                    $errors["success"] = false;
                    $errors["msg"] = "Visible Link for selected Domain already exist";
                    $redirect = false;
                    $update_linkbank = false;
                }
            }

            if($update_linkbank){
                unset($data['DateAdded']);
                $db->update($db->linkbanks, $data, array("id" => parseInt($_GET['LinkBankID'])));
                update_linkbank_conversion_pixels(parseInt($_GET['LinkBankID']));
                
                if($db->get_var("select count(*) from ".$db->linkbank_conversion_pixels." where userid=".$current_user->id." and LinkBankID=".parseInt($_GET['LinkBankID'])) > 0)
                    $url = "linkbank/stats/conv";//?LinkBankID=".parseInt($_GET['LinkBankID']);
                else
                    $url .= "?success=true&msg=Link Updated Successfully";
            }
        }else{
            if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'])){
                if($db->insert($db->linkbanks, $data)){
                    update_linkbank_conversion_pixels($db->insert_id);

                    if($db->get_var("select count(*) from ".$db->linkbank_conversion_pixels." where userid=".$current_user->id." and LinkBankID=".$db->insert_id) > 0)
                        $url = "linkbank/stats/conv";//?LinkBankID=".$db->insert_id;
                    else
                        $url .= "?success=true&msg=Link Added Successfully";
                } else{
                    $errors["success"] = false;
                    $errors["msg"] = "Link insertion failed";
                    $redirect = false;
                }
            }else{
                $errors["success"] = false;
                $errors["msg"] = "Visible Link for selected Domain already exist";
                $redirect = false;
            }
        }
    }
    
    if($post_act == "delete_link"){
        if($db->delete($db->linkbanks, array("id" => parseInt($_GET['LinkBankID']))))
            $url .= "?success=true&msg=Link Deleted Successfully";
        else
            $url .= "?success=false&msg=Link Deleted Failed";
    }
    
    if($redirect){
        site_redirect($url);
        die;
    }
}else if($post_act == "save_saleinfo"){
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!empty($_GET["type"]) && !empty($_GET["id"])){
        $id = parseInt($_GET['id']);
        $type = $_GET["type"];
        $maintable = $type."s";
        $data = array(
            "SaleQuantity"          => $_POST['SaleQuantity'],
            "GrossSale"             => $_POST['GrossSale'],
            "ExtraIncome"           => $_POST['ExtraIncome'],
            "Expenses"              => $_POST['Expenses'],
            "Commission"            => $_POST['Commission'],
            "PartnerSplit"          => $_POST['PartnerSplit'],
            "AdditionalNotes"       => $_POST['AdditionalNotes'],
            "OwedToPartner"         => $_POST['OwedToPartner'],
            "SaleInfoEntered"       => strtotime("now")
        );
        
        $db->update($db->$maintable, $data, array("id" => $id));

        $saleinfotable = $type."_saleinfo";
        $linkbankedid = $type == "linkbank" ? "LinkBankID" : "CampaignID";
        $db->delete($db->$saleinfotable, array($linkbankedid => $id));
        if(isset($_POST["SaleInfoIndex"]) && is_array($_POST["SaleInfoIndex"])){
            foreach ($_POST["SaleInfoIndex"] as $index){
                $SaleInfoType = $_POST["SaleInfoType-".$index];
                $SaleInfoTitle = $_POST["SaleInfoTitle-".$index];
                $isPartnerAmount = isset($_POST["isPartnerAmount-".$index]);
                $SaleInfoAmount = $_POST["SaleInfoAmount-".$index];

                $db->insert($db->$saleinfotable, array(
                    "LinkBankID"            => $id,
                    "userid"            => $current_user->id,
                    "SaleInfoType"      => $SaleInfoType,
                    "SaleInfoTitle"     => $SaleInfoTitle,
                    "isPartnerAmount"   => $isPartnerAmount,
                    "SaleInfoAmount"    => $SaleInfoAmount,
                    "DateAdded"         => strtotime("now")
                ));
            }
            
        }

        $sql = "select count(*) from ".$db->linkbanks." where userid=".$current_user->id." and SaleInfoEntered = '0' and LinkStatus = 'complete' and ManualSalesEntry = 1";
        $remainin_saleinfos = $db->get_var($sql) * 1;
        if(!isset($_GET["single"]) && $remainin_saleinfos > 0)
            $jsonData["jscode"] = "AddSaleinfo();";
    }
    echo json_encode($jsonData);
    die;
} else if($post_act == "save_partnerpayment"){
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!empty($_GET["LinkBankID"])){
        $LinkBankID = parseInt($_GET['LinkBankID']);
        $data = array(
            "PartnerPaymentReferenceNo"         => $_POST['PartnerPaymentReferenceNo'],
            "PartnerPaymentStatus"              => "complete",
            "PartnerPaymentDate"                => strtotime("now")
        );
        
        $db->update($db->linkbanks, $data, array("id" => $LinkBankID));
        $jsonData["value"] = '<span class="" title="Completed"><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Paid</span>';
    }
    echo json_encode($jsonData);
    die;
} else if($post_act == "linkbank_get_rows"){
    $linkbankstype = $_GET['linkbank_type'] == ""?"all":$_GET['linkbank_type'];
    $linkbank_sale_type = $_GET['linkbank_sale_type'];
    $pageurl = "linkbank/stats";
    $curpage = "linkbank/stats".($linkbank_sale_type == ""?"":"/".$linkbank_sale_type);
    $jsonData = get_linkbank_rows($_POST['draw'], $linkbankstype, $linkbank_sale_type, $pageurl, $curpage, $_POST['start'], $_POST['length'], $_POST['order'][0], $_POST['search'], is_numeric($_POST['SeachColumn'])?$_POST['SeachColumn']:-1);
    echo json_encode($jsonData);
    die;
}