<?php
if($post_act == "save_redirect_link" || $post_act == "delete_redirect_link"){
    $RedirectLinkID = 0;
    if($post_act == "save_redirect_link"){
        $data = array(
            "userid"  => $current_user->id,
            "RedirectLinkName"  => $_POST['RedirectLinkName'],
            "RedirectLinkURL"   => $_POST['RedirectLinkURL'],
            "RedirectLinkType"  => $_GET['RedirectLinkType'],
            "DateAdded"         => strtotime("now")
        );
        
        if(isset($_GET['RedirectLinkID'])){
            unset($data['DateAdded']);
            $db->update($db->redirect_links, $data, array("id" => parseInt($_GET['RedirectLinkID'])));
            $RedirectLinkID = parseInt($_GET['RedirectLinkID']);
        }else{
            $db->insert($db->redirect_links, $data);
            $RedirectLinkID = $db->insert_id;
        }
    }
    
    if($post_act == "delete_redirect_link"){
        $db->delete($db->redirect_links, array("id" => parseInt($_POST['RedirectLinkID'])));
    }

    $sql = "select * from ".$db->redirect_links." where RedirectLinkType='".$_GET['RedirectLinkType']."' and userid = ".$current_user->id." order by RedirectLinkName";
    $redirect_links = $db->get_results($sql);

    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!isset($_GET['nodefault'])) $jsonData["value"] = '<option value="">Select Holding Page</option>';
    foreach($redirect_links as $redirect_link)
        $jsonData["value"] .= '<option value="'.$redirect_link->id.'" '.($redirect_link->id==$RedirectLinkID?' selected="selected" ':'').' >'.$redirect_link->RedirectLinkName.'</option>';
    echo json_encode($jsonData);
    
    die;
}