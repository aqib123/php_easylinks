<?php
if($get_act == "campaign_details"){
    $CampaignID = intval($_GET["CampaignID"]);
    ob_start();
?>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-campaign-details data-table responsive- nowrap" data-nobuttons="true" data-nosort-columns="0" data-default-sort-column="1">
    <thead>
        <tr>
            <th><span></span></th>
            <th><span>Seq#</span></th>
            <th><span>Days Sent</span></th>
            <th><span>Subject</span></th>
            <th><span>Autoresponder</span></th>
            <th><span>List Name</span></th>
            <th><span>Email Type</span></th>
            <th><span>Vendor</span></th>
            <th><span>Platform</span></th>
            <th><span>Email Links</span></th>
            <th><span>Sub Email Clicks</span></th>
            <th><span>CTA Clicks</span></th>
            <th><span>Pixel Fires</span></th>
            <th><span>Click %</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
    $sql = "select * from ".$db->campaign_emails." where userid = ".$current_user->id." and CampaignID=".$CampaignID." order by id";
    $emails = $db->get_results($sql);
    $seqno = 0;
    foreach($emails as $email){
        $autoresponderlist = $db->get_row("select * from ".$db->autoresponders." where userid = ".$current_user->id." and id=".$email->AutoresponderID);
        $autoresponder = $db->get_row("select * from ".$db->autoresponders." where userid = ".$current_user->id." and id=".$autoresponderlist->parentid);
        $emailtype = $db->get_row("select * from ".$db->groups." where GroupType='EmailType' and userid = ".$current_user->id." and id=".$email->EmailTypeID);
        $VendorName = $db->get_var("select VendorName from ".$db->vendors." where id in (select VendorID from ".$db->linkbanks." where id=".$email->LinkBankID.")");
        $PlatformName = $db->get_var("select GroupName from ".$db->groups." where id in (select AffiliatePlatformID from ".$db->linkbanks." where id=".$email->LinkBankID.")");

        $linkbankurl = get_linkbankurl($email->LinkBankID);
        $linkbankurl .= "/mail/".$email->id."/";

        $CampaignClicks = $db->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".$db->linkbank_clicks." where ClickType='mail' and EmailID in (SELECT id from ".$db->campaign_emails." where CampaignID=".$email->CampaignID." and userid=".$current_user->id.")) as Clicks") * 1;
        $SubEmailClicks = $db->get_var("select count(*) as ClickCount from ".$db->linkbank_clicks." where ClickType='mail' and EmailID=".$email->id) * 1;
        $CTAClicks = get_email_cta_clicks($email);//$db->get_var("select count(DISTINCT ClickIp) as ClickCount from ".$db->linkbank_clicks." where ClickType='mail' and EmailID=".$email->id) * 1;
        $PixelFires = $db->get_var("select count(*) as ClickCount from ".$db->linkbank_pixel_fires." where FireType='mail' and EmailID=".$email->id) * 1;
        $ClickPercentage = round(($CampaignClicks > 0?(($SubEmailClicks * 100) / $CampaignClicks):0), 1);

        $CTALinks = get_email_links($email->EmailBody, $email, true);

        $linkbank = $db->get_row("select * from ".$db->linkbanks." where id=".$current_user->id);
        $StatusIcon = get_domain_status_icon($linkbank->DomainID);
        ?>

        <tr>
            <td class="text-center"><?php echo $StatusIcon;?></td>
            <td class="text-center"><span><?php echo (++$seqno);?></span></td>
            <td class="text-center"><span><?php echo $email->SendTime?></span></td>
            <td><span><?php echo $email->EmailSubject?></span></td>
            <td><span><?php echo $autoresponder->AutoresponderName?></span></td>
            <td><span><?php echo $autoresponderlist->AutoresponderName?></span></td>
            <td><span><?php echo $emailtype->GroupName?></span></td>
            <td><span><?php echo !empty($VendorName)?$VendorName:"N/A"; ?></span></td>
            <td><span><?php echo !empty($PlatformName)?$PlatformName:"N/A"; ?></span></td>
            <td class="text-center">
                <a href="javascript: void(0);" data-clipboard-text="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $linkbankurl;?>"><i class="fa fa-clipboard"></i></a>
                <a href="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $linkbankurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
            </td>
            <td class="text-center"><span><?php echo $SubEmailClicks>0?$SubEmailClicks:"N/A";?></span></td>
            <td class="text-center"><span><?php echo $SubEmailClicks>0?$CTAClicks:"N/A";?></span></td>
            <td class="text-center"><span><?php echo $SubEmailClicks>0?$PixelFires:"N/A"?></span></td>
            <td class="text-center"><span><?php echo $SubEmailClicks>0?$ClickPercentage."%":"N/A"?></span></td>
        </tr>
        <?php        
    }
        ?>
    </tbody>
</table>
</div>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
}else if($get_act == "changecampaignstatus"){
    $status = $_GET["status"];
    $CampaignID = intval($_GET["CampaignID"]);
    
    $data = array("CampaignStatus" => $status);
    if($status == "pending"){
        $data["StartDate"] = "";
        $data["EndDate"] = "";
    } else if($status == "active") {
        $data["StartDate"] = strtotime("now");
        $data["EndDate"] = "";
    } else if($status == "complete") {
        $data["EndDate"] = strtotime("now");
    }

    $db->update($db->campaigns, $data, array("id" => $CampaignID));
    $url = "campaign/stats/".(!empty($_GET['campaign_type'])?$_GET['campaign_type']."/":"")."?success=true&msg=Campaign Updated Successfully&".$status."=true&CampaignID=".$CampaignID;
    //echo $url;
    site_redirect($url);
    die;
}else if($get_act == "delete_campaign"){
    $db->delete($db->campaign_emails, array("CampaignID" => parseInt($_GET['CampaignID'])));
    $db->delete($db->campaigns, array("id" => parseInt($_GET['CampaignID'])));
    $url = get_site_url("campaign/stats/?success=true&msg=Campaign Deleted Successfully");
    redirect($url);
    die;
}else if($get_act == "reset_campaign"){
    delete_campaign_stats(parseInt($_GET['CampaignID']));
    $url = get_site_url("campaign/stats/?success=true&msg=Campaign Stats Reset Successfully");
    redirect($url);
    die;
}else if($get_act == "clone_campaign"){
    $url = "campaign/stats/";
    $CampaignID = parseInt($_GET['CampaignID']);
    $campaign = $db->get_row("select * from ".$db->campaigns." where id=".$CampaignID, ARRAY_A);
    if ($campaign){
        unset($campaign["id"]);
        $campaign["userid"] = $current_user->id;
        $campaign["DateAdded"] = strtotime("now");
        $campaign["CampaignStatus"] = "pending";
        if($db->insert($db->campaigns, $campaign) && $db->insert_id > 0){
            $id = $db->insert_id;
            $data = array(
                "CampaignName"   => $campaign["CampaignName"]."-clone-".$id,
            );
            if($db->update($db->campaigns, $data, array("id" => $id))){
                $url = "campaign/?CampaignID=".$id."&success=true&msg=Campaign Cloned Successfully";

                $sql = "select * from ".$db->campaign_emails." where userid=".$current_user->id." and CampaignID=".$CampaignID;
                $campaign_emails = $db->get_results($sql, ARRAY_A);
                foreach($campaign_emails as $campaign_email){
                    unset($campaign_email["id"]);
                    $campaign_email["userid"] = $current_user->id;
                    $campaign_email["DateAdded"] = strtotime("now");
                    $campaign_email["CampaignID"] = $id;
                    $db->insert($db->campaign_emails, $campaign_email);
                }
            }
        }
    }
    site_redirect($url);
    die;
}