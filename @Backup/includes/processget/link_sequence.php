<?php
if($get_act == "add_link_sequence_link"){
    $LinkSequenceID = intval($_GET["LinkSequenceID"]);
    $index = intval($_GET["index"]);
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");

    $data = array(
        "userid"                              => $current_user->id,
        "LinkSequenceID"                 => $LinkSequenceID,
        "LinkSequenceLinkPosition"       => $index,
        "LinkSequenceLinkType"           => "customurl",
        "DateAdded"                           => strtotime("now"),
    );

    $db->insert($db->link_sequence_links, $data);
    if(is_numeric($db->insert_id)){
        $jsonData["value"] = get_link_sequence_link_box($db->insert_id, $index);
    }
    echo json_encode($jsonData);
    die;
}else if($get_act == "update_link_sequence_link_postion"){
    $LinkSequenceID = intval($_GET["LinkSequenceID"]);
    $LinkSequenceLinkIDs = $_GET["LinkSequenceLinkIDs"];
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    
    for ($index = 0; $index < count($LinkSequenceLinkIDs); $index++)
    {
        $LinkSequenceLinkID = $LinkSequenceLinkIDs[$index];
        $LinkSequenceLinkPosition = $index + 1;
        $db->update($db->link_sequence_links, array("LinkSequenceLinkPosition" => $LinkSequenceLinkPosition), array("id" => $LinkSequenceLinkID, "userid" => $current_user->id));
    }
    echo json_encode($jsonData);
    die;
}else if($get_act == "change_link_sequence_link_status"){
    $LinkSequenceLinkID = intval($_GET["link_sequence_linkid"]);
    $LinkSequenceLinkLive = $db->get_var("select LinkSequenceLinkLive from ".$db->link_sequence_links." where userid=".$current_user->id." and id=".$LinkSequenceLinkID) * 1;
    $LinkSequenceLinkLive = $LinkSequenceLinkLive == 1?0:1;
    $jsonData = array("before"=>"", "value"=>"", "after"=>"", "jscode" => "");
    if($db->update($db->link_sequence_links, array("LinkSequenceLinkLive" => $LinkSequenceLinkLive), array("id" => $LinkSequenceLinkID, "userid" => $current_user->id)))
        $jsonData["value"] = "success";
    echo json_encode($jsonData);
    die;
}else if($get_act == "changelink_sequencestatus"){
    $status = $_GET["status"];
    $LinkSequenceID = intval($_GET["LinkSequenceID"]);
    
    $data = array("LinkSequenceStatus" => $status);
    if($status == "pending"){
        $data["StartDate"] = "";
        $data["EndDate"] = "";
    } else if($status == "active") {
        $data["StartDate"] = strtotime("now");
        $data["EndDate"] = "";
    } else if($status == "complete") {
        $data["EndDate"] = strtotime("now");
    }

    $db->update($db->link_sequences, $data, array("id" => $LinkSequenceID));
    $url = "link-sequence/stats/".(!empty($_GET['link_sequence_type'])?$_GET['link_sequence_type']."/":"")."?success=true&msg=Link Sequence Updated Successfully&".$status."=true&LinkSequenceID=".$LinkSequenceID;
    site_redirect($url);
    die;
}else if($get_act == "link_sequence_details"){
    $LinkSequenceID = intval($_GET["LinkSequenceID"]);
    ob_start();
?>
<div class="table-responsive data-table-container">
    <table class="table table-condensed table-bordered table-striped table-link_sequence-details data-table responsive- nowrap" data-nobuttons-="true">
    <thead>
        <tr>
            <th class="text-center"><span>Seq#</span></th>
            <th><span>URl</span></th>
            <th><span>Name</span></th>
            <th class="text-center"><span>End Date</span></th>
            <th class="text-center"><span>Time Left</span></th>
            <th class="text-center"><span>Status</span></th>
            <th class="text-center"><span>Visits</span></th>
        </tr>
    </thead>

    <tbody>
        <?php
    $sql = "select * from ".$db->link_sequences." where id = ".$LinkSequenceID." and userid = ".$current_user->id;
    $LinkSequence = $db->get_row($sql);

    $sql = "select * from ".$db->link_sequence_links." where userid = ".$current_user->id." and LinkSequenceID=".$LinkSequenceID." order by id";
    $link_sequence_links = $db->get_results($sql);
    $seqno = 0;
    foreach($link_sequence_links as $link_sequence_link){
        if ($link_sequence_link->LinkSequenceLinkType == "customurl"){
            $LinkSequenceLinkURL = $link_sequence_link->LinkSequenceLinkURL;
            $LinkSequenceLinkName = $link_sequence_link->LinkSequenceLinkName;
        } else {
            $sql = "select * from ".$db->linkbanks." where userid=".$current_user->id." and id=".$link_sequence_link->LinkBankID;
            $linkbank = $db->get_row($sql);
            $LinkSequenceLinkURL = get_linkbankurl($linkbank);
            $LinkSequenceLinkName = $linkbank->LinkName;
        }
        
        if($LinkSequence->LinkSequenceStatus == "pending"){
            $TimeLeft = "Pending";
            $strEndDate = "Pending";
        } else{
            //if($LinkSequence->LinkSequenceStatus == "complete")
            //    $TimeNow = $LinkSequence->EndDate;
            //else
                $TimeNow = strtotime("now");

            $dtStartDate = get_link_sequence_link_start_date($link_sequence_link, $current_user);
            $dtEndDate = get_link_sequence_link_end_date($link_sequence_link, $dtStartDate, $current_user);
            
            $StartDate = strtotime($dtStartDate->format("r"));
            $EndDate = strtotime($dtEndDate->format("r"));
            
            $dtNowDate = new DateTime(date("r", $TimeNow));
            $days = $dtNowDate->diff($dtEndDate)->format("%a");
            $hours = $dtNowDate->diff($dtEndDate)->format("%h");
            $minutes = $dtNowDate->diff($dtEndDate)->format("%i");

            
            //if($StartDate > $TimeNow){
            //    $TimeLeft = $LinkStatus = "Queued";
            //}else if($TimeNow > $EndDate){// || ($days == 0 && $hours == 0 && $minutes == 0)
            //    $TimeLeft = $LinkStatus = "Completed - 100%";
            //    $LinkComplete = true;
            //}else{
            //    $LinkStatus = "Active";
            //    $TimeLeft = $dtNowDate->diff($dtEndDate)->format("Time Left: %a days, %h hours, %i min");
            //    $LinkActive = true;
            //}

            if($StartDate <= $TimeNow && $EndDate >= $TimeNow){
                $TimeLeft = $dtNowDate->diff($dtEndDate)->format("Time Left: %a days, %h hours, %i min");//, %s seconds
                $LinkActive = true;
                $LinkStatus = "Active";
            } else if($StartDate > $TimeNow){
                $TimeLeft = $LinkStatus = "Queued";
            }else {
                $TimeLeft = $LinkStatus = "Completed - 100%";
                $LinkComplete = true;
            }

            $strEndDate = $dtEndDate->format("m/d/Y h:i A");


            $TotalTime = $EndDate - $StartDate;
            $TotalSpentTime = $TimeNow < $StartDate?0:$TimeNow - $StartDate;
            $Percentage = $TimeLeft == "Completed - 100%"? 100:round($TotalSpentTime * 100 / $TotalTime, 1);
        }

        $SubLinkVisits = $db->get_var("select count(*) as VisitCount from ".$db->link_sequence_clicks." where LinkSequenceLinkID=".$link_sequence_link->id) * 1;
        ?>

        <tr>
            <td class="text-center"><span><?php echo (++$seqno);?></span></td>
            <td><span><?php echo $LinkSequenceLinkURL;?></span></td>
            <td><span><?php echo $LinkSequenceLinkName;?></span></td>
            <td class="text-center"><span><?php echo $strEndDate;?></span></td>
            <td class="text-center"><span><?php echo $TimeLeft;?></span></td>
            <td class="text-center"><span><?php echo $LinkStatus; ?></span></td>
            <td class="text-center"><span><?php echo $SubLinkVisits;?></span></td>
        </tr>
        <?php        
    }
        ?>
    </tbody>
</table>
</div>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
} else if($get_act == "delete_link_sequence"){
    delete_link_sequence_status(parseInt($_GET['LinkSequenceID']));
    $db->delete($db->link_sequence_links, array("LinkSequenceID" => parseInt($_GET['LinkSequenceID'])));
    $db->delete($db->link_sequences, array("id" => parseInt($_GET['LinkSequenceID'])));
    $url = get_site_url("link-sequence/stats/?success=true&msg=Link Sequence Deleted Successfully");
    redirect($url);
    die;
}else if($get_act == "reset_link_sequence"){
    delete_link_sequence_status(parseInt($_GET['LinkSequenceID']));
    $url = get_site_url("link-sequence/stats/?success=true&msg=Link Sequence Stats Reset Successfully");
    redirect($url);
    die;
}else if($get_act == "clone_link_sequence"){
    $url = "link-sequence/stats/";
    $LinkSequenceID = parseInt($_GET['LinkSequenceID']);
    $link_sequence = $db->get_row("select * from ".$db->link_sequences." where id=".$LinkSequenceID, ARRAY_A);
    if ($link_sequence){
        //echo "<pre>".print_r($link_sequence, true)."</pre>";
        unset($link_sequence["id"]);
        $link_sequence["userid"] = $current_user->id;
        $link_sequence["DateAdded"] = strtotime("now");
        $link_sequence["LinkSequenceStatus"] = "pending";
        if($db->insert($db->link_sequences, $link_sequence) && $db->insert_id > 0){
            $id = $db->insert_id;
            $data = array(
                "LinkSequenceName"   => $link_sequence["LinkSequenceName"]."-clone-".$id,
                "VisibleLink"   => $link_sequence["VisibleLink"]."-clone-".$id,
            );
            if($db->update($db->link_sequences, $data, array("id" => $id))){
                $url = "link-sequence/?LinkSequenceID=".$id."&success=true&msg=Link Sequence Cloned Successfully";
                
                $sql = "select * from ".$db->link_sequence_links." where userid=".$current_user->id." and LinkSequenceID=".$LinkSequenceID;
                $link_sequence_links = $db->get_results($sql, ARRAY_A);
                foreach($link_sequence_links as $link_sequence_link){
                    unset($link_sequence_link["id"]);
                    $link_sequence_link["userid"] = $current_user->id;
                    $link_sequence_link["DateAdded"] = strtotime("now");
                    $link_sequence_link["LinkSequenceID"] = $id;
                    $db->insert($db->link_sequence_links, $link_sequence_link);
                }
            }
        }
    }
    site_redirect($url);
    die;
} else if($get_act == "changelink_sequencestatus"){
    $status = $_GET["status"];
    $LinkSequenceID = intval($_GET["LinkSequenceID"]);
    
    $data = array("LinkSequenceStatus" => $status);
    $db->update($db->link_sequences, $data, array("id" => $LinkSequenceID));
    $url = "link-sequence/stats/".(!empty($_GET['link_sequence_type'])?$_GET['link_sequence_type']."/":"")."?success=true&msg=Link Sequence Updated Successfully&".$status."=true&LinkSequenceID=".$LinkSequenceID;
    site_redirect($url);
    die;
}