<?php
if($get_act == "changepaidtrafficstatus"){
    $status = $_GET["status"];
    $PaidTrafficID = intval($_GET["PaidTrafficID"]);

    $data = array("PaidTrafficStatus" => $status);
    if($status == "pending"){
        $data["StartDate"] = "";
        $data["EndDate"] = "";
    } else if($status == "active") {
        $data["StartDate"] = strtotime("now");
        $data["EndDate"] = "";
    } else if($status == "complete") {
        $data["EndDate"] = strtotime("now");
    }

    $db->update($db->paidtraffics, $data, array("id" => $PaidTrafficID));
    $url = "paidtraffic/stats/".(!empty($_GET['paidtraffic_type'])?$_GET['paidtraffic_type']."/":"")."?success=true&msg=Paid Traffic Updated Successfully&".$status."=true&PaidTrafficID=".$PaidTrafficID;
    //echo $url;
    site_redirect($url);
    die;
}else if($get_act == "delete_paidtraffic"){
    delete_paid_traffic_stats(parseInt($_GET['PaidTrafficID']));
    $db->delete($db->paidtraffics, array("id" => parseInt($_GET['PaidTrafficID'])));
    $db->delete($db->paidtraffic_saleinfo, array("PaidTrafficID" => $PaidTrafficID));
    $url = get_site_url("paidtraffic/stats/?success=true&msg=Paid Traffic Deleted Successfully");
    redirect($url);
    die;
}else if($get_act == "reset_paidtraffic"){
    delete_paid_traffic_stats(parseInt($_GET['PaidTrafficID']));
    $url = get_site_url("paidtraffic/stats/?success=true&msg=Paid Traffic Stats Reset Successfully");
    redirect($url);
    die;
}else if($get_act == "clone_paidtraffic"){
    $url = "paidtraffic/stats/";
    $PaidTrafficID = parseInt($_GET['PaidTrafficID']);
    $paidtraffic = $db->get_row("select * from ".$db->paidtraffics." where id=".$PaidTrafficID, ARRAY_A);
    if ($paidtraffic){
        unset($paidtraffic["id"]);
        $paidtraffic["userid"] = $current_user->id;
        $paidtraffic["DateAdded"] = strtotime("now");
        $paidtraffic["PaidTrafficStatus"] = "active";
        if($db->insert($db->paidtraffics, $paidtraffic) && $db->insert_id > 0){
            $id = $db->insert_id;
            $data = array(
                "PaidTrafficName"        => $paidtraffic["PaidTrafficName"]."-clone-".$id,
                "VisibleLink"       => $paidtraffic["VisibleLink"]."-clone-".$id,
            );
            if($db->update($db->paidtraffics, $data, array("id" => $id)))
                $url = "paidtraffic/?PaidTrafficID=".$id."&success=true&msg=Link Cloned Successfully";
        }
    }

    site_redirect($url);
    die;
} else if($get_act == "check_link_name" && isset($_GET["type"]) && $_GET["type"] == "paidtraffic"){
    $paidtraffic = $db->get_row("select * from ".$db->paidtraffics." where userid=".$current_user->id." and PaidTrafficName='".$_GET["PaidTrafficName"]."' and id <> ".intval($_GET["id"]));
    
    if(!$paidtraffic)
        http_response_code(200);
    else
        http_response_code(418);
    die;
}else if($get_act == "paidtraffic_details"){
    $PaidTrafficID = intval($_GET["PaidTrafficID"]);
    ob_start();
?>
<div class="table-responsive- data-table-container-">
    <table class="table table-condensed table-bordered table-striped table-paidtraffic-details data-table responsive- nowrap" data-nobuttons-="true">
    <thead>
        <tr>
            <th class="text-center"><span>Seq#</span></th>
            <th class="text-center"><span>Type</span></th>
            <th class="text-center"><span>Upsell/Downsell Name</span></th>
            <th class="text-center"><span>URL</span></th>
            <th class="text-center"><span>Conv #</span></th>
            <th class="text-center"><span>Conv $</span></th>
            <th class="text-center"><span>% Sales</span></th>
        </tr>
    </thead>

    <tbody>
    <?php
    $conv_total = $db->get_var("select count(*) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$PaidTrafficID." and ConversionType='sales'") * 1;

    $sql = "select * from ".$db->paidtraffic_conversion_pixels." where userid = ".$current_user->id." and PaidTrafficID=".$PaidTrafficID." order by id";
    $paidtraffic_conversion_pixels = $db->get_results($sql);
    $seqno = 0;
    foreach($paidtraffic_conversion_pixels as $paidtraffic_conversion_pixel){
        $ConversionPixelURL = $paidtraffic_conversion_pixel->ConversionPixelURL;

        $conv_no = $db->get_var("select count(*) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$PaidTrafficID." and PaidTrafficConversionPixelID=".$paidtraffic_conversion_pixel->id." and ConversionType='sales'") * 1;
        $conv_amount = $db->get_var("select sum(UnitPrice) from ".$db->paidtraffic_conversions." where PaidTrafficID=".$PaidTrafficID." and PaidTrafficConversionPixelID=".$paidtraffic_conversion_pixel->id." and ConversionType='sales'") * 1;
        $conv_percent = $conv_total == 0?0:($conv_no / $conv_total * 100);
    ?>
        <tr>
            <td class="text-center"><span><?php echo (++$seqno);?></span></td>
            <td class="text-center"><span><?php echo $paidtraffic_conversion_pixel->ConversionPixelType;?></span></td>
            <td><a href="<?php echo $ConversionPixelURL?>" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$ConversionPixelURL;?>"><?php echo $paidtraffic_conversion_pixel->ConversionPixelName;?></a></td>
            <td>
                <a href="javascript: void(0);" data-clipboard-text="<?php echo $ConversionPixelURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $ConversionPixelURL;?>"><i class="fa fa-clipboard"></i></a>
                <a href="<?php echo $ConversionPixelURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $ConversionPixelURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
            </td>
            <td class="text-center"><span><?php echo get_formated_number($conv_no);?></span></td>
            <td class="text-center"><span>$<?php echo get_formated_number(round($conv_amount, 2));?></span></td>
            <td class="text-center"><span><?php echo get_formated_number(round($conv_percent, 2));?>%</span></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div>
<?php
    $contents = ob_get_contents();
    ob_end_clean();
    $jsonData = array("before"=>"", "value"=>$contents, "after"=>"", "jscode" => "");
    echo json_encode($jsonData);
    die;
}