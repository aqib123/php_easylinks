<?php
if($get_act == "delete_manualsale"){
    if(isset($_GET["ManualSaleID"]) && is_numeric($_GET["ManualSaleID"])){
        $pageurl = "manual-sale/stats/";
        $ManualSale = $db->get_row("select * from ".$db->manualsales." where userid=".$current_user->id." and id = ".$_GET["ManualSaleID"]);//MasterCampaignID = ".$MasterCampaignID." and LinkBankID=".$LinkBankID." and 
        if($ManualSale){
            $db->query("delete from ".$db->manualsale_incomes." where ManualSaleSplitPartnerID in (select id from ".$db->manualsale_split_partners." where userid = ".$current_user->id." and ManualSaleID = ".$ManualSale->id.")");
            $db->query("delete from ".$db->manualsale_other_incomes." where ManualSaleSplitPartnerID in (select id from ".$db->manualsale_split_partners." where userid = ".$current_user->id." and ManualSaleID = ".$ManualSale->id.")");
            $db->query("delete from ".$db->manualsale_expenses." where ManualSaleSplitPartnerID in (select id from ".$db->manualsale_split_partners." where userid = ".$current_user->id." and ManualSaleID = ".$ManualSale->id.")");
            $db->delete($db->manualsale_split_partners, array("userid" => $current_user->id, "ManualSaleID" => $ManualSale->id));
            $db->delete($db->manualsales, array("userid" => $current_user->id, "id" => $ManualSale->id));
            $pageurl .= "?success=true&msg=Sale Deleted Successfully";
        }
        
        site_redirect($pageurl);
        die;
    }
}