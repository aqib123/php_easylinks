<?php
if ($get_act == "get_ipaddress_info"){
    $IpAddress = $_GET["ipaddress"];
    $IpAddressInfo = get_ipaddress_info($IpAddress);
    $jsonData = array(
        "latitude"      => $IpAddressInfo->latitude, 
        "longitude"     => $IpAddressInfo->longitude,
        "IpAddress"     => $IpAddress, 
        "title"         => 'Ip: '.$IpAddress
    );
    echo json_encode($jsonData);
    die;
}else if ($get_act == "get_ipaddresses_info"){
    $IpAddresses = json_decode($_GET["ipaddresses"]);
    $jsonData = array();
    foreach ($IpAddresses as $IpAddress){
        $IpAddressInfo = get_ipaddress_info($IpAddress);
        $jsonData[] = array(
            "latitude"      => $IpAddressInfo->latitude, 
            "longitude"     => $IpAddressInfo->longitude,
            "IpAddress"     => $IpAddress, 
            "title"         => 'Ip: '.$IpAddress
        );
    }
    echo json_encode($jsonData);
    die;
}