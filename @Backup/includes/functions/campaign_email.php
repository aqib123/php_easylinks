<?php
function get_campaign_cta_clicks($campaign){
    global $db, $current_user;
    
    if(!is_numeric($campaign))
        $campaign = $campaign->id;
    
    $ClicksTotal = 0;
    $sql = "select * from ".$db->campaign_emails." where userid=".$current_user->id." and CampaignID=".$campaign;
    $emails = $db->get_results($sql);
    foreach ($emails as $email){
    	$CTAClicks = get_email_cta_clicks($email);
        $ClicksTotal += $CTAClicks;
    }
    
    return $ClicksTotal;
}

function get_email_links($str, $email, $onlylink = false){
    global $db, $current_user;
	$data = array("html" => "", "cta_links" => array());

    if(!$onlylink){
        $linkbankurl = get_linkbankurl($email->LinkBankID, 'mail/'.$email->id);
        //$linkbankurl .= '/mail/'.$email->id.'/';
        $str = preg_replace('/{easy_links_mail}/', $linkbankurl, $str);
    }

    $html = str_get_html($str);
    if($html){
        foreach($html->find('a.easy_links_mail_cta') as $element){
            $LinkBankID = intval($element->href);

            if(!$onlylink){
                $linkbankurl = get_linkbankurl($LinkBankID, 'cta/'.$email->id);
                //$linkbankurl .= '/cta/'.$email->id.'/';
                $element->href = $linkbankurl;
            }

            if(!in_array($LinkBankID, $data["cta_links"]))
                $data["cta_links"][] = $LinkBankID;
        }
    }

    if(!$onlylink)
        $data["html"] = $html."";
    else
        $data = $data["cta_links"];

    return $data;
}

function get_email_box($CampaignID, $email, $index, $emailstype = "all"){
    global $db, $current_user;
    ob_start();
    if($email != null){
        $linkbankdata = get_email_links($email->EmailBody, $email);
        $body = $linkbankdata["html"];
        //$linkbankurl = get_linkbankurl($email->LinkBankID);

        $sql = "select count(*) from ".$db->linkbank_clicks." where LinkBankID = ".$email->LinkBankID." and ClickType='mail' and EmailID = ".$email->id;
        $EmailClicks = $db->get_var($sql);

        $sql = "select * from ".$db->linkbanks." where id = ".$email->LinkBankID;
        $linkbank = $db->get_row($sql);

        $sql = "select GroupName from ".$db->groups." where id = ".$email->EmailTypeID;
        $EmailType = $db->get_var($sql);

        $CTAClicks = get_email_cta_clicks($email);

        $EmailStatus = 3;
        if($EmailClicks <= 0)
        {
            if($email->isCopied == 0)
                $EmailStatus = 1;
            else if($email->isCopied == 1)
                $EmailStatus = 2;
        }

        if($EmailStatus == 1 && $emailstype != "all" && $emailstype != "pending")
            return "";
        else if($EmailStatus == 2 && $emailstype != "all" && $emailstype != "copied")
            return "";
        else if($EmailStatus == 3 && $emailstype != "all" && $emailstype != "active")
            return "";


        $sql = "select count(*) as TotalLists from ".$db->autoresponders." where userid = 1 and parentid in (select parentid from ".$db->autoresponders." where id = ".$email->AutoresponderID." and userid=".$current_user->id.")";
        $TotalLists = $db->get_var($sql) * 1;

        $sql = "select AutoresponderName as TotalLists from ".$db->autoresponders." where userid = 1 and id in (select parentid from ".$db->autoresponders." where id = ".$email->AutoresponderID." and userid=".$current_user->id.")";
        $AutoresponderName = $db->get_var($sql);

        $EmailBody = html_entity_decode($body, ENT_QUOTES);
        $EmailBody = str_replace("\r", "\\r", $EmailBody);
        $EmailBody = str_replace("\n", "\\n", $EmailBody);
        $EmailBody = htmlentities($EmailBody);

        if($email->SetWidth == 1)
            $EmailBody = preg_replace( '~((?:\S*?\s){13})~', "$1\\n", $EmailBody);//wordwrap($EmailBody, 13, "\\n");//

?>
<div class="small-box <?php echo $EmailStatus == 3?"bg-green email-active":($EmailStatus == 1?"bg-primary email-pending":"bg-teal email-copied");?> email-box">
    <div class="small-box-header clearfix">
        <ul class="list-inline">
            <li>
                <div class="mailsubject">
                    <?php if(true || $EmailStatus == 1 || $EmailStatus == 3){ ?>
                    <a href="javascript:" data-clipboard-text="subj" class="scissors copy-email-data" data-copy-type="subject" data-object-id="emails_<?php echo $email->id;?>" title="Copy Email Subject"><i class="fa fa-scissors"></i></a>
                    <?php } ?>
                    <b>Subj:</b>&nbsp;<?php echo $email->EmailSubject;?>
                </div>
            </li>
            <li class="pull-right">
                <a href="<?php site_url("campaign/email/".$CampaignID."/?index=".$index)?>" data-widget="RemoveOption" data-values="act=delete_email&EmailID=<?php echo $email->id;?>" data-related="#email-box-<?php echo $index;?>" data-method="post"><i class="fa fa-times"></i></a>
            </li>
            <li class="pull-right">
                <a href="<?php site_url("campaign/email/".$CampaignID."/?EmailID=".$email->id."&index=".$index)?>" data-widget="ShowLinkModel" data-related="#email-box-<?php echo $index;?>" data-method="post" data-page-title="Emails Manager" data-model-width="750px"><i class="fa fa-pencil"></i></a>
            </li>
            <li class="pull-right"><span>Day <?php echo $email->SendTime;?></span></li>
        </ul>
    </div>
    <div class="inner">
        <h3>
            <?php if($EmailStatus == 1){ ?>
            <a href="javascript:" data-clipboard-text="body" class="headericon scissors copy-email-data" data-copy-type="body" data-object-id="emails_<?php echo $email->id;?>" title="Copy Email Body"><i class="fa fa-scissors"></i></a>
            <span>Pending</span>
            <?php } else if($EmailStatus == 2){ ?>
            <a href="javascript:" data-clipboard-text="body" class="headericon scissors copy-email-data" data-copy-type="body" data-object-id="emails_<?php echo $email->id;?>" title="Copy Email Body"><i class="fa fa-scissors"></i></a>
            <span class="headericon"><i class="fa fa-check"></i></span>
            <span>&nbsp;Copied</span>
            <?php } else if($EmailStatus == 3){ ?>
            <a href="javascript:" data-clipboard-text="body" class="headericon scissors copy-email-data" data-copy-type="body" data-object-id="emails_<?php echo $email->id;?>" title="Copy Email Body"><i class="fa fa-scissors"></i></a>
            <span class="main_clicks"><?php echo $EmailClicks;?>&nbsp;Main Clicks</span>
            <small class="cta_clicks"><?php echo $CTAClicks;?>&nbsp;CTA Clicks</small>
            <?php } ?>
        </h3>
        <?php if($EmailStatus == 3){ ?>
        <div class="email-link-date">
            <?php echo isset($linkbank->EndDate) && !empty($linkbank->EndDate)?date("l: m/d/Y", $linkbank->EndDate):"";?>
        </div>
        <div class="email-link-time">
            <?php echo isset($linkbank->EndDate) && !empty($linkbank->EndDate)?date("H:i", $linkbank->EndDate)." hrs":"";?>
        </div>
        <?php } ?>
    </div>
    <div class="icon">
        <?php if($EmailStatus == 1){ ?>
            <i class="ion ion-android-time"></i>
        <?php } else if($EmailStatus == 2){ ?>
            <i class="ion ion-ios-copy-outline"></i>
        <?php } else if($EmailStatus == 3){ ?>

        <?php } ?>
    </div>
    <div class="autoresponder">Autoresponder Tagged:</div>
    <div class="small-box-footer">
        <ul class="list-inline clearfix">
            <li>
                <div class="emailtype">Type: <?php echo $EmailType;?></div>
            </li>
            <?php if($EmailStatus == 1 || $EmailStatus == 2){ ?>
            <li class="pull-right smallcheck">
                <input type="checkbox" class="blue email-copied" name="isCopied" id="isCopied" data-related="#email-box-<?php echo $index;?>" data-url="<?php site_url("campaign/email/".$CampaignID."/?index=".$index)?>" data-emailid="<?php echo $email->id;?>" <?php if($email->isCopied == 1) echo ' checked="checked" ' ?> />
            </li>
            <?php } else { ?>
            <li class="pull-right">
                <div><?php echo ucfirst($AutoresponderName)."&nbsp;(".$TotalLists.")";?></div>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<script>
    emails["emails_<?php echo $email->id;?>"] = { "subject": "<?php echo htmlentities(str_replace(array("\r", "\n"), "", html_entity_decode($email->EmailSubject, ENT_QUOTES)));?>", "body": "<?php echo $EmailBody;?>" };
</script>
<?php
    }else{
?>
<div class="small-box bg-gray email-box">
    <div class="small-box-header clearfix">
        <ul class="list-inline">
            <li class="pull-right"><a href="#"><i class="fa fa-times-"></i></a></li>
        </ul>
    </div>
    <div class="inner">
        <h3>
            <a href="<?php site_url("campaign/email/".$CampaignID."/?index=".$index)?>" data-widget="ShowLinkModel" data-related="#email-box-<?php echo $index;?>" data-method="post" data-page-title="Emails Manager" data-model-width="750px">
                <span>Empty</span>
                <span class="small">Click To Create</span>
            </a>
        </h3>
    </div>
    <div class="icon">
        <i class="ion ion-email"></i>
    </div>
</div>
<?php
    }
	$contents = ob_get_contents();
    ob_end_clean();
    return $contents;
}

function get_email_cta_clicks($email){
    global $db, $current_user;

    if(is_numeric($email)){
        $sql = "select * from ".$db->campaign_emails." where userid=".$current_user->id." and id=".$email;
        $email = $db->get_row($sql);
    }

    $cta_links = get_email_links($email->EmailBody, $email, true);
    
    $ClicksTotal = 0;
    if (count($cta_links) > 0){
    	$sql = "select count(*) from ".$db->linkbank_clicks." where ClickType='cta' and EmailID = ".$email->id." and LinkBankID in (".implode(",", $cta_links).")";
        $ClicksTotal = $db->get_var($sql) * 1;
    }

    return $ClicksTotal;
}