<?php
function get_all_linkbank_date_clicks($LinkBankID, $StartDate, $EndDate){
	global $AllDateClicks, $db;
    if(!isset($AllDateClicks))
        $AllDateClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as ClicksDate, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$StartDate." and ".$db->linkbank_clicks.".DateAdded <= ".$EndDate.") GROUP BY ClicksDate", OBJECT_K);
    //print_r("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as DateAdded, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$StartDate." and ".$db->linkbank_clicks.".DateAdded <= ".$EndDate.") GROUP BY FROM_UNIXTIME(DateAdded, '%m/%d/%Y')");die;
    return $AllDateClicks;
}

function LinkBank_SnapShot($LinkBankID, $linkbank_stats = array(), $colors = array()){
    global $db, $current_user;
    $legends = array("Unique", "Non-Unique", "Bots");
    //$colors = array();
    $legendsvalues = array("Total" => 0);

    $piedata = array();
    foreach($legends as $legend){
        if(!array_key_exists($legend, $colors)){
            $color = get_random_rbg($colors);//
            $colors[$legend] = $color;
        }else{
            $color = $colors[$legend];
        }
        $nearstcolor = get_nearest_rbg($color);

        $piedata[] = array("value" => $linkbank_stats[strtolower($legend)], "color" => $color,  "highlight" => $nearstcolor, "label" => $legend);

        $legendsvalues[$legend] = $linkbank_stats[strtolower($legend)];
        $legendsvalues["Total"] += $linkbank_stats[strtolower($legend)];
    }

    //$UniqueClicks = $db->get_results("call linkbank_snapshot_unique(".$LinkBankID.", '".$linkbank_stats['startdate']."', '".$linkbank_stats['enddate']."')", OBJECT_K);
    //$NonUniqueClicks = $db->get_results("call linkbank_snapshot_nonunique(".$LinkBankID.", '".$linkbank_stats['startdate']."', '".$linkbank_stats['enddate']."')", OBJECT_K);
    //$BotsClicks = $db->get_results("call linkbank_snapshot_bots(".$LinkBankID.", '".$linkbank_stats['startdate']."', '".$linkbank_stats['enddate']."')", OBJECT_K);

    $AllDatesClicks = get_all_linkbank_date_clicks($LinkBankID, $linkbank_stats['startdate'], $linkbank_stats['enddate']);
    $UniqueIps = $db->get_col("select ClickIp from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." GROUP BY ClickIp HAVING COUNT(ClickIp) > 1");
    $UniqueClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as ClicksDate, SUM(count) as DateClicks FROM (select 1 AS count, DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$linkbank_stats['startdate']." and ".$db->linkbank_clicks.".DateAdded <= ".$linkbank_stats['enddate'].") and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount GROUP BY ClicksDate", OBJECT_K);
    //$NonUniqueClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as ClicksDate, SUM(count) as DateClicks FROM (select count(ClickIp) AS count, DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$linkbank_stats['startdate']." and ".$db->linkbank_clicks.".DateAdded <= ".$linkbank_stats['enddate'].") and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount GROUP BY ClicksDate", OBJECT_K);
    $NonUniqueClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as ClicksDate, count(*) as DateClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$linkbank_stats['startdate']." and ".$db->linkbank_clicks.".DateAdded <= ".$linkbank_stats['enddate'].") and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) and ClickIp in ('".implode("', '", $UniqueIps)."') GROUP BY ClicksDate", OBJECT_K);
    $BotsClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as ClicksDate, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$linkbank_stats['startdate']." and ".$db->linkbank_clicks.".DateAdded <= ".$linkbank_stats['enddate'].") and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL) GROUP BY ClicksDate", OBJECT_K);
    //print_r($NonUniqueClicks);die;
    $labels = array();
    $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
    for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
        $DateAdded = $dtStart->format('m/d/Y');
        if(isset($AllDatesClicks[$DateAdded]))// isset($UniqueClicks[$DateAdded]) || isset($NonUniqueClicks[$DateAdded]) || isset($BotsClicks[$DateAdded]))
            $labels[] = $DateAdded;//$dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }

    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);

        $dataset = array("label" => $legend, "strokeColor" => $color, "pointColor" => $nearstcolor, "fillColor" => $color, "strokeColor" => $color, "highlightFill" => $nearstcolor, "highlightStroke" => $nearstcolor, "data" => array());
        $datavalues = array();

        

        $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd->modify("+1 day");

        for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
            $value = 0;
            $DateAdded = $dtStart->format('m/d/Y');
            if($legend == "Unique"){                    
                $value = isset($UniqueClicks[$DateAdded])?$UniqueClicks[$DateAdded]->DateClicks * 1:0;
            }else if($legend == "Non-Unique"){
                $value = isset($NonUniqueClicks[$DateAdded])?$NonUniqueClicks[$DateAdded]->DateClicks * 1:0;
            }else if($legend == "Bots"){
                $value = isset($BotsClicks[$DateAdded])?$BotsClicks[$DateAdded]->DateClicks * 1:0;
            }
            if(isset($AllDatesClicks[$DateAdded]))
                $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        $dataset['data'] = $datavalues;

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function LinkBank_TopBrowser($LinkBankID, $linkbank_stats = array()){
    global $db, $current_user;
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($linkbank_stats['startdate']) && !empty($linkbank_stats['enddate'])?" and (".$db->linkbank_clicks.".DateAdded >= '".$linkbank_stats['startdate']."' and ".$db->linkbank_clicks.".DateAdded <= '".$linkbank_stats['enddate']."') ":"";
    $sql = "select count(*) as BrowserClicks, CONCAT(BrowserName , ' ' , BrowserVersion) as Browser FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY Browser";
    $Browsers = $db->get_results($sql);
    foreach($Browsers as $Browser){
        $legend = $Browser->Browser;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $BrowserClicks = $Browser->BrowserClicks;

        //$piedata[] = '{"value": "'.$BrowserClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
        $piedata[] = array("value" => $BrowserClicks, "color" => $color,  "highlight" => $nearstcolor, "label" => $legend);

        $legendsvalues[$legend] = $BrowserClicks;
        $legendsvalues["Total"] += $BrowserClicks;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
    $BotsClicks = $db->get_var($sql) * 1;

    //$piedata[] = '{"value": "'.$BotsClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
    $piedata[] = array("value" => $BotsClicks, "color" => $color,  "highlight" => $nearstcolor, "label" => $legend);

    $legendsvalues[$legend] = $BotsClicks;
    $legendsvalues["Total"] += $BotsClicks;

    $AllDatesClicks = get_all_linkbank_date_clicks($LinkBankID, $linkbank_stats['startdate'], $linkbank_stats['enddate']);
    $labels = array();
    $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
    for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
        $DateAdded = $dtStart->format('m/d/Y');
        if(isset($AllDatesClicks[$DateAdded]))
            $labels[] = $DateAdded;//$labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    //$BrowserClicks = $db->get_results("SELECT FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as DateAdded, SUM(count) as DateClicks FROM (select 1 AS count, DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID." and (".$db->linkbank_clicks.".DateAdded >= ".$linkbank_stats['startdate']." and ".$db->linkbank_clicks.".DateAdded <= ".$linkbank_stats['enddate'].") and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount GROUP BY FROM_UNIXTIME(DateAdded, '%m/%d/%Y')", OBJECT_K);
    $AllBrowsersClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~', BrowserName , ' ' , BrowserVersion) as DateAddedBrowser, count(*) as DateClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY DateAddedBrowser", OBJECT_K);
    $AllBotsClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~Bots') as DateAddedBots, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL) GROUP BY DateAddedBots", OBJECT_K);

    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        //$dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $dataset = array("label" => $legend, "strokeColor" => $color, "pointColor" => $nearstcolor, "fillColor" => $color, "strokeColor" => $color, "highlightFill" => $nearstcolor, "highlightStroke" => $nearstcolor, "data" => array());

        $datavalues = array();

        $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
            $value = 0;
            $DateAdded = $dtStart->format('m/d/Y');
            $ClicksKey = $DateAdded."~".$legend;
            
            //$datesql = " and (".$db->linkbank_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".$db->linkbank_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";

            if($legend == 'Bots'){
                $value = isset($AllBotsClicks[$ClicksKey])?$AllBotsClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
            } else {

                $value = isset($AllBrowsersClicks[$ClicksKey])?$AllBrowsersClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(*) as BrowserClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and CONCAT(BrowserName , ' ' , BrowserVersion) = '".$legend."' and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY CONCAT(BrowserName , ' ' , BrowserVersion)";
            }

            //$value = $db->get_var($sql) * 1;
            if(isset($AllDatesClicks[$DateAdded]))
                $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        //$dataset .= implode(',', $datavalues).']}';
        $dataset['data'] = $datavalues;
        
        $datasets[] = $dataset;
    }
    
    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function LinkBank_TopPlatform($LinkBankID, $linkbank_stats = array()){
    global $db, $current_user;
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($linkbank_stats['startdate']) && !empty($linkbank_stats['enddate'])?" and (".$db->linkbank_clicks.".DateAdded >= '".$linkbank_stats['startdate']."' and ".$db->linkbank_clicks.".DateAdded <= '".$linkbank_stats['enddate']."') ":"";
    $sql = "select count(*) as PlatformClicks, Platform FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY Platform";
    $Platforms = $db->get_results($sql);
    foreach($Platforms as $Platform){
        $legend = $Platform->Platform;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $PlatformClicks = $Platform->PlatformClicks;

        //$piedata[] = '{"value": "'.$PlatformClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
        $piedata[] = array("value" => $PlatformClicks, "color" => $color,  "highlight" => $nearstcolor, "label" => $legend);

        $legendsvalues[$legend] = $PlatformClicks;
        $legendsvalues["Total"] += $PlatformClicks;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
    $BotsClicks = $db->get_var($sql) * 1;

    //$piedata[] = '{"value": "'.$BotsClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
    $piedata[] = array("value" => $BotsClicks, "color" => $color,  "highlight" => $nearstcolor, "label" => $legend);

    $legendsvalues[$legend] = $BotsClicks;
    $legendsvalues["Total"] += $BotsClicks;


    $AllDatesClicks = get_all_linkbank_date_clicks($LinkBankID, $linkbank_stats['startdate'], $linkbank_stats['enddate']);
    $labels = array();
    $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
    for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
        $DateAdded = $dtStart->format('m/d/Y');
        if(isset($AllDatesClicks[$DateAdded]))
            $labels[] = $DateAdded;//$labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    $AllPlatformsClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~', Platform) as DateAddedPlatform, count(*) as DateClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY DateAddedPlatform", OBJECT_K);
    $AllBotsClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~Bots') as DateAddedBots, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL) GROUP BY DateAddedBots", OBJECT_K);

    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        //$dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $dataset = array("label" => $legend, "strokeColor" => $color, "pointColor" => $nearstcolor, "fillColor" => $color, "strokeColor" => $color, "highlightFill" => $nearstcolor, "highlightStroke" => $nearstcolor, "data" => array());
        $datavalues = array();

        $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
            $value = 0;
            $DateAdded = $dtStart->format('m/d/Y');
            $ClicksKey = $DateAdded."~".$legend;
            //$datesql = " and (".$db->linkbank_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".$db->linkbank_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";

            if($legend == 'Bots'){
                $value = isset($AllBotsClicks[$ClicksKey])?$AllBotsClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
            } else {
                $value = isset($AllPlatformsClicks[$ClicksKey])?$AllPlatformsClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(*) as PlatformClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and Platform = '".$legend."' and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY Platform";
            }

            //$value = $db->get_var($sql) * 1;
            if(isset($AllDatesClicks[$DateAdded]))
                $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        //$dataset .= implode(',', $datavalues).']}';
        $dataset['data'] = $datavalues;

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}

function LinkBank_TopCountries($LinkBankID, $linkbank_stats = array()){
    global $db, $current_user;
    $legends = array();
    $colors = array();
    $piedata = array();
    $legendsvalues = array("Total" => 0);

    $datesql = !empty($linkbank_stats['startdate']) && !empty($linkbank_stats['enddate'])?" and (".$db->linkbank_clicks.".DateAdded >= '".$linkbank_stats['startdate']."' and ".$db->linkbank_clicks.".DateAdded <= '".$linkbank_stats['enddate']."') ":"";
    $sql = "select count(*) as CountryClicks, countryname FROM ".$db->linkbank_clicks." INNER JOIN ".$db->countries." on ".$db->linkbank_clicks.".CountryCode = ".$db->countries.".countrycode where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ".$db->linkbank_clicks.".CountryCode";
    $Countries = $db->get_results($sql);
    foreach($Countries as $Country){
        $legend = $Country->countryname;
        $legends[] = $legend;
        $color = get_random_rbg($colors);//
        $colors[$legend] = $color;
        $nearstcolor = get_nearest_rbg($color);
        $CountryClicks = $Country->CountryClicks;

        //$piedata[] = '{"value": "'.$CountryClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
        $piedata[] = array("value" => $CountryClicks ,"color" => $color, "highlight" => $nearstcolor, "label" => $legend);

        $legendsvalues[$legend] = $CountryClicks;
        $legendsvalues["Total"] += $CountryClicks;
    }


    $legend = "Bots";
    $legends[] = $legend;
    $color = get_random_rbg($colors);//
    $colors[$legend] = $color;
    $nearstcolor = get_nearest_rbg($color);

    $sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
    $BotsClicks = $db->get_var($sql) * 1;

    //$piedata[] = '{"value": "'.$BotsClicks.'" ,"color": "'.$color.'", "highlight": "'.$nearstcolor.'","label": "'.$legend.'"}';
    $piedata[] = array("value" => $BotsClicks ,"color" => $color, "highlight" => $nearstcolor, "label" => $legend);

    $legendsvalues[$legend] = $BotsClicks;
    $legendsvalues["Total"] += $BotsClicks;


    $AllDatesClicks = get_all_linkbank_date_clicks($LinkBankID, $linkbank_stats['startdate'], $linkbank_stats['enddate']);
    $labels = array();
    $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
    for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
        $DateAdded = $dtStart->format('m/d/Y');
        if(isset($AllDatesClicks[$DateAdded]))
            $labels[] = $DateAdded;//$labels[] = $dtStart->format('m/d/Y');
        $dtStart->modify("+1 day");
    }
    
    //$AllCountriesClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~', CountryCode) as DateAddedCountries, count(*) as DateClicks FROM ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY DateAddedCountries", OBJECT_K);
    $AllCountriesClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(".$db->linkbank_clicks.".DateAdded, '%m/%d/%Y'), '~', ".$db->countries.".countryname) as DateAddedCountries, count(*) as DateClicks FROM ".$db->linkbank_clicks." INNER JOIN ".$db->countries." on ".$db->linkbank_clicks.".CountryCode = ".$db->countries.".countrycode where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY DateAddedCountries", OBJECT_K);
    $AllBotsClicks = $db->get_results("select CONCAT(FROM_UNIXTIME(DateAdded, '%m/%d/%Y'), '~Bots') as DateAddedBots, count(ClickIp) as DateClicks from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL) GROUP BY DateAddedBots", OBJECT_K);
    
    $datasets = array();
    foreach($legends as $legend){
        $color = $colors[$legend];
        $nearstcolor = get_nearest_rbg($color);
        
        //$dataset = '{"label": "'.$legend.'","strokeColor": "'.$color.'","pointColor": "'.$nearstcolor.'","fillColor": "'.$color.'","strokeColor": "'.$color.'","highlightFill": "'.$nearstcolor.'","highlightStroke": "'.$nearstcolor.'","data": [';
        $dataset = array("label" => $legend, "strokeColor" => $color, "pointColor" => $nearstcolor, "fillColor" => $color, "strokeColor" => $color, "highlightFill" => $nearstcolor, "highlightStroke" => $nearstcolor, "data" => array());
        $datavalues = array();

        $dtStart = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd = new DateTime(date('r', $linkbank_stats['startdate']));
        $dtEnd->modify("+1 day");
        
        for($dayindex = 1; $dayindex <= $linkbank_stats['totaldays']; $dayindex++){
            $value = 0;
            $DateAdded = $dtStart->format('m/d/Y');
            $ClicksKey = $DateAdded."~".$legend;
            //$datesql = " and (".$db->linkbank_clicks.".DateAdded >= '".strtotime($dtStart->format('r'))."' and ".$db->linkbank_clicks.".DateAdded < '".strtotime($dtEnd->format('r'))."') ";

            if($legend == 'Bots'){
                $value = isset($AllBotsClicks[$ClicksKey])?$AllBotsClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$LinkBankID.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
            } else {
                $value = isset($AllCountriesClicks[$ClicksKey])?$AllCountriesClicks[$ClicksKey]->DateClicks * 1:0;
                //$sql = "select count(*) as CountryClicks FROM ".$db->linkbank_clicks." INNER JOIN ".$db->countries." on ".$db->linkbank_clicks.".CountryCode = ".$db->countries.".countrycode where LinkBankID=".$LinkBankID.$datesql." and countryname='".$legend."' and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ".$db->linkbank_clicks.".CountryCode";
            }

            //$value = $db->get_var($sql) * 1;
            if(isset($AllDatesClicks[$DateAdded]))
                $datavalues[] = $value;

            $dtStart->modify("+1 day");
            $dtEnd->modify("+1 day");
        }
        //$dataset .= implode(',', $datavalues).']}';
        $dataset['data'] = $datavalues;

        $datasets[] = $dataset;
    }

    $data = array(
        "piedata"           => $piedata, 
        "legends"           => $legends,
        "legendsvalues"     => $legendsvalues,
        "colors"            => $colors, 
        "labels"            => $labels, 
        "datasets"          => $datasets
    );
    return $data;
}