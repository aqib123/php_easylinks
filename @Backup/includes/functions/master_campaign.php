<?php
function get_master_campaign_details($MasterCampaignID){
    global $db, $current_user;
	$MasterCampaignDetails = array();

    $LinkBankClicks = $db->get_var("select count(*) from ".$db->linkbank_clicks." inner join ".$db->linkbanks." on ".$db->linkbank_clicks.".LinkBankID = ".$db->linkbanks.".id where MasterCampaignID=".$MasterCampaignID) * 1;
    $EPCLinkBankClicks = $db->get_var("select count(*) from ".$db->linkbank_clicks." inner join ".$db->linkbanks." on ".$db->linkbank_clicks.".LinkBankID = ".$db->linkbanks.".id where TrackEPC = 1 and MasterCampaignID=".$MasterCampaignID) * 1;
    $RotatorClicks = $db->get_var("select SUM(RotatorClickCount) as TotalRotatorClicks from (select count(*) as RotatorClickCount from ".$db->rotator_clicks." where RotatorLinkID in (SELECT id from ".$db->rotator_links." where MasterCampaignID=".$MasterCampaignID." and userid=".$current_user->id.")) as RotatorClicks") * 1;
    $LinkSequenceClicks = $db->get_var("select SUM(LinkSequenceClickCount) as TotalLinkSequenceClicks from (select count(*) as LinkSequenceClickCount from ".$db->link_sequence_clicks." where LinkSequenceLinkID in (SELECT id from ".$db->link_sequence_links." where MasterCampaignID=".$MasterCampaignID." and userid=".$current_user->id.")) as LinkSequenceClicks") * 1;
    $PaidTrafficClicks = $db->get_var("select count(*) from ".$db->paidtraffic_clicks." inner join ".$db->paidtraffics." on ".$db->paidtraffic_clicks.".PaidTrafficID = ".$db->paidtraffics.".id where MasterCampaignID=".$MasterCampaignID) * 1;
    $TotalClicks = $LinkBankClicks + $RotatorClicks + $PaidTrafficClicks;

    $MasterCampaignDetails["LinkBankClicks"] = $LinkBankClicks;
    $MasterCampaignDetails["EPCLinkBankClicks"] = $EPCLinkBankClicks;
    $MasterCampaignDetails["RotatorClicks"] = $RotatorClicks;
    $MasterCampaignDetails["LinkSequenceClicks"] = $LinkSequenceClicks;
    $MasterCampaignDetails["PaidTrafficClicks"] = $PaidTrafficClicks;
    $MasterCampaignDetails["TotalClicks"] = $TotalClicks;

    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "pending"));
    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "active"));
    $MasterCampaignDetails = array_merge($MasterCampaignDetails, get_master_campaign_sub_details($MasterCampaignID, "complete"));

    return $MasterCampaignDetails;
}

function get_master_campaign_sub_details($MasterCampaignID, $status){
	global $db, $current_user;
	$MasterCampaignSubDetails = array();

    $LinkBanks = $db->get_var("select count(*) from ".$db->linkbanks." where MasterCampaignID=".$MasterCampaignID." and LinkStatus='".$status."'") * 1;
    $Rotators = $db->get_var("SELECT count(*) from ".$db->rotators." where MasterCampaignID=".$MasterCampaignID." and RotatorStatus='".$status."'") * 1;
    $LinkSequences = $db->get_var("SELECT count(*) from ".$db->link_sequences." where MasterCampaignID=".$MasterCampaignID." and LinkSequenceStatus='".$status."'") * 1;
    $PaidTraffics = $db->get_var("select count(*) from ".$db->paidtraffics." where MasterCampaignID=".$MasterCampaignID." and PaidTrafficStatus='".$status."'") * 1;
    $Campaigns = $db->get_var("select count(*) from ".$db->campaigns." where MasterCampaignID=".$MasterCampaignID." and CampaignStatus='".$status."'") * 1;
    $Total = $LinkBanks + $Rotators + $PaidTraffics + $Campaigns;

    $MasterCampaignSubDetails["LinkBanks".ucwords($status)] = $LinkBanks;
    $MasterCampaignSubDetails["Rotators".ucwords($status)] = $Rotators;
    $MasterCampaignSubDetails["LinkSequences".ucwords($status)] = $LinkSequences;
    $MasterCampaignSubDetails["PaidTraffics".ucwords($status)] = $PaidTraffics;
    $MasterCampaignSubDetails["Campaigns".ucwords($status)] = $Campaigns;
    $MasterCampaignSubDetails["Total".ucwords($status)] = $Total;

    return $MasterCampaignSubDetails;
}

function delete_campaign_stats($CampaignID){
    global $current_user, $db;
    if($db->get_row("select * from ".$db->campaigns." where id=".$CampaignID." and userid=".$current_user->id)){
        $db->query("delete from ".$db->linkbank_clicks." where EmailID in (select id from ".$db->campaign_emails." where userid = ".$current_user->id." and CampaignID = ".$CampaignID.")");
    }
}