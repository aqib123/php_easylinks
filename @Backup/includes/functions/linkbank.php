<?php
function get_linkbankurl($linkbank, $sublink = ""){
    global $db, $current_user;
    $username = $current_user->user_login;
    if(is_numeric($linkbank)){
        $sql = "select * from ".$db->linkbanks." where userid=".$current_user->id." and id=".$linkbank;
        $linkbank = $db->get_row($sql);
    }
    $linkbankurl = "#";
    if($linkbank){
        $domain = $db->get_row("select * from ".$db->domains." where id=".$linkbank->DomainID);
        if($domain){
            if ($domain->DomainType == "admindomain")
                $linkbankurl = str_replace("://", "://".$username.".", rtrim($domain->DomainUrl, "/"))."/";
            else
                $linkbankurl = rtrim($domain->DomainUrl, "/")."/";
            
            if(!empty($sublink))
                $linkbankurl .= rtrim($sublink, "/")."/";

            $linkbankurl .= rtrim($linkbank->VisibleLink, "/");
        }
    }
    return $linkbankurl;
}

function get_linkbank_stats($linkbankid, $startdate = "", $enddate = ""){
    global $db, $current_user;

    $stats = array(
        "total" => 0,
        "unique" => 0,
        "nonunique" => 0,
        "bots" => 0,
        "dailyaverage" => 0,
    );

    $sql = "select * from ".$db->linkbanks." where id=".$linkbankid." and userid=".$current_user->id;
    $linkbank = $db->get_row($sql);
    if($linkbank){

        $datesql = !empty($startdate) && !empty($enddate)?" and (".$db->linkbank_clicks.".DateAdded >= '".strtotime($startdate)."' and ".$db->linkbank_clicks.".DateAdded <= '".strtotime($enddate)."') ":"";

        $sql = "select count(*) from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql;
        $TotalClicks = $db->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select 1 AS count from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount";
        $UniqueClicks = $db->get_var($sql) * 1;

        $sql = "SELECT SUM(count) FROM (select count(ClickIp) AS count from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." and (".$db->linkbank_clicks.".BotName = '' or ".$db->linkbank_clicks.".BotName is NULL) GROUP BY ClickIp) as ClicksCount";
        $NonUniqueClicks = $db->get_var($sql) * 1;
        //$NonUniqueClicks -= $UniqueClicks;

        $sql = "select count(ClickIp) from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." and (".$db->linkbank_clicks.".BotName <> '' and ".$db->linkbank_clicks.".BotName is not NULL)";
        $BotsClicks = $db->get_var($sql) * 1;
        
        //$sql = "select count(*) from ".$db->linkbank_clicks." inner join ".$db->countries." on ".$db->linkbank_clicks.".CountryCode = ".$db->countries.".countrycode where countrytier = 1 and (BotName = '' or BotName is NULL) and LinkBankID=".$linkbank->id.$datesql;
        //$TotalTopClicks = $db->get_var($sql) * 1;

        //$TopClicksPercentage = $TotalClicks > 0?round(($TotalTopClicks * 100) / $TotalClicks):0;

        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".$db->linkbank_clicks.".DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." ORDER BY ".$db->linkbank_clicks.".DateAdded ASC limit 0,1";
            $date1 = $db->get_var($sql);

            $sql = "select ".$db->linkbank_clicks.".DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." ORDER BY ".$db->linkbank_clicks.".DateAdded DESC limit 0,1";
            $date2 = $db->get_var($sql);

            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $DailyAverage = $TotalDays > 0?round($TotalClicks / $TotalDays, 2):0;

        $stats = array(
            "total"         => $TotalClicks,
            "unique"        => $UniqueClicks,
            "non-unique"     => $NonUniqueClicks,
            "bots"          => $BotsClicks,
            "dailyaverage"  => $DailyAverage,
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $stats;
}

function get_linkbank_dates($linkbankid, $startdate = "", $enddate = ""){
    global $db, $current_user;

    $dates = array(
        "startdate"     => 0,
        "enddate"       => 0,
        "totaldays"     => 0,
    );

    $sql = "select * from ".$db->linkbanks." where id=".$linkbankid." and userid=".$current_user->id;
    $linkbank = $db->get_row($sql);
    if($linkbank){
        if(!empty($startdate) && !empty($enddate)){
            $date1 = strtotime($startdate);
            $date2 = strtotime($enddate);
            $TotalDays = round(abs($date2-$date1)/86400) + 1;
        }else{
            $sql = "select ".$db->linkbank_clicks.".DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." ORDER BY ".$db->linkbank_clicks.".DateAdded ASC limit 0,1";
            $date1 = $db->get_var($sql);

            $sql = "select ".$db->linkbank_clicks.".DateAdded from ".$db->linkbank_clicks." where LinkBankID=".$linkbankid.$datesql." ORDER BY ".$db->linkbank_clicks.".DateAdded DESC limit 0,1";
            $date2 = $db->get_var($sql);

            $TotalDays = round(abs($date2-$date1)/86400) + 2;
        }

        $dates = array(
            "startdate"     => $date1,
            "enddate"       => $date2,
            "totaldays"     => $TotalDays,
        );
    }

    return $dates;
}

function get_linkbank_conversion_pixel($conversion_pixel, $pixel_key = ""){
	global $db, $current_user;
    if($conversion_pixel != null && is_numeric($conversion_pixel))
        $conversion_pixel = $db->get_row("select * from ".$db->linkbank_conversion_pixels." where userid=".$current_user->id." and id=".$conversion_pixel);

    $conversion_pixel_id = $conversion_pixel == null?$pixel_key:$conversion_pixel->id;
	ob_start();
?>
<div class="linkbank_conversion_pixel_container">
    <a href="javascript:" class="btn btn-sm btn-remove-pixel" title="Delete Pixel"><i class="fa fa-trash"></i></a>
    <input class="linkbank_conversion_pixel_id" type="hidden" name="linkbank_conversion_pixels[]" value="<?php echo $conversion_pixel_id;?>" />
    <div class="form-group select-form-group">
        <label for="ConversionPixelType-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion Type</label>
        <div class="col-sm-8 ">
            <select name="ConversionPixelType-<?php echo $conversion_pixel_id;?>" id="ConversionPixelType-<?php echo $conversion_pixel_id;?>" class="form-control select2" style="width: 100%">
                <option value="Frontend" <?php if($conversion_pixel->ConversionPixelType == "Frontend") echo ' selected="selected" ';?>>Frontend</option>
                <option value="Upsell" <?php if($conversion_pixel->ConversionPixelType == "Upsell") echo ' selected="selected" ';?>>Upsell</option>
                <option value="Downsell" <?php if($conversion_pixel->ConversionPixelType == "Downsell") echo ' selected="selected" ';?>>Downsell</option>
            </select>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelName-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion Name</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelName?>" class="form-control " id="ConversionPixelName-<?php echo $conversion_pixel_id;?>" name="ConversionPixelName-<?php echo $conversion_pixel_id;?>" placeholder="Conversion Name" <?php echo NAME_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Conversion URL</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelURL?>" class="form-control " id="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" name="ConversionPixelURL-<?php echo $conversion_pixel_id;?>" placeholder="Conversion URL" <?php echo URL_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group has-feedback">
        <label for="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Unit Price</label>
        <div class="col-sm-8">
            <input type="text" value="<?php if($conversion_pixel) echo $conversion_pixel->ConversionPixelUnitPrice?>" class="form-control conversion_pixel_unit_price" id="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" name="ConversionPixelUnitPrice-<?php echo $conversion_pixel_id;?>" placeholder="Unit Price For Sales & Conversions" <?php echo FLOAT_PATTERN;?> />
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <small class="help-block with-errors"></small>
        </div>
    </div>

    <div class="form-group">
        <label for="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" class="col-sm-4 control-label">Tracking Code</label>
        <div class="col-sm-8">
            <textarea readonly="readonly" class="form-control ConversionPixelTrackingCode" id="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" name="ConversionPixelTrackingCode-<?php echo $conversion_pixel_id;?>" placeholder="Tracking Code For Sales & Conversions" cols="3"><?php echo $conversion_pixel?$conversion_pixel->ConversionPixelTrackingCode:"Save Linkbank to get pixel code";?></textarea>
            <small class="help-block text-green text-xs">Place this tracking code just below the opening &lt;body&gt; on the page that follows your Conv Url</small>
            <small class="help-block with-errors"></small>
        </div>
    </div>
    <hr />
</div>
<?php
    $contents = ob_get_contents();
    ob_clean();

    return $contents;
}

function delete_linkbank_status($LinkBankID){
    global $current_user, $db;
    if($db->get_row("select * from ".$db->linkbanks." where id=".$LinkBankID." and userid=".$current_user->id)){
        $db->delete($db->linkbank_clicks, array("LinkBankID" => $LinkBankID));
        $db->delete($db->linkbank_conversions, array("LinkBankID" => $LinkBankID));
        $db->delete($db->linkbank_pixel_fires, array("LinkBankID" => $LinkBankID));
    }
}

function get_sale_info_details($linkbank) {
    global $db, $current_user;
    
    $SaleInfo = array(
                "GrossSale" => 0,
                "OwedCommissions" => 0,
                "ExtraIncome" => 0,
                "GrossIncome" => 0,
                "Commission" => 0,
                "NetIncome" => 0,
                "OwedToPartner" => 0,
                "Expenses" => 0,
                "PartnerExpenses" => 0
            );

    if (is_numeric($linkbank)){
    	$sql = "select * from ".$db->linkbanks." where userid=".$current_user->id." and id=".$linkbank;
        $linkbank = $db->get_row($sql);
    }

    if ($linkbank) {
        $GrossSale = $linkbank->GrossSale * 1;
        $ExtraIncome = $linkbank->ExtraIncome * 1;
        $Expenses = $linkbank->Expenses * 1;
        $Commission = $linkbank->Commission * 1;
        $PartnerSplit = $linkbank->PartnerSplit * 1;
        $SplitExpensesWithPartner = $linkbank->SplitExpensesWithPartner * 1;
        $SplitExpensesPercentage = $linkbank->SplitExpensesPercentage * 1;
        $GrossIncome = $GrossSale + $ExtraIncome;

        $GrossTotal = $GrossSale;// + $ExtraIncome;
        $NetIncomeTotal = $GrossTotal;
        $OwedToPartner = 0;
        $PartnerExpenses = 0;
        $OwedCommissions = 0;

        if ($Commission > 0){
            $NetIncomeTotal = $GrossTotal * $Commission / 100;
            $OwedCommissions = $GrossSale * $Commission / 100;
        }
        $NetIncomeTotal += $ExtraIncome;

        if ($PartnerSplit > 0)
            $OwedToPartner = $NetIncomeTotal * $PartnerSplit / 100;
        
        $NetIncomeTotal = $NetIncomeTotal - $OwedToPartner;

        if ($SplitExpensesWithPartner == 1 && $SplitExpensesPercentage > 0){
            $PartnerExpenses = $Expenses * $SplitExpensesPercentage / 100;
            $Expenses = $Expenses - $PartnerExpenses;
            $OwedToPartner = $OwedToPartner - $PartnerExpenses;

            $SaleInfo["PartnerExpenses"] = $PartnerExpenses;
        }

        $NetIncomeTotal -= $Expenses;
        $SaleInfo["GrossSale"] = $GrossSale;
        $SaleInfo["OwedCommissions"] = $OwedCommissions;
        $SaleInfo["ExtraIncome"] = $ExtraIncome;
        $SaleInfo["Expenses"] = $Expenses;
        $SaleInfo["Commission"] = $Commission;
        $SaleInfo["GrossIncome"] = $GrossIncome;//$GrossTotal;
        $SaleInfo["NetIncome"] = $NetIncomeTotal;
        $SaleInfo["OwedToPartner"] = $OwedToPartner;
    }

    return $SaleInfo;
}

function update_linkbank_conversion_pixels($LinkBankID){
	global $db, $current_user;
    if(isset($_POST["linkbank_conversion_pixels"]) && is_array($_POST["linkbank_conversion_pixels"])){
        $linkbank_conversion_pixels = $_POST["linkbank_conversion_pixels"];
        foreach ($linkbank_conversion_pixels as $linkbank_conversion_pixel_id){
            $data = array(
                "LinkBankID"                    => $LinkBankID,
                "ConversionPixelType"           => $_POST["ConversionPixelType-".$linkbank_conversion_pixel_id],
                "ConversionPixelName"           => $_POST["ConversionPixelName-".$linkbank_conversion_pixel_id],
                "ConversionPixelURL"            => $_POST["ConversionPixelURL-".$linkbank_conversion_pixel_id],
                "ConversionPixelUnitPrice"      => $_POST["ConversionPixelUnitPrice-".$linkbank_conversion_pixel_id],
                "ConversionPixelTrackingCode"   => $_POST["ConversionPixelTrackingCode-".$linkbank_conversion_pixel_id],
            );
        	if(strpos($linkbank_conversion_pixel_id, "newpixel-") > -1){
                $data["DateAdded"] = strtotime("now");
                unset($data["ConversionPixelTrackingCode"]);
                $data["userid"] = $current_user->id;
                if($db->insert($db->linkbank_conversion_pixels, $data))
                    $db->update($db->linkbank_conversion_pixels, array("ConversionPixelTrackingCode" => get_pixel_image("sp", $data["ConversionPixelUnitPrice"]), "lpid=".$db->insert_id), array("id" => $db->insert_id));
            } else {
                $db->update($db->linkbank_conversion_pixels, $data, array("id" => $linkbank_conversion_pixel_id));
            }
        }
    }

    if(isset($_POST["linkbank_conversion_pixel_delete"]) && is_array($_POST["linkbank_conversion_pixel_delete"])){
        $linkbank_conversion_pixel_delete = $_POST["linkbank_conversion_pixel_delete"];
        foreach ($linkbank_conversion_pixel_delete as $linkbank_conversion_pixel_id){
        	if(strpos($linkbank_conversion_pixel_id, "newpixel-") <= -1){
                $db->delete(
                    $db->linkbank_conversion_pixels, 
                    array(
                        "id" => $linkbank_conversion_pixel_id, 
                        "LinkBankID" => $LinkBankID
                    ));
            }
        }
    }
}

function get_linkbank_rows($draw, $linkbankstype, $linkbank_sale_type, $pageurl, $curpage, $start, $length, $order, $search, $searchcolumn = -1){
    global $db, $current_user;

    $jsonData = array(
        "draw" => $draw,
        "recordsTotal" => 0,
        "recordsFiltered" => 0,
        'data' => array()
    );

    if($linkbank_sale_type == "conv") {
        $columns = array("", "", "", "LinkName", "", "VendorName", "GroupName", "MasterCampaignName", "RawClicks", "UniqueClicks", "ConversionsCount", "ConversionsAmount", "ConversionsPercentage", "CreatedDate", "");
    } else {
        $columns = array("", "", "LinkName", "", "VendorName", "GroupName", "MasterCampaignName", "RawClicks", "UniqueClicks", "", "CreatedDate", "");
    }

    $sql = "select id, LinkName, LinkStatus, VisibleLink, DomainID, MasterCampaignID, DateAdded, FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as CreatedDate, 
                                    GrossSale, ExtraIncome, Expenses, Commission, PartnerSplit, SplitExpensesWithPartner, SplitExpensesPercentage, 
	                                (select GroupName from ".$db->groups." where id=".$db->linkbanks.".GroupID) as GroupName, 
	                                (select VendorName from ".$db->vendors." where id=".$db->linkbanks.".VendorID) as VendorName,
	                                LinkBankUniqueClicks(".$db->linkbanks.".id) as UniqueClicks,
	                                (select count(*) from ".$db->linkbank_clicks." where LinkBankID=".$db->linkbanks.".id) as RawClicks,
                                    (select MasterCampaignName from ".$db->master_campaigns." where id=".$db->linkbanks.".MasterCampaignID) as MasterCampaignName ";
    if($linkbank_sale_type == "conv") {
        $sql .= ",
	                                (select count(*) from ".$db->linkbank_conversions." where LinkBankID=".$db->linkbanks.".id and ConversionType='sales') as ConversionsCount,
	                                (select sum(UnitPrice) from ".$db->linkbank_conversions." where LinkBankID=".$db->linkbanks.".id and ConversionType='sales') as ConversionsAmount,
                                    (IF((select ConversionsCount)> 0,(select ConversionsCount) / (select UniqueClicks) * 100, 0)) as ConversionsPercentage ";
    }
    $sql .= " from ".$db->linkbanks." where id ".($linkbank_sale_type == ""?"not ":"")." in(select LinkBankID from ".$db->linkbank_conversion_pixels.") and userid=".$current_user->id;

    if($linkbankstype == "pending")
        $sql .= " and LinkStatus = 'pending' ";
    else if($linkbankstype == "active")
        $sql .= " and LinkStatus = 'active' ";
    else if($linkbankstype == "complete")
        $sql .= " and LinkStatus = 'complete' ";
    else if($linkbankstype == "evergreen")
        $sql .= " and LinkStatus = 'evergreen' ";
    else if($linkbankstype == "mylink")
        $sql .= " and LinkStatus = 'mylink' ";

    if(is_array($search) && !empty($search['value'])){
        $sql .= " having (";
        foreach ($columns as $column){
            if($searchcolumn == -1 or $columns[$searchcolumn] == $column){
                if(!empty($column)){
                    if($column != "RawClicks" && $column != "UniqueClicks")
                        $sql .= $column." like '%".$search['value']."%' or ";
                    else if(is_numeric($search['value']))
                        $sql .= $column."=".$search['value']." or ";
                }
            }
        }
        $sql = rtrim($sql, 'or ');
        $sql .= ") ";

        $sqlCount = "select count(*) from (".$sql.") as FilteredResults";
    } else {
        $sqlCount = "select count(*) from ".$db->linkbanks." where id ".($linkbank_sale_type == ""?"not ":"")."in(select LinkBankID from ".$db->linkbank_conversion_pixels.") and userid=".$current_user->id;
    }

    if(is_array($order)){
        $columnname = $columns[$order['column']]=="CreatedDate"?"DateAdded":$columns[$order['column']];
        $sql .= " order by ".$columnname." ".$order['dir'];
    }

    if($length > 0)
        $sql .= " limit ".$start.", ".$length;

    //$sqlCount = "select count(*) from ".$db->linkbanks." where id ".($linkbank_sale_type == ""?"not ":"")."in(select LinkBankID from ".$db->linkbank_conversion_pixels.") and userid=".$current_user->id;

    $jsonData['recordsTotal'] = $db->get_var($sqlCount);
    $jsonData['recordsFiltered'] = $db->get_var($sqlCount);

    $linkbanks = $db->get_results($sql);
    foreach($linkbanks as $linkbank){
        $GroupName = $linkbank->GroupName;
        $VendorName = $linkbank->VendorName;
        //$userid = $current_user->id;
        
        $UniqueClicks = $linkbank->UniqueClicks;
        $RawClicks = $linkbank->RawClicks;

        //$Actions = $linkbank->Actions;
        //$PixelFires = $linkbank->PixelFires;

        $ManualSale = $db->get_row("select * from ".$db->manualsales." where MasterCampaignID = 0 and LinkBankID=".$linkbank->id." and userid=".$current_user->id);
        if($ManualSale){
            $SaleAmount = get_manualsale_amount($linkbank->id);
            $manualsalelink = '<a href="'.get_site_url("manual-sale/split-partners/?LinkBankID=".$linkbank->id).'" class="" title="Manual Sale">$'.$SaleAmount['TotalAmount'].'</a>';
        } else {
            $manualsalelink = '<a href="'.get_site_url("manual-sale/?LinkBankID=".$linkbank->id).'" class="" title="Manual Sale">create</a>';
        }

        $linkbankurl = get_linkbankurl($linkbank);
        //$SaleInfoDetails = get_sale_info_details($linkbank);

        $LinkStatus = $linkbank->LinkStatus;
        if($LinkStatus == "active"){
            //$linkbankstatus = "Live";
            $linkbankstatusicon = "fa fa-rocket";
        }else if($LinkStatus == "pending"){
            //$linkbankstatus = "Pending";
            $linkbankstatusicon = "fa fa-hourglass-o";
        }else if($LinkStatus == "complete"){
            //$linkbankstatus = "Completed";
            $linkbankstatusicon = "fa fa-check-square-o";
        }else if($LinkStatus == "evergreen"){
            //$linkbankstatus = "Evergreen";
            $linkbankstatusicon = "fa fa-tree";
        }else if($LinkStatus == "mylink"){
            //$linkbankstatus = "My Links";
            $linkbankstatusicon = "fa fa-user";
        }

        $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$linkbank->id);

        $StatusIcon = get_domain_status_icon($linkbank->DomainID);

        //$MasterCampaignName = $linkbank->MasterCampaignName;

        $conv_no = $linkbank->ConversionsCount;
        $conv_amount = $linkbank->ConversionsAmount;
        $conv_percent =  $linkbank->ConversionsPercentage;//$UniqueClicks == 0?0:($conv_no / $UniqueClicks * 100);

        //$actions_percentage = $linkbank->ActionPercentage;//$Actions / $UniqueClicks * 100;

        $linkbankData = array();
        $linkbankData[] = str_replace('<span ', '<span data-ajax-parent-class="text-center" ', $StatusIcon);
        if($linkbank_sale_type == "conv")
            $linkbankData[] = '<a  data-ajax-parent-class="text-center grey-scale" href="javascript:" data-url="'.get_site_url($curpage."?act=linkbank_details&LinkBankID=".$linkbank->id).'" data-toggle="DetailRow" id="" class="btn btn-rotator-detail"><i class="fa fa-caret-square-o-down"></i></a>';

        $linkbankData[] = '<a href="javascript: void(0);" data-clipboard-text="'.$linkbankurl.'" class="btn btn-sm btn-default copy" title="Copy '.$linkbankurl.'">Copy</a>';
        $linkbankData[] = '<a href="'.$namelinkedurl.'" class="pull-left qtip2" data-qtip-image="'.(PREVIEW_URL.$linkbankurl).'">'.$linkbank->LinkName.'</a>';
        $linkbankData[] = '<span>'.$VendorName.'</span>';
        //$linkbankData[] = '<span>'.$MasterCampaignName.'</span>';
        $linkbankData[] = '<span data-ajax-parent-class="text-center">'.get_formated_number($RawClicks).'</span>';
        $linkbankData[] = '<span data-ajax-parent-class="text-center">'.get_formated_number($UniqueClicks).'</span>';



        if($linkbank_sale_type == "")
            $linkbankData[] = '<span data-ajax-parent-class="text-center">'.$manualsalelink.'</span>';

        if($linkbank_sale_type == "conv") {
            $linkbankData[] = '<span data-ajax-parent-class="text-center">'.get_formated_number($conv_no).'</span>';
            $linkbankData[] = '<span data-ajax-parent-class="text-center">$'.get_formated_number(round($conv_amount, 2)).'</span>';
            $linkbankData[] = '<span data-ajax-parent-class="text-center">'.get_formated_number(round($conv_percent, 2)).'%</span>';
        }
        $linkbankData[] = '<span>'.$GroupName.'</span>';
        $linkbankData[] = '<span data-ajax-parent-class="text-center">'.$linkbank->CreatedDate.'</span>';

        $strForm = '<a data-ajax-parent-class="text-center grey-scale" href="javascript:" class="status_icon" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click" id=""><i class="fa '.$linkbankstatusicon.'"></i></a>
        <div class="qtipsubmenu grey-scale">
            <form class="form-inline status_form" role="form" method="get" action="'.get_site_url($curpage.(!empty($linkbankstype) && $linkbankstype != "all"?"/".$linkbankstype."/":"")).'">
                <div class="form-group select-xs">
                    <select name="status" id="LinkStatus" class="form-control select2">';

        $link_statuses = array("pending" => "Pending", "active" => "Active", "complete" => "Completed", "evergreen" => "Evergreen", "mylink" => "My Links");
        foreach($link_statuses as $key => $value)
            $strForm .= '<option value="'.$key.'" '.($linkbank->LinkStatus == $key?' selected = "selected" ':'').'>'.$value.'</option>';
        
        $strForm .= '</select>
                </div>
                <button type="submit" class="btn btn-xs btn-default">Change</button>
                <input type="hidden" name="act" value="changelinkstatus" />
                <input type="hidden" name="LinkBankID" value="'.$linkbank->id.'" />
            </form>
        </div>';
        $linkbankData[] = $strForm;
        $linkbankData[] = '<a data-ajax-parent-class="text-center" href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
        <div class="qtipsubmenu grey-scale">
            <ul class="list-unstyled">
                <li><a href="'.get_site_url("linkbank/?LinkBankID=".$linkbank->id).'" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i>Edit Link</a></li>
                <li><a href="'.$linkbankurl."/".'" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i>Direct Link</a></li>
                <li><a href="'.get_site_url($curpage."/?act=delete_link&LinkBankID=".$linkbank->id).'" class="btn btn-sm btn-links btn-delete" title="Delete Link"><i class="fa fa-trash"></i>Delete Link</a></li>
                <li><a href="'.get_site_url($curpage."/?act=reset_link&LinkBankID=".$linkbank->id).'" class="btn btn-sm btn-links btn-reset" title="Reset Statistics"><i class="fa fa-ban"></i>Reset Statistics</a></li>
                <li><a href="'.get_site_url($curpage."/?act=clone_link&LinkBankID=".$linkbank->id).'" class="btn btn-sm btn-links" title="Clone Link"><i class="fa fa-clone"></i>Clone Link</a></li>
                <li><a href="javascript: void(0);" data-clipboard-text="'.$linkbankurl.'" class="btn btn-sm btn-links copy" title="Copy '.$linkbankurl.'"><i class="fa fa-clipboard"></i>Copy Link</a></li>
                <li><a href="'.get_site_url($pageurl."/".$linkbank->id."/details").'" class="btn btn-sm btn-links" title="Statistics"><i class="fa fa-globe"></i>Statistics</a></li>
            </ul>
        </div>';

        $jsonData["data"][] = $linkbankData;
    }
    return $jsonData;
}
