<?php
$errors = array();
if($_POST && isset($_POST['apiact'])){
    $act =  $_POST['apiact'];
    
    if($act == "save_paidtraffic"){
        $admindomain = get_available_domain(0, false);
        $jsonData = array("result" => "", "msg" => "",  "PaidTrafficID" => "-1");
        $data = array(
           "userid"                                => $apiUser->id,
           "VendorID"                              => $_POST['VendorID'],
           "PaidTrafficName"                       => $_POST['PaidTrafficName'],
           "DestinationURL"                        => $_POST['DestinationURL'],
           "PaidTrafficStatus"                     => "active",
           "TrafficCost"                           => $_POST['TrafficCost'],
           "TrafficCostType"                       => "CPC",
           "NoOfClicks"                            => $_POST['NoOfClicks'],
           "VisibleLink"                           => $_POST['VisibleLink'],
           "UseAdminDomain"                        => 1,
           "DomainID"                              => $admindomain->id,
           "CloakURL"                              => 0,
           "GroupID"                               => !empty($_POST['GroupID'])?$_POST['GroupID']:"390",
           "StartDate"                             => !empty($_POST['StartDate'])?strtotime($_POST['StartDate']):"",
           "EndDate"                               => !empty($_POST['EndDate'])?strtotime($_POST['EndDate']):"",
           "PaidTrafficActive"                     => 0,
           "TrackingCodeForActions"                => "",
           "PixelID"                               => 0,
           "EasyLinkID"                            => 0,
           "UnitPriceSales"                        => 9.95,
           "TrackingCodeForSalesAndConversions"    => "",
           "AdditionalNotes"                       => "",
           "PageImage"                             => "",
           "PendingPageID"                         => 0,
           "CompletePageID"                        => 0,
           "MasterCampaignID"                      => 0,
           "ReferenceID"                           => $_GET["ref"],
           "DateAdded"                             => strtotime("now")
        );
            
        if(is_visible_link_valid($data['DomainID'], $data['VisibleLink'], $apiUser)){
            if($db->insert($db->paidtraffics, $data)){
                $jsonData["result"] = "successful";
                $jsonData["msg"] = "Paid Traffic insertion successful";
                $jsonData["PaidTrafficID"] = $db->insert_id;
            }else{
                $jsonData["result"] = "failed";
                $jsonData["msg"] = "Paid Traffic insertion failed";
            }
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Visible Link for selected Domain already exist";
        }

        echo json_encode($jsonData);
        die;
    }else if($act == "delete_paidtraffic"){
        delete_paid_traffic_stats(parseInt($_POST['PaidTrafficID']), $apiUser);
        if($db->delete($db->paidtraffics, array("id" => parseInt($_POST['PaidTrafficID'])))){
            $db->delete($db->paidtraffic_saleinfo, array("PaidTrafficID" => $PaidTrafficID));
            $jsonData["result"] = "successful";
            $jsonData["msg"] = "Paid Traffic Deleted Successfully";
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Paid Traffic Not Deleted";
        }
        echo json_encode($jsonData);
        die;
    }else if($act == "reset_paidtraffic"){
        delete_paid_traffic_stats(parseInt($_POST['PaidTrafficID']), $apiUser);
        $jsonData["result"] = "successful";
        $jsonData["msg"] = "Paid Traffic Stats Reset Successfully";
        echo json_encode($jsonData);
        die;
    }else if($act == "save_vendor"){
        $data = array(
            "userid"            => $apiUser->id,
            "VendorName"        => $_POST['VendorName'],
            "VendorType"        => "SoloProvider",
            "VendorPicture"     => "",
            "WebsiteUrl"        => $_POST['WebsiteUrl'],
            "SkypeName"         => $_POST['SkypeName'],
            "LinkedIn"          => $_POST['LinkedIn'],
            "FacebookID"        => $_POST['FacebookID'],
            "EmailAddress"      => $_POST['EmailAddress'],
            "AdditionalNotes"   => $_POST['AdditionalNotes'],
            "VendorTags"        => $_POST['VendorTags'],
            "ReferenceID"       => $_GET["ref"],
            "DateAdded"         => strtotime("now"),
        );

        if($db->insert($db->vendors, $data)){
            $jsonData["result"] = "successful";
            $jsonData["msg"] = "Paid Traffic Provider insertion successful";
            $jsonData["PaidTrafficProviderID"] = $db->insert_id;
            $jsonData["PaidTrafficProviderName"] = $data["VendorName"];
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Paid Traffic Provider insertion failed";
        }

        echo json_encode($jsonData);
        die;
    }else if($act == "get_apitoken"){
        if(empty($apiUser->apitoken)){
            $apitoken = md5(uniqid(microtime() . mt_rand(), true));
            while($db->get_row("select * from ".$db->users." where apitoken='".$apitoken."'"))
                $apitoken = md5(uniqid(microtime() . mt_rand(), true));

            $apiUser->apitoken = $apitoken;
            $db->update($db->users, array("apitoken" => $apitoken), array("id" => $apiUser->id));
        }
        $jsonData = array("result" => "successful", "apitoken" => $apiUser->apitoken);
        echo json_encode($jsonData);
        die;
    } else if($act == "save_group"){
        $data = array(
            "userid"            => $apiUser->id,
            "GroupName"         => $_POST['GroupName'],
            "GroupType"         => "PaidTraffic",
            "ReferenceID"       => $_GET["ref"],
            "DateAdded"         => strtotime("now"),
        );

        if($db->insert($db->groups, $data)){
            $jsonData["result"] = "successful";
            $jsonData["msg"] = "Group insertion successful";
            $jsonData["GroupID"] = $db->insert_id;
            $jsonData["GroupName"] = $data["GroupName"];
        }else{
            $jsonData["result"] = "failed";
            $jsonData["msg"] = "Group insertion failed";
        }

        echo json_encode($jsonData);
        die;
    } else if($act == "register_action_pixel_old") {
        $jsonData = array("PaidTrafficID" => 0, "PaidTrafficClickID" => 0, "PaidTrafficConversionID" => 0);
        $ip_address = $_POST["ip_address"];
        $ip_address2 = $_POST["ip_address2"];
        //$sql = "select ".$db->paidtraffic_clicks.".* from ".$db->paidtraffics." inner join ".$db->paidtraffic_clicks." on ".$db->paidtraffics.".id = ".$db->paidtraffic_clicks.".PaidTrafficID where ".$db->paidtraffics.".userid = ".$apiUser->id." and ".$db->paidtraffic_clicks.".ClickIp = '".$ip_address."' and ".$db->paidtraffics.".id not in (select PaidTrafficID from ".$db->paidtraffic_conversions." where ConversionIp = '".$ip_address."' and ConversionType = 'action') order by ".$db->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $sql = "select ".$db->paidtraffic_clicks.".* from ".$db->paidtraffics." inner join ".$db->paidtraffic_clicks." on ".$db->paidtraffics.".id = ".$db->paidtraffic_clicks.".PaidTrafficID where ".$db->paidtraffics.".userid = ".$apiUser->id." and ".$db->paidtraffic_clicks.".ClickIp in ('".$ip_address."', '".$ip_address2."') and ".$db->paidtraffics.".id not in (select PaidTrafficID from ".$db->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action') order by ".$db->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $paidtraffic_click = $db->get_row($sql);
        if($paidtraffic_click){
            $data = array(
                    "PaidTrafficID"     =>  $paidtraffic_click->PaidTrafficID,
                    "DateAdded"         =>  strtotime("now"),
                    "ConversionType"    =>  "action",
                    "UnitPrice"         =>  0,
                    "ConversionIp"      =>  $paidtraffic_click->ClickIp,
                    "CountryCode"       =>  $paidtraffic_click->CountryCode,
                    "BotName"           =>  $paidtraffic_click->BotName,
                    "BrowserName"       =>  $paidtraffic_click->BrowserName,
                    "BrowserVersion"    =>  $paidtraffic_click->BrowserVersion,
                    "Platform"          =>  $paidtraffic_click->Platform,
                    "PaidTrafficConversionPixelID"     =>  0,
                );
            $db->insert($db->paidtraffic_conversions, $data);

            $jsonData = array(
                "PaidTrafficID" => $paidtraffic_click->PaidTrafficID, 
                "PaidTrafficClickID" => $paidtraffic_click->id, 
                "PaidTrafficConversionID" => $db->insert_id
            );
        }
        echo json_encode($jsonData);
        die;
    } else if($act == "register_action_pixel") {
        $jsonData = array("PaidTrafficID" => 0, "PaidTrafficClickID" => 0, "PaidTrafficConversionID" => 0, "PaidTrafficURL" => "");
        $ip_address = $_POST["ip_address"];
        $ip_address2 = $_POST["ip_address2"];
        //$sql = "select ".$db->paidtraffic_clicks.".* from ".$db->paidtraffics." inner join ".$db->paidtraffic_clicks." on ".$db->paidtraffics.".id = ".$db->paidtraffic_clicks.".PaidTrafficID where ".$db->paidtraffics.".userid = ".$apiUser->id." and ".$db->paidtraffic_clicks.".ClickIp = '".$ip_address."' and ".$db->paidtraffics.".id not in (select PaidTrafficID from ".$db->paidtraffic_conversions." where ConversionIp = '".$ip_address."' and ConversionType = 'action') order by ".$db->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $sql = "select ".$db->paidtraffic_clicks.".* from ".$db->paidtraffics." inner join ".$db->paidtraffic_clicks." on ".$db->paidtraffics.".id = ".$db->paidtraffic_clicks.".PaidTrafficID where ".$db->paidtraffics.".userid = ".$apiUser->id." and ".$db->paidtraffic_clicks.".ClickIp in ('".$ip_address."', '".$ip_address2."') and ".$db->paidtraffics.".id not in (select PaidTrafficID from ".$db->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action' and ".$db->paidtraffic_clicks.".DateAdded < (select DateAdded from ".$db->paidtraffic_conversions." where ConversionIp in ('".$ip_address."', '".$ip_address2."') and ConversionType = 'action' and PaidTrafficID = ".$db->paidtraffic_clicks.".PaidTrafficID ORDER BY DateAdded desc LIMIT 0,1)) order by ".$db->paidtraffic_clicks.".DateAdded desc limit 0,1";
        $jsonData["sql"] = $sql;
        $paidtraffic_click = $db->get_row($sql);
        if($paidtraffic_click){
            $data = array(
                    "PaidTrafficID"     =>  $paidtraffic_click->PaidTrafficID,
                    "DateAdded"         =>  strtotime("now"),
                    "ConversionType"    =>  "action",
                    "UnitPrice"         =>  0,
                    "ConversionIp"      =>  $paidtraffic_click->ClickIp,
                    "CountryCode"       =>  $paidtraffic_click->CountryCode,
                    "BotName"           =>  $paidtraffic_click->BotName,
                    "BrowserName"       =>  $paidtraffic_click->BrowserName,
                    "BrowserVersion"    =>  $paidtraffic_click->BrowserVersion,
                    "Platform"          =>  $paidtraffic_click->Platform,
                    "PaidTrafficConversionPixelID"     =>  0,
                );
            $db->insert($db->paidtraffic_conversions, $data);

            $jsonData = array(
                "PaidTrafficID" => $paidtraffic_click->PaidTrafficID, 
                "PaidTrafficClickID" => $paidtraffic_click->id, 
                "PaidTrafficConversionID" => $db->insert_id,
                "PaidTrafficURL" => get_paidtrafficurl($paidtraffic_click->PaidTrafficID, "", $apiUser),
            );
        }
        echo json_encode($jsonData);
        die;
    }
}