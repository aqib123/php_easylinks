<?php
define('Main_DIR', rtrim($_SERVER["DOCUMENT_ROOT"], "/"));
include_once Main_DIR."/includes/functions.php";

$MailFooterText='Karthik Ramani & Chad Nicely';
$TransactionType = strtoupper($_POST['ctransaction']);
if ($TransactionType == 'SALE') {
    $CustomerName = $_POST['ccustname'];
    //$CustomerState = $_POST['ccustemail'];
    $CustomCountryCode = $_POST['ccustcc'];
    $CustomerEmailAddress = $_POST['ccustemail'];
    $CustomerProductID = $_POST['cproditem'];
    $CustomerProductTitle = $_POST['cprodtitle'];
    $CustomerProductType = $_POST['cprodtype'];
    $isAffiliateTransaction = $_POST['ctransaffiliate'];
    $TotalSaleAmount = $_POST['ctransamount'];
    $PaymentMethod = $_POST['ctranspaymentmethod'];
    $VendorID = $_POST['ctransvendor'];
    $PaymentID = $_POST['ctransreceipt'];
    $UpSellPaymentID = $_POST['cupsellreceipt'];
    $AffiliateID = $_POST['caffitid'];
    $VerificationKey = $_POST['cverify'];
    $TransactionTime = $_POST['ctranstime'];
    $PurchaseSource = 'jvzoo';

    if($CustomerProductID == "225777" || $CustomerProductID == "349943") {
        $LevelID=3;
        $PurchasedProductType="";
        $PurchasedProductInfo ="If you have not purchased the EasyLinks Pro Version<br/> you may do so by clicking on the link below.<br/><br />https://easylinks.ninja/pro/";
    }elseif($CustomerProductID == "227675" || $CustomerProductID == "349955") {
        $LevelID=4;
        $PurchasedProductType=" Lite";
        $PurchasedProductInfo ="If you have not purchased the EasyLinks Marketer Version<br/> you may do so by clicking on the link below.<br/><br />https://easylinks.ninja/marketer/";
    }elseif($CustomerProductID == "225778" || $CustomerProductID == "349961") {
        $LevelID=5;
        $PurchasedProductType=" Pro with Developer license";
        $PurchasedProductInfo ="If you have not purchased the EasyLinks Marketer Version<br/> you may do so by clicking on the link below.<br/><br />https://easylinks.ninja/marketer/";
    }elseif($CustomerProductID == "227679" || $CustomerProductID == "349959") {
        $LevelID=6;
        $PurchasedProductType=" Paid Traffic";
    }elseif($CustomerProductID == "225780" || $CustomerProductID == "349957") {
        $LevelID=7;
        $PurchasedProductType=" Marketer";
    }

    $isNewUser = false;
    $isExistingUser = false;
    $UserID = 0;
    $PaymentLog = DB()->get_row("select ".DB()->payment_log->Asterisk." from ".DB()->payment_log." where ".DB()->payment_log->custemail."'".$CustomerEmailAddress."' and ".DB()->payment_log->proditem."'".$CustomerProductID."' and ".DB()->payment_log->transid."'".$PaymentID."'");
    if($PaymentLog){
        DB()->update(DB()->payment_log, [
            "status" => $TransactionType,
            "amount" => $TotalSaleAmount,
            "uptranid" => $UpSellPaymentID
        ], ["id" => $PaymentLog->id]);
        //$UserID = $PaymentLog->UserID;
    } else {
        DB()->insert(
            DB()->payment_log, [
                "CustomerName"  =>  $CustomerName,
                "CustomerEmailAddress"          =>  $CustomerEmailAddress,
                "CustomerProductID"             =>  $CustomerProductID,
                "CustomCountryCode"             =>  $CustomCountryCode,
                "CustomerProductType"           =>  $CustomerProductType,
                "TransactionType"               =>  $TransactionType,
                "isAffiliateTransaction"        =>  $isAffiliateTransaction,
                "TotalSaleAmount"               =>  $TotalSaleAmount,
                "UpSaleAmount"                  =>  '',
                "PaymentMethod"                 =>  $PaymentMethod,
                "VendorID"                      =>  $VendorID,
                "PaymentID"                     =>  $PaymentID,
                "UpSellPaymentID"               =>  $UpSellPaymentID,
                "AffiliateID"                   =>  $AffiliateID,
                "VerificationKey"               =>  $VerificationKey,
                "TransactionTime"               =>  $TransactionTime,
                "UserID"                        =>  0,
                "username"                      =>  "",
                "password"                      =>  "",
                "LevelID"                       =>  $LevelID,
                "LevelAccessKey"                =>  $LevelAccessKey,
                "active_status"                 =>  1,
                "sent_license_key"              =>  1,
                "PaymentData"                   =>  SYS()->json_encode($_POST)
            ]
        );
        $PaymentID = DB()->insert_id;
        $PaymentData = [];
        $Level = DBLevels()->get(DBLevels()->id." = ".$LevelID);
        $User = DBUsers()->get("md5(".DB()->users->user_email.") = md5('".$CustomerEmailAddress."')");
        if(!$User){
            $isNewUser = true;
            $UserData = elUsers()->add([
                "username"          =>  strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $CustomerName)),
                "email"             =>  $CustomerEmailAddress,
                "LevelAccessKey"    =>  $Level->LevelAccessKey
            ]);

            if($UserData["added"]){
                $UserID = $UserData["UserID"];
                $Username = $UserData["user_login"];
                $Password = $UserData["user_pass"];
            }
        } else {
            $isExistingUser = true;
            $UserID = $User->id;
            $Username = $User->user_login;
            $Password = $User->user_pass;

            $UserLevel = DBUserLevels()->get_row($LevelID, $UserID);
            if(!$UserLevel){
                DB()->insert(
                    DB()->user_levels, [
                        "UserID"            => $UserID,
                        "LevelID"           => $LevelID,
                        "UserLevelEnabled"  => 1
                    ]
                );
            } else {
                DB()->update(
                    DB()->user_levels, [
                        "UserID"            => $UserID,
                        "LevelID"           => $LevelID,
                        "UserLevelEnabled"  => 1
                    ],[
                        "id"                => $UserLevel->id
                    ]
                );
            }
        }

        if($UserID > 0){
            $PaymentData["UserID"] = $UserID;
            $PaymentData["username"] = $Username;
            $PaymentData["password"] = $Password;
            DB()->update(DB()->payment_log, $PaymentData, ["id" => $PaymentID]);
        }
    }

    $EmailSubject = "";
    //if(!$isExistingUser){
    if($UserID > 0) {
        $EmailSubject = 'Here is your Product login info for '.SITE_TITLE.$PurchasedProductType;
        $EmailMessage ='Hi there,<br/><br/>
            Congratulations on your purchase of '.SITE_TITLE.$PurchasedProductType.'.<br/>Please find below your login credentials.<br/><br/>
            https://easylinks.io/login/ <br/><br/>
           User Name: '.$Username.'<br/>
           Password: '.(!$isExistingUser ? $Password : "Use Existing Account Password").'<br/><br/>
           The tool is built on an intuitive design<br/>
           platform making it very user friendly.<br/><br/>
           If you need any assistance, you can refer<br/>
           to the video tutorials in your members area.<br/><br/>
           If you have any queries, please reach out<br/>
           to the support team and we will be more<br/>
           than happy to assist you.<br/><br/>
           https://chadnicely.freshdesk.com <br/><br/>
           '.$PurchasedProductInfo.'<br/><br/>
           Looking forward to serving you.<br/><br/>
           Cheers,<br/>
           '.$MailFooterText.'';
    } else if($isNewUser && $LevelID == 5) {
        $EmailSubject = 'Thank you for purchasing '.SITE_TITLE.$PurchasedProductType;
        $EmailMessage ='Hi there,<br/><br/>
            Congratulations on your purchase of EasyLinks Pro with Developer license.<br/><br/>
            From our records, we understand that you have purchased the pro version without<br/>
            purchasing EasyLinks - the basic version of the tool.<br/><br/>
            Contact our support desk along with your JVZoo Purchase details<br/>
            and we will be glad to assist you.<br/><br/>
            Here is our support desk details:<br/><br/>
            https://chadnicely.freshdesk.com <br/><br/>
            Looking forward to serving you,<br/><br/>
            Cheers,<br/>
            '.$MailFooterText.'';
    }
    //} else {
    //    $EmailSubject = 'Upgrade information for EasyLinks';
    //    $EmailMessage ='
    //        Hi there,<br/><br/>
    //        Congratulations upgrading your EasyLinks account.<br/><br/>
    //        Please submit a support ticket to https://chadnicely.freshdesk.com <br/>
    //        with a copy of the receipt of your purchase, so we can get your<br/>
    //        account upgraded for you.<br/><br/>
    //        If you need any assistance, you can refer to the video tutorials<br/>
    //        in your members area.<br/><br/>
    //        If you have any queries, please reach out to the support team<br/>
    //        and we will be more than happy to assist you.<br/><br/>
    //        https://chadnicely.freshdesk.com <br/><br/>
    //        Looking forward to serving you.<br/><br/>
    //        Cheers,<br/><br/>
    //        Karthik Ramani & Chad Nicely<br/>
    //    ';
    //}
    if(!empty($EmailSubject)){
        //SYS()->send_mail($CustomerEmailAddress, $EmailSubject, $EmailMessage);
        SYS()->send_message($CustomerEmailAddress, $EmailSubject, $EmailMessage);
    }
} elseif ($TransactionType == 'RFND') {
    if (!empty($_POST['ctransreceipt'])) {
        //Please deactivate the user access
    }
}