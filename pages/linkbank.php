<?php
$pagetitle = "Link Bank";
$pageurl = "linkbank";
$modulename = "linkbank";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
$AvailableDomainID = 0;
if($_GET && isset($_GET['LinkBankID']) && !isset($errors["msg"])){
    $LinkBankID = parseInt($_GET['LinkBankID']);
    $sql = "select * from ".DB()->linkbanks." where userid = ".$LoggedInUser->id." and id=".$LinkBankID;
    $linkbank = DB()->get_row($sql);
    if($linkbank){
        $isUpdateForm = true;

        if($linkbank->UseAdminDomain == 1)
            $AvailableDomainID = $linkbank->DomainID;
    }
}
$admindomain = get_available_domain($AvailableDomainID);

//echo $linkbank->EndDate;
//echo date("m/d/Y", $linkbank->EndDate);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $pagetitle;?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("linkbank")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank/stats")?>" role="button">Linkbank Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-linkbank-conv" href="<?php site_url("linkbank/stats/conv")?>" role="button">Linkbank Conversions Statistics</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2><?php echo $isUpdateForm?"Edit":"Create";?> Link Bank</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("linkbank").(isset($_GET['LinkBankID'])?"?LinkBankID=".$_GET['LinkBankID']:"")?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="VendorID" class="col-sm-4 col-xs-12 control-label">Choose Vendor</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <div class="input-group right-addon">
                                                    <select name="VendorID" id="VendorID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Vendor</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->vendors." where VendorType='Affiliate' and userid = ".$LoggedInUser->id." order by VendorName";
                                                        $vendors = DB()->get_results($sql);
                                                        foreach($vendors as $vendor){
                                                        ?>
                                                        <option value="<?php echo $vendor->id;?>" <?php if(($isUpdateForm && $linkbank->VendorID == $vendor->id) || (isset($_POST['VendorID']) && $_POST['VendorID'] == $vendor->id)) echo ' selected = "selected" ';?>><?php echo $vendor->VendorName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("vendor/?js=true&VendorType=Affiliate")?>" data-widget="ShowLinkModel" data-related="#VendorID" data-method="post" data-page-title="Vendor" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="LinkName" class="col-sm-4 control-label">Link Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $linkbank->LinkName; else if(isset($_POST['LinkName'])) echo $_POST['LinkName'];?>" class="form-control " id="LinkName" name="LinkName" placeholder="Link Name" data-remote="<?php site_url("linkbank/?act=check_link_name&type=linkbank&id=".(isset($_GET['LinkBankID'])?$_GET['LinkBankID']:"0"))?>" data-remote-error="Link Name already exist" <?php echo NAME_PATTERN;?> required="required" />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="DestinationURL" class="col-sm-4 control-label">Destination Link</label>
                                            <div class="col-sm-8">
                                                <input type="url" value="<?php if($isUpdateForm) echo $linkbank->DestinationURL; else if(isset($_POST['DestinationURL'])) echo $_POST['DestinationURL'];?>" class="form-control " id="DestinationURL" name="DestinationURL" placeholder="Destination Link" required="required" <?php echo URL_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="LinkStatus" class="col-sm-4 control-label">Choose Status</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group" style="width: 100%">
                                                    <select name="LinkStatus" id="LinkStatus" class="form-control select2" style="width: 100%" data-select="select">
                                                        <?php
                                                        $link_statuses = array("pending" => "Pending", "active" => "Active", "complete" => "Completed", "evergreen" => "Evergreen", "mylink" => "My Links");
                                                        foreach($link_statuses as $key => $value){
                                                        ?>
                                                        <option value="<?php echo $key;?>" <?php if(($isUpdateForm && $linkbank->LinkStatus == $key) || (isset($_POST['LinkStatus']) && $_POST['LinkStatus'] == $key) || (!$isUpdateForm && !isset($_POST['LinkStatus']) && $key == "active")) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="DomainID" class="col-sm-4 col-xs-12 control-label">Choose Domain</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <?php
                                                $checked = "";
                                                $AdminDomainID = "";
                                                $dropdowndisable = "";
                                                if(($isUpdateForm && $linkbank->UseAdminDomain == 1) || (isset($_POST['UseAdminDomain']))){
                                                    $checked = ' checked = "checked" ';
                                                    $AdminDomainID = $linkbank->DomainID;
                                                    $dropdowndisable = ' disabled = "disabled" ';
                                                }else{
                                                    $dropdowndisable = ' data-select="select" ';
                                                }

                                                ?>
                                                <div class="input-group right-addon">
                                                    <select name="DomainID" id="DomainID" class="form-control select2" style="width: 99%" <?php echo $dropdowndisable;?>>
                                                        <option value="">Select Domain</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->domains." where userid = ".$LoggedInUser->id." and DomainType='userdomain' order by DomainName";
                                                        $domains = DB()->get_results($sql);
                                                        $domainurls = array();
                                                        foreach($domains as $domain){
                                                            $domainurls[] = '"'.$domain->id.'": {name:"'.$domain->DomainName.'", forward:"'.$domain->DomainForward.'", url:"'.$domain->DomainUrl.'", type:"'.$domain->DomainType.'"}';
                                                        ?>
                                                        <option value="<?php echo $domain->id;?>" <?php if(($isUpdateForm && $linkbank->DomainID == $domain->id) || (isset($_POST['DomainID']) && $_POST['DomainID'] == $domain->id)) echo ' selected = "selected" ';?>><?php echo $domain->DomainName;?></option>
                                                        <?php
                                                        }
                                                        $str_domains = 'var domains = {'.implode(',', $domainurls).'};';
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("domain/?js=true&DomainType=userdomain")?>" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="User Domain" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="hidden" name="AdminDomainID" id="AdminDomainID" value="<?php echo $AdminDomainID;?>" />
                                                    <input type="checkbox" name="UseAdminDomain" id="UseAdminDomain" class="blue " value="1" <?php echo $checked;?> />&nbsp;
                                                    <span>Use Admin Domain</span>&nbsp;&nbsp;
                                                </div>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="checkbox" name="CloakURL" id="CloakURL" class="blue " value="1" <?php if(($isUpdateForm && $linkbank->CloakURL == 1) || (isset($_POST['CloakURL']))) echo "checked='checked'";?> />&nbsp;
                                                    <span>Cloak URL</span>&nbsp;&nbsp;
                                                </div>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="checkbox" name="TrackEPC" id="TrackEPC" class="blue " value="1" <?php if(($isUpdateForm && $linkbank->TrackEPC == 1) || (isset($_POST['TrackEPC']))) echo "checked='checked'";?> />&nbsp;
                                                    <span>Track EPC</span>&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="VisibleLink" class="col-sm-4 control-label">Visible Link</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $linkbank->VisibleLink; else if(isset($_POST['VisibleLink'])) echo $_POST['VisibleLink'];?>" class="form-control " id="VisibleLink" name="VisibleLink" placeholder="Visible Link" data-remote="<?php site_url("linkbank/")?>" data-remote-values="getVisibleLinkValues()" data-remote-error="Visible Link for selected Domain already exist" required="required" <?php echo VISIBLELINK_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id="linkurl"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-4 control-label">Choose Group</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="GroupID" id="GroupID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Group</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->groups." where GroupType='LinkBank' and userid = ".$LoggedInUser->id." order by GroupName";
                                                        $groups = DB()->get_results($sql);
                                                        foreach($groups as $group){
                                                        ?>
                                                        <option value="<?php echo $group->id;?>" <?php if(($isUpdateForm && $linkbank->GroupID == $group->id) || (isset($_POST['GroupID']) && $_POST['GroupID'] == $group->id)) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=LinkBank")?>" data-widget="ShowLinkModel" data-related="#GroupID" data-method="post" data-page-title="Group" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div class="well well-dotted linkbank-extra-options">
                                                    <h3 style="margin-top: 0;">Extra Options</h3>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#Retargeting" aria-expanded="false" aria-controls="Retargeting">
                                                        Retargeting Pixel
                                                    </button>
                                                    <?php if(elUsers()->is_module_enabled("linkbank-conv")) {?>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#ConversionPixel" aria-expanded="false" aria-controls="ConversionPixel">
                                                        Conversion Pixel
                                                    </button>
                                                    <?php } ?>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#Dates" aria-expanded="false" aria-controls="Dates">
                                                        Dates
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#RedirectPages" aria-expanded="false" aria-controls="RedirectPages">
                                                        Holding Pages
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#Notes" aria-expanded="false" aria-controls="Notes">
                                                        Notes
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#PageImageBox" aria-expanded="false" aria-controls="PageImageBox">
                                                        Page Image
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#AffiliatePlatform" aria-expanded="false" aria-controls="AffiliatePlatform">
                                                        Link Platform
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#MasterCampaign" aria-expanded="false" aria-controls="MasterCampaign">
                                                        Master Campaign
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="Retargeting">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Retargeting Pixel</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Pixel</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="PixelID" id="PixelID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Pixel</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->pixels." where userid = ".$LoggedInUser->id." order by PixelName";
                                                        $pixels = DB()->get_results($sql);
                                                        foreach($pixels as $pixel){
                                                        ?>
                                                        <option value="<?php echo $pixel->id;?>" <?php if(($isUpdateForm && $linkbank->PixelID == $pixel->id) || (isset($_POST['PixelID']) && $_POST['PixelID'] == $pixel->id)) echo ' selected = "selected" ';?>><?php echo $pixel->PixelName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("pixel/?js=true")?>" data-widget="ShowLinkModel" data-related="#PixelID" data-method="post" data-page-title="Retargeting Pixel" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if(elUsers()->is_module_enabled("linkbank-conv")) {?>
                                <div class="box box-gray- box-solid- collapse box-default" id="ConversionPixel">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Conversion Pixel</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <?php
                                        if($isUpdateForm){
                                            $conversion_pixels = DB()->get_results("select * from ".DB()->linkbank_conversion_pixels." where userid=".$LoggedInUser->id." and LinkBankID=".$LinkBankID);
                                            foreach ($conversion_pixels as $conversion_pixel){
                                            	echo get_linkbank_conversion_pixel($conversion_pixel);
                                            }
                                        } 
                                        ?>
                                        <a href="javascript:" class="btn btn-sm btn-link pull-right btn-add-conversion-pixel">Add another conversion pixel</a><br class="clearfix" />
                                    </div>
                                </div>
                                <?php }?>

                                <div class="box box-gray- box-solid- collapse box-default" id="Dates">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Dates</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group left-addon">
                                            <label for="StartDate" class="col-sm-4 control-label">Start Date</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" data-linked="#StartDate" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $linkbank->StartDate); else if(isset($_POST['StartDate'])) echo $_POST['StartDate'];?>" class="form-control  input-singledate-time with-left-addon" id="StartDateInput" name="StartDateInput"/>
                                                    <input type="hidden" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $linkbank->StartDate); else if(isset($_POST['StartDate'])) echo $_POST['StartDate'];?>" id="StartDate" name="StartDate" />
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="EndDate" class="col-sm-4 control-label">End Date</label>
                                            <div class="col-sm-8">
                                                <div class="input-group left-addon">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" data-linked="#EndDate" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $linkbank->EndDate); else if(isset($_POST['EndDate'])) echo $_POST['EndDate'];?>" class="form-control  input-singledate-time with-left-addon" id="EndDateInput" name="EndDateInput" />
                                                    <input type="hidden" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $linkbank->EndDate); else if(isset($_POST['EndDate'])) echo $_POST['EndDate'];?>" id="EndDate" name="EndDate" />
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="RedirectPages">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Holding Pages</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PendingPageID" class="col-sm-4 control-label">Choose Pending Page</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="PendingPageID" id="PendingPageID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Pending Page</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->redirect_links." where RedirectLinkType='PendingPage' and userid = ".$LoggedInUser->id." order by RedirectLinkName";
                                                        $redirect_links = DB()->get_results($sql);
                                                        foreach($redirect_links as $redirect_link){
                                                        ?>
                                                        <option value="<?php echo $redirect_link->id;?>" <?php if(($isUpdateForm && $linkbank->PendingPageID == $redirect_link->id) || (isset($_POST['PendingPageID']) && $_POST['PendingPageID'] == $redirect_link->id)) echo ' selected = "selected" ';?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("holding-page/?js=true&RedirectLinkType=PendingPage")?>" data-widget="ShowLinkModel" data-related="#PendingPageID" data-method="post" data-page-title="Pending Page" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="CompletePageID" class="col-sm-4 control-label">Choose Complete Page</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="CompletePageID" id="CompletePageID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Complete Page</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->redirect_links." where RedirectLinkType='CompletePage' and userid = ".$LoggedInUser->id." order by RedirectLinkName";
                                                        $redirect_links = DB()->get_results($sql);
                                                        foreach($redirect_links as $redirect_link){
                                                        ?>
                                                        <option value="<?php echo $redirect_link->id;?>" <?php if(($isUpdateForm && $linkbank->CompletePageID == $redirect_link->id) || (isset($_POST['CompletePageID']) && $_POST['CompletePageID'] == $redirect_link->id)) echo ' selected = "selected" ';?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("holding-page/?js=true&RedirectLinkType=CompletePage")?>" data-widget="ShowLinkModel" data-related="#CompletePageID" data-method="post" data-page-title="Complete Page" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="Notes">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Notes</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group">
                                            <label for="AdditionalNotes" class="col-sm-4 control-label">Any Additional Notes</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control " id="AdditionalNotes" name="AdditionalNotes" placeholder="Any Additional Notes" cols="3"><?php if($isUpdateForm) echo $linkbank->AdditionalNotes; else if(isset($_POST['AdditionalNotes'])) echo $_POST['AdditionalNotes'];?></textarea>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="PageImageBox">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Page Image</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div id="PageImageButton" class="" data-usehtml="true" data-input-name="PageImage" data-existing-image="<?php if($isUpdateForm) echo $linkbank->PageImage; else if(isset($_POST['PageImage'])) echo $_POST['PageImage'];?>">
                                                    <div class="btn btn-bordered btn-gray">
                                                        <i class="fa fa-upload"></i>&nbsp;
                                                        <span>Upload Page Image</span>
                                                    </div>
                                                    <br class="clearfix" />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="AffiliatePlatform">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Link Platform</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Platform</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="AffiliatePlatformID" id="AffiliatePlatformID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Platform</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->groups." where GroupType='AffiliatePlatform' and userid = ".$LoggedInUser->id." order by GroupName";
                                                        $platforms = DB()->get_results($sql);
                                                        foreach($platforms as $platform){
                                                        ?>
                                                        <option value="<?php echo $platform->id;?>" <?php if(($isUpdateForm && $linkbank->AffiliatePlatformID == $platform->id) || (isset($_POST['AffiliatePlatformID']) && $_POST['AffiliatePlatformID'] == $platform->id)) echo ' selected = "selected" ';?>><?php echo $platform->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=AffiliatePlatform")?>" data-widget="ShowLinkModel" data-related="#AffiliatePlatformID" data-method="post" data-page-title="Link Platform" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="MasterCampaign">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Master Campaign</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Master Campaign</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="MasterCampaignID" id="MasterCampaignID" class="form-control select2" style="width: 99%">
                                                        <option value="0">Select Master Campaign</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->master_campaigns." where MasterCampaignStatus='active' and userid = ".$LoggedInUser->id." order by MasterCampaignName";
                                                        $MasterCampaigns = DB()->get_results($sql);
                                                        foreach($MasterCampaigns as $MasterCampaign){
                                                        ?>
                                                        <option value="<?php echo $MasterCampaign->id;?>" <?php if(($isUpdateForm && $linkbank->MasterCampaignID == $MasterCampaign->id) || (isset($_POST['MasterCampaignID']) && $_POST['MasterCampaignID'] == $MasterCampaign->id)) echo ' selected = "selected" ';?>><?php echo $MasterCampaign->MasterCampaignName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("master-campaign-add/?js=true")?>" data-widget="ShowLinkModel" data-related="#MasterCampaignID" data-method="post" data-page-title="Master Campaign" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("linkbank/stats")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_link" />
                            </form>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking link? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("help-content/?id=10")?>" data-widget="ShowLinkModel" data-no-footer="true" data-model-width="700" data-page-title="" style="top: 0;">How to create a new tracking link</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking link articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var CurrentVisibleLink = "<?php echo $isUpdateForm?$linkbank->VisibleLink:"";?>";
    var CurrentID = "<?php echo isset($_GET["LinkBankID"])?$_GET["LinkBankID"]:"0";?>";
    var PixelBox = '<?php echo str_replace(array("\r", "\n", "\\", "'"), array("", "", "\\\\", "\\'"), get_linkbank_conversion_pixel(null, 'newpixel-{new_conversion_pixel_index}'));?>';
    var PixelIndex = 0;
</script>

<style type="text/css" id="picture_basic_dependence_css"></style>

<!-- LinkBank -->
<script src="<?php site_url("js/linkbank.js");?>" type="text/javascript"></script>

<?php
echo '<script type="text/javascript">'.
$str_domains.
' var userid = "'.$LoggedInUser->id.'";'.
' var admindomain = '.$admindomain.';'.
'</script>';
include_once "member_footer.php";
?>