<?php
$pagetitle = "Groups - Domains";
$pageurl = "group/domains";
$modulename = "dashboard";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/linkbank/")?>" role="button">Link Bank</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/paidtraffics/")?>" role="button">Paid Traffic</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/email/")?>" role="button">Email Campaigns</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/rotator/")?>" role="button">Rotator Groups</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/link-sequence/")?>" role="button">Link Sequence</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/pixels/")?>" role="button">Retargeting Pixels</a>
                <a class="btn btn-flat btn-gray" href="<?php site_url("group/domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/holding-pages/")?>" role="button">Holding Pages</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Domains</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">User Domain</label>
                                            <div class="col-xs-12 ">
                                                <select name="DomainID" id="DomainID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $slugs = array();
                                                    $sql = "select * from ".DB()->domains." where userid = ".$LoggedInUser->id." and DomainType='userdomain' order by DomainName";
                                                    $domains = DB()->get_results($sql);
                                                    foreach($domains as $domain){
                                                        $slugs["id_".$domain->id] = $domain->DomainSlug;
                                                    ?>
                                                    <option value="<?php echo $domain->id;?>"><?php echo $domain->DomainName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 domain-buttons" style="margin-top: 12px">
                                                <a href="<?php site_url("domain/?js=true&DomainType=userdomain&nodefault=true")?>" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="User Domain" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("domain/?js=true&DomainType=userdomain&nodefault=true&DomainID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="User Domain" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("domain/?js=true&DomainType=userdomain&nodefault=true")?>" data-values="act=delete_domain&DomainID=" data-append="true" data-widget="RemoveOption" data-related="#DomainID" data-method="post" data-page-title="User Domain" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                                <a href="<?php site_url("generate-custom-files?simple=true")?>" class="btn btn-link btn-bordered btn-sm btn-generate">Generate Files</a>
                                                <a href="<?php site_url("generate-custom-files?wp=true")?>" class="btn btn-link btn-bordered btn-sm btn-generate">Download WP Plugin</a>
                                                <a href="javascript:" class="btn btn-link btn-bordered btn-sm btn-copy">Copy CName URL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    <?php echo 'var slugs = '.json_encode($slugs).';'; ?>
    $(document).ready(function () {
        $(".btn-generate").on("click", function (e) {
            e.preventDefault();
            var $this = $(this);
            var DomainID = $("#DomainID").val();
            var url = $this.attr("href") + "&DomainID=" + DomainID;
            //alert(url);
            window.location.href = url;
        });

        $('.btn-copy').each(function () {
            var $this = $(this);
            CopyCNameURL($this);
        });

        function CopyCNameURL($ele) {
            var client = new ClipboardJS($ele[0], {
                text: function (trigger) {
                    var DomainID = $("#DomainID").val();
                    var CNameURL = "";
                    if (DomainID != null) {
                        CNameURL = username + "-" + slugs["id_" + DomainID] + ".easylinks.online";
                    }

                    return CNameURL;
                }
            });
            client.on('success', function (e) {
                alert("CName URL Copied to clipboard!");
            });
            //client.on('error', function (e) {
            //    alert("CName URL Copying failed!");
            //});
        }
    });
</script>
<?php
include_once "member_footer.php";
?>