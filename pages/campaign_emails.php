<?php
if($_GET['emailstype'] == "")
    $emailstype = "all";
else
    $emailstype = $_GET['emailstype'];

if(!isset($_GET['emaildash'])){
    $pagetitle = "Email Series";//.ucfirst($emailstype);
    $emaildash = false;
    $curpage = "campaign/emails";
}else{
    $pagetitle = "Email Series - ".ucfirst($emailstype);
    $emaildash = true;
    $curpage = "campaign/emails";
}
$pageurl = "campaign/database";
$modulename = "email-campaigns";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

$isUpdateForm = false;
if($_GET && !empty($_GET['CampaignID'])){
    $CampaignID = parseInt($_GET['CampaignID']);
    $sql = "select * from ".DB()->campaigns." where id=".$CampaignID." and userid=".$LoggedInUser->id;
    $campaign = DB()->get_row($sql);
    if($campaign){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$campaign->GroupID);
$curpage .= "/".$CampaignID;
?>
<script>var emails = {};</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li>
                <a href="<?php site_url("dashboard")?>">
                    <i class="fa fa-home"></i>Home
                </a>
            </li>
            <li>
                <a href="<?php site_url($MenuItem->MenuItemURL)?>">
                    <i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?>
                </a>
            </li>
            <li>
                <a href="<?php site_url("campaign/".$campaign->id."/")?>">
                    <i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $campaign->CampaignName?>
                </a>
            </li>
            <li class="active">
                <?php echo $pagetitle;?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-butttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign/single")?>" role="button">Create Single Email</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign")?>" role="button">Create Email Series</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign/stats")?>" role="button">Campaign Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Emails">
                    <i class="fa fa-file"></i>
                </a>
                <a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Emails">
                    <i class="fa fa-hourglass-o"></i>
                </a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Emails">
                    <i class="fa fa-rocket"></i>
                </a>
                <a href="<?php site_url($curpage."/copied");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Copied Emails">
                    <i class="fa fa-clone"></i>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>
                    <?php echo $pagetitle;?>
                </h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("campaign/".$campaign->id)?>">
                        <?php echo $campaign->CampaignName?>
                    </a>
                    <br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small">
                                <?php echo date("m/d/Y", $campaign->DateAdded)?>
                            </a>
                            <br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small">
                                <?php echo $GroupName?>
                            </a>
                            <br />
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        if($isUpdateForm){
		?>
        <div class="box box-solid box-gray">
            <div class="box-header with-border">
                <h3 class="box-title">Create Email Series</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <?php
            $sql = "select * from ".DB()->campaign_emails." where userid=".$LoggedInUser->id." and isEnabled=1 and CampaignID=".$CampaignID;
            $emails = DB()->get_results($sql);
            foreach($emails as $index => $email){
                $emailbox = get_email_box($CampaignID, $email, $index, $emailstype);
                if($emailbox != ""){
				?>
                <div class="col-lg-4 col-md-6 col-sm-12 emailbox" id="email-box-<?php echo $index;?>" data-box-index="<?php echo $index;?>">
                    <?php echo $emailbox; ?>
                </div>
                <?php
                }
            }
				?>
                <div class="col-lg-4 col-md-6 col-sm-12" id="email-box-create-new">
                    <div class="small-box create-new-box">
                        <a href="javascript:" class="create-new-emailbox">
                            <h2>
                                <i class="fa fa-plus"></i>
                                <br />
                                <span>Create New</span>
                            </h2>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
		?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
var strinsert_strings = [
    { 'name': 'Insert Link' },
    <?php
    $userid = $LoggedInUser->id;
    $sql = "select * from ".DB()->linkbanks." where (LinkStatus = 'active' or LinkStatus = 'evergreen' or LinkStatus = 'mylink') and userid=".$userid." order by LinkName asc";
    $linkbanks = DB()->get_results($sql);
    foreach($linkbanks as $linkbank){
        $linkbankurl = $linkbank->id;
	?>
    { 'name': "<?php echo $linkbank->LinkName?>", 'value': '<?php echo $linkbankurl?>' },
<?php
    }
?>
];
var strinsert_button_label = strinsert_button_title = strinsert_button_voice = 'CTA Links';
var strinsert_toolbar = 'easylinks';
var emptybox = '<div class="col-lg-4 col-md-6 col-sm-12 emailbox" id="email-box-{index}" data-box-index="{index}"><?php echo str_replace(array("\r", "\n"), "", get_email_box($CampaignID, null, "{index}", $emailstype));?></div>';
</script>
<script src="<?php site_url("js/campaign_emails.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/ckeditor/ckeditor.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/ckeditor/adapters/jquery.js");?>" type="text/javascript"></script>
<?php
include_once "member_footer.php";
?>