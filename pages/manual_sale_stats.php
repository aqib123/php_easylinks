<?php
if($_GET['manualsale_type'] == "") 
    $manualsalestype = "all";
else
    $manualsalestype = $_GET['manualsale_type'];

$pagetitle = "Manual Sales Statistics";
$curpage = "manual-sale/stats";
$pageurl = "manual-sale/stats";
$modulename = "manual-sale";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Balances"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($curpage."/open-balance");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Open Balances"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($curpage."/zero-balance");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Zero Balances"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/no-partners");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="No Partners"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Manual Sales</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped data-table responsive- nowrap" data-nobuttons="true" style="width: 100%;" data-nosort-columns="0" data-default-sort-column="1">
                    <thead>
                        <tr>
                            <th class="text-center"><span></span></th>
                            <th><span>Campaign Name</span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Campaign Type</span></th>
                            <th class="text-center"><span>Income</span></th>
                            <th class="text-center"><span>Other Income</span></th>
                            <th class="text-center"><span>Expenses</span></th>
                            <th class="text-center"><span>Clicks</span></th>
                            <th class="text-center"><span>EPC</span></th>
                            <th class="text-center"><span>Open Amount</span></th>
                            <th class="text-center"><span>Net Profit</span></th>
                            <th><span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $userid = $LoggedInUser->id;
                        $sql = "select * from ".DB()->manualsales." where userid=".$userid;

                        $ManualSales = DB()->get_results($sql);
                        foreach($ManualSales as $ManualSale){
                            if($ManualSale->LinkBankID == 0){
                                $ManualSaleType = "Master Campaign";
                                $mastercampaign = DB()->get_row("select * from ".DB()->master_campaigns." where userid=".$LoggedInUser->id." and id=".$ManualSale->MasterCampaignID);
                                $campaignname = $mastercampaign->MasterCampaignName;
                                $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$linkbank->id);
                                $MasterCampaignDetails = get_master_campaign_details($ManualSale->MasterCampaignID);
                                $TotalClicks = $MasterCampaignDetails["TotalClicks"];
                            } else {
                                $ManualSaleType = "LinkBank";
                                $linkbank = DB()->get_row("select * from ".DB()->linkbanks." where userid=".$LoggedInUser->id." and id=".$ManualSale->LinkBankID);
                                $campaignname = $linkbank->LinkName;
                                $campaignurl = get_linkbankurl($linkbank);
                                $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$linkbank->id);
                                $TotalClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$ManualSale->LinkBankID) * 1;
                            }

                            if(!is_percentage_valid($ManualSale)){
                                $TotalIncome = 0;
                                $TotalOtherIncome = 0;
                                $TotalExpense = 0;
                            }else{
                                $TotalIncome = DB()->get_var("select sum(".DB()->manualsale_incomes.".IncomeAmount) from ".DB()->manualsale_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
                                $TotalOtherIncome = DB()->get_var("select sum(".DB()->manualsale_other_incomes.".OtherIncomeAmount) from ".DB()->manualsale_other_incomes." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_other_incomes.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
                                $TotalExpense = DB()->get_var("select sum(".DB()->manualsale_expenses.".ExpenseAmount) from ".DB()->manualsale_expenses." inner join ".DB()->manualsale_split_partners." on ".DB()->manualsale_expenses.".ManualSaleSplitPartnerID = ".DB()->manualsale_split_partners.".id where ManualSaleID =".$ManualSale->id) * 1;
                            }

                            $EPC = $TotalIncome == 0?0:$TotalClicks/$TotalIncome;
                            $TotalNetProfit = ($TotalIncome + $TotalOtherIncome) - $TotalExpense;

                            $OpenBalance = 0;
                            $UserResultValues = get_manual_sale_result_values($ManualSale);
                            foreach ($UserResultValues as $UserResultKey => $UserResultValue){
                                if($UserResultValue['isSplitPartnerSale'] == 1 && count($UserResultValues) > 1 && $UserResultValue['UserBalance'] != 0 && $UserResultValue['UserBalance'] < 0){
                                    foreach ($UserResultValue['SendTo'] as $SendToUser){
                                        $ReceiverResultValue = $UserResultValues["manualsale_split_partner_".$SendToUser["ReceiverID"]];
                                        if($SendToUser["Amount"] != 0){
                                            $PartnerPayment = DB()->get_row("select * from ".DB()->manualsale_split_partner_payments." where userid=".$LoggedInUser->id." and SenderManualSaleSplitPartnerID=".$UserResultValue["SplitPartnerID"]." and ReceiverManualSaleSplitPartnerID=".$SendToUser["ReceiverID"]);
                                            if(!$PartnerPayment){
                                                $Amount = floatval($SendToUser["Amount"]);
                                                $OpenBalance += $Amount;
                                            }else{
                                                $Amount = 0;
                                                $OpenBalance += $Amount;
                                            }
                                        }
                                    }
                                }
                            }
                            $isPartnerSale = array_values($UserResultValues)[0]['isSplitPartnerSale'] != 0 && count($UserResultValues) > 1;
                            if($manualsalestype == "all" || 
                                ($manualsalestype == "open-balance" && $OpenBalance != 0 && $isPartnerSale) ||
                                ($manualsalestype == "zero-balance" && $OpenBalance == 0 && $isPartnerSale) ||
                                ($manualsalestype == "no-partners" && !$isPartnerSale)){
                        ?>
                        <tr>
                            <td class="text-center grey-scale"><a href="<?php site_url("manual-sale/split-partners/?".($ManualSale->LinkBankID == 0?"MasterCampaignID=".$ManualSale->MasterCampaignID:"LinkBankID=".$ManualSale->LinkBankID))?>"><i class="fa ion-network"></i></a></td>
                            <td><a href="<?php site_url("manual-sale/?".($ManualSale->LinkBankID == 0?"MasterCampaignID=".$ManualSale->MasterCampaignID:"LinkBankID=".$ManualSale->LinkBankID))?>"><?php echo empty($ManualSale->ManualSaleName)?"manual sale":$ManualSale->ManualSaleName;?></a></td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("manual-sale/?".($ManualSale->LinkBankID == 0?"MasterCampaignID=".$ManualSale->MasterCampaignID:"LinkBankID=".$ManualSale->LinkBankID));?>" class="btn btn-sm btn-links" title="Edit Manual Sale"><i class="fa fa-pencil-square"></i>Edit Manual Sale</a></li>
                                        <li><a href="<?php site_url("manual-sale/stats/?act=delete_manualsale&ManualSaleID=".$ManualSale->id);?>" class="btn btn-sm btn-links btn-delete" title="Delete Manual Sale"><i class="fa fa-trash"></i>Delete Manual Sale</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo $ManualSaleType;?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number($TotalIncome);?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number($TotalOtherIncome);?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number($TotalExpense);?></span></td>
                            <td class="text-center"><span><?php echo get_formated_number($TotalClicks);?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number(round($EPC, 2));?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number(round($OpenBalance, 2));?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number(round($TotalNetProfit, 2));?></span></td>
                            <td class="text-center">
                                <?php
                                if(array_values($UserResultValues)[0]['isSplitPartnerSale'] != 0 && count($UserResultValues) > 1){
                                ?>
                                <a href="<?php site_url("manual-sale/split-partners/pay-partners/".$ManualSale->id)?>" class="btn-link btn-xs" data-widget="ShowLinkModel" data-method="post" data-page-title="Record Partner Payments"><?php echo $OpenBalance != 0?"Pay Partner(s)":"View Confirmation"; ?></a>
                                <?php 
                                } else {
                                ?>
                                <small>N/A</small>    
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include_once "member_footer.php";
?>