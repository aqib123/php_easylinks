<?php
include_once "member_popup_header.php";
?>
<?php
session_start();
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form">
        <input type="hidden" name="Email" value="<?php echo($LoggedInUser->user_email); ?>">
        <div class="form-group has-feedback">
            <label for="VendorName" class="col-sm-4 control-label">Current Password</label>
            <div class="col-sm-8">
                <input type="password" value="" class="form-control " id="CurrentPassword" name="CurrentPassword" placeholder="Current Password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="VendorName" class="col-sm-4 control-label">New Password</label>
            <div class="col-sm-8">
                <input type="password" value="" class="form-control " id="NewPassword" name="NewPassword" placeholder="New Password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="VendorName" class="col-sm-4 control-label">Confirm Password</label>
            <div class="col-sm-8">
                <input type="password" value="" class="form-control " id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password" data-match="#NewPassword" data-match-error="Passwords do not match" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="change_password" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>