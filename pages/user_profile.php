<?php
$pagetitle = "User Profile";
$pageurl = "dashboard";
$modulename = "dashboard";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <h2>User Profile</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("user-profile")?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group has-feedback">
                                            <label for="CampaignName" class="col-sm-4 control-label">Nic Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo isset($_POST['user_nicename'])?$_POST['user_nicename']:$LoggedInUser->user_nicename;?>" class="form-control " id="user_nicename" name="user_nicename" placeholder="Nic Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="CampaignName" class="col-sm-4 control-label">Display Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo isset($_POST['display_name'])?$_POST['display_name']:$LoggedInUser->display_name;?>" class="form-control " id="display_name" name="display_name" placeholder="Display Name" required="required" <?php echo NAME_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="VendorID" class="col-sm-4 col-xs-12 control-label">Choose Timezone</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <select name="timezoneid" id="timezoneid" class="form-control select2" style="width: 100%" data-select="select" data-enable-search="true">
                                                    <option value="EST" <?php if($LoggedInUser->timezoneid == "EST") echo ' selected = "selected" ';?>>Default(EST)</option>
                                                    <?php
                                                    $zones = timezone_identifiers_list();
                                                    foreach ($zones as $zone) {
                                                        $zone = explode('/', $zone);
                                                        if (isset($zone[1]) != ''){
                                                            $zonevalue = $zone[0]. '/' . $zone[1];
                                                            $zonename = str_replace('_', ' ', $zone[0])." - ".str_replace('_', ' ', $zone[1]);
                                                        } else {
                                                            $zonevalue = $zone[0];
                                                            $zonename = str_replace('_', ' ', $zone[0]);
                                                        }
                                                    ?>
                                                    <option value="<?php echo $zonevalue;?>" <?php if($LoggedInUser->timezoneid == $zonevalue) echo ' selected = "selected" ';?>><?php echo $zonename;?></option>
                                                    <?php
                                                    }
													?>
                                                </select>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="CampaignName" class="col-sm-4 control-label">API Key</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $LoggedInUser->user_api_token;?>" class="form-control readonly" id="user_api_token" name="user_api_token" placeholder="API Key" readonly="readonly"  onclick="this.select();" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <div id="PageImageButton" class="ImageUploader" data-nocenter="true" data-usehtml="true" data-input-name="user_picture" data-existing-image="<?php echo isset($_POST['user_picture'])?$_POST['user_picture']:$LoggedInUser->user_picture;?>">
                                                    <div class="btn btn-bordered btn-gray">
                                                        <i class="fa fa-upload"></i>&nbsp;
                                                        <span>Upload User Image</span>
                                                    </div>
                                                    <br class="clearfix" />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("Dashboard")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="update_profile" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>