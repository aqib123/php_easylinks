<?php
$pagetitle = "Linkbank Stats Chart";
$pageurl = "linkbank/stats";
$modulename = "linkbank";

if(!isset($_GET["LinkBankID"]))
    site_redirect("linkbank/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->linkbanks." where id=".$_GET["LinkBankID"]." and userid=".$LoggedInUser->id;
$linkbank = DB()->get_row($sql);
if(!$linkbank)
    site_redirect("linkbank/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$linkbank->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$linkbank->DomainID);
$linkbankurl = get_linkbankurl($linkbank);

$default_dates = get_last_seven_days();//get_today_dates();
$LinkBankID = $_GET["LinkBankID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:(isset($_GET["showall"])?'':$default_dates["start_date"]);
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:(isset($_GET["showall"])?'':$default_dates["end_date"]);
$linkbank_stats = get_linkbank_stats($LinkBankID, $startdate, $enddate);

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("linkbank/?LinkBankID=".$linkbank->id)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $linkbank->LinkName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank/stats")?>" role="button">Linkbank Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-linkbank-conv" href="<?php site_url("linkbank/stats/conv")?>" role="button">Linkbank Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("linkbank/stats/".$LinkBankID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Tracking Stats Charts</h1>
            </div>
        </div>
        <br />
        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("linkbank/stats/".$linkbank->id."/details")?>"><?php echo $linkbank->LinkName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $linkbank->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $linkbankurl;?>">Tracking Link: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $linkbankurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $linkbankurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $linkbankurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                        <li>
                            <span class="small" title="<?php echo $linkbank->DestinationURL;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $linkbank->DestinationURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $linkbank->DestinationURL;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $linkbank->DestinationURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $linkbank->DestinationURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div id="linkbank_statsdaterange" class="linkbank_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:(isset($_GET["showall"])?'':$default_dates['start_date_only']);?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:(isset($_GET["showall"])?'':$default_dates['end_date_only']);?>" data-url="<?php site_url("linkbank/stats/".$_GET["LinkBankID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                </div>
            </div>
        </div>


        <?php $charttypes = array("line","bar","area","pie");?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach($charttypes as $typeindex => $charttype) { ?>
                <li <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' class="active" '?>><a href="#<?php echo $charttype?>_tab" data-toggle="tab"><i class="fa fa-<?php echo $charttype?>-chart"></i></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <?php 
                $SnapShotData = LinkBank_SnapShot($LinkBankID, $linkbank_stats); 
                $TopBrowserData = LinkBank_TopBrowser($LinkBankID, $linkbank_stats); 
                $TopPlatformData = LinkBank_TopPlatform($LinkBankID, $linkbank_stats);
                $TopCountriesData = LinkBank_TopCountries($LinkBankID, $linkbank_stats);

                if(!empty($_GET['type'])){
                    $chartviewtype = isset($_GET['type'])?$_GET['type']:"Snapshot";
                    if($chartviewtype == "Snapshot"){
                        $data = $SnapShotData;//LinkBank_SnapShot($LinkBankID, $linkbank_stats); 
                        $chartviewtypedata = 'SnapShotData';
                    } else if($chartviewtype == "TopBrowser"){
                        $data = $TopBrowserData;//LinkBank_TopBrowser($LinkBankID, $linkbank_stats); 
                        $chartviewtypedata = 'TopBrowserData';
                    } else if($chartviewtype == "TopPlatform"){
                        $data = $TopPlatformData;//LinkBank_TopPlatform($LinkBankID, $linkbank_stats); 
                        $chartviewtypedata = 'TopPlatformData';
                    } else if($chartviewtype == "TopCountries"){
                        $data = $TopCountriesData;//LinkBank_TopCountries($LinkBankID, $linkbank_stats);
                        $chartviewtypedata = 'TopCountriesData';
                    }
                }
                foreach($charttypes as $typeindex => $charttype) { 
                ?>
                <div class="tab-pane <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' active " '?>" id="<?php echo $charttype?>_tab">
                    <div class="row">
                        <?php 
                    if(empty($_GET['type'])){
                        $titles = array("Snapshot", "Top Browser", "Top Platform", "Top Countries");
                        for($chartindex = 0; $chartindex < 4; $chartindex++){
                            if($chartindex == 0){
                                $data = $SnapShotData;
                                $chartviewtypedata = 'SnapShotData';
                            } else if($chartindex == 1){
                                $data = $TopBrowserData;
                                $chartviewtypedata = 'TopBrowserData';
                            } else if($chartindex == 2){
                                $data = $TopPlatformData;
                                $chartviewtypedata = 'TopPlatformData';
                            } else if($chartindex == 3){
                                $data = $TopCountriesData;
                                $chartviewtypedata = 'TopCountriesData';
                            }
                        ?>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo $titles[$chartindex];?></h3>
                                    <a class="btn btn-link btn-sm" href="<?php site_url("linkbank/stats/".$LinkBankID."/charts/".str_replace(" ", "", $titles[$chartindex])."/".$charttype."/".(isset($_GET['startdate']) && isset($_GET['enddate'])?"?startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""));?>" target="_self">Details</a>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="box-body chart-box-body">
                                    <div class="chart" style="height: 250px;">
                                        <canvas id="<?php echo $charttype."_".$titles[$chartindex];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" data-chart-view-type="<?php echo $chartviewtypedata;?>" height="250" data-chart-values='<?php //echo implode("!", $data['piedata']);?>' data-chart-labels='<?php //echo implode('!', $data['labels'])?>' data-chart-datasets='<?php //echo implode('!', $data['datasets'])?>'></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    }else{
                        ?>
                        <div class="col-md-12">
                            <div class="box-body chart-box-body-">
                                <div class="chart" style="height: 250px;">
                                    <canvas id="<?php echo $charttype."_".$_GET['chartviewtype'];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" data-chart-view-type="<?php echo $chartviewtypedata;?>" height="250" data-chart-values='<?php //echo implode("!", $data['piedata']);?>' data-chart-labels='<?php //echo implode('!', $data['labels'])?>' data-chart-datasets='<?php //echo implode('!', $data['datasets'])?>'></canvas>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    var ChartsData = {};
    <?php
    echo "ChartsData['SnapShotData'] = ".json_encode($SnapShotData).";\n";
    echo "ChartsData['TopBrowserData'] = ".json_encode($TopBrowserData).";\n";
    echo "ChartsData['TopPlatformData'] = ".json_encode($TopPlatformData).";\n";
    echo "ChartsData['TopCountriesData'] = ".json_encode($TopCountriesData).";\n";
    ?>
    $(document).ready(function () {
        CreateDateRange($("#linkbank_statsdaterange"));

        $(document).on('click', '[data-toggle="tab"]', function (e) {
            var $this = $(this);
            var li = $this.parent();
            var $target = $($this.attr('href'));
            if (!li.hasClass("chartadded")) {
                li.addClass("chartadded");
                ChartQueue = [];
                var CurrentChartType = '';
                $target.find(".chart_canvas").each(function () {
                    var $canvas = $(this);
                    var charttype = $canvas.attr("data-chart-type");
                    CurrentChartType = charttype;
                    ChartQueue.push($canvas);
                });

                if (CurrentChartType == "pie") {
                    ExecutePieChartQueue();
                } else if (CurrentChartType == "line") {
                    ExecuteLineChartQueue();
                } else if (CurrentChartType == "bar") {
                    ExecuteBarChartQueue();
                } else if (CurrentChartType == "area") {
                    ExecuteLineChartQueue();//ExecuteAreaChartQueue();
                }
            }
        });
        $(".nav-tabs-custom ul li.active a").trigger("click");
    });

    function LegendChange(e) {
        var $this = $(this);
        var excludes = [];
        $this.closest('.legend_checkbox').find(".checkbox").each(function () {
            var $check = $(this);
            if (!$check.is(':checked'))
                excludes.push($check.val());
        });
        var $boxbody = $this.closest(".box-body");
        var $canvas = $boxbody.find(".chart_canvas");
        var charttype = $canvas.attr("data-chart-type");
        if (charttype == "pie")
            chartlegend = CreatePieChart($canvas, excludes);
        else if (charttype == "line")
            chartlegend = CreateLineChart($canvas, excludes);
        else if (charttype == "bar")
            chartlegend = CreateBarChart($canvas, excludes);
        else if (charttype == "area")
            chartlegend = CreateAreaChart($canvas, excludes);
    };
</script>
<?php
include_once "member_footer.php";
?>