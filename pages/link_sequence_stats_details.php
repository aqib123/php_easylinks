<?php
$pagetitle = "Link Sequence Stats";
$pageurl = "link-sequence/stats";

if(!isset($_GET["LinkSequenceID"]))
    site_redirect("link-sequence/stats");
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

$sql = "select * from ".DB()->link_sequences." where id=".$_GET["LinkSequenceID"]." and userid=".$LoggedInUser->id;
$link_sequence = DB()->get_row($sql);
if(!$link_sequence)
    site_redirect("link-sequence/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$link_sequence->DomainID);
$link_sequenceurl = get_link_sequenceurl($link_sequence);

$LinkSequenceID = $_GET["LinkSequenceID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$link_sequence_stats = get_link_sequences_stats($LinkSequenceID, $startdate, $enddate);

$topchartdata = LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
if($_GET && !empty($_GET['LinkSequenceID'])){
    $LinkSequenceID = parseInt($_GET['LinkSequenceID']);
    $sql = "select * from ".DB()->link_sequences." where id=".$LinkSequenceID." and userid=".$LoggedInUser->id;
    $link_sequence = DB()->get_row($sql);
    if($link_sequence){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
//$curpage .= "/".$LinkSequenceID;
$link_sequence_link = get_link_sequenceurl($link_sequence);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("link-sequence/".$link_sequence->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $link_sequence->LinkSequenceName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Sequence Tracking Stats Details</h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Link Sequence Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("link-sequence/".$link_sequence->id)?>"><?php echo $link_sequence->LinkSequenceName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $link_sequence->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $link_sequence->LinkSequenceType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $link_sequence_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $link_sequence_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $link_sequence_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="link_sequence_statslinks pull-left">
                        <select name="LinkSequenceLinkID" id="LinkSequenceLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".DB()->link_sequence_links." where LinkSequenceID=".$LinkSequenceID." and userid = ".$LoggedInUser->id." order by LinkSequenceLinkPosition";
                            $link_sequence_links = DB()->get_results($sql);
                            foreach($link_sequence_links as $link_sequence_link){
                                if ($link_sequence_link->LinkSequenceLinkType == "customurl")
                                	$LinkSequenceLinkURL = $link_sequence_link->LinkSequenceLinkURL;
                                else
                                    $LinkSequenceLinkURL = get_linkbankurl($link_sequence_link->LinkBankID);
                            ?>
                            <option value="<?php echo $link_sequence_link->id;?>" <?php if(isset($_GET['LinkSequenceLinkID']) && $_GET['LinkSequenceLinkID'] == $link_sequence_link->id) echo ' selected = "selected" ';?>><?php echo $LinkSequenceLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="link_sequence_statsdaterange" class="link_sequence_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/details/?".(isset($_GET['LinkSequenceLinkID'])?"LinkSequenceLinkID=".$_GET['LinkSequenceLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left link_sequence_statscharts">
                        <li>
                            <a href="<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?act=exportlink_sequencedetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Sequence Tracking Stats Details</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#link_sequenceslist" aria-expanded="true" aria-controls="link_sequenceslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="link_sequenceslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link_sequence-tracking-stats data-table responsive- nowrap">
                    <thead>
                        <tr>
                            <th><span>Access Time</span></th>
                            <th><span>URL</span></th>
                            <th><span>IP</span></th>
                            <th class="text-center"><span>Tier</span></th>
                            <th class="text-center"><span>Country</span></th>
                            <th class="text-center"><span>Browser</span></th>
                            <th class="text-center"><span>Platform</span></th>
                            <th class="text-center"><span>Type</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $table = " ".DB()->link_sequence_clicks." inner join ".DB()->link_sequence_links." on ".DB()->link_sequence_clicks.".LinkSequenceLinkID = ".DB()->link_sequence_links.".id ";
                        $sql = "select ".DB()->link_sequence_clicks.".* from ".$table." where LinkSequenceID=".$link_sequence->id;

                        if(isset($_GET['startdate']) && isset($_GET['enddate']))
                            $sql .= " and (".DB()->link_sequence_clicks.".DateAdded >= '".strtotime($_GET['startdate'])."' and ".DB()->link_sequence_clicks.".DateAdded <= '".strtotime($_GET['enddate'])."') ";

                        if(isset($_GET["LinkSequenceLinkID"]))
                            $sql .= " and (".DB()->link_sequence_clicks.".LinkSequenceLinkID in(".$_GET['LinkSequenceLinkID'].")) ";

                        //echo $sql;
                        $link_sequence_clicks = DB()->get_results($sql);
                        foreach($link_sequence_clicks as $link_sequence_click){
                            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);

                            $countrycode = $link_sequence_click->CountryCode;
                            if($countrycode == "") {
                                $countrycode = "unknown";
                                $countryname = "Unknown";
                                $countrytier = 3;
                            }else{
                                $country = DB()->get_row("select * from ".DB()->countries." where countrycode='".$countrycode."'");
                                $countryname = $country->countryname;
                                $countrytier = $country->countrytier;
                                $countrycode = strtolower($countrycode);
                            }

                            switch($link_sequence_click->BrowserName){
                                case 'Android Safari':
                                    $ub = "mobilesafari";
                                    break;
                                case 'Apple Safari':
                                    $ub = "safari";
                                    break;
                                case 'Microsoft Edge';
                                    $ub = "edge";
                                    break;
                                case 'Internet Explorer';
                                    $ub = "msie";
                                    break;
                                case 'Mozilla Firefox';
                                    $ub = "firefox";
                                    break;
                                case 'Google Chrome';
                                    $ub = "chrome";
                                    break;
                                case 'Opera';
                                    $ub = "opera";
                                    break;
                                case 'Netscape';
                                    $ub = "netscape";
                                    break;
                                default:
                                    $ub = "unknown";
                            }

                            if($ub == "unknown")
                                $browsername = "Unknown";
                            else
                                $browsername = $link_sequence_click->BrowserName . " " . $link_sequence_click->BrowserVersion;

                            $platform = $link_sequence_click->Platform;
                            if(strpos($platform, "indows") > 0)
                                $os = "windows";
                            else if($platform == ""){
                                $platform = "Unknown";
                                $os = "unknown";
                            }else
                                $os = strtolower($link_sequence_click->Platform);

                            $link_sequence_clicktype = "nonuniquelink_sequencevisit";
                            $clicktype = "Non-Unique";
                            $typeclass = "fa fa-users";
                            if($link_sequence_click->BotName != ""){
                                $link_sequence_clicktype = "botlink_sequencevisit";
                                $typeclass = "fa fa-bug";
                                $clicktype = "Bot";
                            }else{
                                $link_sequence_clickscount = (DB()->get_var("select count(*) from ".$table." where ClickIp='".$link_sequence_click->ClickIp."' and LinkSequenceID=".$link_sequence->id)) * 1;
                                if($link_sequence_clickscount == 1){
                                    $link_sequence_clicktype = "uniquelink_sequencevisit";
                                    $typeclass = "fa fa-user";
                                    $clicktype = "Unique";
                                }
                            }

                            $LinkSequenceLinkURL = "";
                            $sql = "select * from ".DB()->link_sequence_links." where userid = ".$LoggedInUser->id." and id=".$link_sequence_click->LinkSequenceLinkID;
                            $link_sequence_link = DB()->get_row($sql);
                            if($link_sequence_link){
                                if ($link_sequence_link->LinkSequenceLinkType == "customurl")
                                	$LinkSequenceLinkURL = $link_sequence_link->LinkSequenceLinkURL;
                                else
                                    $LinkSequenceLinkURL = get_linkbankurl($link_sequence_link->LinkBankID);
                            }
                            $flag = file_exists(BASE_DIR."/images/flags/".strtolower($countrycode).".png")?$countrycode:"noflag";
                        ?>
                        <tr>
                            <td><span><?php echo date("m/d/Y h:i:s A", $link_sequence_click->DateAdded)?></span></td>
                            <td><a href="<?php echo $LinkSequenceLinkURL;?>"><?php echo $LinkSequenceLinkURL;?></a></td>
                            <td><span><?php echo $link_sequence_click->ClickIp;?></span></td>
                            <td class="text-center"><span><?php echo $countrytier;?></span></td>
                            <td class="text-center"><a class="" target="_blank" href="https://maps.google.com/?q=<?php echo $countryname;?>" data-toggle="qtiptooltip" title="<?php echo $countryname;?>">
                                <img class="icon16xwidth" src="<?php site_url("images/flags/".$flag.".png")?>" title="<?php echo $countryname?>" /></a>
                                <span class="hidden"><?php echo $countryname;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$ub.".png")?>" data-toggle="qtiptooltip" title="<?php echo $browsername?>" />
                                <span class="hidden"><?php echo $browsername;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$os.".png")?>" data-toggle="qtiptooltip" title="<?php echo $platform;?>" />
                                <span class="hidden"><?php echo $platform;?></span>
                            </td>
                            <td class="text-center">
                                <span class="<?php echo $link_sequence_clicktype;?>" data-toggle="qtiptooltip" title="<?php echo $clicktype;?>"><i class="<?php echo $typeclass;?>"></i></span>
                                <span class="hidden"><?php echo $clicktype;?></span>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#link_sequence_statsdaterange"));

        //CreatePieChart($('#TopChart'));

        $("#LinkSequenceLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/details/?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "LinkSequenceLinkID=" + $this.val();
            }
            window.location.href = url;
        });
    });
</script>
<?php
include_once "member_footer.php";
?>