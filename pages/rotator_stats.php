<?php
if($_GET['rotator_type'] == "") 
    $rotator_type = "all";
else
    $rotator_type = $_GET['rotator_type'];

$pagetitle = "Rotator Stats";//.ucfirst($rotator_type);
$curpage = "rotator/stats";
$pageurl = $curpage;//"rotator";
$modulename = "rotator";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Rotators"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Rotators"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Rotators"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed Rotators"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Rotator</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#rotatorslist" aria-expanded="true" aria-controls="rotatorslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="rotatorslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-rotator-tracking-stats data-table responsive- nowrap" style="width: 100%;" data-nosort-columns="0,1,2" data-default-sort-column="3">
                    <thead>
                        <tr>
                            <th class="text-center"><span></span></th>
                            <th class="text-center"><span></span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Rotator</span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Group</span></th>
                            <th><span>Type</span></th>
                            <th><span>Master</span></th>
                            <th class="text-center stats-column"><span># Rotator Links</span></th>
                            <th class="text-center stats-column"><span>Rotator Link Visited</span></th>
                            <th class="text-center"><span>Status</span></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sql = "select * from ".DB()->rotators." where userid=".$LoggedInUser->id;

                      if($rotator_type == "pending")
                          $sql .= " and RotatorStatus = 'pending' ";//" and ('".strtotime("now")."' < StartDate) ";
                      else if($rotator_type == "active")
                          $sql .= " and RotatorStatus = 'active' ";//" and ('".strtotime("now")."' > StartDate and '".strtotime("now")."' < EndDate) ";
                      else if($rotator_type == "complete")
                          $sql .= " and RotatorStatus = 'complete' ";//" and ('".strtotime("now")."' > EndDate) ";

                      $rotators = DB()->get_results($sql);
                      foreach($rotators as $rotator){
                          $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
                      
                          $RotatorRotatorLinks = DB()->get_var("SELECT count(*) from ".DB()->rotator_links." where RotatorID=".$rotator->id." ") * 1;
                          
                          $UniqueRotatorVisits = DB()->get_var("select SUM(RotatorVisitCount) as TotalRotatorVisits from (select 1 as RotatorVisitCount from ".DB()->rotator_clicks." where RotatorLinkID in (SELECT id from ".DB()->rotator_links." where RotatorID=".$rotator->id." and userid=".$LoggedInUser->id.") group by ClickIp) as RotatorVisits") * 1;
                          $RawRotatorVisits = DB()->get_var("select SUM(RotatorVisitCount) as TotalRotatorVisits from (select count(*) as RotatorVisitCount from ".DB()->rotator_clicks." where RotatorLinkID in (SELECT id from ".DB()->rotator_links." where RotatorID=".$rotator->id." and userid=".$LoggedInUser->id.")) as RotatorVisits") * 1;
                          //$RawRotatorVisits -= $UniqueRotatorVisits;

                          $RotatorStatus = $rotator->RotatorStatus;
                          if($RotatorStatus == "active"){
                              $rotatorstatustext = "Live";
                              $rotatorstatusicon = "fa fa-rocket";
                          }else if($RotatorStatus == "pending"){
                              $rotatorstatustext = "Pending";
                              $rotatorstatusicon = "fa fa-hourglass-o";
                          }else if($RotatorStatus == "complete"){
                              $rotatorstatus = "Completed";
                              $rotatorstatusicon = "fa fa-check-square-o";
                          }

                          $namelinkedurl = get_site_url("rotator/?RotatorID=".$rotator->id);

                          $rotator_linkurl = get_rotatorurl($rotator);
                          $StatusIcon = get_domain_status_icon($rotator->DomainID);

                          $MasterCampaignName = "";
                          if($rotator->MasterCampaignID != "0")
                              $MasterCampaignName = DB()->get_var("select MasterCampaignName from ".DB()->master_campaigns." where id=".$rotator->MasterCampaignID);
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $StatusIcon;?></td>
                            <td class="text-center grey-scale"><a href="javascript:" data-url="<?php site_url($curpage."?act=rotator_details&RotatorID=".$rotator->id);?>" data-toggle="DetailRow" id="" class="btn btn-rotator-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <td class="text-center grey-scale"><a href="<?php site_url("rotator/links/".$rotator->id."/")?>"><i class="fa ion-network"></i></a></td>
                            <td>
                                <a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $rotator->RotatorName?></a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("rotator/?RotatorID=".$rotator->id);?>" class="btn btn-sm btn-links" title="Edit Rotator"><i class="fa fa-pencil-square"></i>Edit Rotator</a></li>
                                        <li><a href="<?php echo $rotator_linkurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i>Direct Link</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=delete_rotator&RotatorID=".$rotator->id);?>" class="btn btn-sm btn-links btn-delete" title="Delete Rotator"><i class="fa fa-trash"></i>Delete Rotator</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=reset_rotator&RotatorID=".$rotator->id);?>" class="btn btn-sm btn-links btn-reset" title="Reset Rotator"><i class="fa fa-ban"></i>Reset Rotator</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=clone_rotator&RotatorID=".$rotator->id);?>" class="btn btn-sm btn-links" title="Clone Rotator"><i class="fa fa-clone"></i>Clone Rotator</a></li>
                                        <li><a href="javascript: void(0);" data-clipboard-text="<?php echo $rotator_linkurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $rotator_linkurl;?>"><i class="fa fa-clipboard"></i>Copy Link</a></li>
                                        <li><a href="<?php site_url("rotator/stats/".$rotator->id."/details");?>" class="btn btn-sm btn-links" title="Statistics"><i class="fa fa-globe"></i>Statistics</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo $GroupName;?></span></td>
                            <td><span><?php echo $rotator->RotatorType;?></span></td>
                            <td><span><?php echo $MasterCampaignName;?></span></td>
                            <td class="text-center"><span><?php echo $RotatorRotatorLinks;?></span></td>
                            <td class="text-center"><span><?php echo $RawRotatorVisits;?></span></td>
                            <td class="text-center grey-scale">
                                <a href="javascript:" class="status_icon" id="" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click"><i class="fa <?php echo $rotatorstatusicon;?>"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <form class="form-inline status_form" role="form" method="get" action="<?php site_url($curpage.(!empty($_GET['rotator_type'])?"/".$_GET['rotator_type']."/":""));?>">
                                        <div class="form-group select-xs">
                                            <select name="status" id="RotatorStatus" class="form-control select2">
                                                <?php
                                                $LinkStatuses = SYS()->LinkStatuses;
                                                foreach($LinkStatuses as $key => $value){
                                                ?>
                                                <option value="<?php echo $key;?>" <?php if($rotator->RotatorStatus == $key || (isset($_POST['RotatorStatus']) && $_POST['RotatorStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-xs btn-default">Change</button>
                                        <input type="hidden" name="act" value="changerotatorstatus" />
                                        <input type="hidden" name="RotatorID" value="<?php echo $rotator->id;?>" />
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br />
        <br />
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>