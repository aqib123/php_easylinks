<?php
if($_GET['campaign_type'] == "") 
    $campaign_type = "all";
else
    $campaign_type = $_GET['campaign_type'];

$pagetitle = "Campaign Stats";//.ucfirst($campaign_type);
$curpage = "campaign/stats";
$pageurl = "campaign/stats";
$modulename = "email-campaigns";

include_once "member_header.php";
$MenuItem = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign/single")?>" role="button">Create Single Email</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("campaign")?>" role="button">Create Email Series</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("campaign/stats")?>" role="button">Campaign Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Campaigns"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Campaigns"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Campaigns"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed Campaigns"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <?php for($campaigntype = 0; $campaigntype <= 1; $campaigntype++) { ?>
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $campaigntype == 0?"Email Series":"Single Email";?></h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#campaignslist" aria-expanded="true" aria-controls="campaignslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="campaignslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-campaign-tracking-stats data-table- responsive- nowrap" data-nobuttons="true">
                    <thead>
                        <tr>
                            <th class="text-center"><span></span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Campaign</span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Group</span></th>
                            <th><span>Master</span></th>
                            <th class="text-center stats-column"><span># Emails</span></th>
                            <th class="text-center stats-column"><span>Email Clicks</span></th>
                            <th class="text-center stats-column"><span>CTA</span></th>
                            <th class="text-center stats-column"><span>Fires</span></th>
                            <th class="text-center"><span>Status</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                  $sql = "select * from ".DB()->campaigns." where userid=".$LoggedInUser->id." and hasSingleEmail = ".$campaigntype;

                          if($campaign_type == "pending")
                              $sql .= " and CampaignStatus = 'pending' ";
                          else if($campaign_type == "active")
                              $sql .= " and CampaignStatus = 'active' ";
                          else if($campaign_type == "complete")
                              $sql .= " and CampaignStatus = 'complete' ";

                          $campaigns = DB()->get_results($sql);
                          foreach($campaigns as $campaign){
                              $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$campaign->GroupID);
                      
                              $Emails = DB()->get_var("SELECT count(*) from ".DB()->campaign_emails." where CampaignID=".$campaign->id." ") * 1;
                              
                              $UniqueClicks = DB()->get_var("select SUM(ClickCount) as TotalClicks from (select 1 as ClickCount from ".DB()->linkbank_clicks." where ClickType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.") group by ClickIp) as Clicks") * 1;
                              $RawClicks = DB()->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".DB()->linkbank_clicks." where ClickType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")) as Clicks") * 1;
                              //$RawClicks -= $UniqueClicks;

                              //$UniqueClicks = DB()->get_var("select count(DISTINCT ClickIp) from ".DB()->linkbank_clicks." where CampaignID=".$campaign->id." group by ClickIp") * 1;
                              $LinkBankSales = DB()->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".DB()->linkbank_conversions." where EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")) as Clicks") * 1;
                              //$LinkBankSales = DB()->get_var("select count(*) from ".DB()->linkbank_conversions." where CampaignID=".$campaign->id) * 1;
                              $Sales = $campaign->SaleQuantity;// DB()->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".DB()->linkbank_conversions." where ConversionType='sales' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")) as Clicks") * 1;
                              //$Sales = DB()->get_var("select sum(UnitPrice) from ".DB()->linkbank_conversions." where CampaignID=".$campaign->id." and ConversionType='sales'") * 1;
        //$PixelFires = DB()->get_var("select SUM(FireCount) as FireCount from (select count(FireIp) as FireCount from ".DB()->linkbank_pixel_fires." where FireType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.") group by FireIp) as FireCounts") * 1;
                              $PixelFires = DB()->get_var("select count(FireIp) as FireCount from ".DB()->linkbank_pixel_fires." where FireType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$campaign->id." and userid=".$LoggedInUser->id.")") * 1;
                              //$PixelFires = DB()->get_var("select count(DISTINCT FireIp) from ".DB()->linkbank_pixel_fires." where CampaignID=".$campaign->id." group by FireIp") * 1;
                              $CTAClicks = get_campaign_cta_clicks($campaign->id);
                      
                              $CampaignStatus = $campaign->CampaignStatus;
                              if($CampaignStatus == "active"){
                                  $campaignstatustext = "Live";
                                  $campaignstatusicon = "fa fa-rocket";
                              }else if($CampaignStatus == "pending"){
                                  $campaignstatustext = "Pending";
                                  $campaignstatusicon = "fa fa-hourglass-o";
                              }else if($CampaignStatus == "complete"){
                                  $campaignstatus = "Completed";
                                  $campaignstatusicon = "fa fa-check-square-o";
                              }

                              $namelinkedurl = get_site_url("campaign/?CampaignID=".$campaign->id);

                              $MasterCampaignName = "";
                              if($campaign->MasterCampaignID != "0")
                                  $MasterCampaignName = DB()->get_var("select MasterCampaignName from ".DB()->master_campaigns." where id=".$campaign->MasterCampaignID);
                        ?>
                        <tr>
                            <td class="text-center grey-scale"><a href="javascript:" data-url="<?php site_url($curpage."?act=campaign_details&CampaignID=".$campaign->id);?>" data-toggle="DetailRow" id="" class="btn btn-campaign-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <td class="text-center grey-scale"><a href="<?php site_url("campaign/emails/".$campaign->id."/")?>" class="pull-right"><i class="fa ion-network"></i></a></td>
                            <td>
                                <a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $campaign->CampaignName?></a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("campaign/?CampaignID=".$campaign->id);?>" class="btn btn-sm btn-links" title="Edit Campaign"><i class="fa fa-pencil-square"></i>Edit Campaign</a></li>
                                        <li><a href="<?php site_url("campaign/stats/?act=delete_campaign&CampaignID=".$campaign->id);?>" class="btn btn-sm btn-links btn-delete" title="Delete Campaign"><i class="fa fa-trash"></i>Delete Campaign</a></li>
                                        <li><a href="<?php site_url("campaign/stats/?act=reset_campaign&CampaignID=".$campaign->id);?>" class="btn btn-sm btn-links btn-reset" title="Reset Statistics"><i class="fa fa-ban"></i>Reset Statistics</a></li>
                                        <li><a href="<?php site_url("campaign/stats/?act=clone_campaign&CampaignID=".$campaign->id);?>" class="btn btn-sm btn-links" title="Clone Campaign"><i class="fa fa-clone"></i>Clone Campaign</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo $GroupName;?></span></td>
                            <td><span><?php echo $MasterCampaignName;?></span></td>
                            <td class="text-center"><span><?php echo $Emails;?></span></td>
                            <td class="text-center"><span><?php echo $RawClicks;?></span></td>
                            <td class="text-center"><span><?php echo $CTAClicks;?></span></td>
                            <td class="text-center"><span><?php echo $PixelFires;?></span></td>
                            <td class="text-center grey-scale">
                                <a href="javascript:" class="status_icon" id="" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click"><i class="fa <?php echo $campaignstatusicon;?>"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <form class="form-inline status_form" role="form" method="get" action="<?php site_url($curpage.(!empty($_GET['campaign_type'])?"/".$_GET['campaign_type']."/":""));?>">
                                        <div class="form-group select-xs">
                                            <select name="status" id="CampaignStatus" class="form-control select2">
                                                <?php
                                              $LinkStatuses = SYS()->LinkStatuses;
                                              foreach($LinkStatuses as $key => $value){
                                                ?>
                                                <option value="<?php echo $key;?>" <?php if($campaign->CampaignStatus == $key || (isset($_POST['CampaignStatus']) && $_POST['CampaignStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                <?php
                                              }
                                                ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-xs btn-default">Change</button>
                                        <input type="hidden" name="act" value="changecampaignstatus" />
                                        <input type="hidden" name="CampaignID" value="<?php echo $campaign->id;?>" />
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
                          }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br />
        <br />
        <?php } ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>