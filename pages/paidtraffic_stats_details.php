<?php
$pagetitle = "Paid Traffic Stats";
$pageurl = "paidtraffic/stats";
$modulename = "paid-traffic";

if(!isset($_GET["PaidTrafficID"]))
    site_redirect("paidtraffic/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->paidtraffics." where id=".$_GET["PaidTrafficID"]." and userid=".$LoggedInUser->id;
$paidtraffic = DB()->get_row($sql);
if(!$paidtraffic)
    site_redirect("paidtraffic/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$paidtraffic->DomainID);
$paidtrafficurl = get_paidtrafficurl($paidtraffic);

$PaidTrafficID = $_GET["PaidTrafficID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$paidtraffic_stats = get_paidtraffic_stats($PaidTrafficID, $startdate, $enddate);

$topchartdata = PaidTraffic_SnapShot($PaidTrafficID, $paidtraffic_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $paidtraffic->PaidTrafficName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic")?>" role="button">Create Paid Campaign</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic/stats")?>" role="button">Paid Traffic Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-paidtraffic-conv" href="<?php site_url("paidtraffic/stats/conv")?>" role="button">Paid Traffic Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Paid Traffic Tracking Stats Details</h1>
            </div>
        </div>
        <br />
        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("paidtraffic/stats/".$paidtraffic->id."/details")?>"><?php echo $paidtraffic->PaidTrafficName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $paidtraffic->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtrafficurl;?>">Tracking PaidTraffic: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtrafficurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtrafficurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtraffic->DestinationURL;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtraffic->DestinationURL;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtraffic->DestinationURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div id="paidtraffic_statsdaterange" class="paidtraffic_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("paidtraffic/stats/".$_GET["PaidTrafficID"]."/details/");?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left paidtraffic_statscharts">
                        <li>
                            <a href="<?php site_url("paidtraffic/stats/".$_GET["PaidTrafficID"]."/details/?act=exportlinkdetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->

                    <!--<div class="pull-left paidtraffic_statschart" style="width:60px; height:60px">
                        <canvas id="TopChart" class="chart_canvas" data-chart-type="" height="60" width="60" data-chart-values='<?php echo implode("!", $topchartdata['piedata']);?>' data-chart-labels='<?php echo implode('!', $topchartdata['labels'])?>' data-chart-datasets='<?php echo implode('!', $topchartdata['datasets'])?>'></canvas>
                    </div>-->
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Paid Traffic Tracking Stats Details</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="linkslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table responsive- nowrap" data-paging="true">
                    <thead>
                        <tr>
                            <th><span>Access Time</span></th>
                            <th><span>IP</span></th>
                            <th class="text-center"><span>Tier</span></th>
                            <th class="text-center"><span>Country</span></th>
                            <th class="text-center"><span>Browser</span></th>
                            <th class="text-center"><span>Platform</span></th>
                            <th class="text-center"><span>Type</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "select * from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id;

                        if(isset($_GET['startdate']) && isset($_GET['enddate']))
                            $sql .= " and (DateAdded >= '".strtotime($_GET['startdate'])."' and DateAdded <= '".strtotime($_GET['enddate'])."') ";
                        
                        $paidtraffic_clicks = DB()->get_results($sql);
                        foreach($paidtraffic_clicks as $paidtraffic_click){
                            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
                            $VendorName = DB()->get_var("select VendorName from ".DB()->vendors." where id=".$paidtraffic->VendorID);

                            $countrycode = $paidtraffic_click->CountryCode;
                            if($countrycode == "") {
                                $countrycode = "unknown";
                                $countryname = "Unknown";
                                $countrytier = 3;
                            }else{
                                $country = DB()->get_row("select * from ".DB()->countries." where countrycode='".$countrycode."'");
                                $countryname = $country->countryname;
                                $countrytier = $country->countrytier;
                                $countrycode = strtolower($countrycode);
                            }

                            switch($paidtraffic_click->BrowserName){
                                case 'Android Safari':
                                    $ub = "mobilesafari";
                                    break;
                                case 'Apple Safari':
                                    $ub = "safari";
                                    break;
                                case 'Microsoft Edge';
                                    $ub = "edge";
                                    break;
                                case 'Internet Explorer';
                                    $ub = "msie";
                                    break;
                                case 'Mozilla Firefox';
                                    $ub = "firefox";
                                    break;
                                case 'Google Chrome';
                                    $ub = "chrome";
                                    break;
                                case 'Opera';
                                    $ub = "opera";
                                    break;
                                case 'Netscape';
                                    $ub = "netscape";
                                    break;
                                default:
                                    $ub = "unknown";
                            }

                            if($ub == "unknown")
                                $browsername = "Unknown";
                            else
                                $browsername = $paidtraffic_click->BrowserName . " " . $paidtraffic_click->BrowserVersion;

                            $platform = $paidtraffic_click->Platform;
                            if(strpos($platform, "indows") > 0)
                                $os = "windows";
                            else if($platform == ""){
                                $platform = "Unknown";
                                $os = "unknown";
                            }else
                                $os = strtolower($paidtraffic_click->Platform);

                            $paidtraffic_clicktype = "nonuniqueclick";
                            $clicktype = "Non-Unique";
                            $typeclass = "fa fa-users";
                            if($paidtraffic_click->BotName != ""){
                                $paidtraffic_clicktype = "botclick";
                                $typeclass = "fa fa-bug";
                                $clicktype = "Bot";
                            }else{
                                $paidtraffic_clickscount = (DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where ClickIp='".$paidtraffic_click->ClickIp."' and PaidTrafficID=".$paidtraffic->id)) * 1;
                                if($paidtraffic_clickscount == 1){
                                    $paidtraffic_clicktype = "uniqueclick";
                                    $typeclass = "fa fa-user";
                                    $clicktype = "Unique";
                                }
                            }
                            $flag = file_exists(BASE_DIR."/images/flags/".strtolower($countrycode).".png")?$countrycode:"noflag";
                        ?>
                        <tr>
                            <td><span><?php echo date("m/d/Y h:i:s A", $paidtraffic_click->DateAdded)?></span></td>
                            <td><span><?php echo $paidtraffic_click->ClickIp;?></span></td>
                            <td class="text-center"><span><?php echo $countrytier;?></span></td>
                            <td class="text-center"><a class="" target="_blank" href="https://maps.google.com/?q=<?php echo $countryname;?>" data-toggle="qtiptooltip" title="<?php echo $countryname;?>">
                                <img class="icon16xwidth" src="<?php site_url("images/flags/".$flag.".png")?>" title="<?php echo $countryname?>" /></a>
                                <span class="hidden"><?php echo $countryname;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$ub.".png")?>" data-toggle="qtiptooltip" title="<?php echo $browsername?>" />
                                <span class="hidden"><?php echo $browsername;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$os.".png")?>" data-toggle="qtiptooltip" title="<?php echo $platform;?>" />
                                <span class="hidden"><?php echo $platform;?></span>
                            </td>
                            <td class="text-center">
                                <span class="<?php echo $paidtraffic_clicktype;?>" data-toggle="qtiptooltip" title="<?php echo $clicktype;?>"><i class="<?php echo $typeclass;?>"></i></span>
                                <span class="hidden"><?php echo $clicktype;?></span>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#paidtraffic_statsdaterange"));

        //CreatePieChart($('#TopChart'));
    });
</script>
<?php
include_once "member_footer.php";
?>