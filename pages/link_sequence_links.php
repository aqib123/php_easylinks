<?php
if($_GET['link_sequence_link_type'] == "") 
    $link_sequence_link_type = "all";
else
    $link_sequence_link_type = $_GET['link_sequence_link_type'];

if(!isset($_GET['link_sequence_linkbank_database'])){
    $pagetitle = "Link Sequence Links";//.ucfirst($link_sequence_link_type);
    $link_sequence_database = false;
    $curpage = "link-sequence/links";
}else{
    $pagetitle = "Link Sequence Links - ".ucfirst($link_sequence_link_type);
    $link_sequence_database = true;
    $curpage = "link-sequence/database";
}
$pageurl = "link-sequence/database";
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

$isUpdateForm = false;
if($_GET && !empty($_GET['LinkSequenceID'])){
    $LinkSequenceID = parseInt($_GET['LinkSequenceID']);
    $sql = "select * from ".DB()->link_sequences." where id=".$LinkSequenceID." and userid=".$LoggedInUser->id;
    $link_sequence = DB()->get_row($sql);
    if($link_sequence){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
$curpage .= "/".$LinkSequenceID;
$link_sequence_link = get_link_sequenceurl($link_sequence);
?>
<script>var link_sequence_links = {};</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("link-sequence/".$link_sequence->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $link_sequence->LinkSequenceName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage."/paused");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Paused Links"><i class="fa fa-pause"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Links"><i class="fa fa-rocket"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Link Sequence Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("link-sequence/".$link_sequence->id)?>"><?php echo $link_sequence->LinkSequenceName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $link_sequence->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $link_sequence->LinkSequenceType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $link_sequence_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $link_sequence_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $link_sequence_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        if($isUpdateForm){
        ?>
        <div class="box box-solid box-gray">
            <div class="box-header with-border">
                <h3 class="box-title">Create Link Sequence Link</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body link_sequence_links clearfix" id="link_sequence_links">
                <?php
            $sql = "select * from ".DB()->link_sequence_links." where userid=".$LoggedInUser->id." and LinkSequenceID=".$LinkSequenceID;
            if ($link_sequence_link_type != "all")
                $sql .= " and LinkSequenceLinkLive = ".($link_sequence_link_type=="paused"?"0":"1")." ";
            $sql .= " order by LinkSequenceLinkPosition";
            
            $link_sequence_links = DB()->get_results($sql);
            $index = 1;
            foreach($link_sequence_links as $link_sequence_link){
                $link_sequence_linkbox = get_link_sequence_link_box($link_sequence_link, $index, $link_sequence_link_type);
                if($link_sequence_linkbox != ""){
                    echo $link_sequence_linkbox;
                }
                $index++;
            }?>
                <a href="#" class="btn btn-link btn-add-link_sequence_link pull-right">Add Another Link</a>
            </div>
        </div>
        <?php
        }
        ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQueryUI -->
<link rel="stylesheet" href="<?php site_url("css/ui-themes/smoothness/jquery-ui.min.css");?>" />

<script>
    var linkindex = '<?php echo $index;?>';
    var url = "<?php site_url("link-sequence/links/".$LinkSequenceID."/")?>"; 
</script>

<!-- link_sequence_links App -->
<script src="<?php site_url("js/link_sequence_links.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/ckeditor/ckeditor.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/ckeditor/adapters/jquery.js");?>" type="text/javascript"></script>
<?php
include_once "member_footer.php";
?>