<?php
$pagetitle = "Link Sequence Stats Chart";
$pageurl = "link-sequence/stats";

if(!isset($_GET["LinkSequenceID"]))
    site_redirect("link-sequence/stats");
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

$sql = "select * from ".DB()->link_sequences." where id=".$_GET["LinkSequenceID"]." and userid=".$LoggedInUser->id;
$link_sequence = DB()->get_row($sql);
if(!$link_sequence)
    site_redirect("link-sequence/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$link_sequence->DomainID);
$link_sequenceurl = get_link_sequenceurl($link_sequence);

$LinkSequenceID = $_GET["LinkSequenceID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$link_sequence_stats = get_link_sequences_stats($LinkSequenceID, $startdate, $enddate);

$topchartdata = LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
if($_GET && !empty($_GET['LinkSequenceID'])){
    $LinkSequenceID = parseInt($_GET['LinkSequenceID']);
    $sql = "select * from ".DB()->link_sequences." where id=".$LinkSequenceID." and userid=".$LoggedInUser->id;
    $link_sequence = DB()->get_row($sql);
    if($link_sequence){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
//$curpage .= "/".$LinkSequenceID;
$link_sequence_link = get_link_sequenceurl($link_sequence);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("link-sequence/".$link_sequence->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $link_sequence->LinkSequenceName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Sequence Tracking Stats Charts</h1>
            </div>
        </div>
        <br />
        
        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Link Sequence Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("link-sequence/".$link_sequence->id)?>"><?php echo $link_sequence->LinkSequenceName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $link_sequence->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $link_sequence->LinkSequenceType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $link_sequence_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $link_sequence_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $link_sequence_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $link_sequence_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="link_sequence_statslinks pull-left">
                        <select name="LinkSequenceLinkID" id="LinkSequenceLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".DB()->link_sequence_links." where LinkSequenceID=".$LinkSequenceID." and userid = ".$LoggedInUser->id." order by LinkSequenceLinkPosition";
                            $link_sequence_links = DB()->get_results($sql);
                            foreach($link_sequence_links as $link_sequence_link){
                                if ($link_sequence_link->LinkSequenceLinkType == "customurl")
                                	$LinkSequenceLinkURL = $link_sequence_link->LinkSequenceLinkURL;
                                else
                                    $LinkSequenceLinkURL = get_linkbankurl($link_sequence_link->LinkBankID);
                            ?>
                            <option value="<?php echo $link_sequence_link->id;?>" <?php if(isset($_GET['LinkSequenceLinkID']) && $_GET['LinkSequenceLinkID'] == $link_sequence_link->id) echo ' selected = "selected" ';?>><?php echo $LinkSequenceLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="link_sequence_statsdaterange" class="link_sequence_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?".(isset($_GET['LinkSequenceLinkID'])?"LinkSequenceLinkID=".$_GET['LinkSequenceLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left link_sequence_statscharts">
                        <li>
                            <a href="<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?act=exportlink_sequencedetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>


        <?php $charttypes = array("line","bar","area","pie");?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach($charttypes as $typeindex => $charttype) { ?>
                <li <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' class="active" '?>><a href="#<?php echo $charttype?>_tab" data-toggle="tab"><i class="fa fa-<?php echo $charttype?>-chart"></i></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <?php 
                if(!empty($_GET['type'])){
                    $chartviewtype = isset($_GET['type'])?$_GET['type']:"Snapshot";
                    if($chartviewtype == "Snapshot")
                        $data = LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats); 
                    else if($chartviewtype == "TopBrowser")
                        $data = LinkSequence_TopBrowser($LinkSequenceID, $link_sequence_stats); 
                    else if($chartviewtype == "TopPlatform")
                        $data = LinkSequence_TopPlatform($LinkSequenceID, $link_sequence_stats); 
                    else if($chartviewtype == "TopCountries")
                        $data = LinkSequence_TopCountries($LinkSequenceID, $link_sequence_stats); 
                }
                foreach($charttypes as $typeindex => $charttype) { 
                ?>
                <div class="tab-pane <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' active " '?>" id="<?php echo $charttype?>_tab">
                    <div class="row">
                    <?php 
                    if(empty($_GET['type'])){
                        $titles = array("Snapshot", "Top Browser", "Top Platform", "Top Countries");
                        for($chartindex = 0; $chartindex < 4; $chartindex++){
                            if($chartindex == 0)
                                $data = LinkSequence_SnapShot($LinkSequenceID, $link_sequence_stats); 
                            else if($chartindex == 1)
                                $data = LinkSequence_TopBrowser($LinkSequenceID, $link_sequence_stats); 
                            else if($chartindex == 2)
                                $data = LinkSequence_TopPlatform($LinkSequenceID, $link_sequence_stats); 
                            else if($chartindex == 3)
                                $data = LinkSequence_TopCountries($LinkSequenceID, $link_sequence_stats); 
                        ?>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo $titles[$chartindex];?></h3>
                                    <a class="btn btn-link btn-sm" href="<?php site_url("link-sequence/stats/".$LinkSequenceID."/charts/".str_replace(" ", "", $titles[$chartindex])."/".$charttype."/".(isset($_GET['startdate']) && isset($_GET['enddate'])?"?startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""));?>" target="_self">Details</a>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="box-body chart-box-body ">
                                    <div class="chart" style="height: 250px;">
                                        <canvas id="<?php echo $charttype."_".$titles[$chartindex];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" height="250" data-chart-values='<?php echo implode("!", $data['piedata']);?>' data-chart-labels='<?php echo implode('!', $data['labels'])?>' data-chart-datasets='<?php echo implode('!', $data['datasets'])?>'></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    }else{
                        ?>
                        <div class="col-md-12">
                            <div class="box-body chart-box-body-">
                                <div class="chart" style="height: 250px;">
                                    <canvas id="<?php echo $charttype."_".$_GET['chartviewtype'];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" height="250" data-chart-values='<?php echo implode("!", $data['piedata']);?>' data-chart-labels='<?php echo implode('!', $data['labels'])?>' data-chart-datasets='<?php echo implode('!', $data['datasets'])?>'></canvas>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#link_sequence_statsdaterange"));

        $(document).on('click', '[data-toggle="tab"]', function (e) {
            var $this = $(this);
            var li = $this.parent();
            var $target = $($this.attr('href'));
            if (!li.hasClass("chartadded")) {
                li.addClass("chartadded");
                $target.find(".chart_canvas").each(function () {
                    var $canvas = $(this);
                    var charttype = $canvas.attr("data-chart-type");
                    var chartlegend = "";
                    if (charttype == "pie")
                        chartlegend = CreatePieChart($canvas);
                    else if (charttype == "line")
                        chartlegend = CreateLineChart($canvas);
                    else if (charttype == "bar")
                        chartlegend = CreateBarChart($canvas);
                    else if (charttype == "area")
                        chartlegend = CreateAreaChart($canvas);

                    $canvas.closest(".box-body").append('<div class="smallcheck legend_checkbox scrollable-div">' + chartlegend + '</div>');
                    $canvas.closest(".box-body").find(".legend_checkbox").find('input[type="checkbox"].blue, input[type="radio"].blue').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                    $canvas.closest(".box-body").find(".legend_checkbox").find('.blue').on('ifChanged', LegendChange);
                    $canvas.closest(".box-body").find(".legend_checkbox").mCustomScrollbar({
                        theme: "dark"
                    });
                });
            }
        });
        //CreatePieChart($('#TopChart'));

        $("#LinkSequenceLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("link-sequence/stats/".$_GET["LinkSequenceID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "LinkSequenceLinkID=" + $this.val();
            }
            window.location.href = url;
        });

        $(".nav-tabs-custom ul li.active a").trigger("click");
    });

    function LegendChange(e) {
        var $this = $(this);
        var excludes = [];
        $this.closest('.legend_checkbox').find(".checkbox").each(function () {
            var $check = $(this);
            if (!$check.is(':checked'))
                excludes.push($check.val());
        });
        var $boxbody = $this.closest(".box-body");
        var $canvas = $boxbody.find(".chart_canvas");
        var charttype = $canvas.attr("data-chart-type");
        if (charttype == "pie")
            chartlegend = CreatePieChart($canvas, excludes);
        else if (charttype == "line")
            chartlegend = CreateLineChart($canvas, excludes);
        else if (charttype == "bar")
            chartlegend = CreateBarChart($canvas, excludes);
        else if (charttype == "area")
            chartlegend = CreateAreaChart($canvas, excludes);
    };
</script>
<?php
include_once "member_footer.php";
?>