<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['PartnerID'])){
    $PartnerID = parseInt($_GET['PartnerID']);
    $sql = "select * from ".DB()->split_partners." where userid = ".$LoggedInUser->id." and id=".$_GET['PartnerID'];
    $split_partner = DB()->get_row($sql);
    if($split_partner)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form">
        <div class="form-group has-feedback">
            <label for="PartnerName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->PartnerName;?>" class="form-control " id="PartnerName" name="PartnerName" placeholder="Name" required="required" <?php NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group">
            <label for="" class="col-sm-4 control-label">Picture</label>
            <div class="col-sm-8">
                <div id="PageImageButton" class="ImageUploader" data-usehtml="true" data-input-name="PartnerPicture" data-existing-image="<?php if($isUpdateForm) echo $split_partner->PartnerPicture; else if(isset($_POST['PartnerPicture'])) echo $_POST['PartnerPicture'];?>">
                    <div class="btn btn-bordered btn-gray">
                        <i class="fa fa-upload"></i>&nbsp;
                        <span>Upload Partner Picture</span>
                    </div>
                    <br class="clearfix" />
                    <br />
                </div>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="WebsiteUrl" class="col-sm-4 control-label">Website Url</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->WebsiteUrl;?>" class="form-control " id="WebsiteUrl" name="WebsiteUrl" placeholder="Website Url" <?php echo URL_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="SkypeName" class="col-sm-4 control-label">Skype Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->SkypeName;?>" class="form-control " id="SkypeName" name="SkypeName" placeholder="Skype Name" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="LinkedIn" class="col-sm-4 control-label">Linked In</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->LinkedIn;?>" class="form-control " id="LinkedIn" name="LinkedIn" placeholder="Linked In" <?php echo URL_PATTERN?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="FacebookID" class="col-sm-4 control-label">Facebook ID</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->FacebookID;?>" class="form-control " id="FacebookID" name="FacebookID" placeholder="Facebook ID" <?php echo URL_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="EmailAddress" class="col-sm-4 control-label">Email Address</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->EmailAddress;?>" class="form-control " id="EmailAddress" name="EmailAddress" placeholder="Email Address" <?php echo EMAIL_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="PartnerTags" class="col-sm-4 control-label">Tags</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $split_partner->PartnerTags;?>" class="form-control tokenfieldinput ignore" id="PartnerTags" name="PartnerTags" placeholder="Partner Tags" />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group">
            <label for="AdditionalNotes" class="col-sm-4 control-label">Additional Notes</label>
            <div class="col-sm-8">
                <textarea rows="3" class="form-control " id="AdditionalNotes" name="AdditionalNotes" placeholder="Additional Notes"><?php if($isUpdateForm) echo $split_partner->AdditionalNotes;?></textarea>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_split_partner" />
    </form>
</div>
<script>
    $("#UserImageButton").PictureCut({
        InputOfImageDirectory: "image",
        PluginFolderOnServer: "/jquery.picture.cut/",
        FolderOnServer: "/uploads/",
        EnableCrop: true,
        CropWindowStyle: "Bootstrap",
    });
</script>
<?php include_once "member_popup_footer.php"; ?>