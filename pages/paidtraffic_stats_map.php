<?php
$pagetitle = "PaidTraffic Stats";
$pageurl = "paidtraffic/stats";
$modulename = "paid-traffic";

if(!isset($_GET["PaidTrafficID"]))
    site_redirect("paidtraffic/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->paidtraffics." where id=".$_GET["PaidTrafficID"]." and userid=".$LoggedInUser->id;
$paidtraffic = DB()->get_row($sql);
if(!$paidtraffic)
    site_redirect("paidtraffic/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$paidtraffic->DomainID);
$paidtrafficurl = get_paidtrafficurl($paidtraffic);

$PaidTrafficID = $_GET["PaidTrafficID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";

$MenuItem = get_menuitem($pageurl);

$sql = "select ".DB()->paidtraffic_clicks.".ClickIp from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$paidtraffic->id;
if(isset($_GET['startdate']) && isset($_GET['enddate']))
    $sql .= " and (DateAdded >= '".strtotime($_GET['startdate']." 00:00:00")."' and DateAdded <= '".strtotime($_GET['enddate']." 24:00:00")."') ";
$IpAddresses = DB()->get_col($sql);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $paidtraffic->PaidTrafficName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic/stats")?>" role="button">PaidTraffic Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-paidtraffic-conv" href="<?php site_url("paidtraffic/stats/conv")?>" role="button">Paid Traffic Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>PaidTraffic Tracking Stats Details</h1>
            </div>
        </div>
        <br />
        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("paidtraffic/stats/".$paidtraffic->id."/details")?>"><?php echo $paidtraffic->PaidTrafficName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $paidtraffic->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtrafficurl;?>">Tracking Link: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtrafficurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtrafficurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtraffic->DestinationURL;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtraffic->DestinationURL;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtraffic->DestinationURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div id="paidtraffic_statsdaterange" class="paidtraffic_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("paidtraffic/stats/".$_GET["PaidTrafficID"]."/map/");?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Tracking Stats Map</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body map-box-body-">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="map_canvas" class="embed-responsive-item" style="border: 1px solid #f4f4f4" data-map-image="<?php site_url('images/map_marker_inside_chartreuse.png');?>" data-map-simple="true"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<link href="<?php site_url("css/jquery-jvectormap-2.0.3.css");?>" rel="stylesheet" type="text/css" />
<script src="<?php site_url("js/jquery-jvectormap-2.0.3.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/jquery-jvectormap-world-merc-en.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/gdp-data.js");?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo "IpAddresses = ['".implode("','",  $IpAddresses)."'];\n";?>
        CreateMap($('#map_canvas'));
        AddMarker();
        CreateDateRange($("#paidtraffic_statsdaterange"));
    });
</script>
<?php
include_once "member_footer.php";
?>