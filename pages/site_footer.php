<div class="container">
    <footer class="site-footer">
        <div class="row">
            <div class="col-sm-12">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.2.0
                </div>
                <strong>Copyright &copy; 2020 <a href="http://chadnicely.com">Chad Nicely</a>.</strong> All rights reserved.
            </div>
        </div>
    </footer>
</div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php site_url("js/bootstrap.min.js");?>"></script>

<!-- iCheck -->
<script src="<?php site_url("js/icheck.min.js");?>" type="text/javascript"></script>

<!-- Select2 -->
<script src="<?php site_url("js/select2.min.js");?>"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?php site_url("js/moment.min.js");?>"></script>
<script type="text/javascript" src="<?php site_url("js/daterangepicker.js");?>"></script>

<!-- SlimScroll -->
<script src="<?php site_url("js/jquery.slimscroll.min.js");?>" type="text/javascript"></script>

<!-- FastClick -->
<script src="<?php site_url("js/fastclick.min.js");?>" type="text/javascript"></script>

<!-- Validator -->
<script src="<?php site_url("js/validator.js");?>" type="text/javascript"></script>

<!-- clipboard -->
<script src="<?php site_url("js/clipboard.min.js");?>" type="text/javascript"></script>

<!-- jQuery Knob -->
<script src="<?php site_url("js/jquery.knob.js");?>" type="text/javascript"></script>

<!-- ChartJs Knob -->
<script src="<?php site_url("js/Chart.min.js");?>" type="text/javascript"></script>

<!-- custom scrollbar plugin -->
<script src="<?php site_url("js/jquery.mCustomScrollbar.concat.min.js");?>" type="text/javascript"></script>

<!-- jquery picture cut plugin -->
<script id="picture_element_css_to_bootstrap" src="<?php site_url("js/jquery.picture.cut.js");?>" type="text/javascript"></script>

<!-- bootstrap tokenfield plugin -->
<script src="<?php site_url("js/bootstrap-tokenfield.min.js");?>" type="text/javascript"></script>

<!-- Datatable plugin -->
<script src="<?php site_url("js/dataTables/jquery.dataTables.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/dataTables.bootstrap.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/responsive/dataTables.responsive.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/dataTables.buttons.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.bootstrap.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.print.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.html5.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/buttons.colVis.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/pdfmake.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/vfs_fonts.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/dataTables/buttons/jszip.min.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/easylinks.js");?>" type="text/javascript"></script>

<script src="<?php site_url("js/bootstrap.modelfix.js");?>"></script>

<!-- AdminLTE App -->
<script src="<?php site_url("js/app.min.js");?>" type="text/javascript"></script>

</body>
</html>