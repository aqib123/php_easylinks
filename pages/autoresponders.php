<?php
$pagetitle = "Autoresponders";
$pageurl = "vendors";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php echo $MenuItem->MenuItemURL?>"><i class="<?php echo $MenuItem->MenuItemClass;?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn-bordered" href="<?php site_url("vendors/")?>" role="button">Profiles</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/")?>" role="button">Groups</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("pixels/")?>" role="button">Pixels</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-gray" href="<?php site_url("autoresponders/")?>" role="button">Autoresponders</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Manage Groups</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="AutoresponderID" class="col-xs-12 control-label">Autoresponder</label>
                                            <div class="col-xs-12 ">
                                                <select name="AutoresponderID" id="AutoresponderID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".DB()->autoresponders." where AutoresponderType='Email' and userid = ".$LoggedInUser->id." and parentid=0 order by AutoresponderName";
                                                    $autoresponders = DB()->get_results($sql);
                                                    foreach($autoresponders as $autoresponder){
                                                    ?>
                                                    <option value="<?php echo $autoresponder->id;?>"><?php echo $autoresponder->AutoresponderName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-widget="ShowLinkModel" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&AutoresponderID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-values="act=delete_autoresponder&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#AutoresponderID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="AutoresponderID" class="col-xs-12 control-label">Autoresponder List</label>
                                            <div class="col-xs-12 ">
                                                <select name="AutoresponderID" id="AutoresponderListID" class="form-control select2" style="width: 100%" data-select="select">
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&ParentID=")?>" data-append="true" data-append-object="#AutoresponderID" data-widget="ShowLinkModel" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email&AutoresponderID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("autoresponder/?js=true&nodefault=true&Type=Email")?>" data-values="act=delete_autoresponder&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#AutoresponderListID" data-method="post" data-page-title="Autoresponder Manager" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>

                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    /// <reference path="_references.js" />
    $(document).ready(function () {
        $("#AutoresponderID").on("change", function (e) {
            var url = "<?php site_url("autoresponders/")?>";
            $this = $(this);
            if ($this.val() != null && $this.val() != "") {
                var AutoresponderID = $this.val();
                url += "?act=get_autoresponder&AutoresponderID=" + AutoresponderID;
                $.get(url, "", function (data) {
                    var $AutoresponderListID = $("#AutoresponderListID");
                    var jsonData = $.parseJSON(data);

                    $AutoresponderListID.before(jsonData.before);
                    $AutoresponderListID.html(jsonData.value);
                    $AutoresponderListID.select2({
                        minimumResultsForSearch: Infinity
                    });

                    $AutoresponderListID.after(jsonData.after);
                    eval(jsonData.jscode);
                });
            }
        });
        $("#AutoresponderID").trigger("change");
    })
</script>
<?php
include_once "member_footer.php";
?>