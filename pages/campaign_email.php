<?php
$modulename = "email-campaigns";
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['EmailID'])){
    $EmailID = parseInt($_GET['EmailID']);
    $sql = "select * from ".DB()->campaign_emails." where userid = ".$LoggedInUser->id." and id=".$EmailID;
    $email = DB()->get_row($sql);
    if($email)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form" role="form">
        <div class="form-group has-feedback">
            <label for="SendTime" class="col-sm-3 control-label">Send Time</label>
            <div class="col-sm-9">
                <input type="text" value="<?php if($isUpdateForm) echo $email->SendTime;?>" class="form-control " id="SendTime" name="SendTime" placeholder="# Days" required="required" <?php //echo INT_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="EmailSubject" class="col-sm-3 control-label">Subject Line</label>
            <div class="col-sm-9">
                <input type="text" value="<?php if($isUpdateForm) echo $email->EmailSubject;?>" class="form-control " id="EmailSubject" name="EmailSubject" placeholder="Enter Your Subject Line" required="required" <?php //echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group select-form-group">
            <label for="LinkBankID" class="col-sm-3 control-label">Easylinks</label>
            <div class="col-sm-9 ">
                <div class="input-group- right-addon-">
                    <select name="LinkBankID" id="LinkBankID" class="form-control select2" style="width: 100%" data-select="select">
                        <?php
                        $userid = $LoggedInUser->id;
                        $sql = "select * from ".DB()->linkbanks." where (LinkStatus = 'active' or LinkStatus = 'evergreen' or LinkStatus = 'mylink') and userid=".$userid." order by LinkName asc";
                        $linkbanks = DB()->get_results($sql);
                        foreach($linkbanks as $linkbank){
                        ?>
                        <option value="<?php echo $linkbank->id;?>" <?php if(($isUpdateForm && $email->LinkBankID == $linkbank->id) || (isset($_POST['LinkBankID']) && $_POST['LinkBankID'] == $linkbank->id)) echo ' selected = "selected" ';?>><?php echo $linkbank->LinkName;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group select-form-group">
            <label for="" class="col-sm-3 control-label"></label>
            <div class="col-sm-9 ">
                <label>
                    <input type="checkbox" name="SetWidth" value="1" class="blue" <?php if(($isUpdateForm && $email->SetWidth == 1) || isset($_POST['SetWidth'])) echo ' checked = "checked" ';?> />
                    &nbsp; Set Width
                </label>
            </div>
        </div>

        <div class="form-group has-feedback">
            <div class="col-sm-12">
                <textarea class="ckeditor" name="EmailBody" id="EmailBody"><?php if($isUpdateForm) echo $email->EmailBody;?></textarea>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="GroupParentID" class="col-sm-2 control-label no-gutter">Auto Responder</label>
            <div class="col-sm-4">
                <select name="AutoresponderParentID" id="AutoresponderParentID" class="form-control select2" style="width: 100%">
                    <?php
                    $sql = "select * from ".DB()->autoresponders." where AutoresponderType='Email' and userid = ".$LoggedInUser->id." and parentid=0 order by AutoresponderName";
                    $autoresponders = DB()->get_results($sql);
                    foreach($autoresponders as $autoresponder){
                    ?>
                    <option value="<?php echo $autoresponder->id;?>"><?php echo $autoresponder->AutoresponderName;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>

            <label for="AutoresponderID" class="col-sm-2 control-label no-gutter">Choose List</label>
            <div class="col-sm-4">
                <select name="AutoresponderID" id="AutoresponderID" class="form-control select2" style="width: 100%" data-select="select">
                </select>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group select-form-group">
            <label for="GroupID" class="col-sm-2 no-gutter control-label">Email Type</label>
            <div class="col-sm-4 ">
                <select name="EmailTypeID" id="EmailTypeID" class="form-control select2" style="width: 100%" data-select="select">
                    <?php
                    $sql = "select * from ".DB()->groups." where GroupType='EmailType' and userid = ".$LoggedInUser->id." order by GroupName";
                    $emailtypes = DB()->get_results($sql);
                    foreach($emailtypes as $emailtype){
                    ?>
                    <option value="<?php echo $emailtype->id;?>" <?php if(($isUpdateForm && $email->EmailTypeID == $emailtype->id) || (isset($_POST['EmailTypeID']) && $_POST['EmailTypeID'] == $emailtype->id)) echo ' selected = "selected" ';?>><?php echo $emailtype->GroupName;?></option>
                    <?php
                    }
                    ?>
                </select>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_email" />
    </form>
</div>
<script>
    /// <reference path="_references.js" />
    $("#AutoresponderParentID").on("change", function (e) {
        var url = "<?php site_url("campaign/email/".$_GET["CampaignID"]."/")?>";
        $this = $(this);
        if ($this.val() != null && $this.val() != "") {
            var AutoresponderID = $this.val();
            url += "?act=get_autoresponder&AutoresponderID=" + AutoresponderID;
            $.get(url, "", function (data) {
                var $AutoresponderID = $("#AutoresponderID");
                var jsonData = $.parseJSON(data);

                $AutoresponderID.before(jsonData.before);
                $AutoresponderID.html(jsonData.value);
                $AutoresponderID.select2({
                    minimumResultsForSearch: Infinity
                });

                $AutoresponderID.after(jsonData.after);
                eval(jsonData.jscode);
            });
        }
    });
    $("#AutoresponderParentID").trigger("change");

    $('select.select2').select2({
        minimumResultsForSearch: Infinity
    });
</script>
<?php include_once "member_popup_footer.php"; ?>