<?php
$modulename = "master-campaign";
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['MasterCampaignID'])){
    $MasterCampaignID = parseInt($_GET['MasterCampaignID']);
    $sql = "select * from ".DB()->master_campaigns." where userid = ".$LoggedInUser->id." and id=".$MasterCampaignID;
    $MasterCampaign = DB()->get_row($sql);
    if($MasterCampaign)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form">
        <div class="form-group has-feedback">
            <label for="MasterCampaignName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $MasterCampaign->MasterCampaignName;?>" class="form-control " tabindex="0" id="MasterCampaignName" name="MasterCampaignName" placeholder="Master Campaign Name" required="required" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>
        
        <div class="form-group has-feedback select-form-group">
            <label for="MasterCampaignStatus" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8">
                <select id="MasterCampaignStatus" name="MasterCampaignStatus" class="form-control select2" tabindex="1">
                    <!--<option value="pending" <?php if($isUpdateForm && $MasterCampaign->MasterCampaignStatus == "pending") echo ' selected="selected" '?>>Pending</option>-->
                    <option value="active" <?php if($isUpdateForm && $MasterCampaign->MasterCampaignStatus == "active") echo ' selected="selected" '?>>Active</option>
                    <option value="complete" <?php if($isUpdateForm && $MasterCampaign->MasterCampaignStatus == "complete") echo ' selected="selected" '?>>Completed</option>
                </select>
            </div>
        </div>

        <input type="hidden" name="act" value="save_master_campaign" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>