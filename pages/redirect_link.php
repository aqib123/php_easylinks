<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['RedirectLinkID'])){
    $RedirectLinkID = parseInt($_GET['RedirectLinkID']);
    $sql = "select * from ".DB()->redirect_links." where userid = ".$LoggedInUser->id." and id=".$_GET['RedirectLinkID'];
    $redirect_link = DB()->get_row($sql);
    if($redirect_link)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form" data-toggle="validator" role="form">
        <div class="form-group has-feedback">
            <label for="RedirectLinkName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $redirect_link->RedirectLinkName;?>" class="form-control " id="RedirectLinkName" name="RedirectLinkName" placeholder="Link Name" required="required" <?php echo NAME_PATTERN;?> />
                <small class="help-block text-green text-xs">Enter URL without the http:// and short description</small>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="RedirectLinkURL" class="col-sm-4 control-label">URL</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $redirect_link->RedirectLinkURL;?>" class="form-control " id="RedirectLinkURL" name="RedirectLinkURL" placeholder="Link URL" required="required" <?php echo URL_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_redirect_link" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>