<?php
$pagetitle = "Rotator Stats";
$pageurl = "rotator/stats";
$modulename = "rotator";

if(!isset($_GET["RotatorID"]))
    site_redirect("rotator/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->rotators." where id=".$_GET["RotatorID"]." and userid=".$LoggedInUser->id;
$rotator = DB()->get_row($sql);
if(!$rotator)
    site_redirect("rotator/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$rotator->DomainID);
$rotatorurl = get_rotatorurl($rotator);

$RotatorID = $_GET["RotatorID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$rotator_stats = get_rotators_stats($RotatorID, $startdate, $enddate);

$topchartdata = Rotator_SnapShot($RotatorID, $rotator_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$MenuItem = get_menuitem($pageurl);

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
$rotator_link = get_rotatorurl($rotator);

$sql = "select ".DB()->rotator_clicks.".ClickIp from ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id where ".DB()->rotator_links.".RotatorID=".$rotator->id." ";

if(isset($_GET['startdate']) && isset($_GET['enddate']))
    $sql .= " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($_GET['startdate']." 00:00:00")."' and ".DB()->rotator_clicks.".DateAdded <= '".strtotime($_GET['enddate']." 24:00:00")."') ";

if(isset($_GET['RotatorLinkID']) && !empty($_GET['RotatorLinkID']))
    $sql .= " and ".DB()->rotator_clicks.".RotatorLinkID = ".$_GET['RotatorLinkID']." ";

$IpAddresses = DB()->get_col($sql);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("rotator/".$rotator->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $rotator->RotatorName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("rotator/stats/".$RotatorID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Rotator Tracking Stats Details</h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Rotator Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("rotator/".$rotator->id)?>"><?php echo $rotator->RotatorName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $rotator->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $rotator->RotatorType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $rotator_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $rotator_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $rotator_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="rotator_statslinks pull-left">
                        <select name="RotatorLinkID" id="RotatorLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".DB()->rotator_links." where RotatorID=".$RotatorID." and userid = ".$LoggedInUser->id." order by RotatorLinkPosition";
                            $rotator_links = DB()->get_results($sql);
                            foreach($rotator_links as $rotator_link){
                                if ($rotator_link->RotatorLinkType == "customurl")
                                	$RotatorLinkURL = $rotator_link->RotatorLinkURL;
                                else
                                    $RotatorLinkURL = get_linkbankurl($rotator_link->LinkBankID);
                            ?>
                            <option value="<?php echo $rotator_link->id;?>" <?php if(isset($_GET['RotatorLinkID']) && $_GET['RotatorLinkID'] == $rotator_link->id) echo ' selected = "selected" ';?>><?php echo $RotatorLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="rotator_statsdaterange" class="rotator_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("rotator/stats/".$_GET["RotatorID"]."/details/?".(isset($_GET['RotatorLinkID'])?"RotatorLinkID=".$_GET['RotatorLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Rotator Tracking Stats Map</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body map-box-body-">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="map_canvas" class="embed-responsive-item" style="border: 1px solid #f4f4f4" data-map-image="<?php site_url('images/map_marker_inside_chartreuse.png');?>" data-map-simple="true"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<link href="<?php site_url("css/jquery-jvectormap-2.0.3.css");?>" rel="stylesheet" type="text/css" />
<script src="<?php site_url("js/jquery-jvectormap-2.0.3.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/jquery-jvectormap-world-merc-en.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/gdp-data.js");?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo "IpAddresses = ['".implode("','",  $IpAddresses)."'];\n";?>
        CreateMap($('#map_canvas'));
        AddMarker();
        CreateDateRange($("#rotator_statsdaterange"));

        $("#RotatorLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("rotator/stats/".$_GET["RotatorID"]."/map/?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "RotatorLinkID=" + $this.val();
            }
            window.location.href = url;
        });
    });
</script>
<?php
include_once "member_footer.php";
?>