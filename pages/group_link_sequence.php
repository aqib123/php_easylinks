<?php
$pagetitle = "Groups";
$pageurl = "group/rotator";
$modulename = "link-sequence";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/linkbank/")?>" role="button">Link Bank</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/paidtraffics/")?>" role="button">Paid Traffic</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/email/")?>" role="button">Email Campaigns</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/rotator/")?>" role="button">Rotator Groups</a>
                <a class="btn btn-flat btn-gray" href="<?php site_url("group/link-sequence/")?>" role="button">Link Sequence</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/pixels/")?>" role="button">Retargeting Pixels</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/domains/")?>" role="button">Domains</a>
                <a class="btn btn-flat btn-bordered" href="<?php site_url("group/holding-pages/")?>" role="button">Holding Pages</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Link Sequence Groups</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-900 container-white">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-gray- box-solid- box-border-doted">
                                <div class="box-body box-top-padded">
                                    <div class="row">
                                        <div class="col-sm-6 margin-bottom-20">
                                            <label for="inputEmail3" class="col-xs-12 control-label">Link Sequence Group</label>
                                            <div class="col-xs-12 ">
                                                <select name="GroupID" id="LinkSequenceGroupID" class="form-control select2" style="width: 100%" data-select="select">
                                                    <?php
                                                    $sql = "select * from ".DB()->groups." where GroupType='LinkSequence' and userid = ".$LoggedInUser->id." order by GroupName";
                                                    $groups = DB()->get_results($sql);
                                                    foreach($groups as $group){
                                                    ?>
                                                    <option value="<?php echo $group->id;?>"><?php echo $group->GroupName;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-12" style="margin-top: 12px">
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkSequence")?>" data-widget="ShowLinkModel" data-related="#LinkSequenceGroupID" data-method="post" data-page-title="Link Sequence Group" class="btn btn-link btn-bordered btn-sm">Add New</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkSequence&GroupID=")?>" data-append="true" data-widget="ShowLinkModel" data-related="#LinkSequenceGroupID" data-method="post" data-page-title="Link Sequence Group" class="btn btn-link btn-bordered btn-sm">Edit Selected</a>
                                                <a href="<?php site_url("group/?js=true&nodefault=true&GroupType=LinkSequence")?>" data-values="act=delete_group&GroupID=" data-append="true" data-widget="RemoveOption" data-related="#LinkSequenceGroupID" data-method="post" data-page-title="Link Sequence Group" class="btn btn-link btn-bordered btn-sm">Delete Selected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>