<?php
$PermissionMessage = get_option("Permission Message");
$PermissionMessage = preg_replace(array('/{back-url}/', '/{upgrade-link}/'), array(get_site_url("dashboard"), ($CurrentLevel->LevelID==3?"http://easylinks.ninja/pro/":"http://easylinks.ninja/marketer/")), $PermissionMessage);
?>
<div class="content-wrapper white-bg">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <?php echo $PermissionMessage;?>
            </div>
        </div>
    </section>
</div>
<?php
include_once "member_footer.php";
die;