<?php
$bodyclasses = ' login-page ';
$pagetitle = ' Register ';
include_once "site_header.php";
?>
<div class="register-box">
    <!-- /.login-logo -->
    <div class="register-box-body">
        <p class="register-box-msg page-header">
            Welcome to the Dashboard
                <small>Sign in to start your session</small>
        </p>
        <form class="form-horizontal easylink-form register-form" method="post" data-toggle="custom-validator" role="form" action="">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="User Name" required="required" <?php echo ALPHANUMERIC_PATTERN;?> />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="repassword" class="form-control" placeholder="Retype Password" data-match="#password" required="required" <?php echo ALLALLOWED_PATTERN;?> />
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email Address" required="required" <?php echo EMAIL_PATTERN;?> />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="displayname" class="form-control" placeholder="Displayname" required="required" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <small class="help-block with-errors"></small>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-block btn-flat btn-primary"><span class="text-uppercase">Register <i class="ion-log-in"></i></span></button>
                    <input type="hidden" name="act" value="register_user" />
                </div>
            </div>
        </form>
        <br />

        <div class="row">
            <div class="col-sm-12 text-center">
                <a class="text-center" href="<?php site_url("login");?>">I already have a membership</a>
            </div>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
<script src="<?php site_url("js/register.js");?>" type="text/javascript"></script>
<?php
include_once "site_footer.php";
?>