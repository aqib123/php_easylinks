<?php
include_once "../includes/functions.php";
if(!elUsers()->is_user_loggedin())
    redirect(get_site_url("login"));


$CurrentLevel = $LoggedInUser->Levels[count($LoggedInUser->Levels) - 1];

//if(!isset($modulename) || !elUsers()->is_module_enabled($modulename)){
//    $PermissionMessage = get_option("Permission Message");
//    $PermissionMessage = preg_replace(array('/{back-url}/', '/{upgrade-link}/'), array(get_site_url("dashboard"), ($CurrentLevel->LevelID==3?"http://easylinks.ninja/pro/":"http://easylinks.ninja/marketer/")), $PermissionMessage);
//    include_once 'site_header.php';
//    echo "<div class='container'><div class='row'><div class='sol-sm-12'>".$PermissionMessage."</div></div></div>";
//    include_once 'site_footer.php';
//    die;
//}

//if(SYS()->is_valid($curpage)){
//    SYS()->set_page_info($curpage);
//}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title><?php echo SITE_TITLE;?> | <?php echo $pagetitle;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link rel="apple-touch-icon" sizes="57x57" href="<?php site_url("images/favicons/apple-icon-57x57.png");?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php site_url("images/favicons/apple-icon-60x60.png");?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php site_url("images/favicons/apple-icon-72x72.png");?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php site_url("images/favicons/apple-icon-76x76.png");?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php site_url("images/favicons/apple-icon-114x114.png");?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php site_url("images/favicons/apple-icon-120x120.png");?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php site_url("images/favicons/apple-icon-144x144.png");?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php site_url("images/favicons/apple-icon-152x152.png");?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php site_url("images/favicons/apple-icon-180x180.png");?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php site_url("images/favicons/android-icon-192x192.png");?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php site_url("images/favicons/favicon-32x32.png");?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php site_url("images/favicons/favicon-96x96.png");?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php site_url("images/favicons/favicon-16x16.png");?>">
    <link rel="manifest" href="<?php site_url("images/favicons/manifest.json");?>">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php site_url("css/bootstrap.min.css");?>" />

    <!-- Font Awesome Icons -->
    <link href="<?php site_url("css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->
    <link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="<?php site_url("css/AdminLTE.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link href="<?php site_url("css/skin-black-light.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php site_url("css/blue.css");?>" />

    <!-- Select2 -->
    <link href="<?php site_url("css/select2.min.css");?>" rel="stylesheet" />

    <!-- Include Date Range Picker -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/daterangepicker.css");?>" />

    <!-- custom scrollbar stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery.mCustomScrollbar.min.css");?>" />

    <!-- qtip2 stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery.qtip.min.css");?>" />

    <!-- Datatables stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/dataTables.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/responsive/responsive.bootstrap.min.css");?>" />
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/dataTables/buttons/buttons.bootstrap.min.css");?>" />

    <link href="<?php site_url("css/style.css");?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php site_url("js/jquery-1.11.3.min.js");?>"></script>

    <!-- jQueryUI -->
    <script src="<?php site_url("js/jquery-ui.min.js");?>"></script>

    <!-- jQueryUI stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/jquery-ui-1.10.0.custom.css");?>" />

    <!-- bootstrap tokenfield plugin -->
    <link rel="stylesheet" type="text/css" href="<?php site_url("css/bootstrap-tokenfield.min.css");?>" />

    <script>
        var siteurl = '<?php site_url("");?>';
        var ajaxURL = '<?php site_url("ajax/");?>';
        var userid = '<?php echo $LoggedInUser->id;?>';
        var username = '<?php echo $LoggedInUser->user_login;?>';
        var defaultpagelength = parseInt('<?php echo DEFAULTPAGELENGTH; ?>');
        var defaultpages = parseInt('<?php echo DEFAULTPAGES; ?>');
        var usertimezone = '<?php echo $LoggedInUser->timezoneid;?>';
    </script>
</head>
<body class="skin-black-light sidebar-mini <?php echo isset($bodyclasses)?$bodyclasses:"";?> ">

    <div id="EasyLinksModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="EasyLinksModelTitle">Modal title</h4>
                </div>
                <div class="modal-body">
                    asdf
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="EasyLinksModelSave">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="EasyLinksWaitModel" class="modal fade" role="dialog" aria-labelledby="EasyLinksWaitModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Please wait...</h4>
                </div>
                <div class="modal-body">
                    <div class="progress progress-sm active">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">100%</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?php site_url("");?>" class="logo hidden-xs">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                    <img src="<?php site_url($CurrentLevel->LevelLogo);//site_url("images/easylinks_logo.png")?>" class="easylinks_logo" title="Easy Links" />
                </span>

            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <span class="logo-mini visible-xs-inline-block">
                    <img src="<?php site_url($CurrentLevel->LevelLogo);//site_url("images/easylinks_logo.png")?>" class="easylinks_logo" title="Easy Links" />
                </span>

                <?php if(isset($pageurl) && $pageurl == "linkbank/stats") {?>
                <form action="<?php site_url($curpage);?>" method="post" class="form-inline mini-search-form visible-xs-inline-block">
                    <div class="form-group">
                        <label class="sr-only" for="searchlinkbank">Search Link Bank</label>
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" id="linkbank_keywords" name="keywords" placeholder="Search Link Bank" value="<?php echo isset($_POST["keywords"]) ? $_POST["keywords"] : "";?>">
                            <div class="input-group-addon">
                                <button type="submit" class="btn btn-block"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="act" value="search_linkbank" />
                    <a class="btn-close-form"><i class="fa fa-times"></i></a>
                </form>
                <?php } ?>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php site_url(!empty($LoggedInUser->user_picture)?$LoggedInUser->user_picture:"images/avatar5.png");?>" class="user-image" alt="User Image" />
                                <span class="hidden-xs"><?php echo $LoggedInUser->display_name;?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php site_url(!empty($LoggedInUser->user_picture)?$LoggedInUser->user_picture:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $LoggedInUser->display_name;?>
                                        <small>Member since <?php echo date("M, Y", strtotime($LoggedInUser->user_registered));?></small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php site_url("user-profile")?>" class="btn btn-default btn-flat">Update Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php site_url("change-password/?js=true")?>" data-widget="ShowLinkModel" data-method="post" data-page-title="Password Manager" class="btn btn-default btn-flat">Change Password</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <?php if(isset($pageurl) && $pageurl == "linkbank/stats") {?>
                <a class="visible-xs-inline-block mobile-search-icon" href="javascript:">
                    <i class="fa fa-search"></i>
                </a>
                <?php } ?>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar linear_gradient-">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php site_url(!empty($LoggedInUser->user_picture)?$LoggedInUser->user_picture:"images/avatar5.png");?>" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p>
                            <small class="center-block">Welcome</small>
                            <span><?php echo $LoggedInUser->display_name;?></span>
                        </p>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <?php
                    $colors = array();
                    $MenuItems = $LoggedInUser->MenuItems;
                    foreach($MenuItems as $MenuItem){
                        if($MenuItem->MenuItemParentID == "0"){
                            $color = get_random_rbg($colors);//
                            $colors[] = $color;
                            $MenuItem->color = $color;
                            $submenu_items = DB()->get_results("select * from ".DB()->menu_items." where MenuItemEnabled = 1 and MenuItemParentID=".$MenuItem->id, ARRAY_A);
                            $has_active_submenu_items = DB()->get_var("select count(*) from ".DB()->menu_items." where MenuItemParentID=".$MenuItem->id." and MenuItemURL='".$pageurl."'") * 1;
					?>
                    <li class="<?php if($MenuItem->MenuItemURL == $pageurl || $has_active_submenu_items > 0) echo "active";?>  <?php if(count($submenu_items) > 0) echo "treeview";?>">
                        <a href="<?php site_url($MenuItem->MenuItemURL)?>">
                            <i class="<?php echo $MenuItem->MenuItemClass?>" style="color: <?php echo $MenuItem->color;?>;"></i>
                            <span><?php echo $MenuItem->MenuItemLabel?></span>
                        </a>
                        <?php
                            if (count($submenu_items) > 0){
						?>
                        <ul class="treeview-menu">
                            <?php
                                foreach ($submenu_items as $submenu_item){
                                    $color = get_random_rbg($colors);//
                                    $colors[] = $color;
                                    $submenu_item["color"] = $color;
							?>
                            <li class="<?php if($submenu_item['MenuItemURL'] == $pageurl) echo "active";?>">
                                <a href="<?php site_url($submenu_item['MenuItemURL'])?>">
                                    <i class="<?php echo $submenu_item['MenuItemClass']?>" style="color: <?php echo $submenu_item["color"];?>;"></i>
                                    <span><?php echo $submenu_item['MenuItemLabel']?></span>
                                </a>
                            </li>
                            <?php
                                }
							?>
                        </ul>
                        <?php
                            }
						?>
                    </li>
                    <?php    
                        }
                    }
					?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

   <?php
   if($LoggedInUser->force_username_change == 1 || $LoggedInUser->force_email_change == 1 || $LoggedInUser->force_password_change == 1){
       include_once 'force_change.php';
   }

   if(!isset($modulename) || !elUsers()->is_module_enabled($modulename)){
       include_once 'permission_error.php';
   }
   ?>