<?php
include_once "member_popup_header.php";

$isUpdateForm = false;
if($_GET && isset($_GET['GroupID'])){
    $GroupID = parseInt($_GET['GroupID']);
    $sql = "select * from ".DB()->groups." where userid = ".$LoggedInUser->id." and id=".$_GET['GroupID'];
    $group = DB()->get_row($sql);
    if($group)
        $isUpdateForm = true;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form" data-toggle="validator" role="form">
        <div class="form-group has-feedback">
            <label for="GroupName" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" value="<?php if($isUpdateForm) echo $group->GroupName;?>" class="form-control " id="GroupName" name="GroupName" placeholder="Group Name" required="required" <?php echo NAME_PATTERN;?> />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <input type="hidden" name="act" value="save_group" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>