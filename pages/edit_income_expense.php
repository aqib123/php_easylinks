<?php
include_once "member_popup_header.php";
$type = ucwords($_GET["type"]);
$table = $type == "Income"?DB()->manualsale_incomes:($type == "OtherIncome"?DB()->manualsale_other_incomes:DB()->manualsale_expenses);
$id = $_GET["id"];

$isUpdateForm = false;
if($_GET && is_numeric($id)){
    $sql = "select * from ".$table." where id=".$id." and userid=".$LoggedInUser->id;
    $row = DB()->get_row($sql);
    if($row)
        $isUpdateForm = true;

    $NameField = $type.Name;
    $AmountField = $type.Amount;
    $NoteField = $type.Note;
}
?>
<div class="container-fluid">
    <form class="form-horizontal easylink-form domain-form">
        <div class="form-group has-feedback">
            <label for="<?php echo $type;?>Name" class="col-sm-4 control-label"><?php echo $type;?> Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control"  value="<?php if($isUpdateForm) echo $row->$NameField;?>" id="<?php echo $type;?>Name" name="<?php echo $type;?>Name" placeholder="<?php echo $type;?> Name" required="required" <?php echo NAME_PATTERN;?>>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="<?php echo $type;?>Amount" class="col-sm-4 control-label"><?php echo $type;?> Amount</label>
            <div class="col-sm-8">
                <input type="text" class="form-control"  value="<?php if($isUpdateForm) echo $row->$AmountField;?>" id="<?php echo $type;?>Amount" name="<?php echo $type;?>Amount" placeholder="<?php echo $type;?> Amount" required="required" <?php echo FLOAT_PATTERN;?>>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>

        <div class="form-group has-feedback">
            <label for="<?php echo $type;?>Note" class="col-sm-4 control-label"><?php echo $type;?> Note</label>
            <div class="col-sm-8">
                <textarea type="text" class="form-control" id="<?php echo $type;?>Note" name="<?php echo $type;?>Note" placeholder="<?php echo $type;?> Note"><?php if($isUpdateForm) echo $row->$NoteField;?></textarea>
            </div>
        </div>

        <input type="hidden" name="act" value="update_<?php echo $_GET["type"]?>" />
    </form>
</div>
<?php include_once "member_popup_footer.php"; ?>