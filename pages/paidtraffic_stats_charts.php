<?php
$pagetitle = "PaidTraffic Stats Chart";
$pageurl = "paidtraffic/stats";
$modulename = "paid-traffic";

if(!isset($_GET["PaidTrafficID"]))
    site_redirect("paidtraffic/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->paidtraffics." where id=".$_GET["PaidTrafficID"]." and userid=".$LoggedInUser->id;
$paidtraffic = DB()->get_row($sql);
if(!$paidtraffic)
    site_redirect("paidtraffic/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$paidtraffic->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$paidtraffic->DomainID);
$paidtrafficurl = get_paidtrafficurl($paidtraffic);

$default_dates = get_last_seven_days();//get_today_dates();
$PaidTrafficID = $_GET["PaidTrafficID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:(isset($_GET["showall"])?'':$default_dates["start_date"]);
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:(isset($_GET["showall"])?'':$default_dates["end_date"]);
$paidtraffic_stats = get_paidtraffic_stats($PaidTrafficID, $startdate, $enddate);

$MenuItem = get_menuitem($pageurl);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("paidtraffic/?PaidTrafficID=".$paidtraffic->id)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $paidtraffic->LinkName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic/stats")?>" role="button">PaidTraffic Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-paidtraffic-conv" href="<?php site_url("paidtraffic/stats/conv")?>" role="button">PaidTraffic Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Link Tracking Stats Charts</h1>
            </div>
        </div>
        <br />
        <div class="box box-solid box-default" id="TrackingPixel">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Campaign Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("paidtraffic/stats/".$paidtraffic->id."/details")?>"><?php echo $paidtraffic->LinkName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $paidtraffic->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtrafficurl;?>">Tracking Link: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtrafficurl;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtrafficurl;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtrafficurl;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                        <li>
                            <span class="small" title="<?php echo $paidtraffic->DestinationURL;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $paidtraffic->DestinationURL;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $paidtraffic->DestinationURL;?>" class="btn btn-link btn-small-icon" title="<?php echo $paidtraffic->DestinationURL;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div id="paidtraffic_statsdaterange" class="paidtraffic_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:(isset($_GET["showall"])?'':$default_dates['start_date_only']);?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:(isset($_GET["showall"])?'':$default_dates['end_date_only']);?>" data-url="<?php site_url("paidtraffic/stats/".$_GET["PaidTrafficID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                </div>
            </div>
        </div>


        <?php $charttypes = array("line","bar","area","pie");?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach($charttypes as $typeindex => $charttype) { ?>
                <li <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' class="active" '?>><a href="#<?php echo $charttype?>_tab" data-toggle="tab"><i class="fa fa-<?php echo $charttype?>-chart"></i></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <?php 
                $SnapShotData = PaidTraffic_SnapShot($PaidTrafficID, $paidtraffic_stats); 
                $TopBrowserData = PaidTraffic_TopBrowser($PaidTrafficID, $paidtraffic_stats); 
                $TopPlatformData = PaidTraffic_TopPlatform($PaidTrafficID, $paidtraffic_stats);
                $TopCountriesData = PaidTraffic_TopCountries($PaidTrafficID, $paidtraffic_stats);

                if(!empty($_GET['type'])){
                    $chartviewtype = isset($_GET['type'])?$_GET['type']:"Snapshot";
                    if($chartviewtype == "Snapshot"){
                        $data = $SnapShotData;//PaidTraffic_SnapShot($PaidTrafficID, $paidtraffic_stats); 
                        $chartviewtypedata = 'SnapShotData';
                    } else if($chartviewtype == "TopBrowser"){
                        $data = $TopBrowserData;//PaidTraffic_TopBrowser($PaidTrafficID, $paidtraffic_stats); 
                        $chartviewtypedata = 'TopBrowserData';
                    } else if($chartviewtype == "TopPlatform"){
                        $data = $TopPlatformData;//PaidTraffic_TopPlatform($PaidTrafficID, $paidtraffic_stats); 
                        $chartviewtypedata = 'TopPlatformData';
                    } else if($chartviewtype == "TopCountries"){
                        $data = $TopCountriesData;//PaidTraffic_TopCountries($PaidTrafficID, $paidtraffic_stats);
                        $chartviewtypedata = 'TopCountriesData';
                    }
                }
                foreach($charttypes as $typeindex => $charttype) { 
                ?>
                <div class="tab-pane <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' active " '?>" id="<?php echo $charttype?>_tab">
                    <div class="row">
                        <?php 
                    if(empty($_GET['type'])){
                        $titles = array("Snapshot", "Top Browser", "Top Platform", "Top Countries");
                        for($chartindex = 0; $chartindex < 4; $chartindex++){
                            if($chartindex == 0){
                                $data = $SnapShotData;
                                $chartviewtypedata = 'SnapShotData';
                            } else if($chartindex == 1){
                                $data = $TopBrowserData;
                                $chartviewtypedata = 'TopBrowserData';
                            } else if($chartindex == 2){
                                $data = $TopPlatformData;
                                $chartviewtypedata = 'TopPlatformData';
                            } else if($chartindex == 3){
                                $data = $TopCountriesData;
                                $chartviewtypedata = 'TopCountriesData';
                            }
                        ?>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo $titles[$chartindex];?></h3>
                                    <a class="btn btn-link btn-sm" href="<?php site_url("paidtraffic/stats/".$PaidTrafficID."/charts/".str_replace(" ", "", $titles[$chartindex])."/".$charttype."/".(isset($_GET['startdate']) && isset($_GET['enddate'])?"?startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""));?>" target="_self">Details</a>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="box-body chart-box-body">
                                    <div class="chart" style="height: 250px;">
                                        <canvas id="<?php echo $charttype."_".$titles[$chartindex];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" data-chart-view-type="<?php echo $chartviewtypedata;?>" height="250" data-chart-values='<?php //echo implode("!", $data['piedata']);?>' data-chart-labels='<?php //echo implode('!', $data['labels'])?>' data-chart-datasets='<?php //echo implode('!', $data['datasets'])?>'></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    }else{
                        ?>
                        <div class="col-md-12">
                            <div class="box-body chart-box-body-">
                                <div class="chart" style="height: 250px;">
                                    <canvas id="<?php echo $charttype."_".$_GET['chartviewtype'];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" data-chart-view-type="<?php echo $chartviewtypedata;?>" height="250" data-chart-values='<?php //echo implode("!", $data['piedata']);?>' data-chart-labels='<?php //echo implode('!', $data['labels'])?>' data-chart-datasets='<?php //echo implode('!', $data['datasets'])?>'></canvas>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    var ChartsData = {};
    <?php
    echo "ChartsData['SnapShotData'] = ".json_encode($SnapShotData).";\n";
    echo "ChartsData['TopBrowserData'] = ".json_encode($TopBrowserData).";\n";
    echo "ChartsData['TopPlatformData'] = ".json_encode($TopPlatformData).";\n";
    echo "ChartsData['TopCountriesData'] = ".json_encode($TopCountriesData).";\n";
    ?>
    $(document).ready(function () {
        CreateDateRange($("#paidtraffic_statsdaterange"));

        $(document).on('click', '[data-toggle="tab"]', function (e) {
            var $this = $(this);
            var li = $this.parent();
            var $target = $($this.attr('href'));
            if (!li.hasClass("chartadded")) {
                li.addClass("chartadded");
                ChartQueue = [];
                var CurrentChartType = '';
                $target.find(".chart_canvas").each(function () {
                    var $canvas = $(this);
                    var charttype = $canvas.attr("data-chart-type");
                    CurrentChartType = charttype;
                    ChartQueue.push($canvas);
                });

                if (CurrentChartType == "pie") {
                    ExecutePieChartQueue();
                } else if (CurrentChartType == "line") {
                    ExecuteLineChartQueue();
                } else if (CurrentChartType == "bar") {
                    ExecuteBarChartQueue();
                } else if (CurrentChartType == "area") {
                    ExecuteLineChartQueue();//ExecuteAreaChartQueue();
                }
            }
        });
        $(".nav-tabs-custom ul li.active a").trigger("click");
    });

    function LegendChange(e) {
        var $this = $(this);
        var excludes = [];
        $this.closest('.legend_checkbox').find(".checkbox").each(function () {
            var $check = $(this);
            if (!$check.is(':checked'))
                excludes.push($check.val());
        });
        var $boxbody = $this.closest(".box-body");
        var $canvas = $boxbody.find(".chart_canvas");
        var charttype = $canvas.attr("data-chart-type");
        if (charttype == "pie")
            chartlegend = CreatePieChart($canvas, excludes);
        else if (charttype == "line")
            chartlegend = CreateLineChart($canvas, excludes);
        else if (charttype == "bar")
            chartlegend = CreateBarChart($canvas, excludes);
        else if (charttype == "area")
            chartlegend = CreateAreaChart($canvas, excludes);
    };
</script>
<?php
include_once "member_footer.php";
?>