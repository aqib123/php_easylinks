<?php
$pagetitle = "Paid Traffic";
$pageurl = "paidtraffic";
$modulename = "paid-traffic";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
$AvailableDomainID = 0;
if($_GET && isset($_GET['PaidTrafficID']) && !isset($errors["msg"])){
    $PaidTrafficID = parseInt($_GET['PaidTrafficID']);
    $sql = "select * from ".DB()->paidtraffics." where userid = ".$LoggedInUser->id." and id=".$PaidTrafficID;
    $paidtraffic = DB()->get_row($sql);
    if($paidtraffic){
        $isUpdateForm = true;

        if($paidtraffic->UseAdminDomain == 1)
            $AvailableDomainID = $paidtraffic->DomainID;
    }
}
$admindomain = get_available_domain($AvailableDomainID);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $pagetitle;?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("paidtraffic")?>" role="button">Create New Paid Traffic</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("paidtraffic/stats")?>" role="button">Paid Traffic Statistics</a>
                <a class="btn btn-flat btn btn-bordered btn-paidtraffic-conv" href="<?php site_url("paidtraffic/stats/conv")?>" role="button">Paid Traffic Conversions Statistics</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2>Paid Traffic</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid container-white">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form class="form-horizontal easylink-form" method="post" data-toggle="custom-validator" role="form" action="<?php echo get_site_url("paidtraffic").(isset($_GET['PaidTrafficID'])?"?PaidTrafficID=".$_GET['PaidTrafficID']:"")?>">
                                <div class="box box-gray- box-solid- box-border-doted">
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="VendorID" class="col-sm-4 col-xs-12 control-label">Choose Paid Traffic Provider</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <div class="input-group right-addon">
                                                    <select name="VendorID" id="VendorID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Vendor</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->vendors." where VendorType='SoloProvider' and userid = ".$LoggedInUser->id." order by VendorName";
                                                        $vendors = DB()->get_results($sql);
                                                        foreach($vendors as $vendor){
                                                        ?>
                                                        <option value="<?php echo $vendor->id;?>" <?php if(($isUpdateForm && $paidtraffic->VendorID == $vendor->id) || (isset($_POST['VendorID']) && $_POST['VendorID'] == $vendor->id)) echo ' selected = "selected" ';?>><?php echo $vendor->VendorName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("vendor/?js=true&VendorType=SoloProvider")?>" data-widget="ShowLinkModel" data-related="#VendorID" data-method="post" data-page-title="Solo Providers Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="PaidTrafficName" class="col-sm-4 control-label">Paid Traffic Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $paidtraffic->PaidTrafficName; elseif(isset($_POST['PaidTrafficName'])) echo $_POST['PaidTrafficName'];?>" class="form-control " id="PaidTrafficName" name="PaidTrafficName" placeholder="Paid Traffic Name" data-remote="<?php site_url("paidtraffic/?act=check_link_name&type=paidtraffic&id=".(isset($_GET['PaidTrafficID'])?$_GET['PaidTrafficID']:"0"))?>" data-remote-error="Paid Traffic Name already exist" <?php echo NAME_PATTERN;?> required="required" />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="GroupID" class="col-sm-4 control-label">Choose Group</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="GroupID" id="GroupID" class="form-control select2" style="width: 99%" data-select="select">
                                                        <option value="">Select Group</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->groups." where GroupType='PaidTraffic' and userid = ".$LoggedInUser->id." order by GroupName";
                                                        $groups = DB()->get_results($sql);
                                                        foreach($groups as $group){
                                                        ?>
                                                        <option value="<?php echo $group->id;?>" <?php if(($isUpdateForm && $paidtraffic->GroupID == $group->id) || (isset($_POST['GroupID']) && $_POST['GroupID'] == $group->id)) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("group/?js=true&GroupType=PaidTraffic")?>" data-widget="ShowLinkModel" data-related="#GroupID" data-method="post" data-page-title="Groups Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="DestinationURL" class="col-sm-4 control-label">Destination Link</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $paidtraffic->DestinationURL; elseif(isset($_POST['DestinationURL'])) echo $_POST['DestinationURL'];?>" class="form-control " id="DestinationURL" name="DestinationURL" placeholder="Destination Link" required="required" <?php echo URL_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="PaidTrafficStatus" class="col-sm-4 control-label">Choose Status</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group" style="width: 100%">
                                                    <select name="PaidTrafficStatus" id="PaidTrafficStatus" class="form-control select2" style="width: 100%" data-select="select">
                                                        <?php
                                                        $LinkStatuses = SYS()->LinkStatuses;
                                                        foreach($LinkStatuses as $key => $value){
                                                        ?>
                                                        <option value="<?php echo $key;?>" <?php if(($isUpdateForm && $paidtraffic->PaidTrafficStatus == $key) || (isset($_POST['PaidTrafficStatus']) && $_POST['PaidTrafficStatus'] == $key) || (!$isUpdateForm && !isset($_POST['PaidTrafficStatus']) && $key == "active")) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="TrafficCostType" class="col-sm-4 control-label">Traffic Cost</label>
                                            <div class="col-sm-8 smallradio- smallcheck-">
                                                <input type="text" value="<?php if($isUpdateForm) echo $paidtraffic->TrafficCost; elseif(isset($_POST['TrafficCost'])) echo $_POST['TrafficCost'];?>" class="form-control " id="TrafficCost" name="TrafficCost" placeholder="Traffic Cost" required="required" <?php echo FLOAT_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <div style="margin-top: 12px; display: none;">
                                                    <?php
                                                    $options = array("CPC", "CPA", "Daily", "Monthly");
                                                    $checked = "";
                                                    foreach($options as $option){
                                                        if(!isset($_GET['PaidTrafficID']) && $option == $options[0])
                                                            $checked = ' checked = "checked" ';
                                                        else if(($isUpdateForm && $paidtraffic->TrafficCostType == $option) || (isset($_POST['TrafficCostType']) && $_POST['TrafficCostType'] == $option)) 
                                                            $checked = ' checked = "checked" ';
                                                        else
                                                            $checked = ' ';
                                                    ?>
                                                    <input type="radio" name="TrafficCostType" class="blue" value="<?php echo $option;?>" <?php echo $checked;?> />&nbsp;<span><?php echo $option;?></span>&nbsp;
                                                <?php } ?>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="NoOfClicks" class="col-sm-4 control-label"># Of Clicks</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $paidtraffic->NoOfClicks; elseif(isset($_POST['NoOfClicks'])) echo $_POST['NoOfClicks'];?>" class="form-control " id="NoOfClicks" name="NoOfClicks" placeholder="# Of Clicks" required="required" <?php echo INT_PATTERN ;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="DomainID" class="col-sm-4 col-xs-12 control-label">Choose Domain</label>
                                            <div class="col-sm-8 col-xs-12 ">
                                                <?php
                                                $checked = "";
                                                $AdminDomainID = "";
                                                $dropdowndisable = "";
                                                if(($isUpdateForm && $paidtraffic->UseAdminDomain == 1) || (isset($_POST['UseAdminDomain']))){
                                                    $checked = ' checked = "checked" ';
                                                    $AdminDomainID = $paidtraffic->DomainID;
                                                    $dropdowndisable = ' disabled = "disabled" ';
                                                }else{
                                                    $dropdowndisable = ' data-select="select" ';
                                                }

                                                ?>
                                                <div class="input-group right-addon">
                                                    <select name="DomainID" id="DomainID" class="form-control select2" style="width: 99%" <?php echo $dropdowndisable;?>>
                                                        <option value="">Select Domain</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->domains." where userid=".$LoggedInUser->id." and DomainType='userdomain' order by DomainName";
                                                        $domains = DB()->get_results($sql);
                                                        $domainurls = array();
                                                        foreach($domains as $domain){
                                                            $domainurls[] = '"'.$domain->id.'": {name:"'.$domain->DomainName.'", forward:"'.$domain->DomainForward.'", url:"'.$domain->DomainUrl.'", type:"'.$domain->DomainType.'"}';
                                                        ?>
                                                        <option value="<?php echo $domain->id;?>" <?php if(($isUpdateForm && $paidtraffic->DomainID == $domain->id) || (isset($_POST['DomainID']) && $_POST['DomainID'] == $domain->id)) echo ' selected = "selected" ';?>><?php echo $domain->DomainName;?></option>
                                                        <?php
                                                        }
                                                        $str_domains = 'var domains = {'.implode(',', $domainurls).'};';
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("domain/?js=true&DomainType=userdomain")?>" data-widget="ShowLinkModel" data-related="#DomainID" data-method="post" data-page-title="Domains Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                </div>
                                                <small class="help-block with-errors"></small>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="hidden" name="AdminDomainID" id="AdminDomainID" value="<?php echo $AdminDomainID;?>" />
                                                    <input type="checkbox" name="UseAdminDomain" id="UseAdminDomain" class="blue " value="1" <?php echo $checked;?> />&nbsp;
                                                    <span>Use Admin Domain</span>&nbsp;&nbsp;
                                                </div>
                                                <div class="col-xs-12 no-gutter" style="padding-top: 10px">
                                                    <input type="checkbox" name="CloakURL" id="CloakURL" class="blue " value="1" <?php if(($isUpdateForm && $paidtraffic->CloakURL == 1) || (isset($_POST['CloakURL']))) echo "checked='checked'";?> />&nbsp;
                                                    <span>Cloak URL</span>&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label for="VisibleLink" class="col-sm-4 control-label">Visible Link</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php if($isUpdateForm) echo $paidtraffic->VisibleLink; elseif(isset($_POST['VisibleLink'])) echo $_POST['VisibleLink'];?>" class="form-control " id="VisibleLink" name="VisibleLink" placeholder="Visible Link" data-remote="<?php site_url("paidtraffic/")?>" data-remote-values="getVisibleLinkValues()" data-remote-error="Visible Link for selected Domain already exist" required="required" <?php echo VISIBLELINK_PATTERN;?> />
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <small class="help-block with-errors"></small>
                                                <small class="help-block text-left text-light-blue no-margin" id="paidtrafficurl"></small>
                                            </div>
                                        </div>

                                        <div class="form-group left-addon">
                                            <label for="StartDate" class="col-sm-4 control-label">Start Date</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" data-linked="#StartDate" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $paidtraffic->StartDate); else if(isset($_POST['StartDate'])) echo $_POST['StartDate'];?>" class="form-control  input-singledate-time with-left-addon" id="StartDateInput" name="StartDateInput"/>
                                                    <input type="hidden" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $paidtraffic->StartDate); else if(isset($_POST['StartDate'])) echo $_POST['StartDate'];?>" id="StartDate" name="StartDate" />
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="EndDate" class="col-sm-4 control-label">End Date</label>
                                            <div class="col-sm-8">
                                                <div class="input-group left-addon">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" data-linked="#EndDate" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $paidtraffic->EndDate); else if(isset($_POST['EndDate'])) echo $_POST['EndDate'];?>" class="form-control  input-singledate-time with-left-addon" id="EndDateInput" name="EndDateInput" />
                                                    <input type="hidden" value="<?php if($isUpdateForm) echo date("m/d/Y h:i A", $paidtraffic->EndDate); else if(isset($_POST['EndDate'])) echo $_POST['EndDate'];?>" id="EndDate" name="EndDate" />
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="well well-dotted well-sm">
                                            <div class="form-group">
                                                <label for="TrackingCodeForActions" class="col-sm-4 control-label">Tracking Code For Actions</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control " id="TrackingCodeForActions" name="TrackingCodeForActions" placeholder="Tracking Code For Actions" cols="3"><?php if($isUpdateForm) echo $paidtraffic->TrackingCodeForActions; elseif(isset($_POST['TrackingCodeForActions'])) echo $_POST['TrackingCodeForActions'];?></textarea>
                                                    <small class="help-block text-green text-xs">Place this Tracking code just below the opening &lt;body&gt;</small>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div class="well well-dotted extra-options">
                                                    <h3 style="margin-top: 0;">Extra Options</h3>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#Retargeting" aria-expanded="false" aria-controls="Retargeting">
                                                        Retargeting Pixel
                                                    </button>
                                                    <?php if(elUsers()->is_module_enabled("paid-traffic-conv")) {?>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#ConversionPixel" aria-expanded="false" aria-controls="ConversionPixel">
                                                        Conversion Pixel
                                                    </button>
                                                    <?php } ?>
                                                    <button class="btn btn-default btn-sm hidden" type="button" data-toggle="customcollapse" data-target="#Goals" aria-expanded="false" aria-controls="Goals">
                                                        Goals
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#Notes" aria-expanded="false" aria-controls="Notes">
                                                        Notes
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#PageImageBox" aria-expanded="false" aria-controls="PageImageBox">
                                                        Page Image
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#RedirectPages" aria-expanded="false" aria-controls="RedirectPages">
                                                        Holding Pages
                                                    </button>
                                                    <button class="btn btn-default btn-sm" type="button" data-toggle="customcollapse" data-target="#MasterCampaign" aria-expanded="false" aria-controls="MasterCampaign">
                                                        Master Campaign
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="Retargeting">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Retargeting Pixel</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Pixel</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="PixelID" id="PixelID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Pixel</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->pixels." where userid = ".$LoggedInUser->id." order by PixelName";
                                                        $pixels = DB()->get_results($sql);
                                                        foreach($pixels as $pixel){
                                                        ?>
                                                        <option value="<?php echo $pixel->id;?>" <?php if(($isUpdateForm && $paidtraffic->PixelID == $pixel->id) || (isset($_POST['PixelID']) && $_POST['PixelID'] == $pixel->id)) echo ' selected = "selected" ';?>><?php echo $pixel->PixelName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("pixel/?js=true")?>" data-widget="ShowLinkModel" data-related="#PixelID" data-method="post" data-page-title="Pixels Manager" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if(elUsers()->is_module_enabled("paid-traffic-conv")) {?>
                                <div class="box box-gray- box-solid- collapse box-default" id="ConversionPixel">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Conversion Pixel</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <?php
                                        if($isUpdateForm){
                                            $conversion_pixels = DB()->get_results("select * from ".DB()->paidtraffic_conversion_pixels." where userid=".$LoggedInUser->id." and PaidTrafficID=".$PaidTrafficID);
                                            foreach ($conversion_pixels as $conversion_pixel){
                                            	echo get_paidtraffic_conversion_pixel($conversion_pixel);
                                            }
                                        } 
                                        ?>
                                        <a href="javascript:" class="btn btn-sm btn-link pull-right btn-add-conversion-pixel">Add another conversion pixel</a><br class="clearfix" />
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="box box-gray- box-solid- collapse box-default" id="Goals">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Goals</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <?php
                                        if($isUpdateForm){
                                            $paidtraffic_goals = DB()->get_results("select * from ".DB()->paidtraffic_goals." where userid=".$LoggedInUser->id." and PaidTrafficID=".$PaidTrafficID);
                                            foreach ($paidtraffic_goals as $paidtraffic_goal){
                                            	echo get_paidtraffic_goal($paidtraffic_goal);
                                            }
                                        ?>
                                        <a href="<?php site_url("paidtraffic-goal/0")?>" data-widget="ShowLinkModel" data-method="post" data-page-title="Paid Traffic Goal" class="btn btn-sm btn-link pull-right btn-add-conversion-pixel">Add another Goal</a><br class="clear" />
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="Notes">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Notes</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group">
                                            <label for="AdditionalNotes" class="col-sm-4 control-label">Any Additional Notes</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control " id="AdditionalNotes" name="AdditionalNotes" placeholder="Any Additional Notes" cols="3"><?php if($isUpdateForm) echo $paidtraffic->AdditionalNotes; elseif(isset($_POST['AdditionalNotes'])) echo $_POST['AdditionalNotes'];?></textarea>
                                                <small class="help-block with-errors"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="PageImageBox">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Page Image</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div id="PageImageButton" class="" data-usehtml="true" data-input-name="PageImage" data-existing-image="<?php if($isUpdateForm) echo $paidtraffic->PageImage; else if(isset($_POST['PageImage'])) echo $_POST['PageImage'];?>">
                                                    <div class="btn btn-bordered btn-gray">
                                                        <i class="fa fa-upload"></i>&nbsp;
                                                        <span>Upload Page Image</span>
                                                    </div>
                                                    <br class="clearfix" />
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="RedirectPages">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Holding Pages</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PendingPageID" class="col-sm-4 control-label">Choose Pending Page</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="PendingPageID" id="PendingPageID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Pending Page</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->redirect_links." where RedirectLinkType='PendingPage' and userid = ".$LoggedInUser->id." order by RedirectLinkName";
                                                        $redirect_links = DB()->get_results($sql);
                                                        foreach($redirect_links as $redirect_link){
                                                        ?>
                                                        <option value="<?php echo $redirect_link->id;?>" <?php if(($isUpdateForm && $paidtraffic->PendingPageID == $redirect_link->id) || (isset($_POST['PendingPageID']) && $_POST['PendingPageID'] == $redirect_link->id)) echo ' selected = "selected" ';?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("holding-page/?js=true&RedirectLinkType=PendingPage")?>" data-widget="ShowLinkModel" data-related="#PendingPageID" data-method="post" data-page-title="Pending Page" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group select-form-group">
                                            <label for="CompletePageID" class="col-sm-4 control-label">Choose Complete Page</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="CompletePageID" id="CompletePageID" class="form-control select2" style="width: 99%">
                                                        <option value="">Select Complete Page</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->redirect_links." where RedirectLinkType='CompletePage' and userid = ".$LoggedInUser->id." order by RedirectLinkName";
                                                        $redirect_links = DB()->get_results($sql);
                                                        foreach($redirect_links as $redirect_link){
                                                        ?>
                                                        <option value="<?php echo $redirect_link->id;?>" <?php if(($isUpdateForm && $paidtraffic->CompletePageID == $redirect_link->id) || (isset($_POST['CompletePageID']) && $_POST['CompletePageID'] == $redirect_link->id)) echo ' selected = "selected" ';?>><?php echo $redirect_link->RedirectLinkName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("holding-page/?js=true&RedirectLinkType=CompletePage")?>" data-widget="ShowLinkModel" data-related="#CompletePageID" data-method="post" data-page-title="Complete Page" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- collapse box-default" id="MasterCampaign">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Master Campaign</h3>
                                        <div class="box-tools pull-right">
                                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body box-top-padded">
                                        <div class="form-group select-form-group">
                                            <label for="PixelID" class="col-sm-4 control-label">Choose Master Campaign</label>
                                            <div class="col-sm-8 ">
                                                <div class="input-group right-addon">
                                                    <select name="MasterCampaignID" id="MasterCampaignID" class="form-control select2" style="width: 99%">
                                                        <option value="0">Select Master Campaign</option>
                                                        <?php
                                                        $sql = "select * from ".DB()->master_campaigns." where MasterCampaignStatus='active' and userid = ".$LoggedInUser->id." order by MasterCampaignName";
                                                        $MasterCampaigns = DB()->get_results($sql);
                                                        foreach($MasterCampaigns as $MasterCampaign){
                                                        ?>
                                                        <option value="<?php echo $MasterCampaign->id;?>" <?php if(($isUpdateForm && $paidtraffic->MasterCampaignID == $MasterCampaign->id) || (isset($_POST['MasterCampaignID']) && $_POST['MasterCampaignID'] == $MasterCampaign->id)) echo ' selected = "selected" ';?>><?php echo $MasterCampaign->MasterCampaignName;?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <a href="<?php site_url("master-campaign-add/?js=true")?>" data-widget="ShowLinkModel" data-related="#MasterCampaignID" data-method="post" data-page-title="Master Campaign" class="input-group-addon"><i class="fa fa-plus"></i></a>
                                                    <small class="help-block with-errors"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-gray- box-solid- box-controls">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="submit" class="btn btn-success" value="Save" />
                                                <a href="<?php site_url("paidtraffic/stats")?>" class="btn btn-danger">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="save_paidtraffic" />
                            </form>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="vendor-stats-scores">
                                <?php if($isUpdateForm) echo get_vendor_score($paidtraffic->VendorID, $LoggedInUser);?>
                            </div>
                            <div class="box box-gray- box-solid- box-border-doted" style="display: none;">
                                <div class="box-body">
                                    <h2 style="margin-top: 5px;">Need help?</h2>
                                    <div style="font-size: 14px; margin-top: -10px;">
                                        Have some difficulties in creating your tracking Paid Traffic? Don't worry, here are some resources for you:
                   
                                        <ul>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">How to create a new tracking Paid Traffic</a>(video)</li>
                                            <li><a class="blueLink" href="<?php site_url("under-development/");?>" target="_blank" style="top: 0;">Tracking Paid Traffic articles</a>(F.A.Q.)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var CurrentVisibleLink = "<?php echo $isUpdateForm?$paidtraffic->VisibleLink:"";?>";
    var CurrentID = "<?php echo isset($_GET["PaidTrafficID"])?$_GET["PaidTrafficID"]:"0";?>";
    var PixelBox = '<?php echo str_replace(array("\r", "\n", "\\", "'"), array("", "", "\\\\", "\\'"), get_paidtraffic_conversion_pixel(null, 'newpixel-{new_conversion_pixel_index}'));?>';
    var PixelIndex = 0;
</script>

<!-- PaidTraffic -->
<script src="<?php site_url("js/paidtraffic.js");?>" type="text/javascript"></script>

<?php
echo '<script type="text/javascript">'.
$str_domains.
' var userid = "'.$LoggedInUser->id.'";'.
' var admindomain = '.$admindomain.';'.
' var EasyLinkURLs = '.json_encode($EasyLinkURLs).';'.
'</script>';
include_once "member_footer.php";
?>