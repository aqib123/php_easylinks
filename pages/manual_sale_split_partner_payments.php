<?php
$modulename = "manual-sale";
include_once "member_popup_header.php";

if(empty($_GET["ManualSaleID"])) die;

$ManualSale = DB()->get_row("select * from ".DB()->manualsales." where userid=".$LoggedInUser->id." and id=".$_GET["ManualSaleID"]);
if(!$ManualSale) die;

//$SplitPartners = DB()->get_results("select * from ".DB()->manualsale_split_partners." where userid=".$LoggedInUser->id." and ManualSaleID=".$ManualSale->id);
//if(!$SplitPartners) die;

$UserResultValues = get_manual_sale_result_values($ManualSale);
if(!$UserResultValues) die;

$OpenBalance = 0;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12-">
            <h3 style="padding: 0; margin: 0 0 12px;">
                <span><?php echo $ManualSale->ManualSaleName;?></span>
                <span id="OpenBlance">$0</span>
            </h3>
        </div>
    </div>
    <form class="form-horizontal easylink-form" role="form" id="saleinfoform">
        <?php
        foreach ($UserResultValues as $UserResultKey => $UserResultValue){
            if($UserResultValue['isSplitPartnerSale'] == 1 && count($UserResultValues) > 1 && $UserResultValue['UserBalance'] != 0 && $UserResultValue['UserBalance'] < 0){
                foreach ($UserResultValue['SendTo'] as $SendToUser){
                    $ReceiverResultValue = $UserResultValues["manualsale_split_partner_".$SendToUser["ReceiverID"]];
                    if($SendToUser["Amount"] != 0){
                        $ReceiverPartner = DB()->get_row("select * from ".DB()->split_partners." where userid = ".$LoggedInUser->id." and id = ".$SendToUser["ReceiverID"]);
                        $PartnerPayment = DB()->get_row("select * from ".DB()->manualsale_split_partner_payments." where userid=".$LoggedInUser->id." and SenderManualSaleSplitPartnerID=".$UserResultValue["SplitPartnerID"]." and ReceiverManualSaleSplitPartnerID=".$SendToUser["ReceiverID"]);
                        if(!$PartnerPayment){
                            $Amount = floatval($SendToUser["Amount"]);
                            $OpenBalance += $Amount;
                            $isUpdateForm = false;
                        }else{
                            $Amount = 0;
                            $OpenBalance += $Amount;
                            $isUpdateForm = true;
                        }
        ?>
        <div class="form-group has-feedback">
            <label for="ReferenceNo" class="col-sm-offset-2 col-sm-9">
                <span>Payment from <?php echo $UserResultValue["DisplayName"]?> to <?php echo $ReceiverPartner->PartnerName;?></span>
                <span class="pull-right">$<?php echo $Amount;?></span>
            </label>
            <div class="col-sm-offset-2 col-sm-9">
                <div class="input-group left-addon colored">
                    <div class="input-group-addon">#</div>
                    <input type="text" value="<?php if($isUpdateForm) echo $PartnerPayment->ReferenceNo;?>" class="form-control " id="ReferenceNo_<?php echo $UserResultValue["SplitPartnerID"]."_".$SendToUser["ReceiverID"]?>" name="ReferenceNo_<?php echo $UserResultValue["SplitPartnerID"]."_".$SendToUser["ReceiverID"]?>" placeholder="Reference No" <?php echo NAME_PATTERN;?> />
                </div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <small class="help-block with-errors"></small>
            </div>
        </div>
        <?php
                    }
                }
            }
        }
        ?>
        <input type="hidden" name="act" value="save_split_partner_payments" />
    </form>
</div>
<script>
    $("#OpenBlance").text("$<?php echo $OpenBalance;?>")
</script>
<?php include_once "member_popup_footer.php"; ?>