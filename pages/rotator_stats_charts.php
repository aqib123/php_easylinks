<?php
$pagetitle = "Rotator Stats Chart";
$pageurl = "rotator/stats";
$modulename = "rotator";

if(!isset($_GET["RotatorID"]))
    site_redirect("rotator/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->rotators." where id=".$_GET["RotatorID"]." and userid=".$LoggedInUser->id;
$rotator = DB()->get_row($sql);
if(!$rotator)
    site_redirect("rotator/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$rotator->DomainID);
$rotatorurl = get_rotatorurl($rotator);

$RotatorID = $_GET["RotatorID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$rotator_stats = get_rotators_stats($RotatorID, $startdate, $enddate);

$topchartdata = Rotator_SnapShot($RotatorID, $rotator_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

//include_once "rotatorchartdata.php";

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
if($_GET && !empty($_GET['RotatorID'])){
    $RotatorID = parseInt($_GET['RotatorID']);
    $sql = "select * from ".DB()->rotators." where id=".$RotatorID." and userid=".$LoggedInUser->id;
    $rotator = DB()->get_row($sql);
    if($rotator){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
//$curpage .= "/".$RotatorID;
$rotator_link = get_rotatorurl($rotator);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("rotator/".$rotator->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $rotator->RotatorName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("rotator/stats/".$RotatorID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Rotator Tracking Stats Charts</h1>
            </div>
        </div>
        <br />
        
        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Rotator Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("rotator/".$rotator->id)?>"><?php echo $rotator->RotatorName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $rotator->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $rotator->RotatorType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $rotator_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $rotator_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $rotator_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="rotator_statslinks pull-left">
                        <select name="RotatorLinkID" id="RotatorLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".DB()->rotator_links." where RotatorID=".$RotatorID." and userid = ".$LoggedInUser->id." order by RotatorLinkPosition";
                            $rotator_links = DB()->get_results($sql);
                            foreach($rotator_links as $rotator_link){
                                if ($rotator_link->RotatorLinkType == "customurl")
                                	$RotatorLinkURL = $rotator_link->RotatorLinkURL;
                                else
                                    $RotatorLinkURL = get_linkbankurl($rotator_link->LinkBankID);
                            ?>
                            <option value="<?php echo $rotator_link->id;?>" <?php if(isset($_GET['RotatorLinkID']) && $_GET['RotatorLinkID'] == $rotator_link->id) echo ' selected = "selected" ';?>><?php echo $RotatorLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="rotator_statsdaterange" class="rotator_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("rotator/stats/".$_GET["RotatorID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?".(isset($_GET['RotatorLinkID'])?"RotatorLinkID=".$_GET['RotatorLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left rotator_statscharts">
                        <li>
                            <a href="<?php site_url("rotator/stats/".$_GET["RotatorID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?act=exportrotatordetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>


        <?php $charttypes = array("line","bar","area","pie");?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach($charttypes as $typeindex => $charttype) { ?>
                <li <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' class="active" '?>><a href="#<?php echo $charttype?>_tab" data-toggle="tab"><i class="fa fa-<?php echo $charttype?>-chart"></i></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <?php 
                if(!empty($_GET['type'])){
                    $chartviewtype = isset($_GET['type'])?$_GET['type']:"Snapshot";
                    if($chartviewtype == "Snapshot")
                        $data = Rotator_SnapShot($RotatorID, $rotator_stats); 
                    else if($chartviewtype == "TopBrowser")
                        $data = Rotator_TopBrowser($RotatorID, $rotator_stats); 
                    else if($chartviewtype == "TopPlatform")
                        $data = Rotator_TopPlatform($RotatorID, $rotator_stats); 
                    else if($chartviewtype == "TopCountries")
                        $data = Rotator_TopCountries($RotatorID, $rotator_stats); 
                }
                foreach($charttypes as $typeindex => $charttype) { 
                ?>
                <div class="tab-pane <?php if((empty($_GET['chartviewtype']) && $typeindex == 0) || (!empty($_GET['chartviewtype']) && $_GET['chartviewtype'] == $charttype)) echo ' active " '?>" id="<?php echo $charttype?>_tab">
                    <div class="row">
                    <?php 
                    if(empty($_GET['type'])){
                        $titles = array("Snapshot", "Top Browser", "Top Platform", "Top Countries");
                        for($chartindex = 0; $chartindex < 4; $chartindex++){
                            if($chartindex == 0)
                                $data = Rotator_SnapShot($RotatorID, $rotator_stats); 
                            else if($chartindex == 1)
                                $data = Rotator_TopBrowser($RotatorID, $rotator_stats); 
                            else if($chartindex == 2)
                                $data = Rotator_TopPlatform($RotatorID, $rotator_stats); 
                            else if($chartindex == 3)
                                $data = Rotator_TopCountries($RotatorID, $rotator_stats); 
                        ?>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo $titles[$chartindex];?></h3>
                                    <a class="btn btn-link btn-sm" href="<?php site_url("rotator/stats/".$RotatorID."/charts/".str_replace(" ", "", $titles[$chartindex])."/".$charttype."/".(isset($_GET['startdate']) && isset($_GET['enddate'])?"?startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""));?>" target="_self">Details</a>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="box-body chart-box-body ">
                                    <div class="chart" style="height: 250px;">
                                        <canvas id="<?php echo $charttype."_".$titles[$chartindex];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" height="250" data-chart-values='<?php echo implode("!", $data['piedata']);?>' data-chart-labels='<?php echo implode('!', $data['labels'])?>' data-chart-datasets='<?php echo implode('!', $data['datasets'])?>'></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                    }else{
                        ?>
                        <div class="col-md-12">
                            <div class="box-body chart-box-body-">
                                <div class="chart" style="height: 250px;">
                                    <canvas id="<?php echo $charttype."_".$_GET['chartviewtype'];?>" class="<?php echo $charttype;?>Chart- chart_canvas" data-chart-type="<?php echo $charttype;?>" height="250" data-chart-values='<?php echo implode("!", $data['piedata']);?>' data-chart-labels='<?php echo implode('!', $data['labels'])?>' data-chart-datasets='<?php echo implode('!', $data['datasets'])?>'></canvas>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#rotator_statsdaterange"));

        $(document).on('click', '[data-toggle="tab"]', function (e) {
            var $this = $(this);
            var li = $this.parent();
            var $target = $($this.attr('href'));
            if (!li.hasClass("chartadded")) {
                li.addClass("chartadded");
                $target.find(".chart_canvas").each(function () {
                    var $canvas = $(this);
                    var charttype = $canvas.attr("data-chart-type");
                    var chartlegend = "";
                    if (charttype == "pie")
                        chartlegend = CreatePieChart($canvas);
                    else if (charttype == "line")
                        chartlegend = CreateLineChart($canvas);
                    else if (charttype == "bar")
                        chartlegend = CreateBarChart($canvas);
                    else if (charttype == "area")
                        chartlegend = CreateAreaChart($canvas);

                    $canvas.closest(".box-body").append('<div class="smallcheck legend_checkbox scrollable-div">' + chartlegend + '</div>');
                    $canvas.closest(".box-body").find(".legend_checkbox").find('input[type="checkbox"].blue, input[type="radio"].blue').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                    $canvas.closest(".box-body").find(".legend_checkbox").find('.blue').on('ifChanged', LegendChange);
                    $canvas.closest(".box-body").find(".legend_checkbox").mCustomScrollbar({
                        theme: "dark"
                    });
                });
            }
        });
        //CreatePieChart($('#TopChart'));

        $("#RotatorLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("rotator/stats/".$_GET["RotatorID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "RotatorLinkID=" + $this.val();
            }
            window.location.href = url;
        });

        $(".nav-tabs-custom ul li.active a").trigger("click");
    });

    function LegendChange(e) {
        var $this = $(this);
        var excludes = [];
        $this.closest('.legend_checkbox').find(".checkbox").each(function () {
            var $check = $(this);
            if (!$check.is(':checked'))
                excludes.push($check.val());
        });
        var $boxbody = $this.closest(".box-body");
        var $canvas = $boxbody.find(".chart_canvas");
        var charttype = $canvas.attr("data-chart-type");
        if (charttype == "pie")
            chartlegend = CreatePieChart($canvas, excludes);
        else if (charttype == "line")
            chartlegend = CreateLineChart($canvas, excludes);
        else if (charttype == "bar")
            chartlegend = CreateBarChart($canvas, excludes);
        else if (charttype == "area")
            chartlegend = CreateAreaChart($canvas, excludes);
    };
</script>
<?php
include_once "member_footer.php";
?>