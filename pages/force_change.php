<?php
//
?>
<div class="content-wrapper white-bg">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Following changes are required before you can use the system</h3>
                    </div>
                    <!-- Main content -->
                    <section class="box-body">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <form class="form-horizontal- easylink-form-" method="post" data-toggle="custom-validator" role="form" action="">
                                    <?php if($LoggedInUser->force_username_change){ ?>
                                    <div class="form-group">
                                        <label class="control-label" for="username">New Username</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input type="text" name="username" id="username" class="form-control" placeholder="Enter New Username" required="required" data-minlength="4" data-minlength-error="Minimum 4 characters required" data-remote="<?php site_url($pageurl."?act=check_username")?>" data-remote-error="Username already exist" />
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <small class="help-block with-errors"></small>
                                    </div><?php } ?><?php if($LoggedInUser->force_email_change){ ?>
                                    <div class="form-group">
                                        <label class="control-label" for="email">New Email</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter New Email" required="required" data-remote="<?php site_url($pageurl."?act=check_email")?>" data-remote-error="Email already exist" />
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <small class="help-block with-errors"></small>
                                    </div><?php } ?><?php if($LoggedInUser->force_password_change){ ?>
                                    <div class="form-group">
                                        <label class="control-label" for="password">New Password</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-key"></i>
                                            </span>
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter New Password" required="required" />
                                        </div>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <small class="help-block with-errors"></small>
                                    </div><?php } ?>
                                    <div class="form-group text-center">
                                        <input type="submit" class="btn btn-success" value="Save" />
                                    </div>
                                    <input type="hidden" name="act" value="update_forced_changes" />
                                </form>
                            </div>
                        </div>
                    </section>
                    <!-- /.content -->
                </div>
            </div>
        </div>
    </section>
</div>
<?php
include_once "member_footer.php";
die;