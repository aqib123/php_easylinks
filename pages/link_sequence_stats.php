<?php
if($_GET['link_sequence_type'] == "") 
    $link_sequence_type = "all";
else
    $link_sequence_type = $_GET['link_sequence_type'];

$pagetitle = "Link Sequence Stats";//.ucfirst($link_sequence_type);
$curpage = "link-sequence/stats";
$pageurl = $curpage;//"link-sequence";
$bodyclasses = " link-sequence-body ";
$modulename = "link-sequence";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("link-sequence")?>" role="button">Create Link Sequence</a>
                <a class="btn btn-flat btn btn-gray" href="<?php site_url("link-sequence/stats")?>" role="button">Link Sequence Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All LinkSequences"><i class="fa fa-file"></i></a>
                <!--<a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending LinkSequences"><i class="fa fa-hourglass-o"></i></a>-->
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active LinkSequences"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed LinkSequences"><i class="fa fa-check-square-o"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Sequence</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#link_sequenceslist" aria-expanded="true" aria-controls="link_sequenceslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="link_sequenceslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-link_sequence-tracking-stats data-table responsive- nowrap" style="width: 100%;" data-nosort-columns="0,1,2" data-default-sort-column="3">
                    <thead>
                        <tr>
                            <th class="text-center"><span></span></th>
                            <th class="text-center"><span></span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Name</span></th>
                            <th class="text-center"><span></span></th>
                            <th><span>Group</span></th>
                            <th><span>Master</span></th>
                            <th class="text-center stats-column"><span># Links</span></th>
                            <th class="text-center stats-column"><span>Links Visited</span></th>
                            <th class="text-center"><span>Status</span></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                      $sql = "select * from ".DB()->link_sequences." where userid=".$LoggedInUser->id;

                      if($link_sequence_type == "pending")
                          $sql .= " and LinkSequenceStatus = 'pending' ";
                      else if($link_sequence_type == "active")
                          $sql .= " and LinkSequenceStatus = 'active' ";
                      else if($link_sequence_type == "complete")
                          $sql .= " and LinkSequenceStatus = 'complete' ";

                      $link_sequences = DB()->get_results($sql);
                      foreach($link_sequences as $link_sequence){
                          $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$link_sequence->GroupID);
                      
                          $LinkSequenceLinkSequenceLinks = DB()->get_var("SELECT count(*) from ".DB()->link_sequence_links." where LinkSequenceID=".$link_sequence->id." ") * 1;
                          
                          $UniqueLinkSequenceVisits = DB()->get_var("select SUM(LinkSequenceVisitCount) as TotalLinkSequenceVisits from (select 1 as LinkSequenceVisitCount from ".DB()->link_sequence_clicks." where LinkSequenceLinkID in (SELECT id from ".DB()->link_sequence_links." where LinkSequenceID=".$link_sequence->id." and userid=".$LoggedInUser->id.") group by ClickIp) as LinkSequenceVisits") * 1;
                          $RawLinkSequenceVisits = DB()->get_var("select SUM(LinkSequenceVisitCount) as TotalLinkSequenceVisits from (select count(*) as LinkSequenceVisitCount from ".DB()->link_sequence_clicks." where LinkSequenceLinkID in (SELECT id from ".DB()->link_sequence_links." where LinkSequenceID=".$link_sequence->id." and userid=".$LoggedInUser->id.")) as LinkSequenceVisits") * 1;
                          //$RawLinkSequenceVisits -= $UniqueLinkSequenceVisits;

                          $LinkSequenceStatus = $link_sequence->LinkSequenceStatus == "complete"?$link_sequence->LinkSequenceStatus:update_link_sequence_status($link_sequence);
                          if($LinkSequenceStatus == "active"){
                              $link_sequencestatustext = "Live";
                              $link_sequencestatusicon = "fa fa-rocket";
                          }else if($LinkSequenceStatus == "pending"){
                              $link_sequencestatustext = "Pending";
                              $link_sequencestatusicon = "fa fa-hourglass-o";
                          }else if($LinkSequenceStatus == "complete"){
                              $link_sequencestatus = "Completed";
                              $link_sequencestatusicon = "fa fa-check-square-o";
                          }

                          $namelinkedurl = get_site_url("link-sequence/?LinkSequenceID=".$link_sequence->id);

                          $link_sequence_linkurl = get_link_sequenceurl($link_sequence);
                          $StatusIcon = get_domain_status_icon($link_sequence->DomainID);

                          $MasterCampaignName = "";
                          if($link_sequence->MasterCampaignID != "0")
                              $MasterCampaignName = DB()->get_var("select MasterCampaignName from ".DB()->master_campaigns." where id=".$link_sequence->MasterCampaignID);
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $StatusIcon;?></td>
                            <td class="text-center grey-scale"><a href="javascript:" data-url="<?php site_url($curpage."?act=link_sequence_details&LinkSequenceID=".$link_sequence->id);?>" data-toggle="DetailRow" id="" class="btn btn-link_sequence-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <td class="text-center grey-scale"><a href="<?php site_url("link-sequence/links/".$link_sequence->id."/")?>"><i class="fa ion-network"></i></a></td>
                            <td>
                                <a href="<?php echo $namelinkedurl;?>" class="pull-left"><?php echo $link_sequence->LinkSequenceName?></a>
                            </td>
                            <td class="text-center">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("link-sequence/?LinkSequenceID=".$link_sequence->id);?>" class="btn btn-sm btn-links" title="Edit Link Sequence"><i class="fa fa-pencil-square"></i>Edit Link Sequence</a></li>
                                        <li><a href="<?php echo $link_sequence_linkurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct LinkSequence Link"><i class="fa fa-external-link-square"></i>Direct Link</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=delete_link_sequence&LinkSequenceID=".$link_sequence->id);?>" class="btn btn-sm btn-links btn-delete" title="Delete Link Sequence"><i class="fa fa-trash"></i>Delete Link Sequence</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=reset_link_sequence&LinkSequenceID=".$link_sequence->id);?>" class="btn btn-sm btn-links btn-reset" title="Reset Link Sequence"><i class="fa fa-ban"></i>Reset Link Sequence</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=clone_link_sequence&LinkSequenceID=".$link_sequence->id);?>" class="btn btn-sm btn-links" title="Clone Link Sequence"><i class="fa fa-clone"></i>Clone Link Sequence</a></li>
                                        <li><a href="javascript: void(0);" data-clipboard-text="<?php echo $link_sequence_linkurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $link_sequence_linkurl;?>"><i class="fa fa-clipboard"></i>Copy Link</a></li>
                                        <li><a href="<?php site_url("link-sequence/stats/".$link_sequence->id."/details");?>" class="btn btn-sm btn-links" title="Statistics"><i class="fa fa-globe"></i>Statistics</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td><span><?php echo $GroupName;?></span></td>
                            <td><span><?php echo $MasterCampaignName;?></span></td>
                            <td class="text-center"><span><?php echo $LinkSequenceLinkSequenceLinks;?></span></td>
                            <td class="text-center"><span><?php echo $RawLinkSequenceVisits;?></span></td>
                            <td class="text-center grey-scale">
                                <a href="javascript:" class="status_icon" id="" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click"><i class="fa <?php echo $link_sequencestatusicon;?>"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <form class="form-inline status_form" role="form" method="get" action="<?php site_url($curpage.(!empty($_GET['link_sequence_type'])?"/".$_GET['link_sequence_type']."/":""));?>">
                                        <div class="form-group select-xs">
                                            <select name="status" id="LinkSequenceStatus" class="form-control select2">
                                                <?php
                                                $link_statuses = array("active" => "Active", "complete" => "Completed");
                                                foreach($link_statuses as $key => $value){
                                                ?>
                                                <option value="<?php echo $key;?>" <?php if($link_sequence->LinkSequenceStatus == $key || (isset($_POST['LinkSequenceStatus']) && $_POST['LinkSequenceStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-xs btn-default">Change</button>
                                        <input type="hidden" name="act" value="changelink_sequencestatus" />
                                        <input type="hidden" name="LinkSequenceID" value="<?php echo $link_sequence->id;?>" />
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br />
        <br />
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>