<?php
if($_GET['rotatorlink_type'] == "") 
    $rotatorlink_type = "all";
else
    $rotatorlink_type = $_GET['rotatorlink_type'];

if(!isset($_GET['rotatorlinkbank_database'])){
    $pagetitle = "Rotator Links";//.ucfirst($rotatorlink_type);
    $rotator_database = false;
    $curpage = "rotator/links";
}else{
    $pagetitle = "Rotator Links - ".ucfirst($rotatorlink_type);
    $rotator_database = true;
    $curpage = "rotator/database";
}
$pageurl = "rotator/database";
$modulename = "rotator";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);

$isUpdateForm = false;
if($_GET && !empty($_GET['RotatorID'])){
    $RotatorID = parseInt($_GET['RotatorID']);
    $sql = "select * from ".DB()->rotators." where id=".$RotatorID." and userid=".$LoggedInUser->id;
    $rotator = DB()->get_row($sql);
    if($rotator){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
$curpage .= "/".$RotatorID;
$rotator_link = get_rotatorurl($rotator);
?>
<script>var rotatorlinks = {};</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("rotator/".$rotator->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $rotator->RotatorName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage."/paused");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Paused Links"><i class="fa fa-pause"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Links"><i class="fa fa-rocket"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Rotator Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("rotator/".$rotator->id)?>"><?php echo $rotator->RotatorName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $rotator->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $rotator->RotatorType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $rotator_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $rotator_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $rotator_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        if($isUpdateForm){
        ?>
        <div class="box box-solid box-gray">
            <div class="box-header with-border">
                <h3 class="box-title">Create Rotator Link</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="qtiptooltip" title="Collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div class="box-body rotatorlinks clearfix" id="rotatorlinks">
                <?php
            $sql = "select * from ".DB()->rotator_links." where userid=".$LoggedInUser->id." and RotatorID=".$RotatorID;
            if ($rotatorlink_type != "all")
                $sql .= " and RotatorLinkLive = ".($rotatorlink_type=="paused"?"0":"1")." ";
            $sql .= " order by RotatorLinkPosition";
            
            $rotator_links = DB()->get_results($sql);
            $index = 1;
            foreach($rotator_links as $rotator_link){
                $rotator_linkbox = get_rotatorlink_box($rotator_link, $index, $rotatorlink_type);
                if($rotator_linkbox != ""){
                    echo $rotator_linkbox;
                }
                $index++;
            }?>
                <a href="#" class="btn btn-link btn-add-rotatorlink pull-right">Add Another Link</a>
            </div>
        </div>
        <?php
        }
        ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- jQueryUI -->
<link rel="stylesheet" href="<?php site_url("css/ui-themes/smoothness/jquery-ui.min.css");?>" />

<script>
    var linkindex = '<?php echo $index;?>';
    var url = "<?php site_url("rotator/links/".$RotatorID."/")?>"; 
</script>

<!-- rotatorlinks App -->
<script src="<?php site_url("js/rotatorlinks.js");?>" type="text/javascript"></script>

<!-- members js -->
<script src="<?php site_url("js/ckeditor/ckeditor.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/ckeditor/adapters/jquery.js");?>" type="text/javascript"></script>
<?php
include_once "member_footer.php";
?>