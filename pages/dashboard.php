<?php
$pagetitle = "Members Dashboard";
$pageurl = "dashboard";
$modulename = "dashboard";

include_once "member_header.php";

$timenow = strtotime("now");
$timelast30days = strtotime("-30 days");

$time_startdate = isset($_GET['startdate'])?strtotime($_GET['startdate']." 00:00:00"):"";
$time_enddate = isset($_GET['enddate'])?strtotime($_GET['enddate']." 24:00:00"):"";


$bank_clicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." inner join ".DB()->linkbanks." on ".DB()->linkbank_clicks.".LinkBankID = ".DB()->linkbanks.".id where ".DB()->linkbanks.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->linkbank_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->linkbank_clicks.".DateAdded <= '".$time_enddate."') ":"")) * 1;
$rotator_clicks = DB()->get_var("select count(*) from ".DB()->rotator_clicks." inner join ".DB()->rotators." on ".DB()->rotator_clicks.".RotatorID = ".DB()->rotators.".id where ".DB()->rotators.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->rotator_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->rotator_clicks.".DateAdded <= '".$time_enddate."') ":"")) * 1;
$paidtraffic_clicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." inner join ".DB()->paidtraffics." on ".DB()->paidtraffic_clicks.".PaidTrafficID = ".DB()->paidtraffics.".id where ".DB()->paidtraffics.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->paidtraffic_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->paidtraffic_clicks.".DateAdded <= '".$time_enddate."') ":"")) * 1;
$email_clicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where  (".DB()->linkbank_clicks.".ClickType = 'mail' or ".DB()->linkbank_clicks.".ClickType = 'cta') and ".DB()->linkbank_clicks.".EmailID in (SELECT id from ".DB()->campaign_emails." where ".DB()->campaign_emails.".userid=".$LoggedInUser->id.")".(isset($_GET["startdate"])?" and (".DB()->linkbank_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->linkbank_clicks.".DateAdded <= '".$time_enddate."') ":"")) * 1;
$total_clicks = $bank_clicks + $rotator_clicks + $paidtraffic_clicks + $email_clicks;

$total_automatic_sales = DB()->get_var("select sum(".DB()->linkbank_conversions.".UnitPrice) from ".DB()->linkbank_conversions." inner join ".DB()->linkbanks." on ".DB()->linkbank_conversions.".LinkBankID = ".DB()->linkbanks.".id where ".DB()->linkbanks.".userid=".$LoggedInUserID." and ".DB()->linkbank_conversions.".ConversionType='sales' and ".DB()->linkbanks.".ManualSalesEntry <> 1") * 1;
$total_automatic_sales_last30days = DB()->get_var("select sum(".DB()->linkbank_conversions.".UnitPrice) from ".DB()->linkbank_conversions." inner join ".DB()->linkbanks." on ".DB()->linkbank_conversions.".LinkBankID = ".DB()->linkbanks.".id where ".DB()->linkbanks.".userid=".$LoggedInUserID." and ".DB()->linkbank_conversions.".ConversionType='sales' and ".DB()->linkbank_conversions.".DateAdded >= '".$timelast30days."' and ".DB()->linkbanks.".ManualSalesEntry <> 1") * 1;
$total_automatic_sales_percentage = round(($total_automatic_sales == 0?0:$total_automatic_sales_last30days * 100 / $total_automatic_sales), 1);

$total_manual_sales_data = get_manualsales();
$total_manual_sales_last30days_data = get_manualsales($timelast30days, $timenow);

$total_manual_sales = $total_manual_sales_data["TotalAmount"];//DB()->get_var("select sum(".DB()->linkbanks.".GrossSale) from ".DB()->linkbanks." where ".DB()->linkbanks.".userid=".$LoggedInUserID." and ".DB()->linkbanks.".ManualSalesEntry = 1") * 1;
$total_manual_sales_last30days = $total_manual_sales_last30days_data["TotalAmount"];//DB()->get_var("select sum(".DB()->linkbanks.".GrossSale) from ".DB()->linkbanks." where ".DB()->linkbanks.".userid=".$LoggedInUserID." and ".DB()->linkbank_conversions.".SaleInfoEntered >= '".$timelast30days."' and ".DB()->linkbanks.".ManualSalesEntry = 1") * 1;
$total_manual_sales_percentage = round(($total_manual_sales == 0?0:$total_manual_sales_last30days * 100 / $total_manual_sales), 1);

$total_conversion = DB()->get_var("select sum(".DB()->paidtraffic_conversions.".UnitPrice) from ".DB()->paidtraffic_conversions." inner join ".DB()->paidtraffics." on ".DB()->paidtraffic_conversions.".PaidTrafficID = ".DB()->paidtraffics.".id where ".DB()->paidtraffics.".userid=".$LoggedInUserID." and ".DB()->paidtraffic_conversions.".ConversionType='action'") * 1;
$total_conversion_last30days = DB()->get_var("select sum(".DB()->paidtraffic_conversions.".UnitPrice) from ".DB()->paidtraffic_conversions." inner join ".DB()->paidtraffics." on ".DB()->paidtraffic_conversions.".PaidTrafficID = ".DB()->paidtraffics.".id where ".DB()->paidtraffics.".userid=".$LoggedInUserID." and ".DB()->paidtraffic_conversions.".ConversionType='action' and ".DB()->paidtraffic_conversions.".DateAdded >= '".$timelast30days."'") * 1;
$total_conversion_percentage = round(($total_conversion == 0?0:$total_conversion_last30days * 100 / $total_conversion), 1);

$total_sale = $total_automatic_sales + $total_manual_sales;
$total_sale_last30days = $total_automatic_sales_last30days + $total_manual_sales_last30days;
$total_sale_percentage = round(($total_sale == 0?0:$total_sale_last30days * 100 / $total_sale), 1);

$sql = "select ".DB()->linkbank_clicks.".ClickIp from ".DB()->linkbank_clicks." inner join ".DB()->linkbanks." on ".DB()->linkbank_clicks.".LinkBankID = ".DB()->linkbanks.".id where ".DB()->linkbanks.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->linkbank_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->linkbank_clicks.".DateAdded <= '".$time_enddate."') ":"")." group by ".DB()->linkbank_clicks.".ClickIp limit 0, 500";//1=2 and
$IpAddresses_linkbank = DB()->get_col($sql);

$sql = "select ".DB()->paidtraffic_clicks.".ClickIp from ".DB()->paidtraffic_clicks." inner join ".DB()->paidtraffics." on ".DB()->paidtraffic_clicks.".PaidTrafficID = ".DB()->paidtraffics.".id where ".DB()->paidtraffics.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->paidtraffic_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->paidtraffic_clicks.".DateAdded <= '".$time_enddate."') ":"")." group by ".DB()->paidtraffic_clicks.".ClickIp limit 0, 500";//1=2 and
$IpAddresses_paidtraffic = DB()->get_col($sql);

$sql = "select ".DB()->rotator_clicks.".ClickIp from ".DB()->rotator_clicks." inner join ".DB()->rotators." on ".DB()->rotator_clicks.".RotatorID = ".DB()->rotators.".id where ".DB()->rotators.".userid=".$LoggedInUserID.(isset($_GET["startdate"])?" and (".DB()->rotator_clicks.".DateAdded >= '".$time_startdate."' and ".DB()->rotator_clicks.".DateAdded <= '".$time_enddate."') ":"")." group by ".DB()->rotator_clicks.".ClickIp limit 0, 500";//1=2 and
$IpAddresses_rotator = DB()->get_col($sql);

$IpAddresses = array_merge($IpAddresses_linkbank, $IpAddresses_paidtraffic, $IpAddresses_rotator);
$IpAddresses = array_unique($IpAddresses);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li class="active"><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <br />
        <br />
        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Visitors Report</h3>
                        <div class="box-tools pull-right">
                            <div id="dashboard_statsdaterange" class="dashboard_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("dashboard/");?>">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                                <span class="pull-left"></span><b class="caret pull-right"></b>
                            </div>
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                            <button data-widget="hide" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body no-padding" style="overflow: hidden;">
                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <div class="pad">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <div id="map_canvas" class="embed-responsive-item"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-2 col-sm-2 bg-green">
                                <div class="pad box-pane-right bg-green" style="min-height: 280px; padding: 10px 0;">
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                                        <h5 class="description-header"><?php echo get_formated_number($total_clicks);?></h5>
                                        <span class="description-text">Total Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header"><?php echo get_formated_number($bank_clicks);?></h5>
                                        <span class="description-text">Bank Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header"><?php echo get_formated_number($rotator_clicks);?></h5>
                                        <span class="description-text">Rotator Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 bg-green">
                                <div class="pad box-pane-right bg-green" style="min-height: 280px; padding: 10px 0;">
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                                        <h5 class="description-header"><?php echo get_formated_number($email_clicks);?></h5>
                                        <span class="description-text">Email Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header"><?php echo get_formated_number($paidtraffic_clicks);?></h5>
                                        <span class="description-text">Paid Traffic Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header"><?php echo 0;?></h5>
                                        <span class="description-text">Split Clicks</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i style="visibility:hidden" class="ion ion-ios-pricetag-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Manual Sales</span>
                        <span class="info-box-number">$<?php echo get_formated_number($total_manual_sales);?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $total_manual_sales_percentage;?>%"></div>
                        </div>
                        <span class="progress-description"><?php echo $total_manual_sales_percentage;?>% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i style="visibility:hidden" class="ion ion-ios-heart-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Conversion Pixel Sales</span>
                        <span class="info-box-number">$<?php echo get_formated_number($total_automatic_sales);?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $total_automatic_sales_percentage;?>%"></div>
                        </div>
                        <span class="progress-description"><?php echo $total_automatic_sales_percentage;?>% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i style="visibility:hidden" class="ion ion-ios-cloud-download-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Sales</span>
                        <span class="info-box-number">$<?php echo get_formated_number($total_sale);?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo $total_sale_percentage;?>%"></div>
                        </div>
                        <span class="progress-description"><?php echo $total_sale_percentage;?>% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
        <div class="row">
            <?php
            $items = array("linkbank" => "bg-aqua","rotator" => "bg-red","campaign" => "bg-green","paidtraffic" => "bg-yellow");//,"pixel" => "bg-light-blue-active", "clicks" => "bg-maroon-active", "visits" => "bg-purple-active");
            foreach ($items as $item => $bg){
                $count = 0;
                $linkbanklabel = "";
                $linkbankurl = "#";
                $linkbankclass = "";

                if($item == "linkbank"){
                    $sqlCount = "select count(*) from ".DB()->linkbanks." where userid=".$LoggedInUserID." and (LinkStatus = 'active') ";// or LinkStatus = 'evergreen'
                    $count = DB()->get_var($sqlCount) * 1;
                    $MenuItem = get_menuitem($item."/stats");
                    $linkbankurl = get_site_url("linkbank/stats");
                    $linkbanklabel = $MenuItem->MenuItemLabel."&nbsp;(Live)";
                    $linkbankclass = $MenuItem->MenuItemClass;
                }else if($item == "rotator"){
                    $sqlCount = "select count(*) from ".DB()->rotators." where userid=".$LoggedInUserID." and RotatorStatus = 'active' ";
                    $count = DB()->get_var($sqlCount) * 1;
                    $MenuItem = get_menuitem($item."/stats");
                    $linkbankurl = get_site_url("rotator/stats");
                    $linkbanklabel = $MenuItem->MenuItemLabel."&nbsp;(Live)";
                    $linkbankclass = $MenuItem->MenuItemClass;
                }else if($item == "campaign"){
                    $sqlCount = "select count(*) from ".DB()->campaigns." where userid=".$LoggedInUserID." and CampaignStatus = 'active' ";
                    $count = DB()->get_var($sqlCount) * 1;
                    $MenuItem = get_menuitem($item."/stats");
                    $linkbankurl = get_site_url("campaign/stats");
                    $linkbanklabel = $MenuItem->MenuItemLabel."&nbsp;(Live)";
                    $linkbankclass = $MenuItem->MenuItemClass;
                }else if($item == "paidtraffic"){
                    $sqlCount = "select count(*) from ".DB()->paidtraffics." where userid=".$LoggedInUserID." and PaidTrafficStatus = 'active' ";
                    $count = DB()->get_var($sqlCount) * 1;
                    $MenuItem = get_menuitem($item."/stats");
                    $linkbankurl = get_site_url("paidtraffic/stats");
                    $linkbanklabel = $MenuItem->MenuItemLabel."&nbsp;(Live)";
                    $linkbankclass = $MenuItem->MenuItemClass;
                }
            ?>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon <?php echo $bg;?>"><i class="<?php echo $linkbankclass?>"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?php echo $linkbanklabel?></span>
                        <span class="info-box-number"><?php echo $count?></span>
                    </div>
                </div>
                <!-- /.info-box -->

                <!--<div class="dashboard-box small-box <?php echo $bg;?>">
                    <div class="inner">
                        <h3><?php echo $count?></h3>
                        <p><?php echo $linkbanklabel?></p>
                    </div>
                    <div class="icon">
                        <i class="<?php echo $linkbankclass?>"></i>
                    </div>
                    <a class="small-box-footer" href="<?php echo $linkbankurl;?>">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>-->

                <!--<div class="dashboard-box info-box <?php echo $bg;?>"">
                    <span class="info-box-icon"><i class="<?php echo $linkbankclass?>"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?php echo $linkbanklabel?></span>
                        <span class="info-box-number"><?php echo $count?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">70% Live in 7 Days
                        </span>
                    </div>
                </div>-->
            </div>
            <?php } ?>
        </div>

        <div class="row">
            <?php
            $dashboard_list_boxes = array("linkbank" => "Link Bank","rotator" => "Power Rotator","campaign" => "Email Campaign","paidtraffic" => "Paid Traffic");//,"pixel" => "bg-light-blue-active", "clicks" => "bg-maroon-active", "visits" => "bg-purple-active");
            foreach ($dashboard_list_boxes as $dashboard_list_box => $title){
                if($dashboard_list_box == "linkbank"){
                    $sql = "select * from ".DB()->linkbanks." where userid=".$LoggedInUserID." and (LinkStatus = 'active' or LinkStatus = 'evergreen' or LinkStatus = 'mylink') ";// or LinkStatus = 'evergreen'
                }else if($dashboard_list_box == "rotator"){
                    $sql = "select * from ".DB()->rotators." where userid=".$LoggedInUserID." and RotatorStatus = 'active' ";
                }else if($dashboard_list_box == "campaign"){
                    $sql = "select * from ".DB()->campaigns." where userid=".$LoggedInUserID." and CampaignStatus = 'active' ";
                }else if($dashboard_list_box == "paidtraffic"){
                    $sql = "select * from ".DB()->paidtraffics." where userid=".$LoggedInUserID." and PaidTrafficStatus = 'active' ";
                }
            ?>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="box box-primary dashboard-list-box">
                    <div class="box-body no-padding scrollable-div">
                        <table class="table">
                            <thead>
                                <tr>
                                    <?php
                    if($dashboard_list_box == "linkbank"){
?>
                                    <th class="text-center"><span></span></th>
                                    <th class="text-left"><span>Campaign</span></th>
                                    <th class="text-center"><span>Expires</span></th>
                                    <th class="text-center"><span>Raw</span></th>
                                    <th class="text-center"><span>Unique</span></th>
                                    <th class="text-center action-column"><span>Actions</span></th>
                                    <?php
                }else if($dashboard_list_box == "rotator"){
?>
                                    <th class="text-center"><span></span></th>
                                    <th class="text-left"><span>Campaign</span></th>
                                    <th class="text-center"><span>Type</span></th>
                                    <th class="text-center"><span>Links</span></th>
                                    <th class="text-center"><span>Clicks</span></th>
                                    <th class="text-center action-column"><span>Actions</span></th>
                                    <?php
                }else if($dashboard_list_box == "campaign"){
?>
                                    <th class="text-center"><span></span></th>
                                    <th class="text-left"><span>Campaign</span></th>
                                    <th class="text-center"><span>Expires</span></th>
                                    <th class="text-center"><span>Emails</span></th>
                                    <th class="text-center"><span>Clicks</span></th>
                                    <th class="text-center action-column"><span>Actions</span></th>
                                    <?php
                }else if($dashboard_list_box == "paidtraffic"){
?>
                                    <th class="text-center"><span></span></th>
                                    <th class="text-left"><span>Campaign</span></th>
                                    <th class="text-center"><span>Expires</span></th>
                                    <th class="text-center"><span>Clicks</span></th>
                                    <th class="text-center"><span>Actions</span></th>
                                    <th class="text-center"><span>Conv</span></th>
                                    <th class="text-center action-column"><span>Actions</span></th>
<?php
                }
?>

                                </tr>
                            </thead>
                            <tbody>
<?php              
                $items = DB()->get_results($sql);
                foreach($items as $item){
                    $userid = $LoggedInUserID;
                    $itemid = $item->id;
                    $RawClicks = 0;
                    $UniqueClicks = 0;
                    $expires = '0';
                    $namelinkedurl = '';
                    $itemurl = '';
                    $itemname = '';
                    if($dashboard_list_box == "linkbank"){
                        $itemname = $item->LinkName;
                        
                        $UniqueClicks = DB()->get_var("select sum(ClickCount) from (select 1 as ClickCount from ".DB()->linkbank_clicks." where LinkBankID=".$itemid." group by ClickIp) as UniqueClicks") * 1;
                        $RawClicks = DB()->get_var("select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".$itemid) * 1;
                        //$RawClicks -= $UniqueClicks;

                        $expires = '0';
                        $itemurl = get_linkbankurl($item);
                        $namelinkedurl = get_site_url("linkbank/stats/".$itemid."/details");//"item/?LinkBankID=".$itemid);
                        $editurl = "linkbank/?LinkBankID=".$itemid;
                        $deleteurl = "linkbank/database/?act=delete_link&LinkBankID=".$itemid;
                        $StatusIcon = get_domain_status_icon($item->DomainID);
?>
                                <tr>
                                    <td class="text-center"><?php echo $StatusIcon;?></td>
                                    <td><a href="<?php echo $namelinkedurl; ?>" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$itemurl;?>"><?php echo $itemname?></a></td>
                                    <td class="text-center"><span><?php echo $expires;?></span></td>
                                    <td class="text-center"><span><?php echo $RawClicks;?></span></td>
                                    <td class="text-center"><span><?php echo $UniqueClicks;?></span></td>
                                    <td>
                                        <a href="<?php site_url($editurl);?>" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i></a>
                                        <a href="javascript: void(0);" data-clipboard-text="<?php echo $itemurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $itemurl;?>"><i class="fa fa-clipboard"></i></a>
                                        <a href="<?php echo $itemurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i></a>
                                    </td>
                                </tr>
<?php
                    }else if($dashboard_list_box == "rotator"){
                        $itemname = $item->RotatorName;
                        $RotatorType = $item->RotatorType;
                        $RawClicks = DB()->get_var("select SUM(RotatorVisitCount) as TotalRotatorVisits from (select count(*) as RotatorVisitCount from ".DB()->rotator_clicks." where RotatorLinkID in (SELECT id from ".DB()->rotator_links." where RotatorID=".$itemid." and userid=".$LoggedInUserID.")) as RotatorVisits") * 1;
                        $RotatorLinks = DB()->get_var("SELECT count(*) from ".DB()->rotator_links." where RotatorID=".$itemid." and userid=".$LoggedInUserID."") * 1;
                        $expires = '0';
                        $itemurl = get_rotatorurl($item);
                        $namelinkedurl = get_site_url("rotator/stats/".$itemid."/details");//"rotator/?RotatorID=".$itemid);
                        $editurl = "rotator/?RotatorID=".$itemid;
                        $deleteurl = "rotator/database/?act=delete_rotator&RotatorID=".$itemid;
                        $StatusIcon = get_domain_status_icon($item->DomainID);
?>
                                <tr>
                                    <td class="text-center"><?php echo $StatusIcon;?></td>
                                    <td><a href="<?php echo $namelinkedurl; ?>"><?php echo $itemname?></a></td>
                                    <td class="text-center"><span><?php echo $RotatorType;?></span></td>
                                    <td class="text-center"><span><?php echo $RotatorLinks;?></span></td>
                                    <td class="text-center"><span><?php echo $RawClicks;?></span></td>
                                    <td>
                                        <a href="<?php site_url($editurl);?>" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i></a>
                                        <a href="javascript: void(0);" data-clipboard-text="<?php echo $itemurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $itemurl;?>"><i class="fa fa-clipboard"></i></a>
                                        <a href="<?php echo $itemurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i></a>
                                    </td>
                                </tr>
<?php
                    }else if($dashboard_list_box == "campaign"){
                        $itemname = $item->CampaignName;
                        $RawClicks = DB()->get_var("select SUM(ClickCount) as TotalClicks from (select count(*) as ClickCount from ".DB()->linkbank_clicks." where ClickType='mail' and EmailID in (SELECT id from ".DB()->campaign_emails." where CampaignID=".$itemid." and userid=".$LoggedInUserID.")) as Clicks") * 1;
                        $EmailsCount = DB()->get_var("SELECT count(*) from ".DB()->campaign_emails." where CampaignID=".$itemid." and userid=".$LoggedInUserID."") * 1;
                        $expires = '0';
                        $itemurl = '#';//get_rotatorurl($item);
                        $namelinkedurl = get_site_url("campaign/emails/".$itemid."/");//"campaign/?CampaignID=".$itemid);
                        $editurl = "campaign/?CampaignID=".$itemid;
                        $deleteurl = "campaign/database/?act=delete_campaign&CampaignID=".$itemid;
                        $StatusIcon = get_domain_status_icon();
?>
                                <tr>
                                    <td class="text-center"><?php echo $StatusIcon;?></td>
                                    <td><a href="<?php echo $namelinkedurl; ?>"><?php echo $itemname?></a></td>
                                    <td class="text-center"><span><?php echo $expires;?></span></td>
                                    <td class="text-center"><span><?php echo $EmailsCount;?></span></td>
                                    <td class="text-center"><span><?php echo $RawClicks;?></span></td>
                                    <td>
                                        <a href="<?php site_url($editurl);?>" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i></a>
                                        <a href="javascript: void(0);" data-clipboard-text="<?php echo $itemurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $itemurl;?>"><i class="fa fa-clipboard"></i></a>
                                        <a href="<?php echo $itemurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i></a>
                                    </td>
                                </tr>
<?php
                    }else if($dashboard_list_box == "paidtraffic"){
                        $itemname = $item->PaidTrafficName;
                        $RawClicks = DB()->get_var("select count(*) from ".DB()->paidtraffic_clicks." where PaidTrafficID=".$itemid) * 1;
                        $NoOfClicks = $item->NoOfClicks;
                        $TotalActions = DB()->get_var("select count(*) from ".DB()->paidtraffic_conversions." where PaidTrafficID=".$itemid." and ConversionType='action'") * 1;
                        $expires = '0';
                        $itemurl = get_paidtrafficurl($item);
                        $namelinkedurl = get_site_url("paidtraffic/stats/".$itemid."/details");//"paidtraffic/?PaidTrafficID=".$itemid);
                        $editurl = "paidtraffic/?PaidTrafficID=".$itemid;
                        $deleteurl = "paidtraffic/database/?act=delete_paidtraffic&PaidTrafficID=".$itemid;
                        $StatusIcon = get_domain_status_icon($item->DomainID);
?>
                                <tr>
                                    <td class="text-center"><?php echo $StatusIcon;?></td>
                                    <td><a href="<?php echo $namelinkedurl; ?>" class="qtip2" data-qtip-image="<?php echo PREVIEW_URL.$itemurl;?>"><?php echo $itemname?></a></td>
                                    <td class="text-center"><span><?php echo $expires;?></span></td>
                                    <td class="text-center"><span><?php echo $RawClicks."/".$NoOfClicks;?></span></td>
                                    <td class="text-center"><span><?php echo $TotalActions;?></span></td>
                                    <td class="text-center"><span><?php echo "0";?></span></td>
                                    <td>
                                        <a href="<?php site_url($editurl);?>" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i></a>
                                        <a href="javascript: void(0);" data-clipboard-text="<?php echo $itemurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $itemurl;?>"><i class="fa fa-clipboard"></i></a>
                                        <a href="<?php echo $itemurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i></a>
                                    </td>
                                </tr>
<?php
                    }
                }
?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <a class="uppercase" href="<?php site_url($dashboard_list_box."/stats/")?>">View All <?php echo $title;?></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<link href="<?php site_url("css/jquery-jvectormap-2.0.3.css");?>" rel="stylesheet" type="text/css" />
<script src="<?php site_url("js/jquery-jvectormap-2.0.3.min.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/jquery-jvectormap-world-merc-en.js");?>" type="text/javascript"></script>
<script src="<?php site_url("js/gdp-data.js");?>" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo "IpAddresses = ['".implode("','",  $IpAddresses)."'];\n";?>
        CreateMap($('#map_canvas'));
        AddMarker();

        CreateDateRange($("#dashboard_statsdaterange"));

        $(".yellow-icon").each(function () {
            var $this = $(this);
            MoveRowToTop($this);
        });

        $(".red-icon").each(function () {
            var $this = $(this);
            MoveRowToTop($this);
        });

        //CreatePieChart($('#TopChart'));
    });
    function MoveRowToTop($icon) {
        var $tr = $icon.closest("tr");
        if ($tr.prev("tr").length > 0) {
            var $tbody = $icon.closest("tbody");
            $tr.insertBefore($tbody.find("tr").eq(0));
        }
    }
</script>
<?php
    include_once "member_footer.php";
?>