<?php
include_once "../includes/functions.php";

$LastCheckDate = date("m-d-Y");
$sql = "select * from ".DB()->domains." where id not in (select distinct DomainID from ".DB()->domain_statuses." where LastCheckDate='".$LastCheckDate."')";
$domains = DB()->get_results($sql);
if(is_array($domains)){
    $index = 0;
    while($index < count($domains)){
        $domain = $domains[$index];
        $DomainUrl = $domain->DomainUrl;
        $DomainPort = strpos($DomainUrl, "https://") !== false?443:80;
        $DomainHost = strpos($DomainUrl, "://") !== false?explode("://", $DomainUrl)[1]:$DomainUrl;
        $DomainHost = explode("/", ltrim($DomainHost, "/"))[0];
        
        $BlackListStatus = get_domain_black_list_status($DomainHost);
        $DomainServerError = get_domain_server_error($DomainHost, $DomainPort);

        $data = array(
            "DomainID"              => $domain->id,
            "DomainBlacklisted"     => 0,
            "DomainServerError"     => $DomainServerError,
            "DateAdded"             => strtotime("now"),
            "LastCheckDate"         => $LastCheckDate
        );

        if($BlackListStatus->status == 0 && $BlackListStatus->blacklisted > 0)
            $data["DomainBlacklisted"] = 1;

        if($BlackListStatus->status != 18)
            DB()->insert(DB()->domain_statuses, $data);

        sleep(80);

        if($BlackListStatus->status != 18)
            $index++;
    }
}
