<?php
include_once "../includes/functions.php";

@session_start();
unset($_SESSION[SITE_PREFIX."-LoggedInUser"]);
unset($_SESSION[SITE_PREFIX."user_loggedin"]);
unset($_SESSION['username']);
unset($_SESSION['UserID']);
unset($_SESSION['EmailAddress']);
unset($_SESSION['user_status']);
unset($_SESSION['product_type']);
@session_destroy();

SYS()->remove_cookie(SITE_PREFIX."username");
SYS()->remove_cookie(SITE_PREFIX."password");

//unset($_SESSION["current_user"]);
//unset($_SESSION["user_loggedin"]);
//session_destroy();

//remove_cookie("el_username");
//remove_cookie("el_password");

site_redirect("login/?loggedout");
die;