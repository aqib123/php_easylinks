<?php
$pagetitle = "Rotator Stats";
$pageurl = "rotator/stats";
$modulename = "rotator";

if(!isset($_GET["RotatorID"]))
    site_redirect("rotator/stats");

include_once "member_header.php";

$sql = "select * from ".DB()->rotators." where id=".$_GET["RotatorID"]." and userid=".$LoggedInUser->id;
$rotator = DB()->get_row($sql);
if(!$rotator)
    site_redirect("rotator/stats");

$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
$domain = DB()->get_row("select * from ".DB()->domains." where id=".$rotator->DomainID);
$rotatorurl = get_rotatorurl($rotator);

$RotatorID = $_GET["RotatorID"];
$startdate = isset($_GET["startdate"])?$_GET["startdate"]:"";
$enddate = isset($_GET["enddate"])?$_GET["enddate"]:"";
$rotator_stats = get_rotators_stats($RotatorID, $startdate, $enddate);

$topchartdata = Rotator_SnapShot($RotatorID, $rotator_stats, array("Unique" => "rgba(125, 178, 43, 1.0)", "Non-Unique" => "rgba(255, 156, 0, 1.0)", "Bots" => "rgba(245, 26, 97, 1.0)"));

$MenuItem = get_menuitem($pageurl);
$isUpdateForm = false;
if($_GET && !empty($_GET['RotatorID'])){
    $RotatorID = parseInt($_GET['RotatorID']);
    $sql = "select * from ".DB()->rotators." where id=".$RotatorID." and userid=".$LoggedInUser->id;
    $rotator = DB()->get_row($sql);
    if($rotator){
        $isUpdateForm = true;
    }
}
$GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);
//$curpage .= "/".$RotatorID;
$rotator_link = get_rotatorurl($rotator);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li><a href="<?php site_url("rotator/".$rotator->id."/")?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $rotator->RotatorName?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator")?>" role="button">Create Rotator</a>
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("rotator/stats")?>" role="button">Rotator Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url("rotator/stats/".$RotatorID."/details/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Details"><i class="fa fa-clock-o"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/map/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Maps"><i class="fa fa-globe"></i></a>
                <a href="<?php site_url("rotator/stats/".$RotatorID."/charts/");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Charts"><i class="fa fa-trophy"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1>Rotator Tracking Stats Details</h1>
            </div>
        </div>
        <br />

        <div class="box box-solid box-default">
            <div class="box-body">
                <div class="col-lg-6 no-gutter">
                    <label class="small">Rotator Name: </label>
                    &nbsp;
                    <a class="" href="<?php site_url("rotator/".$rotator->id)?>"><?php echo $rotator->RotatorName?></a><br />

                    <ul class="list-inline">
                        <li>
                            <span class="small">Date Created: </span>
                            <a href="#" class="small"><?php echo date("m/d/Y", $rotator->DateAdded)?></a><br />
                        </li>
                        <li>
                            <span class="small">Group: </span>
                            <a href="#" class="small"><?php echo $GroupName?></a><br />
                        </li>
                        <li>
                            <span class="small">Type: </span>
                            <a href="#" class="small"><?php echo $rotator->RotatorType?></a><br />
                        </li>
                        <li>
                            <span class="small" title="<?php echo $rotator_link;?>">Destination: </span>
                            <a href="javascript: void(0);" data-clipboard-text="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon copy" title="Copy <?php echo $rotator_link;?>"><i class="fa fa-clipboard"></i></a>
                            <a href="<?php echo $rotator_link;?>" class="btn btn-link btn-small-icon" title="<?php echo $rotator_link;?>" target="_blank"><i class="fa fa-external-link"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 no-gutter" style="padding: 15px 0;">
                    <div class="rotator_statslinks pull-left">
                        <select name="RotatorLinkID" id="RotatorLinkID" class="form-control select2" style="width: 100%">
                            <option value="">Select Link</option>
                            <?php
                            $sql = "select * from ".DB()->rotator_links." where RotatorID=".$RotatorID." and userid = ".$LoggedInUser->id." order by RotatorLinkPosition";
                            $rotator_links = DB()->get_results($sql);
                            foreach($rotator_links as $rotator_link){
                                if ($rotator_link->RotatorLinkType == "customurl")
                                	$RotatorLinkURL = $rotator_link->RotatorLinkURL;
                                else
                                    $RotatorLinkURL = get_linkbankurl($rotator_link->LinkBankID);
                            ?>
                            <option value="<?php echo $rotator_link->id;?>" <?php if(isset($_GET['RotatorLinkID']) && $_GET['RotatorLinkID'] == $rotator_link->id) echo ' selected = "selected" ';?>><?php echo $RotatorLinkURL;?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="rotator_statsdaterange" class="rotator_statsdaterange pull-left" data-startdate="<?php echo isset($_GET['startdate'])?$_GET['startdate']:'';?>" data-enddate="<?php echo isset($_GET['enddate'])?$_GET['enddate']:'';?>" data-url="<?php site_url("rotator/stats/".$_GET["RotatorID"]."/details/?".(isset($_GET['RotatorLinkID'])?"RotatorLinkID=".$_GET['RotatorLinkID']."&":""));?>">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar pull-left"></i>&nbsp;
                        <span class="pull-left"></span><b class="caret pull-right"></b>
                    </div>
                    <!--<ul class="list-inline pull-left rotator_statscharts">
                        <li>
                            <a href="<?php site_url("rotator/stats/".$_GET["RotatorID"]."/charts/".(!empty($_GET['type'])?$_GET['type']."/":"").(!empty($_GET['chartviewtype'])?$_GET['chartviewtype']."/":"")."?act=exportrotatordetails".(isset($_GET['startdate']) && isset($_GET['enddate'])?"&startdate=".$_GET['startdate']."&enddate=".$_GET['enddate']:""))?>" class="btn btn-bordered" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-bordered"><i class="fa fa-cog"></i></a>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>

        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Rotator Tracking Stats Details</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#rotatorslist" aria-expanded="true" aria-controls="rotatorslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="rotatorslist" class="table-responsive data-table-container collapse in" aria-expanded="true">
                <table class="table table-condensed table-bordered table-striped table-rotator-tracking-stats data-table responsive- nowrap" data-paging="true">
                    <thead>
                        <tr>
                            <th><span>Access Time</span></th>
                            <th><span>URL</span></th>
                            <th><span>IP</span></th>
                            <th class="text-center"><span>Tier</span></th>
                            <th class="text-center"><span>Country</span></th>
                            <th class="text-center"><span>Browser</span></th>
                            <th class="text-center"><span>Platform</span></th>
                            <th class="text-center"><span>Type</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $table = " ".DB()->rotator_clicks." inner join ".DB()->rotator_links." on ".DB()->rotator_clicks.".RotatorLinkID = ".DB()->rotator_links.".id ";
                        $sql = "select ".DB()->rotator_clicks.".* from ".$table." where RotatorID=".$rotator->id;

                        if(isset($_GET['startdate']) && isset($_GET['enddate']))
                            $sql .= " and (".DB()->rotator_clicks.".DateAdded >= '".strtotime($_GET['startdate'])."' and ".DB()->rotator_clicks.".DateAdded <= '".strtotime($_GET['enddate'])."') ";

                        if(isset($_GET["RotatorLinkID"]))
                            $sql .= " and (".DB()->rotator_clicks.".RotatorLinkID in(".$_GET['RotatorLinkID'].")) ";

                        //echo $sql;
                        $rotator_clicks = DB()->get_results($sql);
                        foreach($rotator_clicks as $rotator_click){
                            $GroupName = DB()->get_var("select GroupName from ".DB()->groups." where id=".$rotator->GroupID);

                            $countrycode = $rotator_click->CountryCode;
                            if($countrycode == "") {
                                $countrycode = "unknown";
                                $countryname = "Unknown";
                                $countrytier = 3;
                            }else{
                                $country = DB()->get_row("select * from ".DB()->countries." where countrycode='".$countrycode."'");
                                $countryname = $country->countryname;
                                $countrytier = $country->countrytier;
                                $countrycode = strtolower($countrycode);
                            }

                            switch($rotator_click->BrowserName){
                                case 'Android Safari':
                                    $ub = "mobilesafari";
                                    break;
                                case 'Apple Safari':
                                    $ub = "safari";
                                    break;
                                case 'Microsoft Edge';
                                    $ub = "edge";
                                    break;
                                case 'Internet Explorer';
                                    $ub = "msie";
                                    break;
                                case 'Mozilla Firefox';
                                    $ub = "firefox";
                                    break;
                                case 'Google Chrome';
                                    $ub = "chrome";
                                    break;
                                case 'Opera';
                                    $ub = "opera";
                                    break;
                                case 'Netscape';
                                    $ub = "netscape";
                                    break;
                                default:
                                    $ub = "unknown";
                            }

                            if($ub == "unknown")
                                $browsername = "Unknown";
                            else
                                $browsername = $rotator_click->BrowserName . " " . $rotator_click->BrowserVersion;

                            $platform = $rotator_click->Platform;
                            if(strpos($platform, "indows") > 0)
                                $os = "windows";
                            else if($platform == ""){
                                $platform = "Unknown";
                                $os = "unknown";
                            }else
                                $os = strtolower($rotator_click->Platform);

                            $rotator_clicktype = "nonuniquerotatorvisit";
                            $clicktype = "Non-Unique";
                            $typeclass = "fa fa-users";
                            if($rotator_click->BotName != ""){
                                $rotator_clicktype = "botrotatorvisit";
                                $typeclass = "fa fa-bug";
                                $clicktype = "Bot";
                            }else{
                                $rotator_clickscount = (DB()->get_var("select count(*) from ".$table." where ClickIp='".$rotator_click->ClickIp."' and RotatorID=".$rotator->id)) * 1;
                                if($rotator_clickscount == 1){
                                    $rotator_clicktype = "uniquerotatorvisit";
                                    $typeclass = "fa fa-user";
                                    $clicktype = "Unique";
                                }
                            }

                            $RotatorLinkURL = "";
                            $sql = "select * from ".DB()->rotator_links." where id=".$rotator_click->RotatorLinkID;
                            $rotator_link = DB()->get_row($sql);
                            if($rotator_link){
                                if ($rotator_link->RotatorLinkType == "customurl")
                                	$RotatorLinkURL = $rotator_link->RotatorLinkURL;
                                else
                                    $RotatorLinkURL = get_linkbankurl($rotator_link->LinkBankID);
                            }
                            $flag = file_exists(BASE_DIR."/images/flags/".strtolower($countrycode).".png")?$countrycode:"noflag";
                        ?>
                        <tr>
                            <td><span><?php echo date("m/d/Y h:i:s A", $rotator_click->DateAdded)?></span></td>
                            <td><a href="<?php echo $RotatorLinkURL;?>"><?php echo $RotatorLinkURL;?></a></td>
                            <td><span><?php echo $rotator_click->ClickIp;?></span></td>
                            <td class="text-center"><span><?php echo $countrytier;?></span></td>
                            <td class="text-center"><a class="" target="_blank" href="https://maps.google.com/?q=<?php echo $countryname;?>" data-toggle="qtiptooltip" title="<?php echo $countryname;?>">
                                <img class="icon16xwidth" src="<?php site_url("images/flags/".$flag.".png")?>" title="<?php echo $countryname?>" /></a>
                                <span class="hidden"><?php echo $countryname;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$ub.".png")?>" data-toggle="qtiptooltip" title="<?php echo $browsername?>" />
                                <span class="hidden"><?php echo $browsername;?></span>
                            </td>
                            <td class="text-center">
                                <img class="icon16xwidth" src="<?php site_url("images/".$os.".png")?>" data-toggle="qtiptooltip" title="<?php echo $platform;?>" />
                                <span class="hidden"><?php echo $platform;?></span>
                            </td>
                            <td class="text-center">
                                <span class="<?php echo $rotator_clicktype;?>" data-toggle="qtiptooltip" title="<?php echo $clicktype;?>"><i class="<?php echo $typeclass;?>"></i></span>
                                <span class="hidden"><?php echo $clicktype;?></span>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        CreateDateRange($("#rotator_statsdaterange"));

        //CreatePieChart($('#TopChart'));

        $("#RotatorLinkID").on("change", function () {
            var $this = $(this);
            var url = '<?php site_url("rotator/stats/".$_GET["RotatorID"]."/details/?".(isset($_GET['startdate'])?"startdate=".$_GET['startdate']."&":"").(isset($_GET['enddate'])?"enddate=".$_GET['enddate']."&":""));?>';
            if ($this.val() != "") {
                url += "RotatorLinkID=" + $this.val();
            }
            window.location.href = url;
        });
    });
</script>
<?php
include_once "member_footer.php";
?>