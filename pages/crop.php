<?php

require_once('../includes/PictureCut.php');

try {

	$pictureCut = PictureCut::createSingleton();
	
	if($pictureCut->crop()){
		print $pictureCut->toJson();
	} else {
     print $pictureCut->exceptionsToJson(); //print exceptions if the upload fails
  	}

} catch (Exception $e) {
	print $e->getMessage();
}


