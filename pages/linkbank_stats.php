<?php
if($_GET['linkbank_type'] == "") 
    $LinkBanksType = "all";
else
    $LinkBanksType = $_GET['linkbank_type'];

$LinkBankSaleType = $_GET['linkbank_sale_type'];

$pagetitle = "Linkbank Stats";//.ucfirst($LinkBanksType);
$curpage = "linkbank/stats".($LinkBankSaleType == ""?"":"/".$LinkBankSaleType);
$ajaxpage = $curpage.($_GET['linkbank_type'] == ""?"":"/".$_GET['linkbank_type']);
$pageurl = "linkbank/stats";//"linkbank/stats";
$modulename = "linkbank".($LinkBankSaleType == ""?"":"-".$LinkBankSaleType);

//$curpage .= !empty($_GET['linkbank_type'])?"/".$_GET['linkbank_type']:"";

include_once "member_header.php";

$MenuItem = get_menuitem($pageurl);
$SystemUserID = $SystemUser->id;
$PageNo = 1;
$PerPage = 15;
list($Rows, $TotalRows) = DBLinkbanks()->get_pagged($LinkBanksType, $SystemUserID, $LinkBankSaleType, $_GET, $PageNo, $PerPage);
$LinkStatuses = SYS()->LinkStatusesAll;
$HasMoreLinks = $TotalRows > $PerPage;
$NextPageNo = $PageNo + 1;
?>
<script>
    var HasMoreLinks = <?php echo $HasMoreLinks > 0 ? "true" : "false";?>;
    var NextPageNo = <?php echo $NextPageNo;?>;
    var PerPage = <?php echo $PerPage;?>;
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper white-bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php site_url("dashboard")?>"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="<?php site_url($MenuItem->MenuItemURL)?>"><i class="<?php echo $MenuItem->MenuItemClass?>"></i><?php echo $MenuItem->MenuItemLabel;?></a></li>
            <li class="active"><?php echo $pagetitle;?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 top-buttons">
                <a class="btn btn-flat btn btn-bordered" href="<?php site_url("linkbank")?>" role="button">Create New Link</a>
                <a class="btn btn-flat btn btn-<?php echo $LinkBankSaleType == ""?"gray":"bordered"?>" href="<?php site_url($pageurl)?>" role="button">Linkbank Statistics</a>
                <a class="btn hidden-xs btn-flat btn-linkbank-conv btn btn-<?php echo $LinkBankSaleType == "conv"?"gray":"bordered"?>" href="<?php site_url($pageurl."/conv")?>" role="button">Linkbank Conversions Statistics</a>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php site_url($curpage);?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="All Links"><i class="fa fa-file"></i></a>
                <a href="<?php site_url($curpage."/pending");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Pending Links"><i class="fa fa-hourglass-o"></i></a>
                <a href="<?php site_url($curpage."/active");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Active Links"><i class="fa fa-rocket"></i></a>
                <a href="<?php site_url($curpage."/complete");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Completed Links"><i class="fa fa-check-square-o"></i></a>
                <a href="<?php site_url($curpage."/evergreen");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="Evergreen Links"><i class="fa fa-tree"></i></a>
                <a href="<?php site_url($curpage."/mylink");?>" class="btn btn-bordered" data-toggle="qtiptooltip" title="My Links"><i class="fa fa-user"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $pagetitle;?></h1>
            </div>
        </div>
        <br />
        <div class="box box-gray-">
            <div class="box-header with-border">
                <h3 class="box-title">Link Campaigns</h3>
                <div class="box-tools pull-right">
                    <button data-toggle="customcollapse" data-target="#linkslist" aria-expanded="true" aria-controls="linkslist" class="btn btn-box-tool"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            
            <div class="filter-box">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-3">
                        <form action="<?php site_url($curpage);?>" method="get" class="">
                            <select name="VendorID" id="VendorID" class="form-control select2" style="width: 100%" onchange="javascript: if(this.value != '') this.form.submit();">
                                <option value="">Select Vendor</option>
                                <?php
                                $sql = "select * from ".DB()->vendors." where VendorType='Affiliate' and userid = ".$LoggedInUser->id." order by VendorName";
                                $vendors = DB()->get_results($sql);
                                foreach($vendors as $vendor){
								?>
                                <option value="<?php echo $vendor->id;?>" <?php if (isset($_GET['VendorID']) && $_GET['VendorID'] == $vendor->id) echo ' selected = "selected" ';?>><?php echo $vendor->VendorName;?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="act" value="filter_linkbank_vendor" />
                        </form>
                    </div>
                    <div class="col-sm-3">
                        <form action="<?php site_url($curpage);?>" method="get" class="">
                            <select name="GroupID" id="GroupID" class="form-control select2" style="width: 100%" onchange="javascript: if (this.value != '') this.form.submit();">
                                <option value="">Select Group</option>
                                <?php
                                $sql = "select * from ".DB()->groups." where GroupType='LinkBank' and userid = ".$LoggedInUser->id." order by GroupName";
                                $groups = DB()->get_results($sql);
                                foreach($groups as $group){
								?>
                                <option value="<?php echo $group->id;?>" <?php if (isset($_GET['GroupID']) && $_GET['GroupID'] == $group->id) echo ' selected = "selected" ';?>><?php echo $group->GroupName;?></option>
                                <?php } ?>
                            </select>
                            <input type="hidden" name="act" value="filter_linkbank_group" />
                        </form>
                    </div>
                    <div class="col-sm-3 hidden-xs">
                        <form action="<?php site_url($curpage);?>" method="get" class="form-inline search-form-lg" onsubmit="javascript: if (!('#linkbank_keywords').val() == '') return false;">
                            <div class="form-group">
                                <label class="sr-only" for="searchlinkbank">Search Link Bank</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" id="linkbank_keywords" name="keywords" placeholder="Search Link Bank" value="<?php echo isset($_POST["keywords"]) ? $_POST["keywords"] : "";?>">
                                    <div class="input-group-addon">
                                        <button type="submit" class="btn btn-block"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="act" value="search_linkbank" />
                        </form>
                    </div>
                </div>
            </div>
            <div id="linkslist" class="table-responsive- data-table-container- collapse in" aria-expanded="true">
                <?php //$jsonData = get_linkbank_rows($_POST['draw'], $LinkBanksType, $LinkBankSaleType, $pageurl, $curpage, 0, -1); ?>
                <table id="LinkbanksList" class="table table-condensed table-bordered table-striped table-link-tracking-stats data-table dt-responsive- wrap" data-nosort-columns="<?php echo $LinkBankSaleType == "conv"?"0,1,2,12,13":"0,1,6,9,10"; ?>" data-default-sort-column="<?php echo SYS()->is_valid($_GET["column_no"]) ? $_GET["column_no"] : ($LinkBankSaleType == "conv"?"11":"8"); ?>" data-default-sort-type="<?php echo SYS()->is_valid($_GET["order_type"]) ? $_GET["order_type"] : (!SYS()->is_valid($_GET["column_no"]) ? "desc" : "asc"); ?>" data-searching-="true" data-info="true" data-paging-="true" data-ajax-url-="<?php site_url($ajaxpage);?>" data-ajax-act="linkbank_get_rows" data-ajax-method="POST" data-ajax-pipeline="true" data-date-columns="9" data-date-format="MM/DD/YYYY" <?php if($TotalRows > 10) echo 'data-server-sort-="'.SYS()->get_site_url($curpage).'"';?> >
                    <thead>
                        <tr>
                            <th class="hidden-xs"><span></span></th>
                            <?php if($LinkBankSaleType == "conv") { ?>
                            <th><span></span></th>
                            <?php } ?>
                            <th class="text-center">
                                <span class="hidden-xs">Link</span>
                                <span class="visible-xs-inline-block">Actions</span>
                            </th>
                            <th><span>Link Name</span></th>
                            <th class="hidden-xs"><span>Vendor</span></th>
                            <!--<th><span>Master</span></th>-->
                            <th class="text-center stats-column"><span>Raw</span></th>
                            <th class="text-center stats-column"><span>Unique</span></th>
                            <?php if($LinkBankSaleType == "") { ?>
                            <th class="text-center stats-column hidden-xs" data-no-search="true"><span>Manual</span></th>
                            <?php } ?>
                            <?php if($LinkBankSaleType == "conv") { ?>
                            <th class="text-center conv-column"><span>Sale #</span></th>
                            <th class="text-center conv-column"><span>Sale $</span></th>
                            <th class="text-center conv-column"><span>Sale %</span></th>
                            <?php } ?>
                            <th class="hidden-xs"><span>Group</span></th>
                            <th class="text-center hidden-xs"><span>Created</span></th>
                            <th class="text-center hidden-xs" data-no-search="true"><span>Status</span></th>
                            <th class="hidden-xs"><span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php DBLinkbanks()->get_display_rows($Rows, $SystemUserID, $LinkBanksType, $LinkBankSaleType); ?>
                    <?php if(false) { ?>
                        <?php
                              //$columns = array("LinkName", "VendorName", "GroupName", "MasterCampaignName", "RawClicks", "UniqueClicks", "CreatedDate", "");
                              //$sql = "select id, LinkName, LinkStatus, VisibleLink, DomainID, MasterCampaignID, DateAdded, FROM_UNIXTIME(DateAdded, '%m/%d/%Y') as CreatedDate, 
                              //      GrossSale, ExtraIncome, Expenses, Commission, PartnerSplit, SplitExpensesWithPartner, SplitExpensesPercentage, 
                              //      if(LinkStatus = 'active','Live',if(LinkStatus = 'pending','Pending', if(LinkStatus = 'complete','Completed', if(LinkStatus = 'evergreen','Evergreen', 'My Links')))) as LinkBankStatus,
                              //      if(LinkStatus = 'active','fa fa-rocket',if(LinkStatus = 'pending','fa fa-hourglass-o', if(LinkStatus = 'complete','fa fa-check-square-o', if(LinkStatus = 'evergreen','fa fa-tree', 'fa fa-user')))) as LinkBankStatusIcon,
                              //      (select GroupName from ".DB()->groups." where id=".DB()->linkbanks.".GroupID) as GroupName, 
                              //      (select VendorName from ".DB()->vendors." where id=".DB()->linkbanks.".VendorID) as VendorName,
                              //      LinkBankUniqueClicks(".DB()->linkbanks.".id) as UniqueClicks,
                              //      (select count(*) from ".DB()->linkbank_clicks." where LinkBankID=".DB()->linkbanks.".id) as RawClicks,
                              //      (select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='action') as Actions,
                              //      (select count(*) as FireCount from ".DB()->linkbank_pixel_fires." where LinkBankID=".DB()->linkbanks.".id) as PixelFires,
                              //      (select MasterCampaignName from ".DB()->master_campaigns." where id=".DB()->linkbanks.".MasterCampaignID) as MasterCampaignName,
                              //      (select count(*) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='sales') as ConversionsCount,
                              //      (select sum(UnitPrice) from ".DB()->linkbank_conversions." where LinkBankID=".DB()->linkbanks.".id and ConversionType='sales') as ConversionsAmount
                              //   from ".DB()->linkbanks." where id ".($LinkBankSaleType == ""?"not ":"")." in(select LinkBankID from ".DB()->linkbank_conversion_pixels.") and userid=".$LoggedInUser->id;

                              //if($LinkBanksType == "pending")
                              //    $sql .= " and LinkStatus = 'pending' ";
                              //else if($LinkBanksType == "active")
                              //    $sql .= " and LinkStatus = 'active' ";
                              //else if($LinkBanksType == "complete")
                              //    $sql .= " and LinkStatus = 'complete' ";
                              //else if($LinkBanksType == "evergreen")
                              //    $sql .= " and LinkStatus = 'evergreen' ";
                              //else if($LinkBanksType == "mylink")
                              //    $sql .= " and LinkStatus = 'mylink' ";

                              //if(isset($_GET["act"]) && $_GET["act"] == "filter_linkbank_vendor")
                              //    $sql .= " and ".DB()->linkbanks.".VendorID = '".intval($_GET['VendorID'])."' ";

                              //if(isset($_GET["act"]) && $_GET["act"] == "filter_linkbank_group")
                              //    $sql .= " and ".DB()->linkbanks.".GroupID = '".intval($_GET['GroupID'])."' ";
                              

                              //$sql .= " group by ".DB()->linkbanks.".id ";
                              //if(isset($_POST["act"]) && $_POST["act"] == "search_linkbank"){
                              //    $keywords = " like '%".$_POST["keywords"]."%' ";
                              //    $HavingSQL = array();
                              //    foreach ($columns as $column){
                              //        if(!empty($column)){
                              //            $HavingSQL[] = $column.$keywords;
                              //        }
                              //    }
                              //    $sql .= " having ".implode(" or ", $HavingSQL);
                              //}


                              //$LinkBanks = DB()->get_results($sql);
                              $LinkBanks = DBLinkbanks()->get_typed_rows($Rows);
                              foreach($LinkBanks as $LinkBank){
                                  $GroupName = $LinkBank->GroupName;
                                  $VendorName = $LinkBank->VendorName;
                                  $userid = $LoggedInUser->id;
                                  $LinkBankID = $LinkBank->id;

                                  $UniqueClicks = $LinkBank->UniqueClicks;
                                  $RawClicks = $LinkBank->RawClicks;

                                  $Actions = $LinkBank->Actions;
                                  $PixelFires = $LinkBank->PixelFires;

                                  $ManualSale = DB()->get_row("select * from ".DB()->manualsales." where MasterCampaignID = 0 and LinkBankID=".$LinkBankID." and userid=".$LoggedInUser->id);
                                  if($ManualSale){
                                      $SaleAmount = get_manualsale_amount($LinkBankID);
                                      $ManualSaleLink = '<a href="'.get_site_url("manual-sale/split-partners/?LinkBankID=".$LinkBankID).'" class="" title="Manual Sale">$'.$SaleAmount['TotalAmount'].'</a>';
                                  } else {
                                      $ManualSaleLink = '<a href="'.get_site_url("manual-sale/?LinkBankID=".$LinkBankID).'" class="" title="Manual Sale">create</a>';
                                  }

                                  $LinkBankurl = get_linkbankurl($LinkBank);
                                  //$SaleInfoDetails = get_sale_info_details($LinkBank);

                                  $LinkStatus = $LinkBank->LinkStatus;
                                  if($LinkStatus == "active"){
                                      $LinkBankStatus = "Live";
                                      $LinkBankStatusIcon = "fa fa-rocket";
                                  }else if($LinkStatus == "pending"){
                                      $LinkBankStatus = "Pending";
                                      $LinkBankStatusIcon = "fa fa-hourglass-o";
                                  }else if($LinkStatus == "complete"){
                                      $LinkBankStatus = "Completed";
                                      $LinkBankStatusIcon = "fa fa-check-square-o";
                                  }else if($LinkStatus == "evergreen"){
                                      $LinkBankStatus = "Evergreen";
                                      $LinkBankStatusIcon = "fa fa-tree";
                                  }else if($LinkStatus == "mylink"){
                                      $LinkBankStatus = "My Links";
                                      $LinkBankStatusIcon = "fa fa-user";
                                  }

                                  $namelinkedurl = get_site_url("linkbank/?LinkBankID=".$LinkBankID);

                                  $StatusIcon = get_domain_status_icon($LinkBank->DomainID);

                                  $MasterCampaignName = $LinkBank->MasterCampaignName;

                                  $ConversionsCount = $LinkBank->ConversionsCount;
                                  $ConversionsAmount = $LinkBank->ConversionsAmount;
                                  $ConversionsPercentage =  $LinkBank->ConversionsPercentage;//$UniqueClicks == 0 ? 0 : ($ConversionsCount / $UniqueClicks * 100);

                                  $actions_percentage = $Actions / $UniqueClicks * 100;
						?>
                        <tr>
                            <td class="text-center hidden-xs"><?php echo $StatusIcon;?></td>
                            <?php if($LinkBankSaleType == "conv") { ?>
                            <td class="text-center grey-scale"><a href="javascript:" data-url="<?php site_url($curpage."?act=linkbank_details&LinkBankID=".$LinkBankID);?>" data-toggle="DetailRow" id="" class="btn btn-rotator-detail"><i class="fa fa-caret-square-o-down"></i></a></td>
                            <?php } ?>
                            <td class="text-center">
                                <a href="<?php site_url("linkbank/?LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links visible-xs-inline-block" title="Edit Link"><i class="fa fa-pencil-square"></i></a>
                                <a href="javascript: void(0);" data-clipboard-text="<?php echo $LinkBankurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $LinkBankurl;?>">Copy</a>
                            </td>
                            <td>
                                <a href="<?php echo $namelinkedurl; ?>" class="pull-left qtip2" data-qtip-image="<?php echo PREVIEW_URL.$LinkBankurl;?>"><?php echo $LinkBank->LinkName?></a>
                            </td>
                            
                            <td class="hidden-xs"><span><?php echo $VendorName;?></span></td>
                            <td class="text-center"><span><?php echo get_formated_number($RawClicks);?></span></td>
                            <td class="text-center"><span><?php echo get_formated_number($UniqueClicks);?></span></td>
                            <?php if($LinkBankSaleType == "") { ?>
                            <td class="text-center hidden-xs"><span><?php echo $ManualSaleLink;?></span></td>
                            <?php } ?>
                            <?php if($LinkBankSaleType == "conv") { ?>
                            <td class="text-center"><span><?php echo get_formated_number($ConversionsCount);?></span></td>
                            <td class="text-center"><span>$<?php echo get_formated_number(round($ConversionsAmount, 2));?></span></td>
                            <td class="text-center"><span><?php echo get_formated_number(round($ConversionsPercentage, 2));?>%</span></td>
                            <?php } ?>
                            <td class="hidden-xs"><span><?php echo $GroupName;?></span></td>
                            <td class="text-center hidden-xs">
                                <span><?php echo $LinkBank->CreatedDate;?></span>
                            </td>
                            <td class="text-center grey-scale hidden-xs">
                                <a href="javascript:" class="status_icon" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" data-toggles="StatusForm" data-qtip-showevent="click" id=""><i class="fa <?php echo $LinkBankStatusIcon;?>"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <form class="form-inline status_form" role="form" method="get" action="<?php site_url($curpage.(!empty($_GET['linkbank_type'])?"/".$_GET['linkbank_type']."/":""));?>">
                                        <div class="form-group select-xs">
                                            <select name="status" id="LinkStatus" class="form-control select2">
                                                <?php foreach($LinkStatuses as $key => $value){ ?>
                                                <option value="<?php echo $key;?>" <?php if($LinkBank->LinkStatus == $key || (isset($_POST['LinkStatus']) && $_POST['LinkStatus'] == $key)) echo ' selected = "selected" ';?>><?php echo $value;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-xs btn-default">Change</button>
                                        <input type="hidden" name="act" value="changelinkstatus" />
                                        <input type="hidden" name="LinkBankID" value="<?php echo $LinkBankID;?>" />
                                    </form>
                                </div>
                            </td>
                            <td class="text-center hidden-xs">
                                <a href="javascript:" data-toggle="ShowQtipSubMenu" data-qtip-target=".qtipsubmenu" class="grey-scale pull-right-"><i class="fa fa-cog"></i></a>
                                <div class="qtipsubmenu grey-scale">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php site_url("linkbank/?LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links" title="Edit Link"><i class="fa fa-pencil-square"></i>Edit Link</a></li>
                                        <li><a href="<?php echo $LinkBankurl."/"?>" target="_blank" class="btn btn-sm btn-links" title="Direct Link"><i class="fa fa-external-link-square"></i>Direct Link</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=delete_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links btn-delete" title="Delete Link"><i class="fa fa-trash"></i>Delete Link</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=reset_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links btn-reset" title="Reset Statistics"><i class="fa fa-ban"></i>Reset Statistics</a></li>
                                        <li><a href="<?php site_url($curpage."/?act=clone_link&LinkBankID=".$LinkBankID);?>" class="btn btn-sm btn-links" title="Clone Link"><i class="fa fa-clone"></i>Clone Link</a></li>
                                        <li><a href="javascript: void(0);" data-clipboard-text="<?php echo $LinkBankurl;?>" class="btn btn-sm btn-links copy" title="Copy <?php echo $LinkBankurl;?>"><i class="fa fa-clipboard"></i>Copy Link</a></li>
                                        <li><a href="<?php site_url($pageurl."/".$LinkBankID."/details");?>" class="btn btn-sm btn-links" title="Statistics"><i class="fa fa-globe"></i>Statistics</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="loadmorewait">
                    <img src="<?php SYS()->site_url("images/ajaxloader.gif") ?>" class="" />
                </div>
                <?php //SYS()->create_pagination("linkbank-list", $TotalRows);?>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script src="<?php site_url("js/linkbank_stats.js");?>" type="text/javascript"></script>
<!-- /.content-wrapper -->
<?php
include_once "member_footer.php";
?>